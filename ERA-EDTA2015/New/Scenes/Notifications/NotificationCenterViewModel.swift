//
//  NotificationCenterViewModel.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 7/5/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RxSwift
import RxCocoa
import RealmSwift
import RxRealm
import DateToolsSwift

final class NotificationCenterViewModel : ViewModelType {
    
    let realm = try! Realm()
    
    func transform(input: Input) -> Output {
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
        //fetch from server and propagate errors
        let fetchNotifications = input.trigger.flatMapLatest { _ in
            return provider.request(.notifications)
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .mapObject(ArrayResponse<ConferenceNotification>.self)
                .do(onNext: { [weak self] (articlesResponse) in
                    guard let instance = self else { return }
                    try! instance.realm.write {
                        for n in articlesResponse.array! {
                            if let ntf = self?.realm.object(ofType: ConferenceNotification.self, forPrimaryKey: n.id) {
                                n.read = ntf.read
                            }
                            instance.realm.add(n, update: true)
                        }
                        
                    }
                })
                .mapToVoid()
                .asDriverOnErrorJustComplete()
        }
        let ntfs = realm.objects(ConferenceNotification.self).sorted(byKeyPath: "createdAt",
                                                                     ascending: false)
        let section = Observable.collection(from: ntfs)
            .map { n in return n.map { ntf in
                let date = Date().addingTimeInterval(TimeInterval(Calendar.current.timeZone.secondsFromGMT(for: Date())))
                return NotificationItemViewModel(with: ntf.message ?? "",
                                                 ago: ntf.createdAt?.shortTimeAgo(since: date) ?? "",
                                                 and: ntf.read)
                
                }
            }
            .map { [NotificationSection(items: $0)] }
            .asDriverOnErrorJustComplete()
        let selection = input.selection
            .map { [weak self] in
                return self?.realm.objects(ConferenceNotification.self)[$0.row]
            }
            .filterNil()
            .map { $0.id }
            .do(onNext: { ConferenceNotification.read($0) })
            .mapToVoid()
            .asDriver()
        let errors = errorTracker
            .asDriver()
            .map { $0.localizedDescription }
        let fetching = activityIndicator
            .asDriver()
        return Output(sections: section,
                      error: errors,
                      fetching: fetching,
                      toFetch: fetchNotifications,
                      onSelected: selection)
    }
    
}

extension NotificationCenterViewModel {
    
    struct Input {
        let trigger: Driver<Void>
        let selection: Driver<IndexPath>
    }
    
    struct Output {
        let sections: Driver<[NotificationSection]>
        let error: Driver<String>
        let fetching: Driver<Bool>
        let toFetch: Driver<Void>
        let onSelected: Driver<Void>
    }
    
}
