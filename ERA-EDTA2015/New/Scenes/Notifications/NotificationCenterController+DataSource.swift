//
//  NotificationCenterController+DataSource.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 7/5/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RxSwift
import RxCocoa
import RxDataSources

extension NotificationCenterController {
    static func dataSource() -> RxTableViewSectionedReloadDataSource<NotificationSection> {
        return RxTableViewSectionedReloadDataSource<NotificationSection>(
            configureCell: { (dataSource, table, idxPath, model) in
                let cell: NotificationCell = table.dequeueReusableCell(withIdentifier: NotificationCell.reuseId) as! NotificationCell
                cell.bind(model)
                return cell
        })
    }
}

struct NotificationSection {
    var items: [NotificationItemViewModel]
}

extension NotificationSection: SectionModelType {
    
    init(original: NotificationSection, items: [NotificationItemViewModel]) {
        self = NotificationSection(items: items)
    }
    
}
