//
//  NotificationCenterController.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 7/5/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RxSwift
import RxCocoa

class NotificationCenterController: UIViewController {
    
    var viewModel: NotificationCenterViewModel!
    private let disposeBag = DisposeBag()
    @IBOutlet weak var tableView: UITableView!
    fileprivate let ds = NotificationCenterController.dataSource()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //tv setup
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.refreshControl = UIRefreshControl()
        tableView.register(UINib(nibName: NotificationCell.reuseId, bundle: nil),
                           forCellReuseIdentifier: NotificationCell.reuseId)
        bindViewModel()
    }
    
    func bindViewModel() {
        assert(viewModel != nil)
        //refresh trigger
        let viewWillAppear = rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
            .mapToVoid()
            .asDriverOnErrorJustComplete()
        let pull = tableView.refreshControl!.rx
            .controlEvent(.valueChanged)
            .asDriverOnErrorJustComplete()
        let selection = tableView.rx.itemSelected
            .do (onNext: { [weak self] ip in self?.tableView.deselectRow(at: ip, animated: true) })
            .asDriverOnErrorJustComplete()
        //view model bindings
        let input = NotificationCenterViewModel.Input(trigger: Driver.merge(viewWillAppear, pull),
                                                      selection: selection)
        let output = viewModel.transform(input: input)
        //Bind Posts to UITableView
        output.sections
            .drive(tableView.rx.items(dataSource: ds))
            .disposed(by: disposeBag)
        output.fetching
            .drive(tableView.refreshControl!.rx.isRefreshing)
            .disposed(by: disposeBag)
        output.error
            .drive(rx.error)
            .disposed(by: disposeBag)
        output.toFetch
            .drive()
            .disposed(by: disposeBag)
        output.onSelected
            .drive()
            .disposed(by: disposeBag)
    }
    
}
