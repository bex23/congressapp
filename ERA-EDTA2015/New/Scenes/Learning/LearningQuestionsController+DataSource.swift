//
//  LearningQuestionsController+DataSource.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 5/18/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RxSwift
import RxCocoa
import RxDataSources

extension LearningQuestionsController {
    static func dataSource() -> RxTableViewSectionedReloadDataSource<LearningSectionModel> {
        return RxTableViewSectionedReloadDataSource<LearningSectionModel>(
            configureCell: { (dataSource, table, idxPath, _) in
                switch dataSource[idxPath] {
                case let .campaignSectionItem(viewModel):
                    let cell: CampaignCell = table.dequeueReusableCell(withIdentifier: CampaignCell.reuseId) as! CampaignCell
                    cell.bind(viewModel)
                    return cell
                case let .textSectionItem(title):
                    let cell = table.dequeueReusableCell(withIdentifier: "TextCell")
                    cell?.textLabel?.attributedText = title
                    return cell!
                }
        })
    }
}

enum LearningSectionModel {
    case campaignSection(items: [LearningSectionItem])
    case textSection(titles: [LearningSectionItem])
}

enum LearningSectionItem {
    case campaignSectionItem(item: CampaignViewModel)
    case textSectionItem(title: NSAttributedString)
}

extension LearningSectionModel: SectionModelType {

    typealias Item = LearningSectionItem
    
    var items: [LearningSectionItem] {
        switch  self {
        case .campaignSection(let its),
             .textSection(let its):
            return its
        }
    }
    
    init(original: LearningSectionModel, items: [LearningSectionItem]) {
        switch original {
        case .campaignSection:
            self = .campaignSection(items: items)
        case .textSection:
            self = .textSection(titles: items)
        }
    }
}
