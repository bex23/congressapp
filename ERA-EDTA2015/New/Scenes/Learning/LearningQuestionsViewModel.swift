//
//  LearningQuestionsViewModel.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 5/18/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RxSwift
import RxCocoa

final class LearningQuestionsViewModel : ViewModelType {
    
    let badge: String
    init(badge: String) {
        self.badge = badge
    }
    
    func transform(input: LearningQuestionsViewModel.Input) -> LearningQuestionsViewModel.Output {
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
        let headerSection: LearningSectionModel = .textSection(titles: [
            LearningSectionItem.textSectionItem(title: header1),
            LearningSectionItem.textSectionItem(title: header2),
            LearningSectionItem.textSectionItem(title: header3),
            LearningSectionItem.textSectionItem(title: header4)])
        let footerSection: LearningSectionModel = .textSection(titles: [LearningSectionItem.textSectionItem(title: footer)])
        let staticSections = Observable.of([headerSection,
                                            footerSection])
        let available = input.trigger
            .asObservable()
            .flatMapLatest { [weak self] _ in
                return provider.request(.availableCampaigns(badge: self?.badge ?? ""))
                    .trackActivity(activityIndicator)
                    .trackError(errorTracker)
                    .filterSuccessfulStatusCodes()
                    .mapJSON()
                    .map { $0 as? [String: Any] }
                    .map { $0?["data"] as? [Int] }
                    .filterNil()
            }
        let answered = input.trigger
            .asObservable()
            .flatMapLatest { [weak self] _ in
                return provider.request(.answeredCampaigns(badge: self?.badge ?? ""))
                    .trackActivity(activityIndicator)
                    .trackError(errorTracker)
                    .filterSuccessfulStatusCodes()
                    .mapJSON()
                    .map { $0 as? [String: Any] }
                    .map { $0?["data"] as? [Int] }
                    .filterNil()

            }
        let campaigns = input.trigger
            .asObservable()
            .flatMapLatest { _ in
                return provider.request(.campaigns)
                    .trackActivity(activityIndicator)
                    .trackError(errorTracker)
                    .filterSuccessfulStatusCodes()
                    .mapObject(ArrayResponse<Campaign>.self)
                    .map { $0.array }
                    .filterNil()
            }
        
        let all = Observable.zip(campaigns, answered, available)
            .map { args -> [CampaignViewModel] in
                let (camps, ans, ava) = args
                return camps.enumerated().map {
                    let vm = CampaignViewModel(with: $0.1.name ?? "",
                                               order: $0.0 + 1,
                                               and: ans.contains($0.1.id) ? .answered : ava.contains($0.1.id) ? .available : .unavailable)
                    return vm
                }
            }
            .map { return LearningSectionModel.campaignSection(items: $0.map { LearningSectionItem.campaignSectionItem(item: $0) }) }
            .withLatestFrom(staticSections) { (campaignsSection, staticSections) -> [LearningSectionModel] in
                var sections = [LearningSectionModel]()
                sections.append(staticSections.first!)
                sections.append(campaignsSection)
                sections.append(staticSections.last!)
                return sections
            }
        
        let errors = errorTracker
            .asDriver()
            .throttle(1.0)
            .map { $0.localizedDescription }
        let fetching = activityIndicator
            .asDriver()
        let selected = input.selection
            .asObservable()
            .filter { $0.section == 1 }
            .map { $0.row }
            .withLatestFrom(campaigns) { (i, camps) -> Campaign in
                return camps[i]
            }
            .withLatestFrom(available, resultSelector: { return ($0, $1) })
            .filter { $1.contains($0.id) }
            .map { $0.0 }
            .withLatestFrom(answered, resultSelector: { return ($0, $1) })
            .filter { !$1.contains($0.id) }
            .map { $0.0 }
            .do(onNext: Application.shared.navigator.toSurvey)
            .mapToVoid()
            .asDriverOnErrorJustComplete()
        return LearningQuestionsViewModel.Output(sections: all.asDriverOnErrorJustComplete(), fetching: fetching, error: errors, selected: selected)
    }
    
}

extension LearningQuestionsViewModel {
    
    struct Input {
        let trigger: Driver<Void>
        let selection: Driver<IndexPath>
    }
    
    struct Output {
        let sections: Driver<[LearningSectionModel]>
        let fetching: Driver<Bool>
        let error: Driver<String>
        let selected: Driver<Void>
    }
    
}

extension LearningQuestionsViewModel {
    
    open var header1: NSAttributedString {
        get {
            let temp = NSMutableAttributedString(
                string:"\nTest your Knowledge before and after the ERA-EDTA 2018 congress",
                attributes: [.font : UIFont(name: "Avenir-Heavy", size: 13.0)!,
                             .foregroundColor: UIColor(red: 60.0/255.0, green: 60.0/255.0, blue: 60.0/255.0, alpha: 1.0)])
            return temp
        }
    }
    open var header2: NSAttributedString {
        get {
            let temp = NSMutableAttributedString(string: "Test your knowledge now and after the congress and track your learning progress. \rCompare your results with your those of your peers. \r\rStep 1: Select your preferred category below and answer the questions now\rStep 2: After the congress you will get an email and you can try these questions for a second time.\rStep 3: We will then send you", attributes: [
                .font: UIFont(name: "Avenir-Light", size: 14.0)!,
                .foregroundColor: UIColor(white: 60.0 / 255.0, alpha: 1.0)
                ])
            temp.addAttribute(.font, value: UIFont(name: "Avenir-Heavy", size: 14.0)!, range: NSRange(location: 136, length: 7))
            temp.addAttribute(.font, value: UIFont(name: "Avenir-Heavy", size: 14.0)!, range: NSRange(location: 210, length: 7))
            temp.addAttribute(.font, value: UIFont(name: "Avenir-Heavy", size: 14.0)!, range: NSRange(location: 310, length: 7))
            return temp
        }
    }
    
    open var header3: NSAttributedString {
        get {
            let temp = NSMutableAttributedString(
                string:" •    the correct answers\n •    how you improved over time\n •    how your peers performed",
                attributes: [.font : UIFont(name: "Avenir-Light", size: 13.0)!,
                             .foregroundColor: UIColor(red: 60.0/255.0,
                                                       green: 60.0/255.0,
                                                       blue: 60.0/255.0,
                                                       alpha: 1.0)])
            return temp
        }
    }
    
    open var header4: NSAttributedString {
        get {
            let temp = NSMutableAttributedString(
                string:"So start now and watch out for those topics during the scientific program of the ERA-EDTA 2018 congress. Happy Learnings and Discoveries.\n\nSelect at least one category:",
                attributes: [.font : UIFont(name: "Avenir-Light", size: 13.0)!,
                             .foregroundColor: UIColor(red: 60.0/255.0,
                                                       green: 60.0/255.0,
                                                       blue: 60.0/255.0,
                                                       alpha: 1.0)])
            return temp
        }
    }
    
    open var footer: NSAttributedString {
        get {
            let temp = NSMutableAttributedString(string: "Completing this activity counts towards the ENP Learning Quest where you can win a free ENP  T-Shirt.\n\nTo collect your stamp for this activity or for more information visit the ENP Station at the e-campus in Hall E.", attributes: [
                .font: UIFont(name: "Avenir-Light", size: 14.0)!,
                .foregroundColor: UIColor(white: 60.0 / 255.0, alpha: 1.0)
                ])
            temp.addAttribute(.font, value: UIFont(name: "Avenir-Heavy", size: 14.0)!, range: NSRange(location: 44, length: 18))
            return temp
        }
    }
    
    
}
