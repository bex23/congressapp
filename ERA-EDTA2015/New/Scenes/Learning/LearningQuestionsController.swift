//
//  LearningQuestionsController.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 5/18/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RxSwift
import RxCocoa

class LearningQuestionsController: UIViewController {
    
    var viewModel: LearningQuestionsViewModel!
    private let disposeBag = DisposeBag()
    
    @IBOutlet private weak var tableView: UITableView!
    fileprivate let ds = LearningQuestionsController.dataSource()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "LEARNING QUESTIONS"
        tableView.refreshControl = UIRefreshControl()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.register(UINib(nibName: ConferenceCell.reuseId, bundle: nil),
                           forCellReuseIdentifier: ConferenceCell.reuseId)
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
        }
        bindViewModel()
    }
    
    func bindViewModel() {
        assert(viewModel != nil)
        //refresh trigger
        let viewWillAppear = rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
            .mapToVoid()
            .asDriverOnErrorJustComplete()
        let pull = tableView.refreshControl!.rx
            .controlEvent(.valueChanged)
            .asDriver()
        let selection = tableView.rx.itemSelected
            .do (onNext: { [weak self] ip in self?.tableView.deselectRow(at: ip, animated: true) })
            .asDriverOnErrorJustComplete()
        //view model bindings
        let input = LearningQuestionsViewModel.Input(trigger: Driver.merge(viewWillAppear, pull),
                                                     selection: selection)
        let output = viewModel.transform(input: input)
        output.sections
            .drive(tableView.rx.items(dataSource: ds))
            .disposed(by: disposeBag)
        output.fetching
            .drive(tableView.refreshControl!.rx.isRefreshing)
            .disposed(by: disposeBag)
        output.error
            .drive(rx.error)
            .disposed(by: disposeBag)
        output.selected
            .drive()
            .disposed(by: disposeBag)
    }
    
}

extension Reactive where Base: LearningQuestionsController {
    
    /// Bindable sink for `progress` property
    fileprivate var error: Binder<String> {
        return Binder(self.base) { controller, message in
            let alert = UIAlertController(title: "Error",
                                          message: message,
                                          preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK",
                                          style: .cancel,
                                          handler: nil))
            DispatchQueue.main.async {
                controller.present(alert, animated: true)
            }
        }
    }
    
}

