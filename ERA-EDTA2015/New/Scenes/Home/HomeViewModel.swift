//
//  HomeViewModel.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 4/7/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RxSwift
import RxCocoa
import RealmSwift
import RxRealm

final class HomeViewModel: NSObject, ViewModelType {
    
    let realm = try! Realm()
    private let notificationCount: BehaviorSubject<Int>
    var beaconSubject = PublishSubject<Int>()

    override init() {
        notificationCount = Application.shared.ntfCount
        super.init()
    }
    
    func transform(input: Input) -> Output {
        let learningKitSelection = PublishSubject<Int>()
        let newsSelection = PublishSubject<Int>()
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
        let fetchArticles = input.trigger.flatMapLatest { _ in
            return provider.request(.articles(page: 1))
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .mapObject(ArrayResponse<Article>.self)
                .do(onNext: { [weak self] (articlesResponse) in
                    guard let instance = self else { return }
                    try! instance.realm.write {
                        instance.realm.add(articlesResponse.array!, update: true)
                    }
                })
                .map { $0.array }
                .asDriverOnErrorJustComplete()
        }
        
        let talks = input.trigger.flatMapLatest { _ in
            return provider.request(.recommendations)
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .mapObject(ArrayResponse<Presentation>.self)
                .map { $0.array }
                .filterNil()
                .map { presentations -> [Int] in
                    let ids = presentations.map { pres -> Int in
                        return pres.id
                    }
                    return ids
                }
                .map { ids -> [Talk] in
                    if ids.count == 0 { return [Talk]() }
                    let predicate = NSPredicate(format: "tlkTalkId IN %@", ids)
                    let talks = LLDataAccessLayer.sharedInstance().fetchManagedObjects(withName: "Talk",
                                                                                       predicate: predicate,
                                                                                       sortDescriptors: nil,
                                                                                       inMOC: LLDataAccessLayer
                                                                                        .sharedInstance()
                                                                                        .managedObjectContext) as! [Talk]
                    var limited = [Talk]()
                    for talk in talks {
                        if limited.count < 3 {
                            limited.append(talk)
                        } else {
                            break
                        }
                    }
                    return limited
                }
                .asDriverOnErrorJustComplete()
        }
                
            let talksSection = talks
                .map { talks -> MultipleSectionModel in
                    if talks.count > 0 {
                        var mappedTalks: [SectionItem] = talks.map { .recommendationItem(item: $0) }
                        mappedTalks.append(.allRecommendations)
                        return MultipleSectionModel.recommendationsSection(title: "RECOMMENDED FOR YOU",
                                                                           items: mappedTalks)
                        
                    } else {
                        return MultipleSectionModel.recommendationsSection(title: "No recommendations available at this moment.",
                                                                           items: [])
                    }
            }.startWith(MultipleSectionModel.recommendationsSection(title: "No recommendations available at this moment.",
                                                                    items: []))
        
        let articles = realm.objects(Article.self)
            .sorted(byKeyPath: "publishedAt", ascending: false)
        let articlesSection = Observable.collection(from: articles)
            .map { articles -> [Article] in
                var array = [Article]()
                for article in articles {
                    if array.count < 10 {
                        array.append(article)
                    } else {
                        break
                    }
                }
                return array
            }
            .filter { $0.count > 0 }
            .flatMap { articles -> Observable<MultipleSectionModel> in
                let vms = articles.map { NewsItemViewModel(with: $0.title ?? "",
                                                              and: URL(string: $0.croppedUrl ?? "")) }
                var items = vms.map { NewsSubsection.item(item: $0) }
                items.append(NewsSubsection.all)
                let section: MultipleSectionModel = .newsSection(title: "NEWS",
                                                                 items: [.newsItems(items: items,
                                                                                    row: newsSelection)])
                return Observable.of(section)
            }.startWith(.newsSection(title: "NEWS",
                                     items: []))
        
        let learningSection: MultipleSectionModel = .learningKitSection(title: "LEARNING KIT",
                                                                        items: [.learningKitItems(items:  Application.shared.learningSection, row: learningKitSelection)])
        let textSection: MultipleSectionModel = .textColorSection(title: "",
                                                                  items: [.textColorItem(title: "INDUSTRY SYMPOSIA", colorHex: "426397"),
                                                                          .textColorItem(title: "EXHIBITORS", colorHex: "f28c0d"),
                                                                          .textColorItem(title: "#ERA-EDTA TWITTER FEED", colorHex: "019eda")])
        let staticSections = Observable.of([learningSection,
                                            textSection])
        let sectionsDriver = Observable
            .combineLatest(staticSections, articlesSection, talksSection.asObservable()) { staticSections, articles, talks -> [MultipleSectionModel] in
                var arr = [MultipleSectionModel]()
                arr.append(staticSections[0])
                arr.append(articles)
                arr.append(talks)
                arr.append(staticSections[1])
                return arr
            }
            .asDriverOnErrorJustComplete()
        //Output
        let requests = fetchArticles
            .mapToVoid()
        let errors = errorTracker
            .asDriver()
            .map { $0.localizedDescription }
        let fetching = activityIndicator
            .asDriver()
        let textSelection = input.selection
            .filter { $0.section == 3 }
            .map { $0.row }
            .do(onNext: { row in
                if row == 0 {
                    Application.shared.navigator.toIndustrySymposia()
                } else if row == 1 {
                    Application.shared.navigator.toExhibitorsAndSponsors()
                } else if row == 2 {
                    Application.shared.navigator.toTwitter()
                }
        })
        .mapToVoid()
        let recommendationSelection = input.selection
            .filter { $0.section == 2 }
            .map { $0.row }
            .withLatestFrom(talks.asDriver().distinctUntilChanged()) { (row, trt) in return (row, trt) }
        let allRecommendationsSelection = recommendationSelection
            .filter { $0.0 == $0.1.count }
            .mapToVoid()
            .do(onNext: Application.shared.navigator.toProgramme)
        let recommendedPresentationSelection = recommendationSelection
            .filter { $0.0 != $0.1.count }
            .do(onNext: { (row, talks) in
                Application.shared.navigator.toTalk(talks: talks,
                                                    index: row)
            })
            .map { $0.1[$0.0] }
            .flatMap { talk in
                return provider.request(.track(action: .click,
                                               id: Int(truncating: talk.tlkTalkId ?? 0),
                                               type: "Presentation",
                                               objectInfo: "recommendation-ios"))
                        .mapToVoid()
                        .asDriverOnErrorJustComplete()
            }
        let ntf = input.notification
            .do(onNext: Application.shared.navigator.toNotifications)
        let learningKitOutput = learningKitSelection
            .asObservable()
            .map(Application.shared.react)
            .asDriverOnErrorJustComplete()
        
        let toArticle = newsSelection
            .filter { (articles.count <= 10 && $0 != articles.count) || (articles.count > 10 && $0 != 10) }
            .asObservable()
            .map { articles[$0] }
            .do(onNext: Application.shared.navigator.toArticle)
            .mapToVoid()
            .asDriverOnErrorJustComplete()
        
        let toAllArticles = newsSelection
            .filter { (articles.count <= 10 && $0 == articles.count) || (articles.count > 10 && $0 == 10) }
            .mapToVoid()
            .do(onNext: Application.shared.navigator.toAllArticles)
            .asDriverOnErrorJustComplete()
        
        let toAdditional = input.activities
            .do(onNext: Application.shared.navigator.toEraEdtaActivites)
        
        let refresh = Observable<Int>
            .interval(600.0, scheduler: MainScheduler.instance)
            .startWith(1)
            .mapToVoid()
            .do(onNext: ESyncData.sharedInstance().syncWithServerRepeatedly )
            .asDriverOnErrorJustComplete()
        
        let refreshConfInfo = input
            .trigger
            .asObservable()
            .take(1)
            .flatMapLatest { _ in
                return provider.request(.portal)
                    .trackError(errorTracker)
                    .mapObject(PortalResponse.self)
                    .do(onNext: { [weak self] (portal) in
                        guard let instance = self else { return }
                        try! instance.realm.write {
                            instance.realm.add(portal.portal, update: true)
                            if let confs = portal.conferences {
                                instance.realm.add(confs, update: true)
                            }
                        }
                    })
            }
            .mapToVoid()
            .asDriverOnErrorJustComplete()
        
        let beacon = beaconSubject
            .throttle(120.0, scheduler: MainScheduler.instance)
            .flatMap { id in
                return provider.request(.track(action: .view,
                                               id: id,
                                               type: "Beacon",
                                               objectInfo: UIDevice.current.identifierForVendor!.uuidString))
            }
            .asDriverOnErrorJustComplete()
            .mapToVoid()


        
        return Output(refresh: refresh,
                      fetching: fetching,
                      error: errors,
                      sections: sectionsDriver,
                      selection: Driver.merge(textSelection,
                                              allRecommendationsSelection,
                                              recommendedPresentationSelection,
                                              learningKitOutput,
                                              toArticle,
                                              toAllArticles,
                                              ntf,
                                              requests,
                                              toAdditional,
                                              refreshConfInfo),
                      notificationCount: notificationCount.asDriverOnErrorJustComplete(),
                      beacon: beacon)
    }
    
}

extension HomeViewModel {
    
    struct Input {
        let trigger: Driver<Void>
        let activities: Driver<Void>
        let selection: Driver<IndexPath>
        let notification: Driver<Void>
    }
    
    struct Output {
        let refresh: Driver<Void>
        let fetching: Driver<Bool>
        let error: Driver<String>
        let sections: Driver<[MultipleSectionModel]>
        let selection: Driver<Void>
        let notificationCount: Driver<Int>
        let beacon: Driver<Void>
    }
    
}
