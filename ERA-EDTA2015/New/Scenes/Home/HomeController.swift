//
//  HomeViewController.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 4/3/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RxSwift
import RxCocoa
import RxGesture

class HomeController: UIViewController {
    //MARK: Properties
    //rx+vm
    var viewModel: HomeViewModel!
    private let disposeBag = DisposeBag()
    private lazy var bannerService: BannerService = {
        let temp = BannerService.getService(view: self.view)
        return temp
    }()
    private lazy var footerImageView: UIImageView = {
        let width = UIScreen.main.bounds.width
        let iv = UIImageView(frame: CGRect(x: 0.0,
                                           y: 0.0,
                                           width: width,
                                           height: width * 0.408))
        return iv
    }()
    @IBOutlet private weak var tableView: UITableView!
    fileprivate var storedOffsets = [Int: CGFloat]()
    fileprivate let ds = HomeController.dataSource()
    private lazy var notificationButton: UIButton = {
        let image = #imageLiteral(resourceName: "notification").withRenderingMode(.alwaysOriginal)
        let buttonView = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        buttonView.setBackgroundImage(image, for: .normal)
        return buttonView
    }()
    //MARK: View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.titleView = UIImageView(image: #imageLiteral(resourceName: "congressLogoSmaller"))
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: notificationButton)
        //customize badge
        navigationItem.rightBarButtonItem!.badgeBGColor = .blue //UIColor(hex: kOtherColor, andAlpha: 1.0)
        navigationItem.rightBarButtonItem!.badgeTextColor = .black
        navigationItem.rightBarButtonItem!.badgeFont = UIFont(name: "Avenir-Medium", size: 11.0)
        navigationItem.rightBarButtonItem!.badgeOriginX -= 12
        navigationItem.rightBarButtonItem!.badgeOriginY = -1
        navigationItem.rightBarButtonItem!.badgePadding = 3
        //tv setup
        tableView.refreshControl = UIRefreshControl()
        tableView.register(UINib(nibName: CollectionTableViewCell.reuseId, bundle: nil),
                           forCellReuseIdentifier: CollectionTableViewCell.reuseId)
        tableView.register(UINib(nibName: RecommendationCell.reuseId, bundle: nil),
                           forCellReuseIdentifier: RecommendationCell.reuseId)
        tableView.register(UINib(nibName: RoundButtonCell.reuseId, bundle: nil),
                           forCellReuseIdentifier: RoundButtonCell.reuseId)
        tableView.tableFooterView = FooterView().fromNib() as! FooterView
        tableView.register(UINib(nibName: TextCell.reuseId, bundle: nil),
                           forCellReuseIdentifier: TextCell.reuseId)

        bindViewModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        BannersServices.shared.add(imageView: footerImageView, at: .bottomBanner, sender: self)
        bannerService.showBannerIfNeeded()
    }
    
    func bindViewModel() {
        assert(viewModel != nil)
        //additional tv methods
        tableView.rx.setDelegate(self)
            .disposed(by: disposeBag)
        //refresh trigger
        let viewWillAppear = rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
            .mapToVoid()
            .asDriverOnErrorJustComplete()
        let pull = tableView.refreshControl!.rx
            .controlEvent(.valueChanged)
            .asDriverOnErrorJustComplete()
        let selection = tableView.rx.itemSelected
            .do (onNext: { [weak self] ip in self?.tableView.deselectRow(at: ip, animated: true) })
            .asDriverOnErrorJustComplete()
        //view model bindings
        let input = HomeViewModel.Input(trigger: Driver.merge(viewWillAppear, pull),
                                        activities: tableView.tableFooterView!.rx
                                            .tapGesture()
                                            .skip(1)
                                            .mapToVoid()
                                            .asDriverOnErrorJustComplete(),
                                        selection: selection,
                                        notification: notificationButton.rx
                                            .tap
                                            .asDriver())
        let output = viewModel.transform(input: input)
        //Bind Posts to UITableView
        output.sections
            .drive(tableView.rx.items(dataSource: ds))
            .disposed(by: disposeBag)
        output.fetching
            .drive(tableView.refreshControl!.rx.isRefreshing)
            .disposed(by: disposeBag)
        output.error
            .drive(rx.error)
            .disposed(by: disposeBag)
        output.selection
            .drive()
            .disposed(by: disposeBag)
        output.refresh
            .drive()
            .disposed(by: disposeBag)
//        output.beacon
//            .drive()
//            .disposed(by: disposeBag)
        output.notificationCount
            .drive(rx.notificationCount)
            .disposed(by: disposeBag)
    }
    
}

extension HomeController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let collectionViewCell = cell as? CollectionTableViewCell else { return }
        collectionViewCell.collectionViewOffset = storedOffsets[indexPath.section] ?? 0
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let collectionViewCell = cell as? CollectionTableViewCell else { return }
        storedOffsets[indexPath.section] = collectionViewCell.collectionViewOffset
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return SectionHeaderView.with(ds[section].title)
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == 0 {
            return footerImageView
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 || section == 1 || section == 2 {
            return 46.0
        } else {
            return 0.0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 0 {
            return UIScreen.main.bounds.width * 0.408
        }
        return 0
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let section = indexPath.section
        if section == 0 {
            return 100.0
        } else if section == 1 {
            return 160.0
        } else if section == 2 {
            let items = ds[section].items
            if indexPath.row == items.count - 1 {
                return 60.0
            } else {
                return UITableViewAutomaticDimension
            }
        } else {
            return 60.0
        }
    }
    
}

extension Reactive where Base: HomeController {
    
    fileprivate var notificationCount: Binder<Int> {
        return Binder(self.base) { controller, value in
            controller.navigationItem.rightBarButtonItem!.badgeBGColor = UIColor(hex: kOtherColor, andAlpha: 1.0)
            controller.navigationItem.rightBarButtonItem!.badgeValue = "\(value)"
        }
    }

}

extension Reactive where Base: UIViewController {
    
    /// Bindable sink for `progress` property
    var error: Binder<String> {
        return Binder(self.base) { controller, message in
            let alert = UIAlertController(title: "Error",
                                          message: message,
                                          preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK",
                                          style: .cancel,
                                          handler: nil))
            DispatchQueue.main.async {
                controller.present(alert, animated: true)
            }
        }
    }
    
}

