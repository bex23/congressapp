//
//  HomeController+DataSource.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 4/23/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RxSwift
import RxCocoa
import RxDataSources
import Kingfisher

extension HomeController {
    static func dataSource() -> RxTableViewSectionedReloadDataSource<MultipleSectionModel> {
        return RxTableViewSectionedReloadDataSource<MultipleSectionModel>(
            configureCell: { (dataSource, table, idxPath, _) in
                switch dataSource[idxPath] {
                case let .learningKitItems(items, subject):
                    let cell: CollectionTableViewCell = table.dequeueReusableCell(withIdentifier: CollectionTableViewCell.reuseId, for: idxPath) as! CollectionTableViewCell
                    Observable.of(items)
                        .asDriverOnErrorJustComplete()
                        .drive(cell.collectionView.rx.items) { (collectionView, row, element) in
                            let itemCell = collectionView.dequeueReusableCell(withReuseIdentifier: LearningKitItemCell.reuseId,
                                                                              for: IndexPath(row: row, section: 0) ) as! LearningKitItemCell
                            itemCell.bind(element)
                            return itemCell
                        }
                        .disposed(by: cell.disposeBag)
                    cell.collectionView.rx
                        .itemSelected
                        .map { $0.row }
                        .bind(to: subject)
                        .disposed(by: cell.disposeBag)
                    return cell
                case let .newsItems(items, subject):
                    let cell: CollectionTableViewCell = table.dequeueReusableCell(withIdentifier: CollectionTableViewCell.reuseId, for: idxPath) as! CollectionTableViewCell
                    cell.height = 160                    
                    Observable.of(items)
                        .asDriverOnErrorJustComplete()
                        .drive(cell.collectionView.rx.items) { (collectionView, row, element) in
                            switch element {
                            case .item(let vm):
                                let itemCell = collectionView.dequeueReusableCell(withReuseIdentifier: ArticleCollectionViewCell.reuseId,
                                                                                  for: IndexPath(row: row, section: 0) ) as! ArticleCollectionViewCell
                                itemCell.bind(vm)
                                return itemCell
                            case .all:
                                let buttonCell = collectionView.dequeueReusableCell(withReuseIdentifier: RoundButtonCollectionViewCell.reuseId,
                                                                                  for: IndexPath(row: row, section: 0) ) as! RoundButtonCollectionViewCell
                                return buttonCell
                            }
                        }
                        .disposed(by: cell.disposeBag)
                    cell.collectionView.rx
                        .itemSelected
                        .map { $0.row }
                        .bind(to: subject)
                        .disposed(by: cell.disposeBag)
                    return cell
                case let .textColorItem(text, colorHex):
                    let cell: TextCell = table.dequeueReusableCell(withIdentifier: TextCell.reuseId) as! TextCell
                    cell.titleLabel.text = text
                    cell.color = UIColor(hex: colorHex, andAlpha: 1.0)
                    return cell
                case let .recommendationItem(item):
                    let cell: RecommendationCell = table.dequeueReusableCell(withIdentifier: RecommendationCell.reuseId) as! RecommendationCell
                    cell.setUp(withTalk: item)
                    return cell
                case .allRecommendations:
                    let cell: RoundButtonCell = table.dequeueReusableCell(withIdentifier: RoundButtonCell.reuseId) as! RoundButtonCell
                    return cell
                }
        }, titleForHeaderInSection: { dataSource, index in
            let section = dataSource[index]
            return section.title
        })
    }
}

enum MultipleSectionModel {
    case learningKitSection(title: String, items: [SectionItem])
    case newsSection(title: String, items: [SectionItem])
    case textColorSection(title: String, items: [SectionItem])
    case recommendationsSection(title: String, items: [SectionItem])
}

enum SectionItem {
    case learningKitItems(items: [LearningKitItemViewModel], row: PublishSubject<Int>)
    case newsItems(items: [NewsSubsection], row: PublishSubject<Int>)
    case textColorItem(title: String, colorHex: String)
    case recommendationItem(item: Talk)
    case allRecommendations
}

enum NewsSubsection {
    case item(item: NewsItemViewModel)
    case all
}

extension MultipleSectionModel: SectionModelType {
    typealias Item = SectionItem
    
    var items: [SectionItem] {
        switch  self {
        case .learningKitSection(_, let its),
             .newsSection(_, let its),
             .textColorSection(_ , let its),
             .recommendationsSection(_, let its):
            return its
        }
    }
    
    init(original: MultipleSectionModel, items: [Item]) {
        switch original {
        case let .learningKitSection(title: title, items: _):
            self = .learningKitSection(title: title, items: items)
        case let .newsSection(title: title, items: _):
            self = .newsSection(title: title, items: items)
        case let .textColorSection(title: title, items: _):
            self = .textColorSection(title: title, items: items)
        case let .recommendationsSection(title: title, items: _):
            self = .recommendationsSection(title: title, items: items)
        }
    }
}

extension MultipleSectionModel {
    var title: String {
        switch self {
        case .learningKitSection(title: let title, items: _),
             .newsSection(title: let title, items: _),
             .textColorSection(title: let title, items: _),
             .recommendationsSection(title: let title, items: _):
            return title
        }
    }
}
