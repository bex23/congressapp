//
//  SettingsController+DataSource.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 4/27/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RxSwift
import RxCocoa
import RxDataSources

extension SettingsController {
    static func dataSource() -> RxTableViewSectionedReloadDataSource<SettingsSectionModel> {
        return RxTableViewSectionedReloadDataSource<SettingsSectionModel>(
            configureCell: { (dataSource, table, idxPath, _) in
                switch dataSource[idxPath] {
                case let .text(title):
                    let cell = table.dequeueReusableCell(withIdentifier: "Cell")
                    cell?.textLabel?.text = title
                    cell?.imageView?.image = nil
                    return cell!
                case let .image(title, image):
                    let cell = table.dequeueReusableCell(withIdentifier: "Cell")
                    cell?.textLabel?.text = title
                    cell?.imageView?.image = image
                    return cell!
                case let .switchCell(title, state):
                    let cell = table.dequeueReusableCell(withIdentifier: SwitchCell.reuseId) as! SwitchCell
                    cell.title.text = title
                    cell.cellSwitch.isOn = try! state.value()
                    cell.cellSwitch.rx.value
                        .bind(to: state.asObserver())
                        .disposed(by: cell.disposeBag)
                    return cell
                }
        }, titleForHeaderInSection: { dataSource, index in
            let section = dataSource[index]
            return section.title
        })
    }
}

enum SettingsSectionModel {
    case text(title: String, items: [SettingsItem])
    case image(title: String, items: [SettingsItem])
    case switchSection(title: String, items: [SettingsItem])
}

enum SettingsItem {
    case text(title: String)
    case image(title: String, image: UIImage)
    case switchCell(title: String, state: BehaviorSubject<Bool>)
}

extension SettingsSectionModel: SectionModelType {
    typealias Item = SettingsItem
    
    var items: [SettingsItem] {
        switch  self {
        case .text(_, let its),
             .image(_, let its),
             .switchSection(_, let its):
            return its.map { $0 }
        }
    }
    
    init(original: SettingsSectionModel, items: [Item]) {
        switch original {
        case .text(let title, _):
            self = .text(title: title, items: items)
        case .image(let title, _):
            self = .image(title: title, items: items)
        case .switchSection(let title, _):
            self = .switchSection(title: title, items: items)
        }
    }
    
}

extension SettingsSectionModel {
    var title: String {
        switch self {
        case .text(title: let title, items: _),
             .image(title: let title, items: _),
             .switchSection(title: let title, items: _):
            return title
        }
    }
}
