//
//  SettingsViewModel.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 4/27/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RxSwift
import RxCocoa
import RxDataSources
import RealmSwift
import RxRealm

final class SettingsViewModel: ViewModelType {
    
    let realm = try! Realm()
    
    func transform(input: SettingsViewModel.Input) -> SettingsViewModel.Output {
        let type: UIAlertControllerStyle = (UIDevice.current.userInterfaceIdiom == .phone) ? .actionSheet : .alert
        let value = UserDefaultsHelper.helper.fetch(.hcp) as? Bool ?? true
        let hcpSubject = BehaviorSubject<Bool>(value: value)
        let sections: [SettingsSectionModel] = [.image(title: "LEARNING CENTRE",
                                                       items: Application.shared.learningItems.map { $0.settingsItem }),
                                                .text(title: "CONGRESS",
                                                      items: [.text(title: "Programme"),
                                                              .text(title: "Speakers"),
                                                              .text(title: "Congress Info"),
                                                              .text(title: "Exhibitors"),
                                                              .text(title: "Industry Symposia"),
                                                              .text(title: "Twitter"),
                                                              .text(title: "ERA-EDTA Activities")]),
                                                .switchSection(title: "SETTINGS",
                                                               items: [.switchCell(title: "Health care professional",
                                                                                   state: hcpSubject)]),
                                                .text(title: "",
                                                      items: [.text(title: "About App"),
                                                              .text(title: "BACK TO ERA-EDTA")])]
        let login = input.loginAction
            .skip(1)
            .filter { User.loggedIn == nil }
            .do (onNext: { _ in
                let loginNavigation = UIStoryboard(name: "LoginRegistration",
                                                   bundle: nil).instantiateViewController(withIdentifier: "navLogin")
                UIViewController.topViewController().present(loginNavigation,
                                                             animated: true)
            })
            .asDriver()
        let logout = input.logoutAction
            .filter { User.loggedIn != nil }
            .flatMapLatest { _ -> Driver<Void> in
                return UIViewController.topViewController().promptFor(title: "",
                                                                      message: "Are you sure you want to log out?",
                                                                      type: type,
                                                                      cancelAction: "Cancel",
                                                                      destructiveAction: "Log Out")
                    .filter { $0 == "Log Out" }
                    .subscribeOn(MainScheduler.instance)
                    .mapToVoid()
                    .do(onNext: { _ in
                        ESyncData.sharedInstance().logOut()
                    })
                    .asDriverOnErrorJustComplete()
            }
            .asDriver()
        let backToPortal = input.selection
            .filter { $0.section == sections.count - 1 && $0.row == 1 }
            .flatMapLatest { _ -> Driver<Void> in
                return UIViewController.topViewController().promptFor(title: "",
                                                                      message: "Are you sure you want to go back to portal page?",
                                                                      type: type,
                                                                      cancelAction: "No",
                                                                      destructiveAction: "Yes")
                    .filter { $0 == "Yes" }
                    .mapToVoid()
                    .do(onNext: { _ in
                        UserDefaultsHelper.helper.delete(.selectedConferenceId)
                        let tab = UIViewController.topViewController().tabBarController!
                        let selectionVc = UIStoryboard(name: "New",
                                                       bundle: nil).instantiateViewController(withIdentifier: "ConferenceSelectionViewController") as! ConferenceSelectionViewController
                        selectionVc.viewModel = ConferenceSelectionViewModel()
                        UIApplication.shared.delegate!.window!!.rootViewController = selectionVc
                        tab.viewControllers = nil
                    })
                    .subscribeOn(MainScheduler.instance)
                    .asDriverOnErrorJustComplete()
            }
            .asDriver()
        let otherSelection = input.selection
            .do(onNext: { (indexPath) in
                switch indexPath.section {
                case 0:
                    Application.shared.react(row: indexPath.row)
                    break
                case 1:
                    switch indexPath.row {
                    case 0:
                        Application.shared.navigator.toProgramme()
                        break
                    case 1:
                        Application.shared.navigator.toSpeakers()
                        break
                    case 2:
                        Application.shared.navigator.toCongressInfo()
                        break
                    case 3:
                        Application.shared.navigator.toExhibitorsAndSponsors()
                        break
                    case 4:
                        Application.shared.navigator.toIndustrySymposia()
                        break
                    case 5:
                        Application.shared.navigator.toTwitter()
                        break
                    case 6:
                        Application.shared.navigator.toEraEdtaActivites()
                        break
                    default:
                        break
                    }
                    break
                case 3:
                    if indexPath.row == 0 {
                        Application.shared.navigator.toAbout()
                    }
                    break
                default:
                    break
                }
            })
            .mapToVoid()
        let selectedRow = Driver.merge(backToPortal, otherSelection)
        let users = realm.objects(User.self)
        let updateDriver = Observable.collection(from: users)
            .mapToVoid()
            .asDriverOnErrorJustComplete()
        let userData = Driver.merge(input.trigger, logout, updateDriver)
            .map { User.current() }
            .map { user -> (String?, String?) in
                return (user?.fullName, user?.imageUrl)
        }
        let combined = Observable.combineLatest(hcpSubject.asObservable()
            .skip(1)
            .distinctUntilChanged(),
                                                updateDriver.asObservable())
            .skip(1)
            .map { return $0.0 }
            .do(onNext: { (isHcp) in
                UserDefaultsHelper.helper.set(isHcp, forKey: .hcp)
            })
            .filter { _ in User.current() != nil }
            .flatMapLatest { isHcp in
                return provider.request(.updateHcp(hcp: isHcp,
                                                   id: User.current()!.id))
            }
            .mapToVoid()
            .asDriverOnErrorJustComplete()
        let info = input.infoAction
            .flatMapLatest { _ -> Driver<Void> in
                return UIViewController.topViewController().promptFor(title: "",
                                                                      message: "Login info",
                                                                      cancelAction: "OK")
                    .mapToVoid()
                    .asDriverOnErrorJustComplete()
        }
        return Output(sections: Driver.of(sections),
                      selectedRow: selectedRow,
                      loginAction: login,
                      logoutAction: logout,
                      infoAction: info,
                      userData: userData,
                      hcp: combined)//Driver.merge(hcpDriver, loginHpcDriver))
    }
    
}

extension SettingsViewModel {
    
    struct Input {
        let trigger: Driver<Void>
        let selection: Driver<IndexPath>
        let loginAction: Driver<Void>
        let logoutAction: Driver<Void>
        let infoAction: Driver<Void>
    }
    
    struct Output {
        let sections: Driver<[SettingsSectionModel]>
        let selectedRow: Driver<Void>
        let loginAction: Driver<Void>
        let logoutAction: Driver<Void>
        let infoAction: Driver<Void>
        let userData: Driver<(String?, String?)>
        let hcp: Driver<Void>
    }
    
}
