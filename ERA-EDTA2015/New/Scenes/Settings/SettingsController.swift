//
//  SettingsController.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 4/27/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RxSwift
import RxCocoa
import RxDataSources
import RxGesture
import Kingfisher

class SettingsController: UIViewController {
    //rx+vm
    var viewModel: SettingsViewModel!
    fileprivate let disposeBag = DisposeBag()
    //outlets
    @IBOutlet weak var infoButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var userView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var logoutButton: UIButton!
    //data source
    fileprivate let ds = SettingsController.dataSource()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "SETTINGS"
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        }
        userImageView.layer.cornerRadius = userImageView.frame.size.width / 2.0
        userImageView.layer.masksToBounds = true
        //register cells
        tableView.register(UINib(nibName: SwitchCell.reuseId, bundle: nil),
                           forCellReuseIdentifier: SwitchCell.reuseId)
        bindViewModel()
        //hack
        NotificationCenter.default.addObserver(forName: NSNotification.Name("Opt-In-Ntf"),
                                               object: nil,
                                               queue: .main) { (ntf) in
                                                //ask to opt in
                                                let alertView = UIAlertController(title: nil,
                                                                                  message: nil,
                                                                                  preferredStyle: .alert)
                                                alertView.addAction(UIAlertAction(title: "Accept",
                                                                                  style: .cancel,
                                                                                  handler: { _ in
                                                                                    ESyncData.sharedInstance().setUserAction("opt-in")
                                                }))
                                                alertView.addAction(UIAlertAction(title: "Skip",
                                                                                  style: .default))
                                                alertView.setValue(ntf.object as! NSAttributedString, forKey: "attributedMessage")
                                                self.present(alertView,
                                                             animated: true)
        }
    }
    
    func bindViewModel() {
        assert(viewModel != nil)
        tableView.rx.setDelegate(self)
            .disposed(by: disposeBag)
        let trigger = rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
            .mapToVoid()
            .asDriverOnErrorJustComplete()
        let selection = tableView.rx.itemSelected
            .do (onNext: { [weak self] ip in self?.tableView.deselectRow(at: ip, animated: true) })
            .asDriverOnErrorJustComplete()
        let loginAction = userView.rx.tapGesture()
            .mapToVoid()
            .asDriverOnErrorJustComplete()
        let logoutAction = logoutButton.rx
            .tap
            .asDriver()
        let infoAction = infoButton.rx
            .tap
            .asDriver()
        let input = SettingsViewModel.Input(trigger: trigger,
                                            selection: selection,
                                            loginAction: loginAction,
                                            logoutAction: logoutAction,
                                            infoAction: infoAction)
        let output = viewModel.transform(input: input)
        output.sections
            .drive(tableView.rx.items(dataSource: ds))
            .disposed(by: disposeBag)
        output.selectedRow
            .drive()
            .disposed(by: disposeBag)
        output.loginAction
            .drive()
            .disposed(by: disposeBag)
        output.logoutAction
            .drive()
            .disposed(by: disposeBag)
        output.userData
            .drive(rx.userData)
            .disposed(by: disposeBag)
        output.hcp
            .drive()
            .disposed(by: disposeBag)
        output.infoAction
            .drive()
            .disposed(by: disposeBag)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
}

//Header and footers
extension SettingsController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return SectionHeaderView.settingsHeader(ds[section].title)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if ds[section].title.count > 0 {
            return 46.0
        } else {
            return 0.0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if ds[section].title.count > 0 {
            let footerView = UIView(frame: CGRect(x: 0,
                                                  y: 0,
                                                  width: UIScreen.main.bounds.width,
                                                  height: 10.0))
            footerView.backgroundColor = .white
            let lineView = UIView(frame: CGRect(x: 16,
                                                y: 4.5,
                                                width: UIScreen.main.bounds.width - 32,
                                                height: 1.0))
            
            lineView.backgroundColor = UIColor(hex: "c9c9c9", andAlpha: 1.0)
            footerView.addSubview(lineView)
            return footerView
        } else {
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if ds[section].title.count > 0 {
            return 10.0
        } else {
            return 0.0
        }
    }
    
}

extension Reactive where Base: SettingsController {
    
    /// Bindable sink for `progress` property
    fileprivate var userData: Binder<(String?, String?)> {
        return Binder(self.base) { controller, data in
            if let name = data.0 {
                controller.nameLabel.text = name
                controller.userView.backgroundColor = UIColor.init(hex: "f2f2f2",
                                                                   andAlpha: 1.0)
                controller.logoutButton.isHidden = false
            } else {
                controller.nameLabel.text = "Login / Register"
                controller.userView.backgroundColor = .white
                controller.logoutButton.isHidden = true
            }
            if
                let imageUrlString = data.1,
                let imageUrl = URL(string: imageUrlString) {
                controller.userImageView.kf.setImage(with: imageUrl,
                                                     placeholder: #imageLiteral(resourceName: "avatar"))
            } else {
                controller.userImageView.image = #imageLiteral(resourceName: "avatar")
            }
            controller.infoButton.isHidden = !controller.logoutButton.isHidden
        }
    }
    
    
}
