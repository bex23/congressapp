//
//  ArticlesViewModel.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 5/9/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RxSwift
import RxCocoa
import RealmSwift

final class ArticlesViewModel: ViewModelType {
    
    let realm = try! Realm()
    let paginationState = Variable<Pagination>(Pagination(JSON: [:])!)
    
    func transform(input: ArticlesViewModel.Input) -> ArticlesViewModel.Output {
        let errorTracker = ErrorTracker()
        let articles = realm.objects(Article.self).sorted(byKeyPath: "publishedAt", ascending: false)
        let articlesSection = Observable.collection(from: articles)
            .map { [weak self] articles -> [Article] in
                let state = self?.paginationState.value.currentPage ?? 1
                var array = [Article]()
                for article in articles {
                    if array.count < 10 * state {
                        array.append(article)
                    } else {
                        break
                    }
                }
                return array
            }
            .flatMap { articles -> Observable<ArticleSection> in
                let mapped = articles.map { NewsItemViewModel(with: $0.title!,
                                                              and: URL(string: $0.croppedUrl ?? "") ?? URL(string: "https://www.apple.com/")!) }
                let section: ArticleSection = ArticleSection(header: "", items: mapped)
                return Observable.of(section)
            }
        let nearBottomTrigger = input.nearBottom
            .skip(1)
            .throttle(2.0)
            .filter { [weak self] _ in return self?.paginationState.value.hasNext ?? true }
            .debug("FIRING")
            .flatMapLatest { [weak self] _ in
                return provider.request(.articles(page: (self?.paginationState.value.currentPage ?? 1) + 1))
                    .trackError(errorTracker)
                    .mapObject(PaginatedArrayResponse<Article>.self)
                    .do(onNext: { [weak self] (articlesResponse) in
                        self?.paginationState.value = articlesResponse.pagination!
                        guard let instance = self else { return }
                        try! instance.realm.write {
                            instance.realm.add(articlesResponse.array!, update: true)
                        }
                    })
                    .map { $0.array }
                    .asDriverOnErrorJustComplete()
                    .mapToVoid()
            }
        let selected = input.selection
            .map { articles[$0.row] }
            .do(onNext: Application.shared.navigator.toArticle)
            .mapToVoid()
        let errors = errorTracker
            .asDriver()
            .map { $0.localizedDescription }
        
        return Output(sections: articlesSection
            .map { [$0] }
            .asDriverOnErrorJustComplete(),
                      selected: selected,
                      reachedBottom: nearBottomTrigger,
                      error: errors)
    }
    
}

extension ArticlesViewModel {
    
    struct Input {
        let appear: Driver<Void>
        let refresh: Driver<Void>
        let selection: Driver<IndexPath>
        let nearBottom: Driver<Void>
    }
    
    struct Output {
        let sections: Driver<[ArticleSection]>
        let selected: Driver<Void>
        let reachedBottom: Driver<Void>
        let error: Driver<String>
    }
    
}
