//
//  ArticlesController.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 5/9/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RxSwift
import RxCocoa

extension UIScrollView {
    func  isNearBottomEdge(edgeOffset: CGFloat = 20.0) -> Bool {
        return self.contentOffset.y + self.frame.size.height + edgeOffset > self.contentSize.height
    }
}

class ArticlesController: UIViewController {
    
    var viewModel: ArticlesViewModel!
    private let disposeBag = DisposeBag()
    
    @IBOutlet private weak var tableView: UITableView!
    fileprivate let ds = ArticlesController.dataSource()
    
    static let offset: CGFloat = 50.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "NEWS"
        tableView.register(UINib(nibName: ArticleTableViewCell.reuseId, bundle: nil),
                           forCellReuseIdentifier: ArticleTableViewCell.reuseId)
        tableView.refreshControl = UIRefreshControl()
        tableView.rowHeight = ((UIScreen.main.bounds.width - 30.0) / 3.0) / 1.86 + 30.0
        bindViewModel()
    }
    
    func bindViewModel() {
        assert(viewModel != nil)
        let viewWillAppear = rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
            .mapToVoid()
            .asDriverOnErrorJustComplete()
        let pull = tableView.refreshControl!.rx
            .controlEvent(.valueChanged)
            .asDriver()
        let selection = tableView.rx.itemSelected
            .do (onNext: { [weak self] ip in self?.tableView.deselectRow(at: ip, animated: true) })
            .asDriverOnErrorJustComplete()
        let input = ArticlesViewModel.Input(appear: viewWillAppear,
                                            refresh: pull,
                                            selection: selection,
                                            nearBottom: tableView.rx.contentOffset.asDriver()
                                                .filter { [weak self] _ in
                                                    return self?.tableView.isNearBottomEdge(edgeOffset: ArticlesController.offset) ?? false
                                                }.mapToVoid())
        let output = viewModel.transform(input: input)
        output.sections
            .drive(tableView.rx.items(dataSource: ds))
            .disposed(by: disposeBag)
        output.selected
            .drive()
            .disposed(by: disposeBag)
        output.reachedBottom
            .drive()
            .disposed(by: disposeBag)
        output.error
            .drive(rx.error)
            .disposed(by: disposeBag)
    }
    
}
