//
//  ArticlesController+DataSource.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 5/9/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RxSwift
import RxCocoa
import RxDataSources

extension ArticlesController {
    static func dataSource() -> RxTableViewSectionedReloadDataSource<ArticleSection> {
        return RxTableViewSectionedReloadDataSource<ArticleSection>(
            configureCell: { (dataSource, table, idxPath, model) in
                let cell: ArticleTableViewCell = table.dequeueReusableCell(withIdentifier: ArticleTableViewCell.reuseId) as! ArticleTableViewCell
                cell.bind(model)
                return cell
        })
    }
}

struct ArticleSection {
    var header: String
    var items: [NewsItemViewModel]
}

extension ArticleSection: SectionModelType {

    init(original: ArticleSection, items: [NewsItemViewModel]) {
        self = ArticleSection(header: "",
                              items: items)
    }
    
}
