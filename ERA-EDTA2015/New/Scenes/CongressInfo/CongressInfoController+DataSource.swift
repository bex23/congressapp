//
//  CongressInfoController+DataSource.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 4/30/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RxSwift
import RxCocoa
import RxDataSources

extension CongressInfoController {
    static func dataSource() -> RxTableViewSectionedReloadDataSource<CongressInfoSection> {
        return RxTableViewSectionedReloadDataSource<CongressInfoSection>(
            configureCell: { (dataSource, table, idxPath, _) in
                switch dataSource[idxPath] {
                case let .title(item):
                    let cell: CongressInfoTitleCell = table.dequeueReusableCell(withIdentifier: CongressInfoTitleCell.reuseId, for: idxPath) as! CongressInfoTitleCell
                    cell.titleLabel.text = item
                    return cell
                case let .description(item):
                    let cell: CongressInfoDescriptionCell = table.dequeueReusableCell(withIdentifier: CongressInfoDescriptionCell.reuseId, for: idxPath) as! CongressInfoDescriptionCell
                    cell.desrLabel.attributedText =  item.html2AttributedString
                    return cell
                }
        })
    }
}

struct CongressInfoSection {
    var items: [CongressInfoItem]
}

enum CongressInfoItem {
    case title(item: String)
    case description(item: String)
}

extension CongressInfoSection: SectionModelType {
    
    init(original: CongressInfoSection, items: [CongressInfoItem]) {
        self = CongressInfoSection(items: items)
    }

}
