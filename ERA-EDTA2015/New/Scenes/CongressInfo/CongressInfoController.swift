//
//  CongressInfoController.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 4/30/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RxSwift
import RxCocoa
import NSObject_Rx
import RxDataSources

class CongressInfoController: UIViewController {
    
    var viewModel: CongressInfoViewModel!
    private let disposeBag = DisposeBag()
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var scrollViewContainer: UIView!
    var scrollView: UIScrollView!
    var scrollViewContentView = UIView()
    var scrollingForward = Variable<Bool>(false)
    
    let dataSources = [CongressInfoController.dataSource(),
                       CongressInfoController.dataSource()]
    
    lazy var selectedIndex: () -> Int = { [weak self] in
        guard
            let scrollView = self?.scrollView,
            let segmentedControl = self?.segmentedControl,
            let tableViews = self?.tableViews else {
                return 0
        }
        let visibleRect = CGRect(
            x: scrollView.contentOffset.x,
            y: scrollView.contentOffset.y,
            width: scrollView.bounds.width,
            height: scrollView.bounds.height
        )
        for tv in tableViews where visibleRect.intersects(tv.frame) {
            let intersectionFrame = visibleRect.intersection(tv.frame)
            if intersectionFrame.size.width > tv.frame.width / 2.0 {
                return tv.tag
            }
        }
        return segmentedControl.selectedSegmentIndex
    }
    
    lazy var tableViews: [UITableView] = { [weak self] in
        var tvs = [UITableView]()
        for i in 0...1 {
            let tableView = UITableView(frame: CGRect(x: 0, y: 0, width: 0, height: 0) , style: .plain)
            tableView.dataSource = nil
            tableView.delegate = nil
            tableView.tag = i
            tableView.setFrame(forIndex: i)
            tableView.rowHeight = UITableViewAutomaticDimension
            tableView.estimatedRowHeight = UITableViewAutomaticDimension
            tableView.separatorColor = .clear
            tableView.register(UINib(nibName: CongressInfoTitleCell.reuseId, bundle: nil),
                               forCellReuseIdentifier: CongressInfoTitleCell.reuseId)
            tableView.register(UINib(nibName: CongressInfoDescriptionCell.reuseId, bundle: nil),
                               forCellReuseIdentifier: CongressInfoDescriptionCell.reuseId)
            self?.scrollViewContentView.addSubview(tableView)
            tvs.append(tableView)
        }
        return tvs
    }()
    
    private lazy var tvSelection: Driver<(Int, IndexPath)> = {
        var drivers = [Driver<(Int, IndexPath)>]()
        for tv in tableViews {
            let index = Observable.of(tv.tag)
            let item = tv.rx.itemSelected.asObservable()
            let selectionDriver = Observable.combineLatest(index, item)
                .asDriverOnErrorJustComplete()
            drivers.append(selectionDriver)
        }
        let merged = Driver.merge(drivers)
        return merged
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //scroll view setup
        scrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.bounces = false
        scrollViewContentView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        scrollViewContainer.addSubview(scrollView)
        scrollView.addSubview(scrollViewContentView)
        //customize buttons
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
        }
        navigationItem.title = "CONGRESS INFO"
        scrollView.contentSize.width = UIScreen.main.bounds.width * 2.0
        bindViewModel()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        scrollView.frame = scrollViewContainer.bounds
        scrollView.contentSize.height = scrollViewContainer.frame.height
        scrollView.contentSize.width = UIScreen.main.bounds.width * 2.0
        scrollViewContentView.frame = CGRect(x: 0, y: 0, width: scrollView.contentSize.width, height: scrollView.contentSize.height)
        tableViews.enumerated().forEach { (arg) in
            let (index, tv) = arg
            tv.setFrame(forIndex: index)
        }
    }
    
    func bindViewModel() {
        assert(viewModel != nil)
        let input = CongressInfoViewModel.Input(selection: tvSelection)
        //view model bindings
        let output = viewModel.transform(input: input)
        output.sections
            .drive(rx.items)
            .disposed(by: disposeBag)
        output.selected
            .drive()
            .disposed(by: disposeBag)
        //////////////////
        //Bind events
        segmentedControl.rx
            .valueChanged
            .subscribe(onNext: { [weak self] in
                guard
                    let index = self?.segmentedControl.selectedSegmentIndex,
                    let tv = self?.tableViews[index] else {
                        return
                }
                self?.scrollView.scrollRectToVisible(tv.frame, animated: true)
            })
            .disposed(by: rx.disposeBag)
        //scroll view delegation
        scrollView.rx.didEndDragging
            .filter{ !$0 }
            .subscribe(onNext: { event in
                self.centerSubview()
            })
            .disposed(by: rx.disposeBag)
        scrollView.rx.didEndDecelerating
            .subscribe(onNext: {
                self.centerSubview()
            })
            .disposed(by: rx.disposeBag)
        //true == forward
        scrollView.rx.didScroll
            .map { self.scrollView.contentOffset.x }
            .scan([0.0], accumulator: {a,val in
                return [a.last!, val]
            })
            .map { el -> Bool in
                let previous = el[0]
                let current = el[1]
                if current - previous >= 0 {
                    return true
                } else {
                    return false
                }
            }.bind(to: scrollingForward)
            .disposed(by: rx.disposeBag)
        scrollView.rx.didScroll
            .map(selectedIndex)
            .bind(to: segmentedControl.rx.selectedSegmentIndex)
            .disposed(by: rx.disposeBag)
        
        scrollView.rx.willBeginDecelerating
            .subscribe(onNext: {
                self.flickSubview()
            })
            .disposed(by: rx.disposeBag)
        //////////////////
    }
    
}

extension CongressInfoController {
    
    func flickSubview() {
        let visibleRect = CGRect(
            x: scrollView.contentOffset.x,
            y: scrollView.contentOffset.y,
            width: scrollView.bounds.width,
            height: scrollView.bounds.height
        )
        var intersectingTVs = [UITableView]()
        for tv in tableViews where visibleRect.intersects(tv.frame) {
            intersectingTVs.append(tv)
        }
        let tv = scrollingForward.value ? intersectingTVs.last! : intersectingTVs.first!
        scrollView.scrollRectToVisible(tv.frame, animated: true)
    }
    
    func centerSubview() {
        let visibleRect = CGRect(
            x: scrollView.contentOffset.x,
            y: scrollView.contentOffset.y,
            width: scrollView.bounds.width,
            height: scrollView.bounds.height
        )
        for tv in tableViews where visibleRect.intersects(tv.frame) {
            let intersectionFrame = visibleRect.intersection(tv.frame)
            if intersectionFrame.size.width > tv.frame.width / 2.0 {
                scrollView.scrollRectToVisible(tv.frame, animated: true)
            }
        }
    }
    
}

extension Reactive where Base: CongressInfoController {
    
    fileprivate var items: Binder<[Driver<[CongressInfoSection]>]> {
        return Binder(self.base) { controller, items in
            items.enumerated().forEach({ (offset, items) in
                let tv = controller.tableViews[offset]
                let ds = controller.dataSources[offset]
                items
                    .drive(tv.rx.items(dataSource: ds))
                    .disposed(by: controller.rx.disposeBag)
            })
        }
    }
    
}

