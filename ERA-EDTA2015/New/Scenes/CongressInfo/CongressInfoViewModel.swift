//
//  CongressInfoViewModel.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 4/30/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RxSwift
import RxCocoa
import RealmSwift

final class CongressInfoViewModel: ViewModelType {
    
    let realm = try! Realm()
    
    func transform(input: Input) -> Output {
        guard
            let confId = UserDefaultsHelper.helper.fetch(.selectedConferenceId) as? Int,
            let conference = realm
                .objects(Conference.self)
                .filter("id == \(confId)")
                .first,
            let settings = MobileSettings(JSONString: conference.mobileSettings ?? "") else { fatalError() }
        let conferenceInfo = settings.conferenceInfo ?? []
        let titles = conferenceInfo.map { (td) -> String in
            return td.title ?? ""
        }
        let descriptions = conferenceInfo.map { (td) -> String in
            return td.descr ?? ""
        }
        
        var sections = [CongressInfoSection]()
        titles.forEach { (title) in
            sections.append(CongressInfoSection(items: [.title(item: title)]))
        }
        let sectionsObservable = Observable.of(sections)
        
        let updatedSection = input.selection
            .asObservable()
            .filter { (ofset, _) in
                return ofset == 0
            }
            .filter { (_, ip) in
                return ip.row == 0
            }
            .map { (_, ip) in return (sections, ip.section) }
            .map { (arg) -> [CongressInfoSection] in
                var (sects, section) = arg
                var selected = sects[section]
                if selected.items.count == 1 {
                    selected.items.append(.description(item: descriptions[section]))
                } else {
                    selected.items.remove(at: 1)
                }
                sects[section] = selected
                sections = sects
                return sects
        }
        let congressInfoSections = Observable.merge(sectionsObservable, updatedSection).asDriverOnErrorJustComplete()
        let maps: [CongressInfoSection] = [
            CongressInfoSection(items: [.title(item: "Congress Centre Maps".uppercased())]),
            CongressInfoSection(items: [.title(item: "Exhibitors Hall Map".uppercased())])
        ]
        let mapsDriver = Observable.of(maps).asDriverOnErrorJustComplete()
        
        let mapsSelection = input.selection
            .filter { (ofset, _) in
                return ofset == 1
            }
            .map { $0.1.section }
            .do(onNext: { section in
                if section == 0 {
                    Application.shared.navigator.toCongressMaps()
                } else {
                    Application.shared.navigator.toExhibitorMaps()
                }
            })
        return Output(sections: Driver.of([congressInfoSections, mapsDriver]),
                      selected: Driver.merge(input.selection.mapToVoid(),
                                             mapsSelection.mapToVoid()))
    }
    
}

extension CongressInfoViewModel {
    
    struct Input {
        let selection: Driver<(Int, IndexPath)>
    }
    
    struct Output {
        let sections: Driver<[Driver<[CongressInfoSection]>]>
        let selected: Driver<Void>
    }
    
}
