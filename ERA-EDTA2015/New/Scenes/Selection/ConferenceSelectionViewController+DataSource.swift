//
//  ConferenceSelectionViewController+DataSource.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 4/23/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import Foundation

import RxSwift
import RxCocoa
import RxDataSources

extension ConferenceSelectionViewController {
    static func dataSource() -> RxTableViewSectionedReloadDataSource<SelectionSectionModel> {
        return RxTableViewSectionedReloadDataSource<SelectionSectionModel>(
            configureCell: { (dataSource, table, idxPath, _) in
                switch dataSource[idxPath] {
                case let .conferenceItem(vm):
                    let cell: ConferenceCell = table.dequeueReusableCell(withIdentifier: ConferenceCell.reuseId, for: idxPath) as! ConferenceCell
                    cell.bind(vm)
                    return cell
                }
        }, titleForHeaderInSection: { dataSource, index in
            let section = dataSource[index]
            return section.title
        })
    }
}

enum SelectionSectionModel {
    case conferencesSection(title: String, items: [SelectionSectionItem])
}

enum SelectionSectionItem {
    case conferenceItem(item: ConferenceItemViewModel)
}

extension SelectionSectionModel: SectionModelType {
    typealias Item = SelectionSectionItem
    
    var items: [SelectionSectionItem] {
        switch  self {
        case .conferencesSection(_, let its):
            return its
        }
    }
    
    init(original: SelectionSectionModel, items: [Item]) {
        switch original {
        case let .conferencesSection(title: title, items: _):
            self = .conferencesSection(title: title, items: items)
        }
    }
}

extension SelectionSectionModel {
    var title: String {
        switch self {
        case .conferencesSection(title: let title, items: _):
            return title
        }
    }
}
