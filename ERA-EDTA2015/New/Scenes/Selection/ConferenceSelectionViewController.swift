//
//  SelectionViewController.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 3/8/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RxSwift
import RxCocoa

@objc class ConferenceSelectionViewController: UIViewController {
    @objc var viewModel: ConferenceSelectionViewModel!
    fileprivate let disposeBag = DisposeBag()
    @IBOutlet fileprivate weak var tableView: UITableView!
    @IBOutlet fileprivate weak var organizationButton: UIButton!
    @IBOutlet fileprivate weak var portalButton: UIButton!
    fileprivate let ds = ConferenceSelectionViewController.dataSource()
    fileprivate let openSubject = PublishSubject<String>()
    fileprivate let okSubject = PublishSubject<Void>()
    
    override func viewDidLoad() {
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.refreshControl = UIRefreshControl()
        bindViewModel()
    }
    
    func bindViewModel() {
        assert(viewModel != nil)
        tableView.rx.setDelegate(self)
            .disposed(by: disposeBag)
        tableView.emptyDataSetSource = self
        let viewWillAppear = rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
            .mapToVoid()
            .asDriverOnErrorJustComplete()
        let viewWillDisappear = rx.sentMessage(#selector(UIViewController.viewWillDisappear(_:)))
            .mapToVoid()
            .asDriverOnErrorJustComplete()
        let pull = tableView.refreshControl!.rx
            .controlEvent(.valueChanged)
            .asDriver()
        let trigger = Driver.merge(pull,
                                   okSubject.asDriverOnErrorJustComplete())
        //view model bindings
        let input = ConferenceSelectionViewModel.Input(appeared: viewWillAppear,
                                                       disappeared: viewWillDisappear,
                                                       trigger: trigger,
                                                       selection: tableView.rx.itemSelected.asDriver())
        let output = viewModel.transform(input: input)
        output.sections
            .drive(tableView.rx.items(dataSource: ds))
            .disposed(by: disposeBag)
        output.fetching
            .drive(tableView.refreshControl!.rx.isRefreshing)
            .disposed(by: disposeBag)
        output.selectedConference
            .drive()
            .disposed(by: disposeBag)
        output.error
            .drive(rx.error)
            .disposed(by: disposeBag)
        output.terms
            .drive()
            .disposed(by: disposeBag)
        output.disappeared
            .drive()
            .disposed(by: disposeBag)
        //hack for now
        organizationButton.rx.tap
            .map { _ in "web.era-edta.org" }
            .bind(to: openSubject.asObserver())
            .disposed(by: disposeBag)
        portalButton.rx.tap
            .map { _ in "www.enp-era-edta.org" }
            .bind(to: openSubject.asObserver())
            .disposed(by: disposeBag)
        openSubject.asObservable()
            .subscribe(onNext: { (url) in
                let alert = UIAlertController(title: nil,
                                              message: "You are about to leave the ERA-EDTA app and be redirected to \(url.contains("enp") ? "ENP" : "ERA-EDTA") at \(url)",
                    preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK",
                                              style: .default,
                                              handler: { _ in
                                                UIApplication.shared.open(URL(string: "\(url.contains("enp") ? "https" : "https")://\(url)")!,
                                                                          options: [:],
                                                                          completionHandler: nil)
                }))
                alert.addAction(UIAlertAction(title: "Cancel",
                                              style: .cancel,
                                              handler: nil))
                self.show(alert, sender: nil)
            })
            .disposed(by: disposeBag)
    }
    
}

extension ConferenceSelectionViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return SectionHeaderView.selectionHeader(ds[section].title)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50.0
    }
    
}

extension ConferenceSelectionViewController: DZNEmptyDataSetSource {
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = "No conferences fetched."
        let attrs = [NSAttributedStringKey.font: UIFont(name: "Avenir-Heavy", size: 18.0)!]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
}

extension Reactive where Base: ConferenceSelectionViewController {
    
    /// Bindable sink for `progress` property
    fileprivate var error: Binder<String> {
        return Binder(self.base) { controller, message in
            let alert = UIAlertController(title: "Error",
                                          message: message,
                                          preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK",
                                          style: .cancel,
                                          handler: { (action) in
                controller.okSubject.onNext(Void())
            }))
            DispatchQueue.main.async {
                controller.present(alert, animated: true)
            }
        }
    }
    
}
