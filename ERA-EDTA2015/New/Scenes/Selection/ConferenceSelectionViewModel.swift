//
//  ConferenceSelectionViewModel.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 4/20/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RxSwift
import RxCocoa
import RealmSwift
import ObjectMapper

@objc final class ConferenceSelectionViewModel: NSObject, ViewModelType {
    
    let realm = try! Realm()
    let syncErrors = PublishSubject<String>()
    var notificationToken: NotificationToken? = nil

    func transform(input: Input) -> Output {
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
        //fetching of portal
        let cached = Observable.of(realm.objects(PortalSwift.self).first)
                        .filterNil()
                        .map { Array($0.conferences) }
        
        
        let reloadTrigger = Driver.merge(input.appeared.asObservable().take(1).asDriverOnErrorJustComplete(),
                                         input.trigger)
        let portal = reloadTrigger.flatMapLatest { _ in
            return provider.request(.portal)
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .mapObject(PortalResponse.self)
                .do(onNext: { [weak self] (portal) in
                    guard let instance = self else { return }
                    try! instance.realm.write {
                        instance.realm.add(portal.portal, update: true)
                        if let confs = portal.conferences {
                            instance.realm.add(confs, update: true)
                        }
                    }
                })
                .map { $0.portal }
                .asDriverOnErrorJustComplete()
        }
        //mapping conferences
        let fetched = portal.map { portal -> [Conference] in
            return Array(portal!.conferences)
        }
        let conferences = cached
            .concat(fetched.asObservable())
            .asDriverOnErrorJustComplete()
        //creating sections
        let sections = conferences.map { confs -> [SelectionSectionModel] in
            let items = confs
                .map { ConferenceItemViewModel(with: $0) }
                .map { SelectionSectionItem.conferenceItem(item: $0) }
            return [.conferencesSection(title: "MEETING APPS", items: items)]
        }
        let fetching = activityIndicator.asDriver()
        //selection
        let selectedConference = input.selection
            .withLatestFrom(conferences) { (ip, conferences) -> Conference in
                return conferences[ip.row]
            }
            .filter {
                if !$0.hasPresentations { CUtils.showNotAvailableYetMessage() }
                return $0.hasPresentations
            }
            .do(onNext: { [weak self] (conference) in
                guard let instance = self else { return }
                //select conference
                UserDefaultsHelper.helper.set(conference.id, forKey: .selectedConferenceId)
                //reset db instance
                LLDataAccessLayer.sharedInstance().persistentStoreCoordinator = nil
                //download data
                NotificationCenter.default.addObserver(instance,
                                                       selector: #selector(instance.syncFinished),
                                                       name: NSNotification.Name(kSyncFinished),
                                                       object: nil)
                instance.fetchInfo()
            })
        let errors = Driver.merge(syncErrors.asDriverOnErrorJustComplete(), errorTracker.asDriver().map { $0.localizedDescription })
        let terms = portal
            .filterNil()
            .filter { _ in UserDefaultsHelper.helper.fetch(.termsAccepted) == nil }
            .do(onNext: { (portal) in
                let vm = TermsAndConditionsViewModel(text: portal.appTermsAndConditions ?? "")
                let vc = UIStoryboard(name: "New", bundle: nil).instantiateViewController(withIdentifier: "TermsAndConditionsController") as! TermsAndConditionsController
                vc.viewModel = vm
                let navigation = UINavigationController(rootViewController: vc)
                navigation.navigationBar.isOpaque = true
                navigation.navigationBar.isTranslucent = false
                UIViewController.topViewController().present(navigation, animated: true)
            })
            .mapToVoid()
        
        let hcp = input.appeared
            .filter { _ in UserDefaultsHelper.helper.fetch(.termsAccepted) != nil }
            .filter { _ in UserDefaultsHelper.helper.fetch(.hcp) == nil }
            .delay(0.6)
            .flatMapLatest { _ -> Driver<Void> in
                return UIViewController.topViewController().promptFor(title: "",
                                                                      message: "Are you a Health care professional?",
                                                                      cancelAction: "Yes",
                                                                      actions: ["No"]
                                                                      )
                    .do(onNext: { action in
                        UserDefaultsHelper.helper.set(action == "Yes" ? true : false,
                                                      forKey: .hcp)
                    })
                    .mapToVoid()
                    .asDriverOnErrorJustComplete()
            }
            .asDriver()
        let dis = input.disappeared.do(onNext: { [weak self] _ in
            guard let instance = self else { return }
            NotificationCenter.default.removeObserver(instance)
        })
        
        return Output(terms: Driver.merge(terms, hcp),
            sections: sections,
            fetching: fetching,
            error: errors,
            selectedConference: selectedConference, disappeared: dis)
    }
    
    func fetchInfo() {
        UIViewController.topViewController().addLoader()
        ESyncData.sharedInstance().downloadFirstJsonFile(success: { [weak self] (success) in
            guard let instance = self else { return }
            if !ESyncData.sharedInstance().unzip() {
                UIViewController.topViewController().removeLoaderIfExists()
                UserDefaultsHelper.helper.delete(.selectedConferenceId)
                instance.syncErrors.onNext("Data synchronization has failed. In order to set up the application synchronisation of data is needed.\n\nPlease check your internet connection.")
            }
        }, failure: { [weak self] in
            UIViewController.topViewController().removeLoaderIfExists()
            guard let instance = self else { return }
            UserDefaultsHelper.helper.delete(.selectedConferenceId)
            instance.syncErrors.onNext("Data synchronization has failed. In order to set up the application synchronisation of data is needed.\n\nPlease check your internet connection.")
        })
    }
    
    @objc func syncFinished() {
        Application.shared.configureMainInterface(in: UIApplication.shared.delegate!.window!!)
        UIViewController.topViewController().removeLoaderIfExists()
    }
    
}

extension ConferenceSelectionViewModel {
    
    struct Input {
        let appeared: Driver<Void>
        let disappeared: Driver<Void>
        let trigger: Driver<Void>
        let selection: Driver<IndexPath>
    }
    
    struct Output {
        let terms: Driver<Void>
        let sections: Driver<[SelectionSectionModel]>
        let fetching: Driver<Bool>
        let error: Driver<String>
        let selectedConference: Driver<Conference>
        let disappeared: Driver<Void>
    }
    
}
