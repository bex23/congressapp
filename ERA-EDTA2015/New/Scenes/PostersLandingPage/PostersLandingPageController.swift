//
//  PostersLandingPageController.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 5/7/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RxSwift
import RxCocoa

class PostersLandingPageController: UITableViewController {
    
    private typealias Input = PostersLandingPageViewModel.Input
    
    var viewModel: PostersLandingPageViewModel!
    let disposeBag = DisposeBag()
    
    @IBOutlet weak var searchForPostersButton: UIButton!
    @IBOutlet weak var scanToCheckInButton: UIButton!
    @IBOutlet weak var scanForAudioButton: UIButton!
    @IBOutlet weak var scanForTaggingButton: UIButton!
    
    override func viewDidLoad() {
        title = "\(RealmRepository.shared.confName()) \(RealmRepository.shared.confYear()) E-POSTERS"
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
        }
        bindViewModel()
    }
    
    func bindViewModel() {
        assert(viewModel != nil)
        let input = Input(searchForPosters: searchForPostersButton.rx.tap.asDriver(),
                          scanToCheckIn: scanToCheckInButton.rx.tap.asDriver(),
                          scanForAudio: scanForAudioButton.rx.tap.asDriver(),
                          scanForTagging: scanForTaggingButton.rx.tap.asDriver())
        let output = viewModel.transform(input: input)
        output.tapped
            .drive()
            .disposed(by: disposeBag)
        output.scanned
            .drive()
            .disposed(by: disposeBag)
    }
    
}
