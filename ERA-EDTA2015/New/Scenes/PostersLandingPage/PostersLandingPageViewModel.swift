//
//  PostersLandingPageViewModel.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 5/7/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RxSwift
import RxCocoa
import BarcodeScanner
import Moya

final class PostersLandingPageViewModel: ViewModelType {
    
    private enum ScanType {
        case checkIn
        case audio
        case tag
    }
    
    private let code = PublishSubject<String>()
    private let scanType = Variable<ScanType>(.checkIn)
    
    func transform(input: PostersLandingPageViewModel.Input) -> PostersLandingPageViewModel.Output {
        let toSearchForPosters = input.searchForPosters
            .do(onNext: Application.shared.navigator.toSearchPosters)
        let toScanToCheckIn = input.scanToCheckIn
            .filter { _ in
                if User.loggedIn == nil { CUtils.showLoginQuestion() }
                return User.loggedIn != nil
            }
            .do(onNext: { [weak self] _ in
                guard let me = self else { return }
                me.scanType.value = .checkIn
                Application.shared.navigator.toBarcode(with: me)
            })
        let toScanForAudio = input.scanForAudio
            .do(onNext: { [weak self] _ in
                guard let me = self else { return }
                me.scanType.value = .audio
                Application.shared.navigator.toBarcode(with: me)
            })
        let toScanForTagging = input.scanForTagging
            .do(onNext: { [weak self] _ in
                guard let me = self else { return }
                me.scanType.value = .tag
                Application.shared.navigator.toBarcode(with: me)
            })
        //code processing
        let scannedCode = code.asObservable()
        let checkInScan = scannedCode
            .filter { [weak self] _ in self?.scanType.value == .checkIn }
            .flatMap { code -> Observable<Bool> in
                return provider.request(.qrCode(code: code))
                    .filterSuccessfulStatusCodes()
                    .map { _ in return true }
                    .catchServerError { e in
                        return UIViewController.topViewController().promptFor(title: "Error",
                                                                              message: e,
                                                                              cancelAction: "OK")
                            .map { _ in return false }
                    }
            }
            .filter { $0 }
            .flatMapLatest { _ in
                return UIViewController.topViewController().promptFor(title: "Success",
                                                                      message: "You are checked in.",
                                                                      cancelAction: "OK")
            }
        let audioScan = scannedCode
            .filter { [weak self] _ in self?.scanType.value == .audio }
            .map { code -> Bool in
                let predicate = NSPredicate(format: "tlkBoardNo == %@", code)
                let talks = LLDataAccessLayer.sharedInstance().fetchManagedObjects(withName: "Talk",
                                                                                   predicate: predicate,
                                                                                   sortDescriptors: nil,
                                                                                   inMOC: LLDataAccessLayer
                                                                                    .sharedInstance()
                                                                                    .managedObjectContext) as! [Talk]
                
                if talks.count == 1 {
                    Application.shared.navigator.toTalk(talks: talks, index: 0)
                    return true
                } else {
                    return false
                }
            }
            .flatMapLatest { status -> Observable<String> in
                if !status {
                    let title = status ? "Success" : "Error"
                    let message = status
                        ? "Poster session successfully tagged."
                        : "Could not find poster session with this code in our database."
                    return UIViewController.topViewController().promptFor(title: title,
                                                                          message: message,
                                                                          cancelAction: "OK")
                } else {
                    return Observable.just("")
                }

            }
        let tagScan = scannedCode
            .filter { [weak self] _ in self?.scanType.value == .tag }
            .map { code -> Bool in
                let predicate = NSPredicate(format: "tlkBoardNo == %@", code)
                let talks = LLDataAccessLayer.sharedInstance().fetchManagedObjects(withName: "Talk",
                                                                                   predicate: predicate,
                                                                                   sortDescriptors: nil,
                                                                                   inMOC: LLDataAccessLayer
                                                                                    .sharedInstance()
                                                                                    .managedObjectContext) as! [Talk]
                
                if talks.count == 1 {
                    Talk.updateTimeline(talks.first!, isAddition: true)
                    return true
                } else {
                    return false
                }
            }
            .flatMapLatest { status -> Observable<String> in
                let title = status ? "Success" : "Error"
                let message = status
                    ? "Poster session successfully tagged."
                    : "Could not find poster session with this code in our database."
                return UIViewController.topViewController().promptFor(title: title,
                                                                      message: message,
                                                                      cancelAction: "OK")
            }
        let scanned = Observable.merge(checkInScan, audioScan, tagScan)
            .mapToVoid()
            .asDriverOnErrorJustComplete()
        return Output(tapped: Driver.merge(toSearchForPosters,
                                           toScanToCheckIn,
                                           toScanForAudio,
                                           toScanForTagging),
                      scanned: scanned)
    }
    
}

extension PostersLandingPageViewModel {
    
    struct Input {
        let searchForPosters: Driver<Void>
        let scanToCheckIn: Driver<Void>
        let scanForAudio: Driver<Void>
        let scanForTagging: Driver<Void>
    }
    
    struct Output {
        let tapped: Driver<Void>
        let scanned: Driver<Void>
    }
    
}

extension PostersLandingPageViewModel: BarcodeScannerCodeDelegate, BarcodeScannerDismissalDelegate {
    
    func scanner(_ controller: BarcodeScannerViewController, didCaptureCode code: String, type: String) {
        controller.dismiss(animated: true) {
            self.code.onNext(code)
        }
    }
    
    func scannerDidDismiss(_ controller: BarcodeScannerViewController) {
        controller.dismiss(animated: true)
    }
    
}
