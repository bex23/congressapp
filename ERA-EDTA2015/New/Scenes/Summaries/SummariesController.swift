//
//  SummariesController.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 5/22/18.
//  Copyright © 2018 Navus. All rights reserved.
//

class SummariesController: UIViewController {
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var programButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //
        let attributedString = NSMutableAttributedString(string: "Access your comments and tagged slides via your PC at the European Nephrology Portal (ENP) at  www.enp-era-etda.org\n\nClick on ERA-EDTA 2018 ‘E-materials’ and select ‘Summary Builder’. You need an ENP account and log in with a valid badge number.", attributes: [
            .font: UIFont(name: "Avenir-Light", size: 14.0)!,
            .foregroundColor: UIColor(white: 60.0 / 255.0, alpha: 1.0)
            ])
        attributedString.addAttributes([
            .font: UIFont(name: "Avenir-Medium", size: 14.0)!,
            .foregroundColor: UIColor.fadedBlue
            ], range: NSRange(location: 95, length: 20))
        //
        textView.attributedText = attributedString
        programButton.rx.tap
            .do(onNext: { _ in
                Application.shared.navigator.toProgramme()
            })
            .subscribe()
            .disposed(by: rx.disposeBag)
    }
    
}
