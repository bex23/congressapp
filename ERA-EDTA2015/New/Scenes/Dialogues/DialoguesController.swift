//
//  DialoguesController.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 5/22/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RxSwift
import RxCocoa

class DialoguesController: UIViewController {
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var programButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //
        let attributedString = NSMutableAttributedString(string: "An email will inform the faculty members. \n\nYou can also check replies and any ongoing session dialogues via www.enp-era-edta.org. Click on ERA-EDTA 2018 ‘E-materials’ and enter ‘Dialogues’. You need an ENP account and log in with a valid badge number.", attributes: [
            .font: UIFont(name: "Avenir-Light", size: 14.0)!,
            .foregroundColor: UIColor(white: 60.0 / 255.0, alpha: 1.0)
            ])
        attributedString.addAttributes([
            .font: UIFont(name: "Avenir-Medium", size: 14.0)!,
            .foregroundColor: UIColor.fadedBlue
            ], range: NSRange(location: 109, length: 20))
        //
        textView.attributedText = attributedString
        programButton.rx.tap
            .do(onNext: { _ in
                Application.shared.navigator.toProgramme()
            })
            .subscribe()
            .disposed(by: rx.disposeBag)
    }
    
}

extension UIColor {
    @nonobjc class var fadedBlue: UIColor {
        return UIColor(red: 107.0 / 255.0, green: 166.0 / 255.0, blue: 206.0 / 255.0, alpha: 1.0)
    }
}
