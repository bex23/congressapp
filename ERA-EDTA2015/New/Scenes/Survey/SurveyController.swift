//
//  SurveyController.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 5/19/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RxSwift
import RxCocoa

class SurveyController: UIViewController {
    
    var viewModel: SurveyViewModel!
    private let disposeBag = DisposeBag()
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var footerVIew: UIView!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet fileprivate weak var tableView: UITableView!
    fileprivate let ds = SurveyController.dataSource()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = ""
        tableView.tableHeaderView = headerView
        tableView.tableFooterView = footerVIew
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.register(UINib(nibName: RoundButtonCell.reuseId, bundle: nil),
                           forCellReuseIdentifier: RoundButtonCell.reuseId)
        bindViewModel()
    }
    
    func bindViewModel() {
        assert(viewModel != nil)
        //refresh trigger
        let viewWillAppear = rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
            .mapToVoid()
            .asDriverOnErrorJustComplete()
        //view model bindings
        let input = SurveyViewModel.Input(trigger: viewWillAppear,
                                          submit: submitButton.rx
                                            .tap
                                            .asDriver(),
                                          selection: tableView.rx
                                            .itemSelected
                                            .asDriverOnErrorJustComplete())
        let output = viewModel.transform(input: input)
        output.sections
            .drive(tableView.rx.items(dataSource: ds))
            .disposed(by: disposeBag)
        output.sections
            .map { $0.count > 0 ? true : false }
            .drive(rx.showFooter)
            .disposed(by: disposeBag)
        output.error
            .drive(rx.error)
            .disposed(by: disposeBag)
        output.title
            .drive(rx.headerTitle)
            .disposed(by: disposeBag)
        output.selection
            .drive()
            .disposed(by: disposeBag)
        output.submitted
            .drive()
            .disposed(by: disposeBag)
    }
    
}

extension Reactive where Base: SurveyController {
    
    /// Bindable sink for `progress` property
    fileprivate var error: Binder<String> {
        return Binder(self.base) { controller, message in
            let alert = UIAlertController(title: "Error",
                                          message: message,
                                          preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK",
                                          style: .cancel,
                                          handler: nil))
            DispatchQueue.main.async {
                controller.present(alert, animated: true)
            }
        }
    }
    
    fileprivate var headerTitle: Binder<String> {
        return Binder(self.base) { controller, title in
            let titleSize = title.height(withConstrainedWidth: UIScreen.main.bounds.width - 30.0,
                                         font: controller.headerLabel.font)
            controller.headerView.frame.size.height = titleSize + 30.0
            controller.headerLabel.text = title
        }
    }
    
    fileprivate var showFooter: Binder<Bool> {
        return Binder(self.base) { controller, show in
            controller.tableView.tableFooterView = show ? controller.footerVIew : nil
        }
    }
    
}
