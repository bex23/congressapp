//
//  SurveyController+DataSource.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 5/19/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RxSwift
import RxCocoa
import RxDataSources

extension SurveyController {
    static func dataSource() -> RxTableViewSectionedReloadDataSource<SurveySectionModel> {
        return RxTableViewSectionedReloadDataSource<SurveySectionModel>(
            configureCell: { (dataSource, table, idxPath, _) in
                switch dataSource[idxPath] {
                case let .questionItem(title):
                    let cell = table.dequeueReusableCell(withIdentifier: "TextCell")
                    cell?.textLabel?.text = title
                    return cell!
                case let .answerItem(title, isSelected):
                    let cell: RadioButtonCell = table.dequeueReusableCell(withIdentifier: RadioButtonCell.reuseId) as! RadioButtonCell
                    cell.titleLabel.text = title
                    cell.radioButton.isUserInteractionEnabled = false
                    cell.radioButton.isSelected = isSelected
                    return cell
                }
        })
    }
}

enum SurveySectionModel {
    case questionSection(items: [SurveySectionItem])
}

enum SurveySectionItem {
    case questionItem(title: String)
    case answerItem(title: String, isSelected: Bool)
}

extension SurveySectionModel: SectionModelType {
    
    typealias Item = SurveySectionItem
    
    var items: [SurveySectionItem] {
        switch  self {
        case .questionSection(let its):
            return its
        }
    }
    
    init(original: SurveySectionModel, items: [SurveySectionItem]) {
        switch original {
        case .questionSection:
            self = .questionSection(items: items)
        }
    }
}

public extension NSObject{
    public class var nameOfClass: String{
        return NSStringFromClass(self).components(separatedBy: ".").last!
    }
    public var nameOfClass: String {
        return NSStringFromClass(type(of: self)).components(separatedBy: ".").last!
        
    }
}
