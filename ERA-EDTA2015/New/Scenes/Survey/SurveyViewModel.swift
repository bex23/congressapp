//
//  SurveyViewModel.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 5/19/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RxSwift
import RxCocoa

final class SurveyViewModel : ViewModelType {
    
    let campaign: Campaign
    init(campaignId: Campaign) {
        self.campaign = campaignId
    }
    
    private let survey = Variable<SurveyEntry>(SurveyEntry())
    private let selectionSubject = BehaviorSubject<Void>(value: ())
    
    func transform(input: SurveyViewModel.Input) -> SurveyViewModel.Output {
        survey.value.badge = User.current()?.badge
        let errorTracker = ErrorTracker()
        let errors = errorTracker
            .asDriver()
            .throttle(1.0)
            .map { $0.localizedDescription }
        let id = campaign.id
        let questions: Observable<[Question]> = input.trigger
            .asObservable()
            .flatMapLatest { _ in
                return provider.request(.questions(capaignId: id))
                    .trackError(errorTracker)
                    .filterSuccessfulStatusCodes()
                    .mapObject(ObjectResponse<Campaign>.self)
                    .map { $0.object }
                    .filterNil()
                    .map { Array($0.questions) }
                    .do(onNext: { [weak self] (questions) in
                        guard let instance = self else { return }
                        instance.survey.value.answers.removeAll()
                        questions.forEach {
                            let a = Answer()
                            a.question = $0
                            instance.survey.value.answers.append(a)
                        }
                        print(instance.survey.value.answers.count)
                    })
            }
        
        let sections = Observable.combineLatest(questions, selectionSubject.asObservable())
        { questions, _ in return questions }
            .map { [weak self] questions -> [SurveySectionModel] in
                guard let instance = self else { fatalError() }
                let sections = questions.enumerated().map { (index, question) -> SurveySectionModel in
                    var items = [SurveySectionItem]()
                    items.append(SurveySectionItem.questionItem(title: question.title ?? ""))
                    question.options.forEach({ (option) in
                        let content = instance.survey.value.answers[index].content
                        items.append(SurveySectionItem.answerItem(title: option, isSelected: (content ?? "") == option))
                    })
                    return SurveySectionModel.questionSection(items: items)
                }
                return sections
            }
        let selection = input.selection
            .filter { $0.row > 0 }
            .asObservable()
            .withLatestFrom(questions) { (indexPath, qs) -> (String, Int) in
                let question = qs[indexPath.section]
                let option = question.options[indexPath.row - 1]
                return (option, indexPath.section)
            }
            .do(onNext: { [weak self] (option, section) in
                guard let instance = self else { return }
                let answer = instance.survey.value.answers[section]
                answer.content = option
                instance.selectionSubject.onNext(())
            })
            .mapToVoid()
            .asDriverOnErrorJustComplete()
        let submitted = input.submit
            .asObservable()
            .filter(survey.value.validateAll)
            .flatMap { [weak self] _ -> Observable<Bool> in
                guard let instance = self else { fatalError() }
                let request = EmaterialsAPI.answers(enties: [instance.survey.value])
                return provider.request(request)
                    .filterSuccessfulStatusCodes()
                    .map { _ in return true }
                    .catchServerError { e in
                        return UIViewController.topViewController().promptFor(title: "Error",
                                                                              message: e,
                                                                              cancelAction: "OK")
                            .map { _ in return false }
                    }
            }
            .filter { $0 }
            .flatMapLatest { _ -> Driver<Void> in
                return UIViewController.topViewController().promptFor(title: "Success",
                                                                      message: "Your answers have been submitted successfully.",
                                                                      cancelAction: "OK")
                    .do(onNext: { _ in
                        UIViewController.topViewController().navigationController?.popViewController(animated: true)
                    })
                    .mapToVoid()
                    .asDriverOnErrorJustComplete()
            }
            .asDriverOnErrorJustComplete()
        let submittedError = input.submit
            .filter { [weak self] in
                guard let instance = self else { return false }
                return !instance.survey.value.validateAll()
            }
            .flatMapLatest { _ -> Driver<Void> in
                return UIViewController.topViewController().promptFor(title: "Error",
                                                                      message: "Please provide answers to all questions.",
                                                                      cancelAction: "OK")
                    .mapToVoid()
                    .asDriverOnErrorJustComplete()
            }

        
        return SurveyViewModel.Output(title: Driver.of(campaign.name ?? ""),
                                      sections: sections.asDriverOnErrorJustComplete(),
                                      error: errors,
                                      submitted: Driver.merge(submitted, submittedError),
                                      selection: selection)
    }
    
}

extension SurveyViewModel {
    
    struct Input {
        let trigger: Driver<Void>
        let submit: Driver<Void>
        let selection: Driver<IndexPath>
    }
    
    struct Output {
        let title: Driver<String>
        let sections: Driver<[SurveySectionModel]>
        let error: Driver<String>
        let submitted: Driver<Void>
        let selection: Driver<Void>
    }
    
}
