//
//  LearningCentreController.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 4/27/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RxSwift
import RxCocoa
import RxDataSources

class LearningCentreContrller: UIViewController {
    //rx+vm
    var viewModel: LearningCentreViewModel!
    fileprivate let disposeBag = DisposeBag()
    //outlets
    @IBOutlet weak var collectionView: UICollectionView!
    fileprivate let ds = LearningCentreContrller.dataSource()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "LEARNING CENTRE"
        collectionView.register(UINib(nibName: LearningKitItemCell.reuseId, bundle: nil),
                                forCellWithReuseIdentifier: LearningKitItemCell.reuseId)
        bindViewModel()
    }
    
    func bindViewModel() {
        assert(viewModel != nil)
        let input = LearningCentreViewModel.Input(selection: collectionView.rx.itemSelected.asDriver())
        let output = viewModel.transform(input: input)
        output.sections
            .drive(collectionView.rx.items(dataSource: ds))
            .disposed(by: disposeBag)
        output.selectedItem
            .drive()
            .disposed(by: disposeBag)
    }
    
}
