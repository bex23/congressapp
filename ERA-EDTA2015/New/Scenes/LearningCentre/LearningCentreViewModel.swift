//
//  LearningCentreViewModel.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 4/27/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RxSwift
import RxCocoa

final class LearningCentreViewModel: ViewModelType {
    
    func transform(input: LearningCentreViewModel.Input) -> LearningCentreViewModel.Output {
        let items = Application.shared.learningSection
        let sections: [LearningCentreSectionModel] = [.learningKitSection(title: "", items: items)]
        let sectionsDriver = Observable.of(sections).asDriverOnErrorJustComplete()
        let selectedConference = input.selection
            .map { $0.row }
            .map(Application.shared.react)
        
        return Output(sections: sectionsDriver,
                      selectedItem: selectedConference.mapToVoid())
    }
    
}

extension LearningCentreViewModel {
    
    struct Input {
        let selection: Driver<IndexPath>
    }
    
    struct Output {
        let sections: Driver<[LearningCentreSectionModel]>
        let selectedItem: Driver<Void>
    }
    
}
