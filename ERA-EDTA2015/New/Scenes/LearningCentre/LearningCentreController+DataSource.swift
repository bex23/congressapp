//
//  LearningCentreController+DataSource.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 4/27/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RxSwift
import RxCocoa
import RxDataSources

extension LearningCentreContrller {
    static func dataSource() -> RxCollectionViewSectionedReloadDataSource<LearningCentreSectionModel> {
        return RxCollectionViewSectionedReloadDataSource<LearningCentreSectionModel>(
            configureCell: { (_, collectionView, idxPath, viewModel) -> UICollectionViewCell in
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: LearningKitItemCell.reuseId, for: idxPath) as! LearningKitItemCell
                cell.bind(viewModel)
                return cell
        })
    }
    
}

enum LearningCentreSectionModel {
    case learningKitSection(title: String, items: [LearningKitItemViewModel])
}


extension LearningCentreSectionModel: SectionModelType {
    typealias Item = LearningKitItemViewModel
    
    var items: [LearningKitItemViewModel] {
        switch  self {
        case .learningKitSection(_, let its):
            return its
        }
    }
    
    init(original: LearningCentreSectionModel, items: [Item]) {
        switch original {
        case let .learningKitSection(title: title, items: _):
            self = .learningKitSection(title: title, items: items)
        }
    }
}

extension LearningCentreSectionModel {
    var title: String {
        switch self {
        case .learningKitSection(title: let title, items: _):
            return title
        }
    }
}
