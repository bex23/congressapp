//
//  AdditionalActivitesViewModel.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 5/10/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RxSwift
import RxCocoa
import RealmSwift

final class EraEdtaActivitiesViewModel: ViewModelType {
    
    let realm = try! Realm()
    
    func transform(input: Input) -> Output {
        guard
            let confId = UserDefaultsHelper.helper.fetch(.selectedConferenceId) as? Int,
            let conference = realm
                .objects(Conference.self)
                .filter("id == \(confId)")
                .first,
            let settings = MobileSettings(JSONString: conference.mobileSettings ?? ""),
            let conferenceInfo = settings.additionalActivities else {
                return Output(sections: Driver.of([]),
                              selected: input.selection.mapToVoid())
        }
        let titles = conferenceInfo.map { (td) -> String in
            return td.title ?? ""
        }
        let descriptions = conferenceInfo.map { (td) -> String in
            return td.descr ?? ""
        }
        
        var sections = [CongressInfoSection]()
        titles.forEach { (title) in
            sections.append(CongressInfoSection(items: [.title(item: title)]))
        }
        
        let updatedSection = input.selection
            .filter { $0.row == 0 }
            .map { ip in return (sections, ip.section) }
            .map { (arg) -> [CongressInfoSection] in
                var (sects, section) = arg
                var selected = sects[section]
                if selected.items.count == 1 {
                    selected.items.append(.description(item: descriptions[section]))
                } else {
                    selected.items.remove(at: 1)
                }
                sects[section] = selected
                sections = sects
                return sects
        }
        
        
        let sectionsDriver = Driver.of(sections)
        
        return Output(sections: Driver.merge(sectionsDriver, updatedSection),
                      selected: input.selection.mapToVoid())
    }
    
}

extension EraEdtaActivitiesViewModel {
    
    struct Input {
        let selection: Driver<IndexPath>
    }
    
    struct Output {
        let sections: Driver<[CongressInfoSection]>
        let selected: Driver<Void>
    }
    
}

