//
//  AdditionalActivitiesController.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 5/10/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RxSwift
import RxCocoa

class EraEdtaActivitiesController: UIViewController {
    
    @IBOutlet weak var footerView: UIView!
    var viewModel: EraEdtaActivitiesViewModel!
    private let disposeBag = DisposeBag()
    @IBOutlet weak var tableView: UITableView!
    let ds = CongressInfoController.dataSource()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //customize buttons
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
        }
        navigationItem.title = "ERA-EDTA ACTIVITIES"
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = footerView
        bindViewModel()
    }
    
    func bindViewModel() {
        assert(viewModel != nil)
        let input = EraEdtaActivitiesViewModel.Input(selection: tableView.rx.itemSelected.asDriver())
        //view model bindings
        let output = viewModel.transform(input: input)
        output.sections
            .drive(tableView.rx.items(dataSource: ds))
            .disposed(by: disposeBag)
        output.selected
            .drive()
            .disposed(by: disposeBag)
    }
    
}
