//
//  ProgrammeViewModel.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 4/30/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RxSwift
import RxCocoa
import RealmSwift

final class ProgrammeViewModel: ViewModelType {
    
    func transform(input: ProgrammeViewModel.Input) -> ProgrammeViewModel.Output {
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
        
        let scientific = input.scientificProgrammeSelection
            .do(onNext: Application.shared.navigator.toScientificProgramme)
            .mapToVoid()
        let programme = input.programmeAtAGlanceSelection
            .do(onNext: Application.shared.navigator.toProgrammeAtGlance)
            .mapToVoid()
        let posters = input.posterSessionsSelection
            .do(onNext: Application.shared.navigator.toEPosters)
            .mapToVoid()
        let speakers = input.speakersSelection
            .do(onNext: Application.shared.navigator.toSpeakers)
            .mapToVoid()
        let tap = Driver.merge(scientific, programme, posters, speakers)
       
        
        let talks = input.trigger.flatMapLatest { _ in
            return provider.request(.recommendations)
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .mapObject(ArrayResponse<Presentation>.self)
                .map { $0.array }
                .filterNil()
                .map { presentations -> [Int] in
                    let ids = presentations.map { pres -> Int in
                        return pres.id
                    }
                    return ids
                }
                .map { ids -> [Talk] in
                    if ids.count == 0 { return [Talk]() }
                    let predicate = NSPredicate(format: "tlkTalkId IN %@", ids)
                    let talks = LLDataAccessLayer.sharedInstance().fetchManagedObjects(withName: "Talk",
                                                                                       predicate: predicate,
                                                                                       sortDescriptors: nil,
                                                                                       inMOC: LLDataAccessLayer
                                                                                        .sharedInstance()
                                                                                        .managedObjectContext) as! [Talk]
                    return talks
                }
                .asDriverOnErrorJustComplete()
        }
        
        let sections = talks.map { talks -> [RecommendationSection] in
            return [RecommendationSection(header: "RECOMMENDED FOR YOU", items: talks)]
        }
        let selection = Observable.combineLatest(input.selection.asObservable(),
                                                 talks.asObservable().distinctUntilChanged())
            .do(onNext: { (ip, t) in
                Application.shared.navigator.toTalk(talks: t, index: ip.row)
            })
            .filter { $0.1.count > $0.0.row }
            .map { $0.1[$0.0.row] }
            .flatMap { talk in
                return provider.request(.track(action: .click,
                                               id: Int(truncating: talk.tlkTalkId ?? 0),
                                               type: "Presentation",
                                               objectInfo: "recommendation-ios"))

            }
            .mapToVoid()
            .asDriverOnErrorJustComplete()
        
        
        return Output(fetching: activityIndicator.asDriver(),
                      error: errorTracker.asDriver()
                        .map { $0.localizedDescription },
                      sections: sections,
                      selected: selection,
                      tap: tap)
    }
    
    
}

extension ProgrammeViewModel {
    
    struct Input {
        let trigger: Driver<Void>
        let scientificProgrammeSelection: Driver<Void>
        let programmeAtAGlanceSelection: Driver<Void>
        let posterSessionsSelection: Driver<Void>
        let speakersSelection: Driver<Void>
        let selection: Driver<IndexPath>
    }
    
    struct Output {
        let fetching: Driver<Bool>
        let error: Driver<String>
        let sections: Driver<[RecommendationSection]>
        let selected: Driver<Void>
        let tap: Driver<Void>
    }
    
}
