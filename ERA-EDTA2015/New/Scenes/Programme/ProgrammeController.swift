//
//  ProgrammeController.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 4/30/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RxSwift
import RxCocoa
import RxDataSources

class ProgrammeController: UIViewController {
    
    var viewModel: ProgrammeViewModel!
    private let disposeBag = DisposeBag()
    
    @IBOutlet weak var scientificProgrammeButton: UIButton!
    @IBOutlet weak var programmeAtAGlanceButton: UIButton!
    @IBOutlet weak var posterSessionsButton: UIButton!
    @IBOutlet weak var speakersButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var buttons: [UIButton] {
        get {
            return [scientificProgrammeButton,
                    programmeAtAGlanceButton,
                    posterSessionsButton,
                    speakersButton]
        }
    }
    
    @IBOutlet weak var firstLeading: NSLayoutConstraint!
    @IBOutlet weak var secondLeading: NSLayoutConstraint!
    @IBOutlet weak var thirdLeading: NSLayoutConstraint!
    @IBOutlet weak var fourthLeading: NSLayoutConstraint!
    
    var leadings: [NSLayoutConstraint] {
        get {
            return [firstLeading,
                    secondLeading,
                    thirdLeading,
                    fourthLeading]
        }
    }
    
    var leading: CGFloat {
        get {
            return (UIScreen.main.bounds.width - 280.0) / 5.0
        }
    }

    let ds = ProgrammeController.dataSource()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //customize buttons
        buttons.forEach {
            $0.titleLabel?.numberOfLines = 2
            $0.titleLabel?.textAlignment = .center
        }
        leadings.forEach {
            $0.constant = leading
        }
        navigationItem.title = "PROGRAMME"
        tableView.refreshControl = UIRefreshControl()
        tableView.register(UINib(nibName: RecommendationCell.reuseId, bundle: nil),
                           forCellReuseIdentifier: RecommendationCell.reuseId)
        tableView.rowHeight = UITableViewAutomaticDimension
        bindViewModel()
    }
    
    func bindViewModel() {
        assert(viewModel != nil)
        tableView.rx.setDelegate(self)
            .disposed(by: disposeBag)
        tableView.emptyDataSetSource = self
        let viewWillAppear = rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
            .mapToVoid()
            .asDriverOnErrorJustComplete()
        let pull = tableView.refreshControl!.rx
            .controlEvent(.valueChanged)
            .asDriver()
        //view model bindings
        let input = ProgrammeViewModel.Input(trigger: Driver.merge(viewWillAppear, pull),
                                             scientificProgrammeSelection: scientificProgrammeButton.rx.tap.asDriver(),
                                             programmeAtAGlanceSelection: programmeAtAGlanceButton.rx.tap.asDriver(),
                                             posterSessionsSelection: posterSessionsButton.rx.tap.asDriver(),
                                             speakersSelection: speakersButton.rx.tap.asDriver(),
                                             selection: tableView.rx.itemSelected.asDriver())
        let output = viewModel.transform(input: input)
        output.sections
            .drive(tableView.rx.items(dataSource: ds))
            .disposed(by: disposeBag)
        output.fetching
            .drive(tableView.refreshControl!.rx.isRefreshing)
            .disposed(by: disposeBag)
        output.selected
            .drive()
            .disposed(by: disposeBag)
        output.error
            .drive(rx.error)
            .disposed(by: disposeBag)
        output.tap
            .drive()
            .disposed(by: disposeBag)
    }
    
}

extension ProgrammeController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard ds[section].items.count > 0 else { return nil }
        return SectionHeaderView.with(ds[section].header)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard ds[section].items.count > 0 else { return 0 }
        return 46.0
    }
    
}

extension ProgrammeController: DZNEmptyDataSetSource {
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = "No recommendations available at this moment."
        let attrs = [NSAttributedStringKey.font: UIFont(name: "Avenir-Heavy", size: 18.0)!]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
}

//Rx animating height change
extension Reactive where Base: ProgrammeController {
    
    /// Bindable sink for `progress` property
    fileprivate var error: Binder<String> {
        return Binder(self.base) { controller, message in
            let alert = UIAlertController(title: "Error",
                                          message: message,
                                          preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK",
                                          style: .cancel,
                                          handler: nil))
            DispatchQueue.main.async {
                controller.present(alert, animated: true)
            }
        }
    }
    
}

