//
//  ProgrammeController+DataSource.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 4/30/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RxSwift
import RxCocoa
import RxDataSources

extension ProgrammeController {
    static func dataSource() -> RxTableViewSectionedReloadDataSource<RecommendationSection> {
        return RxTableViewSectionedReloadDataSource<RecommendationSection>(
            configureCell: { (dataSource, table, idxPath, model) in
                let cell: RecommendationCell = table.dequeueReusableCell(withIdentifier: RecommendationCell.reuseId) as! RecommendationCell
                cell.setUp(withTalk: model)
                return cell
        }, titleForHeaderInSection: { dataSource, index in
            let section = dataSource[index]
            return section.header
        })
    }
}

struct RecommendationSection {
    var header: String
    var items: [Talk]
}

extension RecommendationSection: SectionModelType {
    
    init(original: RecommendationSection, items: [Talk]) {
        self = RecommendationSection(header: "RECOMMENDED FOR YOU",
                                     items: items)
    }

}

