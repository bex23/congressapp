//
//  ArticleController.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 5/3/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RxSwift
import RxCocoa
import RxGesture
import Kingfisher

class ArticleController: UIViewController {
    
    var viewModel: ArticleViewModel!
    let disposeBag = DisposeBag()
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var articleTextView: UITextView!
    @IBOutlet weak var shareButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        articleTextView.delegate = self
        bindViewModel()
    }
    
    func bindViewModel() {
        assert(viewModel != nil)
        let output = viewModel.transform(input: ArticleViewModel.Input(shown: Driver.of(Void()),
                                                                       share: shareButton.rx
                                                                                    .tap
                                                                                    .asDriver(),
                                                                       imageTap: mainImageView.rx
                                                                                    .tapGesture()
                                                                                    .mapToVoid()
                                                                                    .asDriverOnErrorJustComplete()))
        output.title
            .drive(titleLabel.rx.text)
            .disposed(by: disposeBag)
        output.image
            .do(onNext: { [weak self] resource in
                self?.mainImageView.kf.setImage(with: resource)
            })
            .drive()
            .disposed(by: disposeBag)
        output.content
            .drive(articleTextView.rx.attributedText)
            .disposed(by: disposeBag)
        output.shared
            .drive()
            .disposed(by: disposeBag)
        output.shown
            .drive()
            .disposed(by: disposeBag)
    }
    
}

extension ArticleController: UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldInteractWith textAttachment: NSTextAttachment, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        print(characterRange)
        return false
    }
    
}
