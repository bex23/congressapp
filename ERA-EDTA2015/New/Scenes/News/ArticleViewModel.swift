//
//  ArticleViewModel.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 5/3/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RxSwift
import RxCocoa
import Kingfisher

final class ArticleViewModel: ViewModelType {
    
    let article: Article
    
    init(article: Article) {
        self.article = article
    }
    
    func transform(input: ArticleViewModel.Input) -> ArticleViewModel.Output {
        guard
            let title = article.title,
            let resourceString = article.croppedUrl,
            let resourceUrl = URL(string: resourceString),
            let content = article.formattedContent,
            let articleURL = article.articleUrl else { fatalError("bad article data") }
        let font = NSMutableAttributedString(attributedString: content)
        font.addAttributes([NSAttributedStringKey.font: UIFont(name: "Avenir-Book", size: 18)!],
                           range: NSRange(location: 0, length: content.length))
        let shared = input.share
            .do(onNext: { _ in
                // set up activity view controller
                let activityViewController = UIActivityViewController(activityItems: ["Check out this article:", articleURL],
                                                                      applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = UIViewController.topViewController().view
                // present the view controller
                 UIViewController.topViewController().present(activityViewController,
                                                              animated: true)
            })
        let shown = input.shown
            .flatMap { [weak self] _ in
                return provider.request(.track(action: .show,
                                               id: self?.article.id ?? 0,
                                               type: "Article",
                                               objectInfo: "ios"))
                .asDriverOnErrorJustComplete()
                .mapToVoid()
            }
        return Output(shown: shown,
                      title: Observable
                        .just(title)
                        .asDriverOnErrorJustComplete(),
                      image: Observable
                        .just(resourceUrl)
                        .asDriverOnErrorJustComplete(),
                      content: Observable
                        .just(font)
                        .asDriverOnErrorJustComplete(),
                      shared: shared)
    }
    
}

extension ArticleViewModel {
    
    struct Input {
        let shown: Driver<Void>
        let share: Driver<Void>
        let imageTap: Driver<Void>
    }
    
    struct Output {
        let shown: Driver<Void>
        let title: Driver<String>
        let image: Driver<Resource>
        let content: Driver<NSAttributedString>
        let shared: Driver<Void>
    }
    
}

