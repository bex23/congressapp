//
//  TermsAndConditionsController.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 4/26/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RxSwift
import RxCocoa

class TermsAndConditionsController: UIViewController {
    var viewModel: TermsAndConditionsViewModel!
    fileprivate let disposeBag = DisposeBag()
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var acceptButton: UIBarButtonItem!
    @IBOutlet weak var textView: UITextView!
    
    override func viewDidLoad() {
        bindViewModel()
    }
    
    func bindViewModel() {
        assert(viewModel != nil)
        let input = TermsAndConditionsViewModel.Input(cancel: cancelButton.rx.tap.asDriver(),
                                                      accept: acceptButton.rx.tap.asDriver())
        let output = viewModel.transform(input: input)
        output.text
            .map { $0.html2AttributedString }
            .drive(textView.rx.attributedText)
            .disposed(by: disposeBag)
        output.accepted
            .drive()
            .disposed(by: disposeBag)
        output.dismisssed
            .drive()
            .disposed(by: disposeBag)
    }
    
}

extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}

extension String {
    var html2AttributedString: NSAttributedString? {
        return Data(utf8).html2AttributedString
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}
