//
//  TermsAndConditionsViewModel.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 4/26/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RxSwift
import RxCocoa

final class TermsAndConditionsViewModel: ViewModelType {
    
    private let text: String
    
    init(text: String) {
        self.text = text
    }
    
    func transform(input: TermsAndConditionsViewModel.Input) -> TermsAndConditionsViewModel.Output {
        let textDriver =  Driver.of(text)
        let dismissed = input.cancel
            .asDriver()
            .do(onNext: {
                exit(0)
            })
        let accepted = input.accept
            .asDriver()
            .do(onNext: {
                UserDefaultsHelper.helper.set(true, forKey: .termsAccepted)
                UIViewController.topViewController().dismiss(animated: true)
            })
        return Output(text: textDriver,
                      dismisssed: dismissed,
                      accepted: accepted)
    }
    
}

extension TermsAndConditionsViewModel {
    
    struct Input {
        let cancel: Driver<Void>
        let accept: Driver<Void>
    }
    
    struct Output {
        let text: Driver<String>
        let dismisssed: Driver<Void>
        let accepted: Driver<Void>
    }
    
}
