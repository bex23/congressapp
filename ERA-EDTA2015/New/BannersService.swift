//
//  BannersService.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 5/12/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import Kingfisher
import RxSwift
import RxGesture
import NSObject_Rx

@objc enum BannerPosition: Int {
    case topBanner = 0
    case bottomBanner = 1
    case ematerialBanner = 2
    case popup = 3
}

extension BannerPosition {
    var description: String {
        switch self {
        case .topBanner:
            return "Top Banner"
        case .bottomBanner:
            return "Bottom Banner"
        case .ematerialBanner:
            return "Ematerial Banner"
        case .popup:
            return "Popup Banner"
        }
    }
    var placeholder: UIImage {
        switch self {
        case .topBanner:
            return #imageLiteral(resourceName: "smallBannerPlaceholder")
        case .bottomBanner:
            return #imageLiteral(resourceName: "largeBannerPlaceholder")
        case .ematerialBanner:
            return #imageLiteral(resourceName: "bannerEmaterialsPlaceholder")
        case .popup:
            return #imageLiteral(resourceName: "bannerPopup")
        }
    }
}

@objc class BannersServices: NSObject {
    
    @objc static let shared = BannersServices()
    private let db = DisposeBag()
    private override init() { super.init() }
    
    @objc func add(imageView: UIImageView, at position: BannerPosition, sender: UIViewController) {
        let placeholder = ImageCache.default.retrieveImageInDiskCache(forKey: position.description) != nil
            ? ImageCache.default.retrieveImageInDiskCache(forKey: position.description)
            : position.placeholder
        imageView.image = placeholder
        imageView.isUserInteractionEnabled = true
        
        let disappeared = sender.rx.sentMessage(#selector(UIViewController.viewWillDisappear(_:)))
        //fetch data
        let bannerData = provider.request(.banner(positon: position))
            .takeUntil(disappeared)
            .filterSuccessfulStatusCodes()
            .catchErrorJustComplete()
            .mapJSON()
            .map { ($0 as? [String: Any])?["data"] as? [String: Any] }
        let img = bannerData
            .takeUntil(disappeared)
            .filterNil()
            .map { $0["resource_url"] as? String }
            .filterNil()
            .map { URL(string: $0) }
            .filterNil()
            .do(onNext: { (url) in
                let imageResource = ImageResource(downloadURL: url)
                imageView.kf.setImage(with: imageResource,
                                      placeholder: placeholder,
                                      completionHandler: { (image, _, _, _) in
                    if let image = image {
                        ImageCache.default.store(image,
                                                 forKey: position.description,
                                                 toDisk: true)
                    }
                })
            })
            .mapToVoid()
            .startWith(Void())
        let shownAction = bannerData
            .takeUntil(disappeared)
            .map { $0?["id"] as? Int }
            .filterNil()
            .flatMap { id in
                return provider.request(.track(action: .show,
                                               id: id,
                                               type: "banner",
                                               objectInfo: "ios"))
                    .mapToVoid()
            }
            .startWith(Void())
        let tap = imageView.rx.tapGesture()
            .takeUntil(disappeared)
            .skip(1)
            .withLatestFrom(bannerData)
            .filterNil()
            .filter { $0["id"] != nil && $0["external_url"] != nil && ($0["external_url"] as? String ?? "").count > 0 }
            .map { return ($0["id"] as! Int, $0["external_url"] as! String) }
            .flatMap { args in
                return provider.request(.track(action: .click,
                                               id: args.0,
                                               type: "banner",
                                               objectInfo: "ios"))
                    .map { _ in return args }
            }
            .do(onNext: { CUtils.showLeavingTheAppMessage(withUrl: $0.1) })
            .mapToVoid()
        Observable.combineLatest(img, shownAction, tap)
            .subscribe()
            .disposed(by: sender.rx.disposeBag)
    }
    
}
