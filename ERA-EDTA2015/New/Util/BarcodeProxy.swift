//
//  BarcodeProxy.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 5/10/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import BarcodeScanner

@objc class BarcodeScannerProxy: NSObject {
    
    public typealias CodeCompletion = (_ code: String?) -> Void
    @objc static let shared = BarcodeScannerProxy()
    private override init() {
        super.init()
    }
    
    var completion: CodeCompletion!
    
    @objc func showBarcode(callback: @escaping CodeCompletion) {
        completion = callback
        let scanner = BarcodeScannerViewController()
        scanner.codeDelegate = self
        scanner.dismissalDelegate = self
        UIViewController.topViewController().present(scanner,
                                                     animated: true)
    }
    
}

extension BarcodeScannerProxy: BarcodeScannerCodeDelegate, BarcodeScannerDismissalDelegate {
    
    func scanner(_ controller: BarcodeScannerViewController, didCaptureCode code: String, type: String) {
        controller.dismiss(animated: true) {
            self.completion(code)
        }
    }
    
    func scannerDidDismiss(_ controller: BarcodeScannerViewController) {
        controller.dismiss(animated: true)
    }
    
}
