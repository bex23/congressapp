//
//  CongressAppDateTransformer.swift
//  CongressApp
//
//  Created by Dejan Bekic on 10/3/17.
//  Copyright © 2017 Navus Consulting GmbH. All rights reserved.
//

import ObjectMapper

class CongressAppDateTransformer: TransformType {
    
    typealias Object = Date
    typealias JSON = String
    
    fileprivate var dateFormat: String {
        get {
             return "yyyy-MM-dd HH:mm:ss"
        }
    }
    
    private lazy var dateFormatter: DateFormatter = {
        let tempFormatter = DateFormatter()
        tempFormatter.dateFormat = dateFormat
        tempFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        return tempFormatter
    }()
    
    func transformFromJSON(_ value: Any?) -> Object? {
        guard let value = value as? String
            else { return nil }
        return dateFormatter.date(from: value)
    }
    
    func transformToJSON(_ value: Object?) -> JSON? {
        guard let date = value
            else { return nil }
        return dateFormatter.string(from: date)
    }
    
}

class CongressAppShortDateTransformer: CongressAppDateTransformer {
    
    override fileprivate var dateFormat: String {
        get {
            return "yyyy-MM-dd"
        }
    }
    
}

