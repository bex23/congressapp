//
//  UserDefaultsHelper.swift
//  CongressApp
//
//  Created by Dejan Bekic on 11/17/17.
//  Copyright © 2017 Navus Consulting GmbH. All rights reserved.
//

import Foundation

enum UserDefaultKeys: String {
    case selectedConferenceId
    case termsAccepted = "kTermsAccepted"
    case loggedInUserId
    case downloadedApp = "downloaded-app"
    case hcp
    
    static let allValues = [selectedConferenceId,
                            termsAccepted,
                            loggedInUserId,
                            downloadedApp,
                            hcp]
}

@objc class UserDefaultsHelper: NSObject {
    
    /// Singleton instance
    @objc static let helper = UserDefaultsHelper()
    fileprivate override init() {
        super.init()
    }
    
    fileprivate let ud = UserDefaults.standard
    
    func set(_ object: Any, forKey key: UserDefaultKeys) {
        //Further improvement -> type checking
        ud.set(object, forKey: key.rawValue)
        ud.synchronize()
    }
    
    func fetch(_ key: UserDefaultKeys) -> Any? {
        return ud.object(forKey: key.rawValue)
    }
    
    func delete(_ key: UserDefaultKeys) {
        ud.removeObject(forKey: key.rawValue)
        ud.synchronize()
    }
    
    func deleteAll() {
        for value in UserDefaultKeys.allValues {
            delete(value)
        }
    }
    
}

//Objc exposed methods
extension UserDefaultsHelper {
    
    @objc func setLoggedInData(id: Int) {
        set(id,
            forKey: .loggedInUserId)
    }
    
    @objc func deleteLoggedInData() {
        delete(.loggedInUserId)
    }
    
}
