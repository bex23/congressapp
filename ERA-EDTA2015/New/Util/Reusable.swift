//
//  Reusable.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 4/10/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import UIKit
import RxSwift

protocol Reusable {
    static var reuseId: String {get}
}

extension Reusable {
    static var reuseId: String {
        return String(describing: self)
    }
}

extension UITableViewCell: Reusable {}

extension UICollectionViewCell: Reusable {}

extension UIViewController: Reusable {}

extension UITableView {
    
    func dequeueReusableCell<T>(ofType cellType: T.Type = T.self, at indexPath: IndexPath) -> T where T: UITableViewCell {
        guard let cell = dequeueReusableCell(withIdentifier: cellType.reuseId,
                                             for: indexPath) as? T else {
                                                fatalError()
        }
        return cell
    }
    
}

extension UIStoryboard {
    
    func instantiateViewController<T>(ofType type: T.Type = T.self) -> T where T: UIViewController {
        guard let viewController = instantiateViewController(withIdentifier: type.reuseId) as? T else {
            fatalError()
        }
        return viewController
    }
    
}

///UITableViewCell with Rx DisposeBag.
class ReusableCell: UITableViewCell {
    
    private(set) var disposeBag = DisposeBag()
    
    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }
    
}

///UICollectionViewCell with Rx DisposeBag.
class ReusableCollectionViewCell: UICollectionViewCell {
    
    private(set) var disposeBag = DisposeBag()
    
    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }
    
}

