//
//  BaseObject.swift
//  CongressApp
//
//  Created by Dejan Bekic on 10/3/17.
//  Copyright © 2017 Navus Consulting GmbH. All rights reserved.
//

import ObjectMapper
import RealmSwift
import RxDataSources

class BaseObject: Object, Mappable {
    
    @objc dynamic var id: Int = 0
    @objc dynamic var updatedAt: Date? = nil
    @objc dynamic var deletedAt: Date? = nil
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        let dateTransformer = CongressAppDateTransformer()
        id          <- map[ServerKeys.id.rawValue]
        updatedAt   <- (map[ServerKeys.updatedAt.rawValue], dateTransformer)
        deletedAt   <- (map[ServerKeys.deletedAt.rawValue], dateTransformer)
    }
    
    private enum ServerKeys: String {
        case id
        case updatedAt = "updated_at"
        case deletedAt = "deleted_at"
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}

extension BaseObject: IdentifiableType {
    var identity: Int {
        return deletedAt != nil ? 0 : id
    }
}

