//
//  User.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 4/28/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RealmSwift
import ObjectMapper
import RxSwift

class User: BaseObject {
    
    @objc dynamic var firstName: String? = nil
    @objc dynamic var lastName: String? = nil
    @objc dynamic var email: String? = nil
    @objc dynamic var imageUrl: String? = nil
    @objc dynamic var institution: String? = nil
    @objc dynamic var token: String? = nil
    @objc dynamic var hcp: Bool = false
    @objc dynamic var hasBadge: Bool = false
    @objc dynamic var isMember: Bool = false
    @objc dynamic var userActions: String? = nil
    @objc dynamic var badge: String? = nil
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)

    }

}

extension User {
    
    @objc static func save(from dictionary: [String: Any], optIn: Bool = false) {
        //create
        let realm = try! Realm()
        guard let userId = dictionary["id"] as? Int else { fatalError("Bad user response") }
        guard let conferenceId = UserDefaultsHelper.helper.fetch(.selectedConferenceId) as? Int else { return }
        var user: User
        if let u = realm.objects(User.self).filter("id == \(userId)")
            .first {
            user = User(value: u)
        } else {
            user = User()
            user.id = userId
        }
        //save user id
        UserDefaultsHelper.helper.set(userId, forKey: .loggedInUserId)
        //update all values
        if let email = dictionary["email"] as? String {
            user.email = email
        }
        if let firstName = dictionary["first_name"] as? String {
            user.firstName = firstName
        }
        if let lastName = dictionary["last_name"] as? String {
            user.lastName = lastName
        }
        if let imageUrl = dictionary["image_url"] as? String {
            user.imageUrl = imageUrl
        }
        if let institution = dictionary["institution"] as? String {
            user.institution = institution
        }
        if let token = dictionary["token"] as? String {
            user.token = token
        }
        if let hcp = dictionary["hcp"] as? Bool {
            user.hcp = hcp
        }
        if let epoints = dictionary["e_points"] as? [String: Any] {
            if let points = epoints["\(conferenceId)"] as? Int {
                user.hasBadge = true
                if points < 0 {
                    user.isMember = true
                }
            }
        }
        if let userActionsDict = dictionary["user_actions"] as? [[String: Any]] {
            var dict = [String: Any]()
            dict["user_actions"] = userActionsDict
            user.userActions = dict.json
        }
        if
            let party = dictionary["party"] as? [String: Any],
            let code = party["code"] as? String {
            user.badge = code
        }
        //register action
        if UserDefaultsHelper.helper.fetch(.downloadedApp) == nil {
            ESyncData.sharedInstance().setUserAction("downloaded-app")
            UserDefaultsHelper.helper.set(true, forKey: .downloadedApp)
        }
        
        if optIn { user.checkOptIn() }
        
        //save to realm
        try! realm.write {
            realm.add(user, update: true)
        }
    }
    
    @objc static func current() -> User? {
        guard let userId = UserDefaultsHelper.helper.fetch(.loggedInUserId) as? Int
            else { return nil }
        let realm = try! Realm()
        let user = realm.objects(User.self)
            .filter("id == \(userId)")
            .first
        return user
    }
    
    static var loggedIn: User? {
        get {
            guard let userId = UserDefaultsHelper.helper.fetch(.loggedInUserId) as? Int
                else { return nil }
            let realm = try! Realm()
            let user = realm.objects(User.self)
                .filter("id == \(userId)")
                .first
            return user
        }
    }
    
    @objc func deleteUser() {
        let realm = try! Realm()
        try! realm.write {
            realm.delete(self)
        }
    }
    
    @objc func add(badge: String) {
        let realm = try! Realm()
        try! realm.write {
            self.badge = badge
            realm.add(self, update: true)
        }
    }
    
}

extension User {
    
    open var fullName: String {
        get {
            return "\(firstName ?? "") \(lastName ?? "")"
        }
    }
    
}

extension User {
    
    @objc func checkOptIn() {
        guard
            let realm = try? Realm(),
            let confId = UserDefaultsHelper.helper.fetch(.selectedConferenceId) as? Int,
            let conference = realm
                .objects(Conference.self)
                .filter("id == \(confId)")
                .first,
            let settings = MobileSettings(JSONString: conference.mobileSettings ?? ""),
            let optIn = settings.optIn,
            let userActionsJson = userActions,
            let actions = UserActions(JSONString: userActionsJson)?.actions else { return }
        let optAction = actions.filter { $0.title == "opt-in" }
        if optAction.count == 0 {
            NotificationCenter.default.post(name: NSNotification.Name("Opt-In-Ntf"),
                                            object: optIn.html2AttributedString)
        }
        return
    }
    
    
}

class UserActions: Mappable {
    
    var actions: [NameDescription]?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        actions <- map["user_actions"]
    }
    
}

class NameDescription: Mappable {
    
    var title: String?
    var descr: String?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        title <- map["name"]
        descr <- map["description"]
    }
    
}
