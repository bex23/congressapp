//
//  News.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 4/28/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RealmSwift
import ObjectMapper

class Article: BaseObject {
    
    @objc dynamic var title: String? = nil
    @objc dynamic var content: String? = nil
    @objc dynamic var lead: String? = nil
    @objc dynamic var publishedAt: Date? = nil
    @objc dynamic var order: Int = 0
    @objc dynamic var thumbUrl: String? = nil
    @objc dynamic var croppedUrl: String? = nil
    @objc dynamic var imageUrl: String? = nil
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        let dateTransformer = CongressAppDateTransformer()
        title       <- map[ServerKeys.title.rawValue]
        content     <- map[ServerKeys.content.rawValue]
        lead        <- map[ServerKeys.lead.rawValue]
        publishedAt <- (map[ServerKeys.publishedAt.rawValue], dateTransformer)
        order       <- map[ServerKeys.order.rawValue]
        thumbUrl    <- map[ServerKeys.thumbUrl.rawValue]
        croppedUrl  <- map[ServerKeys.croppedUrl.rawValue]
        imageUrl    <- map[ServerKeys.imageUrl.rawValue]
    }
    
    private enum ServerKeys: String {
        case title
        case content
        case lead
        case publishedAt = "published_at"
        case order
        case thumbUrl = "resource.thumb"
        case croppedUrl = "resource.800x430"
        case imageUrl = "resource.url"
    }
    
}

extension Article {
    
    var formattedContent: NSAttributedString? {
        get {
            guard
                let content = content,
                let html = content.html2AttributedString else { return nil }
            let mutable = NSMutableAttributedString(attributedString: html)
            mutable.enumerateAttribute(NSAttributedStringKey.attachment,
                                    in: NSMakeRange(0, mutable.length), options: .init(rawValue: 0),
                                    using: { (value, range, stop) in
                if let attachement = value as? NSTextAttachment {
                    let image = attachement.image(forBounds: attachement.bounds, textContainer: NSTextContainer(), characterIndex: range.location)!
                    let screenSize: CGRect = UIScreen.main.bounds
                    if image.size.width > screenSize.width-20 {
                        let newImage = image.resize(scale: (screenSize.width-2)/image.size.width)
                        let newAttribut = NSTextAttachment()
                        newAttribut.image = newImage
                        mutable.addAttribute(NSAttributedStringKey.attachment,
                                             value: newAttribut,
                                             range: range)
                    }
                }
            })
            return mutable
        }
    }
    
    var articleUrl: URL? {
        get {
            let realm = try! Realm()
            let portal = realm.objects(PortalSwift.self).first!
            return URL(string: "\(portal.url!)article/\(id)")
        }
    }
    
}

private extension UIImage {
    
    func resize(scale: CGFloat) -> UIImage {
        let newSize = CGSize(width: self.size.width*scale, height: self.size.height*scale)
        let rect = CGRect(origin: CGPoint.zero, size: newSize)
        UIGraphicsBeginImageContext(newSize)
        self.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
}

class Pagination: Mappable {
    
    @objc dynamic var total: Int = 0
    @objc dynamic var count: Int = 0
    @objc dynamic var perPage: Int = 0
    @objc dynamic var currentPage: Int = 1
    @objc dynamic var totalPages: Int = 0
    @objc dynamic var next: String? = nil
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        total <- map["total"]
        count <- map["count"]
        perPage <- map["per_page"]
        currentPage <- map["current_page"]
        totalPages <- map["total_pages"]
        next <- map["links.next"]
    }
    
}

extension Pagination {
    
    var hasNext: Bool {
        get {
            return currentPage != totalPages
        }
    }
}
