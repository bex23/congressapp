//
//  Presentation.swift
//  CongressApp
//
//  Created by Dejan Bekic on 10/6/17.
//  Copyright © 2017 Navus Consulting GmbH. All rights reserved.
//

import RealmSwift
import ObjectMapper

class Presentation: BaseObject {
    
    @objc dynamic var title: String? = nil
    @objc dynamic var section: String? = nil
    @objc dynamic var type: String? = nil
    @objc dynamic var startsAt: Date? = nil
    @objc dynamic var endsAt: Date? = nil
    @objc dynamic var code: String? = nil
    @objc dynamic var desc: String? = nil
    @objc dynamic var speakerName: String? = nil
    @objc dynamic var speakerImage: String? = nil
    @objc dynamic var speakerImageThumb: String? = nil
    @objc dynamic var available: Bool = false
    @objc dynamic var availableExternal: Bool = false
    @objc dynamic var importedId: String? = nil
    @objc dynamic var allowComments: Bool = false
    @objc dynamic var allowFeedback: Bool = false
    @objc dynamic var sponsored: Bool = false
    @objc dynamic var popularity: Int = 0
    @objc dynamic var availableMainMaterial: Bool = false
    @objc dynamic var availableSupportingMaterial: Bool = false
    @objc dynamic var availableWebcastMaterial: Bool = false
    @objc dynamic var availableHtmlMaterial: Bool = false
    @objc dynamic var availableSummaryMaterial: Bool = false
    @objc dynamic var hasMainMaterial: Bool = false
    @objc dynamic var hasSupportingMaterial: Bool = false
    @objc dynamic var hasWebcastMaterial: Bool = false
    @objc dynamic var hasHtmlMaterial: Bool = false
    @objc dynamic var hasSummaryMaterial: Bool = false
    @objc dynamic var consentCompleted: Bool = false
    @objc dynamic var watermark: Bool = false
    @objc dynamic var disclaimer: Bool = false
    @objc dynamic var downloadable: Bool = false
    @objc dynamic var externalUrl: String? = nil
    @objc dynamic var free: Bool = false
    @objc dynamic var externalId: String? = nil
    @objc dynamic var access: Bool = false
    @objc dynamic var hasExcludedSlides: Bool = false
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
//        let dateTransformer = CongressAppDateTransformer()
//        title                       <- map[ServerKeys.title.rawValue]
//        section                     <- map[ServerKeys.section.rawValue]
//        type                        <- map[ServerKeys.type.rawValue]
//        startsAt                    <- (map[ServerKeys.startsAt.rawValue], dateTransformer)
//        endsAt                      <- (map[ServerKeys.endsAt.rawValue], dateTransformer)
//        code                        <- map[ServerKeys.code.rawValue]
//        desc                        <- map[ServerKeys.desc.rawValue]
//        speakerName                 <- map[ServerKeys.speakerName.rawValue]
//        speakerImage                <- map[ServerKeys.speakerImage.rawValue]
//        speakerImageThumb           <- map[ServerKeys.speakerImageThumb.rawValue]
//        available                   <- map[ServerKeys.available.rawValue]
//        availableExternal           <- map[ServerKeys.availableExternal.rawValue]
//        importedId                  <- map[ServerKeys.importedId.rawValue]
//        allowComments               <- map[ServerKeys.allowComments.rawValue]
//        allowFeedback               <- map[ServerKeys.allowFeedback.rawValue]
//        sponsored                   <- map[ServerKeys.sponsored.rawValue]
//        popularity                  <- map[ServerKeys.popularity.rawValue]
//        availableMainMaterial       <- map[ServerKeys.availableMainMaterial.rawValue]
//        availableSupportingMaterial <- map[ServerKeys.availableSupportingMaterial.rawValue]
//        availableWebcastMaterial    <- map[ServerKeys.availableWebcastMaterial.rawValue]
//        availableHtmlMaterial       <- map[ServerKeys.availableHtmlMaterial.rawValue]
//        availableSummaryMaterial    <- map[ServerKeys.availableSummaryMaterial.rawValue]
//        hasMainMaterial             <- map[ServerKeys.hasMainMaterial.rawValue]
//        hasSupportingMaterial       <- map[ServerKeys.hasSupportingMaterial.rawValue]
//        hasWebcastMaterial          <- map[ServerKeys.hasWebcastMaterial.rawValue]
//        hasHtmlMaterial             <- map[ServerKeys.hasHtmlMaterial.rawValue]
//        hasSummaryMaterial          <- map[ServerKeys.hasSummaryMaterial.rawValue]
//        consentCompleted            <- map[ServerKeys.consentCompleted.rawValue]
//        watermark                   <- map[ServerKeys.watermark.rawValue]
//        disclaimer                  <- map[ServerKeys.disclaimer.rawValue]
//        downloadable                <- map[ServerKeys.downloadable.rawValue]
//        externalUrl                 <- map[ServerKeys.externalUrl.rawValue]
//        free                        <- map[ServerKeys.free.rawValue]
//        externalId                  <- map[ServerKeys.externalId.rawValue]
//        access                      <- map[ServerKeys.access.rawValue]
//        hasExcludedSlides           <- map[ServerKeys.hasExcludedSlides.rawValue]
    }
    
    private enum ServerKeys: String {
        case title
        case section
        case type
        case startsAt           = "starts_at"
        case endsAt             = "ends_at"
        case code
        case desc               = "description"
        case speakerName        = "speaker_name"
        case speakerImage       = "speaker_image"
        case speakerImageThumb  = "speaker_image_thumb"
        case available
        case availableExternal  = "available_external"
        case blockId            = "block_id"
        case importedId         = "imported_id"
        case allowComments      = "allow_comments"
        case allowFeedback      = "allow_feedback"
        case sponsored
        case popularity
        case availableMainMaterial = "available_main_material"
        case availableSupportingMaterial = "available_supporting_material"
        case availableWebcastMaterial = "available_webcast_material"
        case availableHtmlMaterial = "available_html_material"
        case availableSummaryMaterial = "available_summary_material"
        case hasMainMaterial = "has_main_material"
        case hasSupportingMaterial = "has_supporting_material"
        case hasWebcastMaterial = "has_webcast_material"
        case hasHtmlMaterial = "has_html_material"
        case hasSummaryMaterial = "has_summary_material"
        case consentCompleted = "consent_completed"
        case watermark
        case disclaimer
        case downloadable
        case externalUrl = "external_url"
        case free
        case externalId = "external_id"
        case access
        case hasExcludedSlides = "has_excluded_slides"
    }
    
}

extension Presentation {
    
    open var timespan: String? {
        get {
            guard
                let start = startsAt,
                let end = endsAt else {
                    return nil
            }
            let tempFormatter = DateFormatter()
            tempFormatter.dateFormat = "hh:MM"
            tempFormatter.timeZone = TimeZone(secondsFromGMT: 0)
            let st = tempFormatter.string(from: start)
            let en = tempFormatter.string(from: end)
            return "\(st) - \(en)"
        }
    }
    
}

