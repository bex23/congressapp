//
//  Portal.swift
//  CongressApp
//
//  Created by Dejan Bekic on 10/3/17.
//  Copyright © 2017 Navus Consulting GmbH. All rights reserved.
//

import ObjectMapper
import RealmSwift

class PortalSwift: BaseObject {
    
    @objc dynamic var name: String? = nil
    @objc dynamic var url: String? = nil
    @objc dynamic var slug: String? = nil
    @objc dynamic var desc: String? = nil
    @objc dynamic var externalUrl: String? = nil
    @objc dynamic var logo: String? = nil
    @objc dynamic var header: String? = nil
    @objc dynamic var footer: String? = nil
    @objc dynamic var primaryColor: String? = nil
    @objc dynamic var appTermsAndConditions: String? = nil
    @objc dynamic var termsAndConditions: String? = nil
    @objc dynamic var gdpr: String? = nil
    @objc dynamic var cssOverride: String? = nil
    @objc dynamic var defaultLanguage: String? = nil
    @objc dynamic var useCode: Bool = false
    @objc dynamic var summaryBuilder: Bool = false
    @objc dynamic var metaTitle: String? = nil
    @objc dynamic var metaKeywords: String? = nil
    @objc dynamic var metaDescription: String? = nil
    @objc dynamic var trackingScript: String? = nil
    @objc dynamic var activeConference: String? = nil
    
    // MARK: Relationships
    
    let conferences = LinkingObjects(fromType: Conference.self, property: "portal")
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        name                    <- map[ServerKeys.name.rawValue]
        url                     <- map[ServerKeys.url.rawValue]
        slug                    <- map[ServerKeys.slug.rawValue]
        desc                    <- map[ServerKeys.desc.rawValue]
        externalUrl             <- map[ServerKeys.externalUrl.rawValue]
        logo                    <- map[ServerKeys.logo.rawValue]
        header                  <- map[ServerKeys.header.rawValue]
        footer                  <- map[ServerKeys.footer.rawValue]
        primaryColor            <- map[ServerKeys.primaryColor.rawValue]
        appTermsAndConditions   <- map[ServerKeys.appTermsAndConditions.rawValue]
        termsAndConditions      <- map[ServerKeys.termsAndConditions.rawValue]
        gdpr                    <- map[ServerKeys.gdpr.rawValue]
        cssOverride             <- map[ServerKeys.cssOverride.rawValue]
        defaultLanguage         <- map[ServerKeys.defaultLanguage.rawValue]
        useCode                 <- map[ServerKeys.useCode.rawValue]
        summaryBuilder          <- map[ServerKeys.summaryBuilder.rawValue]
        metaTitle               <- map[ServerKeys.metaTitle.rawValue]
        metaKeywords            <- map[ServerKeys.metaKeywords.rawValue]
        metaDescription         <- map[ServerKeys.metaDescription.rawValue]
        trackingScript          <- map[ServerKeys.trackingScript.rawValue]
        activeConference        <- map[ServerKeys.activeConference.rawValue]
    }
    
    private enum ServerKeys: String {
        case name
        case url
        case slug
        case desc                   = "description"
        case externalUrl            = "external_url"
        case logo
        case header
        case footer
        case primaryColor           = "primary_color"
        case termsAndConditions     = "terms_and_conditions"
        case appTermsAndConditions  = "app_terms_and_conditions"
        case gdpr
        case cssOverride            = "css_override"
        case defaultLanguage        = "default_language"
        case useCode                = "use_code"
        case summaryBuilder         = "summary_builder"
        case metaTitle              = "meta_title"
        case metaKeywords           = "meta_keywords"
        case metaDescription        = "meta_description"
        case trackingScript         = "tracking_script"
        case activeConference       = "active_conference"
    }
    
}

class PortalResponse: Mappable {
    
    var portal: PortalSwift!
    var conferences: [Conference]?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        portal <- map["data"]
        map.context = ConferenceContext(portal: portal)
        conferences <- map["data.conferences"]
    }
    
}
