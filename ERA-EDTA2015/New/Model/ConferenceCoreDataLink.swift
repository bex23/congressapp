//
//  ConferenceSync.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 4/25/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RealmSwift

class ConferenceCoreDataLink: Object {
    
    @objc dynamic var id: String = UUID().uuidString
    //block location
    @objc dynamic var blockLocation: Bool = false //LinkedSessionVenue
    //presentation
    @objc dynamic var presentationMaterial: Bool = false //LinkedMaterialTalk
    @objc dynamic var presentationNote: Bool = false //LinkedTalkNote
    @objc dynamic var presentationOrder: Bool = false //LinkedTalkOrder
    @objc dynamic var presentationBlock: Bool = false //LinkedTalkSession
    @objc dynamic var presentationSpeaker: Bool = false //LinkedTalkSpeaker
    //exhibitor
    @objc dynamic var blockExhibitorRole: Bool = false //LinkedExhibitorRoleSession
    @objc dynamic var boothExhibitor: Bool = false //LinkedBoothExhibitor
    @objc dynamic var exhibitorExhibitorRole: Bool = false //LinkedExhibitorRoleExhibitor

    override static func primaryKey() -> String? {
        return "id"
    }
    
}
