//
//  ConferenceSettings.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 4/25/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RealmSwift

class ConferenceSettings: Object {
    
    @objc dynamic var id: String = UUID().uuidString
    @objc dynamic var appDataVersion: Int = 0
    @objc dynamic var lastSync: Date? = nil
    @objc dynamic var link: ConferenceCoreDataLink? = ConferenceCoreDataLink()
    @objc dynamic var conference: Conference?
    
    let presentations = LinkingObjects(fromType: PresentationTimeline.self, property: "conferenceSettings")
    let votes = LinkingObjects(fromType: PresentationVote.self, property: "conferenceSettings")
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}
