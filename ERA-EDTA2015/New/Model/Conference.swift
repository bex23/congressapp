//
//  Conference.swift
//  CongressApp
//
//  Created by Dejan Bekic on 10/3/17.
//  Copyright © 2017 Navus Consulting GmbH. All rights reserved.
//

import ObjectMapper
import RealmSwift

class Conference: BaseObject {
    
    @objc dynamic var name: String? = nil
    @objc dynamic var startsAt: Date? = nil
    @objc dynamic var endsAt: Date? = nil
    @objc dynamic var slug: String? = nil
    @objc dynamic var desc: String? = nil
    @objc dynamic var location: String? = nil
    @objc dynamic var allowDownloads: Bool = false
    @objc dynamic var termsAndConditions: String? = nil
    @objc dynamic var sponsored: Bool = false
    @objc dynamic var sponsorBanner: String? = nil
    @objc dynamic var sponsorTitle: String? = nil
    @objc dynamic var sponsorLink: String? = nil
    @objc dynamic var bannerSmall: String? = nil
    @objc dynamic var bannerMedium: String? = nil
    @objc dynamic var bannerLarge: String? = nil
    @objc dynamic var favicon: String? = nil
    @objc dynamic var hasPresentations: Bool = false
    @objc dynamic var icon: String? = nil
    @objc dynamic var website: String? = nil
    @objc dynamic var city: String? = nil
    @objc dynamic var venue: String? = nil
    @objc dynamic var coordinates: String? = nil
    @objc dynamic var contactId: Int = 0
    @objc dynamic var countryId: Int = 0
    @objc dynamic var mobileSettings: String? = nil

    // MARK: Relationships
    @objc dynamic var portal: PortalSwift?

    required convenience init?(map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        let dateTransformer = CongressAppShortDateTransformer()
        name                <- map[ServerKeys.name.rawValue]
        startsAt            <- (map[ServerKeys.startsAt.rawValue], dateTransformer)
        endsAt              <- (map[ServerKeys.endsAt.rawValue], dateTransformer)
        slug                <- map[ServerKeys.slug.rawValue]
        desc                <- map[ServerKeys.desc.rawValue]
        location            <- map[ServerKeys.location.rawValue]
        desc                <- map[ServerKeys.desc.rawValue]
        location            <- map[ServerKeys.location.rawValue]
        allowDownloads      <- map[ServerKeys.allowDownloads.rawValue]
        termsAndConditions  <- map[ServerKeys.termsAndConditions.rawValue]
        sponsored           <- map[ServerKeys.sponsored.rawValue]
        sponsorBanner       <- map[ServerKeys.sponsorBanner.rawValue]
        sponsorTitle        <- map[ServerKeys.sponsorTitle.rawValue]
        sponsorLink         <- map[ServerKeys.sponsorLink.rawValue]
        bannerSmall         <- map[ServerKeys.bannerSmall.rawValue]
        bannerMedium        <- map[ServerKeys.bannerMedium.rawValue]
        bannerLarge         <- map[ServerKeys.bannerLarge.rawValue]
        favicon             <- map[ServerKeys.favicon.rawValue]
        hasPresentations    <- map[ServerKeys.hasPresentations.rawValue]
        icon                <- map[ServerKeys.icon.rawValue]
        website             <- map[ServerKeys.website.rawValue]
        city                <- map[ServerKeys.city.rawValue]
        venue               <- map[ServerKeys.venue.rawValue]
        coordinates         <- map[ServerKeys.coordinates.rawValue]
        contactId           <- map[ServerKeys.contactId.rawValue]
        countryId           <- map[ServerKeys.counrtyId.rawValue]
        
        if let mobileSettingsDict = map.JSON[ServerKeys.mobileSettings.rawValue] as? [String: Any] {
            mobileSettings = mobileSettingsDict.json
        }

        if let context = map.context as? ConferenceContext {
            portal = context.portal
        }
    }
    
    private enum ServerKeys: String {
        case name
        case startsAt           = "starts_at"
        case endsAt             = "ends_at"
        case slug
        case desc               = "description"
        case location
        case allowDownloads     = "allow_downloads"
        case termsAndConditions = "terms_and_conditions"
        case sponsored
        case sponsorBanner      = "sponsor_banner"
        case sponsorTitle       = "sponsor_title"
        case sponsorLink        = "sponsor_link"
        case bannerSmall        = "banner_small"
        case bannerMedium       = "banner_medium"
        case bannerLarge        = "banner_large"
        case favicon
        case hasPresentations   = "has_presentations"
        case icon
        case website
        case city
        case venue
        case coordinates
        case contactInfo        = "contact_info"
        case contactId          = "contact_id"
        case counrtyId          = "country_id"
        case mobileSettings     = "mobile_settings"
    }
    
}

extension Conference {
    
    func getActiveDays() -> [Date] {
        let calendar = Calendar.current
        var date = Date(timeInterval: 0.0, since: startsAt!)
        let endDate = Date(timeInterval: 0.0, since: endsAt!)
        var dates = [Date]()
        
        while date <= endDate {
            dates.append(date)
            date = calendar.date(byAdding: .day, value: 1, to: date)!
        }
        
        return dates
    }
    
}

class MobileSettings: Mappable {
    
    var optIn: String?
    var conferenceInfo: [TitleDescription]?
    var additionalActivities: [TitleDescription]?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        optIn                   <- map["opt_in"]
        conferenceInfo          <- map["conference_info"]
        additionalActivities    <- map["additional_activities"]
    }
    
}

class TitleDescription: Mappable {
    
    var title: String?
    var descr: String?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        title <- map["title"]
        descr <- map["description"]
    }

}
