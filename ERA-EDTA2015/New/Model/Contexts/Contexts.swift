//
//  Contexts.swift
//  CongressApp
//
//  Created by Dejan Bekic on 10/17/17.
//  Copyright © 2017 Navus Consulting GmbH. All rights reserved.
//

import ObjectMapper

/// Used for mapping relationships
struct ConferenceContext: MapContext {
    var portal: PortalSwift?
}
/*
struct BlockCategoryContext: MapContext {
    var conference: Conference?
}

struct BlockContext: MapContext {
    var blockCategories: [BlockCategory]?
    var locations: [Location]?
}

struct PresentationContext: MapContext {
    var blocks: [Block]?
}

struct Connector<T: Pivot, U: BaseObject> {
    var connectionArray: [T]?
    var resourceArray: [U]?
    
    func getResouce(forId id: Int) -> [U]? {
        guard
            let connectionArray = connectionArray,
            let resourceArray = resourceArray
            else { return nil }
        let matchingConnectionsIds = connectionArray.filter{$0.partyId == id}.map{$0.resourceId!}
        let matchingPresentations = resourceArray.filter{matchingConnectionsIds.contains($0.id)}
        return matchingPresentations.count > 0 ? matchingPresentations : nil
    }
}

struct SpeakerContext: MapContext {
    var speakerConferenceConnector: Connector<ConferenceSpeaker, Presentation>
}

struct ExhibitorContext: MapContext {
    var locations: [Location]?
    var conferenceConnector: Connector<Pivot, Conference>
    var blockConnector: Connector<Pivot, Block>
    var presentationConnector: Connector<Pivot, Presentation>

}
*/
