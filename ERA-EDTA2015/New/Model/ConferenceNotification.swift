//
//  Notification.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 7/5/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RealmSwift
import ObjectMapper

class ConferenceNotification: Object, Mappable {
    
    @objc dynamic var id: String = ""
    @objc dynamic var message: String?
    @objc dynamic var createdAt: Date? = nil
    @objc dynamic var conferenceId: Int = 0
    @objc dynamic var read: Bool = false
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["message_id"]
        message <- map["message"]
        createdAt <- (map["created_at"], CongressAppDateTransformer())
        conferenceId <- map["conference_id"]
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}

extension ConferenceNotification {
    
    @objc class func insert(_ json: [String: Any]) {
        DispatchQueue.main.async {
            UIApplication.shared.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber + 1
            let ntf = ConferenceNotification(JSON: json)
            let realm = try! Realm()
            if let ntf = ntf {
                try! realm.write {
                    realm.add(ntf, update: true)
                }
            }
        }
    }
    
    @objc class func read(_ id: String) {
        DispatchQueue.main.async {
            let number = UIApplication.shared.applicationIconBadgeNumber - 1
            UIApplication.shared.applicationIconBadgeNumber = number >= 0 ? number : 0
            let realm = try! Realm()
            let ntf = realm.object(ofType: ConferenceNotification.self, forPrimaryKey: id)
            if let ntf = ntf {
                try! realm.write {
                    ntf.read = true
                    realm.add(ntf, update: true)
                }
            }
        }
        
    }
    
}

class ArrayResponse<T: Mappable>: Mappable {
    var array: [T]?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        array <- map["data"]
    }
}

class PaginatedArrayResponse<T: Mappable>: ArrayResponse<T> {
    var pagination: Pagination?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        pagination <- map["meta.pagination"]
    }
    
}

class ObjectResponse<T: Mappable>: Mappable {
    var object: T?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        object <- map["data"]
    }
}

