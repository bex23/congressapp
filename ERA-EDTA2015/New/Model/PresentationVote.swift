//
//  PresentationVote.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 4/25/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RealmSwift

class PresentationVote: Object {
    
    @objc dynamic var presentationId: Int = 0
    @objc dynamic var voted: Bool = false
    @objc dynamic var conferenceSettings: ConferenceSettings? = nil
    
    override static func primaryKey() -> String? {
        return "presentationId"
    }
    
    @objc func dict() -> [String: Any] {
        return ["presentation_id": presentationId,
                "tlkVoted": voted ? 1 : 0]
    }
    
}
