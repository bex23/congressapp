//
//  Answer.swift
//  LeadLink
//
//  Created by Dejan Bekic on 2/18/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RealmSwift
import ObjectMapper

class Answer: Object, Mappable {

    @objc dynamic var id: String? = UUID().uuidString
    @objc dynamic var content: String? {
        didSet {
            if content != oldValue {
                syncDate = Date()
            }
        }
    }
    @objc dynamic var syncDate: Date?
    @objc dynamic var question: Question?
    @objc dynamic var entry: SurveyEntry?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    required convenience init(content: String? = nil, question: Question, entry: SurveyEntry) {
        self.init()
        self.content = content
        self.syncDate = Date()
        self.question = question
        self.entry = entry
    }
    
    func mapping(map: Map) { }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}

extension Answer {
    
    var height: CGFloat {
        if question?.type == "string" || question?.type == "dropdown" {
            return 60.0
        } else if question?.type == "radio" {
            return 60.0
        } else if question?.type == "textarea" {
            return question?.options.count ?? 0 > 0 ? 268 : 110
        } else {
            return 32.0
        }
    }
    
}
