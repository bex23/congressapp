//
//  Question.swift
//  LeadLink
//
//  Created by Dejan Bekic on 12/25/17.
//  Copyright © 2017 Navus. All rights reserved.
//

import RealmSwift
import ObjectMapper

class Question: BaseObject {
    
    @objc dynamic var title: String? = nil
    @objc dynamic var type: String? = nil
    @objc dynamic var group: String? = nil
    @objc dynamic var min: Int = 0
    @objc dynamic var max: Int = 0
    @objc dynamic var order: Int = 0
    @objc dynamic var required: Bool = false
    @objc dynamic var desc: String? = nil
    @objc dynamic var elementId: String? = nil
    @objc dynamic var campaign: Campaign?
    
    let options = List<String>()
    
    let answers = LinkingObjects(fromType: Answer.self, property: "question")
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        title       <- map[ServerKeys.title.rawValue]
        type        <- map[ServerKeys.type.rawValue]
        group       <- map[ServerKeys.group.rawValue]
        min         <- map[ServerKeys.min.rawValue]
        max         <- map[ServerKeys.max.rawValue]
        required    <- map[ServerKeys.required.rawValue]
        desc        <- map[ServerKeys.desc.rawValue]
        elementId   <- map[ServerKeys.elementId.rawValue]
        order       <- map[ServerKeys.order.rawValue]
        if let optionsString = map.JSON[ServerKeys.options.rawValue] as? String {
            let optionsArray = optionsString.split(separator: "|")
            optionsArray.forEach {
                options.append(String($0).trimmingCharacters(in: .whitespacesAndNewlines))
            }
        }
        if let context = map.context as? CampaignContext {
            campaign = context.campaign
        }
    }
    
    private enum ServerKeys: String {
        case title
        case type
        case group
        case min
        case max
        case order
        case options
        case required
        case desc      = "description"
        case elementId = "element_id"
    }
    
}
