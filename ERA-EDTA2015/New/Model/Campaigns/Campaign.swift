//
//  Campaign.swift
//  LeadLink
//
//  Created by Dejan Bekic on 12/25/17.
//  Copyright © 2017 Navus. All rights reserved.
//

import RealmSwift
import ObjectMapper

class Campaign: BaseObject {
    
    @objc dynamic var name: String? = nil
    @objc dynamic var desc: String? = nil
    @objc dynamic var type: String? = nil
    @objc dynamic var userId: Int = 0
    @objc dynamic var createdAt: Date? = nil
    @objc dynamic var primaryColor: String? = nil
    @objc dynamic var color: String? = nil
    @objc dynamic var logo: String? = nil
    @objc dynamic var conferenceId: Int = 0
    @objc dynamic var scan: Bool = true
    @objc dynamic var about: String? = nil
    
    let questions = List<Question>()
    let entries = LinkingObjects(fromType: SurveyEntry.self, property: "campaign")
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        name            <- map[ServerKeys.name.rawValue]
        desc            <- map[ServerKeys.desc.rawValue]
        userId          <- map[ServerKeys.userId.rawValue]
        createdAt       <- (map[ServerKeys.createdAt.rawValue], DateTransformer())
        primaryColor    <- map[ServerKeys.primaryColor.rawValue]
        color           <- map[ServerKeys.color.rawValue]
        logo            <- map[ServerKeys.logo.rawValue]
        conferenceId    <- map[ServerKeys.conferenceId.rawValue]
        scan            <- map[ServerKeys.scan.rawValue]
        about           <- map[ServerKeys.about.rawValue]
        
        if let questionsRaw = map.JSON[ServerKeys.questions.rawValue] as? Array<[String: Any]> {
            let context = CampaignContext(campaign: self)
            questionsRaw.forEach { questions.append(Question(JSON: $0, context: context)!) }
        }

    }
    
    private enum ServerKeys: String {
        case name
        case desc           = "description"
        case userId         = "user_id"
        case createdAt      = "created_at"
        case primaryColor   = "primary_color"
        case color
        case logo
        case conferenceId   = "conference_id"
        case questions
        case scan           = "settings.scan"
        case about          = "settings.about"
    }
    
}

struct CampaignContext: MapContext {
    var campaign: Campaign?
}
