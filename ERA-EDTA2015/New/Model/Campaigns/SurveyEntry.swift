//
//  SurveyEntry.swift
//  LeadLink
//
//  Created by Dejan Bekic on 2/18/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RealmSwift
import ObjectMapper

class SurveyEntry: Object, Mappable {
    
    @objc dynamic var id: String? = UUID().uuidString
    @objc dynamic var badge: String! = ""
    @objc dynamic var synced: Bool = false
    @objc dynamic var lastUpdate: Date? = nil
    @objc dynamic var campaign: Campaign?
    let answers = List<Answer>()
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    required convenience init(badge: String, campaign: Campaign) {
        self.init()
        self.badge = badge
        self.campaign = campaign
    }
    
    func mapping(map: Map) {}
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}

extension SurveyEntry {
    
    func validate() -> (Bool, String?) {
        var validation = true
        var error = ""
        
        let mandatoryAnswers = answers.toArray().filter { ($0.question?.required)! }
        mandatoryAnswers.forEach { answer in
            if answer.content == nil || answer.content?.count ?? 0 == 0 {
                validation = false
                var title: String
                if (answer.question?.title ?? "").count == 0 {
                    title = answer.question?.group ?? ""
                } else {
                    title = answer.question?.title ?? ""
                }
                error.append("\(title)\n")
            }
        }
        
        return (validation, error)
    }
    
    func validateAll() -> Bool {
        for a in answers {
            if a.content == nil {
                return false
            }
        }
        return true
    }
    
    func formatAnswersForSending() -> [[String: Any]] {
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        var a = [[String: Any]]()
        answers.toArray().forEach { answer in
            let dict: [String: Any] = [
                "question_id": answer.question?.id ?? 0,
                "content": answer.content ?? "",
                "badge": badge,
                "synced_at": df.string(from: answer.syncDate ?? Date())
            ]
            a.append(dict)
        }
        return a
    }
    
}
