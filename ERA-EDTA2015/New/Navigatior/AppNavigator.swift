//
//  AppNavigator.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 5/1/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RealmSwift
import BarcodeScanner

@objc class AppNavigator: NSObject {
    
    @objc let conference: Conference!
    
    init(with conference: Conference) {
        self.conference = conference
        super.init()
    }
    
    // MARK: Push
    func toTimeline() {
        push(from: "Timeline")
    }
    func toMyNotes() {
        push(from: "Notes")
    }
    func toSpeakers() {
        push(from: "Speakers")
    }
    func toEPosters() {
        let storyboard = UIStoryboard(name: "New",
                                      bundle: nil)
        let postersLP = storyboard.instantiateViewController(withIdentifier: PostersLandingPageController.reuseId) as! PostersLandingPageController
        postersLP.viewModel = PostersLandingPageViewModel()
        UIViewController.topViewController().navigationController?.pushViewController(postersLP,
                                                                                      animated: true)
    }
    func toSearchPosters() {
        push(from: "PosterSessionsViewController")
    }
    func toExhibitorsAndSponsors() {
        push(from: "SponsorsAndExhibitors")
    }
    func toIndustrySymposia() {
        push(from: "IndustrySymposia")
    }
    func toTwitter() {
        let alert = UIAlertController(title: "Info",
                                      message: "You are now leaving the official \(conference.name ?? "") part of the congress app.\n\n\(conference.name ?? "") is not responsible for any content outside of the app.",
                                      preferredStyle: .alert)
        let close = UIAlertAction(title: "Close",
                                  style: .cancel)
        let con = UIAlertAction(title: "Continue",
                                style: .default) { (action) in
                                    self.push(from: "Twitter")
        }
        alert.addAction(close)
        alert.addAction(con)
        UIViewController.topViewController().present(alert,
                                                     animated: true)        
    }
    @objc func toNotifications() {
        let storyboard = UIStoryboard(name: "New",
                                      bundle: nil)
        let n = storyboard.instantiateViewController(withIdentifier: NotificationCenterController.reuseId) as! NotificationCenterController
        n.viewModel = NotificationCenterViewModel()
        UIViewController.topViewController().navigationController?.pushViewController(n,
                                                                                      animated: true)
    }
    // MARK: Login protected
    func toVoting() {
        push(from: "Voting")
    }
    func toEmaterials() {
        guard let _ = User.current() else {
            CUtils.showLoginQuestion()
            return
        }
        push(from: "Ematerials")
    }
    
    //MARK: About
    func toAbout() {
        let vc = UIStoryboard.init(name: "New", bundle: nil).instantiateViewController(withIdentifier: AboutViewController.reuseId)
        UIViewController.topViewController().navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: Comming soon
    func toLearningQuestions() {
        guard let _ = User.current()
            else {
                CUtils.showLoginQuestion()
                return
        }
        guard
            let user = User.current(),
            let _ = user.badge
            else {
                CUtils.showRequiredBadgeMessage()
                return
        }
        let storyboard = UIStoryboard(name: "New",
                                      bundle: nil)
        let lqc = storyboard.instantiateViewController(withIdentifier: LearningQuestionsController.reuseId) as! LearningQuestionsController
        lqc.viewModel = LearningQuestionsViewModel(badge: User.current()?.badge ?? "")
        UIViewController.topViewController().navigationController?.pushViewController(lqc,
                                                                                      animated: true)
    }
    func toSurvey(campaign: Campaign) {
        let storyboard = UIStoryboard(name: "New",
                                      bundle: nil)
        let sc = storyboard.instantiateViewController(withIdentifier: SurveyController.reuseId) as! SurveyController
        sc.viewModel = SurveyViewModel(campaignId: campaign)
        UIViewController.topViewController().navigationController?.pushViewController(sc,
                                                                                      animated: true)
    }
    func toDialogues() {
        let storyboard = UIStoryboard(name: "New",
                                      bundle: nil)
        let d = storyboard.instantiateViewController(withIdentifier: DialoguesController.reuseId)
        UIViewController.topViewController().navigationController?.pushViewController(d,
                                                                                      animated: true)
    }
    func toBuildASummary() {
        let storyboard = UIStoryboard(name: "New",
                                      bundle: nil)
        let s = storyboard.instantiateViewController(withIdentifier: SummariesController.reuseId)
        UIViewController.topViewController().navigationController?.pushViewController(s,
                                                                                      animated: true)
    }
    func toEraEdtaActivites() {
        let storyboard = UIStoryboard(name: "New",
                                      bundle: nil)
        let additional = storyboard.instantiateViewController(withIdentifier: EraEdtaActivitiesController.reuseId) as! EraEdtaActivitiesController
        additional.viewModel = EraEdtaActivitiesViewModel()
        UIViewController.topViewController().navigationController?.pushViewController(additional,
                                                                                      animated: true)
    }
    
    // MARK: Tabs
    func toProgramme() {
        let storyboard = UIStoryboard(name: "New",
                                      bundle: nil)
        let programmeController = storyboard.instantiateViewController(withIdentifier: ProgrammeController.reuseId) as! ProgrammeController
        programmeController.viewModel = ProgrammeViewModel()
        UIViewController.topViewController().navigationController?.pushViewController(programmeController,
                                                                                      animated: true)
    }
    func toCongressInfo(){
        let storyboard = UIStoryboard(name: "New",
                                      bundle: nil)
        let congressInfo = storyboard.instantiateViewController(withIdentifier: CongressInfoController.reuseId) as! CongressInfoController
        congressInfo.viewModel = CongressInfoViewModel()
        UIViewController.topViewController().navigationController?.pushViewController(congressInfo,
                                                                                      animated: true)
    }
    
    // MARK: Programme
    func toScientificProgramme() {
        push(from: "ScientificProgramme")
    }
    func toProgrammeAtGlance() {
        push(from: "ProgrammeAtAGlance")
    }
    
    func toAllArticles() {
        let storyboard = UIStoryboard(name: "New",
                                      bundle: nil)
        let articlesController = storyboard.instantiateViewController(withIdentifier: ArticlesController.reuseId) as! ArticlesController
        articlesController.viewModel = ArticlesViewModel()
        UIViewController.topViewController().navigationController?.pushViewController(articlesController,
                                                                                      animated: true)
    }
    func toArticle(atricle: Article) {
        let storyboard = UIStoryboard(name: "New",
                                      bundle: nil)
        let vm = ArticleViewModel(article: atricle)
        let articleController = storyboard.instantiateViewController(withIdentifier: ArticleController.reuseId) as! ArticleController
        articleController.viewModel = vm
        UIViewController.topViewController().navigationController?.pushViewController(articleController,
                                                                                      animated: true)
    }
    func toTalk(talks: [Talk], index: Int){
        let vc = UIStoryboard(name: "PresentationDetailsViewController", bundle: nil).instantiateInitialViewController() as! PresentationDetailsViewController
        vc.currentTalkIndex = index
        vc.arrayTalks = NSMutableArray(array: talks)
        UIViewController.topViewController().navigationController?.pushViewController(vc,
                                                                                      animated: true)
    }
    func toBarcode<T>(with delegate: T) where T: BarcodeScannerCodeDelegate,
        T: BarcodeScannerDismissalDelegate {
        let scanner = BarcodeScannerViewController()
        scanner.codeDelegate = delegate
        scanner.dismissalDelegate = delegate
        UIViewController.topViewController().present(scanner,
                                                     animated: true)
    }
    func toCongressMaps() {
        push(from: "CongressCenterMap")
    }
    func toExhibitorMaps() {
        push(from: "ExhibitorMapViewController")
    }
    // MARK: Presentation
    private func push(from storyboard: String) {
        let vc = UIStoryboard(name: storyboard,
                              bundle: nil).instantiateInitialViewController()
        UIViewController.topViewController()
            .navigationController?
            .pushViewController(vc!, animated: true)
    }
}
