//
//  ViewModelType.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 4/2/18.
//  Copyright © 2018 Navus. All rights reserved.
//

protocol ViewModelType {
    associatedtype Input
    associatedtype Output
    
    func transform(input: Input) -> Output
}
