//
//  Application.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 4/3/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RealmSwift
import Kingfisher
import RxSwift
import RxRealm

@objc final class Application: NSObject {
    
    @objc private (set) var navigator: AppNavigator!
    @objc static let shared = Application()
    private override init() { super.init() }
    private (set) var learningItems = [LearningKitItem]()
    let ntfCount = BehaviorSubject<Int>(value: 0)
    
    var learningSection: [LearningKitItemViewModel] {
        get {
            return learningItems.map { $0.item }
        }
    }
    
    func react(row: Int) -> () {
        return learningItems[row].react()
    }
    
    @objc func configureMainInterface(in window: UIWindow) {
        configureAppearance()
        let realm = try! Realm()
        let conference = realm
            .objects(Conference.self)
            .filter("id == \(UserDefaultsHelper.helper.fetch(.selectedConferenceId) as! Int)")
            .first!
        let k = realm.objects(ConferenceNotification.self)
            .filter("read == false")
        Observable.collection(from: k)
            .map { $0.count }
            .do(onNext: { UIApplication.shared.applicationIconBadgeNumber = $0 })
            .bind(to: ntfCount)
            .disposed(by: rx.disposeBag)
        learningItems = [.programme,
                         .eMaterials,
                         .ePosters,
                         .timeline,
                         .learningQuestions,
                         .myNotes,
                         .voting,
                         .dialogues,
                         .buildASummary]
        
        navigator = AppNavigator(with: Conference(value: conference))
        let storyboard = UIStoryboard(name: "New", bundle: nil)
        //home
        let home = storyboard.instantiateViewController(withIdentifier: "HomeController") as! HomeController
        home.viewModel = HomeViewModel()
        let homeNavigation = UINavigationController(rootViewController: home)
        homeNavigation.tabBarItem = UITabBarItem(title: nil,
                                                 image: #imageLiteral(resourceName: "newHome"),
                                                 selectedImage: nil)
        //programme
        let programmeController = storyboard.instantiateViewController(withIdentifier: ProgrammeController.reuseId) as! ProgrammeController
        programmeController.viewModel = ProgrammeViewModel()
        let programmeNavigation = UINavigationController(rootViewController: programmeController)
        programmeNavigation.tabBarItem = UITabBarItem(title: nil,
                                                      image: #imageLiteral(resourceName: "program"),
                                                      selectedImage: nil)
        //congress info
        let congressInfoController = storyboard.instantiateViewController(withIdentifier: CongressInfoController.reuseId) as! CongressInfoController
        congressInfoController.viewModel = CongressInfoViewModel()
        let congressNavigation = UINavigationController(rootViewController: congressInfoController)
        congressNavigation.tabBarItem = UITabBarItem(title: nil,
                                                     image: #imageLiteral(resourceName: "congress"),
                                                     selectedImage: nil)
        //learning
        let learningCentre = storyboard.instantiateViewController(withIdentifier: "LearningCentreContrller") as! LearningCentreContrller
        learningCentre.viewModel = LearningCentreViewModel()
        let learningNavigation = UINavigationController(rootViewController: learningCentre)
        learningNavigation.tabBarItem = UITabBarItem(title: nil,
                                                     image: #imageLiteral(resourceName: "learning"),
                                                     selectedImage: nil)
        //settings
        let settings = storyboard.instantiateViewController(withIdentifier: "SettingsController") as! SettingsController
        settings.viewModel = SettingsViewModel()
        let settingsNavigation = UINavigationController(rootViewController: settings)
        settingsNavigation.tabBarItem = UITabBarItem(title: nil,
                                                     image: #imageLiteral(resourceName: "menu"),
                                                     selectedImage: nil)
        let tabBarController = UITabBarController()
        tabBarController.tabBar.isTranslucent = false
        tabBarController.viewControllers = [
            homeNavigation,
            programmeNavigation,
            congressNavigation,
            learningNavigation,
            settingsNavigation
        ]
        tabBarController.viewControllers?.forEach({ (vc) in
            vc.tabBarItem.imageInsets = UIEdgeInsets(top: 5, left: 0, bottom: -5, right: 0)
            vc.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: 200)
        })
        window.rootViewController = tabBarController
    }
    
    private func configureAppearance() {
        let globalColor = UIColor(hex: kGlobalColor, andAlpha: 1.0)
        let backButtonColor = UIColor(hex: "515251", andAlpha: 1.0)
        let navTitleColor = UIColor(hex: "404040", andAlpha: 1.0)
        // Set tint color of all buttons
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window.tintColor = globalColor
        // change UINavigationBar background color
        UINavigationBar.appearance().isTranslucent = false
        // change UINavigationBar text color
        UINavigationBar.appearance().tintColor = backButtonColor
        UINavigationBar.appearance().titleTextAttributes = [
            NSAttributedStringKey.foregroundColor: navTitleColor!,
            NSAttributedStringKey.font: UIFont(name: "Avenir-Heavy", size: 16.0)!
        ]
        
        if #available(iOS 11.0, *) {
            UINavigationBar.appearance().largeTitleTextAttributes = [
                NSAttributedStringKey.foregroundColor: navTitleColor!,
                NSAttributedStringKey.font: UIFont(name: "Avenir-Heavy", size: 36.0)!
            ]
        }
        
        //toolbar colors
        UIToolbar.appearance().tintColor = globalColor
        // change statusBarStyle to dark
        UIApplication.shared.statusBarStyle = .default
        // change UISegmentedControl text and background color
        UISegmentedControl.appearance().tintColor = .white
        UISegmentedControl.appearance().backgroundColor = globalColor
        // UITableView - hide table's separators if no data found
        UITableView.appearance().tableFooterView = UIView()
        // UISwitch - change on tint color
        UISwitch.appearance().onTintColor = UIColor(hex: kOtherColor, andAlpha: 1.0)
    }
    
}

extension UITabBar {
    // Workaround for iOS 11's new UITabBar behavior where on iPad, the UITabBar inside
    // the Master view controller shows the UITabBarItem icon next to the text
    override open var traitCollection: UITraitCollection {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return UITraitCollection(horizontalSizeClass: .compact)
        }
        return super.traitCollection
    }
}

protocol Reactor {
    associatedtype ItemType
    var item: ItemType { get }
    func react()
}

enum LearningKitItem {
    case programme
    case eMaterials
    case ePosters
    case timeline
    case learningQuestions
    case myNotes
    case voting
    case dialogues
    case buildASummary
}

extension LearningKitItem: Reactor {
    
    typealias ItemType = LearningKitItemViewModel
    
    var item: LearningKitItemViewModel {
        switch self {
        case .programme:
            return LearningKitItemViewModel(with: "Programme", and: #imageLiteral(resourceName: "programme"))
        case .eMaterials:
            return LearningKitItemViewModel(with: "E-materials", and: #imageLiteral(resourceName: "ematerials"))
        case .ePosters:
            return LearningKitItemViewModel(with: "E-posters", and: #imageLiteral(resourceName: "eposters"))
        case .timeline:
            return LearningKitItemViewModel(with: "Timeline", and: #imageLiteral(resourceName: "timeline"))
        case .learningQuestions:
            return LearningKitItemViewModel(with: "Learning Questions", and: #imageLiteral(resourceName: "knowledgeTest"))
        case .myNotes:
            return LearningKitItemViewModel(with: "My Notes", and: #imageLiteral(resourceName: "newNotes"))
        case .voting:
            return LearningKitItemViewModel(with: "Voting", and: #imageLiteral(resourceName: "voting"))
        case .dialogues:
            return LearningKitItemViewModel(with: "Dialogues", and: #imageLiteral(resourceName: "dialogue"))
        case .buildASummary:
            return LearningKitItemViewModel(with: "Build a summary", and: #imageLiteral(resourceName: "summaries"))
        }
    }
    
    func react() {
        switch self {
        case .programme:
            return Application.shared.navigator.toProgramme()
        case .eMaterials:
            return Application.shared.navigator.toEmaterials()
        case .ePosters:
            return Application.shared.navigator.toEPosters()
        case .timeline:
            return Application.shared.navigator.toTimeline()
        case .learningQuestions:
            return Application.shared.navigator.toLearningQuestions()
        case .myNotes:
            return Application.shared.navigator.toMyNotes()
        case .voting:
            return Application.shared.navigator.toVoting()
        case .dialogues:
            return Application.shared.navigator.toDialogues()
        case .buildASummary:
            return Application.shared.navigator.toBuildASummary()
        }
    }
    
}

extension LearningKitItem {
    
    var settingsItem: SettingsItem {
        switch self {
        case .programme:
            return .image(title: "Programme", image: #imageLiteral(resourceName: "settingsProgram"))
        case .eMaterials:
            return .image(title: "E-materials", image: #imageLiteral(resourceName: "settingsEmaterials"))
        case .ePosters:
            return .image(title: "E-posters", image: #imageLiteral(resourceName: "settingsEposters"))
        case .timeline:
            return .image(title: "Timeline", image: #imageLiteral(resourceName: "settingsTimeline"))
        case .learningQuestions:
            return .image(title: "Learning Questions", image: #imageLiteral(resourceName: "settingsKnowledgeTest"))
        case .myNotes:
            return .image(title: "My Notes", image: #imageLiteral(resourceName: "settingsNotes"))
        case .voting:
            return .image(title: "Voting", image: #imageLiteral(resourceName: "settingsVoting"))
        case .dialogues:
            return .image(title: "Dialogues", image: #imageLiteral(resourceName: "settingsDialogues"))
        case .buildASummary:
            return .image(title: "Build a Summary", image: #imageLiteral(resourceName: "settingsSummaries"))
        }
    }
    
}

