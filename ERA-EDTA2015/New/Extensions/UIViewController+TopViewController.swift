//
//  UIViewController+TopViewController.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 4/26/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import UIKit

extension UIViewController {
    
    static func topViewController() -> UIViewController {
        return UIViewController.topViewControllerWithRootViewController(UIApplication.shared.keyWindow!.rootViewController!)
    }
    
    static func topViewControllerWithRootViewController(_ root: UIViewController) -> UIViewController {
        if root is UITabBarController {
            let tabBarController = root as! UITabBarController
            return UIViewController.topViewControllerWithRootViewController(tabBarController.selectedViewController!)
        } else if root is UINavigationController {
            let navigationController = root as! UINavigationController
            return UIViewController.topViewControllerWithRootViewController(navigationController.viewControllers.last!)
        } else if root.presentedViewController != nil {
            let presentedViewController = root.presentedViewController!
            if presentedViewController is UISearchController {
                return root
            }
            return  UIViewController.topViewControllerWithRootViewController(presentedViewController)
        } else {
            return root
        }
    }
    
}
