//
//  DateExtension.swift
//  CongressApp
//
//  Created by Dejan Bekic on 11/27/17.
//  Copyright © 2017 Navus Consulting GmbH. All rights reserved.
//

import Foundation

extension Date {
    
    static func timeline(from start: Date, to end: Date) -> String {
        let df = DateFormatter()
        df.dateFormat = "MM/dd/yy"
        let day = df.string(from: start)
        df.dateFormat = "hh:mm a"
        let startHour = df.string(from: start)
        let endHour = df.string(from: end)
        return "\(day) \(startHour) - \(endHour)"
    }
    
    static func duration(from start: Date, to end: Date) -> String? {
        let dateComponentsFormatter = DateComponentsFormatter()
        dateComponentsFormatter.allowedUnits = [.day,.hour,.minute,]
        dateComponentsFormatter.maximumUnitCount = 2
        dateComponentsFormatter.unitsStyle = .full
        let duration = dateComponentsFormatter.string(from: start, to: end)  // "1 month"
        return duration
    }
    
    func shortTime() -> String {
        let df = DateFormatter()
        df.dateFormat = "hh:mm a"
        return df.string(from: self)
    }
    
    var daySuffix: String {
        get {
            let calendar = Calendar.current
            let dayOfMonth = calendar.component(.day, from: self)
            switch dayOfMonth {
            case 1, 21, 31: return "st"
            case 2, 22: return "nd"
            case 3, 23: return "rd"
            default: return "th"
            }
        }
    }
    
    static func rangeString(for start: Date, and end: Date) -> String {
        let calendar = Calendar.current
        let df = DateFormatter()
        df.dateFormat = "MMMM dd"
        let startMonth = calendar.component(.month, from: start)
        let endMonth = calendar.component(.month, from: end)
        let startDay = calendar.component(.day, from: start)
        let endDay = calendar.component(.day, from: end)
        
        var startString: String
        var endString: String
        
        if startDay == endDay && startMonth == endMonth {
            //one day conf
            startString = df.string(from: start)
            startString.append("\(start.daySuffix)")
            endString = ""
            return startString
        } else if startMonth != endMonth {
            //dif months
            startString = df.string(from: start)
            endString = df.string(from: end)
            endString.append("\(end.daySuffix)")
        } else {
            //same month
            startString = df.string(from: start)
            endString = String(endDay)
            endString.append("\(end.daySuffix)")
        }
        
        return "\(startString) - \(endString)"
    }
    
    func year() -> Int {
        return Calendar.current.component(.year, from: self)
    }
    
}
