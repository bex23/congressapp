//
//  String+JSON.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 4/19/18.
//  Copyright © 2018 Navus. All rights reserved.
//

extension String {
    
    func convertToDictionary() -> [String: Any]? {
        if let data = data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                fatalError(error.localizedDescription)
            }
        }
        return nil
    }
    
}

extension Dictionary {
    
    var json: String? {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
            return String(bytes: jsonData, encoding: String.Encoding.utf8) ?? nil
        } catch {
            return nil
        }
    }
}
