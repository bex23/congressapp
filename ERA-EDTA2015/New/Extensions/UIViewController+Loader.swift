//
//  UIViewController+Loader.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 4/26/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import UIKit

extension UIViewController {
    
    @objc func addLoader() {
        let loader = Bundle.main.loadNibNamed("LoaderView", owner: self, options: nil)?.first! as! LoaderView
        if
            let navigation = navigationController,
            let superView = navigation.view.superview {
            loader.center = superView.center
            superView.isUserInteractionEnabled = false
            superView.addSubview(loader)
            superView.bringSubview(toFront: loader)
        } else {
            loader.center = view.center
            view.isUserInteractionEnabled = false
            view.addSubview(loader)
            view.bringSubview(toFront: loader)
        }
    }
    
    @objc func removeLoaderIfExists() {
        var container: UIView = view
        if
            let navigation = navigationController,
            let superView = navigation.view.superview {
            container = superView
        }
        container.subviews.forEach {
            if $0 is LoaderView {
                $0.removeFromSuperview()
            }
        }
        container.isUserInteractionEnabled = true
    }
    
}
