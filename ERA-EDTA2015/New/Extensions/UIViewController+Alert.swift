//
//  UIViewController+Alert.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 4/28/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RxSwift
import RxCocoa

extension UIViewController {
    
    func promptFor<Action : CustomStringConvertible>(title: String = "Error",
                                                     message: String,
                                                     type: UIAlertControllerStyle = .alert,
                                                     cancelAction: Action,
                                                     destructiveAction: Action? = nil,
                                                     actions: [Action] = []) -> Observable<Action> {
        return Observable.create { observer in
            let alertView = UIAlertController(title: title,
                                              message: message,
                                              preferredStyle: type)
            alertView.addAction(UIAlertAction(title: cancelAction.description,
                                              style: .cancel) { _ in
                                                observer.on(.next(cancelAction))
            })
            
            if let destructiveAction = destructiveAction {
                alertView.addAction(UIAlertAction(title: destructiveAction.description,
                                                  style: .destructive) { _ in
                                                    observer.on(.next(destructiveAction))
                })
            }
            
            for action in actions {
                alertView.addAction(UIAlertAction(title: action.description,
                                                  style: .default) { _ in
                                                    observer.on(.next(action))
                })
            }
            
            self.present(alertView, animated: true, completion: nil)
            
            return Disposables.create {
                alertView.dismiss(animated:false, completion: nil)
            }
        }
    }
    
    
}
