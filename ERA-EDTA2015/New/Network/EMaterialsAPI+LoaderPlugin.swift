//
//  EMaterialsAPI+Loader.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 5/19/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import UIKit
import Moya
import Result

public final class LoaderPlugin: PluginType {
    
    public init() {}
    
    // MARK: Plugin
    
    /// Called immediately before a request is sent over the network (or stubbed).
    public func willSend(_ request: RequestType, target: TargetType) {
        guard
            let target = target as? EmaterialsAPI,
            target.shouldShowLoader else { return }
        UIViewController.topViewController().addLoader()
    }
    
    /// Called after a response has been received, but before the MoyaProvider has invoked its completion handler.
    public func didReceive(_ result: Result<Moya.Response, MoyaError>, target: TargetType) {
        guard
            let target = target as? EmaterialsAPI,
            target.shouldShowLoader else { return }
        UIViewController.topViewController().removeLoaderIfExists()
    }
    
}

private extension EmaterialsAPI {
    
    var shouldShowLoader: Bool {
        switch self {
        case .questions, .answers:
            return true
        default:
            return false
        }
    }
    
}

