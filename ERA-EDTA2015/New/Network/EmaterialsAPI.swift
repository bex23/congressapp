//
//  VotingAPI.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 2/1/18.
//  Copyright © 2018 Navus. All rights reserved.
//

enum EmaterialsAPI {
    //portal
    case portal
    //voting
    case votingQuestions(presentationId: Int)
    case answerQuestion(presentationId: Int, questionId: Int, answer: String)
    case activeVotings(conferenceId: Int)
    case finishedVotings(conferenceId: Int)
    case votingFrame(presentationId: Int)
    //articles
    case articles(page: Int)
    //recommendations
    case recommendations
    //hcp
    case updateHcp(hcp: Bool, id: Int)
    //qr
    case qrCode(code: String)
    //opt in
    case optIn(id: Int)
    //banner
    case banner(positon: BannerPosition)
    case track(action: ActionType, id: Int, type: String, objectInfo: String)
    //learning questions
    case campaigns
    case questions(capaignId: Int)
    case answers(enties: [SurveyEntry])
    //campaign status
    case availableCampaigns(badge: String)
    case answeredCampaigns(badge: String)
    case notifications
}

enum ActionType: String {
    case show = "SHOWN"
    case click = "CLICK"
    case view = "VIEWED"
}
