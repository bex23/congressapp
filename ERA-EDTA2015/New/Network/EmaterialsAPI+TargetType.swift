//
//  VotingApi+TargetType.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 2/1/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import Moya
import UIKit
import Alamofire

extension EmaterialsAPI: TargetType {
    
    var baseURL: URL {
        switch self {
        case .campaigns, .questions, .answers, .answeredCampaigns, .availableCampaigns:
            return URL(string: "http://leadlink.e-materials.com/api/")!

        default:
            return URL(string: kBaseAPIURL)!
        }
    }
    
    var path: String {
        switch self {
        case .portal:
            return "portals/current"
        case .votingQuestions(let presentationId):
            return "presentations/\(presentationId)/votings"
        case .answerQuestion(let presentationId, let questionId, _):
            return "presentations/\(presentationId)/votings/\(questionId)/vote"
        case .articles:
            return "articles"
        case .recommendations:
            return "recommendations"
        case .updateHcp(_, let id),
             .optIn(let id):
            return "users/\(id)"
        case .qrCode:
            return "qrcode"
        case .banner:
            return "banners/next"
        case .track:
            return "track_user_actions"
        case .campaigns:
            return "campaigns"
        case .questions(let campaignId):
            return "campaigns/\(campaignId)"
        case .answers:
            return "answers"
        case .availableCampaigns(let badge):
            return "users/\(badge)/available"
        case .answeredCampaigns(let badge):
            return "users/\(badge)/answered"
        case .activeVotings(let confId):
            return "conference/\(confId)/votings/active"
        case .finishedVotings(let confId):
            return "conference/\(confId)/votings/finished"
        case .votingFrame(let presentationId):
            return "presentations/\(presentationId)/frame"
        case .notifications:
            return "notifications"
        }
    }
    
    var sampleData: Data {
        return "{\"status\":200}".data(using: String.Encoding.utf8)!
    }
    
    var task: Task {
        return .requestParameters(parameters: parameters ?? [:],
                                  encoding: encoding)
    }
    
    var method: Moya.Method {
        switch self {
        case .answerQuestion,
             .qrCode,
             .optIn,
             .track,
             .answers:
            return .post
        case .updateHcp:
            return .put
        default:
            return .get
        }
    }
    
    private var encoding: ParameterEncoding {
        switch self.method {
        case .post:
            switch self {
            case .answers:
                return JsonArrayEncoding.default
            default:
                return URLEncoding.default
            }
        case .put:
            return JSONEncoding.default
        default:
            return URLEncoding.default
        }
    }
    
    private var parameters: [String: Any]? {
        switch self {
        case .answerQuestion(_, _,let answer):
            return ["content": ["choices": [answer]]]
        case .articles(let page):
            return ["is_hot_topic": "0",
                    "per_page": "10",
                    "page": page]
        case .recommendations:
            return ["limit": "10",
                    "conference_id": "\(UserDefaultsHelper.helper.fetch(.selectedConferenceId) ?? "")"]
        case .updateHcp(let hcp, _):
            return ["hcp": hcp ? 1 : 0] 
        case .qrCode(let code):
            return ["code": code]
        case .banner(let position):
            let hcp = UserDefaultsHelper.helper.fetch(.hcp) as? Bool ?? false ? 1 : 0
            return ["position": position.description,
                    "hcp":  hcp,
                    "conference_id": "\(UserDefaultsHelper.helper.fetch(.selectedConferenceId) ?? "")"]
        case .notifications:
            return ["conference_id": "\(UserDefaultsHelper.helper.fetch(.selectedConferenceId) ?? "")"]
        case .optIn:
            return ["action_name": "opt-in",
                    "conference_id": "\(UserDefaultsHelper.helper.fetch(.selectedConferenceId) ?? "")"]
        case .track(let action, let id, let type, let info):
            return ["relation": action.rawValue,
                    "object_id": id,
                    "object_type": type,
                    "object_info": info,
                    "conference_id": "\(UserDefaultsHelper.helper.fetch(.selectedConferenceId) ?? "")"]
        case .questions:
            return ["include": "questions"]
        case .answers(let entries):
            var arr = [[String: Any]]()
            entries.forEach {
                arr.append(contentsOf: $0.formatAnswersForSending())
            }
            return ["jsonArray": arr]
        default:
            return nil
        }
    }
    
    var headers: [String: String]? {
        var hdrs = [String: String]()
        hdrs["Api-Key"] = kApiKey
        switch self {
        case .campaigns, .questions, .answers, .answeredCampaigns, .availableCampaigns:
            hdrs["Authorization"] = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJmaXJzdF9uYW1lIjoiRVJBRURUQSIsInN1YiI6MTQsImlzcyI6Imh0dHA6XC9cL2xlYWRsaW5rLmUtbWF0ZXJpYWxzLmNvbVwvYXBpXC9sb2dpbiIsImlhdCI6MTUyNjU0OTcyMSwiZXhwIjoxODM3NTg5NzIxLCJuYmYiOjE1MjY1NDk3MjEsImp0aSI6ImJkNTA0ZWJkYTkwYmI5MmZjZjE1ZWY3NjkzOGYwMDk4In0.aeMoffSuvQhYtaMAWnYROHFbJ0bwGK-_zf2lI36RfpM"
        case .portal:
            return hdrs
        default:
            if let token = UserDefaults.standard.string(forKey: "kUserToken") {
                hdrs["Authorization"] = "Bearer \(token)"
            }
            hdrs["device-id"] = UIDevice.current.identifierForVendor!.uuidString
        }
        return hdrs
    }
    
}

func url(_ route: TargetType) -> String {
    return route.baseURL.appendingPathComponent(route.path).absoluteString
}

struct JsonArrayEncoding: Moya.ParameterEncoding {
    
    public static var `default`: JsonArrayEncoding { return JsonArrayEncoding() }
    
    /// Creates a URL request by encoding parameters and applying them onto an existing request.
    ///
    /// - parameter urlRequest: The request to have parameters applied.
    /// - parameter parameters: The parameters to apply.
    ///
    /// - throws: An `AFError.parameterEncodingFailed` error if encoding fails.
    ///
    /// - returns: The encoded request.
    public func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
        var req = try urlRequest.asURLRequest()
        let json = try JSONSerialization.data(withJSONObject: parameters!["jsonArray"]!, options: JSONSerialization.WritingOptions.prettyPrinted)
        req.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        req.httpBody = json
        return req
    }
    
}
