//
//  Networking.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 2/1/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import Foundation
import Moya
import RxSwift
import Alamofire
import Reachability
import BOTNetworkActivityIndicator

fileprivate(set) var provider = Networking.newDefaultNetworking()

class OnlineProvider<Target> where Target: Moya.TargetType {
    
    fileprivate let online: Observable<Bool>
    fileprivate let provider: MoyaProvider<Target>
    
    init(endpointClosure: @escaping MoyaProvider<Target>.EndpointClosure = MoyaProvider.defaultEndpointMapping,
         requestClosure: @escaping MoyaProvider<Target>.RequestClosure = MoyaProvider.defaultRequestMapping,
         stubClosure: @escaping MoyaProvider<Target>.StubClosure = MoyaProvider.neverStub,
         manager: Manager = MoyaProvider<Target>.defaultAlamofireManager(),
         plugins: [PluginType] = [],
         trackInflights: Bool = false,
         online: Observable<Bool> = connectedToInternet()) {
        self.online = online
        self.provider = MoyaProvider(endpointClosure: endpointClosure, requestClosure: requestClosure, stubClosure: stubClosure, manager: manager, plugins: plugins, trackInflights: trackInflights)
    }
    
    func request(_ token: Target) -> Observable<Moya.Response> {
        let actualRequest = provider.rx.request(token)
        return online
            .take(1)        // Take 1 to make sure we only invoke the API once.
            .flatMap { _ in // Turn the online state into a network request
                return actualRequest
        }
    }
    
    func requestWithProgress(_ token: Target) -> Observable<Moya.ProgressResponse> {
        let actualRequest = provider.rx.requestWithProgress(token)
        return online
            .filter{ $0 == true } // Wait until we're online
            .take(1)        // Take 1 to make sure we only invoke the API once.
            .flatMap { _ in // Turn the online state into a network request
                return actualRequest
        }
    }
}

protocol NetworkingType {
    associatedtype T: TargetType
    var provider: OnlineProvider<T> { get }
}

struct Networking: NetworkingType {
    typealias T = EmaterialsAPI
    let provider: OnlineProvider<EmaterialsAPI>
}


// "Public" interfaces
extension Networking {
    /// Request to fetch a given target. Ensures that valid XApp tokens exist before making request
    func request(_ token: EmaterialsAPI, defaults: UserDefaults = UserDefaults.standard) -> Observable<Moya.Response> {
        // TODO: Add token refresh
        return self.provider.request(token)
    }
    
    func requestWithProgress(_ token: EmaterialsAPI, defaults: UserDefaults = UserDefaults.standard) -> Observable<Moya.ProgressResponse> {
        // TODO: Add token refresh
        return self.provider.requestWithProgress(token)
    }
}

// Static methods
extension NetworkingType {
    
    static func newDefaultNetworking() -> Networking {
        return Networking(provider: newProvider(plugins))
    }
    
    static func newStubbingNetworking() -> Networking {
        return Networking(provider: OnlineProvider(endpointClosure: endpointsClosure(), requestClosure: Networking.endpointResolver(), stubClosure: MoyaProvider.immediatelyStub, online: .just(true)))
    }
    
    static func endpointsClosure<T>(_ xAccessToken: String? = nil) -> (T) -> Endpoint<T> where T: TargetType {
        return { target in
            let endpoint: Endpoint<T> = Endpoint<T>(url: url(target), sampleResponseClosure: {.networkResponse(200, target.sampleData)}, method: target.method, task: target.task, httpHeaderFields: target.headers)
            //Inject additional parameters if needed
            return endpoint
            
        }
    }
    
    static var plugins: [PluginType] {
        return [NetworkActivityPlugin(networkActivityClosure: { status, _ in
                status == .began ? BOTNetworkActivityIndicator.shared().pushNetworkActivity() : BOTNetworkActivityIndicator.shared().popNetowrkActivity()}),
                LoaderPlugin()]
    }
    
    // (Endpoint<Target>, NSURLRequest -> Void) -> Void
    static func endpointResolver<T>() -> MoyaProvider<T>.RequestClosure where T: TargetType {
        return { (endpoint, closure) in
            var request = try! endpoint.urlRequest()
            request.httpShouldHandleCookies = false
            closure(.success(request))
        }
    }
    
    static func stubBehaviour<T>(_: T) -> Moya.StubBehavior {
        return .never
    }
}

private func newProvider<T>(_ plugins: [PluginType], xAccessToken: String? = nil) -> OnlineProvider<T> where T: TargetType {
    return OnlineProvider(endpointClosure: Networking.endpointsClosure(xAccessToken),
                          requestClosure: Networking.endpointResolver(),
                          stubClosure: Networking.stubBehaviour,
                          plugins: plugins)
}

private let reachabilityManager = ReachabilityManager()
func connectedToInternet() -> Observable<Bool> {
    guard let online = reachabilityManager?.reach else {
        return Observable.just(false)
    }
    return online
}
private class ReachabilityManager {
    
    private let reachability: Reachability
    
    let _reach = ReplaySubject<Bool>.create(bufferSize: 1)
    var reach: Observable<Bool> {
        return _reach.asObservable()
    }
    
    init?() {
        guard let r = Reachability() else {
            return nil
        }
        self.reachability = r
        
        do {
            try self.reachability.startNotifier()
        } catch {
            return nil
        }
        
        self._reach.onNext(self.reachability.connection != .none)
        
        self.reachability.whenReachable = { _ in
            DispatchQueue.main.async { self._reach.onNext(true) }
        }
        
        self.reachability.whenUnreachable = { _ in
            DispatchQueue.main.async { self._reach.onNext(false) }
        }
    }
    
    deinit {
        reachability.stopNotifier()
    }
}
