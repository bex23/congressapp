//
//  QuestionView.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 3/2/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import SnapKit
import RxSwift

class QuestionView: UIView {
    
    static let followText = "Select only one answer. Use the lecture screen as an\nanswering reference."
    static let thankYouText = "Thank you for participating in this voting."
    
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var resultsImageView: UIImageView!
    @IBOutlet weak var borderHolder: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var activeHolder: UIView!
    @IBOutlet weak var optionsHolder: UIView!
    @IBOutlet weak var selectLabel: UILabel!
    @IBOutlet weak var resultsHolder: UIView!
    @IBOutlet weak var resultsHolderHeight: NSLayoutConstraint!
    var resultViews: [ResultView]? = nil {
        didSet {
            guard let resViews = resultViews else { return }
            resViews.enumerated().forEach({ (index, view) in
                resultsHolder.addSubview(view)
                view.backgroundColor = borderHolder.backgroundColor
                self.setNeedsUpdateConstraints()
                view.snp.makeConstraints({ (make) in
                    make.leading.equalTo(0)
                    make.trailing.equalTo(0)
                    make.top.equalTo(index == 0
                        ? 0
                        : resViews[index - 1].snp.bottom)
                })
                let totalHeight = resViews.map { $0.height }.reduce(0, +)
                self.resultsHolderHeight.constant = totalHeight
                self.layoutIfNeeded()
            })
        }
    }
    
    private var optionsView: AnswersView!
    fileprivate var subject: PublishSubject<(VotingQuestion, String)>!
    fileprivate var skipSubject: PublishSubject<Int>?
    private lazy var border: CAShapeLayer = {
        let dashedBorder = CAShapeLayer()
        dashedBorder.strokeColor =  UIColor(hex: kGlobalColor, andAlpha: 1.0).cgColor
        dashedBorder.lineDashPattern = [5, 5]
        dashedBorder.lineWidth = 2
        dashedBorder.fillColor = nil
        return dashedBorder
    }()
    
    var question: VotingQuestion! {
        didSet {
            title = "\(question.order ?? 1). \(question.title ?? "N/A")"
            if optionsHolder.subviews.count == 0 {
                optionsView = AnswersView(containerView: optionsHolder, margin: 35, options: question.options!)
                optionsView.delegate = self
            }
            state = (question.active, question.finished, question.userAnswer)

        }
    }
    
    var scrollView: UIScrollView? {
        get {
            guard
                let superview = superview,
                let scrollView = superview.superview as? UIScrollView else { return nil }
            return scrollView
        }
    }
    var state: (active: Bool?, finished: Bool?, userAnswer: UserAnswer?) = (nil, nil, nil) {
        didSet {
            if state.active! {
                if (resultsImageView != nil) { resultsImageView.alpha = 0.0 }
                if state.userAnswer != nil  {
                    border.removeFromSuperlayer()
                    optionsView.isUserInteractionEnabled = false
                    optionsView.setChoice((state.userAnswer?.choices?.first)!)
                    selectLabel.text = QuestionView.thankYouText
                    UIView.animate(
                        withDuration: 0.2,
                        animations: {
                            self.borderHolder.backgroundColor = UIColor(hex: kGlobalColor, andAlpha: 1.0)
                            self.titleLabel.textColor = .white
                            self.selectLabel.textColor = .white
                    })
                } else {
                    selectLabel.text = QuestionView.followText
                    borderHolder.layer.addSublayer(border)
                    borderHolder.backgroundColor = .white
                    titleLabel.textColor = UIColor(hex: "231F20", andAlpha: 1.0)
                    if oldValue.active != state.active {
                        self.activeHolder.snp.removeConstraints()
                        self.border.frame = self.borderHolder.bounds
                        self.border.path = UIBezierPath(rect: self.borderHolder.bounds).cgPath
                    }
                }
            } else if state.finished! {
                border.removeFromSuperlayer()
                borderHolder.backgroundColor = UIColor(hex: "F3F3F3", andAlpha: 1.0)
                titleLabel.textColor = UIColor(hex: "231F20", andAlpha: 1.0)
                if oldValue.finished != state.finished {
                    self.activeHolder.snp.updateConstraints({ make in
                        make.height.equalTo(0)
                    })
                }
                if question.statistics?.count ?? 0 > 0 {
                    //results image view show
                    resultsImageView.image = UIImage(named: "results_yes")!
                    //add subviews
                    resultViews = ResultsViewFactory.getResultViews(question: question)
                } else {
                    //no results image view show
                    resultsImageView.image = UIImage(named: "results_no")!
                }
            } else {
                if (resultsImageView != nil) { resultsImageView.alpha = 0.0 }
                border.removeFromSuperlayer()
                borderHolder.backgroundColor = UIColor(hex: "F3F3F3", andAlpha: 1.0)
                titleLabel.textColor = UIColor(hex: "939598", andAlpha: 1.0)
                self.activeHolder.snp.updateConstraints({ make in
                    make.height.equalTo(0)
                })
            }
            if (resultsImageView != nil) {
                self.resultsImageView.alpha = state.finished! ? 1.0 : 0.0
            }
            let shouldScroll = ((state.active ?? false) && (state.userAnswer == nil || state.userAnswer?.choices?.count == 0))
                                || ((state.finished ?? false) && (oldValue.finished != state.finished))
            NotificationCenter.default.post(name: NSNotification.Name("SrollViewNTF"),
                                            object: shouldScroll ? self : nil)
        }
    }
    
    private var title: String? {
        didSet {
            titleLabel.text = title
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.font = UIFont.systemFont(ofSize: 16.0, weight: .semibold)
        if (resultsImageView != nil) {
            resultsImageView.alpha = 0.0
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let bounds = borderHolder.bounds
        border.frame = bounds
        border.path = UIBezierPath(rect: bounds).cgPath
    }
    
    @IBAction func onSkip(_ sender: Any) {
        if let skip = skipSubject {
            skip.onNext(question.id ?? 0)
        }
    }
}

extension QuestionView {
    
    class func from(question: VotingQuestion, with tag: Int, selection: PublishSubject<(VotingQuestion, String)>) -> QuestionView {
        let view = UINib(nibName: "QuestionView",
                         bundle: nil).instantiate(withOwner: nil,
                                                  options: nil)[0] as! QuestionView
        view.question = question
        view.tag = tag
        view.subject = selection
        return view
    }
    
    class func popupView(question: VotingQuestion, with tag: Int, selection: PublishSubject<(VotingQuestion, String)>, skip: PublishSubject<Int>) -> QuestionView {
        let view = UINib(nibName: "QuestionPopupView",
                         bundle: nil).instantiate(withOwner: nil,
                                                  options: nil)[0] as! QuestionView
        view.frame.size.width = UIScreen.main.bounds.width - 30.0
        view.question = question
        view.tag = tag
        view.subject = selection
        view.skipSubject = skip
        return view
    }
    
}

extension QuestionView: AnswersViewDelegate {
    
    func didSelect(option: String) {
        let q = question
        q?.userAnswer = UserAnswer()
        q?.userAnswer?.choices = [option]
        question = q
        subject.onNext((question, option))
    }
    
}
