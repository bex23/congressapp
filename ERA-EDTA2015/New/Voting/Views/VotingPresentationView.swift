//
//  VotingPresentationView.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 3/3/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RxCocoa
import RxSwift

class VotingPresentationView: UIView {
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var roomLabel: UILabel!
    @IBOutlet weak var speakerNameLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet fileprivate weak var followLabel: UILabel!
    @IBOutlet weak var followLabelHeight: NSLayoutConstraint!
    @IBOutlet fileprivate weak var speakerImageImageView: UIImageView!
    @IBOutlet weak var ongoingImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        speakerImageImageView.layer.cornerRadius = speakerImageImageView.frame.width / 2.0
        speakerImageImageView.layer.masksToBounds = true
    }
    
}

extension Reactive where Base: VotingPresentationView {
    
    /// Bindable sink for `talk` property
    var talk: Binder<Talk> {
        return Binder(self.base) { view, talk in
            view.titleLabel.text = talk.tlkTitle ?? "N/A"
            view.speakerNameLabel.text = talk.speakerName()
            view.roomLabel.text = talk.session?.venue?.venName ?? "N/A"
            view.timeLabel.text = talk.timeframe()
            view.speakerImageImageView.kf.setImage(with: URL(string: talk.tlkSpeakerImageUrl ?? ""), placeholder: UIImage(named: "speaker-placeholder"))
            view.ongoingImage.alpha = !VotingService.shared.isOngoing(talk) ? 0.0 : 1.0
            view.timeLabel.font = UIFont.systemFont(ofSize: 13.0, weight: !VotingService.shared.isOngoing(talk) ? .medium : .bold)
            view.followLabelHeight.constant = VotingService.shared.isOngoing(talk) ?             (view.followLabel.text ?? "").height(withConstrainedWidth: UIScreen.main.bounds.width - 50.0, font: view.followLabel.font) : 0.0
            UIView.animate(withDuration: 0.2, animations: {
                view.layoutIfNeeded()
            })
        }
    }
    
}
