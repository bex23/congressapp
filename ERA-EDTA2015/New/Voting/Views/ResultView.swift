//
//  ResultView.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 3/7/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import Foundation

class ResultView: UIView {

    @IBOutlet private weak var answerImageView: UIImageView!
    @IBOutlet open weak var titleLabel: UILabel!
    @IBOutlet private weak var placeholderLabel: UILabel!
    @IBOutlet private weak var placeholderView: UIView!
    @IBOutlet private weak var percentageView: UIView!
    @IBOutlet private weak var percentageLabel: UILabel!
    @IBOutlet private weak var percentageWidth: NSLayoutConstraint!
    @IBOutlet private weak var placeholderLabelWidth: NSLayoutConstraint!
    @IBOutlet private weak var labelSpacing: NSLayoutConstraint!
    private var percentage: Int!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        placeholderLabel.text = ""
        placeholderLabel.backgroundColor = .clear
        placeholderView.backgroundColor = .clear
    }
    
    func setUp(
        withTitle title: String,
        resultsAttributedText: NSAttributedString,
        answerImage: UIImage,
        votesPercentage: Int,
        textWidth: CGFloat) {
        titleLabel.text = title
        answerImageView.image = answerImage
        percentageLabel.attributedText = resultsAttributedText
        placeholderLabelWidth.constant = textWidth
        percentage = votesPercentage
        labelSpacing.constant = percentage == 0 ? 0.0 : 10.0
        let fullBarWidth = UIScreen.main.bounds.width - (15 + 15 + 20 + 56 + 20 + 10 + textWidth + 20)
        let constant = fullBarWidth * (CGFloat(percentage) / 100.0)
        percentageWidth.constant = constant
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    private static let boldAttributes: [NSAttributedStringKey: Any] = [.font: UIFont.systemFont(ofSize: 14.0, weight: .semibold),
                                                                       .foregroundColor: UIColor(hex: "231F20", andAlpha: 1.0)]
    private static let regularAttributes: [NSAttributedStringKey: Any] = [.font: UIFont.systemFont(ofSize: 14.0, weight: .regular),
                                                                          .foregroundColor: UIColor(hex: "231F20", andAlpha: 1.0)]
    private static func maxWidth(question: VotingQuestion) -> CGFloat {
        let max = question.statistics!.sorted(by: { (first, second) -> Bool in
            first.percentage ?? 0 > second.percentage ?? 0
        }).first
        guard let maximum = max else { return 20.0 }
        let maxVotesWidth = ("\(maximum.votings ?? 0) " as NSString).size(withAttributes: boldAttributes).width
        let maxPercentageWidth = ("(\(maximum.percentage ?? 0)%)" as NSString).size(withAttributes: regularAttributes).width
        return maxVotesWidth + maxPercentageWidth
    }
    
    
}

extension String {
    
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
}
