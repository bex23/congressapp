//
//  VotingSegmentedControl.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 1/30/18.
//  Copyright © 2018 Navus. All rights reserved.
//

class VotingSegmentedControl: UISegmentedControl {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setTitleTextAttributes(
            [
                NSAttributedStringKey.foregroundColor: UIColor(hex: "414141", andAlpha: 1.0),
                NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15)
            ],
            for: .normal
        )
        setTitleTextAttributes(
            [
                NSAttributedStringKey.foregroundColor: UIColor.white,
                NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 15.0)
            ],
            for: .selected
        )
        setTitleTextAttributes(
            [
                NSAttributedStringKey.foregroundColor: UIColor.white,
                NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 15.0)
            ],
            for: .highlighted
        )
        let color = UIColor(hex: kGlobalColor, andAlpha: 1.0)!
        tintColor = color
        backgroundColor = .clear
        setBackgroundImage(UIImage(color: .white), for: .normal, barMetrics: .default)
        setBackgroundImage(UIImage(color: color), for: .selected, barMetrics: .default)
        setBackgroundImage(UIImage(color: color.withAlphaComponent(0.3)), for: .highlighted, barMetrics: .default)
    }
    
}
