//
//  ButtonsView.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 2/7/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RxCocoa
import RxSwift

protocol AnswersViewDelegate {
    func didSelect(option: String)
}

class AnswersView: UIView {
    
    var delegate: AnswersViewDelegate?
    private let containerView: UIView!
    private let margin: Int!
    private let options: Options!
    private let buttons: [OptionButton]
    fileprivate let disposeBag = DisposeBag()
    
    init(containerView cv: UIView, margin mar: Int, options opt: Options) {
        containerView = cv
        margin = mar
        options = opt
        buttons = OptionButton.generateButtons(fromOptions: opt)
        super.init(frame: containerView.bounds)
        //layout code
        layoutButtons()
        containerView.addSubview(self)
        containerView.clipsToBounds = true
        buttons.forEach { addSubview($0) }
        buttons.forEach { $0.addTarget(
            self,
            action: #selector(self.optionSelected(_:)),
            for: .touchUpInside)
        }
        //Disable when no internet
        connectedToInternet()
            .bind(to: rx.isUserInteractionEnabled)
            .disposed(by: disposeBag)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        var bounds = containerView.bounds
        bounds.size.height = 56.0
        frame = bounds
        layoutButtons()
    }
    
    private func layoutButtons() {
        let viewSize = containerView.frame.size
        let buttonSize = CGSize(width: 56.0, height: 56.0)
        let spaceArea = viewSize.width - buttonSize.width * CGFloat(buttons.count)
        let singleSpacing = buttons.count == 2 ? spaceArea / 3 : spaceArea / CGFloat(buttons.count - 1)
        let initialSpacing: CGFloat = buttons.count == 2 ? singleSpacing : 0.0
        buttons.enumerated().forEach {
            $0.1.frame = CGRect(
                x: initialSpacing + singleSpacing * CGFloat($0.0) + (buttonSize.width * CGFloat($0.0)),
                y: 0,
                width: buttonSize.width,
                height: buttonSize.height
            )
        }
    }
    
    //The target function
    @objc private func optionSelected(_ button: OptionButton){
        //set selected
        button.isSelected = true
        //delegate
        delegate?.didSelect(option: button.option)
    }
    
}

class OptionButton: UIButton {
    
    let option: String
    
    init(option opt: String) {
        option = opt
        super.init(frame: .zero)
        setImage(UIImage(named: "vote_\(option.uppercased())")!, for: .normal)
        setImage(UIImage(named: "vote_\(option.uppercased())_pressed")!, for: .highlighted)
        setImage(UIImage(named: "vote_\(option.uppercased())_pressed")!, for: .selected)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    class func generateButtons(fromOptions options: Options) -> [OptionButton] {
        var buttons = [OptionButton]()
        options.choices?.forEach {
            buttons.append(OptionButton(option: $0.choice ?? ""))
        }
        return buttons
    }
    
}

extension AnswersView {
    
    func setChoice(_ choice: String) {
        buttons
            .filter { $0.option == choice }
            .forEach {
            $0.isSelected = true
        }
    }
    
}
