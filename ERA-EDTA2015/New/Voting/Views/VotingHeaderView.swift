//
//  VotingHeaderView.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 1/31/18.
//  Copyright © 2018 Navus. All rights reserved.
//

class VotingHeaderView: UIView {
    
    private static let color = UIColor(hex: "231F20", andAlpha: 1)
    private static let labelFont = UIFont.systemFont(ofSize: 12.0, weight: .medium)
    
    convenience init (withHeight height: Int, title: String) {
        self.init(frame: CGRect(x: 0, y: 0, width: Int(UIScreen.main.bounds.width), height: height))
        backgroundColor = .white
        titleLabel.text = title
        //calculate xCoordinate of line view based on text width
        let xCoordinate = (title as NSString).size(withAttributes: [NSAttributedStringKey.font: VotingHeaderView.labelFont]).width + 10 + 5
        //init line view
        let lineView = UIView(frame: CGRect(x: xCoordinate, y: frame.height / 2 - 0.5, width: frame.size.width-xCoordinate, height: 1))
        lineView.backgroundColor = VotingHeaderView.color!.withAlphaComponent(0.6)
        //add subviews
        addSubview(titleLabel)
        addSubview(lineView)
    }
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel(frame: CGRect(x: 10, y: 0, width: 100, height: self.frame.height))
        label.textAlignment = .left
        label.textColor = VotingHeaderView.color
        label.font = VotingHeaderView.labelFont
        return label
    }()
    
}
