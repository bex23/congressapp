//
//  VotingSessionController.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 1/31/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RxSwift
import RxCocoa
import SnapKit

@objc class VotingSessionViewController: UIViewController {
    
    // MARK: Constants
    fileprivate static let offlineViewHeight: CGFloat = 35
    fileprivate static let activityViewHeight: CGFloat  = 40
    
    // MARK: Properties
    var viewModel: VotingSessionViewModel!
    fileprivate let disposeBag = DisposeBag()
    @IBOutlet weak var spacer: UIView!
    @IBOutlet weak var activityViewHolder: UIView!
    @IBOutlet fileprivate weak var scrollView: UIScrollView!
    @IBOutlet weak var talkContainerView: VotingPresentationView!
    @IBOutlet fileprivate weak var imgTopBanner: UIImageView!
    @IBOutlet fileprivate weak var offlineViewHeight: NSLayoutConstraint!
    @IBOutlet fileprivate weak var activityViewHeight: NSLayoutConstraint!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var doneButton: UIBarButtonItem!
    
    fileprivate let ntf = PublishSubject<UIView?>()
    
    var views: [QuestionView] = [QuestionView]() {
        didSet {
            if oldValue != views {
                oldValue.forEach { $0.removeFromSuperview() }
                views.enumerated().forEach({ (index, questionView) in
                    questionView.frame = CGRect(x: 0, y: 5000, width: 0, height: 0)
                    scrollView.addSubview(questionView)
                    questionView.snp.makeConstraints { (make) -> Void in
                        make.leading.equalTo(0)
                        make.trailing.equalTo(0)
                        make.top.equalTo(index == 0
                            ? activityViewHolder.snp.bottom
                            : views[index - 1].snp.bottom)
                    }
                })
            }
        }
    }
    
    // MARK: View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        UIView.setAnimationsEnabled(false)
        activityViewHeight.constant = 0
        activityIndicator.alpha = 0.0
        bindViewModel()
        BannersServices.shared.add(imageView: imgTopBanner,
                                   at: .topBanner,
                                   sender: self)
        NotificationCenter.default.addObserver(forName: NSNotification.Name("SrollViewNTF"), object: nil, queue: nil, using: { ntf in
            self.ntf.onNext(ntf.object as? UIView)
        })
        title = "Vote"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIView.setAnimationsEnabled(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
        if self.isMovingFromParentViewController || self.parent != nil {
            viewModel = nil
        }
    }
    
    func bindViewModel() {
        doneButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                self?.dismiss(animated: true, completion: nil)
            })
            .disposed(by: disposeBag)
        viewModel.talk.asObservable()
            .bind(to: talkContainerView.rx.talk)
            .disposed(by: disposeBag)
        viewModel.reachability
            .bind(to: rx.online)
            .disposed(by: disposeBag)
        viewModel.performingActivity
            .bind(to: rx.activity)
            .disposed(by: disposeBag)
        viewModel.questionViews.asObservable()
            .subscribe(onNext: { [weak self] (questionViews) in
                self?.views = questionViews
            })
            .disposed(by: disposeBag)
        //Show errors in alert view
        viewModel
            .errors.asObservable()
            .subscribe(onNext: { [weak self] error in
                self?.showMessage(description: error)
                return
            })
            .disposed(by: disposeBag)
        //resize or scroll
        ntf.asObservable()
            .subscribe(onNext: { _ in
                self.scrollView.contentSize.height = self.talkContainerView.height + self.views.map { $0.height }.reduce(0.0, +) + 50.0
            })
            .disposed(by: disposeBag)
    }
    
    private func showMessage(_ title: String = "error".capitalized, description: String? = nil) {
        alert(title: title, text: description)
            .subscribe(onNext: { [weak self] in
                self?.dismiss(animated: true, completion: nil)
            })
            .disposed(by: disposeBag)
    }
    
    deinit {
        viewModel = nil
    }
}

//Generation of view controller
extension VotingSessionViewController {
    
    @objc class func controller(WithViewModel viewModel: VotingSessionViewModel, inNavigation: Bool = true) -> UIViewController {
        let storyboard = UIStoryboard(name: "Voting", bundle: nil)
        guard inNavigation else {
            let controller = storyboard.instantiateViewController(withIdentifier: "VotingSessionViewController") as! VotingSessionViewController
            controller.viewModel = viewModel
            return controller
        }
        let navi = storyboard.instantiateViewController(withIdentifier: "VotingSessionViewControllerNavigation") as! UINavigationController
        let controller = navi.viewControllers[0] as! VotingSessionViewController
        controller.viewModel = viewModel
        return navi
    }
    
}

//Rx animating height change
extension Reactive where Base: VotingSessionViewController {
    
    /// Bindable sink for `progress` property
    fileprivate var online: Binder<Bool> {
        return Binder(self.base) { controller, isOnline in
            if controller.offlineViewHeight.constant == 0 && isOnline { return }
            controller.offlineViewHeight.constant = isOnline ? 0 : VotingSessionViewController.offlineViewHeight
            UIView.animate(withDuration: 0.5) {
                controller.view.layoutIfNeeded()
            }
        }
    }
    
    fileprivate var activity: Binder<Bool> {
        return Binder(self.base) { controller, isPerformingActivity in
            controller.activityViewHeight.constant = isPerformingActivity ? VotingSessionViewController.activityViewHeight : 0
            UIView.animate(withDuration: 0.5) {
                controller.activityIndicator.alpha = isPerformingActivity ? 1.0 : 0.0
                controller.view.layoutIfNeeded()
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                controller.ntf.onNext(nil)
            }
        }
    }
    
}

extension CGFloat {
    static func random() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}

extension UIColor {
    static func random() -> UIColor {
        return UIColor(red:   .random(),
                       green: .random(),
                       blue:  .random(),
                       alpha: 1.0)
    }
}
