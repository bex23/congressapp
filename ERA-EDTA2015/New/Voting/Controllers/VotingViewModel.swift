//
//  VotingViewModel.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 1/25/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RxSwift
import RxDataSources
import Action

typealias TalkSection = AnimatableSectionModel<String, Talk>

struct VotingViewModel {
    
    let activeTalks: Observable<[TalkSection]>
    let completedTalks: Observable<[TalkSection]>
    let hasNoActive: Observable<Void>
    
    lazy var selectPresentation: Action<(UINavigationController, Talk), Void> = {
        let a = Action<(UINavigationController, Talk), Void> { (navi, talk) in
            let viewModel = VotingSessionViewModel(withTalk: talk)
            let viewController = VotingSessionViewController.controller(WithViewModel: viewModel)
            navi.viewControllers[0].present(viewController, animated: true)
            return .just(Void())
        }
        return a
    }()
    
    init() {
        let at = VotingService.shared.activePresentations
            .share()
        activeTalks = at
            .map { return [TalkSection(model: "Active", items: $0)] }
        completedTalks = VotingService.shared.passedPresentations
            .map {
                return [TalkSection(model: "Passed", items: $0)]
        }
        hasNoActive = at
            .take(1)
            .filter { return $0.count == 0 }
            .map { _ in return Void() }
    }
    
}

extension Talk: IdentifiableType {
    
    public typealias Identity = Int
    
    public var identity: Identity {
        guard let id = tlkTalkId else { return 0 }
        return id.intValue
    }
    
}
