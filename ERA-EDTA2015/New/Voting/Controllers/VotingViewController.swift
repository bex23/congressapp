//
//  VotingViewController.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 1/25/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import DZNEmptyDataSet
import RxOptional

class VotingViewController: UIViewController {
    
    // MARK: Properties
    private var viewModel = VotingViewModel()
    fileprivate let disposeBag = DisposeBag()
    @IBOutlet fileprivate weak var imgTopBanner: UIImageView!
    @IBOutlet private weak var segmentContainer: UIView!
    @IBOutlet private weak var segmentedControl: UISegmentedControl!
    @IBOutlet private weak var scrollViewContainer: UIView!
    @IBOutlet private weak var hairlineView: UIView!
    fileprivate var scrollView: UIScrollView!
    private var scrollViewContentView = UIView()
    private var scrollingForward = Variable<Bool>(false)
    private var tableViews: [UITableView] { get { return [activeTableView, completedTableView] } }
    
    //MARK: Lazy properties
    fileprivate lazy var activeTableView: UITableView = {
        let tv = UITableView(frame: CGRect(x: 0, y: 0, width: 0, height: 0) , style: .plain)
        tv.tag = 0
        tv.setFrame(forIndex: 0)
        return tv
    }()
    fileprivate lazy var completedTableView: UITableView = {
        let tv = UITableView(frame: CGRect(x: 0, y: 0, width: 0, height: 0) , style: .plain)
        tv.tag = 1
        tv.setFrame(forIndex: 1)
        return tv
    }()
    fileprivate let activeDataSource = RxTableViewSectionedAnimatedDataSource<TalkSection>(configureCell: {
        dataSource, tableView, indexPath, talk in
        let cell = tableView.dequeueReusableCell(
            withIdentifier: indexPath.section > 0
                ? VotingViewController.votingPresentationCellIdentifier
                : VotingViewController.votingPresentationActiveCellIdentifier,
            for: indexPath) as! VotingPresentationCell
        cell.setUp(withTalk: talk)
        return cell
    })
    fileprivate let completedDataSource = RxTableViewSectionedAnimatedDataSource<TalkSection>(configureCell: {
        dataSource, tableView, indexPath, talk in
        let cell = tableView.dequeueReusableCell(
            withIdentifier: VotingViewController.votingPresentationCellIdentifier,
            for: indexPath) as! VotingPresentationCell
        cell.setUp(withTalk: talk)
        return cell
    })
    lazy var selectedIndex: () -> Int = { [weak self] in
        guard
            let scrollView = self?.scrollView,
            let segmentedControl = self?.segmentedControl,
            let tableViews = self?.tableViews else {
                return 0
        }
        let visibleRect = CGRect(
            x: scrollView.contentOffset.x,
            y: scrollView.contentOffset.y,
            width: scrollView.bounds.width,
            height: scrollView.bounds.height
        )
        for tv in tableViews where visibleRect.intersects(tv.frame) {
            let intersectionFrame = visibleRect.intersection(tv.frame)
            if intersectionFrame.size.width > tv.frame.width / 2.0 {
                return tv.tag
            }
        }
        return segmentedControl.selectedSegmentIndex
    }
    
    // MARK: Constants
    private static let votingPresentationActiveCellIdentifier = "VotingPresentationCellActive"
    private static let votingPresentationCellIdentifier = "VotingPresentationCell"
    fileprivate static let headerTitle = "UPCOMING"
    fileprivate static let headerHeight = 21
    
    // MARK: View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Voting"
        //scroll view setup
        hairlineView.backgroundColor = UIColor(hex: kGlobalColor, andAlpha: 1.0)
        scrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.bounces = false
        scrollViewContentView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        scrollViewContainer.addSubview(scrollView)
        scrollView.addSubview(scrollViewContentView)
        scrollView.contentSize.width = UIScreen.main.bounds.width * 2.0
        scrollViewContentView.addSubview(activeTableView)
        scrollViewContentView.addSubview(completedTableView)
        tableViews.forEach { [weak self] in
            guard let strongSelf = self else { return }
            $0.rx
                .setDelegate(strongSelf)
                .disposed(by: disposeBag)
            $0.emptyDataSetSource = strongSelf
            $0.emptyDataSetDelegate = strongSelf
            $0.register(UINib(nibName: "VotingPresentationCellActive", bundle: nil),
                        forCellReuseIdentifier: VotingViewController.votingPresentationActiveCellIdentifier)
            $0.register(UINib(nibName: "VotingPresentationCell", bundle: nil),
                        forCellReuseIdentifier: VotingViewController.votingPresentationCellIdentifier)
            $0.separatorStyle = .none
            $0.tableFooterView = UIView()
            $0.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 10))
            $0.rowHeight = UITableViewAutomaticDimension
        }
        activeDataSource.animationConfiguration = AnimationConfiguration(insertAnimation: .automatic, reloadAnimation: .automatic, deleteAnimation: .bottom)
        completedDataSource.animationConfiguration = AnimationConfiguration(insertAnimation: .automatic, reloadAnimation: .automatic, deleteAnimation: .bottom)
        bindViewModel()
        BannersServices.shared.add(imageView: imgTopBanner,
                                   at: .topBanner,
                                   sender: self)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        scrollView.frame = scrollViewContainer.bounds
        scrollView.contentSize.height = scrollViewContainer.frame.height
        scrollViewContentView.frame = CGRect(x: 0, y: 0, width: scrollView.contentSize.width, height: scrollView.contentSize.height)
        tableViews.enumerated().forEach { index, tv in
            tv.setFrame(forIndex: index)
        }
    }
    
    // MARK: Binding
    func bindViewModel() {
        //Bind data source
        viewModel.activeTalks
            .bind(to: activeTableView.rx.items(dataSource: activeDataSource))
            .disposed(by: disposeBag)
        viewModel.completedTalks
            .bind(to: completedTableView.rx.items(dataSource: completedDataSource))
            .disposed(by: disposeBag)
        viewModel.hasNoActive
            .subscribe(onNext: { [weak self] in
                guard let strongSelf = self else { return }
                strongSelf.scrollView.scrollRectToVisible(strongSelf.completedTableView.frame, animated: true)
            })
            .disposed(by: disposeBag)
        //Bind table view item selection
        tableViews.forEach { tableView in
            tableView.rx.itemSelected
                .map { indexPath -> Talk?  in
                    return try tableView.rx.model(at: indexPath)
                }                
                .filterNil()
                .map { [weak self] in
                    return (self!.navigationController!, $0)
                }
                .subscribe(viewModel.selectPresentation.inputs)
                .disposed(by: disposeBag)
        }
        //Bind UI events
        segmentedControl.rx
            .valueChanged
            .subscribe(onNext: { [weak self] in
                guard
                    let index = self?.segmentedControl.selectedSegmentIndex,
                    let tv = self?.tableViews[index] else {
                        return
                }
                self?.scrollView.scrollRectToVisible(tv.frame, animated: true)
            })
            .disposed(by: disposeBag)
        //scroll view delegation
        scrollView.rx.didEndDragging
            .filter{ !$0 }
            .subscribe(onNext: { [weak self] event in
                self?.centerSubview()
            })
            .disposed(by: disposeBag)
        
        scrollView.rx.didEndDecelerating
            .subscribe(onNext: { [weak self] in
                self?.centerSubview()
            })
            .disposed(by: disposeBag)
        //true == forward
        scrollView.rx.didScroll
            .map { [weak self] in
                self?.scrollView.contentOffset.x
            }
            .scan([0.0], accumulator: {a,val in
                return [a.last!, val]
            })
            .map { el -> Bool in
                let previous = el[0]
                let current = el[1]
                if current! - previous! >= 0 {
                    return true
                } else {
                    return false
                }
            }.bind(to: scrollingForward)
            .disposed(by: disposeBag)
        scrollView.rx.didScroll
            .map(selectedIndex)
            .bind(to: segmentedControl.rx.selectedSegmentIndex)
            .disposed(by: disposeBag)
        scrollView.rx.willBeginDecelerating
            .subscribe(onNext: { [weak self] in
                self?.flickSubview()
            })
            .disposed(by: disposeBag)
    }
    
    private func flickSubview() {
        let visibleRect = CGRect(
            x: scrollView.contentOffset.x,
            y: scrollView.contentOffset.y,
            width: scrollView.bounds.width,
            height: scrollView.bounds.height
        )
        var intersectingTVs = [UITableView]()
        for tv in tableViews where visibleRect.intersects(tv.frame) {
            intersectingTVs.append(tv)

        }
        let tv = scrollingForward.value ? intersectingTVs.last! : intersectingTVs.first!
        scrollView.scrollRectToVisible(tv.frame, animated: true)
    }
    
    private func centerSubview() {
        let visibleRect = CGRect(
            x: scrollView.contentOffset.x,
            y: scrollView.contentOffset.y,
            width: scrollView.bounds.width,
            height: scrollView.bounds.height
        )
        for tv in tableViews where visibleRect.intersects(tv.frame) {
            let intersectionFrame = visibleRect.intersection(tv.frame)
            if intersectionFrame.size.width > tv.frame.width / 2.0 {
                scrollView.scrollRectToVisible(tv.frame, animated: true)
            }
        }
    }
    
}

extension VotingViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard
            tableView == activeTableView,
            section == 1,
            activeDataSource.sectionModels[1].items.count > 0
            else { return nil }
        return VotingHeaderView(withHeight: VotingViewController.headerHeight, title: VotingViewController.headerTitle)
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard
            tableView == activeTableView,
            section == 1,
            activeDataSource.sectionModels[1].items.count > 0
            else { return 0 }
        return CGFloat(VotingViewController.headerHeight)
    }
}

extension VotingViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = "Sorry"
        let attrs = [NSAttributedStringKey.font: UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let current = scrollView == activeTableView ? "active or upcomming" : "completed"
        let str = "You don't seem to have any \(current) voting sessions."
        let attrs = [NSAttributedStringKey.font: UIFont.preferredFont(forTextStyle: UIFontTextStyle.body)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    func buttonTitle(forEmptyDataSet scrollView: UIScrollView, for state: UIControlState) -> NSAttributedString? {
        let current = scrollView == activeTableView ? "results" : "active"
        let str = "Go to \(current)"
        if #available(iOS 9.0, *) {
            let attrs = [NSAttributedStringKey.font: UIFont.preferredFont(forTextStyle: UIFontTextStyle.callout),
                         NSAttributedStringKey.foregroundColor: UIColor.white]
            return NSAttributedString(string: str, attributes: attrs)
        } else {
            let attrs = [NSAttributedStringKey.font: UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)]
            return NSAttributedString(string: str, attributes: attrs)
        }
    }
    
    func buttonBackgroundImage(forEmptyDataSet scrollView: UIScrollView!, for state: UIControlState) -> UIImage! {
        let image = UIImage(color: UIColor(hex: kGlobalColor, andAlpha: 1.0),
                            size: CGSize(width: UIScreen.main.bounds.width - 50, height: 20))!
        let capInsets = UIEdgeInsetsMake(30.0, 10.0, 30.0, 10.0)
        return image.resizableImage(withCapInsets: capInsets).withAlignmentRectInsets(.zero)
    }
    
    func emptyDataSet(_ scrollView: UIScrollView, didTap button: UIButton) {
        self.scrollView.scrollRectToVisible(scrollView == activeTableView ? completedTableView.frame : activeTableView.frame, animated: true)
    }
    
    func verticalOffset(forEmptyDataSet scrollView: UIScrollView!) -> CGFloat {
        return -20.0
    }
    
}
