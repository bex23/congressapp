//
//  VotingSessionViewModel.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 1/31/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RxSwift
import RxDataSources
import RxOptional
import Moya
import PopupKit

@objc class VotingSessionViewModel: NSObject {
    
    var activeQuestion: Variable<VotingQuestion?>
    
    private let disposeBag = DisposeBag()
    private let questions: Variable<[VotingQuestion]>
    let questionViews: Variable<[QuestionView]>
    private let _performingActivity = ActivityIndicator()
    let talk: Variable<Talk>
    let performingActivity: Observable<Bool>
    let questionSelection: PublishSubject<(VotingQuestion, String)>
    let errors: PublishSubject<String>
    let voted: PublishSubject<Void>
    let reachability: Observable<Bool>
    
    @objc init(withTalk t: Talk) {
        let _talk = Variable<Talk>(t)
        var popupView: PopupView?
        var skippedQuestions = [Int]()
        performingActivity = _performingActivity.asObservable()
        reachability = connectedToInternet()
        let _errors = PublishSubject<String>()
        let _questions = Variable<[VotingQuestion]>([])
        let _questionViews = Variable<[QuestionView]>([])
        let _voted = PublishSubject<Void>()
        let _presentationStart = VotingService.shared.hasStarted(_talk.value)
        let _presentationEnd = VotingService.shared.hasFinished(_talk.value)
        let _questionSelection = PublishSubject<(VotingQuestion, String)>()
        let _questionSkip = PublishSubject<(Int)>()
        let _activeQuestion = Variable<VotingQuestion?>(nil)
        talk = _talk
        questions = _questions
        questionViews = _questionViews
        errors = _errors
        voted = _voted
        questionSelection = _questionSelection
        activeQuestion = _activeQuestion
        //request
        let request = provider.request(.votingQuestions(presentationId: Int(truncating: t.tlkTalkId ?? 0)))
            .filterSuccessfulStatusCodes()
            .mapObject(ArrayResponse<VotingQuestion>.self)
            .map { $0.array }
            .filterNil()
            .catchServerError { _ in return .just([]) }
            .throttle(1, scheduler: MainScheduler.instance)
            .do(onNext: { (questions) in
                var active: VotingQuestion? = nil
                for question in questions {
                    if question.active {
                        active = question
                        break
                    }
                }
                _activeQuestion.value = active
            })
            .map { questions -> Void in
                let t = _talk.value
                _talk.value = t
                _questions.value = questions
                return Void()
            }
            .share()
        //initial loading
        request.take(1)
            .trackActivity(_performingActivity)
            .subscribe(onNext: {})
            .disposed(by: disposeBag)
        //timer
        let timer = Observable<Int>
            .interval(3.0, scheduler: MainScheduler.instance)
            .startWith(1)
        //reload mechanism
        _ = Observable.combineLatest(timer, reachability.startWith(true), _presentationStart, _presentationEnd)
            .take(VotingService.shared.duration(t), scheduler: MainScheduler.instance)
            .filter { $0.1 && $0.2 && !$0.3}
            .flatMap { _ in return request }
            .subscribe(onNext: {})
            .disposed(by: disposeBag)
        //reload table view on start and end events
        _ = Observable.combineLatest(_presentationStart, _presentationEnd)
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { _ in
                let t = _talk.value
                _talk.value = t
                let q = _questions.value
                _questions.value = q
            })
            .disposed(by: disposeBag)
        _ = Observable.zip(questions.asObservable(), questions.asObservable().skip(1))
            .subscribe(onNext: { previous, current in
                if current == previous {
                    _questionViews.value.enumerated().forEach {
                        $0.1.question = current[$0.0]
                    }
                } else {
                    _questionViews.value = current.enumerated()
                        .map { QuestionView.from(question: $0.1, with: $0.0, selection: _questionSelection) }
                }
            })
            .disposed(by: disposeBag)
        //vote
        questionSelection.asObservable()
            .do(onNext: { _ in
                _voted.onNext(Void())
            })
            .flatMap { return provider.request(.answerQuestion(presentationId: $0.0.presentation_id ?? 0, questionId: $0.0.id ?? 0, answer: $0.1))
                .filterSuccessfulStatusCodes()
                .catchServerError { error in
                    //show error
                    _errors.onNext(error)
                    //reload cell selection
                    let array = _questions.value
                    _questions.value = array
                    //return empty
                    return .just(Response(statusCode: 500, data: Data()))
                }
                .throttle(1, scheduler: MainScheduler.instance)
                .map {
                    return $0.statusCode == 200 ? true : false
                }
            }
            .filter { $0 }
            .do(onNext: { _ in
                if let popup = popupView {
                    popup.dismiss(animated: true)
                    popupView = nil
                }
            })
            .flatMap { _ in return request }
            .subscribe()
            .disposed(by: disposeBag)
        //track active
        _activeQuestion.asObservable()
            .do(onNext: { (question) in
                if let question = question, question.userAnswer == nil, !skippedQuestions.contains(question.id ?? 0) {
                    if popupView == nil {
                        let questionView = QuestionView.popupView(question: question,
                                                                  with: 0,
                                                                  selection: _questionSelection,
                                                                  skip: _questionSkip)
                        popupView = PopupView(contentView: questionView,
                                              showType: .bounceInFromBottom,
                                              dismissType: .bounceOutToBottom,
                                              maskType: .dimmed,
                                              shouldDismissOnBackgroundTouch: false,
                                              shouldDismissOnContentTouch: false)
                        popupView?.contentView.frame.size.height += 20.0
                        popupView?.contentView.frame.size.width = UIScreen.main.bounds.width
                        popupView?.show()
                    }
                } else {
                    popupView?.dismiss(animated: true)
                    popupView = nil
                }
            })
            .subscribe()
            .disposed(by: disposeBag)
        _questionSkip.asObservable()
            .do(onNext: { (id) in
                skippedQuestions.append(id)
            })
            .subscribe()
            .disposed(by: disposeBag)
        super.init()
    }
}
