//
//  Choice.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 2/26/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import ObjectMapper

class Choice: NSObject, Mappable {
    var title: String?
    var choice: String?
    var correct: Bool?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        title   <- map["title"]
        choice  <- map["choice"]
        correct <- map["correct"]
    }
}

class UserAnswer: NSObject, Mappable {
    var choices: [String]?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        choices <- map["choices"]
    }
}

