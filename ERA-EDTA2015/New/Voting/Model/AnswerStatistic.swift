/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import ObjectMapper
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */
class AnswerStatistic: Mappable {
	
    var choice: String?
    var votings: Int?
    var percentage: Int?
    var correct: Bool?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        choice      <- map["choice"]
        votings     <- map["votings"]
        percentage  <- map["percentage"]
        correct     <- map["correct"]
    }
    
}

extension AnswerStatistic {
    
    func image(for userAnswer: UserAnswer?, hasCorrect: Bool) -> UIImage {
        guard let choice = choice else { fatalError() }
        if let answer = userAnswer {
            if hasCorrect {
                if answer.choices?.first ?? "" == choice {
                    return UIImage(named: correct ?? false ? "results_\(choice.uppercased())_voted_right" : "results_\(choice.uppercased())_voted_wrong")!
                } else {
                    return UIImage(named: correct ?? false ? "results_\(choice.uppercased())_right" : "results_\(choice.uppercased())")!
                }
            } else {
                if userAnswer?.choices?.first ?? "" == choice {
                    return UIImage(named: "results_\(choice.uppercased())_voted")!
                }
            }
        } else {
            return UIImage(named: correct ?? false ? "results_\(choice.uppercased())_right" : "results_\(choice.uppercased())")!
        }
        return UIImage(named: "results_\(choice.uppercased())")!
    }
    
}
