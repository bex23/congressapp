/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import ObjectMapper
 
class VotingQuestion: Mappable {
	var id : Int?
	var presentation_id : Int?
	var group : String?
	var title : String?
	var order : Int?
	var duration : Int?
	var options : Options?
    var startsAt: Date?
    var endsAt: Date?
    var active : Bool = false
    var finished : Bool = false
    var userAnswer : UserAnswer?
	var statistics : [AnswerStatistic]?

    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        let dateTransformer = CongressAppDateTransformer()
		id <- map["id"]
		presentation_id <- map["presentation_id"]
		group <- map["group"]
		title <- map["title"]
		order <- map["order"]
        duration <- map["duration"]
        options <- map["options"]
        startsAt <- (map["starts_at"], dateTransformer)
        endsAt <- (map["ends_at"], dateTransformer)
        active <- map["active"]
        finished <- map["finished"]
        userAnswer <- map["user_answer"]
        statistics <- map["statistics"]
	}
    
}

extension VotingQuestion: Equatable {
    
    static func ==(lhs: VotingQuestion, rhs: VotingQuestion) -> Bool {
        return lhs.id == rhs.id
    }
    
}

extension QuestionView {
    
    override func isEqual(_ object: Any?) -> Bool {
        return tag == (object as? QuestionView)?.tag
    }
    
}

extension Array where Element: Equatable {
    
    static func ==(lhs: [Element?], rhs: [Element?]) -> Bool {
        return zip(lhs, rhs)
            .filter { $0.0 == $0.1 }
            .count == lhs.count
    }
    
}
