//
//  QuestionCell.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 2/1/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RxSwift
import RxCocoa

class QuestionCell: UITableViewCell {
    
    @IBOutlet private weak var transparentButton: UIButton!
    @IBOutlet fileprivate weak var titleLabel: UILabel!
    @IBOutlet private weak var optionsView: UIView!
    @IBOutlet private weak var footerLabel: UILabel!
    @IBOutlet fileprivate weak var bgView: UIView!
    @IBOutlet weak var stackView: UIStackView!
    
    fileprivate var question: VotingQuestion!
    fileprivate var subject: PublishSubject<(VotingQuestion, String)>!
    private var tableView: UITableView? = nil
    private lazy var border: CAShapeLayer = {
        let dashedBorder = CAShapeLayer()
        dashedBorder.strokeColor =  UIColor(hex: kGlobalColor, andAlpha: 1.0).cgColor
        dashedBorder.lineDashPattern = [5, 5]
        dashedBorder.lineWidth = 2
        dashedBorder.fillColor = nil
        return dashedBorder
    }()
    
    static fileprivate let enabledTextColor = UIColor(hex: "231F20", andAlpha: 1.0)
    static fileprivate let disabledTextColor = UIColor(hex: "939598", andAlpha: 1.0)
    static fileprivate let answeredTextColor = UIColor.white
    static fileprivate let unansweredBackgroundColor = UIColor(hex: "F3F3F3", andAlpha: 1.0)
    static fileprivate let answeredBackgroundColor = UIColor(hex: kGlobalColor, andAlpha: 1.0)
    
    private var state: QuestionState {
        get {
            if question.userAnswer != nil {
                return .answered
            } else {
                return question.active ? .active : .inactive
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        transparentButton.setTitle("", for: .normal)
        transparentButton.backgroundColor = .clear
        titleLabel.backgroundColor = .clear
        optionsView.backgroundColor = .clear
        footerLabel.backgroundColor = .clear
        footerLabel.text = "Select only one answer. Use the lecture screen as an answering reference."
        optionsView.isHidden = true
        footerLabel.isHidden = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        var bounds = bgView.bounds
        bounds.size.height = titleLabel.frame.height + 50
        border.frame = bounds
        border.path = UIBezierPath(rect: bounds).cgPath
    }
    
    func setUp(withQuestion q: VotingQuestion, selection: PublishSubject<(VotingQuestion, String)>, andTableView tv: UITableView) {
        question = q
        subject = selection
        titleLabel.text = "\(question.title ?? "N/A")"
        tableView = tv
        let options = Options()
        let buttonsView = AnswersView(containerView: optionsView, margin: 35, options: options)
        buttonsView.delegate = self
        
        border.removeFromSuperlayer()
        optionsView.isHidden = true
        footerLabel.isHidden = true
        stateSetUp()
    }

    private func stateSetUp() {
        switch state {
        case .inactive:
            bgView.backgroundColor = QuestionCell.unansweredBackgroundColor
            titleLabel.textColor = QuestionCell.disabledTextColor
            transparentButton.isUserInteractionEnabled = false
        case .active:
            bgView.backgroundColor = QuestionCell.answeredTextColor
            titleLabel.textColor = QuestionCell.enabledTextColor
            transparentButton.isUserInteractionEnabled = true
            bgView.layer.addSublayer(border)
        case .answered:
            bgView.backgroundColor = QuestionCell.answeredBackgroundColor
            titleLabel.textColor = QuestionCell.answeredTextColor
            transparentButton.isUserInteractionEnabled = false
        }
    }
    
    @IBAction func onTapTitle(_ sender: UIButton) {
        UIView.setAnimationsEnabled(false)
        optionsView.isHidden = !optionsView.isHidden
        footerLabel.isHidden = !footerLabel.isHidden
        if optionsView.isHidden {
            stateSetUp()
        } else {
            border.removeFromSuperlayer()
            bgView.backgroundColor = QuestionCell.unansweredBackgroundColor
        }
        tableView?.beginUpdates()
        tableView?.endUpdates()
        UIView.setAnimationsEnabled(true)
    }
    
}

extension QuestionCell: AnswersViewDelegate {
    
    func didSelect(option: String) {
//        question.userAnswer = option
        UIView.animate(
            withDuration: 0.2,
            animations: {
                self.bgView.backgroundColor = QuestionCell.answeredBackgroundColor
                self.titleLabel.textColor = QuestionCell.answeredTextColor
            },
            completion: { completion in
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    self.onTapTitle(UIButton())
                }
            }
        )
        subject.onNext((question, option))
    }
    
}

fileprivate enum QuestionState {
    case inactive
    case active
    case answered
}
