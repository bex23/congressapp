//
//  VotingPresentationCell.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 1/30/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import Kingfisher

class VotingPresentationCell: UITableViewCell {
    @IBOutlet private weak var timeLabel: UILabel!
    @IBOutlet private weak var roomLabel: UILabel!
    @IBOutlet private weak var speakerNameLabel: UILabel!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var speakerImageImageView: UIImageView!
    @IBOutlet private weak var ongoingImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        speakerImageImageView.layer.cornerRadius = speakerImageImageView.frame.width / 2.0
        speakerImageImageView.layer.masksToBounds = true
    }
    
    func setUp(withTalk talk: Talk) {
        titleLabel.text = talk.tlkTitle ?? "N/A"
        speakerNameLabel.text = talk.speakerName()
        roomLabel.text = talk.session?.venue?.venName ?? "N/A"
        timeLabel.text = talk.timeframe()
        speakerImageImageView.kf.setImage(with: URL(string: talk.tlkSpeakerImageUrl ?? ""), placeholder: UIImage(named: "speaker-placeholder"))
        if !VotingService.shared.isOngoing(talk) {
            if ongoingImage != nil { ongoingImage.removeFromSuperview() }
            timeLabel.font = UIFont.systemFont(ofSize: 13.0, weight: .medium)
        }
    }
    
}
