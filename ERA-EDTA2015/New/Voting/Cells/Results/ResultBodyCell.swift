//
//  ResultBodyCell.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 2/8/18.
//  Copyright © 2018 Navus. All rights reserved.
//

class ResultBodyCell: UITableViewCell {
    
    @IBOutlet private weak var answerImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var placeholderLabel: UILabel!
    @IBOutlet private weak var placeholderView: UIView!
    @IBOutlet private weak var percentageView: UIView!
    @IBOutlet private weak var percentageLabel: UILabel!
    @IBOutlet private weak var percentageWidth: NSLayoutConstraint!
    @IBOutlet private weak var placeholderLabelWidth: NSLayoutConstraint!
    @IBOutlet private weak var labelSpacing: NSLayoutConstraint!
    private var percentage: Int!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        placeholderLabel.text = ""
        placeholderLabel.backgroundColor = .clear
        placeholderView.backgroundColor = .clear
    }
    
    func setUp(
        withTitle title: String,
        resultsAttributedText: NSAttributedString,
        answerImage: UIImage,
        votesPercentage: Int,
        textWidth: CGFloat,
        isCorrectAnswer: Bool) {
        titleLabel.text = title
        answerImageView.image = answerImage
        percentageLabel.attributedText = resultsAttributedText
        placeholderLabelWidth.constant = textWidth
        percentage = votesPercentage
        labelSpacing.constant = percentage == 0 ? 0.0 : 10.0
        let fullBarWidth = UIScreen.main.bounds.width - (35 + 56 + 20 + 30 + textWidth)
        let constant = fullBarWidth * (CGFloat(percentage) / 100.0)
        percentageWidth.constant = constant
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
}

