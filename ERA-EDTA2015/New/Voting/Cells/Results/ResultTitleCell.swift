//
//  ResultsTitleCell.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 2/8/18.
//  Copyright © 2018 Navus. All rights reserved.
//

class ResultTitleCell: UITableViewCell {
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var img: UIImageView!
    func setUp(withTitle title: String?, hasResults: Bool) {
        titleLabel.text = title
        img.image = UIImage(named: hasResults ? "results_yes" : "results_no")!
    }
    
}
