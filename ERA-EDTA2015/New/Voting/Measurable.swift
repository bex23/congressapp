//
//  Measurable.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 3/8/18.
//  Copyright © 2018 Navus. All rights reserved.
//

protocol Measurable {
    var height: CGFloat { get }
}

extension VotingPresentationView: Measurable {
    
    open var height: CGFloat {
        get {
            let leftSpacing: CGFloat = 11.0 + (timeLabel.text ?? "").height(withConstrainedWidth: 200, font: timeLabel.font)
            let rightSpacing: CGFloat = ongoingImage.alpha == 0.0 ? 25.0 : 10.0
            let labelHeight: CGFloat = (titleLabel.text ?? "").height(withConstrainedWidth: UIScreen.main.bounds.width - rightSpacing - leftSpacing - 30.0,
                                                                      font: titleLabel.font)
            
            
            let margin: CGFloat = 15.0
            let margin2: CGFloat = 15.0
            let margin3: CGFloat = 19.0
            let margin4: CGFloat = 15.0
            let margin5: CGFloat = 15.0
            let titleHeight = (titleLabel.text ?? "").height(withConstrainedWidth: screenWidth - 50, font: titleLabel.font)
            let speakerNameHeight = (speakerNameLabel.text ?? "").height(withConstrainedWidth: screenWidth - 95, font: speakerNameLabel.font)
            let follow = followLabelHeight.constant
            return margin + margin2 + margin3 + margin4 + margin5 + titleHeight + speakerNameHeight + labelHeight + follow
        }
    }
    
}

extension QuestionView: Measurable {
    
    //margins
    //title /w/wo image
    //
    var height: CGFloat {
        var height: CGFloat = 30.0
        height += 45.0 + (titleLabel.text ?? "").height(withConstrainedWidth: resultsImageView.alpha == 0.0 ? screenWidth - 40.0 : screenWidth - 110.0,
                                                        font: titleLabel.font)
        if (state.active ?? false) {
            height += 56.0 + 25.0 + 25.5
            height += (selectLabel.text ?? "").height(withConstrainedWidth: screenWidth - 70.0, font: selectLabel.font)
        }
        resultViews?.forEach { height += $0.height }
        return height
    }
    
    var popupHeight: CGFloat {
        var height: CGFloat = 30.0
        let width = screenWidth - 30.0
        height += 45.0 + (titleLabel.text ?? "").height(withConstrainedWidth: resultsImageView.alpha == 0.0 ? width - 40.0 : width - 110.0,
                                                        font: titleLabel.font)
        if (state.active ?? false) {
            height += 56.0 + 25.0 + 25.5
            height += (selectLabel.text ?? "").height(withConstrainedWidth: width - 70.0, font: selectLabel.font)
        }
        resultViews?.forEach { height += $0.height }
        return height
    }

}

extension ResultView: Measurable {
    
    open var height: CGFloat {
        get {
            let labelHeight = (titleLabel.text ?? "").height(withConstrainedWidth: UIScreen.main.bounds.width - 156.0,
                                                             font: titleLabel.font)
            return (45.0 + 10.0 + labelHeight)
        }
    }
    
}

extension Measurable {
    
    var screenWidth: CGFloat {
        get {
            return UIScreen.main.bounds.width
        }
    }
    
}
