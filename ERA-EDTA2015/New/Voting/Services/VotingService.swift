//
//  VotingService.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 1/26/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import Foundation
import RxSwift
import Moya

class VotingService {
    
    static let shared = VotingService()
    private init() {}
    
    private var ongoing = Variable<[Int]>([])
    private var passed = Variable<[Int]>([])
    
    private lazy var dateFormatter: DateFormatter = {
        let tempDf = DateFormatter()
        tempDf.dateFormat = kDateFormatInDatabase
        return tempDf
    }()
    
    let disposeBag = DisposeBag()
    
    lazy var activePresentations: Observable<[Talk]> = {
        return Observable<Int>
            .interval(60.0, scheduler: MainScheduler.instance)
            .startWith(1)
            .flatMap { [weak self] _ -> Observable<[Talk]> in
                guard let instance = self else { fatalError() }
                return provider
                    .request(.activeVotings(conferenceId: UserDefaultsHelper.helper.fetch(.selectedConferenceId) as! Int))
                    .filterSuccessfulStatusCodes()
                    .mapJSON()
                    .catchServerError { e in
                        return UIViewController.topViewController().promptFor(title: "Error",
                                                                              message: e,
                                                                              cancelAction: "OK")
                            .map { _ in return [] }
                    }
                    .map { $0 as? [String: Any] }
                    .map { $0?["data"] as? [Int] }
                    .filterNil()
                    .do(onNext: { [weak self] ids in self?.ongoing.value = ids })
                    .map(instance.mapIds)
            }
    }()
    
    lazy var passedPresentations: Observable<[Talk]> = {
        return Observable<Int>
            .interval(60.0, scheduler: MainScheduler.instance)
            .startWith(1)
            .flatMap { [weak self] _ -> Observable<[Talk]> in
                guard let instance = self else { fatalError() }
                return provider
                    .request(.finishedVotings(conferenceId: UserDefaultsHelper.helper.fetch(.selectedConferenceId) as! Int))
                    .filterSuccessfulStatusCodes()
                    .mapJSON()
                    .catchServerError { e in
                        return UIViewController.topViewController().promptFor(title: "Error",
                                                                              message: e,
                                                                              cancelAction: "OK")
                            .map { _ in return [] }
                    }
                    .map { $0 as? [String: Any] }
                    .map { $0?["data"] as? [Int] }
                    .filterNil()
                    .do(onNext: { [weak self] ids in self?.passed.value = ids })
                    .map(instance.mapIds)
            }
    }()
    
    func hasStarted(_ talk: Talk) -> Observable<Bool> {
        return self.ongoing.asObservable()
            .map { $0.contains((talk.tlkTalkId ?? NSNumber(value: 0)).intValue) }
    }
    
    func hasFinished(_ talk: Talk) -> Observable<Bool> {
        return self.passed.asObservable()
            .map { $0.contains((talk.tlkTalkId ?? NSNumber(value: 0)).intValue) }
    }
    
    func isOngoing(_ talk: Talk) -> Bool {
        return ongoing.value.contains((talk.tlkTalkId ?? NSNumber(value: 0)).intValue)
    }
    
    func duration(_ talk: Talk) -> TimeInterval {
        let currentDate = Date()
        guard
            let endTime = talk.tlkEndTime,
            let endDate = self.dateFormatter.date(from: endTime),
            endDate > currentDate
            else { return 0.0 }
        let duration = endDate.timeIntervalSince(currentDate)
        return duration
    }
    
    private let mapIds: ([Int]) -> [Talk] = {
        if $0.count == 0 { return [Talk]() }
        let predicate = NSPredicate(format: "tlkTalkId IN %@", $0)
        let talks = LLDataAccessLayer.sharedInstance().fetchManagedObjects(withName: "Talk",
                                                                           predicate: predicate,
                                                                           sortDescriptors: nil,
                                                                           inMOC: LLDataAccessLayer
                                                                            .sharedInstance()
                                                                            .managedObjectContext) as! [Talk]
        return talks
    }
    
}
