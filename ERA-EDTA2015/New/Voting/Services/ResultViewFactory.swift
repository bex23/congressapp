//
//  ResultsViewFactory
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 2/8/18.
//  Copyright © 2018 Navus. All rights reserved.
//

struct ResultsViewFactory {
    
    private static let boldAttributes: [NSAttributedStringKey: Any] = [.font: UIFont.systemFont(ofSize: 14.0, weight: .semibold),
                                                               .foregroundColor: UIColor(hex: "231F20", andAlpha: 1.0)]
    private static let regularAttributes: [NSAttributedStringKey: Any] = [.font: UIFont.systemFont(ofSize: 14.0, weight: .regular),
                                                                  .foregroundColor: UIColor(hex: "231F20", andAlpha: 1.0)]
    private static func maxWidth(question: VotingQuestion) -> CGFloat {
        let max = question.statistics!.sorted(by: { (first, second) -> Bool in
            first.percentage ?? 0 > second.percentage ?? 0
        }).first
        guard let maximum = max else { return 20.0 }
        let maxPercentageWidth = ("\(maximum.percentage ?? 0)%" as NSString).size(withAttributes: regularAttributes).width
        return maxPercentageWidth
    }
    
    static func getResultViews(question: VotingQuestion) -> [ResultView] {
        var resultViews = [ResultView]()
        let titles = question.options!.choices!.sorted(by: { $0.choice! < $1.choice! }).map { $0.title! }
        let stats = question.statistics!.sorted(by: { $0.choice! < $1.choice! })
        let maxWidth = ResultsViewFactory.maxWidth(question: question)
        zip(stats, titles).forEach { stat, title in
            let resultView = UINib(nibName: "ResultView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! ResultView
            let resultStats = NSAttributedString(string: "\(stat.percentage ?? 0)%", attributes: ResultsViewFactory.regularAttributes)
            resultView.setUp(withTitle: title,
                             resultsAttributedText: resultStats,
                             answerImage: stat.image(for: question.userAnswer, hasCorrect: question.statistics?.hasCorrect ?? false),
                             votesPercentage: stat.percentage ?? 0,
                             textWidth: maxWidth)
            resultViews.append(resultView)
        }
        return resultViews
    }
    
}

extension Array where Element: AnswerStatistic {
    
    var hasCorrect: Bool {
        get {
            return self
                .filter { $0.correct ?? false }
                .count > 0
        }
    }
    
}


