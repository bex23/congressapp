//
//  FooterView.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 4/28/18.
//  Copyright © 2018 Navus. All rights reserved.
//

class FooterView: UIView {
    
    @IBOutlet private weak var container: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        container.layer.borderWidth = 1.0
        container.layer.borderColor = UIColor.init(hex: "7d7d7d", andAlpha: 1.0).cgColor
        container.layer.cornerRadius = 10.0
        container.layer.masksToBounds = true
    }
    
}
