//
//  LoaderView.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 4/26/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import UIKit

class LoaderView: UIView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        alpha = 0.7
        layer.cornerRadius = 5.0
        layer.masksToBounds = true
    }
    
}

