//
//  HeaderView.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 4/2/18.
//  Copyright © 2018 Navus. All rights reserved.
//

class SectionHeaderView: UIView {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    init() {
        super.init(frame: .zero)
        fromNib()
    }
    
    class func with(_ title: String) -> SectionHeaderView {
        let hv = SectionHeaderView().fromNib() as! SectionHeaderView
        hv.titleLabel.text = title
        return hv
    }
    
}

extension SectionHeaderView {
    
    static func selectionHeader(_ title: String) -> SectionHeaderView {
        let hv = SectionHeaderView().fromNib() as! SectionHeaderView
        hv.titleLabel.textColor = .black
        hv.titleLabel.textAlignment = .center
        hv.titleLabel.font = UIFont(name: "Avenir-Heavy", size: 14.0)
        hv.titleLabel.text = title
        return hv
    }
    
}

extension SectionHeaderView {
    
    static func settingsHeader(_ title: String) -> SectionHeaderView {
        let hv = SectionHeaderView().fromNib() as! SectionHeaderView
        hv.titleLabel.textColor = .black
        hv.titleLabel.textAlignment = .left
        hv.titleLabel.font = UIFont(name: "Avenir-Black", size: 14.0)
        hv.titleLabel.text = title
        return hv
    }
    
}

extension SectionHeaderView {
    
    @objc static func industySymposiaHeader(_ title: String) -> SectionHeaderView {
        let hv = SectionHeaderView().fromNib() as! SectionHeaderView
        hv.titleLabel.textColor = .black
        hv.titleLabel.textAlignment = .left
        hv.titleLabel.font = UIFont(name: "Avenir-Heavy", size: 14.0)
        hv.titleLabel.text = title
        return hv
    }
    
}
