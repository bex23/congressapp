//
//  LearningKitItemViewModel.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 4/12/18.
//  Copyright © 2018 Navus. All rights reserved.
//

final class LearningKitItemViewModel   {
    let title: String
    let image: UIImage
    init (with title: String, and image: UIImage) {
        self.title = title
        self.image = image
    }
}

