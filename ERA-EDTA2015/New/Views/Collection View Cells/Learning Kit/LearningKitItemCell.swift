//
//  LearningKitCell.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 4/2/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RxDataSources

class LearningKitItemCell: ReusableCollectionViewCell {
    
    @IBOutlet private weak var iv: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    
    func bind(_ viewModel: LearningKitItemViewModel) {
        self.titleLabel.text = viewModel.title
        self.iv.image = viewModel.image
    }

}
