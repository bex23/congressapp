//
//  RoundButtonCollectionViewCell.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 5/9/18.
//  Copyright © 2018 Navus. All rights reserved.
//

class RoundButtonCollectionViewCell: ReusableCollectionViewCell {

    @IBOutlet weak var borderView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        borderView.layer.cornerRadius = 20.0
        borderView.layer.borderWidth = 1.0
        borderView.layer.borderColor = UIColor.init(hex: "7d7d7d", andAlpha: 1.0).cgColor
        borderView.layer.masksToBounds = true
    }
    
}
