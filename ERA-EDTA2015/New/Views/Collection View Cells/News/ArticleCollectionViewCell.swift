//
//  NewsCell.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 4/2/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import Kingfisher

class ArticleCollectionViewCell: ReusableCollectionViewCell {
    
    @IBOutlet private weak var image: UIImageView!
    @IBOutlet private weak var titleTextView: UITextView!
    
    func bind(_ viewModel: NewsItemViewModel) {
        self.titleTextView.text = viewModel.title
        self.image.kf.setImage(with: viewModel.thumbUrl,
                               placeholder: #imageLiteral(resourceName: "placeholderNews"))
    }
    
}

