//
//  NewsViewModel.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 4/28/18.
//  Copyright © 2018 Navus. All rights reserved.
//

final class NewsItemViewModel {
    let title: String
    let thumbUrl: URL?
    init (with title: String, and thumbUrl: URL?) {
        self.title = title
        self.thumbUrl = thumbUrl
    }
}
