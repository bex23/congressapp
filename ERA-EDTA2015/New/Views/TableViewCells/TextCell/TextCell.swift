//
//  TextCell.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 6/13/18.
//  Copyright © 2018 Navus. All rights reserved.
//

class TextCell: ReusableCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet private weak var bgView: UIView!
    var color: UIColor = .white {
        didSet {
            bgView.backgroundColor = color
        }
    }

}

