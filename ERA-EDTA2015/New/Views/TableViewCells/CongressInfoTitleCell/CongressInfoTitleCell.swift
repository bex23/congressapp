//
//  CongressInfoTitleCell.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 4/30/18.
//  Copyright © 2018 Navus. All rights reserved.
//

class CongressInfoTitleCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
}

