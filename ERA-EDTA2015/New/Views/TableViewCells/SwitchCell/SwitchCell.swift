//
//  SwitchCell.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 5/4/18.
//  Copyright © 2018 Navus. All rights reserved.
//

class SwitchCell: ReusableCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var cellSwitch: UISwitch!
}
