//
//  CampaignCell.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 5/18/18.
//  Copyright © 2018 Navus. All rights reserved.
//

class CampaignCell: ReusableCell {
    
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var availabilityImageView: UIImageView!
    
    func bind(_ viewModel: CampaignViewModel) {
        countLabel.text = viewModel.order
        titleLabel.text = viewModel.title
        availabilityImageView.image = viewModel.stateImage
    }
    
}

final class CampaignViewModel {
    enum CampaignState {
        case available
        case answered
        case unavailable
        
        var image: UIImage? {
            switch self {
            case .available:
                return nil
            case .answered:
                return #imageLiteral(resourceName: "answered")
            case .unavailable:
                return #imageLiteral(resourceName: "unavailable")
            }
        }
    }
    
    let order: String
    let title: String
    let state: CampaignState
    
    var stateImage: UIImage? {
        get {
            return state.image
        }
    }
    
    init (with title: String, order: Int, and state: CampaignState) {
        self.title = title
        self.order = "\(order)."
        self.state = state
    }
}

