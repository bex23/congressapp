//
//  ConferenceCell.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 4/18/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import Kingfisher

class ConferenceCell: ReusableCell {
    
    @IBOutlet private weak var borderedView: UIView!
    @IBOutlet private weak var conferenceImage: UIImageView!
    @IBOutlet private weak var nameDateLabel: UILabel!
    @IBOutlet private weak var locationLabel: UILabel!
    @IBOutlet private weak var heightConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        heightConstraint.constant = (UIScreen.main.bounds.width - 32.0) * 0.268221574
        borderedView.layer.borderWidth = 1.0
        borderedView.layer.borderColor = UIColor(hex: "d9d9d9", andAlpha: 1.0).cgColor
        borderedView.layer.cornerRadius = 10.0
        borderedView.layer.masksToBounds = true
    }
    
    func bind(_ viewModel: ConferenceItemViewModel) {
        self.nameDateLabel.attributedText = viewModel.nameDateString
        self.locationLabel.attributedText = viewModel.locationString
        self.conferenceImage.kf.setImage(with: viewModel.conferenceImageURL)
    }
    
}
