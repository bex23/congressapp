//
//  ConferenceCellViewModel.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 4/18/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RealmSwift

final class ConferenceItemViewModel {
    var nameDateString: NSAttributedString {
        get {
            let color = UIColor(hex: conference.portal?.primaryColor, andAlpha: 1.0) ?? UIColor.init(hex: kGlobalColor, andAlpha: 1.0)!
            let name = conference.name ?? ""
            let dateString = (Date.rangeString(for: conference.startsAt ?? Date(), and: conference.endsAt ?? Date())).uppercased()
            let text = "\(name), \(dateString)"
            let sufix = "\((conference.endsAt ?? Date()).daySuffix)".uppercased()
            let attributedString = NSMutableAttributedString(string: text, attributes: [
                .font: UIFont(name: "Avenir-Heavy", size: 16.0)!,
                .foregroundColor: color
                ])
            text.enumerateSubstrings(in: text.startIndex..<text.endIndex, options: .byWords) {
                (substring, substringRange, _, _) in
                if (substring?.contains(sufix))! {
                    let range = String.Index(encodedOffset: substringRange.upperBound.encodedOffset - 2)..<substringRange.upperBound
                    attributedString.addAttribute(.font, value: UIFont(name: "Avenir-Heavy", size: 8.0)!, range: NSRange(range, in: text))
                    attributedString.addAttribute(.baselineOffset, value: 5, range: NSRange(range, in: text))
                }
            }
            return attributedString
        }
    }
    var locationString: NSAttributedString {
        get {
            let city = conference.city ?? ""
            let countriesString = try! String(contentsOfFile: Bundle.main.path(forResource: "countries", ofType: "json")!)
            let countriesDict = countriesString.convertToDictionary()!
            let countries = countriesDict["countries"] as! [[String: Any]]
            let countryId = conference.countryId == 0 ? 1 : conference.countryId
            let countryDict = countries.filter { ($0["id"] as! Int) == countryId }.first!
            let country = (countryDict["name"] as! String)
            let text = "\(city), \(country)"
            let attributedString = NSMutableAttributedString(string: text, attributes: [
                .font: UIFont(name: "Avenir-Heavy", size: 14.0)!,
                .foregroundColor: UIColor(white: 52.0 / 255.0, alpha: 1.0)
                ])
            text.enumerateSubstrings(in: text.startIndex..<text.endIndex, options: .byWords) {
                (substring, substringRange, _, _) in
                if substring == country {
                    attributedString.addAttribute(.font, value: UIFont(name: "Avenir-Medium", size: 14.0)!, range: NSRange(substringRange, in: text))
                }
            }
            return attributedString
        }
    }
    var conferenceImageURL: URL? {
        get {
            return URL(string: conference.bannerLarge ?? "")
        }
    }
    let conference: Conference
    init (with conference: Conference) {
        self.conference = conference
    }
}
