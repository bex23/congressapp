//
//  RecomendationCell.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 4/2/18.
//  Copyright © 2018 Navus. All rights reserved.
//


class RecommendationCell: ReusableCell {
    
    @IBOutlet private weak var timeLabel: UILabel!
    @IBOutlet private weak var roomLabel: UILabel!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var speakerNameLabel: UILabel!
    
    func setUp(withTalk talk: Talk) {
        titleLabel.text = talk.tlkTitle ?? "N/A"
        speakerNameLabel.text = talk.speakerName()
        roomLabel.text = talk.session?.venue?.venName ?? "N/A"
        timeLabel.text = talk.timeframe()
    }
    
}
