//
//  ArticleCell.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 5/9/18.
//  Copyright © 2018 Navus. All rights reserved.
//

class ArticleTableViewCell: ReusableCell {
    
    @IBOutlet private weak var artcleImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    
    func bind(_ viewModel: NewsItemViewModel) {
        self.titleLabel.text = viewModel.title
        self.artcleImageView.kf.setImage(with: viewModel.thumbUrl,
                               placeholder: #imageLiteral(resourceName: "placeholderNews"))
    }
    
}
