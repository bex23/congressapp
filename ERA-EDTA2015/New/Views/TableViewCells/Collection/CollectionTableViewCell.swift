//
//  CollectionTableViewCell.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 4/2/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RxSwift
import RxCocoa

class CollectionTableViewCell: ReusableCell {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var height = 100
    
    let selection = PublishSubject<IndexPath>()
    
    var collectionViewOffset: CGFloat {
        set { collectionView.contentOffset.x = newValue }
        get { return collectionView.contentOffset.x }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.register(UINib(nibName: LearningKitItemCell.reuseId, bundle: nil),
                                forCellWithReuseIdentifier: LearningKitItemCell.reuseId)
        collectionView.register(UINib(nibName: ArticleCollectionViewCell.reuseId, bundle: nil),
                                forCellWithReuseIdentifier: ArticleCollectionViewCell.reuseId)
        collectionView.register(UINib(nibName: RoundButtonCollectionViewCell.reuseId, bundle: nil),
                                forCellWithReuseIdentifier: RoundButtonCollectionViewCell.reuseId)
        collectionView.delegate = self
    }
    
}

extension CollectionTableViewCell: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: height > 100 ? 140 : 100,
                      height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if height > 100 {
            return UIEdgeInsets(top: 0,
                                left: 10,
                                bottom: 0,
                                right: 10)
        }
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        if height > 100 {
            return 10
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if height > 100 {
            return 10
        }
        return 0
    }
    
}
