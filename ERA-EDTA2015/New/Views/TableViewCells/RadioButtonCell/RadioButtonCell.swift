//
//  RadioButtonCell.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 5/19/18.
//  Copyright © 2018 Navus. All rights reserved.
//

class RadioButtonCell: ReusableCell {
    
    @IBOutlet weak var radioButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    
    
}
