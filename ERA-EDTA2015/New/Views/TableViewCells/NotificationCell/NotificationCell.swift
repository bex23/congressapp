//
//  NotificationCell.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 7/5/18.
//  Copyright © 2018 Navus. All rights reserved.
//

class NotificationCell: ReusableCell {
    
    @IBOutlet weak var typeImageView: UIImageView!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    func bind(_ viewModel: NotificationItemViewModel) {
        self.contentLabel.text = viewModel.title
        self.timeLabel.text = viewModel.ago
        self.backgroundColor = viewModel.read ? .white : UIColor(hex: "f4f4f4", andAlpha: 1.0)
    }
    
}
