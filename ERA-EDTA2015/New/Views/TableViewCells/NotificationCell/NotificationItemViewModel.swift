//
//  NotificationItemViewModel.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 7/5/18.
//  Copyright © 2018 Navus. All rights reserved.
//

final class NotificationItemViewModel   {
    let title: String
    let ago: String
    let read: Bool
    init (with title: String, ago: String, and read: Bool) {
        self.title = title
        self.ago = ago
        self.read = read
    }
}

