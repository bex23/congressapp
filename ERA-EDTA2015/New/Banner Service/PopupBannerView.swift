//
//  PopupBannerView.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 3/13/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RxSwift
import RxGesture

class PopupBannerView: UIView, UIGestureRecognizerDelegate {
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageView: UIImageView!
    @IBAction func closeAction(_ sender: Any) {
        UIView.animate(withDuration: 0.5, animations: {
            self.alpha = 0.0
        }) { (done) in
            self.removeFromSuperview()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        rx.tapGesture(configuration: { _, delegate in
            delegate.touchReceptionPolicy = .custom { [weak self] in
                return !($0.1.view == self?.imageView)
            }
        })
            .skip(1)
            .asDriverOnErrorJustComplete()
            .mapToVoid()
            .drive(onNext: closeAction(_:))
            .disposed(by: rx.disposeBag)
    }
    
}
