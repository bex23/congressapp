//
//  BannerService.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 3/13/18.
//  Copyright © 2018 Navus. All rights reserved.
//

@objc class BannerService: NSObject {
    
    private static let repetitionInterval: TimeInterval = 60 * 60 * 12
    private static let conferenceIsSponsoredKey = "sponsored"
    private static let bannerTimestampKey = "bannerTimestamp"
    
    fileprivate var view: UIView!
    private let userDefaults = UserDefaults.standard
    
    private var bannerTimestamp: TimeInterval {
        get {
            guard let fbt = userDefaults.value(forKey: BannerService.bannerTimestampKey) as? TimeInterval
                else { return 0.0 }
            return fbt
        }
        set {
            userDefaults.set(newValue, forKey: BannerService.bannerTimestampKey)
            userDefaults.synchronize()
        }
    }
    
    @objc func showBannerIfNeeded() {
        if Date().timeIntervalSince1970 - bannerTimestamp >= BannerService.repetitionInterval {
            bannerTimestamp = Date().timeIntervalSince1970
            showView(with: "Banner1")
        }
    }
    
    func showView(with imageName: String) {
        //show first banner
        let bannerView = UINib(nibName: "PopupBannerView", bundle: nil)
            .instantiate(withOwner: nil, options: nil)[0] as! PopupBannerView
        bannerView.frame = view.bounds
        bannerView.alpha = 0.0
        BannersServices.shared.add(imageView: bannerView.imageView,
                                   at: .popup,
                                   sender: UIViewController.topViewController())
        bannerView.widthConstraint.constant = UIScreen.main.bounds.width - 100.0
        bannerView.heightConstraint.constant = bannerView.widthConstraint.constant / 3.0 * 4.0
        view.addSubview(bannerView)
        view.bringSubview(toFront: bannerView)
        UIView.animate(withDuration: 0.5, animations: {
            bannerView.alpha = 1.0
        })
    }
    
}

extension BannerService {
    
    @objc class func getService(view: UIView) -> BannerService {
        let service = BannerService()
        service.view = view
        return service
    }

}
