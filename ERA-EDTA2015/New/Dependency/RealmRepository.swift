//
//  PortalBridge.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 4/19/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RealmSwift

@objc class RealmRepository: NSObject {
    
    private override init() {
        super.init()
    }
    @objc static let shared = RealmRepository()
    private let realm = try! Realm()
    
    @objc func getTerms() -> String {
        let portal = realm.objects(PortalSwift.self).first
        return portal?.termsAndConditions ?? ""
    }
    
    @objc func getSelectedConferenceId() -> String {
        let confId = UserDefaultsHelper.helper.fetch(.selectedConferenceId)
        return "\(confId ?? "")"
    }
    
    @objc func settings() -> ConferenceSettings? {
        let confId = getSelectedConferenceId()
        if confId.count == 0 { return nil }
        let settings = realm.objects(ConferenceSettings.self).filter("conference.id == \(confId)")
        if settings.count == 0 {
            //create new conference
            let conference = realm.objects(Conference.self).filter("id == \(confId)").first!
            let newSettings = ConferenceSettings()
            newSettings.conference = conference
            try! realm.write {
                realm.add(ConferenceSettings(value: newSettings), update: true)
            }
            return ConferenceSettings(value: newSettings)
        } else {
            return ConferenceSettings(value: settings.first!)
        }
    }
    
    @objc func link() -> ConferenceCoreDataLink {
        guard let settings = self.settings() else { return ConferenceCoreDataLink() }
        let link = settings.link
        return ConferenceCoreDataLink(value: link ?? ConferenceCoreDataLink())
    }
    
    @objc func persistSettings(_ settings: ConferenceSettings) {
        try! realm.write {
            realm.add(settings, update: true)
        }
    }
    
    @objc func persistLink(_ link: ConferenceCoreDataLink) {
        try! realm.write {
            realm.add(link, update: true)
        }
    }
    
    //fetching
    
    @objc func getTimelineDict() -> [[String: Any]] {
        guard let settings = self.settings() else { return [[String: Any]]() }
        let timelineArray = Array(settings.presentations)
        var timelineArrayDict = [[String: Any]]()
        timelineArray.forEach { timelineArrayDict.append($0.dict()) }
        return timelineArrayDict
    }
    
    @objc func getVoteDict() -> [[String: Any]] {
        guard let settings = self.settings() else { return [[String: Any]]() }
        let votesArray = Array(settings.votes)
        var votesArrayDict = [[String: Any]]()
        votesArray.forEach { votesArrayDict.append($0.dict()) }
        return votesArrayDict
    }
    
    //saving
    @objc func saveTimeline(presentationId: Int, bookmarked: Bool) {
        let settings = self.settings()
        try! realm.write {
            let timeline = PresentationTimeline()
            timeline.presentationId = presentationId
            timeline.bookmarked = bookmarked
            timeline.conferenceSettings = settings
            realm.add(timeline, update: true)
        }
    }
    
    @objc func saveVote(presentationId: Int, voted: Bool) {
        let settings = self.settings()
        try! realm.write {
            let vote = PresentationVote()
            vote.presentationId = presentationId
            vote.voted = voted
            vote.conferenceSettings = settings
            realm.add(vote, update: true)
        }
    }
    
    //deleting
    @objc func deleteTimeline() {
        if let settings = self.settings() {
            try! realm.write {
                realm.delete(settings.presentations)
            }
        }

    }
    
    @objc func deleteVote() {
        if let settings = self.settings() {
            try! realm.write {
                realm.delete(settings.votes)
            }
        }
    }
    
    // MARK: Data version
    @objc func setDataVersion(_ dataVersion: Int) {
        DispatchQueue.main.async { [weak self] in
            guard let instance = self else { return }
            let confId = instance.getSelectedConferenceId()
            if confId.count == 0 { return }
            guard let settings = instance.realm.objects(ConferenceSettings.self).filter("conference.id == \(confId)").first else { return }
            try! instance.realm.write {
                settings.appDataVersion = dataVersion
                instance.realm.add(settings, update: true)
            }
        }
    }
    
    @objc func dataVersion() -> Int {
        return settings()?.appDataVersion ?? 0
    }
    
    // MARK: Dl date
    @objc func setLastSync(date: Date?) {
        DispatchQueue.main.async { [weak self] in
            guard let instance = self else { return }
            let confId = instance.getSelectedConferenceId()
            if confId.count == 0 { return }
            guard let settings = instance.realm.objects(ConferenceSettings.self).filter("conference.id == \(confId)").first else { return }
            try! instance.realm.write {
                settings.lastSync = date
                instance.realm.add(settings, update: true)
            }
        }
    }
    
    @objc func lastSync() -> Date? {
        return settings()?.lastSync
    }
    
    //core data
    @objc func storePath() -> String {
        return "congressDatabase_\(getSelectedConferenceId()).sqlite"
    }
    
    var conference: Conference {
        get {
            let conference = realm
                .objects(Conference.self)
                .filter("id == \(UserDefaultsHelper.helper.fetch(.selectedConferenceId) as! Int)")
                .first!
            return conference
        }

    }
    
    @objc func leavingAppMessage() -> String {
        return "You are now leaving the official \(conference.name ?? "") part of the congress app.\n\n\(conference.name ?? "") is not responsible for any content outside of the app."
    }
    
    @objc func confName() -> String {
        return conference.name ?? ""
    }
    
    @objc func confSlug() -> String {
        return conference.slug ?? ""
    }
    
    @objc func gdpr() -> NSAttributedString? {
        return conference.portal?.gdpr?.html2AttributedString
    }
    
    @objc func confYear() -> String {
        return "\((conference.startsAt ?? Date()).year())"
    }
    
    @objc func streamingBaseURL() -> String? {
        let externalUrl = conference.portal?.externalUrl
        if let externalUrl = externalUrl {
            return externalUrl
        } else {
            return "https://e-materials.com/\(confSlug())/"
        }
    }
    
}
