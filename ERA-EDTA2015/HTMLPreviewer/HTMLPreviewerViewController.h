//
//  HTMLPreviewerViewController.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 3/7/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HTMLPreviewerDelegate <NSObject>

@optional
-(void)HTMLPreviewerCancel;
@optional
-(void)HTMLPreviewerDidClickLink:(NSString *)link;

@end

@interface HTMLPreviewerViewController : UIViewController

@property (strong, nonatomic) NSString *htmlContent;
@property (strong, nonatomic) NSString *hasCancelButton;

@property (nonatomic, assign) id <HTMLPreviewerDelegate> delegate;


@end
