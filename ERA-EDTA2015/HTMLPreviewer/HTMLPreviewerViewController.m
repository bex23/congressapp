//
//  HTMLPreviewerViewController.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 3/7/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "HTMLPreviewerViewController.h"

@interface HTMLPreviewerViewController () <UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintCancelButton;
@property (weak, nonatomic) IBOutlet UIButton *btnClose;

@end

@implementation HTMLPreviewerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    

    
    [self.textView setHTMLFromString:self.htmlContent];
    
    if ([self.hasCancelButton isEqualToString:@"YES"])
    {
        _constraintCancelButton.constant = 45;
        _btnClose.hidden = NO;
    }
    else
    {
        _constraintCancelButton.constant = 0;
        _btnClose.hidden = YES;
    }
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    [self.textView setContentOffset:CGPointZero animated:NO];
}

#pragma IBActions

- (IBAction)actionClose:(id)sender
{
    if([self.delegate respondsToSelector:@selector(HTMLPreviewerCancel)])
    {
        [self.delegate HTMLPreviewerCancel];
    }
}

#pragma mark - UITextViewDelegate

-(BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange
{
    if([self.delegate respondsToSelector:@selector(HTMLPreviewerDidClickLink:)])
    {
        [self.delegate HTMLPreviewerDidClickLink:[URL absoluteString]];
    }
    else
    {
         [CUtils showLeavingTheAppMessageWithUrl:[URL absoluteString]];
    }
    
    return NO;
}

@end
