//
//  EnterBadgeViewController.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 1/18/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "EnterBadgeViewController.h"
#import <AVFoundation/AVFoundation.h>

@interface EnterBadgeViewController ()

@property (weak, nonatomic) IBOutlet UITextField *txtBarcode;
@property (weak, nonatomic) IBOutlet UIImageView *imgPhotoCamera;
@property (weak, nonatomic) IBOutlet UIImageView *imgTopBanner;
@property (weak, nonatomic) IBOutlet UIImageView *imgBottomBanner;


@end

@implementation EnterBadgeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //Left bar button
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Later" style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Continue" style:UIBarButtonItemStylePlain target:self action:@selector(continueToEmaterialsPage)];
    
    [self setupBanners];
}

-(void)setupBanners
{
    [[BannersServices shared] addWithImageView:self.imgTopBanner
                                            at:BannerPositionTopBanner
                                   sender: self];
    [[BannersServices shared] addWithImageView:self.imgBottomBanner
                                            at:BannerPositionBottomBanner
                                   sender: self];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.imgPhotoCamera setImage:[UIImage imageNamed:@"photo_camera"]];
    
    [self.imgPhotoCamera colorItWithColor:[UIColor colorWithHex:kGlobalColor andAlpha:1.0]];
}

#pragma mark - Continue

-(void)continueToEmaterialsPage
{
    [self.txtBarcode resignFirstResponder];
    
    self.navigationItem.rightBarButtonItem.enabled = NO;
    
    [[ESyncData sharedInstance] addBadge:self.txtBarcode.text success:^(NSArray *arrayResponse) {
        NSNumber *partyId = nil;
        
        for (NSDictionary *dictParty in arrayResponse) {
            partyId = [dictParty objectForKey:@"id"];
        }
        
        [[ESyncData sharedInstance] connectAccountWithoutGetProfile:partyId success:^{
            [[ESyncData sharedInstance] getProfileWithSuccess:^(int points) {
                [[User current] addWithBadge:self.txtBarcode.text];
                [self showMessageForEpoints:points];
            } failure:^{
                self.navigationItem.rightBarButtonItem.enabled = YES;
                [self back];
            }];
        } failure:^{
            self.navigationItem.rightBarButtonItem.enabled = YES;
        }];
    } failure:^{
        self.navigationItem.rightBarButtonItem.enabled = YES;
    }];
}

-(void)showMessageForEpoints:(int)epoints
{
    NSString *message = [NSString stringWithFormat:@"You have gained access for %d presentations.", epoints];
    
    if(epoints == -1)
    {
        message = @"You have gained access for an unlimited number of presentations.";
    }
    
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@""
                                message:message
                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* btnCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action) {
                                                          self.navigationItem.rightBarButtonItem.enabled = YES;
                                                          [self back];
                                                      }];
    
    [alert addAction:btnCancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}


-(void)back
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - IBActions

- (IBAction)actionScanBarcode:(UITapGestureRecognizer *)sender
{
    [self.imgPhotoCamera colorItWithColor:[UIColor colorWithHex:kOtherColor andAlpha:1.0]];
    
    NSString *mediaType = AVMediaTypeVideo;
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:mediaType];
    
    if(authStatus == AVAuthorizationStatusAuthorized)
    {
        [self.imgPhotoCamera colorItWithColor:[UIColor colorWithHex:kGlobalColor andAlpha:1.0]];
        [[BarcodeScannerProxy shared] showBarcodeWithCallback:^(NSString *code) {
            self.txtBarcode.text = code;
        }];
    }
    else if(authStatus == AVAuthorizationStatusDenied)
    {
        // denied
        [self.imgPhotoCamera colorItWithColor:[UIColor colorWithHex:kGlobalColor andAlpha:1.0]];
        [self showMessageAllowCameraAccess];
    }
    else if(authStatus == AVAuthorizationStatusRestricted)
    {
        // restricted, normally won't happen
        [self.imgPhotoCamera colorItWithColor:[UIColor colorWithHex:kGlobalColor andAlpha:1.0]];
        [self showMessageAllowCameraAccess];
    }
    else if(authStatus == AVAuthorizationStatusNotDetermined)
    {
        [self.imgPhotoCamera colorItWithColor:[UIColor colorWithHex:kGlobalColor andAlpha:1.0]];
        
        // not determined?!
        [AVCaptureDevice requestAccessForMediaType:mediaType completionHandler:^(BOOL granted) {
            
            if(granted){
                // Permission has been granted. Use dispatch_async for any UI updating
                // code because this block may be executed in a thread.
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[BarcodeScannerProxy shared] showBarcodeWithCallback:^(NSString *code) {
                        self.txtBarcode.text = code;
                    }];
                });
                
                NSLog(@"Granted access to %@", mediaType);
            } else {
                NSLog(@"Not granted access to %@", mediaType);
            }
        }];
    }
    else
    {
        // impossible, unknown authorization status
    }
}

-(void)showMessageAllowCameraAccess
{
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@"IMPORTANT"
                                message:@"Please allow camera access for barcode scanning"
                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *btnEnter = [UIAlertAction
                               actionWithTitle:@"Settings"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                               }];
    
    UIAlertAction* btnCancel = [UIAlertAction
                                actionWithTitle:@"Cancel"
                                style:UIAlertActionStyleDefault
                                handler:nil];
    
    [alert addAction:btnCancel];
    [alert addAction:btnEnter];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

@end
