//
//  HomeCollectionViewCell.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 26.1.16..
//  Copyright © 2016. Navus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (strong, nonatomic) NSString *imgName;

@end
