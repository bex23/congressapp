//
//  HomeCollectionViewCell.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 26.1.16..
//  Copyright © 2016. Navus. All rights reserved.
//

#import "HomeCollectionViewCell.h"

@implementation HomeCollectionViewCell

-(void)setHighlighted:(BOOL)highlighted
{
    if (highlighted)
    {
        UIImage *imgPressed = [UIImage imageNamed:[NSString stringWithFormat:@"%@_pressed", self.imgName]];
        if (imgPressed)
        {
            self.image.image = imgPressed;
        }
    }
    else
    {
        self.image.image = [UIImage imageNamed:self.imgName];
    }
}

@end
