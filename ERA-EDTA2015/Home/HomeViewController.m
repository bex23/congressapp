//
//  HomeViewController1.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 26.1.16..
//  Copyright © 2016. Navus. All rights reserved.
//

#import "HomeViewController.h"

#import "HomeCollectionViewCell.h"
#import "TermsAndConditionsViewController.h"
#import "PSSinglePickerViewController.h"
#import "HTMLPreviewerViewController.h"
#import "AboutViewController.h"
#import "MKNumberBadgeView.h"

static NSString *const kSignOut = @"Sign out";
static NSString *const kSignIn = @"Sign in";
static NSString *const kReceivePushNotifications = @"Receive push notifications";

@interface HomeViewController () <TermsAndConditionsViewControllerDelegate, PSSinglePickerViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (strong, nonatomic) NSArray *arrayImages;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UILabel *lblDisclaimer;

@property (weak, nonatomic) IBOutlet UINavigationItem *navBarItem;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *barButtonCongressNameYear;
@property (strong, nonatomic) UIBarButtonItem *barButtonSettings;
@property (strong, nonatomic) UIBarButtonItem *barButtonNotifiactionCenter;

@property (nonatomic, retain) WYPopoverController *selectPopoverController;

@property (weak, nonatomic) IBOutlet UIView *viewLoader;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activator;
@property (weak, nonatomic) IBOutlet UIImageView *imgBottomBanner;

@property (strong, nonatomic) IBOutlet NSDictionary *dictBottomBanner;

@property (weak, nonatomic) IBOutlet FLAnimatedImageView *imgLaunch;

@property (strong, nonatomic) MKNumberBadgeView *badge;
@property (strong, nonatomic) BannerService *bannerService;

@end

@implementation HomeViewController

- (BannerService *)bannerService {
    if (!_bannerService) {
        _bannerService = [BannerService getServiceWithView:self.view];
    }
    return _bannerService;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupAppAppearance];
    
    [self setupNavigationBar];
    
    [self setupLaunchGIFImage];
    
    self.lblDisclaimer.text = kSponsorName.length > 0 ? [NSString stringWithFormat:@"This service is sponsored by %@. %@ is not responsible for the content of this service tool and any E-materials requested and accepts no liability for any information obtained.", kSponsorName, kSponsorName] : @"";
    
    self.arrayImages = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"HomeScreenConfig" ofType:@"plist"]];
    
    [self setNeedsStatusBarAppearanceUpdate];

    if ([[RealmRepository shared] lastSync] == nil) {
        // delete in case it didnt remove
        [[NSNotificationCenter defaultCenter] removeObserver:self name:kSyncFinished object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(activatorStop:) name:kSyncFinished object:nil];
        
        [[ESyncData sharedInstance] downloadZipFile];
        
        [self activatorStart];
    }
    else if(![[NSUserDefaults standardUserDefaults] valueForKey:kTermsAccepted])
    {
        NSString *strID = NSStringFromClass([TermsAndConditionsViewController class]);
        TermsAndConditionsViewController* vc = [[UIStoryboard storyboardWithName:strID bundle:nil] instantiateViewControllerWithIdentifier:strID];
        vc.pdfNameNoExtension = @"AppTC";
        vc.delegate = self;
        [self presentViewController:vc animated:NO completion:nil];
    }
    else
    {
        // delete in case it didnt remove
        [[NSNotificationCenter defaultCenter] removeObserver:self name:kSyncFinished object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(activatorStop:) name:kSyncFinished object:nil];
        
        [[ESyncData sharedInstance] syncWithServer];
        
        [self activatorStart];
    }
}

-(void)setupLaunchGIFImage
{
    NSURL *url = nil;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        url = [[NSBundle mainBundle] URLForResource:@"LaunchGIFImage_iPhone" withExtension:@"gif"];
    }
    else
    {
        url = [[NSBundle mainBundle] URLForResource:@"LaunchGIFImage_iPad" withExtension:@"gif"];
    }
    
    if (url)
    {
        FLAnimatedImage *image = [FLAnimatedImage animatedImageWithGIFData:[NSData dataWithContentsOfURL:url]];
        self.imgLaunch.animatedImage = image;
        self.imgLaunch.loopCompletionBlock = ^(NSUInteger loopCountRemaining) {
            [self.imgLaunch stopAnimating];
        };
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // clear navigatio bar color
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    
    // change UINavigationBar text color
    //    [self.navigationController.navigationBar setTintColor:[UIColor colorWithHex:kGlobalColor andAlpha:1.0]];
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, [UIFont systemFontOfSize:18], NSFontAttributeName, nil]];
    [[BannersServices shared] addWithImageView:self.imgBottomBanner
                                            at:BannerPositionBottomBanner
                                   sender: self];
    if ([[NSDate date] timeIntervalSinceDate:[[NSUserDefaults standardUserDefaults] valueForKey:kDateForSyncOnHomePage]] > 10*60
        && [[NSUserDefaults standardUserDefaults] valueForKey:kTypeOfInternetConnection]
        && self.viewLoader.isHidden) // in case sync has started in viewDidLoad
    {
        [[ESyncData sharedInstance] syncWithServerRepeatedly];
        
        [[NSUserDefaults standardUserDefaults] setValue:[NSDate date] forKey:kDateForSyncOnHomePage];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kPushNotificationReceived object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setupNotificationBadge) name:kPushNotificationReceived object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUIUserNotificationTypeChanged object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setupRightBarButtonItems) name:kUIUserNotificationTypeChanged object:nil];
    
    [self setupRightBarButtonItems];
}

-(void)viewDidAppear:(BOOL)animated {
    if (self.viewLoader.isHidden) {
        [self.bannerService showBannerIfNeeded];
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    // take back navigatio bar color
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = nil;
    self.navigationController.navigationBar.translucent = NO;
    
    // change UINavigationBar text color
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, [UIFont systemFontOfSize:18], NSFontAttributeName, nil]];
    
    [self.badge removeFromSuperview];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kPushNotificationReceived object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUIUserNotificationTypeChanged object:nil];
}

-(void)setupNavigationBar
{
    self.barButtonNotifiactionCenter = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"bell"] style:UIBarButtonItemStylePlain target:self action:@selector(actionOpenNotificationCenter)];
    self.barButtonSettings = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"settings"] style:UIBarButtonItemStylePlain target:self action:@selector(actionOpenSettings)];
    
    
    self.barButtonCongressNameYear.title = [NSString stringWithFormat:@"%@ %@",
                                            [[RealmRepository shared] confName],
                                            [[RealmRepository shared] confYear]];
}

-(void)setupRightBarButtonItems
{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIBarButtonItem *exit = [[UIBarButtonItem alloc] initWithTitle:@"Exit" style:UIBarButtonItemStylePlain target:self action:@selector(onExit)];
        // if push notifications turned
        if (![[UIApplication sharedApplication] currentUserNotificationSettings].types)
        {
            self.navBarItem.rightBarButtonItems = @[exit, self.barButtonSettings];
            [self.badge removeFromSuperview];
        }
        else
        {
            self.navBarItem.rightBarButtonItems = @[exit, self.barButtonSettings, self.barButtonNotifiactionCenter];
            
            [self setupNotificationBadge];
        }
    });
}

-(void)setupNotificationBadge
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.badge)
        {
            [self.badge removeFromSuperview];
        }

        // if push notifications turned
        if ([[UIApplication sharedApplication] currentUserNotificationSettings].types)
        {
            UIView* notCenterView = [self.barButtonNotifiactionCenter valueForKey:@"view"];
            CGRect barFrame = [notCenterView convertRect:notCenterView.bounds toView:nil];

            if (notCenterView && barFrame.origin.x != 0 && self == [self.navigationController.viewControllers objectAtIndex:0])
            {
                NSInteger value = 0;//[PushNotification countUnreadNotifications];
                CGFloat badgeWiedt = (value < 10) ? 22 : (value < 100) ? 25 : 30;
                self.badge = [[MKNumberBadgeView alloc]initWithFrame:CGRectMake(barFrame.origin.x + barFrame.size.width / 2, 2, badgeWiedt, 22)];
                self.badge.hideWhenZero = YES;
                self.badge.shadow = NO;
                self.badge.font = [UIFont systemFontOfSize:11];
                self.badge.strokeWidth = 0;
                self.badge.value = value;
                [self.navigationController.navigationBar addSubview:self.badge];
            }
        }
    });
}

#pragma mark - UICollectionView data source

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.arrayImages.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    HomeCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    NSDictionary *item = [self.arrayImages objectAtIndex:indexPath.row];
    cell.image.image = [UIImage imageNamed:item[@"id"]];
    cell.imgName = item[@"id"];
    
    return cell;
}

#pragma mark - UICollectionView delegate

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary *item = [NSMutableDictionary dictionaryWithDictionary: [self.arrayImages objectAtIndex:indexPath.row]];
    
    if (![self checkItem:item])
    {
        return;
    }
    
    if(!item[@"options"])
    {
        [self openViewControllerForItem:item];
    }
    else if (item[@"options"])
    {
        if (item[@"options"][@"alertcontroller"])
        {
            [item setObject:indexPath forKey:@"indexPath"];
            [self openActionControllerForItem:item];
        }
    }
}

#pragma mark - Showing messages

-(void)showNotAvailableYetMessage
{
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:nil
                                message:@"Not yet available."
                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* btnCancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:btnCancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)showRequiredBadgeMessage
{
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:nil
                                message:@"To gain access to E-materials you need to insert the badge number."
                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* btnInsert = [UIAlertAction actionWithTitle:@"Insert"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action) {
                                                          UINavigationController *nav = [[UIStoryboard storyboardWithName:@"EnterBadgeViewController" bundle:nil] instantiateViewControllerWithIdentifier:@"navEnterBadgeViewController"];
                                                          [self.navigationController presentViewController:nav animated:YES completion:nil];
                                                      }];
    
    UIAlertAction* btnCancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
    
    [alert addAction:btnCancel];
    [alert addAction:btnInsert];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)showLeavingTheAppMessageForItem:(NSDictionary *)item
{
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@"Info"
                                message:[[RealmRepository shared] leavingAppMessage]
                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* btnClose = [UIAlertAction
                               actionWithTitle:@"Close"
                               style:UIAlertActionStyleDefault
                               handler:nil];
    
    UIAlertAction* btnContinue = [UIAlertAction
                                  actionWithTitle:@"Continue"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action)
                                  {
                                      [self openViewControllerForItem:item];
                                  }];
    [alert addAction:btnClose];
    [alert addAction:btnContinue];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)showSignOutConfirmationMessage
{
    
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:nil
                                message:@"Do you really want to sign out?"
                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* btnNO = [UIAlertAction
                            actionWithTitle:@"NO"
                            style:UIAlertActionStyleDefault
                            handler:nil];
    
    UIAlertAction* btnYES = [UIAlertAction
                             actionWithTitle:@"YES"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [self signOut];
                             }];
    [alert addAction:btnNO];
    [alert addAction:btnYES];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - UICollectionViewFlowLayout delegate

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewFlowLayout *flow = (UICollectionViewFlowLayout*) collectionView.collectionViewLayout;
    
    CGFloat width = (collectionView.frame.size.width - (flow.sectionInset.left + flow.sectionInset.right)) / 3;
    CGFloat height = (collectionView.frame.size.height - (flow.sectionInset.top + flow.sectionInset.bottom + (3-1)*flow.minimumLineSpacing)) / 3;
    
    return CGSizeMake(width, height);
}

#pragma mark - Global functions

-(void)openViewControllerForItem:(NSDictionary *)item
{
    if (item[@"html-previewer"])
    {
        NSString *strID = NSStringFromClass([HTMLPreviewerViewController class]);
        HTMLPreviewerViewController *vc = [[UIStoryboard storyboardWithName:strID bundle:nil] instantiateViewControllerWithIdentifier:strID];
        
        vc.htmlContent = item[@"html-previewer"];
        if ([item objectForKey:@"title"])
        {
            vc.title = [item objectForKey:@"title"];
        }
        
        [self.navigationController pushViewController:vc animated:YES];
    }
    else if (item[@"webview"])
    {
        NSString *strID = NSStringFromClass([WebViewController class]);
        WebViewController *vc = [[UIStoryboard storyboardWithName:strID bundle:nil] instantiateViewControllerWithIdentifier:strID];
        vc.url = [item objectForKey:@"webview"];
        vc.title = [item objectForKey:@"title"];
        vc.hasRefreshButton = YES;
        
        [self.navigationController pushViewController:vc animated:YES];
    }
    else if (item[@"safari"])
    {
        NSURL *url = [NSURL URLWithString:[item objectForKey:@"safari"]];
        
        if (![[UIApplication sharedApplication] openURL:url]) {
            NSLog(@"%@%@",@"Failed to open url:",[url description]);
        }
    }
    else
    {
        NSString *strID = item[@"id"];
        
        UIViewController *vc = [[UIStoryboard storyboardWithName:strID bundle:nil] instantiateViewControllerWithIdentifier:strID];
        
        if ([item objectForKey:@"title"])
        {
            vc.title = [item objectForKey:@"title"];
        }
        
        [self.navigationController pushViewController:vc animated:YES];
    }
}

-(void)openActionControllerForItem:(NSDictionary *)item
{
    UIAlertControllerStyle style = [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone ? UIAlertControllerStyleActionSheet : UIAlertControllerStyleAlert;
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:item[@"title"]
                                                                   message:nil
                                                            preferredStyle:style];
    
    NSArray *acItems = item[@"options"][@"alertcontroller"];
    for (NSDictionary *item in acItems)
    {
        NSString *title = item[@"title"];
        
        if ([item[@"id"] isEqualToString:@"Sponsored"])
        {
            title = [NSString stringWithFormat:@"%@ Sponsored Services", kSponsorName];
        }
        
        UIAlertAction *alertAction  = [UIAlertAction actionWithTitle:title style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * action) {
                                                                 
                                                                 if (![self checkItem:item])
                                                                 {
                                                                     return;
                                                                 }
                                                                 
                                                                 [self openViewControllerForItem:item];
                                                             }];
        [alert addAction:alertAction];
    }
    
    if (alert.popoverPresentationController)
    {
        alert.popoverPresentationController.sourceView = [self.collectionView cellForItemAtIndexPath:[item objectForKey:@"indexPath"]];
        alert.popoverPresentationController.sourceRect = [self.collectionView cellForItemAtIndexPath:[item objectForKey:@"indexPath"]].bounds;
        alert.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionAny;
        alert.popoverPresentationController.backgroundColor = [UIColor whiteColor];
    }
    else
    {
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                                              handler:nil];
        
        [alert addAction:defaultAction];
    }
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(BOOL)checkItem:(NSDictionary *)item
{
    if ([item[@"available"] isEqual:@0])
    {
        [CUtils showNotAvailableYetMessage];
        return NO;
    }
    else if ([CUtils checkIfCongressEnd] == YES
             && [item[@"id"] isEqualToString:@"MeetingsOnTheGo"])
    {
        [Utils showMessage:@"Congress has now finished and this module is no longer available. See you next year in Copenhagen."];
        return NO;
    }
    else if ([item[@"requiredLogin"] isEqual:@YES] && ![User current])
    {
        [CUtils showLoginQuestion];
        return NO;
    }
    else if (item[@"requiredBadge"] && ![User current].hasBadge) // it is important to requiredLogin is after requiredBadge
    {
        [self showRequiredBadgeMessage];
        return NO;
    }
    else if (item[@"messageLeavingTheApp"])
    {
        [self showLeavingTheAppMessageForItem:item];
        return NO;
    }
    
    return YES;
}

#pragma mark- TermsAndConditionsViewControllerDelegate methods

-(void)termsAndConditionsVC_didCancelTC:(TermsAndConditionsViewController *)sender
{}

-(void)termsAndConditionsVC_didAcceptTC:(TermsAndConditionsViewController *)sender
{
    [[NSUserDefaults standardUserDefaults]setValue:@"YES" forKey:kTermsAccepted];
}

#pragma mark - Actions

- (void)actionOpenNotificationCenter
{
//    NotificationCenterTableViewController *notCenter = [[NotificationCenterTableViewController alloc] initWithNibName:@"NotificationCenterTableViewController" bundle:nil];
//    [self.navigationController pushViewController:notCenter animated:YES];
}

- (void)actionOpenSettings
{
    [self.view setUserInteractionEnabled:NO];
    
    UIView *viewBarButtonItem = [self.barButtonSettings valueForKey:@"view"];
    
    // make array for singe picker view controller
    NSArray *arrayForSinglePicker = [NSArray array];
    if ([User current]) // if user loged in
    {
        arrayForSinglePicker = @[@{@"image" : @"sign_out_in", @"text": kSignOut}, @{@"image" : @"bell", @"text": kReceivePushNotifications}, @{@"image" : @"more-info", @"text": @"About"}];
    }
    else
    {
        arrayForSinglePicker = @[@{@"image" : @"sign_out_in", @"text": kSignIn}, @{@"image" : @"bell", @"text": kReceivePushNotifications}, @{@"image" : @"more-info", @"text": @"About"}];
    }
    
    PSSinglePickerViewController *spVC = [[PSSinglePickerViewController alloc] initWithItems:arrayForSinglePicker DisplayMember:@"text"];
    
    spVC.delegate = self;
    spVC.preferredContentSize = CGSizeMake(270, arrayForSinglePicker.count * 50);
    spVC.popovertag = @"settingsPopover";
    
    spVC.disableTableViewScrollable = YES;
    spVC.disableTableViewHasSeparator = YES;
    spVC.disableTableViewCellSelectionStyle = YES;
    spVC.tableViewTintColor = [UIColor colorWithHex:kOtherColor andAlpha:1.0];
    
    spVC.selectedRow = -1;
    
    spVC.textColor = [UIColor whiteColor];
    spVC.iconColor = [UIColor whiteColor];
    spVC.backgroudColor = [UIColor colorWithHex:kGlobalColor andAlpha:1.0];
    
    spVC.arrayDisabledCheckmarksForIndexs = @[@0]; // disable selection of sign out and sign in item
    
    spVC.imageKey = @"image";
    
    spVC.arrayDisabledCheckmarksForIndexs = @[@0, @2];
    
    // if turn on notification -> select
    if ([[UIApplication sharedApplication] currentUserNotificationSettings].types)
    {
        spVC.selectedRow = 1;
    }
    else
    {
        spVC.arrayDisabledCheckmarksForIndexs = @[@0, @1, @2];
    }
    
    // disable selection of sign out and sign in item + ReceivePushNotifications item
    
    self.selectPopoverController = [[WYPopoverController alloc] initWithContentViewController:spVC];
    self.selectPopoverController.theme.fillTopColor = [UIColor colorWithHex:kGlobalColor andAlpha:1.0];
    
    [self.selectPopoverController presentPopoverFromRect:viewBarButtonItem.bounds
                                                  inView:viewBarButtonItem
                                permittedArrowDirections:WYPopoverArrowDirectionAny
                                                animated:YES
                                              completion:^{
                                                  [self.view setUserInteractionEnabled:YES];
                                              }];
}

#pragma mark - PSSinglePickerViewDelegate

-(void)SinglePickerItemSelected:(id)item Sender:(id)sender
{
    [self.selectPopoverController dismissPopoverAnimated:YES completion:^{
        
        PSSinglePickerViewController *spVC = (PSSinglePickerViewController *)sender;
        
        if ([spVC.popovertag isEqualToString:@"settingsPopover"])
        {
            NSDictionary *dictItem = (NSDictionary *)item;
            if ([[dictItem objectForKey:@"text"] isEqualToString:kSignIn]) // Sign in
            {
                UINavigationController *nav = [[UIStoryboard storyboardWithName:@"LoginRegistration" bundle:nil] instantiateViewControllerWithIdentifier:@"navLogin"];
                [self.navigationController presentViewController:nav animated:YES completion:nil];
            }
            else if ([[dictItem objectForKey:@"text"] isEqualToString:kSignOut]) // Sign out
            {
                [self showSignOutConfirmationMessage];
            }
            else if ([[dictItem objectForKey:@"text"] isEqualToString:kReceivePushNotifications]) // Receive push notifications
            {
                if (kEnableRemoteNotifications == 1)
                {
                    NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                    [[UIApplication sharedApplication] openURL:url];
                }
                else
                {
                    [CUtils showNotAvailableYetMessage];
                }
            }
            else if ([[dictItem objectForKey:@"text"] isEqualToString:@"About"])
            {
                AboutViewController *vc = [[AboutViewController alloc] initWithNibName:NSStringFromClass([AboutViewController class]) bundle:nil];
                
                [self.navigationController pushViewController:vc animated:YES];
            }
        }
    }];
}

-(void)signOut
{
    [[LLDataAccessLayer sharedInstance] resetUserData];
    [[ESyncData sharedInstance] setAuthorizationHeaderWithToken:nil];
    [[ESyncData sharedInstance] resetLoginProperties];
}

#pragma mark - Activator

-(void)activatorStart
{
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    
    [self.viewLoader setHidden:NO];
    self.view.userInteractionEnabled = NO;
    
    [self.navigationController.navigationBar setHidden:YES];
    
    [self.activator startAnimating];
}

-(void)activatorStop:(NSNotification *)notification
{
    NSLog(@"%@ ", notification.name);
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:notification.name object:nil];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        //Your main thread code goes in here
        NSLog(@"Im on the main thread");
        
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
        
        [self.viewLoader setHidden:YES];
        self.view.userInteractionEnabled = YES;
        
        [self.navigationController.navigationBar setHidden:NO];
        
        [self.activator stopAnimating];
        
        // change statusBarStyle to light
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
        
        if(![[NSUserDefaults standardUserDefaults]valueForKey:kTermsAccepted])
        {
            NSString *strID = NSStringFromClass([TermsAndConditionsViewController class]);
            TermsAndConditionsViewController* vc = [[UIStoryboard storyboardWithName:strID bundle:nil] instantiateViewControllerWithIdentifier:strID];
            vc.pdfNameNoExtension = @"AppTC";
            vc.delegate = self;
            [self.navigationController presentViewController:vc animated:NO completion:nil];
        } else {
            if (self.viewLoader.isHidden) {
                [self.bannerService showBannerIfNeeded];
            }
        }
    });
}

- (void)onExit {
    UIApplication.sharedApplication.delegate.window.rootViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ConferenceSelectionViewController"];
}

-(void)setupAppAppearance
{
    // change UINavigationBar background color
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithHex:kGlobalColor andAlpha:1.0]];
    [[UINavigationBar appearance] setTranslucent:NO];
    
    // change UINavigationBar text color
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, [UIFont systemFontOfSize:18], NSFontAttributeName, nil]];
    
    [[UIToolbar appearance] setTintColor:[UIColor colorWithHex:kGlobalColor andAlpha:1.0]];
    
    // Set tint color of all buttons
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [appDelegate.window setTintColor:[UIColor colorWithHex:kGlobalColor andAlpha:1.0]];
    

    // change statusBarStyle to light
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    // change UISegmentedControl text and background color
    [[UISegmentedControl appearance] setTintColor:[UIColor whiteColor]];
    [[UISegmentedControl appearance] setBackgroundColor:[UIColor colorWithHex:kGlobalColor andAlpha:1.0]];
    
    // UIPageControl
    [[UIPageControl appearance] setCurrentPageIndicatorTintColor:[UIColor colorWithHex:kGlobalColor andAlpha:1.0]];
    [[UIPageControl appearance] setPageIndicatorTintColor:[UIColor whiteColor]];
    
    // UITableView - hide table's separators if no data found
    [[UITableView appearance] setTableFooterView:[[UIView alloc] init]];
    
    // UISwitch - change on tint color
    [[UISwitch appearance] setOnTintColor:[UIColor colorWithHex:kOtherColor andAlpha:1.0]];
}

@end
