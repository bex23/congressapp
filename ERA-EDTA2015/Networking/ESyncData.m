//
//  ESyncData.m
//  Ematerials
//
//  Created by admin on 12/20/13.
//  Copyright (c) 2013 Navus. All rights reserved.
//

#import "ESyncData.h"

#import "SSZipArchive.h"
#import "UIDeviceHardware.h"

@interface ESyncData ()

// Sync with server properties
// It needs for app testing
@property (nonatomic) int numOfRequests; // + -> -
@property (nonatomic) int numOfSaved; // - -> +
@property (nonatomic) int numOfLinked; // - -> +
@property (nonatomic) int numOfSavedTalksAndSpeakers;
@property (nonatomic) int numOfUnsyncTimelineUploaded; // + -> -
@property (nonatomic) int numOfUnsyncVote; // + -> -

@property (strong, nonatomic) NSArray *arrayConferenceSpeakers;

@end

@implementation ESyncData

+ (ESyncData *)sharedInstance
{
    __strong static ESyncData *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[ESyncData alloc] init];
    });
    return sharedInstance;
}

#pragma mark - Sync with Server

-(void)syncWithServerRepeatedly // on 10 minutes
{
    NSLog(@"Call function %s",  __FUNCTION__);
    
    
    NSString *url = [NSString stringWithFormat:@"devices/ios/%@?&conference_id=%@",
                     [Utils getDevicUUID],
                     [[RealmRepository shared] getSelectedConferenceId]];
    [[EWebServiceClient sharedInstance] sendRequestWithPath:url
                                                     method:@"GET"
                                                     params:nil
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         NSError *error = nil;
         NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:successResponseData options:NSJSONReadingMutableContainers error:&error];
         
         NSInteger statusCode = operation.response.statusCode;
         
         // 200
         // update
         // finished - sync finished YES - notification
         if (statusCode == 200)
         {
             NSDictionary *data = [jsonData objectForKey:@"data"];
             [self updateJsonData:data];
         }
         else if (statusCode == 205) // get all data -> download first zip file
         {
             [[RealmRepository shared] setLastSyncWithDate:nil];
             [self downloadZipFile];
         }
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         // 304
         // there isnt update
         NSInteger statusCode = failureResponseData.response.statusCode;

         if (statusCode == 304)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 if ([User current]) [self syncUserData:NO];
             });
         }
     }];
}

-(void)syncWithServer
{
    NSLog(@"Call function %s",  __FUNCTION__);
    
    NSString *url = [NSString stringWithFormat:@"devices/ios/%@?&conference_id=%@",
                     [Utils getDevicUUID],
                     [[RealmRepository shared] getSelectedConferenceId]];
    [[EWebServiceClient sharedInstance] sendRequestWithPath:url
                                                     method:@"GET"
                                                     params:nil
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         NSError *error = nil;
         NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:successResponseData options:NSJSONReadingMutableContainers error:&error];
         
         NSInteger statusCode = operation.response.statusCode;
         
         // 200
         // update
         // finished - sync finished YES - notification
         if (statusCode == 200)
         {
             NSDictionary *data = [jsonData objectForKey:@"data"];
             [self updateJsonData:data];
             
             [[NSNotificationCenter defaultCenter] postNotificationName:kSyncFinished object:self userInfo:nil];
         }
         else if (statusCode == 205) // get all data -> download first zip file
         {
             [[RealmRepository shared] setLastSyncWithDate:nil];
             [self downloadZipFile];
         }
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         // 304
         // there isnt update
         NSInteger statusCode = failureResponseData.response.statusCode;
         
         if (statusCode == 304)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 if ([User current]) [self syncUserData:NO];
             });
         }
         
         [[NSNotificationCenter defaultCenter] postNotificationName:kSyncFinished object:self userInfo:nil];
     }];
}

// app needs some time to detect type of interent connection
// because we use seperate function (downloadFirstJsonFile) for first download of data
// if there is no interent connection app will show message - showFirstDownloadMessageError
-(void)downloadZipFile
{
    [self downloadFirstJsonFileWithSuccess:^(id responseObject) {
        [self unzip];
    } failure:^{
        [self showFirstDownloadMessageError];
    }];
}

-(BOOL)unzip
{
    NSString *zipName = [NSString stringWithFormat:@"%@.zip", [[RealmRepository shared] getSelectedConferenceId]];
    NSString *folderName = @"congressData";
    
    // /Users/iMaja/Library/Developer/CoreSimulator/Devices/7191433C-6DA5-4DA8-8408-485B1BD3EB7F/data/Containers/Data/Application/3C58C4F2-494F-4BB0-9B58-D14071233E8B/
    // PATH IS CHANGABLE -> BE CARFULE, DONT SAVE IT ANYWHERE
    NSString *libraryPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    // /Users/iMaja/Library/Developer/CoreSimulator/Devices/7191433C-6DA5-4DA8-8408-485B1BD3EB7F/data/Containers/Data/Application/3C58C4F2-494F-4BB0-9B58-D14071233E8B/Library/congressData
    NSString *filePath = [NSString stringWithFormat:@"%@/%@", libraryPath, folderName];
    
    NSString *zipLocation = [NSString stringWithFormat:@"%@/%@", filePath, zipName];
    
    NSString *zipDestination = [NSString stringWithFormat:@"%@/%@/jsonFile", libraryPath, folderName];
    
    // if materials already unziped - remove it
    if ([[NSFileManager defaultManager] fileExistsAtPath:zipDestination] && [[NSUserDefaults standardUserDefaults] valueForKey:kTypeOfInternetConnection])
    {
        [[NSFileManager defaultManager] removeItemAtPath:zipDestination error:nil];
    }
    
    // Unzip
    if ([SSZipArchive unzipFileAtPath:zipLocation toDestination: zipDestination])
    {
        NSError * error;
        NSArray * directoryContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:zipDestination error:&error];
        
        NSError *deserializingError;
        NSURL *localJsonFileURL = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/%@", zipDestination, [directoryContents firstObject]]];
        NSData *contentOfLocalFile = [NSData dataWithContentsOfURL:localJsonFileURL];
        NSDictionary *dictData = [NSJSONSerialization JSONObjectWithData:contentOfLocalFile
                                                                 options:NSJSONReadingMutableContainers
                                                                   error:&deserializingError];
        [self syncJsonData:dictData];
        return YES;
    }
    else
    {
        [self showFirstDownloadMessageError];
        return NO;
    }
}

-(void)syncJsonData:(NSDictionary *)dictData
{
    // DELETE DATA FIRST
    //    Booth
    //    Country
    //    Exhibitor
    //    ExhibitorRole
    //    Portal
    //    Session
    //    SessionCategory
    //    Speaker
    //    Talk
    //    Venue
    [[LLDataAccessLayer sharedInstance] deleteAllDataForEntity:@"Booth"];
    [[LLDataAccessLayer sharedInstance] deleteAllDataForEntity:@"Venue"];
    [[LLDataAccessLayer sharedInstance] deleteAllDataForEntity:@"Country"];
    [[LLDataAccessLayer sharedInstance] deleteAllDataForEntity:@"ExhibitorRole"];
    [[LLDataAccessLayer sharedInstance] deleteAllDataForEntity:@"Exhibitor"];
    [[LLDataAccessLayer sharedInstance] deleteAllDataForEntity:@"Portal"];
    
    if ([Talk getTimelinedTalks])
    {
        [self saveTalksIdsInTimeline];
    }
    
    if ([Talk getVotedTalks])
    {
        [self saveVotedTalksIds];
    }
    
    [[LLDataAccessLayer sharedInstance] deleteAllDataForEntity:@"Talk"];
    [[LLDataAccessLayer sharedInstance] deleteAllDataForEntity:@"Speaker"];
    [[LLDataAccessLayer sharedInstance] deleteAllDataForEntity:@"Session"];
    [[LLDataAccessLayer sharedInstance] deleteAllDataForEntity:@"SessionCategory"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleSaved:) name:@"TalkSaved" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleSaved:) name:@"SessionSaved" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleSaved:) name:@"VenueSaved" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleSaved:) name:@"SpeakerSaved" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleSaved:) name:@"SessionCategorySaved" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleSaved:) name:@"ExhibitorSaved" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleSaved:) name:@"BoothSaved" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleSaved:) name:@"PortalSaved" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleSaved:) name:@"ExhibitorRoleSaved" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleSaved:) name:@"CountrySaved" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleLinked:) name:@"Linked" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleSyncFinished:) name:kHandlePartOfSyncFinished object:self];
    
    // keys in jsonFile
    //    block_categories, blocks, conference_speakers, countries, current_time, exhibitors, locations, portal, presentations, speakers, campaign, block_sponsors, conference_sponsors
    
    NSNumber *appDataVersionID = [dictData objectForKey:@"conference_data_version_id"];
    if (appDataVersionID)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[RealmRepository shared] setDataVersion:[appDataVersionID integerValue]];
        });
    }
    
    self.numOfSaved = 0;
    self.numOfLinked = 0;
    self.numOfSavedTalksAndSpeakers = 2;
    
    NSDictionary *portal = [dictData objectForKey:@"portal"];
    NSDictionary *conference = [dictData objectForKey:@"conference"];
    [self syncPortal:portal withConference:conference];
    
    NSArray *arrayBlockCategories = [dictData objectForKey:@"block_categories"];
    [self syncBlockCategories:arrayBlockCategories];
    
    NSArray *arrayBlocks = [dictData objectForKey:@"blocks"];
    [self syncBlocks:arrayBlocks];
    
    self.arrayConferenceSpeakers = [dictData objectForKey:@"conference_speakers"];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleFirstSavedOfTalksAndSpeakers:) name:kESyncDataTalksSpeakersSaved object:nil];
    
    NSArray *arrayCountries = [dictData objectForKey:@"countries"];
    [self syncCountries:arrayCountries];
    
    NSArray *arrayExhibitors = [dictData objectForKey:@"exhibitors"];
    [self syncExhibitors:arrayExhibitors];
    
    NSArray *arrayLocations = [dictData objectForKey:@"locations"];
    NSArray *arrayRooms = [arrayLocations filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"type == 'Room'"]];
    NSArray *arrayBooths = [arrayLocations filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"type == 'Booth'"]];
    [self syncRooms:arrayRooms];
    [self syncBooths:arrayBooths];
    
    NSArray *arrayPresentations = [dictData objectForKey:@"presentations"];
    [self syncPresentations:arrayPresentations];
    
    NSArray *arraySpeakers = [dictData objectForKey:@"speakers"];
    [self syncSpeakers:arraySpeakers];
    
    NSMutableArray *arraySponsors = [dictData objectForKey:@"block_sponsors"];
    [arraySponsors addObjectsFromArray:[dictData objectForKey:@"conference_sponsors"]];
    [self syncExhibitorsRoles:arraySponsors];
}

-(void)updateJsonData:(NSDictionary *)dictData
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleSaved:) name:@"TalkSaved" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleSaved:) name:@"SessionSaved" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleSaved:) name:@"VenueSaved" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleSaved:) name:@"SpeakerSaved" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleSaved:) name:@"SessionCategorySaved" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleSaved:) name:@"ExhibitorSaved" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleSaved:) name:@"BoothSaved" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleSaved:) name:@"PortalSaved" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleSaved:) name:@"ExhibitorRoleSaved" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleSaved:) name:@"CountrySaved" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleLinked:) name:@"Linked" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleSyncFinished:) name:kHandlePartOfSyncFinished object:self];
    
    // keys in jsonFile
    //    block_categories, blocks, conference_speakers, countries, current_time, exhibitors, locations, portal, presentations, speakers, campaign, block_sponsors, conference_sponsors
    NSNumber *appDataVersionID = [dictData objectForKey:@"conference_data_version_id"];
    dispatch_async(dispatch_get_main_queue(), ^{
        [[RealmRepository shared] setDataVersion:[appDataVersionID integerValue]];
    });
    
    self.numOfSaved = 0;
    self.numOfLinked = 0;
    self.numOfSavedTalksAndSpeakers = 2;
    
    NSDictionary *portal = [dictData objectForKey:@"portal"];
    NSDictionary *conference = [dictData objectForKey:@"conference"];
    if (conference && portal && conference.count && portal.count)
    {
        [self syncPortal:portal withConference:conference];
    }
    
    NSArray *arrayBlockCategories = [dictData objectForKey:@"block_categories"];
    if (arrayBlockCategories)
    {
        [self syncBlockCategories:arrayBlockCategories];
    }
    
    NSArray *arrayBlocks = [dictData objectForKey:@"blocks"];
    if (arrayBlocks)
    {
        [self syncBlocks:arrayBlocks];
    }
    
    self.arrayConferenceSpeakers = [dictData objectForKey:@"conference_speakers"];
    if (self.arrayConferenceSpeakers)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleFirstSavedOfTalksAndSpeakers:) name:kESyncDataTalksSpeakersSaved object:nil];
    }
    
    NSArray *arrayCountries = [dictData objectForKey:@"countries"];
    if (arrayCountries)
    {
        [self syncCountries:arrayCountries];
    }
    
    NSArray *arrayExhibitors = [dictData objectForKey:@"exhibitors"];
    if (arrayExhibitors)
    {
        [self syncExhibitors:arrayExhibitors];
    }
    
    NSArray *arrayLocations = [dictData objectForKey:@"locations"];
    if (arrayLocations)
    {
        NSArray *arrayRooms = [arrayLocations filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"type == 'Room'"]];
        NSArray *arrayBooths = [arrayLocations filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"type == 'Booth'"]];
        if (arrayRooms)
        {
            [self syncRooms:arrayRooms];
        }
        
        if (arrayBooths)
        {
            [self syncBooths:arrayBooths];
        }
    }
    
    NSArray *arrayPresentations = [dictData objectForKey:@"presentations"];
    if (arrayPresentations)
    {
        [self syncPresentations:arrayPresentations];
    }
    
    NSArray *arraySpeakers = [dictData objectForKey:@"speakers"];
    if (arraySpeakers)
    {
        [self syncSpeakers:arraySpeakers];
    }
    
    NSMutableArray *arraySponsors = [dictData objectForKey:@"block_sponsors"];
    [arraySponsors addObjectsFromArray:[dictData objectForKey:@"conference_sponsors"]];
    
    if (arraySponsors)
    {
        [self syncExhibitorsRoles:arraySponsors];
    }
}

-(void)saveTalksIdsInTimeline
{
    for(Talk *talk in [Talk getUnsyncedTimelineTalks])
    {
        [[RealmRepository shared] saveTimelineWithPresentationId:[talk.tlkTalkId integerValue]
                                                      bookmarked:[talk.zetBookmarked integerValue] == 0
                                                                ? NO
                                                                : YES];
    }
}

-(void)saveVotedTalksIds
{
    for(Talk *talk in [Talk getUnsyncedVotedTalks])
    {
        [[RealmRepository shared] saveVoteWithPresentationId:[talk.tlkTalkId integerValue]
                                                       voted:[talk.tlkVoted integerValue] == 0
                                                            ? NO
                                                            : YES];
    }
}

-(void)downloadFirstJsonFileWithSuccess:(void (^)(id responseObject))success
                                failure:(void (^)(void))failure
{
    [self downloadFileRetryingNumberOfTimes:5
                                    success:^(id responseObject)
     {
         NSLog(@"Success download: %@", responseObject);
         success(responseObject);
     }
                                    failure:^(NSError *error)
     {
         // Can't get real error from request, but set error in downloadFileRetryingNumberOfTimes function
         NSLog(@"Error resending: %@", [[error userInfo] objectForKey:@"error"]);
         failure();
     }];
}

-(void)showFirstDownloadMessageError
{
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@"Error"
                                message:@"Data synchronization has failed. In order to set up the application synchronisation of data is needed.\n\nPlease check your internet connection and try again or contact us at support@e-materials.com."
                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* btnIntConnection = [UIAlertAction actionWithTitle:@"Check internet connection"
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * action) {
                                                                 [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                                                                 exit(0);
                                                             }];
    
    [alert addAction:btnIntConnection];
    
    UIAlertAction* btnContactUs = [UIAlertAction actionWithTitle:@"Contact Us"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action) {
                                                             
                                                             NSString *recipients = @"mailto:support@e-materials.com";
                                                             
                                                             NSString *email = recipients;
                                                             
                                                             email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                                                             
                                                             [[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
                                                             
                                                             exit(0);
                                                         }];
    
    [alert addAction:btnContactUs];
    
    UIAlertAction* btnCancel = [UIAlertAction actionWithTitle:@"Close the app"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action) {
                                                          exit(0);
                                                      }];
    
    [alert addAction:btnCancel];
    
    [[Utils topViewController] presentViewController:alert animated:YES completion:nil];
}

-(void)downloadFileRetryingNumberOfTimes:(NSUInteger)ntimes
                                 success:(void (^)(id responseObject))success
                                 failure:(void (^)(NSError *error))failure
{
    if (ntimes <= 0)
    {
        if (failure)
        {
            NSError *error = [[NSError alloc] initWithDomain:@"Download" code:1 userInfo:[NSDictionary dictionaryWithObject:@"Cant download file." forKey:@"error"]];
            failure(error);
        }
    }
    else
    {
        NSString *zipName = [NSString stringWithFormat:@"%@.zip", [[RealmRepository shared] getSelectedConferenceId]];
        NSString *folderName = @"congressData";
        
        // /Users/iMaja/Library/Developer/CoreSimulator/Devices/7191433C-6DA5-4DA8-8408-485B1BD3EB7F/data/Containers/Data/Application/3C58C4F2-494F-4BB0-9B58-D14071233E8B/
        // PATH IS CHANGABLE -> BE CARFULE, DONT SAVE IT ANYWHERE
        NSString *libraryPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        
        // /Users/iMaja/Library/Developer/CoreSimulator/Devices/7191433C-6DA5-4DA8-8408-485B1BD3EB7F/data/Containers/Data/Application/3C58C4F2-494F-4BB0-9B58-D14071233E8B/Library/congressData
        NSString *filePath = [NSString stringWithFormat:@"%@/%@", libraryPath, folderName];
        
        NSString *fileFullPath = [NSString stringWithFormat:@"%@/%@", filePath, zipName];
        
        if (![[NSFileManager defaultManager] fileExistsAtPath:filePath]) // If folder doesnt exist
        {
            [[NSFileManager defaultManager] createDirectoryAtPath:filePath withIntermediateDirectories:YES attributes:nil error:nil]; // create it
        }
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:fileFullPath]) // File exist.
        {
            if ([[NSUserDefaults standardUserDefaults] valueForKey:kTypeOfInternetConnection]) // File exist. Other type of connection (Wi-Fi). Delete file and dowload again.
            {
                [[NSFileManager defaultManager] removeItemAtPath:fileFullPath error:nil];
            }
        }
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", kURLZip, zipName]]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                        
                                                           timeoutInterval:60.0];
        
        [request setHTTPMethod:@"GET"];
        
        // Requesting for file
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        
        operation.outputStream = [NSOutputStream outputStreamToFileAtPath:fileFullPath append:NO];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             if (success)
             {
                 NSLog(@"Received file.!!!");
                 success(responseObject);
             }
         }
                                         failure:^(AFHTTPRequestOperation *operation, NSError *error)
         {
             NSLog(@"LOCAL URL: %@, error: %@", operation.request.URL,  error);
             //Resend failed file download
             [self downloadFileRetryingNumberOfTimes:ntimes - 1 success:success failure:failure];
         }];
        
        [[EWebServiceClient sharedInstance] enqueueHTTPRequestOperation:operation];
    }
}

-(void)syncUserData:(BOOL)optIn
{
    [self getProfile:optIn];
    [self getOrders];
    [self syncTimeline];
    [self syncVotes];
    [self getAttendedSessions];
    
    if (kNotesExist == YES)
    {
        [self uploadNotes];
        [self getNotes];
    }

    [self syncMaterialBrowsings];
}

#pragma mark - Porta + Current Conference

-(void)syncPortal:(NSDictionary *)jsonData
{
    NSLog(@"Call function %s",  __FUNCTION__);
    
    // for checking of changing current conference id
    __block NSNumber *prevCurrentConferenceId = [[RealmRepository shared] getSelectedConferenceId];
    
    //Save items to CoreData
    self.numOfSaved --;
    
    //    {
    //        conferences =     (
    //                           {
    //                               "allow_downloads" = 1;
    //                               description = "";
    //                               "ends_at" = "2015-04-06";
    //                               id = 7211;
    //                               location = "Valencia, Spain";
    //                               name = "EBMT 2016";
    //                               slug = ebmt2016;
    //                               "sponsor_banner" = "<null>";
    //                               "sponsor_link" = "<null>";
    //                               "sponsor_title" = "<null>";
    //                               sponsored = 0;
    //                               "starts_at" = "2015-04-03";
    //                           }
    //                           );
    //        footer = "<null>";
    //        header = "<null>";
    //        id = 16;
    //        logo = "<null>";
    //        name = "EBMT 2017";
    //        "primary_color" = "<null>";
    //        slug = ebmt2017;
    //        "terms_and_conditions" = "<null>";
    //        url = "<null>";
    //    }
    
    NSMutableDictionary *dictPortal = [NSMutableDictionary dictionaryWithDictionary:jsonData];
    
    __block NSNumber *currentConferenceId = nil;
    
    for (NSDictionary *dictConference in [jsonData objectForKey:@"conferences"])
    {
        currentConferenceId = [dictConference objectForKey:@"id"];
        
        [[NSUserDefaults standardUserDefaults] setObject:[dictConference objectForKey:@"sponsored"] forKey:@"sponsored"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    [dictPortal setObject:currentConferenceId forKey:@"conference_id"];
    
    NSDictionary* attributeMappings = [NSDictionary dictionaryWithObjectsAndKeys:
                                       @"id", @"porPortalId",
                                       @"name", @"porName",
                                       @"terms_and_conditions", @"porTermsAndConditions",
                                       @"conference_id", @"porCurrentConferenceId",
                                       nil];
    
    [[LLDataAccessLayer sharedInstance] insertEntitiesArray:@[dictPortal] withName:@"Portal" receivedPredicatePrimaryKey:@"id" predicateKeyAttribute:@"porPortalId" sortDescriptorKey:@"porPortalId" activeAttributeName:nil attributeMappings:attributeMappings withCompletitionBlock:^(bool finished)
     {
         if (finished)
         {
             if (![prevCurrentConferenceId isEqual:currentConferenceId])
             {
                 [self registerDevice];
             }
         }
     }];
}

-(void)syncPortal:(NSDictionary *)jsonData withConference:(NSDictionary *)conference
{
    NSLog(@"Call function %s",  __FUNCTION__);
    
    // for checking of changing current conference id
    __block NSNumber *prevCurrentConferenceId = [[RealmRepository shared] getSelectedConferenceId];
    
    //Save items to CoreData
    self.numOfSaved --;
    
    //    {
    //        conferences =     (
    //                           {
    //                               "allow_downloads" = 1;
    //                               description = "";
    //                               "ends_at" = "2015-04-06";
    //                               id = 7211;
    //                               location = "Valencia, Spain";
    //                               name = "EBMT 2016";
    //                               slug = ebmt2016;
    //                               "sponsor_banner" = "<null>";
    //                               "sponsor_link" = "<null>";
    //                               "sponsor_title" = "<null>";
    //                               sponsored = 0;
    //                               "starts_at" = "2015-04-03";
    //                           }
    //                           );
    //        footer = "<null>";
    //        header = "<null>";
    //        id = 16;
    //        logo = "<null>";
    //        name = "EBMT 2017";
    //        "primary_color" = "<null>";
    //        slug = ebmt2017;
    //        "terms_and_conditions" = "<null>";
    //        url = "<null>";
    //    }
    
    NSMutableDictionary *dictPortal = [NSMutableDictionary dictionaryWithDictionary:jsonData];
    
    __block NSNumber *currentConferenceId = [conference objectForKey:@"id"];
    
    if ([conference objectForKey:@"sponsored"])
    {
        [[NSUserDefaults standardUserDefaults] setObject:[conference objectForKey:@"sponsored"] forKey:@"sponsored"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    [dictPortal setObject:currentConferenceId forKey:@"conference_id"];
    
    NSDictionary* attributeMappings = [NSDictionary dictionaryWithObjectsAndKeys:
                                       @"id", @"porPortalId",
                                       @"name", @"porName",
                                       @"terms_and_conditions", @"porTermsAndConditions",
                                       @"conference_id", @"porCurrentConferenceId",
                                       nil];
    
    [[LLDataAccessLayer sharedInstance] insertEntitiesArray:@[dictPortal] withName:@"Portal" receivedPredicatePrimaryKey:@"id" predicateKeyAttribute:@"porPortalId" sortDescriptorKey:@"porPortalId" activeAttributeName:nil attributeMappings:attributeMappings withCompletitionBlock:^(bool finished)
     {
         if (finished)
         {
             if (![prevCurrentConferenceId isEqual:currentConferenceId])
             {
                 [self registerDevice];
             }
         }
     }];
}

#pragma mark - Bloks

-(void)syncBlocks:(NSArray *)jsonData
{
    NSLog(@"Call function %s",  __FUNCTION__);
    
    //Save items to CoreData
    self.numOfSaved --;
    
    //    "id": 61,
    //    "name": "Mollitia unde eveniet consequatur ab.",
    //    "starts_at": "2016-04-01 13:28:59",
    //    "ends_at": "2016-04-01 14:39:43",
    //    "code": "bf83469f-a8e1-3388-89d7-e282ea85b334",
    //    "location_id": 53,
    //    "block_category_id": 9
    
    NSDictionary* attributeMappings = [NSDictionary dictionaryWithObjectsAndKeys:
                                       @"id", @"sesSessionId",
                                       @"name", @"sesName",
                                       @"type", @"sesType",
                                       @"starts_at", @"sesStartTime",
                                       @"ends_at", @"sesEndTime",
                                       @"chairperson", @"sesChairPerson",
                                       @"block_category_id", @"sesCategoryId",
                                       @"location_id", @"sesVenueId",
                                       @"code", @"sesCode",
                                       @"survey", @"sesSurvey",
                                       @"topic_id", @"sesTopicId",
                                       @"subtitle", @"sesSubtitle",
                                       nil];
    
    [[LLDataAccessLayer sharedInstance] insertEntitiesArray:jsonData withName:@"Session" receivedPredicatePrimaryKey:@"id" predicateKeyAttribute:@"sesSessionId" sortDescriptorKey:@"sesSessionId" activeAttributeName:@"deleted_at" attributeMappings:attributeMappings withCompletitionBlock:^(bool finished)
     {
         if(finished)
         {
             ConferenceCoreDataLink *link = [[RealmRepository shared] link];
             link.presentationBlock = YES;
             link.blockLocation = YES;
             link.blockExhibitorRole = YES;
             [[RealmRepository shared] persistLink:link];

             self.numOfLinked --;
             [self linkTalkAndSession];
             self.numOfLinked --;
             [self linkSessionAndVenue];
             self.numOfLinked --;
             [self linkExhibitorRoleAndSession];
         }
     }];
}

#pragma mark - Block Categories

-(void)syncBlockCategories:(NSArray *)jsonData
{
    NSLog(@"Call function %s",  __FUNCTION__);
    
    //Save items to CoreData
    self.numOfSaved --;
    
    //    "id": 1,
    //    "name": "est",
    //    "color": "#00c11c",
    //    "conference_id": 1
    
    NSDictionary* attributeMappings = [NSDictionary dictionaryWithObjectsAndKeys:
                                       @"id", @"secSessionCategoryId",
                                       @"name", @"secName",
                                       @"color", @"secColor",
                                       nil];
    
    [[LLDataAccessLayer sharedInstance] insertEntitiesArray:jsonData withName:@"SessionCategory" receivedPredicatePrimaryKey:@"id" predicateKeyAttribute:@"secSessionCategoryId" sortDescriptorKey:@"secSessionCategoryId" activeAttributeName:@"deleted_at" attributeMappings:attributeMappings withCompletitionBlock:^(bool finished)
     {
         //#warning link categories and sessions
     }];
}

#pragma mark - Presentations

-(void)syncPresentations:(NSArray *)jsonData
{
    NSLog(@"Call function %s",  __FUNCTION__);
    
    //Save talks to CoreData
    self.numOfSaved --;
    
    //    access = 0;
    //    "allow_comments" = 0;
    //    "allow_feedback" = 0;
    //    available = 0;
    //    "available_external" = 0;
    //    "available_html_material" = 0;
    //    "available_main_material" = 0;
    //    "available_supporting_material" = 0;
    //    "available_webcast_material" = 0;
    //    "block_id" = 4298;
    //    code = "<null>";
    //    "conference_id" = 7236;
    //    "consent_completed" = 0;
    //    consents =             {
    //    };
    //    description = "<null>";
    //    "ends_at" = "2017-03-26 12:00:00";
    //    "has_html_material" = 0;
    //    "has_main_material" = 0;
    //    "has_supporting_material" = 0;
    //    "has_webcast_material" = 0;
    //    id = 12363;
    //    "imported_id" = 25;
    //    popularity = 0;
    //    section = Scientific;
    //    "speaker_image" = "<null>";
    //    "speaker_image_thumb" = "<null>";
    //    "speaker_name" = "Antonella Santucci, Richard Szydlo";
    //    sponsored = 0;
    //    "starts_at" = "2017-03-26 09:00:00";
    //    title = "Statistics Course \U2013 Survival Analysis";
    //    type = Oral;
    
    NSDictionary* attributeMappings = [NSDictionary dictionaryWithObjectsAndKeys:
                                       @"id", @"tlkTalkId",
                                       @"title", @"tlkTitle",
                                       @"type", @"tlkType",
                                       @"starts_at", @"tlkStartTime",
                                       @"ends_at", @"tlkEndTime",
                                       @"block_id", @"tlkSessionId",
                                       @"speaker_name", @"tlkSpeakerName",
                                       @"speaker_image", @"tlkSpeakerImageUrl",
                                       @"available_external", @"tlkExternalUrl",
                                       @"available", @"tlkAvailable",
                                       @"imported_id", @"tlkImportedId",
                                       @"available_webcast_material", @"tlkAvailableWebcastMaterial",
                                       @"available_main_material", @"tlkAvailableMainMaterial",
                                       @"has_webcast_material", @"tlkHasWebcastMaterial",
                                       @"has_main_material", @"tlkHasMainMaterial",
                                       @"has_audio_material", @"tlkHasAudioMaterial",
                                       @"available_supporting_material", @"tlkAvailableSupportingMaterial",
                                       @"has_supporting_material", @"tlkHasSupportingMaterial",
                                       @"consent_completed", @"tlkConsentCompleted",
                                       @"description", @"tlkDescription",
                                       @"code", @"tlkBoardNo",
                                       @"vote_available", @"tlkVoteAvailable",
                                       @"has_voting", @"tlkVotingEnabled",
                                       nil];
    
    [[LLDataAccessLayer sharedInstance] insertEntitiesArray:jsonData withName:@"Talk" receivedPredicatePrimaryKey:@"id" predicateKeyAttribute:@"tlkTalkId" sortDescriptorKey:@"tlkTalkId" activeAttributeName:@"deleted_at" attributeMappings:attributeMappings withCompletitionBlock:^(bool finished)
     {
         if(finished)
         {
             self.numOfSavedTalksAndSpeakers--;
             [[NSNotificationCenter defaultCenter] postNotificationName:kESyncDataTalksSpeakersSaved object:self userInfo:nil];
             ConferenceCoreDataLink *link = [[RealmRepository shared] link];
             link.presentationBlock = YES;
             link.presentationMaterial = YES;
             link.presentationNote = YES;
             link.presentationOrder = YES;
             [[RealmRepository shared] persistLink:link];

             self.numOfLinked --;
             [self linkTalkAndSession];
             // link notes, orders and materials - because of update program with notification
             // update was delete all relationship between those entities
             // those linking not goes in counting numbOfLinked
             self.numOfLinked --;
             [self linkMaterialAndTalk];
             self.numOfLinked --;
             [self linkTalkAndNote];
             self.numOfLinked --;
             [self linkTalkAndOrder];
             // in case user did not login and he had presentations in timeline -> take it back
             NSArray *timelines = [[RealmRepository shared] getTimelineDict];
             if (timelines.count) {
                 [Talk updateSavedTimelineBeforeUpdate:timelines];
                 [[RealmRepository shared] deleteTimeline];
             }
             NSArray *votes = [[RealmRepository shared] getVoteDict];
             if (votes.count) {
                 [Talk updateSavedVoteBeforeUpdate:votes];
                 [[RealmRepository shared] deleteVote];
             }
         }
     }];
}

#pragma mark - Speaker + Connection with Presentations

-(void)syncSpeakersAndPresentations:(NSArray *)arrayData
{
    ConferenceCoreDataLink *link = [[RealmRepository shared] link];
    link.presentationSpeaker = YES;
    [[RealmRepository shared] persistLink:link];

    self.numOfLinked--;
    
    // need to delete before sync of Speakers and Talks, because there is not indicator of that relationship between speaker anda talk are deleted
    //             [Talk deleteSpeakersForTalks];
    
    [Talk linkTalksAndSpeakrs:arrayData];
}

-(void)syncSpeakers:(NSArray *)jsonData
{
    NSLog(@"Call function %s",  __FUNCTION__);
    
    //Save talks to CoreData
    self.numOfSaved --;
    
    //    {
    //        "additional_info" = "<null>";
    //        attending = 1;
    //        code = "66CCE655-AAF0-41FF-A88E-2680073CEB12";
    //        "conference_id" = 7211;
    //        "country_id" = 211;
    //        "e_points" = 0;
    //        "e_points_used" = 0;
    //        email = "";
    //        "first_name" = Mara;
    //        id = 29785;
    //        "image_url" = "<null>";
    //        "invitations_left" = 0;
    //        "last_name" = "Andr\U00e9s";
    //        name = "Mara Andr\U00e9s";
    //        roles =             {
    //            block =                 (
    //            );
    //            conference =                 {
    //                7211 =                     (
    //                );
    //            };
    //            presentation =                 (
    //            );
    //        };
    //        speaker = 1;
    //        title = "";
    //        type = Delegate;
    //        "user_id" = "<null>";
    //        website = "<null>";
    //    }
    
    NSDictionary* attributeMappings = [NSDictionary dictionaryWithObjectsAndKeys:
                                       @"id", @"spkSpeakerId",
                                       @"first_name", @"spkFirstName",
                                       @"last_name", @"spkLastName",
                                       @"image_url", @"spkPicture",
                                       @"additional_info", @"spkBiography",
                                       @"user_id", @"spkUserId",
                                       nil];
    
    [[LLDataAccessLayer sharedInstance] insertEntitiesArray:jsonData withName:@"Speaker" receivedPredicatePrimaryKey:@"id" predicateKeyAttribute:@"spkSpeakerId" sortDescriptorKey:@"spkSpeakerId" activeAttributeName:@"deleted_at" attributeMappings:attributeMappings withCompletitionBlock:^(bool finished)
     {
         if(finished)
         {
             self.numOfSavedTalksAndSpeakers--;
             [[NSNotificationCenter defaultCenter] postNotificationName:kESyncDataTalksSpeakersSaved object:self userInfo:nil];
         }
     }];
}

#pragma mark - Locations

-(void)syncRooms:(NSArray *)jsonData
{
    NSLog(@"Call function %s",  __FUNCTION__);
    
    //Save items to CoreData
    self.numOfSaved --;
    
    //    color = "<null>";
    //    "conference_id" = 1234;
    //    floor = 0;
    //    id = 3647;
    //    "imported_id" = 11;
    //    name = "Sala Retiro";
    //    "party_id" = "<null>";
    //    type = Room;
    //    "x_coord" = "0.0000000";
    //    "y_coord" = "0.0000000";
    
    
    NSDictionary* attributeMappings = [NSDictionary dictionaryWithObjectsAndKeys:
                                       @"id", @"venVenueId",
                                       @"name", @"venName",
                                       @"color", @"venColor",
                                       @"type", @"venType",
                                       @"floor",@"venFloor",
                                       @"x_coord",@"venCoordX",
                                       @"y_coord",@"venCoordY",
                                       @"imported_id", @"venImportedId",
                                       nil];
    
    [[LLDataAccessLayer sharedInstance] insertEntitiesArray:jsonData withName:@"Venue" receivedPredicatePrimaryKey:@"id" predicateKeyAttribute:@"venVenueId" sortDescriptorKey:@"venVenueId" activeAttributeName:@"deleted_at" attributeMappings:attributeMappings withCompletitionBlock:^(bool finished)
     {
         if(finished)
         {
             ConferenceCoreDataLink *link = [[RealmRepository shared] link];
             link.blockLocation = YES;
             [[RealmRepository shared] persistLink:link];

             self.numOfLinked --;
             [self linkSessionAndVenue];
         }
     }];
}

-(void)syncBooths:(NSArray *)jsonData
{
    NSLog(@"Call function %s",  __FUNCTION__);
    
    //Save items to CoreData
    self.numOfSaved --;
    
    //    "id": 53,
    //    "name": "nesciunt",
    //    "type": "Room",
    //    "color": "#0013e0",
    //    "floor": 0,
    //    "x_coord": "0.0000004",
    //    "y_coord": "0.0000004",
    //    "conference_id": 1,
    //    "party_id": 0
    
    NSDictionary* attributeMappings = [NSDictionary dictionaryWithObjectsAndKeys:
                                       @"id", @"bthBoothId",
                                       @"name", @"bthName",
                                       @"x_coord",@"bthCoordX",
                                       @"y_coord",@"bthCoordY",
                                       @"party_id", @"bthExhibitorId",
                                       nil];
    
    [[LLDataAccessLayer sharedInstance] insertEntitiesArray:jsonData withName:@"Booth" receivedPredicatePrimaryKey:@"id" predicateKeyAttribute:@"bthBoothId" sortDescriptorKey:@"bthBoothId" activeAttributeName:@"deleted_at" attributeMappings:attributeMappings withCompletitionBlock:^(bool finished)
     {
         if(finished)
         {
             ConferenceCoreDataLink *link = [[RealmRepository shared] link];
             link.boothExhibitor = YES;
             [[RealmRepository shared] persistLink:link];

             self.numOfLinked --;
             [self linkBoothAndExhibitor];
         }
     }];
}

#pragma mark - Exhibitors

-(void)syncExhibitors:(NSArray *)exhibitors
{
    NSLog(@"Call function %s",  __FUNCTION__);
    
    //    {
    //        "additional_info" = "<null>";
    //        attending = 1;
    //        code = "<null>";
    //        "conference_id" = 7270;
    //    "contact_info" =     {
    //        address1 = "15 Lyon Road Merton London SW19 2RL, UK";
    //        address2 = "";
    //        city = "<null>";
    //        "country_id" = "<null>";
    //        "created_at" = "2017-03-03 16:40:53";
    //        "deleted_at" = "<null>";
    //        department = 43;
    //        fax = "<null>";
    //        id = 31423;
    //        institution = "";
    //        "party_id" = 57710;
    //        phone = "+44 20 8715 1812";
    //        primary = 1;
    //        state = "<null>";
    //        "updated_at" = "2017-03-03 16:40:53";
    //        website = "www.wisepress.com";
    //        zip = "<null>";
    //    };
    //        "country_id" = "<null>";
    //        "e_points" = 0;
    //        "e_points_used" = 0;
    //        email = "<null>";
    //        "first_name" = "<null>";
    //        id = 108640;
    //        "image_url" = "http://apiv2.e-materials.com/afb/afbb4c522a8ffd205029b3c8bd843c96.png";
    //        "image_url_thumb" = "<null>";
    //        "imported_id" = "<null>";
    //        "invitations_left" = 0;
    //        "last_name" = "<null>";
    //        name = "Bio Rad";
    //        speaker = 0;
    //        title = "<null>";
    //        type = Exhibitor;
    //        "user_id" = "<null>";
    //        website = "http://www.bio-rad.com/";
    //    },
    
    //Save items to CoreData
    self.numOfSaved --;
    
    NSDictionary* attributeMappings = [NSDictionary dictionaryWithObjectsAndKeys:
                                       @"id", @"exhExhibitorId",
                                       @"name", @"exhName",
                                       @"additional_info", @"exhDescription",
                                       @"image_url", @"exhImage",
                                       @"image_url_thumb", @"exhImageThumb",
                                       @"website", @"exhWebsite",
                                       @"email", @"exhEmail",
                                       @"imported_id", @"exhImportedId",
                                       nil];
    
    [[LLDataAccessLayer sharedInstance] insertEntitiesArray:exhibitors withName:@"Exhibitor" receivedPredicatePrimaryKey:@"id" predicateKeyAttribute:@"exhExhibitorId" sortDescriptorKey:@"exhExhibitorId" activeAttributeName:@"deleted_at" attributeMappings:attributeMappings withCompletitionBlock:^(bool finished)
     {
         if(finished)
         {
             ConferenceCoreDataLink *link = [[RealmRepository shared] link];
             link.boothExhibitor = YES;
             link.exhibitorExhibitorRole = YES;
             [[RealmRepository shared] persistLink:link];

             self.numOfLinked --;
             [self linkBoothAndExhibitor];
             self.numOfLinked --;
             [self linkExhibitorRoleAndExhibitor];
         }
     }];
}

#pragma mark - Exhibitors Role

-(void)getExhibitorsRoles
{
    NSLog(@"Call function %s",  __FUNCTION__);
    
    self.numOfRequests++;
    
    [[EWebServiceClient sharedInstance] sendRequestWithPath:@"resource_roles/?resource_type=block&roles=endorser,gold_supporter,contributor_supporter,exhibitor&should_have_party=1"
                                                     method:@"GET"
                                                     params:nil
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         NSError *error = nil;
         NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:successResponseData options:NSJSONReadingMutableContainers error:&error];
         
         if (!jsonData.count)
         {
             NSLog(@"%s success - no json", __FUNCTION__);
         }
         else
         {
             NSLog(@"%s success", __FUNCTION__);
             
             [self getExhiborsRolesTypeConfereces:[jsonData objectForKey:@"data"]];
         }
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         NSLog(@"%s failure error: %@", __FUNCTION__, error);
     }];
}

-(void)getExhiborsRolesTypeConfereces:(NSArray *)exhibitorsRolesTypeBlock
{
    [[EWebServiceClient sharedInstance] sendRequestWithPath:@"resource_roles/?resource_type=conference&roles=endorser,gold_supporter,contributor_supporter,exhibitor&should_have_party=1"
                                                     method:@"GET"
                                                     params:nil
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         NSError *error = nil;
         NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:successResponseData options:NSJSONReadingMutableContainers error:&error];
         
         if (!jsonData.count)
         {
             NSLog(@"%s success - no json", __FUNCTION__);
         }
         else
         {
             NSLog(@"%s success", __FUNCTION__);
             
             //Save items to CoreData
             NSMutableArray *exhibitorsRoles = [NSMutableArray arrayWithArray:exhibitorsRolesTypeBlock];
             [exhibitorsRoles addObjectsFromArray:[jsonData objectForKey:@"data"]];
             [self syncExhibitorsRoles:exhibitorsRoles];
         }
         
         [[NSNotificationCenter defaultCenter] postNotificationName:kESyncDataExhibitorsRolesDownloaded object:self userInfo:nil];
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         NSLog(@"%s failure error: %@", __FUNCTION__, error);
     }];
}

-(void)syncExhibitorsRoles:(NSArray *)jsonData
{
    NSLog(@"Call function %s",  __FUNCTION__);
    
    //Save items to CoreData
    self.numOfSaved --;
    
    //    {
    //        "id": 86263,
    //        "resource_type": "block",
    //        "resource_id": 5129,
    //        "role": "gold_sponsor",
    //        "user_id": null,
    //        "party_id": 56334
    //    },
    NSDictionary *exhibitorsRolesSettings = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"exhibitorsRolesSettings" ofType:@"plist"]];
    
    __block NSMutableArray *exhRolesArray = [NSMutableArray arrayWithCapacity:10];
    
    for (NSDictionary *role in jsonData)
    {
        if ([exhibitorsRolesSettings objectForKey:[role objectForKey:@"role"]])
        {
            NSDictionary *dictRoleSetting = [exhibitorsRolesSettings objectForKey:[role objectForKey:@"role"]];
            NSNumber *orderPosition = [dictRoleSetting objectForKey:@"position"];

            NSMutableDictionary *newDictRole = [NSMutableDictionary dictionaryWithDictionary:role];
            [newDictRole setObject:orderPosition forKey:@"order_position"];

            [exhRolesArray addObject:newDictRole];
        }
    }
    
    NSDictionary* attributeMappings = [NSDictionary dictionaryWithObjectsAndKeys:
                                       @"id", @"erExhibitorRoleId",
                                       @"resource_id", @"erSessionId",
                                       @"role", @"erRoleName",
                                       @"party_id", @"erExhibitorId",
                                       @"order_position", @"erOrderPosition",
                                       nil];
    
    
    [[LLDataAccessLayer sharedInstance] insertEntitiesArray:exhRolesArray withName:@"ExhibitorRole" receivedPredicatePrimaryKey:@"id" predicateKeyAttribute:@"erExhibitorRoleId" sortDescriptorKey:@"erExhibitorRoleId" activeAttributeName:@"deleted_at" attributeMappings:attributeMappings withCompletitionBlock:^(bool finished)
     {
         if (finished)
         {
//             // remove roles those are not on server
//             if (exhRolesArray.count > 0)
//             {
//                 [self removeRolesThoseAreNotOnServer:exhRolesArray];
//             }
             ConferenceCoreDataLink *link = [[RealmRepository shared] link];
             link.exhibitorExhibitorRole = YES;
             link.blockExhibitorRole = YES;
             [[RealmRepository shared] persistLink:link];
             
             self.numOfLinked --;
             [self linkExhibitorRoleAndExhibitor];
             self.numOfLinked --;
             [self linkExhibitorRoleAndSession];
         }
     }];
}

-(void)removeRolesThoseAreNotOnServer:(NSArray *)rolesOnServer
{
    NSMutableArray* receivedItemsIDs = [[NSMutableArray alloc] initWithCapacity:10];
    
    // first extract the IDs of the mapItems we just received and sort them
    for(id item in rolesOnServer)
    {
        if(item != (id)[NSNull null])
        {
            [receivedItemsIDs addObject:(NSString *)[item objectForKey:@"id"]];
        }
    }
    [receivedItemsIDs sortUsingSelector:@selector(compare:)];
    
    // remove presentations from timeline; they deleted from web
    NSPredicate* predicate1 = [NSPredicate predicateWithFormat:@"NOT (erExhibitorRoleId IN %@)", receivedItemsIDs];
    
    NSArray* exhRolesNotOnServer = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"ExhibitorRole"
                                                                                         predicate:predicate1
                                                                                   sortDescriptors:nil
                                                                                             inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    
    
    for(ExhibitorRole *er in exhRolesNotOnServer)
    {
        [[LLDataAccessLayer sharedInstance].managedObjectContext deleteObject:er];
    }
    
    [[LLDataAccessLayer sharedInstance] saveContextWithMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
}
//
//#pragma mark - Survey
//
//-(void)getSurvey
//{
//    NSLog(@"Call function %s",  __FUNCTION__);
//    
//    self.numOfRequests++;
//    
//    [[LLWebServiceClient sharedInstance] sendRequestWithPath:@"campaigns?user_id=7&include=questions"
//                                                      method:@"GET"
//                                                      params:nil
//                                        successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
//     {
//         NSError *error = nil;
//         NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:successResponseData options:NSJSONReadingMutableContainers error:&error];
//         
//         if (!jsonData.count)
//         {
//             NSLog(@"%s success - no json", __FUNCTION__);
//         }
//         else
//         {
//             NSLog(@"%s success", __FUNCTION__);
//             //Save items to CoreData
//             [self saveSurveys:[jsonData objectForKey:@"data"]];
//         }
//         
//         [[NSNotificationCenter defaultCenter] postNotificationName:kESyncDataSurveysDownloaded object:self userInfo:nil];
//     }
//                                        failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
//     {
//         NSLog(@"%s failure error: %@", __FUNCTION__, error);
//     }];
//}
//
//-(void)saveSurveys:(NSArray *)surveys
//{
//    NSLog(@"Call function %s",  __FUNCTION__);
//    
//    //                        "created_at" = "2016-11-07 21:20:44";
//    //                        description = "Please circle the most appropriate answer to each of the questions using the scale below where 1 is poor\nand 5 is excellent.";
//    //                        id = 5692;
//    //                        name = Pfizer;
//    //                        questions =             (
//    //                                                 {
//    //                                                     "campaign_id" = 5692;
//    //                                                     description = "<null>";
//    //                                                     group = "Please rate following aspects of the symposium";
//    //                                                     id = 58;
//    //                                                     max = 5;
//    //                                                     min = 1;
//    //                                                     options = "<null>";
//    //                                                     required = 0;
//    //                                                     title = "Overall evaluation of the symposium";
//    //                                                     type = radio;
//    //                                                 },
//    //                                                 {
//    //                                                     "campaign_id" = 5692;
//    //                                                     description = "<null>";
//    //                                                     group = "Are there any further comments or suggestions that you would like to share? If so, please write these in the box below.";
//    //                                                     id = 96;
//    //                                                     max = 0;
//    //                                                     min = 0;
//    //                                                     options = "<null>";
//    //                                                     required = 0;
//    //                                                     title = "Your comments or suggestions:";
//    //                                                     type = textarea;
//    //                                                 }
//    //                                                 );
//    //                        type = "<null>";
//    //                        "user_id" = 7;
//    //                    }
//    //                    );
//    //    }
//    
//    //Save items to CoreData
//    self.numOfSaved --;
//    
//    //    survCreatedAt
//    //    survDescription
//    //    survName
//    //    survSurveyId
//    //    survType
//    //    survUserId
//    
//    NSDictionary* attributeMappings = [NSDictionary dictionaryWithObjectsAndKeys:
//                                       @"id", @"survSurveyId",
//                                       @"name", @"survName",
//                                       @"created_at", @"survCreatedAt",
//                                       @"description", @"survDescription",
//                                       @"type", @"survType",
//                                       @"user_id", @"survUserId",
//                                       nil];
//    
//    [[LLDataAccessLayer sharedInstance] insertEntitiesArray:surveys withName:@"Survey" receivedPredicatePrimaryKey:@"id" predicateKeyAttribute:@"survSurveyId" sortDescriptorKey:@"survSurveyId" activeAttributeName:@"deleted_at" attributeMappings:attributeMappings withCompletitionBlock:^(bool finished)
//     {
//         if(finished)
//         {
//             [self saveSurveyQuestions:surveys];
//         }
//     }];
//}
//
//-(void)saveSurveyQuestions:(NSArray *)surveys
//{
//    NSLog(@"Call function %s",  __FUNCTION__);
//    
//    
//    //                        "created_at" = "2016-11-07 21:20:44";
//    //                        description = "Please circle the most appropriate answer to each of the questions using the scale below where 1 is poor\nand 5 is excellent.";
//    //                        id = 5692;
//    //                        name = Pfizer;
//    //                        questions =             (
//    //                                                 {
//    //                                                     "campaign_id" = 5692;
//    //                                                     description = "<null>";
//    //                                                     group = "Please rate following aspects of the symposium";
//    //                                                     id = 58;
//    //                                                     max = 5;
//    //                                                     min = 1;
//    //                                                     options = "<null>";
//    //                                                     required = 0;
//    //                                                     title = "Overall evaluation of the symposium";
//    //                                                     type = radio;
//    //                                                 },
//    //                                                 {
//    //                                                     "campaign_id" = 5692;
//    //                                                     description = "<null>";
//    //                                                     group = "Are there any further comments or suggestions that you would like to share? If so, please write these in the box below.";
//    //                                                     id = 96;
//    //                                                     max = 0;
//    //                                                     min = 0;
//    //                                                     options = "<null>";
//    //                                                     required = 0;
//    //                                                     title = "Your comments or suggestions:";
//    //                                                     type = textarea;
//    //                                                 }
//    //                                                 );
//    //                        type = "<null>";
//    //                        "user_id" = 7;
//    //                    }
//    //                    );
//    
//    //Save items to CoreData
//    self.numOfSaved --;
//    
//    //    suqCampaignId
//    //    suqDescription
//    //    suqGroup
//    //    suqMax
//    //    suqMin
//    //    suqRequired
//    //    suqSurveyId
//    //    suqSurveyQuestionId
//    //    suqTitle
//    //    suqType
//    
//    NSMutableArray *arraySurveyQuestions = [NSMutableArray arrayWithCapacity:10];
//    for (NSDictionary *dictSurvey in surveys)
//    {
//        if ([dictSurvey objectForKey:@"questions"])
//        {
//            
//            [arraySurveyQuestions addObjectsFromArray:[dictSurvey objectForKey:@"questions"]];
//        }
//    }
//    
//    NSDictionary* attributeMappings = [NSDictionary dictionaryWithObjectsAndKeys:
//                                       @"id", @"suqSurveyQuestionId",
//                                       @"campaign_id", @"suqCampaignId",
//                                       @"description", @"suqDescription",
//                                       @"group", @"suqGroup",
//                                       @"max", @"suqMax",
//                                       @"min", @"suqMin",
//                                       @"required", @"suqRequired",
//                                       @"title", @"suqTitle",
//                                       @"type", @"suqType",
//                                       nil];
//    
//    [[LLDataAccessLayer sharedInstance] insertEntitiesArray:arraySurveyQuestions withName:@"SurveyQuestion" receivedPredicatePrimaryKey:@"id" predicateKeyAttribute:@"suqSurveyQuestionId" sortDescriptorKey:@"suqSurveyQuestionId" activeAttributeName:@"deleted_at" attributeMappings:attributeMappings withCompletitionBlock:^(bool finished)
//     {
//         if(finished)
//         {
//             NSMutableDictionary* dict = [[[NSUserDefaults standardUserDefaults]valueForKey:@"Links"] mutableCopy];
//             [dict setObject:@"1" forKey:@"LinkedSurveyQuestionSurver"];
//             
//             [[NSUserDefaults standardUserDefaults]setValue:[NSDictionary dictionaryWithDictionary:dict] forKey:@"Links"];
//             [[NSUserDefaults standardUserDefaults]synchronize];
//             
//             self.numOfLinked --;
//             [self linkSurveyQuestionSurvey];
//         }
//     }];
//}
//
//-(void)syncSurveyAnswers
//{
//    NSLog(@"Call function %s",  __FUNCTION__);
//    
//    NSArray *arrayUnsincedSurveyAnswers = [SurveyAnswers getUnsyncedSurveysAnswers];
//    
//    if (arrayUnsincedSurveyAnswers.count == 0)
//    {
//        return;
//    }
//    //    {
//    //        "question_id":58,
//    //        "respondent_id":1,
//    //        "content": "asdasd"
//    //    },
//    NSMutableArray *arrayFormatedUnsycnedSurveyAnswers = [NSMutableArray arrayWithCapacity:10];
//    for (SurveyAnswers *sa in arrayUnsincedSurveyAnswers)
//    {
//        NSDictionary *dictAnswer = @{@"question_id" : sa.suaQuestionId, @"respondent_id" : @1, @"content" : sa.suaContent};
//        [arrayFormatedUnsycnedSurveyAnswers addObject:dictAnswer];
//    }
//    
//    [[LLWebServiceClient sharedInstance] sendRequestWithPath:@"answers"
//                                                      method:@"POST"
//                                                      params:arrayFormatedUnsycnedSurveyAnswers
//                                        successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
//     {
//         NSError *error = nil;
//         NSArray *jsonData = [NSJSONSerialization JSONObjectWithData:successResponseData options:NSJSONReadingMutableContainers error:&error];
//         
//         if (!jsonData.count)
//         {
//             NSLog(@"%s success - no json", __FUNCTION__);
//         }
//         else
//         {
//             NSLog(@"%s success", __FUNCTION__);
//             //Save items to CoreData
//             
//             [SurveyAnswers setSyncedSurveyAnswers:jsonData];
//         }
//         
//     }
//                                        failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
//     {
//         NSLog(@"%s failure error: %@", __FUNCTION__, error);
//     }];
//}

#pragma mark - Countries

-(void)syncCountries:(NSArray *)arrayCountries
{
    self.numOfSaved --;
    
    //             "id": 1,
    //             "name": "Anemiae"
    NSDictionary* attributeMappings = [NSDictionary dictionaryWithObjectsAndKeys:
                                       @"id", @"ctrCountryId",
                                       @"name", @"ctrName",
                                       @"code2", @"ctrCode",
                                       @"code3", @"ctrCode3",
                                       nil];
    
    //Save items to CoreData
    [[LLDataAccessLayer sharedInstance] insertEntitiesArray:arrayCountries withName:@"Country" receivedPredicatePrimaryKey:@"id" predicateKeyAttribute:@"ctrCountryId" sortDescriptorKey:@"ctrCountryId" activeAttributeName:@"deleted_at" attributeMappings:attributeMappings withCompletitionBlock:^(bool finished)
     {
     }];
}

#pragma mark - Banners

-(void)getBanner:(NSString *)position
withCompletionBlock:(void (^)(NSDictionary *banner))completion
{
    if (![[NSUserDefaults standardUserDefaults] objectForKey:kTypeOfInternetConnection])
    {
        completion(nil);
        return;
    }
    
    NSLog(@"Call function %s",  __FUNCTION__);
    
    NSString *bannerTypeByDevice = position;
    
    
    [[EWebServiceClient sharedInstance] sendRequestWithPath:[NSString stringWithFormat:@"banners/next?position=%@", [bannerTypeByDevice stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]
                                                     method:@"GET"
                                                     params:nil
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         NSError *error = nil;
         NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:successResponseData options:NSJSONReadingMutableContainers error:&error];
         
         if (!jsonData.count)
         {
             NSLog(@"%s success - no json", __FUNCTION__);
             completion(nil);
         }
         else
         {
             NSLog(@"%s success", __FUNCTION__);
//             NSLog(@"BANNERS %@", jsonData);
             
             NSDictionary *dictBanner = [jsonData objectForKey:@"data"];
             completion(dictBanner);
         }
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         NSLog(@"%s failure error: %@", __FUNCTION__, error);
         completion(nil);
     }];
}

// relation: "view" OR relation: "click"
-(void)trackUserAction:(NSString *)action
             forBanner:(NSNumber *)bannerId
            objectType:(NSString *)objectType
            objectInfo:(NSString *)objectInfo
{
    NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                            action, @"relation",
                            bannerId, @"object_id",
                            objectType, @"object_type",
                            objectInfo, @"object_info",
                            nil];
    
    NSLog(@"%s : %@",  __FUNCTION__, params);
    
    [[EWebServiceClient sharedInstance] sendRequestWithPath:@"track_user_actions"
                                                     method:@"POST"
                                                     params:params
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         NSLog(@"%s success", __FUNCTION__);
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         NSLog(@"%s failure error: %@", __FUNCTION__, error);
     }];
}

#pragma mark - Highlighting Slides

-(void)getHighlightedSlidesForMaterial:(Material *)material
                            completion:(void (^)(void))completion
{
    NSLog(@"Call function %s",  __FUNCTION__);
    
    [[EWebServiceClient sharedInstance] sendRequestWithPath:[NSString stringWithFormat:@"materials/%@/highlighted_slides", material.matMaterialId]
                                                     method:@"GET"
                                                     params:nil
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         NSError *error = nil;
         NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:successResponseData options:NSJSONReadingMutableContainers error:&error];
         
         if (!jsonData.count)
         {
             NSLog(@"%s success - no json", __FUNCTION__);
             completion();
         }
         else
         {
             NSLog(@"%s success", __FUNCTION__);
             
             // first delete highlighted slides for material
             // because there is no delete_at param
             // then it will be sync with web
             NSPredicate *predicate = [NSPredicate predicateWithFormat:@"hsMaterialId == %@", material.matMaterialId];
             [[LLDataAccessLayer sharedInstance] deleteAllDataForEntity:@"HighlightedSlide" withPredicate:predicate];
             
             //             data =     (
             //                         {
             //                             comments =             (
             //                             );
             //                             "conference_name" = "EBMT 2017";
             //                             id = 1688;
             //                             "material_id" = 5323;
             //                             page = 0;
             //                             "presentation_ends_at" = "2017-03-25 15:00:00";
             //                             "presentation_id" = 16008;
             //                             "presentation_starts_at" = "2017-03-25 13:30:00";
             //                             "presentation_title" = "Apheresis to obtain starting material for innovative cellular therapies";
             //                             "speaker_name" = "Adrian Gee";
             //                             "user_id" = 6663;
             //                         }
             //                         );
             //Save items to CoreData
             
             __block NSArray *arrayHighlightedSlides = [jsonData objectForKey:@"data"];
             
             NSDictionary* attributeMappings = [NSDictionary dictionaryWithObjectsAndKeys:
                                                @"id", @"hsSlideId",
                                                @"page", @"hsPage",
                                                @"material_id", @"hsMaterialId",
                                                nil];
             
             [[LLDataAccessLayer sharedInstance] insertEntitiesArray:arrayHighlightedSlides withName:@"HighlightedSlide" receivedPredicatePrimaryKey:@"id" predicateKeyAttribute:@"hsSlideId" sortDescriptorKey:@"hsSlideId" activeAttributeName:nil attributeMappings:attributeMappings withCompletitionBlock:^(bool finished)
              {
                  if (finished)
                  {
                      [self saveHighlightedSlideComments:arrayHighlightedSlides];
                      completion();
                  }
              }];
         }
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         NSLog(@"%s failure error: %@", __FUNCTION__, error);
         completion();
     }];
}

-(void)saveHighlightedSlideComments:(NSArray *)highlitedSlides
{
    NSMutableArray *arrayComments = [NSMutableArray arrayWithCapacity:10];
    for (NSDictionary *dictSlide in highlitedSlides)
    {
        if ([dictSlide objectForKey:@"comments"])
        {
            for (NSDictionary *dictComment in [dictSlide objectForKey:@"comments"])
            {
                NSMutableDictionary *dictNewCom = [NSMutableDictionary dictionaryWithDictionary:dictComment];
                [dictNewCom setObject:[dictSlide objectForKey:@"id"]  forKey:@"slide_id"];
                [arrayComments addObject:dictNewCom];
            }
        }
    }
    
    NSDictionary* attributeMappings = [NSDictionary dictionaryWithObjectsAndKeys:
                                       @"id", @"hscCommentId",
                                       @"comment", @"hscComment",
                                       @"slide_id", @"hscSlideId",
                                       nil];
    
    [[LLDataAccessLayer sharedInstance] insertEntitiesArray:arrayComments withName:@"HighlightedSlideComment" receivedPredicatePrimaryKey:@"id" predicateKeyAttribute:@"hscCommentId" sortDescriptorKey:@"hscCommentId" activeAttributeName:nil attributeMappings:attributeMappings withCompletitionBlock:^(bool finished)
     {
         if (finished)
         {
             [self linkHighlightedSlideAndComment];
         }
     }];
}

-(void)highlightedSlide:(NSNumber *)page forMaterial:(NSNumber *)materialId
                success:(void (^)(HighlightedSlide *slide))success
                failure:(void (^)(void))failure
{
    NSDictionary* params = @{@"page" : page};
    
    NSLog(@"%s : %@",  __FUNCTION__, params);
    
    [[EWebServiceClient sharedInstance] sendRequestWithPath:[NSString stringWithFormat:@"materials/%@/highlighted_slides", materialId]
                                                     method:@"POST"
                                                     params:params
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         NSError *error = nil;
         NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:successResponseData options:NSJSONReadingMutableContainers error:&error];
         
         if (!jsonData.count)
         {
             NSLog(@"%s success - no json", __FUNCTION__);
         }
         else
         {
             NSLog(@"%s success", __FUNCTION__);
             
             //             data =     {
             //                 comments =         (
             //                 );
             //                 "conference_name" = "EBMT 2017";
             //                 id = 1688;
             //                 "material_id" = 5323;
             //                 page = 0;
             //                 "presentation_ends_at" = "2017-03-25 15:00:00";
             //                 "presentation_id" = 16008;
             //                 "presentation_starts_at" = "2017-03-25 13:30:00";
             //                 "presentation_title" = "Apheresis to obtain starting material for innovative cellular therapies";
             //                 "speaker_name" = "Adrian Gee";
             //                 "user_id" = 6663;
             //             };
             
             NSDictionary *dictSlide = [jsonData objectForKey:@"data"];
             
             HighlightedSlide *hs = [NSEntityDescription insertNewObjectForEntityForName:@"HighlightedSlide" inManagedObjectContext:[LLDataAccessLayer sharedInstance].managedObjectContext];
             
             hs.hsSlideId = [NSNumber numberWithInteger:[[dictSlide objectForKey:@"id"] integerValue]];
             hs.hsPage = page;
             hs.hsMaterialId = materialId;
             hs.hsHighlighted = @YES;
             hs.synced = @YES;
             
             [[LLDataAccessLayer sharedInstance].managedObjectContext save:nil];
             
             success(hs);
         }
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         [self handleError:error withResponse:failureResponseData];
         failure();
     }];
}

-(void)unhighlightedSlide:(HighlightedSlide *)slide
                  success:(void (^)(void))success
                  failure:(void (^)(void))failure
{
    NSLog(@"%s",  __FUNCTION__);
    
    [[EWebServiceClient sharedInstance] sendRequestWithPath:[NSString stringWithFormat:@"highlighted_slides/%@", slide.hsSlideId]
                                                     method:@"DELETE"
                                                     params:nil
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         NSLog(@"%s success", __FUNCTION__);
         
         [[LLDataAccessLayer sharedInstance].managedObjectContext deleteObject:slide];
         [[LLDataAccessLayer sharedInstance].managedObjectContext save:nil];
         success();
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         [self handleError:error withResponse:failureResponseData];
         failure();
     }];
}

-(void)comment:(NSString *)comment highlightedSlide:(HighlightedSlide *)slide
       success:(void (^)(void))success
       failure:(void (^)(void))failure
{
    NSDictionary* params = @{@"comment" : comment};
    
    NSLog(@"%s : %@",  __FUNCTION__, params);
    
    [[EWebServiceClient sharedInstance] sendRequestWithPath:[NSString stringWithFormat:@"highlighted_slides/%@/comments", slide.hsSlideId]
                                                     method:@"POST"
                                                     params:params
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         NSError *error = nil;
         NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:successResponseData options:NSJSONReadingMutableContainers error:&error];
         
         if (!jsonData.count)
         {
             NSLog(@"%s success - no json", __FUNCTION__);
         }
         else
         {
             NSLog(@"%s success", __FUNCTION__);
             
             //             data =     {
             //                 comments =         (
             //                 );
             //                 "conference_name" = "EBMT 2017";
             //                 id = 1688;
             //                 "material_id" = 5323;
             //                 page = 0;
             //                 "presentation_ends_at" = "2017-03-25 15:00:00";
             //                 "presentation_id" = 16008;
             //                 "presentation_starts_at" = "2017-03-25 13:30:00";
             //                 "presentation_title" = "Apheresis to obtain starting material for innovative cellular therapies";
             //                 "speaker_name" = "Adrian Gee";
             //                 "user_id" = 6663;
             //             };
             
             NSDictionary *dictSlideComment = [jsonData objectForKey:@"data"];
             
             HighlightedSlideComment *hsc = [NSEntityDescription insertNewObjectForEntityForName:@"HighlightedSlideComment" inManagedObjectContext:[LLDataAccessLayer sharedInstance].managedObjectContext];
             
             hsc.hscCommentId = [NSNumber numberWithInteger:[[dictSlideComment objectForKey:@"id"] integerValue]];
             hsc.hscComment = comment;
             hsc.hscSlideId = slide.hsSlideId;
             hsc.slide = slide;
             
             [[LLDataAccessLayer sharedInstance].managedObjectContext save:nil];
             
             success();
         }
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         [self handleError:error withResponse:failureResponseData];
         failure();
     }];
}

-(void)deleteComment:(HighlightedSlideComment *)comment
             success:(void (^)(void))success
             failure:(void (^)(void))failure
{
    [[EWebServiceClient sharedInstance] sendRequestWithPath:[NSString stringWithFormat:@"slide_comments/%@", comment.hscCommentId]
                                                     method:@"DELETE"
                                                     params:nil
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         NSLog(@"%s success", __FUNCTION__);
         
         [[LLDataAccessLayer sharedInstance].managedObjectContext deleteObject:comment];
         [[LLDataAccessLayer sharedInstance].managedObjectContext save:nil];
         success();
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         [self handleError:error withResponse:failureResponseData];
         failure();
     }];
}

#pragma mark - Orders

-(void)getOrders
{
    NSLog(@"%s",  __FUNCTION__);
    
    NSString *url = [NSString stringWithFormat:@"orders/ids?with_trashed=1&conference_id=%@", [[RealmRepository shared] getSelectedConferenceId]];
    [[EWebServiceClient sharedInstance] sendRequestWithPath:url
                                                     method:@"GET"
                                                     params:nil
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         NSError *error = nil;
         NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:successResponseData options:NSJSONReadingMutableContainers error:&error];
         
         if (!jsonData.count)
         {
             NSLog(@"%s success - no json", __FUNCTION__);
         }
         else
         {
             [Order deleteAllOrders];
             NSArray *arrayOrders = [[jsonData objectForKey:@"data"] objectForKey:@"presentation_ids"];
             [Order insertOrders:arrayOrders withCompletitionBlock:^{
                 NSLog(@"%s success", __FUNCTION__);
             }];
         }
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         NSLog(@"%s failure error: %@", __FUNCTION__, error);
     }];
}

-(void)getOrdersWithCompletitionBlock:(void (^)(void))finished
{
    NSLog(@"%s",  __FUNCTION__);
    
    
    NSString *url = [NSString stringWithFormat:@"orders/ids?with_trashed=1&conference_id=%@", [[RealmRepository shared] getSelectedConferenceId]];
    [[EWebServiceClient sharedInstance] sendRequestWithPath:url
                                                     method:@"GET"
                                                     params:nil
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         NSError *error = nil;
         NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:successResponseData options:NSJSONReadingMutableContainers error:&error];
         
         if (!jsonData.count)
         {
             NSLog(@"%s success - no json", __FUNCTION__);
             finished();
         }
         else
         {
             [Order deleteAllOrders];
             NSArray *arrayOrders = [[jsonData objectForKey:@"data"] objectForKey:@"presentation_ids"];
             [Order insertOrders:arrayOrders withCompletitionBlock:^{
                 NSLog(@"%s success", __FUNCTION__);
                 finished();
             }];
         }
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         NSLog(@"%s failure error: %@", __FUNCTION__, error);
         finished();
     }];
}

#pragma mark - Order talk

-(void)orderderTalk:(Talk *)talk
            success:(void (^)(void))success
            failure:(void (^)(void))failure
{
    NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   [NSArray arrayWithObjects:[talk.tlkTalkId stringValue], nil] , @"presentation_ids",
                                   nil];
    
    NSLog(@"%s : %@",  __FUNCTION__, params);
    
    [[EWebServiceClient sharedInstance] sendRequestWithPath:@"orders"
                                                     method:@"POST"
                                                     params:params
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         NSError *error = nil;
         __block NSDictionary* jsonData = [NSJSONSerialization JSONObjectWithData:successResponseData options:NSJSONReadingMutableContainers error:&error];
         
         if (!jsonData.count)
         {
             NSLog(@"%s success - no json", __FUNCTION__);
             [Utils showMessage:kMessageInvalideResponse withTitle:@"Error"];
             failure();
         }
         else
         {
             NSLog(@"%s success", __FUNCTION__);
             
             [Order addOrder:[[jsonData[@"data"] firstObject] objectForKey:@"id"] forPresentation:talk.tlkTalkId];
             [self getMaterialsForPresentationWithoutDel:talk.tlkTalkId WithCompletitionBlock:^{
                 success();
             }];
         }
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         NSLog(@"%s failure error: %@", __FUNCTION__, error);
         NSString *message = @"";
         NSString *title = @"Error";
         
         if(failureResponseData.responseData)
         {
             NSError *jsonError = nil;
             NSDictionary* jsonData = [NSJSONSerialization JSONObjectWithData:failureResponseData.responseData options:NSJSONReadingMutableContainers error:&jsonError];
             
             if (!jsonData.count)
             {
                 NSLog(@"%s failure json error %@", __FUNCTION__, jsonError);
             }
             
             NSString* msgParsed = [[ESyncData sharedInstance] parseServerError:jsonData];
             message = [msgParsed stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
         }
         else
         {
             message =  [NSString stringWithFormat:@"%@", [error localizedDescription]];;
         }
         
         if ([message containsString:@"These presentations starts later"])
         {
             message = @"This presentation hasn't started yet. You can only add presentations to your library once they have started.";
             title = @"";
         }
         
         [Utils showMessage:message withTitle:@"Error"];
         failure();
     }];
}

-(void)addOrdersToLibrary:(NSArray *)arrayTalks
                  success:(void (^)(void))success
                  failure:(void (^)(void))failure
{
    NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                            arrayTalks , @"presentation_ids",
                            nil];
    
    NSLog(@"%s : %@",  __FUNCTION__, params);
    
    [[EWebServiceClient sharedInstance] sendRequestWithPath:@"orders"
                                                     method:@"POST"
                                                     params:params
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         NSError *error = nil;
         NSDictionary* jsonData = [NSJSONSerialization JSONObjectWithData:successResponseData options:NSJSONReadingMutableContainers error:&error];
         
         if (!jsonData.count)
         {
             NSLog(@"%s success - no json", __FUNCTION__);
             [Utils showMessage:kMessageInvalideResponse withTitle:@"Error"];
             failure();
         }
         else
         {
             NSLog(@"%s success", __FUNCTION__);
             NSArray *arrayData = [jsonData objectForKey:@"data"];
             for (NSDictionary *dicOrder in arrayData)
             {
                 [Order addOrder:[dicOrder objectForKey:@"id"] forPresentation:[dicOrder objectForKey:@"presentation_id"]];
                 [self getMaterialsForPresentationWithoutDel:[dicOrder objectForKey:@"presentation_id"] WithCompletitionBlock:^{
                     success();
                 }];
             }
         }
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         [self handleError:error withResponse:failureResponseData];
         failure();
     }];
}

// ------------------------------------------------------------------------------------------------------------------------------------------------

#pragma mark - Materials


-(void)getImagesForMaterial:(Material *)material
                    success:(void (^)(NSArray *images))success
                    failure:(void (^)(void))failure
{
    NSLog(@"%s",  __FUNCTION__);
    
    [[EWebServiceClient sharedInstance] sendRequestWithPath:[NSString stringWithFormat:@"materials/%@/images", material.matMaterialId]
                                                     method:@"GET"
                                                     params:nil
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         NSError *error = nil;
         NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:successResponseData options:NSJSONReadingMutableContainers error:&error];
         
         if (!jsonData.count)
         {
             NSLog(@"%s success - no json", __FUNCTION__);
             failure();
         }
         else
         {
             NSArray *arrayOfImages = [jsonData objectForKey:@"images"];
             success(arrayOfImages);
         }
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         NSLog(@"%s failure error: %@", __FUNCTION__, error);
         failure();
     }];
}

-(void)downloadMaterial:(Material *)material
                success:(void (^)(id responseObject))success
                failure:(void (^)(void))failure
{
    [self downloadFileRetryingNumberOfTimes:5 forMaterial:material
                                    success:^(id responseObject)
     {
         NSLog(@"Success download: %@", responseObject);
         success(responseObject);
     }
                                    failure:^(NSError *error)
     {
         // Can't get real error from request, but set error in downloadFileRetryingNumberOfTimes function
         NSLog(@"Error resending: %@", [[error userInfo] objectForKey:@"error"]);
         
         [Utils showMessage:[[error userInfo] objectForKey:@"error"] withTitle:@"Error"];
         failure();
     }];
}

-(void)downloadFileRetryingNumberOfTimes:(NSUInteger)ntimes forMaterial:(Material *)material
                                 success:(void (^)(id responseObject))success
                                 failure:(void (^)(NSError *error))failure
{
    if (ntimes <= 0)
    {
        if (failure)
        {
            NSError *error = [[NSError alloc] initWithDomain:@"Download" code:1 userInfo:[NSDictionary dictionaryWithObject:@"File isn't ready yet." forKey:@"error"]];
            failure(error);
        }
    }
    else
    {
        // /Users/iMaja/Library/Developer/CoreSimulator/Devices/7191433C-6DA5-4DA8-8408-485B1BD3EB7F/data/Containers/Data/Application/3C58C4F2-494F-4BB0-9B58-D14071233E8B/
        // PATH IS CHANGABLE -> BE CARFULE, DONT SAVE IT ANYWHERE
        NSString *libraryPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        
        // /Users/iMaja/Library/Developer/CoreSimulator/Devices/7191433C-6DA5-4DA8-8408-485B1BD3EB7F/data/Containers/Data/Application/3C58C4F2-494F-4BB0-9B58-D14071233E8B/Library/materials/presentation49
        NSString *filePath = [NSString stringWithFormat:@"%@/materials/presentation%@", libraryPath, material.matTalkId]; //fileId
        
        if (![[NSFileManager defaultManager] fileExistsAtPath:filePath]) // If folder doesnt exist
        {
            [[NSFileManager defaultManager] createDirectoryAtPath:filePath withIntermediateDirectories:YES attributes:nil error:nil]; // create it
        }
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:[NSString stringWithFormat:@"%@/%@.zip", filePath, material.matMaterialId]]) // File exist.
        {
            if([[[NSUserDefaults standardUserDefaults] valueForKey:kTypeOfInternetConnection] isEqualToString:@"WWAN"]) // File exist. If mobile net. Dont dowload again -> return
            {
                success(@"success");
                return;
            }
            else if ([[NSUserDefaults standardUserDefaults] valueForKey:kTypeOfInternetConnection]) // File exist. Other type of connection (Wi-Fi). Delete file and dowload again.
            {
                [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@/%@.zip", filePath, material.matMaterialId] error:nil];
            }
        }
        
        User *currentUser = [User current];
        NSString *fileURL = [NSString stringWithFormat:@"%@materials/%@/images/zip?token=%@&resolution=80&quality=80", kBaseAPIURL, material.matMaterialId, currentUser.token];
        
        NSLog(@"Request for file: %@", fileURL);
        
        NSURLRequest *request = [[EWebServiceClient sharedInstance] requestWithMethod:@"GET" path:fileURL parameters:nil];
        
        // Requesting for file
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        
        // /Users/iMaja/Library/Developer/CoreSimulator/Devices/7191433C-6DA5-4DA8-8408-485B1BD3EB7F/data/Containers/Data/Application/3C58C4F2-494F-4BB0-9B58-D14071233E8B/Library/materials/presentation49/material27.pdf
        
        operation.outputStream = [NSOutputStream outputStreamToFileAtPath:[NSString stringWithFormat:@"%@/%@.zip", filePath, material.matMaterialId] append:NO];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             if (success)
             {
                 NSLog(@"Received file.!!!");
                 success(responseObject);
             }
         }
                                         failure:^(AFHTTPRequestOperation *operation, NSError *error)
         {
             NSLog(@"LOCAL URL: %@, error: %@", operation.request.URL,  error);
             //Resend failed file download
             [self downloadFileRetryingNumberOfTimes:ntimes - 1 forMaterial:material success:success failure:failure];
         }];
        
        [[EWebServiceClient sharedInstance] enqueueHTTPRequestOperation:operation];
    }
}

-(void)getMaterialsForPresentation:(NSNumber *)presentationId
             WithCompletitionBlock:(void (^)(void))finished
{
    NSLog(@"%s",  __FUNCTION__);
    
    [[EWebServiceClient sharedInstance] sendRequestWithPath:[NSString stringWithFormat:@"presentations/%@?include=materials", presentationId]
                                                     method:@"GET"
                                                     params:nil
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         NSError *error = nil;
         NSDictionary* jsonData = [NSJSONSerialization JSONObjectWithData:successResponseData options:NSJSONReadingMutableContainers error:&error];
         
         if (!jsonData.count)
         {
             NSLog(@"%s success - no json", __FUNCTION__);
             finished();
         }
         else
         {
             NSArray *arrayMaterials = [[jsonData objectForKey:@"data"] objectForKey:@"materials"];
             
             [Talk addMaterials:arrayMaterials forPresentation:presentationId];
             
             NSLog(@"%s success", __FUNCTION__);
             
             finished();
         }
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         NSLog(@"%s failure error: %@", __FUNCTION__, error);
         finished();
     }];
}

-(void)getMaterialsForPresentationWithoutDel:(NSNumber *)presentationId
                       WithCompletitionBlock:(void (^)(void))finished
{
    NSLog(@"%s",  __FUNCTION__);
    
    [[EWebServiceClient sharedInstance] sendRequestWithPath:[NSString stringWithFormat:@"presentations/%@?include=materials", presentationId]
                                                     method:@"GET"
                                                     params:nil
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         NSError *error = nil;
         NSDictionary* jsonData = [NSJSONSerialization JSONObjectWithData:successResponseData options:NSJSONReadingMutableContainers error:&error];
         
         if (!jsonData.count)
         {
             NSLog(@"%s success - no json", __FUNCTION__);
             finished();
         }
         else
         {
             NSArray *arrayMaterials = [[jsonData objectForKey:@"data"] objectForKey:@"materials"];
             
             [Talk addMaterialsWithoutDelete:arrayMaterials forPresentation:presentationId];
             
             NSLog(@"%s success", __FUNCTION__);
             
             finished();
         }
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         NSLog(@"%s failure error: %@", __FUNCTION__, error);
         finished();
     }];
}

// ------------------------------------------------------------------------------------------------------------------------------------------------

#pragma mark - Epoints

-(void)getPredefinePartyActions
{
    NSLog(@"%s",  __FUNCTION__);
    
    [[EWebServiceClient sharedInstance] sendRequestWithPath:@"conference_party_actions"
                                                     method:@"GET"
                                                     params:nil
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         NSError *error = nil;
         NSDictionary* jsonData = [NSJSONSerialization JSONObjectWithData:successResponseData options:NSJSONReadingMutableContainers error:&error];
         
         if (!jsonData.count)
         {
             NSLog(@"%s success - no json", __FUNCTION__);
         }
         else
         {
             NSDictionary *dicEpointsPredefinedStructure = [jsonData objectForKey:@"data"];
             [Epoint saveEpoints:dicEpointsPredefinedStructure];
             NSLog(@"%s success", __FUNCTION__);
         }
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         NSLog(@"%s failure error: %@", __FUNCTION__, error);
     }];
}

-(void)getEpointsWithCompletitionBlock:(void (^)(void))finished
{
    NSLog(@"%s",  __FUNCTION__);
    
    [[EWebServiceClient sharedInstance] sendRequestWithPath:[NSString stringWithFormat:@"users/me?conference_id=%@", [[RealmRepository shared] getSelectedConferenceId]]
                                                     method:@"GET"
                                                     params:nil
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         NSError *error = nil;
         NSDictionary* jsonData = [NSJSONSerialization JSONObjectWithData:successResponseData options:NSJSONReadingMutableContainers error:&error];
         
         if (!jsonData.count)
         {
             NSLog(@"%s success - no json", __FUNCTION__);
         }
         else
         {
             NSDictionary *dicParty = [[jsonData objectForKey:@"data"] objectForKey:@"party"];
             NSString* available = [dicParty objectForKey:@"e_points"] ? [dicParty objectForKey:@"e_points"] : @"0";
             NSString* spent = [dicParty objectForKey:@"e_points_used"] ? [dicParty objectForKey:@"e_points_used"] : @"0";
             
             if ([available isEqual:@-1])
             {
                 available = @"∞";
             }
             
             [[NSUserDefaults standardUserDefaults] setObject:available forKey:@"available"];
             [[NSUserDefaults standardUserDefaults] setObject:spent forKey:@"spent"];
             [[NSUserDefaults standardUserDefaults] synchronize];
             
             NSLog(@"%s success", __FUNCTION__);
         }
         finished();
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         NSLog(@"%s failure error: %@", __FUNCTION__, error);
         finished();
     }];
}

-(void)getEpointsWithSuccess:(void (^)(int epoints))success
                     failure:(void (^)(void))failure
{
    [[EWebServiceClient sharedInstance] sendRequestWithPath:[NSString stringWithFormat:@"users/me?conference_id=%@", [[RealmRepository shared] getSelectedConferenceId]]
                                                     method:@"GET"
                                                     params:nil
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         NSError *error = nil;
         NSDictionary* jsonData = [NSJSONSerialization JSONObjectWithData:successResponseData options:NSJSONReadingMutableContainers error:&error];
         
         if (!jsonData.count)
         {
             NSLog(@"%s success - no json", __FUNCTION__);
             [Utils showMessage:kMessageInvalideResponse withTitle:@"Error"];
             failure();
         }
         else
         {
             NSLog(@"%s success", __FUNCTION__);
             
             // get e-points
             NSDictionary *dicUserData = [jsonData objectForKey:@"data"];
             NSDictionary *dictEpoints = [dicUserData objectForKey:@"e_points"];
             NSString *currentConferenceId = [[RealmRepository shared] getSelectedConferenceId];
             int epoints = 0;
             
             if ([dictEpoints objectForKey:currentConferenceId] != nil)
             {
                 epoints = [[dictEpoints objectForKey:currentConferenceId] intValue];
             }
             
             success(epoints);
         }
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         [self handleError:error withResponse:failureResponseData];
         failure();
     }];
}

-(void)getPresentationsCost:(NSNumber *)presentationID
                withSuccess:(void (^)(int cost))success
                    failure:(void (^)(void))failure
{
    [[EWebServiceClient sharedInstance] sendRequestWithPath:@"presentations/costs"
                                                     method:@"GET"
                                                     params:@{@"presentation_ids" : @[presentationID]}
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         NSError *error = nil;
         NSDictionary* jsonData = [NSJSONSerialization JSONObjectWithData:successResponseData options:NSJSONReadingMutableContainers error:&error];
         
         if (!jsonData.count)
         {
             NSLog(@"%s success - no json", __FUNCTION__);
             [Utils showMessage:kMessageInvalideResponse withTitle:@"Error"];
             failure();
         }
         else
         {
             NSLog(@"%s success", __FUNCTION__);
             
             //             data =     (
             //                         {
             //                             cost = 1;
             //                             ordered = 1;
             //                             "presentation_id" = 24091;
             //                         }
             //                         );
             
             //             // get cost
             NSArray *arrayCosts = [jsonData objectForKey:@"data"];
             if (arrayCosts.count > 0)
             {
                 NSDictionary *dictCost = [arrayCosts objectAtIndex:0];
                 int presentationsCost = [[dictCost objectForKey:@"cost"] intValue];
                 success(presentationsCost);
             }
             else
             {
                 [Utils showMessage:kMessageInvalideResponse withTitle:@"Error"];
                 failure();
             }
         }
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         NSLog(@"%s failure error: %@", __FUNCTION__, error);
         [self handleError:error withResponse:failureResponseData];
         failure();
     }];
}

-(void)getUsersEpointsStructureWithCompletitionBlock:(void (^)(void))finished
{
    NSLog(@"%s",  __FUNCTION__);
    
    [[EWebServiceClient sharedInstance] sendRequestWithPath:@"my_party_actions"
                                                     method:@"GET"
                                                     params:nil
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         NSError *error = nil;
         NSDictionary* jsonData = [NSJSONSerialization JSONObjectWithData:successResponseData options:NSJSONReadingMutableContainers error:&error];
         
         if (!jsonData.count)
         {
             NSLog(@"%s success - no json", __FUNCTION__);
             finished();
         }
         else
         {
             NSArray *arrayUsersEpoints = [jsonData objectForKey:@"data"];
             [Epoint updateEpoints:arrayUsersEpoints WithCompletitionBlock:^{
                 NSLog(@"%s success", __FUNCTION__);
                 finished();
             }];
         }
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         NSLog(@"%s failure error: %@", __FUNCTION__, error);
         finished();
     }];
}

// ------------------------------------------------------------------------------------------------------------------------------------------------

#pragma mark - User

-(void)checkCode:(NSString *)code ForCountry:(Country *)country
         success:(void (^)(NSDictionary *responseObject))success
         failure:(void (^)(void))failure
{
    NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                            code, @"code",
                            country.ctrCountryId, @"country_id",
                            [[RealmRepository shared] getSelectedConferenceId], @"conference_id",
                            nil];
    
    NSLog(@"%s : %@",  __FUNCTION__, params);
    
    [[EWebServiceClient sharedInstance] sendRequestWithPath:@"checkCode"
                                                     method:@"POST"
                                                     params:params
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         NSError *error = nil;
         NSDictionary* jsonData = [NSJSONSerialization JSONObjectWithData:successResponseData options:NSJSONReadingMutableContainers error:&error];
         
         if (!jsonData.count)
         {
             NSLog(@"%s success - no json", __FUNCTION__);
             [Utils showMessage:kMessageInvalideResponse withTitle:@"Error"];
             failure();
         }
         else
         {
             success([jsonData objectForKey:@"data"]);
         }
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         [self handleError:error withResponse:failureResponseData];
         failure();
     }];
}

-(void)limitedLogin:(NSDictionary *)dicUserData
            success:(void (^)(void))success
            failure:(void (^)(void))failure
{
    NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                            [dicUserData valueForKey:@"email"], @"email",
                            [[RealmRepository shared] getSelectedConferenceId], @"conference_id",
                            nil];
    
    NSLog(@"%s : %@",  __FUNCTION__, params);
    
    [[EWebServiceClient sharedInstance] sendRequestWithPath:@"limitedLogin"
                                                     method:@"POST"
                                                     params:params
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         NSError *error = nil;
         NSDictionary* jsonData = [NSJSONSerialization JSONObjectWithData:successResponseData options:NSJSONReadingMutableContainers error:&error];
         
         if (!jsonData.count)
         {
             [Utils showMessage:kMessageInvalideResponse withTitle:@"Error"];
             failure();
         }
         else
         {
             [self setAuthorizationHeaderWithToken:[[jsonData objectForKey:@"data"] objectForKey:@"token"]];
             
             [[NSUserDefaults standardUserDefaults] setValue:[[jsonData objectForKey:@"data"] objectForKey:@"token"] forKey:kUserToken];
             [[NSUserDefaults standardUserDefaults] synchronize];
             
             success();
         }
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         [self handleError:error withResponse:failureResponseData];
         failure();
     }];
}

-(void)loginWithUsername:(NSString *)username AndPassword:(NSString *)password
                 success:(void (^)(void))success
                 failure:(void (^)(void))failure
{
    NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                            username, @"email",
                            password, @"password",
                            nil];
    
    NSLog(@"%s : %@",  __FUNCTION__, params);
    
    [[EWebServiceClient sharedInstance] sendRequestWithPath:@"login"
                                                     method:@"POST"
                                                     params:params
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         NSError *error = nil;
         NSDictionary* jsonData = [NSJSONSerialization JSONObjectWithData:successResponseData options:NSJSONReadingMutableContainers error:&error];
         
         if (!jsonData.count)
         {
             [Utils showMessage:kMessageInvalideResponse withTitle:@"Error"];
             failure();
         }
         else
         {
             [self setAuthorizationHeaderWithToken:[[jsonData objectForKey:@"data"] objectForKey:@"token"]];
             
             [[NSUserDefaults standardUserDefaults] setValue:[[jsonData objectForKey:@"data"] objectForKey:@"token"] forKey:kUserToken];
             [[NSUserDefaults standardUserDefaults] synchronize];
             
             success();
         }
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         [self handleError:error withResponse:failureResponseData];
         failure();
     }];
}

-(void)setAuthorizationHeaderWithToken:(NSString *)token
{
    if (!token)
    {
        [[EWebServiceClient sharedInstance] setDefaultHeader:@"Authorization" value:nil];
    }
    else
    {
        [[EWebServiceClient sharedInstance] setDefaultHeader:@"Authorization" value:[NSString stringWithFormat:@"Bearer %@", token]];
    }
}

-(void)setApiKeyInHeader:(NSString *)apiKey
{
    [[EWebServiceClient sharedInstance] setDefaultHeader:@"Api-Key" value:apiKey];
}

-(void)setHeaderWithValue:(NSString *)value forKey:(NSString *)key
{
    [[EWebServiceClient sharedInstance] setDefaultHeader:key value:value];
}

-(void)checkEmail:(NSString *)email
          success:(void (^)(void))success
          failure:(void (^)(void))failure
{
    NSLog(@"%s email %@",  __FUNCTION__, email);
    
    [[EWebServiceClient sharedInstance] sendRequestWithPath:[NSString stringWithFormat:@"checkEmail/%@", email]
                                                     method:@"GET"
                                                     params:nil
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         NSLog(@"%s success", __FUNCTION__);
         success();
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         NSLog(@"%s failure error: %@", __FUNCTION__, error);
         failure();
     }];
}

-(void)registerUser:(NSDictionary *)params
            success:(void (^)(NSDictionary *dicResponse))success
            failure:(void (^)(void))failure
{
    NSLog(@"%s : %@",  __FUNCTION__, params);
    
    [[EWebServiceClient sharedInstance] sendRequestWithPath:@"register"
                                                     method:@"POST"
                                                     params:params
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         NSError *error = nil;
         __block NSDictionary* jsonData = [NSJSONSerialization JSONObjectWithData:successResponseData options:NSJSONReadingMutableContainers error:&error];
         
         if (!jsonData.count)
         {
             NSLog(@"%s success - no json", __FUNCTION__);
             [Utils showMessage:kMessageInvalideResponse withTitle:@"Error"];
             failure();
         }
         else
         {
             success(jsonData);
         }
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         [self handleError:error withResponse:failureResponseData];
         failure();
     }];
}

-(void)connectAccount:(NSNumber *)partyId
              success:(void (^)(void))success
              failure:(void (^)(void))failure
{
    NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                            partyId, @"party_id", nil];
    
    NSLog(@"%s : %@",  __FUNCTION__, params);
    
    [[EWebServiceClient sharedInstance] sendRequestWithPath:@"users/connect"
                                                     method:@"POST"
                                                     params:params
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         NSLog(@"%s success", __FUNCTION__);
         
         [self syncUserData:NO];
         
         success();
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         NSLog(@"%s failure error: %@", __FUNCTION__, error);
         
         NSString *message = @"";
         
         if(failureResponseData.responseData)
         {
             NSError *jsonError = nil;
             NSDictionary* jsonData = [NSJSONSerialization JSONObjectWithData:failureResponseData.responseData options:NSJSONReadingMutableContainers error:&jsonError];
             
             if (!jsonData.count)
             {
                 NSLog(@"%s failure json error %@", __FUNCTION__, jsonError);
             }
             else if ([jsonData objectForKey:@"message"])
             {
                 message = [jsonData objectForKey:@"message"];
             }
             else
             {
                 NSString* msgParsed = [[ESyncData sharedInstance] parseServerError:jsonData];
                 message = [msgParsed stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
             }
         }
         else
         {
             message =  [NSString stringWithFormat:@"%@", [error localizedDescription]];;
         }
         
         if ([message isEqualToString:@"user.already_connected"])
         {
             [self syncUserData:NO];
             
             success();
         }
         else
         {
             [Utils showMessage:message withTitle:@"Error"];
             failure();
         }
     }];
}

-(void)connectAccountWithoutGetProfile:(NSNumber *)partyId
                               success:(void (^)(void))success
                               failure:(void (^)(void))failure
{
    NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                            partyId, @"party_id", nil];
    
    NSLog(@"%s : %@",  __FUNCTION__, params);
    
    [[EWebServiceClient sharedInstance] sendRequestWithPath:@"users/connect"
                                                     method:@"POST"
                                                     params:params
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         NSLog(@"%s success", __FUNCTION__);
         success();
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         NSLog(@"%s failure error: %@", __FUNCTION__, error);
         
         NSString *message = @"";
         
         if(failureResponseData.responseData)
         {
             NSError *jsonError = nil;
             NSDictionary* jsonData = [NSJSONSerialization JSONObjectWithData:failureResponseData.responseData options:NSJSONReadingMutableContainers error:&jsonError];
             
             if (!jsonData.count)
             {
                 NSLog(@"%s failure json error %@", __FUNCTION__, jsonError);
             }
             else if ([jsonData objectForKey:@"message"])
             {
                 message = [jsonData objectForKey:@"message"];
             }
             else
             {
                 NSString* msgParsed = [[ESyncData sharedInstance] parseServerError:jsonData];
                 message = [msgParsed stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
             }
         }
         else
         {
             message =  [NSString stringWithFormat:@"%@", [error localizedDescription]];;
         }
         
         if ([message isEqualToString:@"user.already_connected"])
         {
             success();
         }
         else
         {
             [Utils showMessage:message withTitle:@"Error"];
             failure();
         }
         
     }];
}

-(void)addBadge:(NSString *)badge
        success:(void (^)(NSArray *arrayResponse))success
        failure:(void (^)(void))failure
{
    NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                            badge, @"code", nil];
    
    NSLog(@"%s : %@",  __FUNCTION__, params);
    
    [[EWebServiceClient sharedInstance] sendRequestWithPath:@"portals/get_parties_with_code"
                                                     method:@"POST"
                                                     params:params
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         NSError *error = nil;
         NSDictionary* jsonData = [NSJSONSerialization JSONObjectWithData:successResponseData options:NSJSONReadingMutableContainers error:&error];
         
         if (!jsonData.count)
         {
             NSLog(@"%s success - no json", __FUNCTION__);
             [Utils showMessage:kMessageInvalideResponse withTitle:@"Error"];
             failure();
         }
         else
         {
             success([jsonData objectForKey:@"data"]);
         }
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         [self handleError:error withResponse:failureResponseData];
         failure();
     }];
}

-(void)getProfile:(BOOL)optIn
{
    NSLog(@"%s",  __FUNCTION__);
    
    [[EWebServiceClient sharedInstance] sendRequestWithPath:[NSString stringWithFormat:@"users/me?conference_id=%@", [[RealmRepository shared] getSelectedConferenceId]]
                                                     method:@"GET"
                                                     params:nil
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         NSError *error = nil;
         NSDictionary* jsonData = [NSJSONSerialization JSONObjectWithData:successResponseData options:NSJSONReadingMutableContainers error:&error];
         
         if (!jsonData.count)
         {
             NSLog(@"%s success - no json", __FUNCTION__);
         }
         else
         {
             NSDictionary *dicUserData = [jsonData objectForKey:@"data"];
             [User saveFrom:dicUserData optIn:optIn];
             if ([dicUserData valueForKey:@"id"] && ![[NSUserDefaults standardUserDefaults] valueForKey:@"user_id"])
             {
                 [[NSUserDefaults standardUserDefaults] setValue:[[dicUserData valueForKey:@"id"] stringValue] forKey:@"user_id"];
                 [[NSUserDefaults standardUserDefaults] synchronize];
                 
                 [self registerDevice];
             }
             
             NSLog(@"%s success", __FUNCTION__);
         }
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         NSLog(@"%s failure error: %@", __FUNCTION__, error);
     }];
}

-(void)getProfileWithSuccess:(void (^)(int points))success
                     failure:(void (^)(void))failure
{
    NSLog(@"%s",  __FUNCTION__);
    
    [[EWebServiceClient sharedInstance] sendRequestWithPath:[NSString stringWithFormat:@"users/me?conference_id=%@", [[RealmRepository shared] getSelectedConferenceId]]
                                                     method:@"GET"
                                                     params:nil
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         NSError *error = nil;
         NSDictionary* jsonData = [NSJSONSerialization JSONObjectWithData:successResponseData options:NSJSONReadingMutableContainers error:&error];
         
         if (!jsonData.count)
         {
             NSLog(@"%s success - no json", __FUNCTION__);
             failure();
         }
         else
         {
             NSDictionary *dicUserData = [jsonData objectForKey:@"data"];
             [User saveFrom:dicUserData optIn:NO];
             
             if ([dicUserData valueForKey:@"id"] && ![[NSUserDefaults standardUserDefaults] valueForKey:@"user_id"])
             {
                 [[NSUserDefaults standardUserDefaults] setValue:[[dicUserData valueForKey:@"id"] stringValue] forKey:@"user_id"];
                 [[NSUserDefaults standardUserDefaults] synchronize];
                 
                 [self registerDevice];
             }
             
             // get e-points
             NSDictionary *dictEpoints = [dicUserData objectForKey:@"e_points"];
             NSString *currentConferenceId = [[RealmRepository shared] getSelectedConferenceId];
             int epoints = 0;
             
             if ([dictEpoints objectForKey:currentConferenceId] != nil)
             {
                 epoints = [[dictEpoints objectForKey:currentConferenceId] intValue];
             }
             
             success(epoints);
             
             NSLog(@"%s success", __FUNCTION__);
         }
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         NSLog(@"%s failure error: %@", __FUNCTION__, error);
         failure();
     }];
}

-(void)resetLoginProperties
{
    // reset user data, because user have to login before connect account
    [self setAuthorizationHeaderWithToken:nil];
    [[NSUserDefaults standardUserDefaults] setValue:nil forKey:kUserToken];
    //    [[NSUserDefaults standardUserDefaults] setValue:nil forKey:kUsersActionVifor];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)setUserAction:(NSString *)action
{
    NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                            action , @"action_name",
                            [[RealmRepository shared] getSelectedConferenceId], @"conference_id",
                            nil];
    
    NSLog(@"%s : %@",  __FUNCTION__, params);
    [[EWebServiceClient sharedInstance] sendRequestWithPath:[NSString stringWithFormat:@"users/%ld/actions", [User current].id]
                                                     method:@"POST"
                                                     params:params
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         NSLog(@"%s success", __FUNCTION__);
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         NSLog(@"%s failure error: %@", __FUNCTION__, error);
     }];
}

-(void)syncMaterialBrowsings
{
    __block NSArray *arrayUnsyncedMaterialBrowsings = [MaterialBrowsing getUnsyncedMaterialBrowsings];
    if (!arrayUnsyncedMaterialBrowsings || ![[NSUserDefaults standardUserDefaults] objectForKey:kTypeOfInternetConnection])
    {
        return;
    }
    
    NSMutableArray *arrayMatBrow = [[NSMutableArray alloc] initWithCapacity:10];
    for (MaterialBrowsing *matB in arrayUnsyncedMaterialBrowsings)
    {
        NSDictionary* dictMatB = [NSDictionary dictionaryWithObjectsAndKeys:
                                  matB.objectId , @"object_id",
                                  matB.objectType , @"object_type",
                                  matB.sessionId , @"session_id",
                                  matB.duration , @"duration",
                                  matB.objectInfo , @"object_info",
                                  matB.relation , @"relation",
                                  nil];
        
        [arrayMatBrow addObject:dictMatB];
    }
    NSLog(@"%s : %@",  __FUNCTION__, arrayMatBrow);
    
    [[EWebServiceClient sharedInstance] sendRequestWithPath:@"track_bulk_user_actions"
                                                     method:@"POST"
                                                     params:@{@"data" : arrayMatBrow}
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         for (MaterialBrowsing *matB in arrayUnsyncedMaterialBrowsings)
         {
             matB.synced = @YES;
         }
         
         [[LLDataAccessLayer sharedInstance].managedObjectContext save:nil];
         
         NSLog(@"%s success", __FUNCTION__);
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         NSLog(@"%s failure error: %@", __FUNCTION__, error);
     }];
}

// ------------------------------------------------------------------------------------------------------------------------------------------------

#pragma mark - Timeline

-(void)syncTimeline
{
    if (![[EWebServiceClient sharedInstance] defaultValueForHeader:@"Authorization"])
    {
        return;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleTimelinUpload:) name:kESyncAddTalksToTimeline object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleTimelinUpload:) name:kESyncRemoveTalksToTimeline object:nil];
    
    self.numOfUnsyncTimelineUploaded = 0;
    
    [self uploadTimeline];
}

- (void)handleTimelinUpload:(NSNotification*)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:notification.name object:nil];
    
    self.numOfUnsyncTimelineUploaded--;
    
    NSLog(@"TIMELINE %@. Numb: %d", notification.name, self.numOfUnsyncTimelineUploaded);
    
    if (self.numOfUnsyncTimelineUploaded == 0)
    {
        [self getTimeline];
    }
}

-(void)getTimeline
{
    NSLog(@"%s",  __FUNCTION__);

    [[EWebServiceClient sharedInstance] sendRequestWithPath:[NSString stringWithFormat:@"timelines?with_pagination=0&conference_id=%@", [[RealmRepository shared] getSelectedConferenceId]]
                                                     method:@"GET"
                                                     params:nil
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         NSError *error = nil;
         NSDictionary* jsonData = [NSJSONSerialization JSONObjectWithData:successResponseData options:NSJSONReadingMutableContainers error:&error];
         
         if (!jsonData.count)
         {
             NSLog(@"%s success - no json", __FUNCTION__);
             [[NSNotificationCenter defaultCenter] postNotificationName:@"TimelineSynced" object:nil];
         }
         else
         {
             NSArray *arrayPresentationsInTimeline = [jsonData objectForKey:@"data"];
             [Talk updateTimeline:arrayPresentationsInTimeline];
             NSLog(@"%s success", __FUNCTION__);
         }
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         NSLog(@"%s failure error: %@", __FUNCTION__, error);
         [[NSNotificationCenter defaultCenter] postNotificationName:@"TimelineSynced" object:nil];
     }];
}

-(void)uploadTimeline
{
    NSArray* talks = [Talk getUnsyncedTimelineTalks];
    NSMutableArray *arrayTalksForAddToTimeline = [[NSMutableArray alloc] init];
    NSMutableArray *arrayTalksForRemoveFromTimeline = [[NSMutableArray alloc] init];
    
    for(Talk *talk in talks)
    {
        if (talk.zetBookmarked.boolValue == YES)
        {
            [arrayTalksForAddToTimeline addObject:talk.tlkTalkId];
        }
        else
        {
            [arrayTalksForRemoveFromTimeline addObject:talk.tlkTalkId];
        }
    }
    
    if (arrayTalksForAddToTimeline.count > 0)
    {
        [self addTalksToTimeline:arrayTalksForAddToTimeline];
    }
    if (arrayTalksForRemoveFromTimeline.count > 0)
    {
        [self removeTalksFromTimeline:arrayTalksForRemoveFromTimeline];
    }
    
    if (arrayTalksForAddToTimeline.count == 0 && arrayTalksForRemoveFromTimeline.count == 0)
    {
        [self getTimeline];
    }
}

-(void)removeTalksFromTimeline:(NSArray *)arrayTalks
{
    if (![[EWebServiceClient sharedInstance] defaultValueForHeader:@"Authorization"])
    {
        return;
    }
    
    NSLog(@"%s",  __FUNCTION__);
    
    NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                            arrayTalks , @"presentation_ids",
                            nil];
    
    self.numOfUnsyncTimelineUploaded++;
    
    [[EWebServiceClient sharedInstance] sendRequestWithPath:@"timelines"
                                                     method:@"DELETE"
                                                     params:params
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         for (NSNumber *timelinId in arrayTalks)
         {
             Talk *talk = [Talk getTalkWithId:timelinId];
             talk.timelineSynced = @YES;
         }
         
         [[LLDataAccessLayer sharedInstance].managedObjectContext save:nil];
         
         [[NSNotificationCenter defaultCenter] postNotificationName:kESyncRemoveTalksToTimeline object:nil];
         
         NSLog(@"%s success", __FUNCTION__);
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         [[NSNotificationCenter defaultCenter] postNotificationName:kESyncRemoveTalksToTimeline object:nil];
         
         NSLog(@"%s failure error: %@", __FUNCTION__, error);
     }];
}

-(void)addTalksToTimeline:(NSArray *)arrayTalks
{
    if (![[EWebServiceClient sharedInstance] defaultValueForHeader:@"Authorization"])
    {
        return;
    }
    
    NSLog(@"%s",  __FUNCTION__);
    
    NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                            arrayTalks , @"presentation_ids",
                            nil];
    
    self.numOfUnsyncTimelineUploaded++;
    
    [[EWebServiceClient sharedInstance] sendRequestWithPath:@"timelines"
                                                     method:@"POST"
                                                     params:params
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         
         for (NSNumber *timelinId in arrayTalks)
         {
             Talk *talk = [Talk getTalkWithId:timelinId];
             talk.timelineSynced = @YES;
         }
         
         [[LLDataAccessLayer sharedInstance].managedObjectContext save:nil];
         
         [[NSNotificationCenter defaultCenter] postNotificationName:kESyncAddTalksToTimeline object:nil];
         
         NSLog(@"%s success", __FUNCTION__);
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         [[NSNotificationCenter defaultCenter] postNotificationName:kESyncAddTalksToTimeline object:nil];
         
         NSLog(@"%s failure error: %@", __FUNCTION__, error);
     }];
}

#pragma mark - Vote

-(void)syncVotes
{
    if (![[EWebServiceClient sharedInstance] defaultValueForHeader:@"Authorization"])
    {
        return;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleVoteUpload:) name:kESyncVoteTalks object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleVoteUpload:) name:kESyncRemoveVoteTalks object:nil];
    
    self.numOfUnsyncVote = 0;
    
    [self uploadVotes];
}

- (void)handleVoteUpload:(NSNotification*)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:notification.name object:nil];
    
    self.numOfUnsyncVote--;
    
    NSLog(@"VOTE %@. Numb: %d", notification.name, self.numOfUnsyncVote);
    
    if (self.numOfUnsyncVote == 0)
    {
        [self getVotes];
    }
}

-(void)uploadVotes
{
    NSArray* talks = [Talk getUnsyncedVotedTalks];
    NSMutableArray *arrayTalksVoted = [[NSMutableArray alloc] init];
    NSMutableArray *arrayTalksUnvoted = [[NSMutableArray alloc] init];

    for(Talk *talk in talks)
    {
        if (talk.tlkVoted.boolValue == YES)
        {
            [arrayTalksVoted addObject:talk.tlkTalkId];
        }
        else
        {
            [arrayTalksUnvoted addObject:talk.tlkTalkId];
        }
    }

    if (arrayTalksVoted.count > 0)
    {
        [self voteTalks:arrayTalksVoted];
    }
    if (arrayTalksUnvoted.count > 0)
    {
        [self unvotedTalks:arrayTalksUnvoted];
    }

    if (arrayTalksVoted.count == 0 && arrayTalksUnvoted.count == 0)
    {
        [self getVotes];
    }
}

-(void)voteTalks:(NSArray *)arrayTalks
{
    if (![[EWebServiceClient sharedInstance] defaultValueForHeader:@"Authorization"])
    {
        return;
    }
    
    NSLog(@"%s",  __FUNCTION__);
    
    NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                            arrayTalks , @"presentation_ids",
                            nil];
    
    self.numOfUnsyncVote++;
    
    [[EWebServiceClient sharedInstance] sendRequestWithPath:@"presentations/votes"
                                                     method:@"POST"
                                                     params:params
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         for (NSNumber *timelinId in arrayTalks)
         {
             Talk *talk = [Talk getTalkWithId:timelinId];
             talk.tlkVoteSynced = @YES;
         }
         
         [[LLDataAccessLayer sharedInstance].managedObjectContext save:nil];
         
         [[NSNotificationCenter defaultCenter] postNotificationName:kESyncVoteTalks object:nil];
         
         NSLog(@"%s success", __FUNCTION__);
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         [[NSNotificationCenter defaultCenter] postNotificationName:kESyncVoteTalks object:nil];
         
         NSLog(@"%s failure error: %@", __FUNCTION__, error);
     }];
}

-(void)unvotedTalks:(NSArray *)arrayTalks
{
    if (![[EWebServiceClient sharedInstance] defaultValueForHeader:@"Authorization"])
    {
        return;
    }
    
    NSLog(@"%s",  __FUNCTION__);
    
    NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                            arrayTalks , @"presentation_ids",
                            nil];
    
    self.numOfUnsyncVote++;
    
    [[EWebServiceClient sharedInstance] sendRequestWithPath:@"presentations/votes"
                                                     method:@"DELETE"
                                                     params:params
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         for (NSNumber *timelinId in arrayTalks)
         {
             Talk *talk = [Talk getTalkWithId:timelinId];
             talk.tlkVoteSynced = @YES;
         }
         
         [[LLDataAccessLayer sharedInstance].managedObjectContext save:nil];
         
         [[NSNotificationCenter defaultCenter] postNotificationName:kESyncRemoveVoteTalks object:nil];
         
         NSLog(@"%s success", __FUNCTION__);
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         [[NSNotificationCenter defaultCenter] postNotificationName:kESyncRemoveVoteTalks object:nil];
         
         NSLog(@"%s failure error: %@", __FUNCTION__, error);
     }];
}

-(void)getVotes
{
    NSLog(@"%s",  __FUNCTION__);
    
    [[EWebServiceClient sharedInstance] sendRequestWithPath:@"presentations/my_votes"
                                                     method:@"GET"
                                                     params:nil
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         NSError *error = nil;
         NSDictionary* jsonData = [NSJSONSerialization JSONObjectWithData:successResponseData options:NSJSONReadingMutableContainers error:&error];
         
         if (!jsonData.count)
         {
             NSLog(@"%s success - no json", __FUNCTION__);
         }
         else
         {
             NSArray *arrayVotedPresentations = [jsonData objectForKey:@"data"];
             [Talk updateVote:arrayVotedPresentations];
             NSLog(@"%s success", __FUNCTION__);
         }
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         NSLog(@"%s failure error: %@", __FUNCTION__, error);
     }];
}

#pragma mark - Attended Sessions

-(void)getAttendedSessions
{
    NSLog(@"%s",  __FUNCTION__);
    
    
    NSString *url = [NSString stringWithFormat:@"blocks/attended?conference_id=%@", [[RealmRepository shared] getSelectedConferenceId]];
    [[EWebServiceClient sharedInstance] sendRequestWithPath:url
                                                     method:@"GET"
                                                     params:nil
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         NSError *error = nil;
         NSDictionary* jsonData = [NSJSONSerialization JSONObjectWithData:successResponseData options:NSJSONReadingMutableContainers error:&error];
         
         if (!jsonData.count)
         {
             NSLog(@"%s success - no json", __FUNCTION__);
         }
         else
         {
             NSArray *arrayAttendingBlocks = [jsonData objectForKey:@"data"];
             // id: 10,
             // sessionId:2
             [Session updateAttendedSessions:arrayAttendingBlocks];
         }
         
         NSLog(@"%s success", __FUNCTION__);
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         NSLog(@"%s failure error: %@", __FUNCTION__, error);
     }];
}

#pragma mark - Q&A

-(void)getQAForPresentation:(Talk *)talk
{
    NSLog(@"%s",  __FUNCTION__);
    
    [[EWebServiceClient sharedInstance] sendRequestWithPath:[NSString stringWithFormat:@"presentation_comments?presentation_id=%@", talk.tlkTalkId]
                                                     method:@"GET"
                                                     params:nil
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         NSError *error = nil;
         NSDictionary* jsonData = [NSJSONSerialization JSONObjectWithData:successResponseData options:NSJSONReadingMutableContainers error:&error];
         
         if (!jsonData.count)
         {
             NSLog(@"%s success - no json", __FUNCTION__);
         }
         else
         {
             //             "id": 1129,
             //             "presentation_id": 21,
             //             "user_id": 1,
             //             "parent_id": 0,
             //             "subject": null,
             //             "content": "",
             //             "sent": 0,
             //             "trashed": 0,
             //             "public": 1,
             //             "read": 1,
             //             "to_speaker": 0,
             //             "approved": null,
             //             "created_at": "2016-04-22 09:10:46"
             
             NSArray *arrayQAs = [jsonData objectForKey:@"data"];
             
             NSDictionary* attributeMappings = [NSDictionary dictionaryWithObjectsAndKeys:
                                                @"id", @"tqaId",
                                                @"user_id", @"tqaUserId",
                                                @"parent_id", @"tqaParentId",
                                                @"to_speaker", @"tqaSpeakerId",
                                                @"presentation_id", @"tqaTalkId",
                                                @"content", @"tqaContent",
                                                @"created_at", @"tqaCreatedAt",
                                                nil];
             
             //Save talks to CoreData
             [[LLDataAccessLayer sharedInstance] insertEntitiesArray:arrayQAs withName:@"TalkQA" receivedPredicatePrimaryKey:@"id" predicateKeyAttribute:@"tqaId" sortDescriptorKey:@"tqaId" activeAttributeName:@"trashed" attributeMappings:attributeMappings withCompletitionBlock:^(bool finished)
              {
                  if(finished)
                  {
                      [TalkQA linkQAs];
                  }
              }];
             
             NSLog(@"%s success", __FUNCTION__);
         }
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         NSLog(@"%s failure error: %@", __FUNCTION__, error);
     }];
}

-(void)uploadQAs
{
    NSArray* QAs = [TalkQA getUnsyncedQAs];
    
    for(TalkQA *qa in QAs)
    {
        [self uploadQA:qa];
    }
}

-(void)uploadQA:(TalkQA *)qa
{
    //    "presentation_id": 1,
    //    "parent_id": 4,
    //    "subject": "Some subject",
    //    "content": "Some content",
    //    "sent": true
    
    NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                            qa.tqaTalkId, @"presentation_id",
                            qa.tqaContent, @"content",
                            @1, @"sent",
                            nil];
    
    NSLog(@"%s : %@",  __FUNCTION__, params);
    
    [[EWebServiceClient sharedInstance] sendRequestWithPath:@"presentation_comments"
                                                     method:@"POST"
                                                     params:params
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         //         "data": {
         //             "id": 1145
         //             "presentation_id": 1
         //             "user_id": 1
         //             "rating_speaker": 1
         //             "rating_content": 4
         //             "comment": "Some content"
         //         }
         
         NSError *error = nil;
         NSDictionary* jsonData = [NSJSONSerialization JSONObjectWithData:successResponseData options:NSJSONReadingMutableContainers error:&error];
         
         if (!jsonData.count)
         {
             NSLog(@"%s success - no json", __FUNCTION__);
         }
         else
         {
             NSLog(@"%s success", __FUNCTION__);
             
             NSDictionary *dicQA = [jsonData objectForKey:@"data"];
             
             qa.tqaId = [dicQA objectForKey:@"id"];
             qa.synced = @1;
         }
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         NSLog(@"%s failure error: %@", __FUNCTION__, error);
     }];
}

#pragma mark - Notes

-(void)getNotes
{
    NSLog(@"%s",  __FUNCTION__);
    
    [[EWebServiceClient sharedInstance] sendRequestWithPath:@"notes"
                                                     method:@"GET"
                                                     params:nil
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         NSError *error = nil;
         NSDictionary* jsonData = [NSJSONSerialization JSONObjectWithData:successResponseData options:NSJSONReadingMutableContainers error:&error];
         
         if (!jsonData.count)
         {
             NSLog(@"%s success - no json", __FUNCTION__);
             [[NSNotificationCenter defaultCenter] postNotificationName:@"NoteSynced" object:nil];
         }
         else
         {
             //             {
             //                 "id": 1145
             //                 "presentation_id": 1,
             //                 "content": "Sample content",
             //                 "created_at": "2016-04-24 13:46:19"
             //             }
             
             NSArray *arrayNotes = [jsonData objectForKey:@"data"];
             
             NSDictionary* attributeMappings = [NSDictionary dictionaryWithObjectsAndKeys:
                                                @"id", @"ntNoteId",
                                                @"presentation_id", @"ntTalkId",
                                                @"content", @"ntNote",
                                                @"created_at", @"ntDate",
                                                nil];
             
             //Save talks to CoreData
             [[LLDataAccessLayer sharedInstance] insertEntitiesArray:arrayNotes withName:@"Note" receivedPredicatePrimaryKey:@"id" predicateKeyAttribute:@"ntNoteId" sortDescriptorKey:@"ntNoteId" activeAttributeName:nil attributeMappings:attributeMappings withCompletitionBlock:^(bool finished)
              {
                  if(finished)
                  {
                      [self linkTalkAndNote];
                  }
                  else
                  {
                      [[NSNotificationCenter defaultCenter] postNotificationName:@"NoteSynced" object:nil];
                  }
              }];
             
             NSLog(@"%s success", __FUNCTION__);
         }
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         NSLog(@"%s failure error: %@", __FUNCTION__, error);
         [[NSNotificationCenter defaultCenter] postNotificationName:@"NoteSynced" object:nil];
     }];
}

-(void)getNotesForPresentation:(Talk *)talk
{
    NSLog(@"%s",  __FUNCTION__);
    
    [[EWebServiceClient sharedInstance] sendRequestWithPath:[NSString stringWithFormat:@"notes?presentation_id=%@", talk.tlkTalkId]
                                                     method:@"GET"
                                                     params:nil
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         NSError *error = nil;
         NSDictionary* jsonData = [NSJSONSerialization JSONObjectWithData:successResponseData options:NSJSONReadingMutableContainers error:&error];
         
         if (!jsonData.count)
         {
             NSLog(@"%s success - no json", __FUNCTION__);
         }
         else
         {
             //             {
             //                 "id": 1145
             //                 "presentation_id": 1,
             //                 "content": "Sample content",
             //                 "created_at": "2016-04-24 13:46:19"
             //             }
             
             NSArray *arrayNotes = [jsonData objectForKey:@"data"];
             
             NSDictionary* attributeMappings = [NSDictionary dictionaryWithObjectsAndKeys:
                                                @"id", @"ntNoteId",
                                                @"presentation_id", @"ntTalkId",
                                                @"content", @"ntNote",
                                                @"created_at", @"ntDate",
                                                nil];
             
             //Save talks to CoreData
             [[LLDataAccessLayer sharedInstance] insertEntitiesArray:arrayNotes withName:@"Note" receivedPredicatePrimaryKey:@"id" predicateKeyAttribute:@"ntNoteId" sortDescriptorKey:@"ntNoteId" activeAttributeName:nil attributeMappings:attributeMappings withCompletitionBlock:^(bool finished)
              {
                  if(finished)
                  {
                      [Note linkNotes];
                  }
              }];
             
             NSLog(@"%s success", __FUNCTION__);
         }
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         NSLog(@"%s failure error: %@", __FUNCTION__, error);
     }];
}

-(void)uploadNotes
{
    NSArray* arrayUsyncedNotes = [Note getUnsyncedNotes];
    
    for(Note *note in arrayUsyncedNotes)
    {
        if (note.ntDeleted.integerValue == 1)
        {
            [self deleteNote:note];
        }
        else
        {
            [self uploadNote:note];
        }
    }
}

-(void)deleteNote:(Note *)note
{
    NSLog(@"%s",  __FUNCTION__);
    
    [[EWebServiceClient sharedInstance] sendRequestWithPath:[NSString stringWithFormat:@"notes/%@", note.ntNoteId]
                                                     method:@"DELETE"
                                                     params:nil
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         [[LLDataAccessLayer sharedInstance].managedObjectContext deleteObject:note];
         [[LLDataAccessLayer sharedInstance].managedObjectContext save:nil];
         
         NSLog(@"%s success", __FUNCTION__);
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         NSLog(@"%s failure error: %@", __FUNCTION__, error);
     }];
}

-(void)uploadNote:(Note *)note
{
    //    "presentation_id": 1,
    //    "content": "Sample content",
    //    "created_at": "2016-04-24 13:46:19"
    
    NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                            note.ntTalkId, @"presentation_id",
                            note.ntNote, @"content",
                            note.ntDate, @"created_at",
                            nil];
    
    NSLog(@"%s with params %@",  __FUNCTION__, params);
    
    [[EWebServiceClient sharedInstance] sendRequestWithPath:@"notes"
                                                     method:@"POST"
                                                     params:params
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         NSError *error = nil;
         NSDictionary* jsonData = [NSJSONSerialization JSONObjectWithData:successResponseData options:NSJSONReadingMutableContainers error:&error];
         
         if (!jsonData.count)
         {
             NSLog(@"%s success - no json", __FUNCTION__);
         }
         else
         {
             NSDictionary *dicNote = [jsonData objectForKey:@"data"];
             
             note.ntNoteId = [dicNote objectForKey:@"id"];
             note.synced = @YES;
             
             [[LLDataAccessLayer sharedInstance].managedObjectContext save:nil];
             
             NSLog(@"%s success", __FUNCTION__);
         }
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         NSLog(@"%s failure error: %@", __FUNCTION__, error);
     }];
}

#pragma mark - Register Device

-(void)registerDevice
{
    NSString *apnsToken = [[NSUserDefaults standardUserDefaults] valueForKey:kKeyAPNSToken];
    NSString *firebaseToken = [[NSUserDefaults standardUserDefaults] valueForKey:@"firebaseToken"];
    
    if (!apnsToken || !firebaseToken)
    {
        return;
    }
    NSNumber *appDataVersionID = @([[RealmRepository shared] dataVersion]);
    NSString *conferenceId = [[RealmRepository shared] getSelectedConferenceId];
    NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   [Utils getDevicUUID], @"device_id",
                                   @"ios", @"type",
                                   conferenceId, @"conference_id",
                                   nil];
    // FIREBASE TOKEN
    if (firebaseToken)
    {
        [params setObject:firebaseToken forKey:@"token"];
    }
    // APNS TOKEN
    if (apnsToken)
    {
        [params setObject:apnsToken forKey:@"apns_token"];
    }
    // DEVICE PLATFORM/MODEL
    NSString *devicePlatform = [UIDeviceHardware devicePlatformString];
    if (devicePlatform)
    {
        [params setObject:devicePlatform forKey:@"device_name"];
    }
    
    // OS VERSION
    NSString *iosVersion = [[NSUserDefaults standardUserDefaults] valueForKey:kKeyOSVersion];
    if (iosVersion)
    {
        [params setObject:iosVersion forKey:@"os_version"];
    }
    // APP VERSION
    NSString *appVersion = [[NSUserDefaults standardUserDefaults] valueForKey:kKeyAppVersion];
    if (appVersion)
    {
        [params setObject:appVersion forKey:@"app_version"];
    }
    // USER ID
    NSNumber *userId = [[NSUserDefaults standardUserDefaults] valueForKey:@"user_id"];
    if (userId)
    {
        [params setObject:userId forKey:@"user_id"];
    }
    
    NSLog(@"%s with params %@",  __FUNCTION__, params);
    
    [[EWebServiceClient sharedInstance] sendRequestWithPath:@"devices"
                                                     method:@"POST"
                                                     params:params
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         NSLog(@"%s success", __FUNCTION__);
         
         dispatch_async(dispatch_get_main_queue(), ^{
             [[RealmRepository shared] setDataVersion:[appDataVersionID integerValue]];
         });
         
         [[RealmRepository shared] setLastSyncWithDate:[NSDate new]];
         [[NSNotificationCenter defaultCenter] postNotificationName:kSyncFinished object:self userInfo:nil];
         
         
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         NSLog(@"%s failure error: %@", __FUNCTION__, error);
     }];
}

#pragma mark - Meeting on the go

-(void)getMOTGTimesWitCompletionBlock:(void (^)(NSArray *times))completion
{
    NSLog(@"%s",  __FUNCTION__);
    
    [[EWebServiceClient sharedInstance] sendRequestWithPath:@"meeting_times?with_pagination=0"
                                                     method:@"GET"
                                                     params:nil
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         NSError *error = nil;
         NSDictionary* jsonData = [NSJSONSerialization JSONObjectWithData:successResponseData options:NSJSONReadingMutableContainers error:&error];
         
         if (!jsonData.count)
         {
             NSLog(@"%s success - no json", __FUNCTION__);
             completion(nil);
         }
         else
         {
             NSLog(@"%s success", __FUNCTION__);
             
             NSArray *array = [jsonData objectForKey:@"data"];
             completion(array);
         }
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         NSLog(@"%s failure error: %@", __FUNCTION__, error);
         completion(nil);
     }];
}

-(void)getMOTGRoomsSuccess:(void (^)(NSArray *rooms))success
                   failure:(void (^)(void))failure
{
    NSLog(@"%s",  __FUNCTION__);
    
    [[EWebServiceClient sharedInstance] sendRequestWithPath:@"meeting_rooms?with_pagination=0"
                                                     method:@"GET"
                                                     params:nil
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         NSError *error = nil;
         NSDictionary* jsonData = [NSJSONSerialization JSONObjectWithData:successResponseData options:NSJSONReadingMutableContainers error:&error];
         
         if (!jsonData.count)
         {
             NSLog(@"%s success - no json", __FUNCTION__);
             failure();
         }
         else
         {
             NSArray *arrayRooms = [jsonData objectForKey:@"data"];
             
             NSLog(@"%s success", __FUNCTION__);
             
             success(arrayRooms);
         }
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         NSLog(@"%s failure error: %@", __FUNCTION__, error);
         [self handleError:error withResponse:failureResponseData];
         failure();
     }];
}

-(void)getMeetingsOnTheGoForDate:(NSString *)date
                         success:(void (^)(NSArray *meetings))success
                         failure:(void (^)(void))failure
{
    NSLog(@"%s",  __FUNCTION__);
    
    [[EWebServiceClient sharedInstance] sendRequestWithPath:[NSString stringWithFormat:@"meeting_bookings?with_pagination&date=%@", date]
                                                     method:@"GET"
                                                     params:nil
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         NSError *error = nil;
         NSDictionary* jsonData = [NSJSONSerialization JSONObjectWithData:successResponseData options:NSJSONReadingMutableContainers error:&error];
         
         if (!jsonData.count)
         {
             NSLog(@"%s success - no json", __FUNCTION__);
             failure();
         }
         else
         {
             NSArray *array = [jsonData objectForKey:@"data"];
             
             NSLog(@"%s success", __FUNCTION__);
             
             success(array);
         }
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         NSLog(@"%s failure error: %@", __FUNCTION__, error);
         [self handleError:error withResponse:failureResponseData];
         failure();
     }];
}

-(void)bookMeetingWithTitle:(NSString *)title
                     onTime:(NSNumber *)time_id
                     inRoom:(NSNumber *)room_id
                    forDate:(NSString *)date
                   isPublic:(NSString *)isPublic
                    success:(void (^)(void))success
                    failure:(void (^)(void))failure
{
    NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                            room_id, @"room_id",
                            time_id, @"time_id",
                            date, @"date",
                            title, @"title",
                            isPublic, @"public",
                            nil];
    
    NSLog(@"%s with params %@",  __FUNCTION__, params);
    
    [[EWebServiceClient sharedInstance] sendRequestWithPath:@"meeting_bookings"
                                                     method:@"POST"
                                                     params:params
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         success();
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         NSLog(@"%s failure error: %@", __FUNCTION__, error);
         [self handleError:error withResponse:failureResponseData];
         failure();
     }];
}

-(void)deleteMeeting:(NSNumber *)meeting_id
             success:(void (^)(void))success
             failure:(void (^)(void))failure
{
    NSLog(@"%s",  __FUNCTION__);
    
    [[EWebServiceClient sharedInstance] sendRequestWithPath:[NSString stringWithFormat:@"meeting_bookings/%@", meeting_id]
                                                     method:@"DELETE"
                                                     params:nil
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         success();
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         NSLog(@"%s failure error: %@", __FUNCTION__, error);
         [self handleError:error withResponse:failureResponseData];
         failure();
     }];
}

-(void)updateMeeting:(NSNumber *)meeting_id
           WithTitle:(NSString *)title
              onTime:(NSNumber *)time_id
              inRoom:(NSNumber *)room_id
             forDate:(NSString *)date
            isPublic:(NSString *)isPublic
             success:(void (^)(void))success
             failure:(void (^)(void))failure
{
    NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                            room_id, @"room_id",
                            time_id, @"time_id",
                            date, @"date",
                            title, @"title",
                            isPublic, @"public",
                            nil];
    
    NSLog(@"%s with params %@",  __FUNCTION__, params);
    
    [[EWebServiceClient sharedInstance] sendRequestWithPath:[NSString stringWithFormat:@"meeting_bookings/%@", meeting_id]
                                                     method:@"PUT"
                                                     params:params
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         success();
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         NSLog(@"%s failure error: %@", __FUNCTION__, error);
         [self handleError:error withResponse:failureResponseData];
         failure();
     }];
}

#pragma mark Parse server error

-(NSString*)parseServerError:(NSDictionary *)jsonData
{
    NSMutableString* message = [NSMutableString new];
    
    for (NSString* key in jsonData)
    {
        if([key isEqualToString:@"errors"])
        {
            if([[[jsonData objectForKey:key] class] isSubclassOfClass: [NSDictionary class] ])
            {
                NSDictionary* dict = [jsonData objectForKey:key];
                
                for(NSString* dictKey in dict )
                {
                    if([[[dict objectForKey:dictKey] class] isSubclassOfClass: [NSArray class]])
                    {
                        NSArray* errorsArray = [dict objectForKey:dictKey];
                        
                        for(NSString* err in errorsArray)
                        {
                            [message appendString:[NSString stringWithFormat:@"\n\n%@", err]];
                        }
                    }
                    else
                    {
                        [message appendString:[NSString stringWithFormat:@"\n%@",[dict objectForKey:dictKey]]];
                    }
                }
            }
            else if([[[jsonData objectForKey:key] class] isSubclassOfClass: [NSArray class] ])
            {
                NSArray* errorsArray = [jsonData objectForKey:key];
                
                for(NSString* err in errorsArray)
                {
                    [message appendString:[NSString stringWithFormat:@"\n\n%@", err]];
                }
            }
        }
    }
    
    return message;
}

// ---------------------------------------------------------------------------------------------------------------------------

#pragma mark - Link Data Methods

-(void)linkAllData
{
    NSLog(@"LINK ALL DATA CALLED");

    ConferenceSettings *settings = [[RealmRepository shared] settings];
    ConferenceCoreDataLink *link = settings.link;

    if (link.presentationBlock) {
        self.numOfLinked --;
        [self linkTalkAndSession];
    }
    if (link.presentationSpeaker) {
        self.numOfLinked --;
        [self linkTalkAndSpeaker];
    }
    if (link.boothExhibitor) {
        self.numOfLinked --;
        [self linkBoothAndExhibitor];
    }
    if (link.presentationMaterial) {
        self.numOfLinked --;
        [self linkMaterialAndTalk];
    }
    if (link.blockLocation) {
        self.numOfLinked --;
        [self linkSessionAndVenue];
    }
}

-(void)linkTalkAndSpeaker
{
    [[LLDataAccessLayer sharedInstance] linkEntityNameWithToManyRelation:@"Speaker"
                                                     withPKattributeName:@"spkSpeakerId"
                                           toEntityNameWithToOneRelation:@"Talk"
                                                     withPKattributeName:@"tlkTalkId"
                                                     linkIdAttributeName:@"tlkSpeakerId"
                                                   andToManyRelationName:@"speaker" useBgrdMOC:YES];
}

-(void)linkTalkAndSession
{
    [[LLDataAccessLayer sharedInstance] linkEntityNameWithToManyRelation:@"Session"
                                                     withPKattributeName:@"sesSessionId"
                                           toEntityNameWithToOneRelation:@"Talk"
                                                     withPKattributeName:@"tlkTalkId"
                                                     linkIdAttributeName:@"tlkSessionId"
                                                   andToManyRelationName:@"session" useBgrdMOC:YES];
}

-(void)linkMaterialAndTalk
{
    [[LLDataAccessLayer sharedInstance] linkEntityNameWithToManyRelation:@"Talk"
                                                     withPKattributeName:@"tlkTalkId"
                                           toEntityNameWithToOneRelation:@"Material"
                                                     withPKattributeName:@"matMaterialId"
                                                     linkIdAttributeName:@"matTalkId"
                                                   andToManyRelationName:@"talk" useBgrdMOC:YES];
}

-(void)linkSessionAndVenue
{
    [[LLDataAccessLayer sharedInstance] linkEntityNameWithToManyRelation:@"Venue"
                                                     withPKattributeName:@"venVenueId"
                                           toEntityNameWithToOneRelation:@"Session"
                                                     withPKattributeName:@"sesSessionId"
                                                     linkIdAttributeName:@"sesVenueId"
                                                   andToManyRelationName:@"venue" useBgrdMOC:YES];
}

-(void)linkBoothAndExhibitor
{
    [[LLDataAccessLayer sharedInstance] linkEntityNameWithToManyRelation:@"Exhibitor"
                                                     withPKattributeName:@"exhExhibitorId"
                                           toEntityNameWithToOneRelation:@"Booth"
                                                     withPKattributeName:@"bthBoothId"
                                                     linkIdAttributeName:@"bthExhibitorId"
                                                   andToManyRelationName:@"exhibitor" useBgrdMOC:YES];
}

-(void)linkTalkQAAndTalk
{
    [[LLDataAccessLayer sharedInstance] linkEntityNameWithToManyRelation:@"Talk"
                                                     withPKattributeName:@"tlkTalkId"
                                           toEntityNameWithToOneRelation:@"TalkQA"
                                                     withPKattributeName:@"tqaId"
                                                     linkIdAttributeName:@"tqaTalkId"
                                                   andToManyRelationName:@"talk" useBgrdMOC:YES];
}

-(void)linkTalkAndNote
{
    [[LLDataAccessLayer sharedInstance] linkEntityNameWithToManyRelation:@"Talk"
                                                     withPKattributeName:@"tlkTalkId"
                                           toEntityNameWithToOneRelation:@"Note"
                                                     withPKattributeName:@"ntNoteId"
                                                     linkIdAttributeName:@"ntTalkId"
                                                   andToManyRelationName:@"talk" useBgrdMOC:YES];
}

-(void)linkTalkAndOrder
{
    [[LLDataAccessLayer sharedInstance] linkEntityNameWithToManyRelation:@"Talk"
                                                     withPKattributeName:@"tlkTalkId"
                                           toEntityNameWithToOneRelation:@"Order"
                                                     withPKattributeName:@"ordOrderId"
                                                     linkIdAttributeName:@"ordTalkId"
                                                   andToManyRelationName:@"talk" useBgrdMOC:YES];
}

-(void)linkExhibitorRoleAndSession
{
    [[LLDataAccessLayer sharedInstance] linkEntityNameWithToManyRelation:@"Session"
                                                     withPKattributeName:@"sesSessionId"
                                           toEntityNameWithToOneRelation:@"ExhibitorRole"
                                                     withPKattributeName:@"erExhibitorRoleId"
                                                     linkIdAttributeName:@"erSessionId"
                                                   andToManyRelationName:@"session" useBgrdMOC:YES];
}

-(void)linkExhibitorRoleAndExhibitor
{
    [[LLDataAccessLayer sharedInstance] linkEntityNameWithToManyRelation:@"Exhibitor"
                                                     withPKattributeName:@"exhExhibitorId"
                                           toEntityNameWithToOneRelation:@"ExhibitorRole"
                                                     withPKattributeName:@"erExhibitorRoleId"
                                                     linkIdAttributeName:@"erExhibitorId"
                                                   andToManyRelationName:@"exhibitor" useBgrdMOC:YES];
}

-(void)linkSurveyQuestionSurvey
{
    [[LLDataAccessLayer sharedInstance] linkEntityNameWithToManyRelation:@"Survey"
                                                     withPKattributeName:@"survSurveyId"
                                           toEntityNameWithToOneRelation:@"SurveyQuestion"
                                                     withPKattributeName:@"suqSurveyQuestionId"
                                                     linkIdAttributeName:@"suqCampaignId"
                                                   andToManyRelationName:@"survey" useBgrdMOC:YES];
}

-(void)linkHighlightedSlideAndComment
{
    [[LLDataAccessLayer sharedInstance] linkEntityNameWithToManyRelation:@"HighlightedSlide"
                                                     withPKattributeName:@"hsSlideId"
                                           toEntityNameWithToOneRelation:@"HighlightedSlideComment"
                                                     withPKattributeName:@"hscCommentId"
                                                     linkIdAttributeName:@"hscSlideId"
                                                   andToManyRelationName:@"slide" useBgrdMOC:YES];
}

//----------------------------------------------------------------------------------------------

#pragma mark - Handling functions

- (void)handleSyncFinished:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:notification.name object:nil];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([User current])
        {
            [self syncUserData:NO];
        }
        
        if ([[RealmRepository shared] dataVersion] > 0) {
            [self sendAppDownloadedDataVersionIDToServer];
        }
        else
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:kSyncFinished object:self userInfo:nil];
        }
    });
}

-(void)sendAppDownloadedDataVersionIDToServer
{
    NSNumber *appDataVersionID = @([[RealmRepository shared] dataVersion]);
    NSString *conferenceId = [[RealmRepository shared] getSelectedConferenceId];
    
    // device_id, type and conference_data_version_id - required fields
    NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                            [Utils getDevicUUID], @"device_id",
                            @"ios", @"type",
                            appDataVersionID , @"conference_data_version_id",
                            conferenceId, @"conference_id",
                            nil];
    
    NSLog(@"%s with params %@",  __FUNCTION__, params);
    
    [[EWebServiceClient sharedInstance] sendRequestWithPath:@"devices"
                                                     method:@"POST"
                                                     params:params
                                       successResponseBlock:^(AFHTTPRequestOperation* operation, id successResponseData)
     {
         NSLog(@"%s success - no json", __FUNCTION__);
      
         dispatch_async(dispatch_get_main_queue(), ^{
             [[RealmRepository shared] setDataVersion:[appDataVersionID integerValue]];
         });

         [[RealmRepository shared] setLastSyncWithDate:[NSDate new]];
         [[NSNotificationCenter defaultCenter] postNotificationName:kSyncFinished object:self userInfo:nil];
     }
                                       failureResponseBlock:^(AFHTTPRequestOperation* failureResponseData, NSError* error)
     {
         NSLog(@"%s failure error: %@", __FUNCTION__, error);
         if ([[RealmRepository shared] lastSync] == nil)
         {
             [self showFirstDownloadMessageError];
         }
     }];
}

- (void)handleFirstSavedOfTalksAndSpeakers:(NSNotification*)notification
{
    NSLog(@"%@ numOfSavedTalksAndSpeakers: %d", notification.name, self.numOfSavedTalksAndSpeakers);
    
    if (self.numOfSavedTalksAndSpeakers == 0)
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:notification.name object:nil];
        [self syncSpeakersAndPresentations:self.arrayConferenceSpeakers];
        self.arrayConferenceSpeakers = nil;
    }
}

- (void)networkError:(NSNotification *)notification
{
    [self handleDownloaded:nil];
}

- (void)handleDownloaded:(NSNotification*)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:notification.name object:nil];
    
    self.numOfRequests--;
    
    NSLog(@"ENTITY DOWNLOADED %@. Saved: %d, Requests: %d, Linked: %d", notification.name, self.numOfSaved, self.numOfRequests, self.numOfLinked);
    
    if (self.numOfRequests == 0 && self.numOfSaved == 0 && self.numOfLinked == 0)
    {
        //sync if finished
        NSLog(@"Sync finished from HANDLE DOWNLOADED");
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kHandlePartOfSyncFinished object:self userInfo:nil];
    }
}

-(void)handleSaved:(NSNotification*)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:notification.name object:nil];
    
    self.numOfSaved ++;
    
    NSLog(@"ENTITY SAVED %@. Saved: %d, Requests: %d, Linked: %d", notification.name, self.numOfSaved, self.numOfRequests, self.numOfLinked);
    
    if (self.numOfRequests == 0 && self.numOfSaved == 0 && self.numOfLinked == 0)
    {
        //sync if finished
        NSLog(@"Sync finished from HANDLE SAVED");
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kHandlePartOfSyncFinished object:self userInfo:nil];
    }
}

-(void)handleLinked:(NSNotification*)notification
{
    self.numOfLinked ++;
    
    NSLog(@"ENTITY LINKED %@. Saved: %d, Requests: %d, Linked: %d", notification.name, self.numOfSaved, self.numOfRequests, self.numOfLinked);
    
    if (self.numOfRequests == 0 && self.numOfSaved == 0 && self.numOfLinked == 0)
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:notification.name object:nil];
        
        //sync if finished
        NSLog(@"Sync finished from HANDLE LINKED");
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kHandlePartOfSyncFinished object:self userInfo:nil];
    }
}

-(void)handleError:(NSError *) error withResponse:(AFHTTPRequestOperation *)failureResponseData
{
    NSLog(@"%s failure error: %@", __FUNCTION__, error);
    NSString *message = @"";
    
    if(failureResponseData.responseData)
    {
        NSError *jsonError = nil;
        NSDictionary* jsonData = [NSJSONSerialization JSONObjectWithData:failureResponseData.responseData options:NSJSONReadingMutableContainers error:&jsonError];
        
        if (!jsonData.count)
        {
            NSLog(@"%s failure json error %@", __FUNCTION__, jsonError);
        }
        else if ([jsonData objectForKey:@"message"])
        {
            message = [jsonData objectForKey:@"message"];
        }
        else
        {
            NSString* msgParsed = [[ESyncData sharedInstance] parseServerError:jsonData];
            message = [msgParsed stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
        }
    }
    else
    {
        message =  [NSString stringWithFormat:@"%@", [error localizedDescription]];;
    }
    
    [Utils showMessage:message withTitle:@"Info"];
}

# pragma mark - Swift exposed

-(void)logOut {
    [[LLDataAccessLayer sharedInstance] resetUserData];
    [self setAuthorizationHeaderWithToken:nil];
    [self resetLoginProperties];
    [[UserDefaultsHelper helper] deleteLoggedInData];
}

@end
