//
//  ESyncData.h
//  Ematerials
//
//  Created by admin on 12/20/13.
//  Copyright (c) 2013 Navus. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Country;
@class HighlightedSlide;
@class HighlightedSlideComment;

@interface ESyncData : NSObject

@property (nonatomic, strong) NSString* materialsBaseURL;

+ (ESyncData *)sharedInstance;

-(void)downloadZipFile;

-(void)syncWithServer;
-(void)syncWithServerRepeatedly; // on 10 minutes
-(void)syncUserData:(BOOL)optIn;

-(void)registerDevice;

-(void)getBanner:(NSString *)position
withCompletionBlock:(void (^)(NSDictionary *banner))completion;

-(void)trackUserAction:(NSString *)action
             forBanner:(NSNumber *)bannerId
            objectType:(NSString *)objectType
            objectInfo:(NSString *)objectInfo;

// Login and Registration

-(void)checkCode:(NSString *)code ForCountry:(Country *)country
         success:(void (^)(NSDictionary *responseObject))success
         failure:(void (^)(void))failure;
-(void)checkEmail:(NSString *)email
          success:(void (^)(void))success
          failure:(void (^)(void))failure;
-(void)connectAccount:(NSNumber *)partyId
              success:(void (^)(void))success
              failure:(void (^)(void))failure;

// it is just for enter badge //////////////////////////////
-(void)addBadge:(NSString *)badge
        success:(void (^)(NSArray *arrayResponse))success
        failure:(void (^)(void))failure;
-(void)connectAccountWithoutGetProfile:(NSNumber *)partyId
                               success:(void (^)(void))success
                               failure:(void (^)(void))failure;
-(void)getProfileWithSuccess:(void (^)(int points))success
                     failure:(void (^)(void))failure;
-(void)getProfile:(BOOL)optIn;
///////////////////////////////////////////////////////////

-(void)registerUser:(NSDictionary *)params
            success:(void (^)(NSDictionary *dicResponse))success
            failure:(void (^)(void))failure;
-(void)limitedLogin:(NSDictionary *)dicUserData
            success:(void (^)(void))success
            failure:(void (^)(void))failure;

// username is email
-(void)loginWithUsername:(NSString *)username AndPassword:(NSString *)password
                 success:(void (^)(void))success
                 failure:(void (^)(void))failure;

-(void)setAuthorizationHeaderWithToken:(NSString *)token;
-(void)setApiKeyInHeader:(NSString *)apiKey;
-(void)setHeaderWithValue:(NSString *)value forKey:(NSString *)key;

// User data ---------------------------------------------------------------------/

// Materials
//-(void)getMaterials;
-(void)downloadMaterial:(Material *)material
                success:(void (^)(id responseObject))success
                failure:(void (^)(void))failure;
-(void)getImagesForMaterial:(Material *)material
                    success:(void (^)(NSArray *images))success
                    failure:(void (^)(void))failure;
-(void)getMaterialsForPresentation:(NSNumber *)presentationId
             WithCompletitionBlock:(void (^)(void))finished;
// COST
-(void)getPresentationsCost:(NSNumber *)presentationID
                withSuccess:(void (^)(int cost))success
                    failure:(void (^)(void))failure;

// HighlightedSlide

-(void)getHighlightedSlidesForMaterial:(Material *)material
                            completion:(void (^)(void))completion;

-(void)highlightedSlide:(NSNumber *)page forMaterial:(NSNumber *)materialId
                success:(void (^)(HighlightedSlide *slide))success
                failure:(void (^)(void))failure;

-(void)unhighlightedSlide:(HighlightedSlide *)slide
                  success:(void (^)(void))success
                  failure:(void (^)(void))failure;

-(void)comment:(NSString *)comment highlightedSlide:(HighlightedSlide *)slide
       success:(void (^)(void))success
       failure:(void (^)(void))failure;

-(void)deleteComment:(HighlightedSlideComment *)comment
             success:(void (^)(void))success
             failure:(void (^)(void))failure;
// Orders
-(void)getOrders;
-(void)getOrdersWithCompletitionBlock:(void (^)(void))finished;
-(void)orderderTalk:(Talk *)talk
            success:(void (^)(void))success
            failure:(void (^)(void))failure;
-(void)addOrdersToLibrary:(NSArray *)arrayTalks
                  success:(void (^)(void))success
                  failure:(void (^)(void))failure;
// E-points
-(void)getEpointsWithCompletitionBlock:(void (^)(void))finished;
-(void)getUsersEpointsStructureWithCompletitionBlock:(void (^)(void))finished;
-(void)getEpointsWithSuccess:(void (^)(int epoints))success
                     failure:(void (^)(void))failure;

// Timeline
-(void)syncTimeline;
-(void)removeTalksFromTimeline:(NSArray *)arrayTalks;
-(void)addTalksToTimeline:(NSArray *)arrayTalks;

// Vote

-(void)voteTalks:(NSArray *)arrayTalks;
-(void)unvotedTalks:(NSArray *)arrayTalks;

// Attendances
-(void)getAttendedSessions;

//Q&A
-(void)getQAForPresentation:(Talk *)talk;
-(void)uploadQAs;

//Note
-(void)getNotesForPresentation:(Talk *)talk;
-(void)uploadNotes;
-(void)uploadNote:(Note *)note;
-(void)deleteNote:(Note *)note;

-(void)getNotes;

// Meeting on the go
-(void)getMOTGTimesWitCompletionBlock:(void (^)(NSArray *times))completion;
-(void)getMOTGRoomsSuccess:(void (^)(NSArray *rooms))success
                   failure:(void (^)(void))failure;
-(void)getMeetingsOnTheGoForDate:(NSString *)date
                         success:(void (^)(NSArray *meetings))success
                         failure:(void (^)(void))failure;
-(void)bookMeetingWithTitle:(NSString *)title
                     onTime:(NSNumber *)time_id
                     inRoom:(NSNumber *)room_id
                    forDate:(NSString *)date
                   isPublic:(NSString *)isPublic
                    success:(void (^)(void))success
                    failure:(void (^)(void))failure;
-(void)updateMeeting:(NSNumber *)meeting_id
           WithTitle:(NSString *)title
              onTime:(NSNumber *)time_id
              inRoom:(NSNumber *)room_id
             forDate:(NSString *)date
            isPublic:(NSString *)isPublic
             success:(void (^)(void))success
             failure:(void (^)(void))failure;
-(void)deleteMeeting:(NSNumber *)meeting_id
             success:(void (^)(void))success
             failure:(void (^)(void))failure;

//User
-(void)resetLoginProperties;
-(void)setUserAction:(NSString *)action;
-(void)syncMaterialBrowsings;

//Swift
-(void)downloadFirstJsonFileWithSuccess:(void (^)(id responseObject))success
                                failure:(void (^)(void))failure;
-(BOOL)unzip;
-(void)logOut;

// Survey
//-(void)syncSurveyAnswers;

// Actions
//-(void)getUsersActionsWithCompletionBlock:(void (^)(void))completion;

// -------------------------------------------------------------------------------/

-(NSString*)parseServerError:(NSDictionary*)jsonData;

-(void)linkAllData;
-(void)linkTalkAndSession;
-(void)linkTalkAndSpeaker;
-(void)linkMaterialAndTalk;
-(void)linkSessionAndVenue;
-(void)linkBoothAndExhibitor;
-(void)linkTalkQAAndTalk;

@end
