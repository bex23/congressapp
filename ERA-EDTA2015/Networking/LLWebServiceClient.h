//
//  LLWebServiceClient.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 3/18/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "AFNetworking.h"

@interface LLWebServiceClient : AFHTTPClient

@property(strong, nonatomic)NSMutableArray* errorRequestArray;
@property(nonatomic)BOOL isAVShown;

+(NSString*)baseUrl;
+(void)setBaseUrl:(NSString*)newUrl;
+(LLWebServiceClient*)sharedInstance;

-(void)sendRequestWithPath:(NSString*)path method:(NSString*)method params:(NSDictionary*)params successResponseBlock:(void(^)(AFHTTPRequestOperation*, id response))successResponseBlock failureResponseBlock:(void(^)(AFHTTPRequestOperation* , NSError* error))failureResponseBlock;


@end
