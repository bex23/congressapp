//
//  EWebServiceClient.h
//  Ematerials
//
//  Created by admin on 12/17/13.
//  Copyright (c) 2013 Navus. All rights reserved.
//

#import "AFNetworking.h"

@interface EWebServiceClient : AFHTTPClient

@property(strong, nonatomic)NSMutableArray* errorRequestArray;
@property(nonatomic)BOOL isAVShown;

+(NSString*)baseUrl;
+(void)setBaseUrl:(NSString*)newUrl;
+(EWebServiceClient*)sharedInstance;

-(void)sendRequestWithPath:(NSString*)path method:(NSString*)method params:(NSDictionary*)params successResponseBlock:(void(^)(AFHTTPRequestOperation*, id response))successResponseBlock failureResponseBlock:(void(^)(AFHTTPRequestOperation* , NSError* error))failureResponseBlock;

@end
