//
//  EWebServiceClient.m
//  Ematerials
//
//  Created by admin on 12/17/13.
//  Copyright (c) 2013 Navus. All rights reserved.
//

#import "EWebServiceClient.h"
//static NSString * const kABWebServiceBaseURLString = @"http://navusconsulting.ch/api/ematerialAPI/api.php";
//static NSString * const kABWebServiceBaseURLString = @"http://ematerials.navusconsulting.ch/api/";

static NSString* baseUrl = @"";


@implementation EWebServiceClient

+(EWebServiceClient*)sharedInstance
{
    static EWebServiceClient* _sharedClient = nil;
    
    static dispatch_once_t onceToken; //token koji se koristi u svakom requestu
    
    dispatch_once(&onceToken, ^{
        
        _sharedClient = [[EWebServiceClient alloc]initWithBaseURL:[NSURL URLWithString:[self baseUrl]]];
        
    });
    
    return _sharedClient;
}

+(NSString*)baseUrl
{
    return baseUrl;
}

+(void)setBaseUrl:(NSString*)newUrl
{
    baseUrl = newUrl;
}

- (id)initWithBaseURL:(NSURL *)url
{
    //    __weak ABWebServiceClient* weakSelf = self;
    self = [super initWithBaseURL:url];
    if(!self)
    {
        return nil;
    }
    [self setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status)
     {
         switch (status)
         {
             case AFNetworkReachabilityStatusNotReachable:
                 [[NSUserDefaults standardUserDefaults] setValue:nil forKey:kTypeOfInternetConnection];
                 NSLog(@"No network connection");
                 break;
             case AFNetworkReachabilityStatusUnknown:
                 [[NSUserDefaults standardUserDefaults] setValue:@"Unkown" forKey:kTypeOfInternetConnection];
                 NSLog(@"Network connection unknown");
                 break;
             case AFNetworkReachabilityStatusReachableViaWiFi:
                 [[NSUserDefaults standardUserDefaults] setValue:@"WiFi" forKey:kTypeOfInternetConnection];
                 NSLog(@"Network reachable via WiFi");
                 break;
             case AFNetworkReachabilityStatusReachableViaWWAN:
                 [[NSUserDefaults standardUserDefaults] setValue:@"WWAN" forKey:kTypeOfInternetConnection];
                 NSLog(@"Network reachable via WWAN");
                 break;
             default:
                 break;
         }
         [[NSUserDefaults standardUserDefaults] synchronize];

     }];
    [self registerHTTPOperationClass:[AFJSONRequestOperation class]];
    
    //    [self setParameterEncoding:AFFormURLParameterEncoding];
    [self setParameterEncoding:AFJSONParameterEncoding];
    
    //Here we set user and pass in the header of every requst
    //    [self setDefaultHeader:@"api_key" value:@"91C2A02D-67B3-4F6B-A9EB-92E9AF803C63"];
    //    [self setDefaultHeader:@"Password" value:@"kekic"];
    
    self.errorRequestArray = [[NSMutableArray alloc] initWithCapacity:6];
    self.isAVShown = NO;
    
    //  Accept HTTP Header; see http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.1
    
    //    [self setDefaultHeader:@"Accept" value:@"text/html"]; //text/html
    
    return self;
}

-(void)sendRequestWithPath:(NSString*)path method:(NSString*)method params:(NSDictionary*)params successResponseBlock:(void(^)(AFHTTPRequestOperation*, id response))successResponseBlock failureResponseBlock:(void(^)(AFHTTPRequestOperation* , NSError* error))failureResponseBlock
{
    NSMutableURLRequest *request = [[EWebServiceClient sharedInstance] requestWithMethod:method path:path parameters:params];
    [request setTimeoutInterval:120.0];
    
    [self sendRequest:request successResponseBlock:successResponseBlock failureResponseBlock:failureResponseBlock];
}

-(void)sendRequest:(NSURLRequest*)request successResponseBlock:(void(^)(AFHTTPRequestOperation*, id response))successResponseBlock failureResponseBlock:(void(^)(AFHTTPRequestOperation* , NSError* error))failureResponseBlock
{
    NSLog(@"Sending request for url: %@, status %d", request.URL, self.networkReachabilityStatus);
    
    AFHTTPRequestOperation *operation = [[EWebServiceClient sharedInstance] HTTPRequestOperationWithRequest:request
                                                                                                    success:^(AFHTTPRequestOperation* operation, id response)
                                         {
                                             NSInteger statusCode = operation.response.statusCode;
                                             NSLog(@"%@", request.URL.lastPathComponent);
                                             if ([request.URL.lastPathComponent isEqualToString:@"devices"]) {
                                                 if (statusCode == 200 || statusCode == 201) {
                                                     successResponseBlock(operation, response);
                                                     return;
                                                 }
                                             }
                                             
                                             if(statusCode == 200)
                                             {
                                                 NSError* error = nil;
                                                 NSDictionary* isValidJson;
                                                 if (response)
                                                 {
                                                     isValidJson = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&error];
                                                 }
                                                 else
                                                 {
                                                     [self handleErrorCode:3];
                                                 }
                                                 if (!response || !isValidJson)
                                                 {
                                                     //Recived response from service but no data
                                                     //This should never happen
                                                     
                                                     NSLog(@"JSON parse error: %@", error);
                                                     //set error request
                                                     [self.errorRequestArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                        operation.request, @"request",
                                                                                        successResponseBlock, @"successResponseBlock", nil]];
                                                     
                                                     NSLog(@"NoDataErrorForRequestURL: %@",  request.URL);
                                                     [self handleErrorCode:2];
                                                 }
                                                 else
                                                 {
                                                     NSLog(@"Received data for url: %@",  request.URL);
                                                     successResponseBlock(operation, response);
                                                 }
                                             }
                                             else
                                             {
                                                 successResponseBlock(operation, response);
                                             }
                                         }
                                                                                                    failure:^(AFHTTPRequestOperation* operation, NSError* error)
                                         {
                                             failureResponseBlock(operation, error);
                                             
                                             [self.errorRequestArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                operation.request, @"request",
                                                                                [successResponseBlock copy], @"successResponseBlock",
                                                                                [failureResponseBlock copy], @"failureResponseBlock", nil]];
                                             
                                             NSLog(@"errorForRequestURL: %@",  request.URL);
                                             [self handleErrorCode:(int)error.code ];
                                             [[NSNotificationCenter defaultCenter] postNotificationName:@"Network Error" object:self userInfo:nil];
                                             //                                                                                                        NSLog(@"Error in geting data");
                                             //                                                                                                        NSLog(@"%@", error.localizedDescription);
                                         }];
    
    [[EWebServiceClient sharedInstance] enqueueHTTPRequestOperation:operation];
}

-(void)handleErrorCode:(int)errorCode
{
    NSString* poruka;
    BOOL prikaziAV = NO;
    
    //    NSURLErrorUnknown = -1,
    //    NSURLErrorCancelled = -999,
    //    NSURLErrorBadURL = -1000,
    //    NSURLErrorTimedOut = -1001,
    //    NSURLErrorUnsupportedURL = -1002,
    //    NSURLErrorCannotFindHost = -1003,
    //    NSURLErrorCannotConnectToHost = -1004,
    //    NSURLErrorDataLengthExceedsMaximum = -1103,
    //    NSURLErrorNetworkConnectionLost = -1005,
    //    NSURLErrorDNSLookupFailed = -1006,
    //    NSURLErrorHTTPTooManyRedirects = -1007,
    //    NSURLErrorResourceUnavailable = -1008,
    //    NSURLErrorNotConnectedToInternet = -1009,
    //    NSURLErrorRedirectToNonExistentLocation = -1010,
    //    NSURLErrorBadServerResponse = -1011,
    //    NSURLErrorUserCancelledAuthentication = -1012,
    //    NSURLErrorUserAuthenticationRequired = -1013,
    //    NSURLErrorZeroByteResource = -1014,
    //    NSURLErrorCannotDecodeRawData = -1015,
    //    NSURLErrorCannotDecodeContentData = -1016,
    //    NSURLErrorCannotParseResponse = -1017,
    //    NSURLErrorInternationalRoamingOff = -1018,
    //    NSURLErrorCallIsActive = -1019,
    //    NSURLErrorDataNotAllowed = -1020,
    //    NSURLErrorRequestBodyStreamExhausted = -1021,
    //    NSURLErrorFileDoesNotExist = -1100,
    //    NSURLErrorFileIsDirectory = -1101,
    //    NSURLErrorNoPermissionsToReadFile = -1102,
    //    NSURLErrorSecureConnectionFailed = -1200,
    //    NSURLErrorServerCertificateHasBadDate = -1201,
    //    NSURLErrorServerCertificateUntrusted = -1202,
    //    NSURLErrorServerCertificateHasUnknownRoot = -1203,
    //    NSURLErrorServerCertificateNotYetValid = -1204,
    //    NSURLErrorClientCertificateRejected = -1205,
    //    NSURLErrorClientCertificateRequired = -1206,
    //    NSURLErrorCannotLoadFromNetwork = -2000,
    //    NSURLErrorCannotCreateFile = -3000,
    //    NSURLErrorCannotOpenFile = -3001,
    //    NSURLErrorCannotCloseFile = -3002,
    //    NSURLErrorCannotWriteToFile = -3003,
    //    NSURLErrorCannotRemoveFile = -3004,
    //    NSURLErrorCannotMoveFile = -3005,
    //    NSURLErrorDownloadDecodingFailedMidStream = -3006,
    //    NSURLErrorDownloadDecodingFailedToComplete = -3007
    
    switch (errorCode)
    {
        case 1:
            poruka =  @"Not connected to internet.\nFor application to work properly you need to connect to internet.\nPlease go to settings and try to connect to internet and then try again...";
            prikaziAV = YES;
            break;
        case 2:
            poruka = @"Server did not reply with all of the data required.";
            prikaziAV = YES;
            break;
        case 3:
            poruka = @"Server did not reply with valid data.";
            prikaziAV = YES;
            break;
        case NSURLErrorTimedOut:
            poruka = @"Server is not reachable at the moment.\n Please try again.";
            prikaziAV = NO;
            break;
        case NSURLErrorNotConnectedToInternet:
            poruka = @"Not conected to internet.";
            prikaziAV = NO;
            break;
        case NSURLErrorCancelled:
            prikaziAV = NO;
            break;
        default:
            NSLog(@"Error in getting data %d", errorCode);
            poruka = [NSString stringWithFormat:@"Error in data communication:\nErrorID = %d", errorCode];
            break;
    }
    if (prikaziAV)
        [self presentAlertViewWithMessage:poruka];
}

- (void)presentAlertViewWithMessage:(NSString*)message
{
    if (!self.isAVShown)
    {
        UIAlertController* ac = [UIAlertController alertControllerWithTitle:@"Error during request."
                                                                    message:message
                                                             preferredStyle:UIAlertControllerStyleAlert];
        [ac addAction:[UIAlertAction actionWithTitle:@"Try Again" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            self.isAVShown = NO;
            NSArray* copyErrorRequestArray = [self.errorRequestArray copy];
            for (NSDictionary* requestData in copyErrorRequestArray)
            {
                [self sendRequest:[requestData objectForKey:@"request"] successResponseBlock:[requestData objectForKey:@"successResponseBlock"] failureResponseBlock:[requestData objectForKey:@"failureResponseBlock"]];
                [self.errorRequestArray removeObjectAtIndex:0];
            }
            
        }]];
        [ac addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            self.isAVShown = NO;
        }]];
        [[Utils topViewController] presentViewController:ac animated:YES completion:nil];
        self.isAVShown = YES;
    }
}

@end
