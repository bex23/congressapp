//
//  IAMyTimelineViewController.m
//  ERA-EDTA2015
//
//  Created by admin on 7/18/14.
//  Copyright (c) 2014 Navus. All rights reserved.
//

#import "IAMyTimelineViewController.h"

#import "MyTimlineTableViewCell.h"
#import "PresentationDetailsViewController.h"

@interface IAMyTimelineViewController ()

@property (nonatomic, strong)NSFetchedResultsController* fetchedResultsController;
@property (nonatomic, strong)NSManagedObjectContext* managedObjectContext;

@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControlDays;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintSegmentedContorlDaysHeight;
@property (nonatomic) NSArray *days;
@property (weak, nonatomic) IBOutlet UIImageView *imgTopBanner;

@end

@implementation IAMyTimelineViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = self.isPoster ? @"Tagged posters" : @"My timeline";
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 300.0;
    
    self.managedObjectContext = [LLDataAccessLayer sharedInstance].managedObjectContext;
    
    [self loadData];
    

    
    User *user = [User current];
    if (user)
    {
        self.activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        UIBarButtonItem * barButton = [[UIBarButtonItem alloc] initWithCustomView:self.activityIndicator];
        [self navigationItem].rightBarButtonItem = barButton;
        [self.activityIndicator startAnimating];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(timelineSync:) name:@"TimelineSynced" object:nil];

        [[ESyncData sharedInstance] syncTimeline];
    }
    
    // register session cell
    UINib *nib = [UINib nibWithNibName:NSStringFromClass([MyTimlineTableViewCell class]) bundle:nil];
    [[self tableView] registerNib:nib forCellReuseIdentifier:NSStringFromClass([MyTimlineTableViewCell class])];
    
    [[BannersServices shared] addWithImageView:self.imgTopBanner
                                            at:BannerPositionTopBanner
                                   sender: self];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.segmentedControlDays.tintAdjustmentMode = UIViewTintAdjustmentModeNormal;
    
    [self doFetch];
}

#pragma mark - Sync Timeline

-(void)timelineSync:(NSNotification *)notification
{
    NSLog(@"%@ ", notification.name);
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:notification.name object:nil];
    
    [self loadData];
    
    [self.activityIndicator stopAnimating];
    

}

#pragma mark - Setups

-(void)setupDays
{
    NSMutableArray *array = [NSMutableArray array];
    
    NSArray *talks = [Talk getTimelinedTalks];
    if (self.isPoster) {
        talks = [talks filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"session.sesType == 'Poster'"]];
    } else {
        talks = [talks filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"session.sesType != 'Poster'"]];
    }
    
    
    
    for (Talk *talk in talks)
    {
        if (![array containsObject:[Utils getDate:talk.tlkStartTime]])
        {
            [array addObject:[Utils getDate:talk.tlkStartTime]];
        }
    }
    
    self.days = [NSArray arrayWithArray:array];
    
    [_segmentedControlDays removeAllSegments];
    _constraintSegmentedContorlDaysHeight.constant = 0;
    
    if (self.days.count > 0)
    {
        _constraintSegmentedContorlDaysHeight.constant = 45;
        
        int i;
        for(i = 0; i < self.days.count; i++)
        {
            NSString *strDate = [Utils formatDateString:[self.days objectAtIndex:i] fromFormat:kDateDefaultFormat toFormat:kSegmentedControlDateFormat];
            [_segmentedControlDays insertSegmentWithTitle:strDate atIndex:i animated:false];
        }
        
        [_segmentedControlDays setSelectedSegmentIndex:0];
    }
    
    [self.segmentedControlDays setTintColor:[UIColor whiteColor]];
}

-(void)loadData
{
    [self setupDays];
    
    [self doFetch];
}



#pragma mark FRC

-(NSFetchedResultsController *)fetchedResultsController
{
    if (!_fetchedResultsController)
    {
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"Talk" inManagedObjectContext:self.managedObjectContext];
        NSMutableArray *predicates = [[NSMutableArray alloc] initWithCapacity:1];
        
        if (self.isPoster) {
            [predicates addObject:[NSPredicate predicateWithFormat:@"zetBookmarked = 1 && tlkType = %@", kKeyPoster]];
        } else {
            [predicates addObject:[NSPredicate predicateWithFormat:@"zetBookmarked = 1 && tlkType != %@", kKeyPoster]];
        }
        
        if (self.days)
        {
            if(self.days.count > 0 && _segmentedControlDays.selectedSegmentIndex != -1)
            {
                NSString *date = [self.days objectAtIndex:_segmentedControlDays.selectedSegmentIndex];
                //RCF restore soon
                [predicates addObject:[NSPredicate predicateWithFormat:@"tlkStartTime CONTAINS [cd] %@", date]];
            }
        }
        
        fetchRequest.predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
        [fetchRequest setEntity:entity];
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"tlkStartTime" ascending:YES];
        NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
        [fetchRequest setSortDescriptors:sortDescriptors];
//        [NSFetchedResultsController deleteCacheWithName:@"DayCache"];
        NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
        aFetchedResultsController.delegate = self;
        self.fetchedResultsController = aFetchedResultsController;
    }
    return _fetchedResultsController;
}

-(void)doFetch
{
    self.fetchedResultsController = nil;
    
    NSError *error;
    if (![self.fetchedResultsController performFetch:&error])
    {
        NSLog(@"error %@ %@", error, error.userInfo);
        abort();
    }
    
    [self.tableView reloadData];
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeMove:
            break;
        case NSFetchedResultsChangeUpdate:
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            //[self configureCell:[self.tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

-(NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return self.isPoster ? @"Remove\nfrom tagged" : @"Remove\nfrom timeline";
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    Talk *talk = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    [Talk updateTimeline:talk isAddition:NO];
    
    NSError *error;
    
    [[LLDataAccessLayer sharedInstance].managedObjectContext save:&error];
    
    [self doFetch];
}

#pragma mark tableView

// because on iOS 8 doesn't work
// self.tableView.rowHeight = UITableViewAutomaticDimension;
// self.tableView.estimatedRowHeight = 200.0;
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger count = [[_fetchedResultsController sections] count];
    
    if (count == 0)
    {
        count = 1;
    }
    return count;
}

- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section
{
    int rows = 0;
    
    if ([[_fetchedResultsController sections] count] > 0)
    {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[_fetchedResultsController sections] objectAtIndex:section];
        rows = (int)[sectionInfo numberOfObjects];
    }
    
    return rows;
}

- (MyTimlineTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MyTimlineTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([MyTimlineTableViewCell class]) forIndexPath:indexPath];
    
    Talk *currentTalk;
    Talk *perviousTalk = nil;
    
    currentTalk = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    if(indexPath.row > 0)
        perviousTalk = [self.fetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForItem:indexPath.row-1 inSection:indexPath.section]];
    
    cell.lblTalkTitle.text = currentTalk.tlkTitle;
    cell.lblSesionRoom.text = currentTalk.session.venue.venName;
    
    [cell setSessionNo:currentTalk.session.sesCode];
    
    if (currentTalk.tlkAvailable.integerValue == 1)
    {
        cell.imgMaterialAvailabilityIcon.image = [UIImage imageNamed:kEmaterialsAvailable];
        [cell.imgMaterialAvailabilityIcon colorItWithColor:[UIColor colorWithHex:kGlobalColor andAlpha:1.0]];
    }
    else
    {
        cell.imgMaterialAvailabilityIcon.image = [UIImage imageNamed:kEmaterialsUnavailable];
        [cell.imgMaterialAvailabilityIcon colorItWithColor:[UIColor lightGrayColor]];
    }
    
    if (self.isPoster) {
        cell.imgQaIcon.image = [UIImage new];
    } else {
        cell.imgQaIcon.image = [UIImage imageNamed:[currentTalk.tlkVotingEnabled boolValue] ? kVotingAvailable : kVotingUnavailable];
    }

    [cell setTime:currentTalk];
    
//    if(perviousTalk && [self isInConflictPerviousTalk:perviousTalk currentTalk:currentTalk])
//    {
//        // maybe it will need, dont delete
//        //        cell.viewTimeBgd.backgroundColor = [UIColor redColor];
//        cell.lblTalkTime.textColor = [UIColor redColor];
//    }
//    else
//    {
//        // maybe it will need, dont delete
//        //        cell.viewTimeBgd.backgroundColor = [UIColor whiteColor];
//        //        cell.lblTalkTime.textColor = [UIColor colorWithRed:0/255.0 green:157.0/255.0 blue:224.0/255.0 alpha:1.0f];
//    }
    cell.viewSessionColor.backgroundColor = [SessionCategory getColorForSessionCategoryId: currentTalk.session.sesCategoryId];
    
    // because on iOS 8 doesn't work
    // self.tableView.rowHeight = UITableViewAutomaticDimension;
    // self.tableView.estimatedRowHeight = 200.0;
    [cell updateConstraintsIfNeeded];
    
    return cell;
}

-(BOOL)isInConflictPerviousTalk:(Talk *)perviousTalk currentTalk:(Talk*) currentTalk
{
    NSCalendar * gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    int flags = (NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute| NSCalendarUnitSecond);
    NSDateComponents * perviousTalkEndTimeDate = [gregorian components:flags fromDate:[Utils dateFromString:perviousTalk.tlkEndTime]];
    NSDateComponents * currentTalkStartTimeDate = [gregorian components:flags fromDate:[Utils dateFromString:currentTalk.tlkStartTime]];
    
    if (perviousTalkEndTimeDate.hour > currentTalkStartTimeDate.hour)
    {
        return YES;
    }
    else if (perviousTalkEndTimeDate.hour == currentTalkStartTimeDate.hour)
    {
        if (perviousTalkEndTimeDate.minute > currentTalkStartTimeDate.minute)
        {
            return YES;
        }
    }
    
    return NO;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSString *strID = NSStringFromClass([PresentationDetailsViewController class]);
    PresentationDetailsViewController *vc = [[UIStoryboard storyboardWithName:strID bundle:nil]instantiateViewControllerWithIdentifier:strID];
    
    vc.currentTalkIndex = (int)indexPath.row;
    vc.arrayTalks = [NSMutableArray arrayWithArray:[self.fetchedResultsController fetchedObjects]];
    
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - IBActions

- (IBAction)actionDayChanged:(UISegmentedControl *)sender
{
    [self doFetch];
}

#pragma mark - Set left margin of table cells to ziro for iOS 8

-(void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

@end
