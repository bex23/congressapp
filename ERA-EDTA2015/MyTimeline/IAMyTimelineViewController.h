//
//  IAMyTimelineViewController.h
//  ERA-EDTA2015
//
//  Created by admin on 7/18/14.
//  Copyright (c) 2014 Navus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IAMyTimelineViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (assign, nonatomic) BOOL isPoster;

@end
