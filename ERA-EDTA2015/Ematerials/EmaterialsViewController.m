//
//  EmaterialsViewController.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 4.2.16..
//  Copyright © 2016. Navus. All rights reserved.
//

#import "EmaterialsViewController.h"

#import "PresentationsListViewController.h"
#import "HTMLPreviewerViewController.h"

@interface EmaterialsViewController ()  <HTMLPreviewerDelegate>

@property (nonatomic, retain) WYPopoverController *selectPopoverController;
@property (weak, nonatomic) IBOutlet UIButton *btnUrl;
@property (weak, nonatomic) IBOutlet UIImageView *imgEmaterials;
@property (weak, nonatomic) IBOutlet UIImageView *imgEmaterialBanner;
@property (strong, nonatomic) IBOutlet NSDictionary *dictBanner;
@property (weak, nonatomic) IBOutlet UIButton *enterButton;

@end

@implementation EmaterialsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupPage];
}

-(void)setupPage
{
    // setup navigation page

    
    [[BannersServices shared] addWithImageView:self.imgEmaterialBanner
                                            at:BannerPositionEmaterialBanner
                                   sender: self];

    // setup link
    [self.btnUrl setTitle:[NSString stringWithFormat:@"www.e-materials.com/%@", [[RealmRepository shared] confSlug]]
                 forState:UIControlStateNormal];
    
    [self.enterButton setBackgroundColor:[UIColor whiteColor]];
    self.enterButton.layer.cornerRadius = 20.0;
    self.enterButton.layer.masksToBounds = YES;
    self.enterButton.layer.borderWidth = 1.0;
    self.enterButton.layer.borderColor = [[UIColor colorWithHex:kGlobalColor andAlpha:1.0] CGColor];
    [self.enterButton setTitleColor:[UIColor colorWithHex:kGlobalColor andAlpha:1.0]
                           forState:UIControlStateNormal];

    // color ematerials image
    [self.imgEmaterials colorItWithColor:[UIColor colorWithHex:kGlobalColor andAlpha:1.0]];
}



#pragma mark - IBActions

- (IBAction)actionEnter:(id)sender
{
    if ([Talk getAvailableTalks].count == 0)
    {
        [Utils showMessage:@"The E-materials service will be available during the congress."];
        return;
    }
    
    NSString *strID = NSStringFromClass([PresentationsListViewController class]);
    PresentationsListViewController *vc = [[UIStoryboard storyboardWithName:strID bundle:nil] instantiateViewControllerWithIdentifier:strID];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)actionInfo:(UIButton *)sender
{
    sender.enabled = NO;
    
    NSString *strID = NSStringFromClass([HTMLPreviewerViewController class]);
    HTMLPreviewerViewController* vc = [[UIStoryboard storyboardWithName:strID bundle:nil] instantiateViewControllerWithIdentifier:strID];
    
    vc.delegate = self;
    vc.hasCancelButton = @"YES";
    
    vc.htmlContent = [NSString stringWithFormat:@"<h2><b>E-materials</b></h2>E-materials are the presentation slides and webcasts that speakers made available for delegates.<br><br>You can also access your e-material account online at <br><b>www.e-materials.com/%@</b><br><br>Access available materials, submit messages to speakers, rate, give feedback. <br><br><b>New in %@: Create your own annual meeting summary via the online summary builder.</b><br><br>To learn more and to register visit our e-campus in the exhibition hall.", [[RealmRepository shared] confSlug], [[RealmRepository shared] confYear]];
    
    vc.preferredContentSize = CGSizeMake(self.view.frame.size.width - 20, vc.view.frame.size.height - 100);
    
    self.selectPopoverController = [[WYPopoverController alloc] initWithContentViewController:vc];
    
    [self.selectPopoverController presentPopoverFromRect:(self.view).bounds inView:(self.view) permittedArrowDirections:WYPopoverArrowDirectionNone animated:YES];
    
    sender.enabled = YES;
}

- (IBAction)actionOpenSite:(UIButton *)sender
{
    [CUtils showLeavingTheAppMessageWithUrl:sender.currentTitle];
}

#pragma mark - HTMLPreviewerDelegate

-(void)HTMLPreviewerDidClickLink:(NSString *)link
{
    [self.selectPopoverController dismissPopoverAnimated:YES completion:^{
        [CUtils showLeavingTheAppMessageWithUrl:link];
    }];
}

-(void)HTMLPreviewerCancel
{
    [self.selectPopoverController dismissPopoverAnimated:YES];
}

@end
