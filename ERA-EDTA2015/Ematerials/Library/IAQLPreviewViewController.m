//
//  IAQLPreviewViewController.m
//  ERA-EDTA2015
//
//  Created by admin on 7/10/14.
//  Copyright (c) 2014 Navus. All rights reserved.
//

#import "IAQLPreviewViewController.h"

@interface IAQLPreviewViewController ()

@end

@implementation IAQLPreviewViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController.navigationBar setTranslucent:NO];
}

@end
