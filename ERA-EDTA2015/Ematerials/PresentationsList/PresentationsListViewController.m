//
//  AvailableViewController.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 10.2.16..
//  Copyright © 2016. Navus. All rights reserved.
//

#import "PresentationsListViewController.h"

#import "EMSessionTableViewCell.h"
#import "PSMultiSelectViewController.h"
#import "PSSinglePickerViewController.h"
#import "PresentationDetailsViewController.h"

@interface PresentationsListViewController () <PSMultiSelectViewControllerDelegate, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, PSSinglePickerViewDelegate, UIGestureRecognizerDelegate, EMPresentationTableCellDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableViewMain;

@property (nonatomic, strong) NSArray *arrayOfSessions;
@property (nonatomic, strong) NSArray *arrayMain;

@property (nonatomic, strong) NSMutableArray *arrayBools;

@property (strong, nonatomic) UISearchController *searchController;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property (strong, nonatomic) NSString *searchStr;

@property (retain, nonatomic) NSMutableDictionary *sesRoomFlags;
@property (retain, nonatomic) NSMutableDictionary *sesDayFlags;

@property (weak, nonatomic) IBOutlet UILabel *lblFilterItems;

@property (nonatomic) BOOL inMyTimeline;

@property (strong, nonatomic) User *currentUser;

@property (strong, nonatomic) NSArray *arrayFilterOptions;

@property (nonatomic, retain) WYPopoverController *selectPopoverController;

@property (weak, nonatomic) IBOutlet UIButton *btnFilter;

@property (assign, nonatomic) BOOL isAllPresentationsFree;
@property (assign, nonatomic) BOOL isAllPresentationsFreeToday;

@property (weak, nonatomic) IBOutlet UIImageView *imgTopBanner;

@end

@implementation PresentationsListViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.currentUser = [User current];
    
    self.arrayFilterOptions = @[@"All", @"Day", @"Room", @"In My Timeline", @"Clear all filters"];
    
    self.searchBar.delegate = self;
    // remove top and bottom silver lines
    self.searchBar.layer.borderWidth = 1;
    self.searchBar.layer.borderColor = [[UIColor whiteColor] CGColor];
    
    // init filters params
    self.sesRoomFlags = [[NSMutableDictionary alloc] initWithCapacity:3];
    self.sesDayFlags = [[NSMutableDictionary alloc] initWithCapacity:3];
    self.inMyTimeline = 0;
    
    [self initFirst];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    tap.delegate = self;
    [self.view addGestureRecognizer:tap];
    
    self.tableViewMain.rowHeight = UITableViewAutomaticDimension;
    self.tableViewMain.estimatedRowHeight = 300.0;
    
    UIImage *image = [[UIImage imageNamed:@"e-materials-filter"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.btnFilter setImage:image forState:UIControlStateNormal];
    [self.btnFilter setImage:image forState:UIControlStateHighlighted];
    self.btnFilter.tintColor = [UIColor colorWithHex:kGlobalColor andAlpha:1.0];
    
    [[BannersServices shared] addWithImageView:self.imgTopBanner
                                            at:BannerPositionTopBanner
                                   sender: self];
}

-(void)dismissKeyboard
{
    // add self
    [self.searchBar resignFirstResponder];
}

-(void)initFirst
{
    [self loadArrayMain];
    [self loadArrayOfSessions];
    
    self.arrayBools = [[NSMutableArray alloc] init];
    [self initArrayOfBools];
    
    [self.tableViewMain reloadData];
}

#pragma mark - Bools array

-(void)initArrayOfBools
{
    for (int i = 0; i < [self.arrayOfSessions count]; i++)
    {
        [self.arrayBools addObject:@NO];
    }
}

-(void)changeArrayOfBoolsTo:(BOOL)value
{
    [self.arrayBools removeAllObjects];
    for (int i = 0; i < [self.arrayOfSessions count]; i++)
    {
        [self.arrayBools addObject:[NSNumber numberWithBool:value]];
    }
}

#pragma mark - Loading data

-(void)loadArrayMain
{
    self.arrayMain = [Session getSessions];
    
    NSSortDescriptor *sortDescriptor1 = [NSSortDescriptor sortDescriptorWithKey:@"venue.venName" ascending:YES];
    NSSortDescriptor *sortDescriptor2 = [NSSortDescriptor sortDescriptorWithKey:@"sesStartTime" ascending:YES];
    
    self.arrayMain = [self.arrayMain sortedArrayUsingDescriptors:@[sortDescriptor1, sortDescriptor2]];
    self.arrayMain = [self.arrayMain filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"sesType != 'Poster'"]];
    if (self.searchStr.length > 0)
    {
        self.arrayMain = [self.arrayMain filteredArrayUsingPredicate:[NSPredicate
                                                                            predicateWithFormat:@"SUBQUERY(talks, $tlk, ($tlk.tlkHasMainMaterial == 1 || $tlk.tlkHasSupportingMaterial == 1 || $tlk.tlkHasWebcastMaterial == 1) AND $tlk.tlkTitle CONTAINS[cd] %@).@count>0", _searchStr]];
    }
    else
    {
        self.arrayMain = [self.arrayMain filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SUBQUERY(talks, $tlk, $tlk.tlkHasMainMaterial == 1 || $tlk.tlkHasSupportingMaterial == 1 || $tlk.tlkHasWebcastMaterial == 1).@count>0"]];
    }
    
    self.arrayOfSessions = self.arrayMain;
}

-(void)loadArrayOfSessions
{
    if (self.searchStr.length > 0)
    {
        self.arrayOfSessions = [self.arrayMain filteredArrayUsingPredicate:[NSPredicate
                                                                            predicateWithFormat:@"SUBQUERY(talks, $tlk, ($tlk.tlkHasMainMaterial == 1 || $tlk.tlkHasSupportingMaterial == 1 || $tlk.tlkHasWebcastMaterial == 1) AND $tlk.tlkTitle CONTAINS[cd] %@).@count>0", _searchStr]];
    }
    else
    {
        self.arrayOfSessions = [self.arrayMain filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SUBQUERY(talks, $tlk, $tlk.tlkHasMainMaterial == 1 || $tlk.tlkHasSupportingMaterial == 1 || $tlk.tlkHasWebcastMaterial == 1).@count>0"]];
    }
    
    NSString *strDisplayingItems = @"";
    
    if (![self checkIfAllValueZero:self.sesRoomFlags])
    {
        NSMutableArray *arrayTemp = [[NSMutableArray alloc] init];
        
        for (NSString *roomName in self.sesRoomFlags)
        {
            if ([[self.sesRoomFlags valueForKey:roomName] integerValue] == 1)
            {
                NSArray *arrayFiteredSessions = [self.arrayOfSessions filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"venue.venName == %@", roomName]];
                [arrayTemp addObjectsFromArray:arrayFiteredSessions];
                strDisplayingItems = [strDisplayingItems stringByAppendingString:[NSString stringWithFormat:@"%@,", roomName]];
            }
        }
        
        self.arrayOfSessions = arrayTemp;
    }
    if (![self checkIfAllValueZero:self.sesDayFlags])
    {
        NSMutableArray *arrayTemp = [[NSMutableArray alloc] init];
        
        for (NSString *day in self.sesDayFlags)
        {
            if ([[self.sesDayFlags valueForKey:day] integerValue] == 1)
            {
                NSArray *arrayFiteredSessions = [self.arrayOfSessions filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"zetDay == %@", [Utils formatDateString:day fromFormat:kDateFormat toFormat:kDateDefaultFormat]]];
                [arrayTemp addObjectsFromArray:arrayFiteredSessions];
                strDisplayingItems = [strDisplayingItems stringByAppendingString:[NSString stringWithFormat:@"%@,", day]];
            }
        }
        
        self.arrayOfSessions = arrayTemp;
    }
    
    if (self.inMyTimeline == 1)
    {
        self.arrayOfSessions = [self.arrayOfSessions filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SUBQUERY(talks, $tlk, $tlk.zetBookmarked == 1).@count>0"]];
        strDisplayingItems = [strDisplayingItems stringByAppendingString:@"In My Timeline,"];
    }
    
    if ([self checkIfAllValueZero:self.sesDayFlags] && [self checkIfAllValueZero:self.sesRoomFlags] && !self.inMyTimeline)
    {
        strDisplayingItems = @"All";
    }
    
    
    self.lblFilterItems.text = strDisplayingItems;
}

-(BOOL)checkIfAllValueZero:(NSMutableDictionary *)dic
{
    for (NSString *key in [dic allKeys])
    {
        if ([[dic objectForKey:key] integerValue] == 1)
        {
            return NO;
        }
    }
    
    return YES;
}

-(void)loadAllData
{
    [self loadArrayMain];
    [self loadArrayOfSessions];
    [self.tableViewMain reloadData];
}

#pragma mark - UISearchBarDelegate

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    self.searchStr = searchText;
    
    [self loadArrayOfSessions];
    
    if (self.searchStr.length > 0)
    {
        [self changeArrayOfBoolsTo:YES];
    }
    else
    {
        [self changeArrayOfBoolsTo:NO];
    }
    
    [self.tableViewMain reloadData];
}

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [self.tableViewMain reloadData];
    searchBar.showsCancelButton = NO;
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

#pragma mark - TableView data source

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.arrayOfSessions count];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([self.arrayBools[section] boolValue])
    {
        Session *session = [self.arrayOfSessions objectAtIndex:section];
        
        NSArray *arrayOfTalks = [self filterArrayOfTalksForSession:session];
        
        return arrayOfTalks.count + 1;
    }
    else
    {
        return 1;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Session *session = [self.arrayOfSessions objectAtIndex:indexPath.section];
    if (indexPath.row == 0)
    {
        EMSessionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([EMSessionTableViewCell class]) forIndexPath:indexPath];
        cell.lblSessionName.text = session.sesName;
        [cell setRoomAndTime:session];
        
        NSArray *arrayOfTalks = [self filterArrayOfTalksForSession:session];
        
        if (arrayOfTalks.count == 0)
        {
            cell.constraintImgsWidth.constant = 0;
            cell.imgArrow.hidden = YES;
        }
        else
        {
            if ([self.arrayBools[indexPath.section] boolValue]) // open
            {
                cell.imgArrow.image = [UIImage imageNamed:@"downArrow"];
                cell.separatorInset = UIEdgeInsetsMake(0.f, cell.bounds.size.width, 0.f, 0.f);
            }
            else // close
            {
                cell.imgArrow.image = [UIImage imageNamed:@"gray_rightArrow"];
                cell.separatorInset = UIEdgeInsetsZero;
                cell.layoutMargins = UIEdgeInsetsZero;
            }
            cell.constraintImgsWidth.constant = 40;
            cell.imgArrow.hidden = NO;
        }
        
        cell.viewSessionColorCategory.backgroundColor = [SessionCategory getColorForSessionCategoryId:session.sesCategoryId];
        cell.viewSessionColorCategory.layer.cornerRadius = 5;
        
        cell.viewRounded.layer.cornerRadius = 5;
        // border
        cell.viewRounded.layer.borderWidth = 1.0;
        cell.viewRounded.layer.borderColor = [UIColor colorWithHex:@"ECEBEC" andAlpha:1.0].CGColor;
        
        // shadow
        cell.viewRounded.layer.shadowColor = [UIColor lightGrayColor].CGColor;
        cell.viewRounded.layer.shadowOffset = CGSizeMake(1, 1);
        cell.viewRounded.layer.shadowOpacity = 0.5;
        cell.viewRounded.layer.shadowRadius = 1;
        
        // because on iOS 8 doesn't work
        // self.tableView.rowHeight = UITableViewAutomaticDimension;
        // self.tableView.estimatedRowHeight = 200.0;
        [cell updateConstraintsIfNeeded];
        
        return cell;
    }
    else
    {
        EMPresentationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([EMPresentationTableViewCell class]) forIndexPath:indexPath];
        
        cell.delegate = self;
        
        NSArray *arrayOfTalks = [self filterArrayOfTalksForSession:session];
        if (indexPath.row - 1 >= arrayOfTalks.count) return [UITableViewCell new];
        
        Talk *talk = arrayOfTalks[indexPath.row - 1];

        cell.lblSpeakerName.text = talk.tlkSpeakerName;
        cell.lblPresentationName.text = talk.tlkTitle;
        
        [cell setTime:talk];
        
        cell.talk = talk;
        
        cell.viewRounded.layer.cornerRadius = 5;
        // border
        cell.viewRounded.layer.borderWidth = 1.0;
        cell.viewRounded.layer.borderColor = [UIColor colorWithHex:@"ECEBEC" andAlpha:1.0].CGColor;
        
        // shadow
        cell.viewRounded.layer.shadowColor = [UIColor lightGrayColor].CGColor;
        cell.viewRounded.layer.shadowOffset = CGSizeMake(1, 1);
        cell.viewRounded.layer.shadowOpacity = 0.5;
        cell.viewRounded.layer.shadowRadius = 1;
        
        [self configureGainAccessButtonForCell:cell];
        
        cell.separatorInset = UIEdgeInsetsZero;
        cell.layoutMargins = UIEdgeInsetsZero;
        
        // because on iOS 8 doesn't work
        // self.tableView.rowHeight = UITableViewAutomaticDimension;
        // self.tableView.estimatedRowHeight = 200.0;
        [cell updateConstraintsIfNeeded];
        
        return cell;
    }
}

-(void)configureGainAccessButtonForCell:(EMPresentationTableViewCell *)cell
{
//    if (self.isAllPresentationsFree == YES
//        || [cell.talk.session.sesType isEqual: kKeyIndustrialSymposium]
//        || [cell.talk.session.sesType isEqual: kKeyPoster]
//        || self.currentUser.eatMember.integerValue == 1
//        || [Order doesExistOrderWithTalkId:cell.talk.tlkTalkId]
//        || [cell.talk.session.sesAttended isEqualToString:@"1"]
//        || ([CUtils checkIfSessionToday:cell.talk.session] == YES && self.isAllPresentationsFreeToday == YES))
//    {
//    if(self.currentUser.eatMember.integerValue == 1 || [Order doesExistOrderWithTalkId:cell.talk.tlkTalkId])
//    {
        cell.constraintWidthOfLblGainAccess.constant = 0;
//    }
//    else
//    {
//        cell.lblGainAccess.layer.cornerRadius = 5.0;
//        cell.lblGainAccess.clipsToBounds = YES;
//
//        cell.constraintWidthOfLblGainAccess.constant = 75;
//    }
}

// because on iOS 8 doesn't work
// self.tableView.rowHeight = UITableViewAutomaticDimension;
// self.tableView.estimatedRowHeight = 200.0;
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

#pragma mark - TableView delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        NSNumber *toggle = [NSNumber numberWithBool:![self.arrayBools[indexPath.section] boolValue]];
        self.arrayBools[indexPath.section] = toggle;
        
        [tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];
    }
    else
    {
        EMPresentationTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        
        NSString *strID = NSStringFromClass([PresentationDetailsViewController class]);
        PresentationDetailsViewController *vc = [[UIStoryboard storyboardWithName:strID bundle:nil] instantiateViewControllerWithIdentifier:strID];

        NSArray *arrayOfTalksInAllListedSessions = [self getTalksForAllListedSessions];
        
        vc.arrayTalks = [NSMutableArray arrayWithArray:arrayOfTalksInAllListedSessions];
        vc.currentTalkIndex = (int)[arrayOfTalksInAllListedSessions indexOfObject:cell.talk];
        
        [self.navigationController pushViewController:vc animated:YES];
    }
}

-(NSArray *)getTalksForAllListedSessions
{
    NSMutableArray *arrayOfTalksInAllListedSessions = [NSMutableArray array];
    
    for (Session *session in _arrayOfSessions)
    {
        NSArray *arrayOfTalksInSession = [self filterArrayOfTalksForSession:session];
        if (arrayOfTalksInSession.count)
        {
            [arrayOfTalksInAllListedSessions addObjectsFromArray:arrayOfTalksInSession];
        }
    }
    
    return arrayOfTalksInAllListedSessions;
}

#pragma mark - Filtering

-(IBAction)showFilter:(UIButton *)sender
{
    PSSinglePickerViewController* spVC = [[PSSinglePickerViewController alloc] initWithItems:_arrayFilterOptions DisplayMember:nil];
    
    spVC.delegate = self;
    spVC.preferredContentSize = CGSizeMake(170, _arrayFilterOptions.count * 50);
    spVC.popovertag = @"chooseFilterValuePopover";
    spVC.disableTableViewScrollable = YES;
    spVC.backgroudColor = [UIColor colorWithHex:kGlobalColor andAlpha:1.0];
    spVC.textColor = [UIColor whiteColor];
    spVC.tableViewTintColor = [UIColor colorWithHex:kGlobalColor andAlpha:1.0];
    
    spVC.selectedRow = -2;
    
    self.selectPopoverController = [[WYPopoverController alloc] initWithContentViewController:spVC];
    self.selectPopoverController.theme.fillTopColor = [UIColor colorWithHex:kGlobalColor andAlpha:1.0];
    
    [self.selectPopoverController presentPopoverFromRect:(sender).bounds inView:(sender) permittedArrowDirections:WYPopoverArrowDirectionUp animated:YES];
}

#pragma mark - PSSinglePickerViewDelegate

-(void)SinglePickerItemSelected:(id)item Sender:(id)sender
{
    PSSinglePickerViewController *spVC = (PSSinglePickerViewController *)sender;
    
    if ([spVC.popovertag isEqualToString:@"chooseFilterValuePopover"])
    {
        [self.selectPopoverController dismissPopoverAnimated:YES completion:^{
            
            NSString *option = (NSString *)item;
            [self actionForOption:option];
        }];

    }
}

-(void)actionForOption:(NSString *)option
{
    if ([option isEqualToString:@"All"] || [option isEqualToString:@"Clear all filters"])
    {
        [self clearFilter];
    }
    else if ([option isEqualToString:@"Room"])
    {
        [self filterByRoom];
    }
    else if ([option isEqualToString:@"Day"])
    {
        [self filterByDay];
    }
    else if ([option isEqualToString:@"In My Timeline"])
    {
        self.inMyTimeline = !self.inMyTimeline;
        [self loadArrayOfSessions];
        [self changeArrayOfBoolsTo:YES];
        [self.tableViewMain reloadData];
    }
}

-(void)clearFilter
{
    [self.sesDayFlags removeAllObjects];
    [self.sesRoomFlags removeAllObjects];
    
    self.inMyTimeline = 0;

    [self loadArrayOfSessions];
    [self changeArrayOfBoolsTo:NO];
    [self.tableViewMain reloadData];
}

- (void)filterByDay
{
    NSArray* daysArray = [self searchForFilterName:@"Days"];
    
    if (!self.sesDayFlags.count)
    {
        for(NSDictionary* day in daysArray)
        {
            [self.sesDayFlags setValue:[NSNumber numberWithBool:FALSE] forKey:[day objectForKey:@"day"]];
        }
    }
    
    PSMultiSelectViewController* mpVC = [[PSMultiSelectViewController alloc]
                                         initWithItemArray:daysArray
                                         Flags:self.sesDayFlags
                                         Title:@"Select Sesssion Day"
                                         ValueMember:@"day"
                                         DisplayMember:@"day"
                                         FilterName:@"Days"];
    mpVC.delegate = self;
    mpVC.tag = @"sesDayPicker";
    mpVC.title = @"Session Days";
    
    [self.navigationController pushViewController:mpVC animated:YES];
}

- (void)filterByRoom
{
    NSArray* roomsArray = [self searchForFilterName:@"Rooms"];
    
    if (!self.sesRoomFlags.count)
    {
        for(NSDictionary* room in roomsArray)
        {
            [self.sesRoomFlags setValue:[NSNumber numberWithBool:FALSE] forKey:[room objectForKey:@"roomName"]];
        }
    }
    
    PSMultiSelectViewController* mpVC = [[PSMultiSelectViewController alloc]
                                         initWithItemArray:roomsArray
                                         Flags:self.sesRoomFlags
                                         Title:@"Select Session Room"
                                         ValueMember:@"roomName"
                                         DisplayMember:@"roomName"
                                         FilterName:@"Rooms"];
    mpVC.delegate = self;
    mpVC.tag = @"sesRoomPicker";
    mpVC.title = @"Session Rooms";
    
    [self.navigationController pushViewController:mpVC animated:YES];
}

-(NSArray *)searchForFilterName:(NSString *)filterName
{
    NSArray *arrayOfElements = nil;
    if([filterName isEqualToString:@"Rooms"])
    {
        //get grouped room names
        NSMutableSet* roomsSet = [[NSMutableSet alloc] initWithCapacity:3];
        
        for(Session* ses in self.arrayMain)
        {
            if ([ses.venue.venType isEqualToString:@"Room"])
                [roomsSet addObject:ses.venue.venName];
        }
        
        NSMutableArray* sesRooms = [[NSMutableArray alloc] initWithCapacity:3];
        for(NSString* roomName in roomsSet)
        {
            NSDictionary* dict = [NSDictionary dictionaryWithObject:roomName forKey:@"roomName"];
            
            [sesRooms addObject:dict];
        }
        NSArray* sortedRoomsArray = [sesRooms sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"roomName" ascending:YES]]];
        arrayOfElements = [sortedRoomsArray copy];
    }
    else if([filterName isEqualToString:@"Days"])
    {
        //get grouped room names
        NSMutableSet* daysSet = [[NSMutableSet alloc] initWithCapacity:3];
        
        for(Session* ses in self.arrayMain)
        {
            if (ses.sesStartTime.length > 0)
            {
                NSString *formatedDate = [Utils formatDateString:ses.sesStartTime fromFormat:kDateFormatInDatabase toFormat:kDateFormat];
                if (formatedDate.length) {
                    [daysSet addObject:formatedDate];
                }
            }
        }
        
        NSMutableArray* sesDays = [[NSMutableArray alloc] initWithCapacity:3];
        for(NSString* day in daysSet)
        {
            NSDictionary* dict = [NSDictionary dictionaryWithObject:day forKey:@"day"];
            
            [sesDays addObject:dict];
        }
        NSArray* sortedRoomsArray = [sesDays sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"day" ascending:YES]]];
        arrayOfElements = [sortedRoomsArray copy];
    }
    
    return arrayOfElements;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - MultiPickerDelegate methods
////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)MPselectionDone:(id)sender
{
    if([sender isKindOfClass:[PSMultiSelectViewController class]])
    {
        [self loadArrayOfSessions];
        [self changeArrayOfBoolsTo:YES];
        [self.tableViewMain reloadData];
    }
}

- (void)MPitemSelected:(id)item sender:(id)sender
{}

#pragma mark - IAPresentationInformationViewControllerDelegate

-(void)didAddPresentationToLibrary
{
    [self loadAllData];
}

#pragma mark - other functions

-(NSArray *)filterArrayOfTalksForSession:(Session *)session
{
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"tlkStartTime" ascending:YES];
    
    NSArray *arrayOfTalks = [[[session.talks allObjects] sortedArrayUsingDescriptors:@[sortDescriptor]] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"tlkHasMainMaterial == 1 || tlkHasWebcastMaterial == 1 || tlkHasSupportingMaterial == 1"]];
    
    if (self.inMyTimeline)
    {
        arrayOfTalks = [[arrayOfTalks sortedArrayUsingDescriptors:@[sortDescriptor]] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"zetBookmarked == 1"]];
    }
    
    if (self.searchStr.length > 0)
    {
        arrayOfTalks = [arrayOfTalks filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"tlkTitle CONTAINS[cd] %@", self.searchStr]];
    }
    
    return arrayOfTalks;
}

////////////////////////////////////////////////////////////
// UIGestureRecognizerDelegate methods

#pragma mark UIGestureRecognizerDelegate methods

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:self.tableViewMain] && !self.searchBar.isFirstResponder) {
        
        // Don't let selections of auto-complete entries fire the
        // gesture recognizer
        return NO;
    }
    
    return YES;
}

#pragma mark - EMPresentationTableCellDelegate

-(void)gainAccessWasClicked:(EMPresentationTableViewCell *)cell
{
    [cell.activator startAnimating];
    self.view.userInteractionEnabled = NO;
    
    [[ESyncData sharedInstance] getPresentationsCost:cell.talk.tlkTalkId
                                         withSuccess:^(int cost)
     {
         // get epoints for gain message
         [[ESyncData sharedInstance] getEpointsWithSuccess:^(int epoints) {
             
             [cell.activator stopAnimating];
             self.view.userInteractionEnabled = YES;
             [self showGainAccessMessageForEpoints:epoints withCost:cost forCell:cell];
             
         } failure:^{
             [cell.activator stopAnimating];
             self.view.userInteractionEnabled = YES;
         }];
     } failure:^{
         [cell.activator stopAnimating];
         self.view.userInteractionEnabled = YES;
     }];

}

-(void)showGainAccessMessageForEpoints:(int)epoints withCost:(int)cost forCell:(EMPresentationTableViewCell *)cell
{
    if(cost == 0)
    {
        [[ESyncData sharedInstance] orderderTalk:cell.talk success:^{
            cell.constraintWidthOfLblGainAccess.constant = 0;
        } failure:^{
        }];
    }
    else
    {
        NSString *epointsString = [NSString stringWithFormat:@"%d", epoints];
        NSString *costString = [NSString stringWithFormat:@"%d", cost];
        
        UIAlertController *alert;
        if(epoints == 0)
        {
            alert = [UIAlertController
                     alertControllerWithTitle:@""
                     message: @"You currently have no further access points left for this related event. Please contact, support@e-materials.com to request access to this presentation."
                     preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* btnContactUs = [UIAlertAction actionWithTitle:@"Contact Us"
                                                                   style:UIAlertActionStyleDefault
                                                                 handler:^(UIAlertAction * action) {
                                                                     
                                                                     NSString *recipients = @"mailto:support@e-materials.com";
                                                                     
                                                                     NSString *email = recipients;
                                                                     
                                                                     email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                                                                     
                                                                     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
                                                                 }];
            [alert addAction:btnContactUs];
        }
        else
        {
            alert = [UIAlertController
                     alertControllerWithTitle:@""
                     message: [NSString stringWithFormat:@"Gaining access to \n%@\n will cost you %@ access point.\nYou currently have %@ access %@ left for this related event.\nDo you want to access this presentation?", cell.talk.tlkTitle, costString, epointsString, epoints > 1 ? @"points" : @"point"]
                     preferredStyle:UIAlertControllerStyleAlert];
            
            
            UIAlertAction* btnRegister = [UIAlertAction actionWithTitle:@"GAIN ACCESS"
                                                                  style:UIAlertActionStyleDefault
                                                                handler:^(UIAlertAction * action) {
                                                                    
                                                                    [cell.activator startAnimating];
                                                                    self.view.userInteractionEnabled = NO;
                                                                    
                                                                    [[ESyncData sharedInstance] orderderTalk:cell.talk success:^{
                                                                        
                                                                        [cell.activator stopAnimating];
                                                                        self.view.userInteractionEnabled = YES;
                                                                        
                                                                        cell.constraintWidthOfLblGainAccess.constant = 0;
                                                                        
                                                                    } failure:^{
                                                                        [cell.activator stopAnimating];
                                                                        self.view.userInteractionEnabled = YES;
                                                                    }];
                                                                }];
            [alert addAction:btnRegister];
        }
        
        UIAlertAction* btnCancel = [UIAlertAction actionWithTitle:@"CANCEL"
                                                            style:UIAlertActionStyleDefault
                                                          handler:nil];
        
        [alert addAction:btnCancel];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

@end
