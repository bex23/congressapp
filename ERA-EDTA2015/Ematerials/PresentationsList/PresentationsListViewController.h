//
//  AvailableViewController.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 10.2.16..
//  Copyright © 2016. Navus. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "EMPresentationTableViewCell.h"

@interface PresentationsListViewController : UIViewController

@property (strong, nonatomic) NSString *type;

@end
