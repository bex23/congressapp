//
//  EMSessionTableViewCell.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 12/11/16.
//  Copyright © 2016 Navus. All rights reserved.
//

#import "EMSessionTableViewCell.h"

@implementation EMSessionTableViewCell

-(void)setRoomAndTime:(id)managedObject
{
    NSString* startTime = nil;
    NSString* endTime = nil;
    
    self.lblRoomAndTime.text = @"";
    
    if ([managedObject class] == [Session class])
    {
        startTime = [Utils formatTimeStringFromDate: [managedObject valueForKey:@"sesStartTime"]];
        endTime= [Utils formatTimeStringFromDate: [managedObject valueForKey:@"sesEndTime"]];
        
        if ([managedObject valueForKey:@"venue"])
        {
            Venue *venue = [managedObject valueForKey:@"venue"];
            self.lblRoomAndTime.text = [NSString stringWithFormat:@"%@ | ", venue.venName];
        }
        
        self.lblRoomAndTime.text = [self.lblRoomAndTime.text stringByAppendingString:[NSString stringWithFormat:@"%@ - %@", startTime, endTime]];
    }
}

@end
