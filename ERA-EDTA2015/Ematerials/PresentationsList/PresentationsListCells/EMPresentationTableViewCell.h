//
//  EMPresentationTableViewCell.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 12/11/16.
//  Copyright © 2016 Navus. All rights reserved.
//

#import <UIKit/UIKit.h>

@class EMPresentationTableViewCell;

@protocol EMPresentationTableCellDelegate <NSObject>
-(void)gainAccessWasClicked:(EMPresentationTableViewCell *)cell;
@end

@interface EMPresentationTableViewCell : UITableViewCell


@property (weak, nonatomic) id <EMPresentationTableCellDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIView *viewRounded;
@property (weak, nonatomic) IBOutlet UILabel *lblSpeakerName;
@property (weak, nonatomic) IBOutlet UILabel *lblPresentationName;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UILabel *lblGainAccess;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintWidthOfLblGainAccess;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activator;

@property (strong, nonatomic) Talk *talk;

-(void)setTime:(id)managedObject;

@end
