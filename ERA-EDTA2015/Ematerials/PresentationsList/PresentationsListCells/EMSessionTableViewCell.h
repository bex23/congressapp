//
//  EMSessionTableViewCell.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 12/11/16.
//  Copyright © 2016 Navus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EMSessionTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *viewRounded;
@property (weak, nonatomic) IBOutlet UIView *viewSessionColorCategory;
@property (nonatomic, assign) BOOL opened;
@property (weak, nonatomic) IBOutlet UILabel *lblSessionName;
@property (weak, nonatomic) IBOutlet UILabel *lblRoomAndTime;
@property (weak, nonatomic) IBOutlet UIImageView *imgArrow;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintImgsWidth;

-(void)setRoomAndTime:(id)managedObject;

@end
