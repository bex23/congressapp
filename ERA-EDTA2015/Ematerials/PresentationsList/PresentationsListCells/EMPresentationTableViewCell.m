//
//  EMPresentationTableViewCell.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 12/11/16.
//  Copyright © 2016 Navus. All rights reserved.
//

#import "EMPresentationTableViewCell.h"

@implementation EMPresentationTableViewCell

-(void)setTime:(id)managedObject
{
    NSString* startTime = nil;
    NSString* endTime = nil;
    
    if ([managedObject class] == [Talk class])
    {
        startTime = [Utils formatTimeStringFromDate: [managedObject valueForKey:@"tlkStartTime"]];
        
        endTime= [Utils formatTimeStringFromDate: [managedObject valueForKey:@"tlkEndTime"]];
    }
    
    self.lblTime.text = [NSString stringWithFormat:@"%@ - %@", startTime, endTime];
}

- (IBAction)actionGainAccess:(UIButton *)sender
{
    if([self.delegate respondsToSelector:@selector(gainAccessWasClicked:)])
    {
        [self.delegate gainAccessWasClicked:self];
    }
}

@end
