//
//  OXYTwitterViewController.m
//  ERA-EDTA2015
//
//  Created by Predrag Despotovic on 5/12/15.
//  Copyright (c) 2015 Navus. All rights reserved.
//

#import "TwitterViewController.h"

@interface TwitterViewController () <UIWebViewDelegate>

@property (strong, nonatomic) IBOutlet UIWebView *www;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *ai;
@property (strong, nonatomic) NSURLRequest* twitterReq;
@property (strong, nonatomic) NSString *twitterHash;

@end

@implementation TwitterViewController

- (NSString *)twitterHash {
    if (!_twitterHash) {
        NSString *h;
        if ([[[[[Application shared] navigator] conference] name] isEqualToString:@"KML 2018"]) {
            h = @"kml2018";
        } else {
            h = @"era2018";
        }
        _twitterHash = h;
    } return _twitterHash;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSString *url = [NSString stringWithFormat:@"http://e-materials.com/app/%@/twitter.html", [self.twitterHash lowercaseString]];
    
    self.twitterReq = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    
    [self.www loadRequest:self.twitterReq];
    [self.ai startAnimating];
    
    self.www.delegate = self;
    
    self.title = [NSString stringWithFormat:@"Twitter %@", self.twitterHash];
}

-(IBAction)tweetAction:(UIBarButtonItem*)sender
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"twitter://"]])
    {
        NSString *url = [NSString stringWithFormat:@"twitter://search?query=%@", self.twitterHash];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    }
    else
    {
        UIAlertController* ac = [UIAlertController alertControllerWithTitle:@"No Twitter app found" message:@"Please install the Twitter app, login and then try composing again..." preferredStyle:UIAlertControllerStyleAlert];
        [ac addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil]];
        [self presentViewController:ac animated:YES completion:nil];
    }
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    [self.ai stopAnimating];
}

@end
