//
//  PosterTableViewCell.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 2/17/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "PosterTableViewCell.h"

@implementation PosterTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void)setTime:(id)managedObject
{
    NSString* startTime = nil;
    NSString* endTime = nil;
    
    if ([managedObject class] == [Talk class])
    {
        startTime = [Utils formatTimeStringFromDate: [managedObject valueForKey:@"tlkStartTime"]];
        
        endTime= [Utils formatTimeStringFromDate: [managedObject valueForKey:@"tlkEndTime"]];
    }
    else
    {
        startTime = [Utils formatTimeStringFromDate: [managedObject valueForKey:@"sesStartTime"]];
        
        endTime= [Utils formatTimeStringFromDate: [managedObject valueForKey:@"sesEndTime"]];
    }
    
    if (startTime.length == 0 && endTime.length == 0)
    {
        self.lblTime.text = @"";
    }
    else
    {
        self.lblTime.text = [NSString stringWithFormat:@"%@ - %@", startTime, endTime];
    }
}

-(void)setBoardNo:(NSString *)boardNo
{
    NSString *trimmedBoardNo = [boardNo stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    _lblBoardNo.text = trimmedBoardNo;
    
    if (trimmedBoardNo.length)
    {
        self.lblBoardNoText.text = @"Board no:";
    }
    else
    {
        self.lblBoardNoText.text = nil;
    }
}

@end
