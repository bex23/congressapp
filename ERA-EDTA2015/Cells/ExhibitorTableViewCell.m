//
//  IAExibitorTableViewCell.m
//  ERA-EDTA2015
//
//  Created by Predrag Despotović on 7/19/14.
//  Copyright (c) 2014 Navus. All rights reserved.
//

#import "ExhibitorTableViewCell.h"

@implementation ExhibitorTableViewCell : UITableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    // Initialization code
    
    self.viewBorder.layer.cornerRadius = 3;
    // border
    self.viewBorder.layer.borderWidth = 1.0;
    self.viewBorder.layer.borderColor = [UIColor colorWithHex:@"ECEBEC" andAlpha:1.0].CGColor;
    
    // shadow
    self.viewBorder.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    self.viewBorder.layer.shadowOffset = CGSizeMake(1, 1);
    self.viewBorder.layer.shadowOpacity = 0.5;
    self.viewBorder.layer.shadowRadius = 1;
}

@end
