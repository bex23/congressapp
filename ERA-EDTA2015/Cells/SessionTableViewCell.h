//
//  SessionTableViewCell.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 12/11/16.
//  Copyright © 2016 Navus. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SessionTableViewCell;

@interface SessionTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblSessionTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblSessionNoText;
@property (strong, nonatomic) IBOutlet UILabel *lblRoomSessionNo;
@property (strong, nonatomic) IBOutlet UILabel *lblTime;
@property (retain, nonatomic) IBOutlet UIView *viewCategoryColor;
@property (strong, nonatomic) IBOutlet UILabel *lblRoomName;

-(void)setTime:(id)managedObject;
-(void)setViewCategoryColorBackgroundColor:(UIColor*)categoryColor;
-(void)setSessionNo:(NSString *)sessionNo;

@end
