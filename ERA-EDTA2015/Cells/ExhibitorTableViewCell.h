//
//  IAExibitorTableViewCell.h
//  ERA-EDTA2015
//
//  Created by Predrag Despotović on 7/19/14.
//  Copyright (c) 2014 Navus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExhibitorTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgExibitorLogo;
@property (weak, nonatomic) IBOutlet UILabel *lblExibitorName;
@property (weak, nonatomic) IBOutlet UIView *viewBorder;

@end
