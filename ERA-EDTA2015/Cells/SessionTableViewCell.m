//
//  SessionTableViewCell.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 12/11/16.
//  Copyright © 2016 Navus. All rights reserved.
//

#import "SessionTableViewCell.h"

@implementation SessionTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void)setTime:(id)managedObject
{
    NSString* startTime = nil;
    NSString* endTime = nil;
    
    if ([managedObject class] == [Talk class])
    {
        startTime = [Utils formatTimeStringFromDate:[managedObject valueForKey:@"tlkStartTime"]];
        endTime= [Utils formatTimeStringFromDate:[managedObject valueForKey:@"tlkEndTime"]];
    }
    else
    {
        startTime = [Utils formatTimeStringFromDate:[managedObject valueForKey:@"sesStartTime"]];
        
        endTime= [Utils formatTimeStringFromDate:[managedObject valueForKey:@"sesEndTime"]];
    }
    
    if (startTime.length == 0 && endTime.length == 0)
    {
        self.lblTime.text = @"";
    }
    else
    {
        self.lblTime.text = [NSString stringWithFormat:@"%@ - %@", startTime, endTime];
    }
}

-(void)setViewCategoryColorBackgroundColor:(UIColor*)categoryColor
{
    self.viewCategoryColor.backgroundColor = categoryColor;
}

-(void)setSessionNo:(NSString *)sessionNo
{
    NSString *trimmedSessionNo = [sessionNo stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    _lblRoomSessionNo.text = trimmedSessionNo;
    
    if (trimmedSessionNo.length)
    {
        self.lblSessionNoText.text = @"Session no:";
    }
    else
    {
        self.lblSessionNoText.text = nil;
    }
}

@end
