//
//  PresentationTableViewCell.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 12/9/16.
//  Copyright © 2016 Navus. All rights reserved.
//

#import "PresentationTableViewCell.h"

@implementation PresentationTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    // Initialization code
    
//    self.constraintImgMaterialCenterToAddToTimeline.constant = (kJoinTheDialogueExist) ? 13 : 0;
//    self.imgQaIcon.hidden = !kJoinTheDialogueExist;
}

- (IBAction)actionTimelinePressed:(UIButton *)sender
{
    sender.selected = !sender.selected;
    
//    UIAlertController *alert = [Utils showMessageAddToTimeline:sender.isSelected];
//    [self performSelector:@selector(hideAlertController:) withObject:alert afterDelay:1];//sec

    [_delegate presentationTableViewCell:self didTimelinePressed:sender];
}

-(void)hideAlertController:(UIAlertController *)alert
{
    [alert dismissViewControllerAnimated:YES completion:nil];
}

-(void)setTime:(id)managedObject
{
    NSString* startTime = nil;
    NSString* endTime = nil;
    
    if ([managedObject class] == [Talk class])
    {
        startTime = [Utils formatTimeStringFromDate: [managedObject valueForKey:@"tlkStartTime"]];
        
        endTime= [Utils formatTimeStringFromDate: [managedObject valueForKey:@"tlkEndTime"]];
    }
    else
    {
        startTime = [Utils formatTimeStringFromDate: [managedObject valueForKey:@"sesStartTime"]];
        
        endTime= [Utils formatTimeStringFromDate: [managedObject valueForKey:@"sesEndTime"]];
    }
    
    if (startTime.length == 0 && endTime.length == 0)
    {
        self.lblTime.text = @"";
    }
    else
    {
        self.lblTime.text = [NSString stringWithFormat:@"%@ - %@", startTime, endTime];
    }
}

-(void)setBoardNo:(NSString *)boardNo
{
    NSString *trimmedBoardNo = [boardNo stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    _lblBoardNo.text = trimmedBoardNo;
    
    if (trimmedBoardNo.length)
    {
        self.lblBoardNoText.text = @"Board no:";
    }
    else
    {
        self.lblBoardNoText.text = nil;
    }
}

@end
