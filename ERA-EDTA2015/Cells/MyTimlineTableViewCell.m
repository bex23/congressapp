//
//  MyTimlineTableViewCell.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 2/28/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "MyTimlineTableViewCell.h"

@implementation MyTimlineTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    // Initialization code
    
//    self.constraintImgMaterialInidcator.constant = (kJoinTheDialogueExist) ? -13 : 0;
//    self.imgQaIcon.hidden = !kJoinTheDialogueExist;
}

-(void)setTime:(id)managedObject
{
    NSString* startTime = nil;
    NSString* endTime = nil;
    
    if ([managedObject class] == [Talk class])
    {
        startTime = [Utils formatTimeStringFromDate: [managedObject valueForKey:@"tlkStartTime"]];
        
        endTime= [Utils formatTimeStringFromDate: [managedObject valueForKey:@"tlkEndTime"]];
    }
    else
    {
        startTime = [Utils formatTimeStringFromDate: [managedObject valueForKey:@"sesStartTime"]];
        
        endTime= [Utils formatTimeStringFromDate: [managedObject valueForKey:@"sesEndTime"]];
    }
    
    if (startTime.length == 0 && endTime.length == 0)
    {
        self.lblTalkTime.text = @"";
    }
    else
    {
        self.lblTalkTime.text = [NSString stringWithFormat:@"%@ - %@", startTime, endTime];
    }
}

-(void)setViewCategoryColorBackgroundColor:(UIColor*)categoryColor
{
    self.viewSessionColor.backgroundColor = categoryColor;
}

-(void)setSessionNo:(NSString *)sessionNo
{
    NSString *trimmedSessionNo = [sessionNo stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    _lblSessionNo.text = trimmedSessionNo;
    
    if (trimmedSessionNo.length)
    {
        self.lblSessionNoText.text = @"Session no:";
    }
    else
    {
        self.lblSessionNoText.text = nil;
    }
    
}

@end
