//
//  PresentationTableViewCell.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 12/9/16.
//  Copyright © 2016 Navus. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PresentationTableViewCell;

@protocol PresentationTableViewCellDelegate

-(void)presentationTableViewCell:(PresentationTableViewCell *)tableCell didTimelinePressed:(UIButton *)sender;

@end

@interface PresentationTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblSpeakerFullName;
@property (strong, nonatomic) IBOutlet UILabel *lblTalkTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblTime;

@property (strong, nonatomic) IBOutlet UIButton *btnAddToTimeline;

@property (strong, nonatomic) IBOutlet UIImageView *imgMaterialAvailabilityIcon;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintImgMaterialCenterToAddToTimeline;
@property (strong, nonatomic) IBOutlet UIImageView *imgQaIcon;

@property (strong, nonatomic) id <PresentationTableViewCellDelegate>delegate;

@property (weak, nonatomic) IBOutlet UILabel *lblBoardNoText;
@property (weak, nonatomic) IBOutlet UILabel *lblBoardNo;

-(void)setTime:(id)managedObject;
-(void)setBoardNo:(NSString *)boardNo;

@end
