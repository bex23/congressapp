//
//  PosterTableViewCell.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 2/17/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PosterTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblPosterTitle;

@property (strong, nonatomic) NSString *boardNo;
@property (weak, nonatomic) IBOutlet UILabel *lblBoardNoText;
@property (weak, nonatomic) IBOutlet UILabel *lblBoardNo;

@property (weak, nonatomic) IBOutlet UILabel *lblRoom;

@property (weak, nonatomic) IBOutlet UILabel *lblTime;
-(void)setTime:(id)managedObject;

@end
