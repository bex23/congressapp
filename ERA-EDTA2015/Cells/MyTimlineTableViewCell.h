//
//  MyTimlineTableViewCell.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 2/28/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyTimlineTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblTalkTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblSesionRoom;
@property (strong, nonatomic) IBOutlet UILabel *lblSessionNo;
@property (strong, nonatomic) IBOutlet UILabel *lblTalkTime;
@property (strong, nonatomic) IBOutlet UIImageView *imgMaterialAvailabilityIcon;
@property (strong, nonatomic) IBOutlet UIImageView *imgQaIcon;
@property (strong, nonatomic) IBOutlet UIView *viewSessionColor;
@property (strong, nonatomic) UIColor *sessionColor;
@property (weak, nonatomic) IBOutlet UILabel *lblSessionNoText;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintImgMaterialInidcator;

-(void)setTime:(id)managedObject;
-(void)setViewCategoryColorBackgroundColor:(UIColor*)categoryColor;
-(void)setSessionNo:(NSString *)sessionNo;

@end
