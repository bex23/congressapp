//
//  IAAppDelegate.h
//  ERA-EDTA2015
//
//  Created by admin on 6/24/14.
//  Copyright (c) 2014 Navus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
// Maja: landscapeON - use for ProgrammeAtAGlance
@property (nonatomic) BOOL landscapeON;

@end
