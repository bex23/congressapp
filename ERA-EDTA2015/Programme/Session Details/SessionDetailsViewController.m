//
//  SessionDetailsViewController.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 12/5/16.
//  Copyright © 2016 Navus. All rights reserved.
//

#import "SessionDetailsViewController.h"

#import "PresentationTableViewCell.h"
#import "PresentationDetailsViewController.h"
#import "AppDelegate.h"
#import "IACongressCenterMapViewController.h"
#import "ExhibitorMapViewController.h"
#import "SurveyViewController.h"

@interface SessionDetailsViewController () <PresentationTableViewCellDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblTypeOfPresentations;
@property (weak, nonatomic) IBOutlet UILabel *exhibitorRoleLabel;

@property (weak, nonatomic) IBOutlet UIView *tableViewHeader;
@property (weak, nonatomic) IBOutlet UIView *viewSessionDetails;

@property (assign, nonatomic) BOOL isFirstTimeLaod;

@property (weak, nonatomic) IBOutlet UIImageView *imgSponsorLogo;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintSponsorLogoHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintBtnSurveyHeight;

@property (weak, nonatomic) IBOutlet UIButton *btnSurvey;

@property (nonatomic, retain) WYPopoverController *selectPopoverController;

@property (weak, nonatomic) IBOutlet UILabel *lblSessionCategoryText;

@property (weak, nonatomic) IBOutlet UIImageView *imgPin;

@property (weak, nonatomic) IBOutlet UIImageView *imgTopBanner;

@end

@implementation SessionDetailsViewController

#pragma mark - View

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.exhibitorRoleLabel.hidden = YES;
    

    
    // register presentation cell
    UINib *nib = [UINib nibWithNibName:NSStringFromClass([PresentationTableViewCell class]) bundle:nil];
    [[self tableView] registerNib:nib forCellReuseIdentifier:NSStringFromClass([PresentationTableViewCell class])];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 300.0;
    
    // add shadow to view
    [self.viewSessionDetails.layer setShadowColor:[UIColor lightGrayColor].CGColor];
    [self.viewSessionDetails.layer setShadowOpacity:0.8];
    [self.viewSessionDetails.layer setShadowRadius:3.0];
    [self.viewSessionDetails.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [self loadData];
    
    self.isFirstTimeLaod = YES;
    
    [self.imgPin colorItWithColor:[UIColor colorWithHex:kGlobalColor
                                               andAlpha:1.0]];
    [self.exhibitorRoleLabel setTextColor:[UIColor colorWithHex:kGlobalColor
                                                             andAlpha:1.0]];

    [self setupBanners];
}

-(void)setupBanners
{
    [[BannersServices shared] addWithImageView:self.imgTopBanner
                                            at:BannerPositionTopBanner
                                   sender: self];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = NO;
    
    self.view.userInteractionEnabled = YES;
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.landscapeON = NO;
    
    UIApplication* application = [UIApplication sharedApplication];
    if (application.statusBarOrientation != UIInterfaceOrientationPortrait)
    {
        UIViewController *c = [[UIViewController alloc]init];
        [c.view setBackgroundColor:[UIColor clearColor]];
        [self.navigationController presentViewController:c animated:NO completion:^{
            [self.navigationController dismissViewControllerAnimated:YES completion:^{
            }];
        }];
    }
    
    if (_isFirstTimeLaod)
    {
        [self setupTableViewHeight];
        self.isFirstTimeLaod = NO;
    }
    
    [self.tableView reloadData];
}

-(void)setupTableViewHeight
{
    [self.tableViewHeader setNeedsLayout];
    [self.tableViewHeader layoutIfNeeded];
    
    [self.viewSessionDetails setNeedsLayout];
    [self.viewSessionDetails layoutIfNeeded];
    
    CGRect newFrame = self.tableViewHeader.frame;
    CGFloat tableViewHeaderHeight = newFrame.size.height;
    
    // 47 - left and right space from labels to basic view
    CGFloat widthForLabels = self.view.frame.size.width - 47;
    
    tableViewHeaderHeight += [_lblSessionName heightForWidth:widthForLabels] + [_lblRoomName heightForWidth:widthForLabels];
    
    if (_session.sesChairPerson.length > 0)
    {
        tableViewHeaderHeight += [_lblChairs heightForWidth:widthForLabels] + [_lblChairheading heightForWidth:widthForLabels];
    }
    
    if ([SessionCategory getSessionCategoryWithId:_session.sesCategoryId].secName.length && ![[SessionCategory getSessionCategoryWithId:_session.sesCategoryId].secName containsString:@"No category"])
    {
        tableViewHeaderHeight += [_lblSessionCategory heightForWidth:widthForLabels];
    }
    
    _constraintSponsorLogoHeight.constant = 0;
    
    // CHANGE IT AFTER IMPEMENTATION !!!!!!!!!!!!!!!
    
    _btnSurvey.hidden = YES;
    _constraintBtnSurveyHeight.constant = 0;
    // 50 - height of SURVEY buttons
    tableViewHeaderHeight -= 50;
    
    // show hide SURVEY buttons, preview logo
    if ([_session.sesType isEqual: kKeyIndustrialSymposium])
    {
        if (_session.sponsors.count > 0)
        {
            NSString *exhibitorImage = @"";
            
            for (ExhibitorRole *exhRole in _session.sponsors.allObjects)
            {
                if(exhRole.exhibitor.exhImage.length > 0)
                {
                    self.exhibitorRoleLabel.hidden = NO;
                    exhibitorImage = exhRole.exhibitor.exhImage;
                    break;
                }
            }
            
            if(exhibitorImage.length > 0)
            {
                NSString* request = [exhibitorImage stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                NSURL* url = [NSURL URLWithString:request];
                UIImage* img = [UIImage imageNamed:@"company_placeholder"];
                [self.imgSponsorLogo setImageWithURL:url  placeholderImage:img];
                _constraintSponsorLogoHeight.constant = 75;
            }
        }
        
        // show/hide survey button
        _btnSurvey.hidden = !(_session.sesSurvey.integerValue == 1);
        
        if (!_btnSurvey.hidden)
        {
            _constraintBtnSurveyHeight.constant = 50;
            tableViewHeaderHeight += 50; // add top and bottom constrains
        }
    }
    
    // add height of sponsor logo
    tableViewHeaderHeight += _constraintSponsorLogoHeight.constant;
    
    // set table header view height
    newFrame.size.height = tableViewHeaderHeight;
    // set table header view frame
    self.tableViewHeader.frame = newFrame;
    
    [self.tableView setTableHeaderView:self.tableViewHeader];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (void)loadData
{
    if (_session.sesSubtitle.length > 0)
    {
        self.lblSessionName.text = [NSString stringWithFormat:@"%@\n\n%@", _session.sesName, _session.sesSubtitle];
    }
    else
    {
        self.lblSessionName.text = _session.sesName;
    }
    
    [self.lblSessionName sizeToFit];
    
    self.lblDate.text = [NSString stringWithFormat:@"%@,", [Utils formatDateString:[Utils getDate:_session.sesStartTime]]];
    
    self.lblTime.text = [NSString stringWithFormat:@"%@ - %@",[Utils formatTimeStringFromDate:_session.sesStartTime], [Utils formatTimeStringFromDate:_session.sesEndTime]];
    
    if (![SessionCategory getSessionCategoryWithId:_session.sesCategoryId].secName.length || [[SessionCategory getSessionCategoryWithId:_session.sesCategoryId].secName containsString:@"No category"])
    {
        self.lblSessionCategoryText.text = self.lblSessionCategory.text = @"";
    }
    else
    {
        self.lblSessionCategoryText.text = @"Session category:";
        self.lblSessionCategory.text = [SessionCategory getSessionCategoryWithId:_session.sesCategoryId].secName;
    }
    
    [self.lblSessionCategory sizeToFit];
    [self.lblSessionCategoryText sizeToFit];
    
    //    [self.lblSessionCategory sizeToFit];
    self.lblCategoryColor.backgroundColor = [SessionCategory getColorForSessionCategoryId:_session.sesCategoryId];
    
    if (_session.sesChairPerson.length)
    {
        self.lblChairheading.text = @"Chair:";
        self.lblChairs.text = _session.sesChairPerson;
    }
    else
    {
        self.lblChairheading.text = @"";
    }
    
    [self.lblChairheading sizeToFit];
    
    self.lblRoomName.text = _session.venue.venName;
    [self.lblRoomName sizeToFit];
    
    if ([self.session.sesType isEqualToString:kKeyPoster])
    {
        self.title = @"Poster Session Details";
        self.talksArray = [_session.talks.allObjects sortedArrayUsingDescriptors:@[
                                                                                   [NSSortDescriptor sortDescriptorWithKey:@"tlkStartTime" ascending:YES],
                                                                                   [[NSSortDescriptor alloc] initWithKey:@"tlkBoardNo" ascending:YES],
                                                                                   [[NSSortDescriptor alloc] initWithKey:@"tlkTitle" ascending:YES]
                                                                                   ]
                           ];
    }
    else
    {
        self.title = @"Session Details";
        self.talksArray = [_session.talks.allObjects sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"tlkStartTime" ascending:YES],
                                                                                   [[NSSortDescriptor alloc] initWithKey:@"tlkTitle" ascending:YES]]];
    }
    
    self.lblTypeOfPresentations.text = [NSString stringWithFormat:@" %@ PRESENTATIONS ", [self.session.sesType uppercaseString]];
    self.lblTypeOfPresentations.hidden = (_talksArray.count == 0);
    
    [self.tableView reloadData];
}

#pragma mark - TableView Data Source

- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section
{
    return self.talksArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PresentationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PresentationTableViewCell class]) forIndexPath:indexPath];
    cell.delegate = self;
    
    Talk *talk = [self.talksArray objectAtIndex:indexPath.row];
    
    cell.lblTalkTitle.text = talk.tlkTitle;
    cell.lblSpeakerFullName.text = talk.tlkSpeakerName;
    [cell setTime:talk];
    
    if (talk.tlkAvailable.integerValue == 1)
    {
        cell.imgMaterialAvailabilityIcon.image = [UIImage imageNamed:kEmaterialsAvailable];
        [cell.imgMaterialAvailabilityIcon colorItWithColor:[UIColor colorWithHex:kGlobalColor andAlpha:1.0]];
    }
    else
    {
        cell.imgMaterialAvailabilityIcon.image = [UIImage imageNamed:kEmaterialsUnavailable];
        [cell.imgMaterialAvailabilityIcon colorItWithColor:[UIColor lightGrayColor]];
    }
    
    cell.imgQaIcon.image = [UIImage imageNamed:[talk.tlkVotingEnabled boolValue] ? kVotingAvailable : kVotingUnavailable];
    
    if ([talk.tlkType isEqualToString:kKeyPoster])
    {
        cell.imgMaterialAvailabilityIcon.hidden = YES;
        cell.imgQaIcon.hidden = YES;
        [cell.btnAddToTimeline setImage:[UIImage imageNamed:@"list_poster_tag"]
                               forState:UIControlStateNormal];
        [cell.btnAddToTimeline setImage:[UIImage imageNamed:@"list_poster_tagged"]
                               forState:UIControlStateSelected];
        [cell setBoardNo:talk.tlkBoardNo];
    }
    else
    {
        cell.imgMaterialAvailabilityIcon.hidden = NO;
        cell.imgQaIcon.hidden = NO;
        [cell setBoardNo:nil];
    }
    
    if (talk.zetBookmarked.boolValue == YES)
    {
        cell.btnAddToTimeline.selected = YES;
    }
    else
    {
        cell.btnAddToTimeline.selected = NO;
    }
    
    cell.btnAddToTimeline.tag = indexPath.row;
    
    // because on iOS 8 doesn't work
    // self.tableView.rowHeight = UITableViewAutomaticDimension;
    // self.tableView.estimatedRowHeight = 200.0;
    [cell updateConstraintsIfNeeded];
    
    return cell;
}

// because on iOS 8 doesn't work
// self.tableView.rowHeight = UITableViewAutomaticDimension;
// self.tableView.estimatedRowHeight = 200.0;
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

#pragma mark - TableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSString *strID = NSStringFromClass([PresentationDetailsViewController class]);
    PresentationDetailsViewController *vc = [[UIStoryboard storyboardWithName:strID bundle:nil] instantiateViewControllerWithIdentifier:strID];
    
    vc.currentTalkIndex = (int)indexPath.row;
    vc.arrayTalks = [NSMutableArray arrayWithArray:_talksArray];
    
    self.view.userInteractionEnabled = NO;
    
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - PresentationTableViewCellDelegate

- (void)presentationTableViewCell:(PresentationTableViewCell *)tableCell didTimelinePressed:(UIButton *)sender
{
    Talk* talk = [self.talksArray objectAtIndex:sender.tag];
    
    if (talk.zetBookmarked.boolValue == YES)
    {
        [Talk updateTimeline:talk isAddition:NO];
    }
    else
    {
        [Talk updateTimeline:talk isAddition:YES];
    }
    
    NSError *error;
    
    [[LLDataAccessLayer sharedInstance].managedObjectContext save:&error];
}

-(void)hideAlertController:(UIAlertController *)alert
{
    [alert dismissViewControllerAnimated:YES completion:nil];
}



#pragma mark - IBActions

- (IBAction)actionOpenRoom:(id)sender
{
    if (kEnableCongressMapPinning == NO)
    {
        return;
    }
    
    if (self.session.venue && self.session.venue.venCoordX.doubleValue != 0.0 && self.session.venue.venCoordY.doubleValue != 0.0)
    {
        IACongressCenterMapViewController* vc = [[UIStoryboard storyboardWithName:@"CongressCenterMap" bundle:nil] instantiateViewControllerWithIdentifier:@"CongressCenterMap"];
        vc.roomLevel = [self.session.venue.venFloor stringValue];
        vc.strCoordX = self.session.venue.venCoordX;
        vc.strCoordY = self.session.venue.venCoordY;
        
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (IBAction)actionSurvey:(id)sender
{
    Survey *survey = [Survey getSurveyWithId:_session.sesSessionId];
    
    if (survey)
    {
        NSString *strID = NSStringFromClass([SurveyViewController class]);
        SurveyViewController *vc = [[UIStoryboard storyboardWithName:strID bundle:nil] instantiateViewControllerWithIdentifier:strID];
        [vc setSurvey:survey];
        
        [self.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark - IndustrySymposiaImageDelegate

-(void)industrySymposiaImageCancel
{
    [self.selectPopoverController dismissPopoverAnimated:YES];
}

#pragma mark - Set left margin of table cells to ziro for iOS 8

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

@end
