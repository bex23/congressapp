//
//  SurveyViewController.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 3/18/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SurveyViewController : UIViewController

@property (strong, nonatomic) Survey *survey;

// answares property

@end
