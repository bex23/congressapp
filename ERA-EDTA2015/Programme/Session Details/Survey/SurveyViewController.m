//
//  SurveyViewController.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 3/18/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "SurveyViewController.h"

#import "RadioTableViewCell.h"
#import "TextAreaTableViewCell.h"
#import "SectionHeaderTableViewCell.h"

@interface SurveyViewController () <UITableViewDelegate, UITableViewDataSource, TextAreaTableViewCellDelegate, RadioTableViewCellDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblInfo;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSMutableDictionary *dictTempAnswers;

@property (strong, nonatomic) NSMutableArray *arraySurveyQuestionsGrouped;

@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;

@property (strong, nonatomic) NSArray *arraySurveysAnswers;

@property (assign, nonatomic) BOOL isSubmited;

@end

@implementation SurveyViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _dictTempAnswers = [NSMutableDictionary dictionaryWithCapacity:10];
    _arraySurveyQuestionsGrouped = [NSMutableArray arrayWithCapacity:10];
    
    _isSubmited = NO;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"SUBMIT" style:UIBarButtonItemStylePlain target:self action:@selector(submit)];
    
    // register cells
    UINib *nibR = [UINib nibWithNibName:NSStringFromClass([RadioTableViewCell class]) bundle:nil];
    [[self tableView] registerNib:nibR forCellReuseIdentifier:NSStringFromClass([RadioTableViewCell class])];
    
    UINib *nibTV = [UINib nibWithNibName:NSStringFromClass([TextAreaTableViewCell class]) bundle:nil];
    [[self tableView] registerNib:nibTV forCellReuseIdentifier:NSStringFromClass([TextAreaTableViewCell class])];
    
    UINib *nibS = [UINib nibWithNibName:NSStringFromClass([SectionHeaderTableViewCell class]) bundle:nil];
    [[self tableView] registerNib:nibS forCellReuseIdentifier:NSStringFromClass([SectionHeaderTableViewCell class])];
    // ens register cells
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 300.0;
    
    self.tableView.sectionHeaderHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedSectionHeaderHeight = 50;
    
    _lblInfo.text = _survey.survDescription;
    
    NSArray *arraySurveyQuestions = [_survey.questions.allObjects sortedArrayUsingDescriptors:@[[[NSSortDescriptor alloc] initWithKey:@"suqSurveyQuestionId" ascending:YES]]];
    
    NSString *currentGroup = @"";
    NSMutableArray *arrayGroupsQuestons;
    int index = -1;
    for (SurveyQuestion *sq in  arraySurveyQuestions)
    {
        if (![currentGroup isEqualToString:sq.suqGroup] || sq.suqGroup.length == 0)
        {
            index++;
            currentGroup = sq.suqGroup;
            
            arrayGroupsQuestons = [[NSMutableArray alloc] initWithObjects:sq, nil];
            [_arraySurveyQuestionsGrouped addObject:arrayGroupsQuestons];
        }
        else
        {
            [arrayGroupsQuestons addObject:sq];
            [_arraySurveyQuestionsGrouped replaceObjectAtIndex:index withObject:arrayGroupsQuestons];
        }
    }
    
    self.arraySurveysAnswers = [SurveyAnswers getSurveyAnswersForSurveyId:@(_survey.survSurveyId.integerValue)];
    
    // survey already rated
    if (_arraySurveysAnswers.count > 0)
    {
        _isSubmited = YES;

        for (SurveyAnswers *sa in _arraySurveysAnswers)
        {
            [_dictTempAnswers setObject:sa.suaContent forKey:@(sa.suaQuestionId.integerValue)];
        }
    }
}



-(void)submit
{
    NSLog(@"submited");
    
    [self.view endEditing:YES];
    
    if (!_dictTempAnswers.count)
    {
        [Utils showMessage:_survey.survDescription];
        return;
    }
    
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@""
                                message:@"Are you sure you want to submit your ratings? You can only submit once and you can't edit your data anymore."
                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* btnRegister = [UIAlertAction actionWithTitle:@"OK"
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action) {
                                                            [self finishSubmit];
                                                        }];
    
    UIAlertAction* btnCancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
    
    [alert addAction:btnCancel];
    [alert addAction:btnRegister];
    
    [self.navigationController presentViewController:alert animated:YES completion:nil];
}

-(void)finishSubmit
{
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    UIBarButtonItem * barButton = [[UIBarButtonItem alloc] initWithCustomView:self.activityIndicator];
    [self navigationItem].rightBarButtonItem = barButton;
    [self.activityIndicator startAnimating];
    
    self.view.userInteractionEnabled = NO;
    
    [SurveyAnswers insertSurveyAnswers:_dictTempAnswers forSurveyId:@(_survey.survSurveyId.integerValue) withCompletitionBlock:^{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.activityIndicator stopAnimating];
            
            self.navigationItem.rightBarButtonItem = nil;
            
            self.view.userInteractionEnabled = YES;
            
//            [[ESyncData sharedInstance] syncSurveyAnswers];
        
            [self showSubmitedMessage];
        });
    }];
}

-(void)showSubmitedMessage
{
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@""
                                message:@"Thank you for your time and feedback."
                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* btnOK = [UIAlertAction actionWithTitle:@"OK"
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action) {
                                                            [self.navigationController popViewControllerAnimated:YES];
                                                        }];

    [alert addAction:btnOK];
    
    [self.navigationController presentViewController:alert animated:YES completion:nil];
}

#pragma mark - TableView Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _arraySurveyQuestionsGrouped.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *qusetions = [_arraySurveyQuestionsGrouped objectAtIndex:section];
    
    return qusetions.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RadioTableViewCell *cell = nil;
    
    SurveyQuestion *surveyQuestion = [[_arraySurveyQuestionsGrouped objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    
    if ([surveyQuestion.suqType isEqualToString:@"radio"])
    {
        cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([RadioTableViewCell class]) forIndexPath:indexPath];
        
        cell.lblTitle.text = surveyQuestion.suqTitle;
        cell.tag = surveyQuestion.suqSurveyQuestionId.integerValue;
        
        cell.delegate = self;
        
        if ([_dictTempAnswers objectForKey:@(surveyQuestion.suqSurveyQuestionId.integerValue)])
        {
            [cell selectNumber:[_dictTempAnswers objectForKey:@(surveyQuestion.suqSurveyQuestionId.integerValue)]];
        }
        else
        {
            [cell deselectAllNumbers];
        }
    }
    else if ([surveyQuestion.suqType isEqualToString:@"textarea"])
    {
        cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([TextAreaTableViewCell class]) forIndexPath:indexPath];
        ((TextAreaTableViewCell *)cell).lblTitle.text = surveyQuestion.suqTitle;
        cell.tag = surveyQuestion.suqSurveyQuestionId.integerValue;
        
//        NSLog(@"id %d", [surveyQuestion.suqSurveyQuestionId integerValue]);
        
        cell.delegate = self;
        
        if ([_dictTempAnswers objectForKey:@(surveyQuestion.suqSurveyQuestionId.integerValue)])
        {
            ((TextAreaTableViewCell *)cell).textView.text = [_dictTempAnswers objectForKey:@(surveyQuestion.suqSurveyQuestionId.integerValue)];
        }
        else
        {
            ((TextAreaTableViewCell *)cell).textView.text = @"";
        }
    }
    
    if (_isSubmited == YES)
    {
        cell.userInteractionEnabled = NO;
    }
    
    [cell updateConstraintsIfNeeded];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 1;
}

#pragma mark - TableView Delegate

- (void)tableView:(UITableView *)tableView willDisplayFooterView:(UIView *)view forSection:(NSInteger)section {
    
    //Set the background color of the View
    view.tintColor = [UIColor colorWithHex:kGlobalColor andAlpha:1.0];
    
    //Set the TextLabel Color
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setTextColor:[UIColor colorWithHex:kGlobalColor andAlpha:1.0]];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    SectionHeaderTableViewCell *view = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([SectionHeaderTableViewCell class])];
    
    SurveyQuestion *sq = [[_arraySurveyQuestionsGrouped objectAtIndex:section] firstObject];
    
    view.lblTitle.text = sq.suqGroup;
    
    return view;
}

#pragma mark - TextAreaTableViewCellDelegate

-(void)textArealTableViewCell:(TextAreaTableViewCell *)tableCell textViewDidEndEditing:(UITextView *)textView
{
    if (textView.text.length > 0)
    {
        [_dictTempAnswers setObject:textView.text forKey: @(tableCell.tag)];
    }
    else
    {
        [_dictTempAnswers removeObjectForKey:@(tableCell.tag)];
    }
}

#pragma mark - RadioTableViewCellDelegate

-(void)radioTableViewCell:(RadioTableViewCell *)tableCell didRatePressed:(UIButton *)button
{
    if (button.isSelected)
    {
        [_dictTempAnswers setObject:@(button.tag) forKey:@(tableCell.tag)];
    }
    else
    {
        [_dictTempAnswers removeObjectForKey:@(tableCell.tag)];
    }
}

@end
