//
//  TextAreaTableViewCell.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 3/18/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TextAreaTableViewCell;

@protocol TextAreaTableViewCellDelegate <NSObject>

-(void)textArealTableViewCell:(TextAreaTableViewCell *)tableCell textViewDidEndEditing:(UITextView *)textView;

@end

@interface TextAreaTableViewCell : UITableViewCell <UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property (nonatomic, assign) id <TextAreaTableViewCellDelegate> delegate;


@end
