//
//  TextAreaTableViewCell.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 3/18/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "TextAreaTableViewCell.h"

@implementation TextAreaTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    // Initialization code
    
    self.textView.layer.borderWidth = 1;
    self.textView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    self.textView.delegate = self;
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    [self.delegate textArealTableViewCell:self textViewDidEndEditing:textView];
}

@end
