//
//  RadioTableViewCell.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 3/18/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RadioTableViewCell;

@protocol RadioTableViewCellDelegate <NSObject>

-(void)radioTableViewCell:(RadioTableViewCell *)tableCell didRatePressed:(UIButton *)button;

@end


@interface RadioTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property (nonatomic, assign) id <RadioTableViewCellDelegate> delegate;

-(void)selectNumber:(NSNumber *)number;
-(void)deselectAllNumbers;

@end
