//
//  RadioTableViewCell.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 3/18/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "RadioTableViewCell.h"

@implementation RadioTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    // Initialization code
}

-(void)selectNumber:(NSNumber *)number
{
    [self deselectAllNumbers];
    
    for (UIView *button in self.contentView.subviews)
    {
        if ([button isKindOfClass:[UIButton class]] && ((UIButton *)button).tag == number.integerValue)
        {
            [(UIButton *)button setSelected:YES];
        }
    }
}

-(void)deselectAllNumbers
{
    for (UIView *button in self.contentView.subviews)
    {
        if ([button isKindOfClass:[UIButton class]])
        {
            [(UIButton *)button setSelected:NO];
        }
    }
}

- (IBAction)actionRate:(UIButton *)sender
{
    if ([sender isSelected])
    {
        [sender setSelected:NO];
    }
    else
    {
        // Unselect all the buttons in the parent view
        for (UIView *button in sender.superview.subviews)
        {
            if ([button isKindOfClass:[UIButton class]]) {
                [(UIButton *)button setSelected:NO];
            }
        }
        
        [sender setSelected:YES];
    }
    
    [self.delegate radioTableViewCell:self didRatePressed:sender];
}
@end
