//
//  SessionDetailsViewController.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 12/5/16.
//  Copyright © 2016 Navus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SessionDetailsViewController : UIViewController <UITableViewDataSource, UITableViewDataSource>

@property (nonatomic, strong) Session *session;

@property (retain, nonatomic) IBOutlet UILabel *lblCategoryColor;

@property (retain, nonatomic) IBOutlet UILabel *lblSessionName;

@property (retain, nonatomic) IBOutlet UILabel *lblSessionCategory;

@property (strong, nonatomic) IBOutlet UILabel *lblChairheading;
@property (retain, nonatomic) IBOutlet UILabel *lblChairs;

@property (retain, nonatomic) IBOutlet UILabel *lblDate;
@property (retain, nonatomic) IBOutlet UILabel *lblTime;
@property (retain, nonatomic) IBOutlet UILabel *lblRoomName;

@property (retain, nonatomic) IBOutlet UITableView *tableView;

@property (retain, nonatomic) NSArray *talksArray;

@end
