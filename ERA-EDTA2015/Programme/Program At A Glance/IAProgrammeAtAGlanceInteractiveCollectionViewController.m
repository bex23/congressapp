//
//  IAProgrammeAtAGlanceInteractiveCollectionViewController.m
//  ERA-EDTA2015
//
//  Created by admin on 2/19/15.
//  Copyright (c) 2015 Navus. All rights reserved.
//

#import "IAProgrammeAtAGlanceInteractiveCollectionViewController.h"

#import "INSElectronicProgramGuideLayout.h"
#import "ISHourHeader.h"
#import "ISEPGCell.h"
#import "ISSectionHeader.h"
#import "ISCurrentTimeIndicatorView.h"
#import "ISGridlineView.h"
#import "ISHeaderBackgroundView.h"
#import "ISCurrentTimeGridlineView.h"
#import "ISHalfHourLineView.h"
#import "ISFloatingCellOverlay.h"
#import "ISHourHeaderBackgroundView.h"
#import "SessionDetailsViewController.h"
#import "AppDelegate.h"


@interface IAProgrammeAtAGlanceInteractiveCollectionViewController ()<INSElectronicProgramGuideLayoutDataSource, INSElectronicProgramGuideLayoutDelegate, NSFetchedResultsControllerDelegate>
@property (nonatomic, weak) INSElectronicProgramGuideLayout *collectionViewEPGLayout;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@property (nonatomic) NSArray *days;
@property (nonatomic) int selectedDay;
@property (nonatomic,strong)NSMutableArray* filtered;
@property (nonatomic, strong)Session* selectedSession;

//@property (nonatomic, retain)WYPopoverController* selectPopoverController;
@property (nonatomic, strong)UIView* popoverAnchorView;

@end

@implementation IAProgrammeAtAGlanceInteractiveCollectionViewController

static NSString * const reuseIdentifier = @"Cell";

- (INSElectronicProgramGuideLayout *)collectionViewEPGLayout
{
    return (INSElectronicProgramGuideLayout *)self.collectionViewLayout;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIBarButtonItem *bbi = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"filter"] style:UIBarButtonItemStylePlain target:self action:@selector(showDays:)];
    self.navigationItem.rightBarButtonItem = bbi;
    
    //    UIImageView *backgroundImage = [[UIImageView alloc] initWithFrame:self.view.bounds];
    //    backgroundImage.image = [UIImage imageNamed:@"backgroundImage"];
    //    self.collectionView.backgroundView = backgroundImage;
    
    //    self.fetchedResultsController = [Entry MR_fetchAllGroupedBy:@"channel.iD" withPredicate:nil sortedBy:@"channel.iD,channel.name" ascending:YES delegate:self];
    [self doFetch];
    
    [self selectDay];
    self.view.backgroundColor = [UIColor whiteColor];
    // Do any additional setup after loading the view, typically from a nib.
    self.collectionViewEPGLayout.dataSource = self;
    self.collectionViewEPGLayout.delegate = self;
    
    self.collectionViewEPGLayout.shouldResizeStickyHeaders = NO;
    self.collectionViewEPGLayout.shouldUseFloatingItemOverlay = YES;
    self.collectionViewEPGLayout.floatingItemOffsetFromSection = 10.0;
    self.collectionViewEPGLayout.currentTimeVerticalGridlineWidth = 4;
    self.collectionViewEPGLayout.sectionHeight = 70;
    self.collectionViewEPGLayout.sectionHeaderWidth = 120;
    self.collectionViewEPGLayout.hourHeaderHeight = 30;
    self.collectionViewEPGLayout.hourWidth = 300;
    
    [self.collectionView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    //    self.collectionView.contentInset = UIEdgeInsetsMake(0, -1500, 0, -1500);
    
    NSString *timeRowHeaderStringClass = NSStringFromClass([ISHourHeader class]);
    [self.collectionView registerNib:[UINib nibWithNibName:timeRowHeaderStringClass bundle:nil] forSupplementaryViewOfKind:INSEPGLayoutElementKindHourHeader withReuseIdentifier:timeRowHeaderStringClass];
    [self.collectionView registerNib:[UINib nibWithNibName:timeRowHeaderStringClass bundle:nil] forSupplementaryViewOfKind:INSEPGLayoutElementKindHalfHourHeader withReuseIdentifier:timeRowHeaderStringClass];
    
    NSString *cellStringClass = NSStringFromClass([ISEPGCell class]);
    [self.collectionView registerNib:[UINib nibWithNibName:cellStringClass bundle:nil] forCellWithReuseIdentifier:cellStringClass];
    
    NSString *dayColumnHeaderStringClass = NSStringFromClass([ISSectionHeader class]);
    [self.collectionView registerNib:[UINib nibWithNibName:dayColumnHeaderStringClass bundle:nil] forSupplementaryViewOfKind:INSEPGLayoutElementKindSectionHeader withReuseIdentifier:dayColumnHeaderStringClass];
    
    // Required when self.collectionViewEPGLayout.shouldUseFloatingItemOverlay set to YES;
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([ISFloatingCellOverlay class]) bundle:nil] forSupplementaryViewOfKind:INSEPGLayoutElementKindFloatingItemOverlay withReuseIdentifier:NSStringFromClass([ISFloatingCellOverlay class])];
    
    // Optional
    [self.collectionViewEPGLayout registerClass:ISCurrentTimeGridlineView.class forDecorationViewOfKind:INSEPGLayoutElementKindCurrentTimeIndicatorVerticalGridline];
    [self.collectionViewEPGLayout registerClass:ISGridlineView.class forDecorationViewOfKind:INSEPGLayoutElementKindVerticalGridline];
    [self.collectionViewEPGLayout registerClass:ISHalfHourLineView.class forDecorationViewOfKind:INSEPGLayoutElementKindHalfHourVerticalGridline];
    //
    [self.collectionViewEPGLayout registerClass:ISHeaderBackgroundView.class forDecorationViewOfKind:INSEPGLayoutElementKindSectionHeaderBackground];
    [self.collectionViewEPGLayout registerClass:ISHourHeaderBackgroundView.class forDecorationViewOfKind:INSEPGLayoutElementKindHourHeaderBackground];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.collectionViewEPGLayout scrollToCurrentTimeAnimated:YES];
    });
    
    
    //----GENERATED CODE--------------------------------------------------------------------------
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Register cell classes
    //    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:reuseIdentifier];
    
    // Do any additional setup after loading the view.
}

- (void)showDays:(id)sender
{
    [self.days sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];

    UIAlertControllerStyle style = [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone ? UIAlertControllerStyleActionSheet : UIAlertControllerStyleAlert;
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:nil
                                message:nil
                                preferredStyle:style];
    
    for (int i =0 ; i< self.days.count; i ++)
    {
        UIAlertAction* btnLevel = [UIAlertAction
                                   actionWithTitle:[Utils formatDateString:[self.days objectAtIndex:i]]
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       self.selectedDay = i;
                                       [self doFetch];
                                       
                                       [self controllerDidChangeContent:self.fetchedResultsController];
                                       
                                       self.title = [Utils formatDateString:[self.days objectAtIndex:i]];
                                   }];
        
        [alert addAction:btnLevel];
    }
    
    if (alert.popoverPresentationController)
    {
        alert.popoverPresentationController.sourceView = self.view;
        alert.popoverPresentationController.sourceRect = self.view.bounds;
        alert.popoverPresentationController.permittedArrowDirections = 0;
        alert.popoverPresentationController.backgroundColor = [UIColor whiteColor];
    }
    else
    {
        UIAlertAction* btnCancel = [UIAlertAction
                                    actionWithTitle:@"Cancel"
                                    style:UIAlertActionStyleDefault
                                    handler:nil];
        [alert addAction:btnCancel];
    }
    
    [self presentViewController:alert animated:YES completion:nil];
    
    //    if([sender class] == [UIButton class])
    //    {
    //
    //
    //        PSSinglePickerViewController* spVC = [[PSSinglePickerViewController alloc] initWithItems:self.days DisplayMember:nil]  ;
    //        spVC.delegate = self;
    //        spVC.preferredContentSize = CGSizeMake(310,153);
    //        spVC.popovertag = @"changeDayPopover";
    //
    //        self.selectPopoverController = [[WYPopoverController alloc] initWithContentViewController:spVC]  ;
    //        self.selectPopoverController.delegate = self;
    //
    //
    //        [self.selectPopoverController presentPopoverFromRect:((UIView*)sender).bounds inView:((UIView*)sender) permittedArrowDirections:WYPopoverArrowDirectionAny animated:YES];
    //    }
}

//- (void)popoverController:(WYPopoverController *)popoverController willRepositionPopoverToRect:(inout CGRect *)rect inView:(inout UIView *__autoreleasing *)view
//{
//
//}
//- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
//{
//    [UIView animateWithDuration:duration animations:^{
//        CGRect frame = self.selectPopoverController.contentViewController.view.frame;
//        frame.origin.y = (UIInterfaceOrientationIsPortrait(toInterfaceOrientation) ? self.selectPopoverController.contentViewController.view.frame.origin.y : frame.origin.y - frame.size.height * 1.25f);
//        self.selectPopoverController.contentViewController.view.frame = frame;
//    }];
//}
//- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
//{
//    //    [self.selectPopoverController presentPopoverFromRect:self.popoverAnchorView.bounds inView:self.view permittedArrowDirections:WYPopoverArrowDirectionAny animated:YES];
//}
- (void)selectDay
{
    NSMutableArray *array = [NSMutableArray array];
    NSArray* sessionsArray = [self.fetchedResultsController.fetchedObjects sortedArrayUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"sesStartTime" ascending:YES]]];
    
    for (Session *session in sessionsArray)
    {
        if (array.count == 0)
        {
            [array addObject:[Utils getDate:session.sesStartTime]];
            continue;
        }
        if (![array containsObject:[Utils getDate:session.sesStartTime]])
        {
            [array addObject:[Utils getDate:session.sesStartTime]];
        }
    }
    
    self.days = [NSArray arrayWithArray:array];
    
    BOOL hasToday = NO;
    if (self.days.count > 0)
    {
        int i;
        for(i = 0; i < self.days.count; i++)
        {
            if([Utils isStringDateToday:[self.days objectAtIndex:i]])
            {
                hasToday = YES;
                break;
            }
        }
        
        if(hasToday)
        {
            self.selectedDay = i;
            
            self.title = [Utils formatDateString:[self.days objectAtIndex:i]];
        }
        else
        {
            self.selectedDay = 0;
            self.title = [Utils formatDateString:[self.days objectAtIndex:0]] ;
        }
        
        if(self.days.count == 1 || self.days.count - 1 == self.selectedDay)
        {
            //            self.btnNextDay.hidden = YES;
        }
        else
        {
            //            self.btnNextDay.hidden = NO;
        }
        
        if(self.selectedDay == 0)
        {
            //            self.btnPerviousDay.hidden = YES;
        }
        else
        {
            //            self.btnPerviousDay.hidden = NO;
        }
        
        [self doFetch];
    }
}

-(void)doFetch
{
    NSLog(@"Fetching");
    self.fetchedResultsController = nil;
    
    NSError *error;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"error %@ %@", error, error.userInfo);
        abort();
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    ((AppDelegate *)[[UIApplication sharedApplication] delegate]).landscapeON = YES;
        
    self.view.userInteractionEnabled = YES;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.landscapeON = NO;
    
    [self supportedInterfaceOrientations];
    //
    [self shouldAutorotate];
    
    //    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait animated:NO];
}

- (NSString *) stringFromDeviceOrientation:(UIDeviceOrientation)uido
{
    NSString *orientation = @"UIDeviceOrientationUnknown";
    switch (uido) {
        case UIDeviceOrientationPortrait:
            orientation = @"Portrait";
            break;
        case UIDeviceOrientationPortraitUpsideDown:
            orientation = @"PortraitUpsideDown";
            break;
        case UIDeviceOrientationLandscapeRight:
            orientation = @"LandscapeRight";
            break;
        case UIDeviceOrientationLandscapeLeft:
            orientation = @"LandscapeLeft";
            break;
        case UIDeviceOrientationFaceDown:
            orientation = @"FaceDown";
            break;
        case UIDeviceOrientationFaceUp:
            orientation = @"FaceUp";
            break;
        default:
            break;
    }
    return orientation;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSDate *)collectionView:(UICollectionView *)collectionView layout:(INSElectronicProgramGuideLayout *)electronicProgramGuideLayout startTimeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    Session *ses = [self.fetchedResultsController objectAtIndexPath:indexPath];
    return [Utils dateFromString: ses.sesStartTime];
}

- (NSDate *)collectionView:(UICollectionView *)collectionView layout:(INSElectronicProgramGuideLayout *)electronicProgramGuideLayout endTimeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    Session *ses = [self.fetchedResultsController objectAtIndexPath:indexPath];
    return [Utils dateFromString:ses.sesEndTime];
}

- (NSDate *)currentTimeForCollectionView:(UICollectionView *)collectionView layout:(INSElectronicProgramGuideLayout *)collectionViewLayout
{
    return [NSDate date];
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *view = nil;
    if (kind == INSEPGLayoutElementKindSectionHeader) {
        ISSectionHeader *dayColumnHeader = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:NSStringFromClass([ISSectionHeader class]) forIndexPath:indexPath];
        Session *ses = [self.fetchedResultsController objectAtIndexPath:indexPath];
        
        dayColumnHeader.dayLabel.text = ses.venue.venName;
        view = dayColumnHeader;
    } else if (kind == INSEPGLayoutElementKindHourHeader) {
        ISHourHeader *timeRowHeader = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:NSStringFromClass([ISHourHeader class]) forIndexPath:indexPath];
        timeRowHeader.time = [self.collectionViewEPGLayout dateForHourHeaderAtIndexPath:indexPath];
        view = timeRowHeader;
    } else if (kind == INSEPGLayoutElementKindHalfHourHeader) {
        ISHourHeader *timeRowHeader = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:NSStringFromClass([ISHourHeader class]) forIndexPath:indexPath];
        timeRowHeader.time = [self.collectionViewEPGLayout dateForHalfHourHeaderAtIndexPath:indexPath];
        view = timeRowHeader;
    } else if (kind == INSEPGLayoutElementKindFloatingItemOverlay) {
        ISFloatingCellOverlay *overlay = [collectionView dequeueReusableSupplementaryViewOfKind:INSEPGLayoutElementKindFloatingItemOverlay withReuseIdentifier:NSStringFromClass([ISFloatingCellOverlay class]) forIndexPath:indexPath];
        Session *ses = [self.fetchedResultsController objectAtIndexPath:indexPath];
        overlay.titleLabel.text = ses.sesName;
        overlay.leftBorderView.backgroundColor = [SessionCategory getColorForSessionCategoryId:ses.sesCategoryId];
        [overlay setDate:[Utils dateFromString:ses.sesStartTime]];
        view = overlay;
    }
    
    return view;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(INSElectronicProgramGuideLayout *)electronicProgramGuideLayout sizeForFloatingItemOverlayAtIndexPath:(NSIndexPath *)indexPath
{
    Session *ses = [self.fetchedResultsController objectAtIndexPath:indexPath];
    return CGSizeMake([ses.sesName sizeWithAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:17.0]}].width + 20, electronicProgramGuideLayout.sectionHeight);
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return [self.fetchedResultsController.sections count];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [[[self.fetchedResultsController sections] objectAtIndex:section] numberOfObjects];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ISEPGCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([ISEPGCell class]) forIndexPath:indexPath];
    
    return cell;
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.collectionViewEPGLayout invalidateLayoutCache];
    [self.collectionView reloadData];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.landscapeON = NO;
    
    self.selectedSession = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    NSString *strID = NSStringFromClass([SessionDetailsViewController class]);
    SessionDetailsViewController* vc = [[UIStoryboard storyboardWithName:strID bundle:nil] instantiateViewControllerWithIdentifier:strID];
    [vc setSession:self.selectedSession];
    
    self.view.userInteractionEnabled = NO;
    
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - FRC

-(NSFetchedResultsController *)fetchedResultsController
{
    if (!_fetchedResultsController)
    {
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"Session" inManagedObjectContext:[LLDataAccessLayer sharedInstance].managedObjectContext];
        NSMutableArray *predicates = [[NSMutableArray alloc] initWithCapacity:1];
        //        [predicates addObject:[NSPredicate predicateWithFormat:@"venue.venName = %@", self.venue.venName]];
        
        NSString *date = [self.days objectAtIndex:self.selectedDay];
        
        [predicates addObject:[NSPredicate predicateWithFormat:@"sesType == 'Oral' OR sesType == %@", kKeyIndustrialSymposium]];
        
        if (self.days) {
            if (self.days.count > 0) {
                //RCF restore soon
                [predicates addObject:[NSPredicate predicateWithFormat:@"sesStartTime CONTAINS [cd] %@", date]];
            }
        }
        NSLog(@"Predicates : %@", predicates);
        fetchRequest.predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
        [fetchRequest setEntity:entity];
        
//        NSSortDescriptor *sortDescriptorId = [[NSSortDescriptor alloc] initWithKey:@"venue.venVenueId" ascending:YES];
        NSSortDescriptor *sortDescriptorImpotedId = [[NSSortDescriptor alloc] initWithKey:@"venue.venImportedId" ascending:YES];
        NSSortDescriptor *sortDescriptorName = [[NSSortDescriptor alloc] initWithKey:@"venue.venName" ascending:YES];
        NSSortDescriptor *sortDescriptorTime = [[NSSortDescriptor alloc] initWithKey:@"sesStartTime" ascending:YES];
        NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptorImpotedId, sortDescriptorName, sortDescriptorTime, nil];

        [fetchRequest setSortDescriptors:sortDescriptors];
        
        NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[LLDataAccessLayer sharedInstance].managedObjectContext sectionNameKeyPath:@"venue.venImportedId" cacheName:nil];
        
        aFetchedResultsController.delegate = self;
        
        self.fetchedResultsController = aFetchedResultsController;
    }
    
    return _fetchedResultsController;
}

@end
