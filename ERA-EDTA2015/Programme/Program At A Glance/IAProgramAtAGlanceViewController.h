//
//  IAProgramAtAGlanceViewController.h
//  ERA-EDTA2015
//
//  Created by Predrag Despotović on 7/20/14.
//  Copyright (c) 2014 Navus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IAProgramAtAGlanceViewController : UIViewController
-(void)loadBundledPdfWithName:(NSString*)nameNoExtension;
-(void)loadRemotePdfWithURL:(NSString*)targetPdfURL;
@end
