//
//  IAProgramAtAGlanceViewController.m
//  ERA-EDTA2015
//
//  Created by Predrag Despotović on 7/20/14.
//  Copyright (c) 2014 Navus. All rights reserved.
//

#import "IAProgramAtAGlanceViewController.h"

@interface IAProgramAtAGlanceViewController ()<UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControlDay;

@end

@implementation IAProgramAtAGlanceViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.webView.delegate = self;
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    int year = (int)[components year];
    int month = (int)[components month];
    int day = (int)[components day];
    
    //auto select current day programm
    if (year == 2014 && month == 8)
    {
        switch (day - 11)
        {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
                [self.segmentedControlDay setSelectedSegmentIndex:day-11];
                [self didSelectAnotherDay:self.segmentedControlDay];
//                [self.webView.scrollView zoomToRect:CGRectMake(0, 0, 0, 0) animated:YES];
                break;
                
            default:
            {
                [self.segmentedControlDay setSelectedSegmentIndex:0];
                [self didSelectAnotherDay:self.segmentedControlDay];
                break;
            }
        }
    }
    else
    {
        [self.segmentedControlDay setSelectedSegmentIndex:0];
        [self didSelectAnotherDay:self.segmentedControlDay];
    }
}



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - load bundled or remote pdf to webView
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(void)loadBundledPdfWithName:(NSString*)nameNoExtension
{
    NSString *path = [[NSBundle mainBundle] pathForResource:nameNoExtension ofType:@"pdf"];
    NSURL *targetURL = [NSURL fileURLWithPath:path];
    NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
    
    [self.webView loadRequest:request];
}
-(void)loadRemotePdfWithURL:(NSString*)targetPdfURL
{
    [self.activityIndicator startAnimating];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSURL *targetURL = [NSURL URLWithString:targetPdfURL];
    NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
    [self.webView loadRequest:request];
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIWebViewDelegate
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [self.activityIndicator stopAnimating];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}


- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [self.activityIndicator stopAnimating];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    UIAlertController* ac = [UIAlertController alertControllerWithTitle:@"Error" message:@"Failed to load \"Program at a glance\".\nPlease check your internet connection and try again." preferredStyle:UIAlertControllerStyleAlert];
    [ac addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:ac animated:YES completion:nil];
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Actions
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (IBAction)didSelectAnotherDay:(UISegmentedControl *)sender
{
    switch (sender.selectedSegmentIndex)
    {
        case 0:
        {
            self.webView.alpha = 0;
            [UIView animateWithDuration:0.5f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^{
                [self loadBundledPdfWithName:@"programAtAGlanceDay1"];
                self.webView.alpha = 1.0;
            }completion:^(BOOL completed)
             {
                 [self.webView.scrollView zoomToRect:CGRectMake(0,0,100,100) animated:YES];
             }];
            
            break;
        }
        case 1:
        {
            self.webView.alpha = 0;
            [UIView animateWithDuration:0.5f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^{
                [self loadBundledPdfWithName:@"programAtAGlanceDay2"];
                self.webView.alpha = 1.0;
            }completion:^(BOOL completed)
             {
                 [self.webView.scrollView zoomToRect:CGRectMake(0,0,50,50) animated:YES];
             }];
            break;
        }
        case 2:
        {
            self.webView.alpha = 0;
            [UIView animateWithDuration:0.5f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^{
                [self loadBundledPdfWithName:@"programAtAGlanceDay3"];
                self.webView.alpha = 1.0;
            }completion:^(BOOL completed)
             {
                 [self.webView.scrollView zoomToRect:CGRectMake(0,0,50,50) animated:YES];
             }];
            break;
        }
        case 3:
        {
            self.webView.alpha = 0;
            [UIView animateWithDuration:0.5f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^{
                [self loadBundledPdfWithName:@"programAtAGlanceDay4"];
                self.webView.alpha = 1.0;
            }completion:^(BOOL completed)
             {
                 [self.webView.scrollView zoomToRect:CGRectMake(0,0,50,50) animated:YES];
             }];
            break;
        }
        case 4:
        {
            self.webView.alpha = 0;
            [UIView animateWithDuration:0.5f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^{
                [self loadBundledPdfWithName:@"programAtAGlanceDay5"];
                self.webView.alpha = 1.0;
            }completion:^(BOOL completed)
             {
                 [self.webView.scrollView zoomToRect:CGRectMake(0,0,50,50) animated:YES];
             }];
            break;
        }
        
            
        default:
            break;
    }
}

- (IBAction)swipeLeftAction:(UISwipeGestureRecognizer *)sender
{
    if (sender.state == UIGestureRecognizerStateRecognized)
    {
        int nextIndex = (self.segmentedControlDay.selectedSegmentIndex + 1) > 3 ? 4 : (int)(self.segmentedControlDay.selectedSegmentIndex + 1);
        if (nextIndex > 4)
        {
            return;
        }
        [self.segmentedControlDay setSelectedSegmentIndex:nextIndex];
        [self didSelectAnotherDay:self.segmentedControlDay];
    }
}

- (IBAction)swipeRightAction:(UISwipeGestureRecognizer *)sender
{
    if (sender.state == UIGestureRecognizerStateRecognized)
    {
        int nextIndex = (self.segmentedControlDay.selectedSegmentIndex - 1) == 0 ? 0 : (int)(self.segmentedControlDay.selectedSegmentIndex - 1);
        if (nextIndex == -1)
        {
            return;
        }
        [self.segmentedControlDay setSelectedSegmentIndex:nextIndex];
        [self didSelectAnotherDay:self.segmentedControlDay];
    }
}

- (IBAction)doubleTapAction:(UITapGestureRecognizer*)sender
{
    if (sender.state == UIGestureRecognizerStateRecognized)
    {
        if (self.webView.scrollView.contentScaleFactor == 1.0)
        {
            [self.webView.scrollView setContentScaleFactor:4.0];
        }
        else
        {
            [self.webView.scrollView setContentScaleFactor:1.0];
        }
    }
}

@end
