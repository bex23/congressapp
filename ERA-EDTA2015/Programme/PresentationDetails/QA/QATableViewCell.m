//
//  QATableViewCell.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 4/14/16.
//  Copyright © 2016 Navus. All rights reserved.
//

#import "QATableViewCell.h"

@implementation QATableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
