//
//  QAViewController.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 4/14/16.
//  Copyright © 2016 Navus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QAViewController : UIViewController

@property (strong, nonatomic) Talk *talk;

@end
