//
//  QAViewController.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 4/14/16.
//  Copyright © 2016 Navus. All rights reserved.
//

#import "QAViewController.h"

#import "QATableViewCell.h"

@interface QAViewController () <UITableViewDelegate, UITableViewDataSource, UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblSpeakerName;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UILabel *lblPresentationName;
@property (weak, nonatomic) IBOutlet UILabel *lblInfo;

@property (weak, nonatomic) IBOutlet UITableView *tblQA;

@property (nonatomic, strong) NSMutableArray *arrayQA;

@property (weak, nonatomic) IBOutlet UIView *viewQATextView;
@property (weak, nonatomic) IBOutlet UITextView *txtViewQA;

@property (strong, nonatomic) NSString *strPlaceholder;

@property (weak, nonatomic) IBOutlet UIButton *btnAskQuestion;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnSend;

@property (strong, nonatomic) User *currentUser;

@end

@implementation QAViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.arrayQA = [[NSMutableArray alloc] init];
    
    [self loadData];
    

    
    self.strPlaceholder = @"Write your questions to the speaker";
    
    self.currentUser = [User current];
    
    self.lblInfo.text = [NSString stringWithFormat:@"Ask questions during and after the congress. You can also check your Q&A status and replies via a PC on your E-materials account at www.e-materials.com/%@", [[RealmRepository shared] confSlug]];
}

-(void)loadData
{
    NSString *strSpeakersName = nil;
    for (Speaker *speaker in self.talk.speakrs)
    {
        if (strSpeakersName)
        {
            strSpeakersName = [NSString stringWithFormat:@"%@/n %@ %@", strSpeakersName, speaker.spkLastName ? speaker.spkLastName: @"", speaker.spkFirstName ? speaker.spkFirstName: @""];
        }
        else
        {
            strSpeakersName = [NSString stringWithFormat:@"%@ %@", speaker.spkLastName ? speaker.spkLastName: @"", speaker.spkFirstName ? speaker.spkFirstName: @""];
        }
    }
    self.lblSpeakerName.text = strSpeakersName;
    
    self.lblDate.text = [Utils formatDateString:[Utils getDate:self.talk.tlkStartTime]];
    
    NSString* startTime = [Utils formatTimeStringFromDate:self.talk.tlkStartTime];
    NSString* endTime = [Utils formatTimeStringFromDate:self.talk.tlkEndTime];
    self.lblTime.text = [NSString stringWithFormat:@"%@ - %@", startTime, endTime];
    
    self.lblPresentationName.text = self.talk.tlkTitle;
    
    [self loadTableData];
}

-(void)loadTableData
{
    [self.arrayQA removeAllObjects];
    
    NSArray *arrayAllQA = [self.talk.questions allObjects];
    
    // first add questions
    NSArray *arrayOfQuestions = [arrayAllQA filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"tqaParentId = 0"]];
    arrayOfQuestions = [arrayOfQuestions sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"tqaCreatedAt" ascending:YES]]];
    
    // go throught qustions and add answares after question
    for (TalkQA *question in arrayOfQuestions)
    {
        [self.arrayQA addObject:question];
        
        NSArray *arrayOfAnswares = [arrayAllQA filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"tqaParentId == %@ AND tqaParentId != 0", question.tqaId]];
        if (arrayOfAnswares.count > 0)
        {
            [self.arrayQA addObjectsFromArray:arrayOfAnswares];
        }
    }
    
    [self.tblQA reloadData];
}

-(void)initTxtView
{
    self.txtViewQA.layer.borderColor = [UIColor colorWithHex:@"AAAAAA" andAlpha:1.0].CGColor;
    self.txtViewQA.layer.borderWidth = 1;
    self.txtViewQA.text = self.strPlaceholder;
    self.txtViewQA.textColor = [UIColor lightGrayColor];
    self.txtViewQA.delegate = self;
}

#pragma mark - UITextView Delegate

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:self.strPlaceholder])
    {
        textView.text = @"";
    }
    textView.textColor = [UIColor blackColor];
    return YES;
}

-(void)textViewDidChange:(UITextView *)textView
{
    if(textView.text.length == 0)
    {
        textView.textColor = [UIColor lightGrayColor];
        textView.text = self.strPlaceholder;
        [textView resignFirstResponder];
    }
}

#pragma mark - TableView Data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section
{
    return [self.arrayQA count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    QATableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"tblCellQA" forIndexPath:indexPath];
    
    TalkQA *qa = [self.arrayQA objectAtIndex:indexPath.row];
    
    cell.lblQuestion.text = qa.tqaContent;
    
    Speaker *speaker = [Speaker getSpeakerWithId:[qa.tqaUserId stringValue]];
    
    if (speaker)
    {
        cell.lblSpeakerName.text = [NSString stringWithFormat:@"%@ %@", speaker.spkLastName ? speaker.spkLastName: @"", speaker.spkFirstName ? speaker.spkFirstName: @""];
    }
    else if ([qa.tqaUserId isEqual:@(self.currentUser.id)])
    {
        cell.lblSpeakerName.text = [NSString stringWithFormat:@"%@ %@", self.currentUser.firstName, self.currentUser.lastName];
    }
    else
    {
        cell.lblSpeakerName.text = @"";
    }
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TalkQA *qa = [self.arrayQA objectAtIndex:indexPath.row];
    
    CGSize lblQuestionSize = [qa.tqaContent boundingRectWithSize:CGSizeMake(tableView.frame.size.width - 16, FLT_MAX)
                                                         options:NSStringDrawingUsesLineFragmentOrigin
                                                      attributes:@{
                                                                   NSFontAttributeName : [UIFont systemFontOfSize:14]
                                                                   } context:nil].size;

    // 50 = top spaces + lblQuestioner height
    return 50 + lblQuestionSize.height;
}

#pragma mark - Navitation



#pragma mark - IBActions

- (IBAction)askQuestion:(id)sender
{
    [self showInsertQAView];
}

- (IBAction)send:(id)sender
{
    if (![self.txtViewQA.text isEqualToString:self.strPlaceholder])
    {
        User *user = [User current];
        NSDictionary *dicQA = @{ @"talkId" : self.talk.tlkTalkId,
                                 @"content" : self.txtViewQA.text,
                                 @"userId" : @(user.id)};
        
        [TalkQA addQuestion:dicQA];
        [[ESyncData sharedInstance] uploadQAs];
        
        [self loadTableData];
        [self showAllQAsView];
    }
    else
    {
        [self.txtViewQA shake];
    }
}

- (IBAction)cancel:(id)sender
{
    [self showAllQAsView];
}

#pragma mark - other functions

-(void)showInsertQAView
{
    self.viewQATextView.hidden = self.btnSend.hidden = self.btnCancel.hidden = NO;
    self.btnAskQuestion.hidden = YES;
    [self initTxtView];
    self.title = @"Ask a question";
    
}

-(void)showAllQAsView
{
    [self.txtViewQA resignFirstResponder];
    self.viewQATextView.hidden = self.btnSend.hidden = self.btnCancel.hidden = YES;
    self.btnAskQuestion.hidden = NO;
    self.title = @"Q&A";
}

@end
