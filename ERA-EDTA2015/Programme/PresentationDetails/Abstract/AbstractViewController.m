//
//  AbstractViewController.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 2/20/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "AbstractViewController.h"

@interface AbstractViewController ()

@property (weak, nonatomic) IBOutlet UITextView *txtAbstractText;

@end

@implementation AbstractViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.txtAbstractText setHTMLFromString:self.abstractText];
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    [self.txtAbstractText setContentOffset:CGPointZero animated:NO];
}

- (IBAction)actionClose:(id)sender
{
    [self.delegate abstractCancel];
}

@end
