//
//  AbstractInfoViewController.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 5/16/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AbstractInfoDelegate <NSObject>

-(void)abstractInfoCancel;

@end

@interface AbstractInfoViewController : UIViewController

@property (nonatomic, assign) id <AbstractInfoDelegate> delegate;

@end
