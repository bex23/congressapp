//
//  AbstractInfoViewController.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 5/16/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "AbstractInfoViewController.h"

@interface AbstractInfoViewController ()

@end

@implementation AbstractInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (IBAction)actionClose:(id)sender
{
    [self.delegate abstractInfoCancel];
}

@end
