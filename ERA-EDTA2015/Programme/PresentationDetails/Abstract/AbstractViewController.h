//
//  AbstractViewController.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 2/20/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AbstractDelegate <NSObject>

-(void)abstractCancel;

@end

@interface AbstractViewController : UIViewController

@property (nonatomic, assign) id <AbstractDelegate> delegate;

@property (strong, nonatomic) NSString *abstractText;

@end
