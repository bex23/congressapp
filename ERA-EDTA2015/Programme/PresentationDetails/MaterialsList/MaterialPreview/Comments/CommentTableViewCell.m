//
//  CommentTableViewCell.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 4/25/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "CommentTableViewCell.h"

@implementation CommentTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
