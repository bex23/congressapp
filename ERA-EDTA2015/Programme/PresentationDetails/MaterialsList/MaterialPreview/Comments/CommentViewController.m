//
//  CommentViewController.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 4/16/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "CommentViewController.h"

#import "CommentTableViewCell.h"

@interface CommentViewController () <UITextViewDelegate, UITableViewDelegate, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITextView *txtViewComment;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSArray *arrayComments;

@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;

@end

@implementation CommentViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //Left bar button
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Submit" style:UIBarButtonItemStylePlain target:self action:@selector(submit)];
    
    UIBarButtonItem *btnBack = [[UIBarButtonItem alloc]
                                initWithTitle:@"Back"
                                style:UIBarButtonItemStylePlain
                                target:self
                                action:nil];
    self.navigationController.navigationBar.topItem.backBarButtonItem=btnBack;
    
    [self initTxtView];
    
    self.arrayComments = [HighlightedSlideComment getHighlightedSlideCommentsForSlide:self.slide.hsSlideId];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 50.0;
}

-(void)initTxtView
{
    self.txtViewComment.layer.cornerRadius = 5;
    self.txtViewComment.layer.borderColor = [UIColor colorWithHex:@"AAAAAA" andAlpha:1.0].CGColor;
    self.txtViewComment.layer.borderWidth = 1;
    self.txtViewComment.text = @"Add comment";
    self.txtViewComment.textColor = [UIColor lightGrayColor];
    self.txtViewComment.delegate = self;
}

#pragma mark - UITextView delegate

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@"Add comment"])
    {
        textView.text = @"";
    }
    textView.textColor = [UIColor blackColor];
    return YES;
}

-(void)textViewDidChange:(UITextView *)textView
{
    if(textView.text.length == 0)
    {
        textView.textColor = [UIColor lightGrayColor];
        textView.text = @"Add comment";
        [textView resignFirstResponder];
    }
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    if(textView.text.length == 0)
    {
        textView.textColor = [UIColor lightGrayColor];
        textView.text = @"Add comment";
    }
}

#pragma mark - TableView Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrayComments.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CommentTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([CommentTableViewCell class]) forIndexPath:indexPath];
    
    HighlightedSlideComment *comment = [self.arrayComments objectAtIndex:indexPath.row];
    
    cell.lblComment.text = comment.hscComment;
    
    [cell updateConstraintsIfNeeded];
    
    return cell;
}

#pragma mark - TableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @"Remove";
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{    
    HighlightedSlideComment *comment = [self.arrayComments objectAtIndex:indexPath.row];
    
    [[ESyncData sharedInstance] deleteComment:comment success:^{
        
        self.arrayComments = [HighlightedSlideComment getHighlightedSlideCommentsForSlide:self.slide.hsSlideId];
        
        [self.tableView reloadData];
        
    } failure:^{
        
    }];
}

#pragma mark - Submit

-(void)submit
{
    if ([self.txtViewComment.text isEqualToString:@"Add comment"] || self.txtViewComment.text.length == 0)
    {
        [self.txtViewComment shake];
        return;
    }
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    UIBarButtonItem * barButton = [[UIBarButtonItem alloc] initWithCustomView:self.activityIndicator];
    [self navigationItem].rightBarButtonItem = barButton;
    [self.activityIndicator startAnimating];
    
    [[ESyncData sharedInstance] comment:self.txtViewComment.text highlightedSlide:self.slide
                                success:^{
                                    
                                    self.txtViewComment.textColor = [UIColor lightGrayColor];
                                    self.txtViewComment.text = @"Add comment";
                                    [self.txtViewComment resignFirstResponder];
                                    
                                    [self.activityIndicator startAnimating];
                                    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Submit" style:UIBarButtonItemStylePlain target:self action:@selector(submit)];
                                    
                                    self.arrayComments = [HighlightedSlideComment getHighlightedSlideCommentsForSlide:self.slide.hsSlideId];
                                    
                                    [self.tableView reloadData];

                                } failure:^{
                                    [self.activityIndicator stopAnimating];
                                    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Submit" style:UIBarButtonItemStylePlain target:self action:@selector(submit)];
                                }];
}

#pragma mark - Set left margin of table cells to ziro for iOS 8

-(void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

@end
