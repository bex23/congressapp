//
//  CommentTableViewCell.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 4/25/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblComment;

@end
