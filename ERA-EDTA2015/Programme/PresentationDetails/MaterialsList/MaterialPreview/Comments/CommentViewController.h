//
//  CommentViewController.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 4/16/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentViewController : UIViewController 

@property (strong, nonatomic) HighlightedSlide *slide;

@end
