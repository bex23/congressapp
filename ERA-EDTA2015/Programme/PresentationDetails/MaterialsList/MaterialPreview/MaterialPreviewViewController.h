//
//  MaterialPreviewViewController.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 4/15/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MaterialPreviewViewController : UIViewController <UIPageViewControllerDataSource, UIPageViewControllerDelegate>

@property (strong, nonatomic) NSArray *imagesUrls;
@property (strong, nonatomic) Material *material;
@property (assign, nonatomic) BOOL previewMaterialFromLocal;
@property (strong, nonatomic) NSURL *streamURL;

@end
