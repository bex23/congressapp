//
//  ImagesPreviewViewController.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 4/13/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "ImageViewController.h"

@interface ImageViewController () <UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIButton *btnTagForSummary;
@property (weak, nonatomic) IBOutlet UIButton *btnComments;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activator;

@property (strong, nonatomic) HighlightedSlide *highlightedSlide;

@end

@implementation ImageViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupImage];
}

#pragma mark - Setups

-(void)setupImage
{
    if (self.localFile == YES)
    {
        UIImage * image = [UIImage imageWithContentsOfFile:self.imagesUrl];
        [self.imageView setImage:image];
    }
    else
    {
        NSURL *url = [NSURL URLWithString:self.imagesUrl];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        
        [self.activator startAnimating];
        
        [self.imageView setImageWithURLRequest:request placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
            
            [self.imageView setImage:image];
            [self.activator stopAnimating];

        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
            [self.activator stopAnimating];
        }];
    }
}

#pragma mark - UIScrollViewDelegate

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.imageView;
}

#pragma mark - IBActions

- (IBAction)actionDoubleTapsOnImg:(UITapGestureRecognizer *)sender
{
    [self.scrollView setZoomScale:1 animated:YES];
}

@end
