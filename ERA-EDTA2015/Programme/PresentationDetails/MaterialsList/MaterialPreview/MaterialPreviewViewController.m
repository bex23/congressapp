//
//  MaterialPreviewViewController.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 4/15/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "MaterialPreviewViewController.h"
#import "ImageViewController.h"
#import "CommentViewController.h"

@interface MaterialPreviewViewController ()

@property (weak, nonatomic) IBOutlet UILabel *lblMaterialTitle;
@property (weak, nonatomic) IBOutlet UIView *holderView;
@property (weak, nonatomic) IBOutlet UIButton *infoButton;

@property (strong, nonatomic) UIPageViewController *pageController;
@property (weak, nonatomic) IBOutlet UIView *viewMain;

@property (weak, nonatomic) IBOutlet UIButton *btnTagForSummary;
@property (weak, nonatomic) IBOutlet UIButton *btnComments;

@property (strong, nonatomic) HighlightedSlide *highlightedSlide;
@property (assign, nonatomic) NSInteger currentIndex;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (strong, nonatomic) NSTimer *timer;
@property (strong, nonatomic) NSString *sessionID;
@property (strong, nonatomic) UIWebView *webView;

@end

@implementation MaterialPreviewViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    self.currentIndex = 0;
    [self setupTag];
    
    self.pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    
    self.pageController.dataSource = self;
    self.pageController.delegate = self;
    [[self.pageController view] setFrame:[[self viewMain] bounds]];
    
    ImageViewController *initialViewController = [self viewControllerAtIndex:self.currentIndex];
    
    NSArray *viewControllers = [NSArray arrayWithObject:initialViewController];
    
    [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    [self addChildViewController:self.pageController];
    [[self viewMain] addSubview:[self.pageController view]];
    [self.pageController didMoveToParentViewController:self];
    
    [self setCurrentPageNumber:1];
    self.lblMaterialTitle.text = self.material.matName;
    
    [self.btnComments setTitleColor:[UIColor colorWithHex:kGlobalColor andAlpha:1.0] forState:UIControlStateNormal];
    
    [self startSessionPreviewOfMaterial];
    
    if ([self.material.talk.tlkType isEqualToString:@"Poster"]) {
        self.btnTagForSummary.hidden = YES;
        self.infoButton.hidden = YES;
        if (self.streamURL != nil) {
            self.webView = [[UIWebView alloc] initWithFrame:CGRectZero];
            [self.holderView addSubview:self.webView];
            NSURLRequest *request = [[NSURLRequest alloc] initWithURL:self.streamURL];
            [self.webView loadRequest:request];
        }
    }
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    CGRect frame = CGRectMake(0, 0, self.holderView.frame.size.width, self.holderView.frame.size.height);
    [self.webView setFrame:frame];
}

-(void)startSessionPreviewOfMaterial
{
    self.sessionID = [NSString stringWithFormat:@"ios_%@_%@", self.material.matMaterialId, @([[NSDate date] timeIntervalSince1970])];

    [[NSUserDefaults standardUserDefaults] setObject:@{@"active" : @"YES", @"sessionId" : self.sessionID} forKey:kKeyActiveMaterialBrowsingSession];
    [[NSUserDefaults standardUserDefaults] synchronize];

    [MaterialBrowsing insertMaterialBrowsingSession:self.sessionID forMaterial:self.material];

    [self.timer invalidate];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(recordMaterialBrowsing) userInfo:nil repeats:YES];
}

-(void)recordMaterialBrowsing
{
    NSDictionary *dictSessin = [[NSUserDefaults standardUserDefaults] objectForKey:kKeyActiveMaterialBrowsingSession];
    if (dictSessin && [[dictSessin objectForKey:@"active"] isEqualToString:@"YES"])
    {
        [MaterialBrowsing recordMaterialBrowsingForSession:self.sessionID];
    }
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [self.timer invalidate];
    
    [MaterialBrowsing recordMaterialBrowsingForSession:self.sessionID];
    [[ESyncData sharedInstance] syncMaterialBrowsings];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kKeyActiveMaterialBrowsingSession];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

#pragma mark - Setups

-(void)setCurrentPageNumber:(NSInteger)index
{
    self.title = [NSString stringWithFormat:@"%ld of %ld", (unsigned long)index, (unsigned long)self.imagesUrls.count];
}

-(void)setupTag
{
    // init highlightedSlide
    self.highlightedSlide = [HighlightedSlide getHighlitedSlide:[NSNumber numberWithInteger:self.currentIndex] forMaterial:self.material.matMaterialId];
    
    if (self.highlightedSlide && self.highlightedSlide.hsHighlighted.integerValue == 1)
    {
        self.btnTagForSummary.tag = 1;
        
        [self.btnTagForSummary setTitle:@"TAGGED" forState:UIControlStateNormal];
        self.btnTagForSummary.backgroundColor = [UIColor colorWithHex:kOtherColor andAlpha:1.0];
        [self.btnTagForSummary setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        self.btnComments.hidden = NO;
    }
    else
    {
        self.btnTagForSummary.tag = 0;
        
        [self.btnTagForSummary setTitle:@"TAG FOR SUMMARY" forState:UIControlStateNormal];
        self.btnTagForSummary.backgroundColor = [UIColor clearColor];
        [self.btnTagForSummary setTitleColor:[UIColor colorWithHex:kGlobalColor andAlpha:1.0] forState:UIControlStateNormal];

        self.btnComments.hidden = YES;
    }
}

#pragma mark - UIPageViewControllerDataSource

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(ImageViewController *)viewController index];
    
    if (index == 0) {
        return nil;
    }
    
    index--;
    
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(ImageViewController *)viewController index];
    
    index++;
    
    if (index == _imagesUrls.count) {
        return nil;
    }
    
    return [self viewControllerAtIndex:index];
}

- (ImageViewController *)viewControllerAtIndex:(NSUInteger)index
{
    ImageViewController *childViewController = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([ImageViewController class])];
    
    childViewController.index = index;
    childViewController.localFile = self.previewMaterialFromLocal;
    if (self.imagesUrls.count > index) {
        childViewController.imagesUrl = self.imagesUrls[index];
    }
    
    return childViewController;
}

- (void)pageViewController:(UIPageViewController *)pvc didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed
{
    // If the page did not turn
    if (!completed)
    {
        // You do nothing because whatever page you thought
        // the book was on before the gesture started is still the correct page
        return;
    }
    
    // This is where you would know the page number changed and handle it appropriately
    // [self sendPageChangeNotification:YES];
    
    NSInteger currentIndex = ((ImageViewController *)[pvc.viewControllers lastObject]).index;
    [self setCurrentPageNumber:currentIndex + 1];
    
    self.currentIndex = (!currentIndex) ? 0 : currentIndex;
    
    [self setupTag];
}

#pragma mark - IBActions

- (IBAction)actonTagForSummary:(UIButton *)sender
{
    [self activatorStart];
    
    if (sender.tag == 0)
    {
        [[ESyncData sharedInstance] highlightedSlide:[NSNumber numberWithInteger:self.currentIndex] forMaterial:self.material.matMaterialId
                                             success:^(HighlightedSlide *slide) {
                                                 [self activatorStop];
                                                 
                                                 self.highlightedSlide = slide;
                                                 
                                                 self.btnComments.hidden = NO;
                                                 
                                                 sender.tag = 1;
                                                 
                                                 [sender setTitle:@"TAGGED" forState:UIControlStateNormal];
                                                 [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                                                 sender.backgroundColor = [UIColor colorWithHex:kOtherColor andAlpha:1.0];
                                                 
                                             } failure:^{
                                                 [self activatorStop];
                                             }];
    }
    else
    {
        [[ESyncData sharedInstance] unhighlightedSlide:self.highlightedSlide
                                               success:^{
                                                   
                                                   [self activatorStop];
                                                   
                                                   self.highlightedSlide = nil;
                                                   
                                                   self.btnComments.hidden = YES;
                                                   
                                                   sender.tag = 0;
                                                   
                                                   [sender setTitle:@"TAG FOR SUMMARY" forState:UIControlStateNormal];
                                                   [sender setTitleColor:[UIColor colorWithHex:kGlobalColor andAlpha:1.0] forState:UIControlStateNormal];
                                                   sender.backgroundColor = [UIColor clearColor];
                                                   
                                               } failure:^{
                                                   [self activatorStop];
                                               }];
    }
}

- (IBAction)actionComment:(UIButton *)sender
{
    NSString *strId = NSStringFromClass([CommentViewController class]);
    
    CommentViewController *vc = [[UIStoryboard storyboardWithName:strId bundle:nil] instantiateViewControllerWithIdentifier:strId];
    
    // reload object because of syncing comments
    vc.slide = [HighlightedSlide getHighlitedSlide:self.highlightedSlide.hsSlideId];
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)actionInfo:(UIButton *)sender
{
    [Utils showMessage:[NSString stringWithFormat:@"You will find your tagged slides in the Summary Builder. Click on E-materials %@ and open The Summary Builder.", [[RealmRepository shared] confYear]]];
}


#pragma mark - Activity Indicator

-(void)activatorStart
{
    self.view.userInteractionEnabled = NO;
    [self.activityIndicator startAnimating];
}

-(void)activatorStop
{
    self.view.userInteractionEnabled = YES;
    [self.activityIndicator stopAnimating];
}

@end
