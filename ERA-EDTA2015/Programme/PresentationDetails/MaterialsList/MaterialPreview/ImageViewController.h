//
//  ImagesPreviewViewController.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 4/13/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageViewController : UIViewController

@property (strong, nonatomic) NSString *imagesUrl;
@property (assign, nonatomic) BOOL localFile;
@property (assign, nonatomic) NSInteger index;

@end
