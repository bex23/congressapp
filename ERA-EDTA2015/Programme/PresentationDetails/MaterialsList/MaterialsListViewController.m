//
//  MaterialsListViewController.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 2/6/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "MaterialsListViewController.h"

#import "MaterialTableViewCell.h"

#import <QuickLook/QuickLook.h>
#import "IAQLPreviewViewController.h"

#import "MaterialPreviewViewController.h"
#import "SSZipArchive.h"

@interface MaterialsListViewController () // <QLPreviewControllerDataSource, QLPreviewControllerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activator;

@property (assign, nonatomic) BOOL isMaterialZip;

@end

@implementation MaterialsListViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 300.0;
    
    self.tableView.alwaysBounceVertical = NO;
}



#pragma mark - TableView Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section
{
    return _arrayMaterials.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MaterialTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([MaterialTableViewCell class]) forIndexPath:indexPath];
    
    Material *material = [self.arrayMaterials objectAtIndex:indexPath.row];
    
    cell.lblMaterialName.text = material.matName;
    
    cell.imgPDF.image = [cell.imgPDF.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [cell.imgPDF setTintColor:[UIColor colorWithHex:kOtherColor andAlpha:1.0]];
    
    cell.viewLine.backgroundColor = [UIColor colorWithHex:kOtherColor andAlpha:1.0];
    
    [cell updateConstraintsIfNeeded];
        
    return cell;
}

#pragma mark - TableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    User *currentUser = [User current];
    // if user is not logined in
    if (!currentUser)
    {
        [CUtils showLoginQuestion];
        return;
    }
    
    if(!currentUser.hasBadge) // it is important to requiredLogin is after requiredBadge
    {
        [self showRequiredBadgeMessage];
        return;
    }
    
    __block Material *material = [self.arrayMaterials objectAtIndex:indexPath.row];
    self.isMaterialZip = material.matAvailableDownload.integerValue;
    
    NSLog(@"%@", material.matMaterialId);

    // there is no internet connection and material isn't avaiable for downloading
    if (![[NSUserDefaults standardUserDefaults] valueForKey:kTypeOfInternetConnection] && material.matAvailableDownload.integerValue == 0)
    {
        [Utils showMessage:@"Please check your internet connection as the material is for view only and unavailable for downloading, so it can be seen only if your app is working in online mode." withTitle:@"No network connection"];
        return;
        
    } // there is no internet connection and material is avaiable for downloading
    else if (![[NSUserDefaults standardUserDefaults] valueForKey:kTypeOfInternetConnection] && material.matAvailableDownload.integerValue == 1)
    {
        NSString *materialDirPath = [CUtils getDirPathForMaterial:material];
        // material is avaiable for downloading and isn't downloaded yet
        if (![[NSFileManager defaultManager] fileExistsAtPath:materialDirPath])
        {
            [CUtils showOfflineMessage];
            return;
        }
        else // material downloaded, just show it
        {
            [self presentMaterial:material withImagesUrls:nil];
            return;
        }
    }
    
    [self activatorStart];
   
    // -> order -> download -> show
    if ([Order doesExistOrderWithTalkId:material.talk.tlkTalkId])
    {
        [self activatorStop];
        [self downloadMaterial:material];
    }
    else
    {
        if(currentUser.isMember)
        {
            [[ESyncData sharedInstance] orderderTalk:material.talk success:^{
                [self activatorStop];
                [self downloadMaterial:material];
            } failure:^{
                [self activatorStop];
            }];
        }
        else
        {
            [[ESyncData sharedInstance] getPresentationsCost:material.talk.tlkTalkId
                                                 withSuccess:^(int cost)
             {
                 // get epoints for gain message
                 [[ESyncData sharedInstance] getEpointsWithSuccess:^(int epoints) {
                     
                     [self activatorStop];
                     [self showGainAccessMessageForEpoints:epoints forMaterial:material withCost:cost];
                     
                 } failure:^{
                     [self activatorStop];
                 }];
             } failure:^{
                 [self activatorStop];
             }];
        }
    }
}

-(void)showRequiredBadgeMessage
{
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:nil
                                message:@"To gain access to E-materials you need to insert the badge number."
                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* btnInsert = [UIAlertAction actionWithTitle:@"Insert"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action) {
                                                          UINavigationController *nav = [[UIStoryboard storyboardWithName:@"EnterBadgeViewController" bundle:nil] instantiateViewControllerWithIdentifier:@"navEnterBadgeViewController"];
                                                          [self.navigationController presentViewController:nav animated:YES completion:nil];
                                                      }];
    
    UIAlertAction* btnCancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
    
    [alert addAction:btnCancel];
    [alert addAction:btnInsert];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)showGainAccessMessageForEpoints:(int)epoints forMaterial:(Material *)material withCost:(int)cost
{
    if(cost == 0)
    {
        [[ESyncData sharedInstance] orderderTalk:material.talk success:^{
            [self downloadMaterial:material];
        } failure:^{
        }];
    }
    else
    {
        NSString *epointsString = [NSString stringWithFormat:@"%d", epoints];
        NSString *costString = [NSString stringWithFormat:@"%d", cost];
        
        UIAlertController *alert;
        if(epoints == 0)
        {
            alert = [UIAlertController
                     alertControllerWithTitle:@""
                     message: @"You currently have no further access points left for this related event. Please contact, support@e-materials.com to request access to this presentation."
                     preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* btnContactUs = [UIAlertAction actionWithTitle:@"Contact Us"
                                                                   style:UIAlertActionStyleDefault
                                                                 handler:^(UIAlertAction * action) {
                                                                     
                                                                     NSString *recipients = @"mailto:support@e-materials.com";
                                                                     
                                                                     NSString *email = recipients;
                                                                     
                                                                     email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                                                                     
                                                                     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
                                                                 }];
            [alert addAction:btnContactUs];
        }
        else
        {
            alert = [UIAlertController
                     alertControllerWithTitle:@""
                     message: [NSString stringWithFormat:@"Gaining access to \n%@\n will cost you %@ access point.\nYou currently have %@ access %@ left for this related event.\nDo you want to access this presentation?", material.talk.tlkTitle, costString, epointsString, epoints > 1 ? @"points" : @"point"]
                     preferredStyle:UIAlertControllerStyleAlert];
            
            
            UIAlertAction* btnRegister = [UIAlertAction actionWithTitle:@"GAIN ACCESS"
                                                                  style:UIAlertActionStyleDefault
                                                                handler:^(UIAlertAction * action) {
                                                                    
                                                                    [[ESyncData sharedInstance] orderderTalk:material.talk success:^{
                                                                        [self downloadMaterial:material];
                                                                    } failure:^{
                                                                    }];
                                                                }];
            [alert addAction:btnRegister];
        }
        
        
        UIAlertAction* btnCancel = [UIAlertAction actionWithTitle:@"CANCEL"
                                                            style:UIAlertActionStyleDefault
                                                          handler:nil];
        
        [alert addAction:btnCancel];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

-(void)downloadMaterial:(Material *)material
{
    [self activatorStart];
    
    if (!self.isMaterialZip) // WE WILL GET SOME ARRAY OF URLS
    {
        [[ESyncData sharedInstance] getImagesForMaterial:material
                                             success:^(NSArray *images) {
                                                 [self activatorStop];
                                                 [self presentMaterial:material withImagesUrls:images];
                                             } failure:^() {
                                                 [self activatorStop];
                                             }];
    }
    else // WE WILL GET ZIP FILE WITH IMAGES
    {
        [[ESyncData sharedInstance] downloadMaterial:material
                                             success:^(id responseObject) {
                                                 [self activatorStop];
                                                 // it is zip file; you need to unzip
                                                 if([self unzipMaterial:material])
                                                 {
                                                     [self presentMaterial:material withImagesUrls:nil];
                                                 }
                                                 else
                                                 {
                                                     [Utils showMessage:@"File isn't ready yet." withTitle:@"Error"];
                                                 }
                                             } failure:^() {
                                                 [self activatorStop];
                                             }];
    }
}

-(BOOL)unzipMaterial:(Material *)material
{
    NSString *materialFilePath = [CUtils getFilePathForMaterial:material];
    NSString *materialDirPath = [CUtils getDirPathForMaterial:material];
    
    // if materials already unziped - remove it
    if ([[NSFileManager defaultManager] fileExistsAtPath:materialDirPath] && [[NSUserDefaults standardUserDefaults] valueForKey:kTypeOfInternetConnection])
    {
        [[NSFileManager defaultManager] removeItemAtPath:materialDirPath error:nil];
    }
    
    // Unzip
    return [SSZipArchive unzipFileAtPath:materialFilePath toDestination: materialDirPath];
}

-(void)activatorStart
{
    self.view.userInteractionEnabled = NO;
    [self.activator startAnimating];
}

-(void)activatorStop
{
    self.view.userInteractionEnabled = YES;
    [self.activator stopAnimating];
}

#pragma mark - PresentMaterial

- (void)presentMaterial:(Material *)material withImagesUrls:(NSArray *)imagesUrls
{
    [[ESyncData sharedInstance] getHighlightedSlidesForMaterial:material completion:^{
        
        NSString *strID = NSStringFromClass([MaterialPreviewViewController class]);
        MaterialPreviewViewController *ipvc = [[UIStoryboard storyboardWithName:strID bundle:nil] instantiateViewControllerWithIdentifier:strID];
        ipvc.material = material;
        ipvc.previewMaterialFromLocal = self.isMaterialZip;
        ipvc.streamURL = self.streamingURL;
        
        if (self.isMaterialZip)
        {
            //        //QuickLook
            //        IAQLPreviewViewController *previewController = [[IAQLPreviewViewController alloc] init];
            //        previewController.dataSource = self;
            //        previewController.delegate = self;
            //
            //        [self.navigationController pushViewController:previewController animated:YES];
            
            NSString *materialDirPath = [CUtils getDirPathForMaterial:material];
            
            NSError * error;
            NSArray * directoryContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:materialDirPath error:&error];
            
            directoryContents = [directoryContents sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
                
                if ([obj1 intValue] == [obj2 intValue])
                    return NSOrderedSame;
                
                else if ([obj1 intValue] < [obj2 intValue])
                    return NSOrderedAscending;
                
                else
                    return NSOrderedDescending;
                
            }];
            
            NSMutableArray *tempImagesUrls = [NSMutableArray arrayWithCapacity:1];
            
            for (NSString *imageName in directoryContents)
            {
                [tempImagesUrls addObject:[NSString stringWithFormat:@"%@%@",materialDirPath,imageName]];
            }
            
            ipvc.imagesUrls = tempImagesUrls;
        }
        else
        {
            User *currentUser = [User current];
            
            NSMutableArray *tempImagesUrls = [NSMutableArray arrayWithCapacity:1];
            
            for (NSString *imgUrl in imagesUrls)
            {
                NSString* filePath = [imgUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                filePath = [filePath stringByAppendingString:[NSString stringWithFormat:@"?token=%@&resolution=80&quality=80", currentUser.token]];
                
                [tempImagesUrls addObject:filePath];
            }
            
            ipvc.imagesUrls = tempImagesUrls;
        }
        
        [self.navigationController pushViewController:ipvc animated:YES];
    }];
}

#pragma mark - Set left margin of table cells to ziro for iOS 8

-(void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

@end
