//
//  MaterialTableViewCell.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 2/6/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MaterialTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblMaterialName;
@property (weak, nonatomic) IBOutlet UIImageView *imgPDF;
@property (weak, nonatomic) IBOutlet UIView *viewLine;

@end
