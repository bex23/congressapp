//
//  MaterialsListViewController.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 2/6/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MaterialsListViewController : UIViewController

@property (strong, nonatomic) NSArray *arrayMaterials;
@property (strong, nonatomic) NSURL *streamingURL;

@end
