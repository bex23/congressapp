//
//  WebcastViewController.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 9/29/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebcastViewController : UIViewController

@property (assign, nonatomic) BOOL hasRefreshButton;
@property (strong, nonatomic) Material *material;

@end
