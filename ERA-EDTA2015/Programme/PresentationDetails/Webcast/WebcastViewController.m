//
//  WebcastViewController.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 9/29/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "WebcastViewController.h"

@interface WebcastViewController () <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activator;

@property (strong, nonatomic) NSString *url;

@property (strong, nonatomic) NSTimer *timer;
@property (strong, nonatomic) NSString *sessionID;

@end

@implementation WebcastViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (_hasRefreshButton == YES)
    {
        UIBarButtonItem *button = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refresh:)];
        self.navigationItem.rightBarButtonItem = button;
    }
    
    self.url = self.material.matFileUrl.length > 0 ? self.material.matFileUrl : @"";
    
    self.title = self.material.matName;
    
    [self.webView setAllowsInlineMediaPlayback:YES];
    self.webView.mediaPlaybackRequiresUserAction = NO;
    
    [self startSessionPreviewOfMaterial];
}

- (void)refresh:(id)sender
{
    [self.webView reload];
    
    [self.activator startAnimating];
}

-(void)startSessionPreviewOfMaterial
{
    self.sessionID = [NSString stringWithFormat:@"ios_%@_%@", self.material.matMaterialId, @([[NSDate date] timeIntervalSince1970])];
    
    [[NSUserDefaults standardUserDefaults] setObject:@{@"active" : @"YES", @"sessionId" : self.sessionID} forKey:kKeyActiveMaterialBrowsingSession];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [MaterialBrowsing insertMaterialBrowsingSession:self.sessionID forMaterial:self.material];
    
    [self.timer invalidate];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(recordMaterialBrowsing) userInfo:nil repeats:YES];
}

-(void)recordMaterialBrowsing
{
    NSDictionary *dictSessin = [[NSUserDefaults standardUserDefaults] objectForKey:kKeyActiveMaterialBrowsingSession];
    if (dictSessin && [[dictSessin objectForKey:@"active"] isEqualToString:@"YES"])
    {
        [MaterialBrowsing recordMaterialBrowsingForSession:self.sessionID];
    }
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [self.timer invalidate];
    
    [MaterialBrowsing recordMaterialBrowsingForSession:self.sessionID];
    [[ESyncData sharedInstance] syncMaterialBrowsings];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kKeyActiveMaterialBrowsingSession];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.url]];
    
    [self.webView loadRequest:request];
    [self.activator startAnimating];
}



#pragma mark - UIWebViewDelegate

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self.activator stopAnimating];
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self.activator stopAnimating];
}

@end
