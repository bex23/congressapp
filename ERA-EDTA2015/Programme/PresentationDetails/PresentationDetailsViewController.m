//
//  PresentationDetailsViewController.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 12/13/16.
//  Copyright © 2016 Navus. All rights reserved.
//

#import "PresentationDetailsViewController.h"

#import "IACongressCenterMapViewController.h"
#import "QAViewController.h"
#import "PresentationNotesViewController.h"
#import "PresentationsListViewController.h"
#import "MaterialsListViewController.h"
#import "AbstractViewController.h"
#import "AbstractInfoViewController.h"
#import "ExhibitorMapViewController.h"
#import "IASpeakerInfoViewController.h"
#import "WebcastViewController.h"

@interface PresentationDetailsViewController () <AbstractDelegate, PSSinglePickerViewDelegate, AbstractInfoDelegate>

@property (strong, nonatomic) UIImage* timelineAddImage;
@property (strong, nonatomic) UIImage* timelineRemoveImage;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *viewContent;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintContentViewHeight;

// nav previous - next
@property (weak, nonatomic) IBOutlet UIButton *btnPrevious;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintNavTalksHeight;

@property (weak, nonatomic) IBOutlet UIView *viewDetails;

// speaker
@property (weak, nonatomic) IBOutlet UIImageView *imgSpeaker;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintSpeakerImageWidth;
@property (weak, nonatomic) IBOutlet UILabel *lblSpeakersFullNames;

// session
@property (weak, nonatomic) IBOutlet UIView *viewSessionCatColor;
@property (weak, nonatomic) IBOutlet UILabel *lblSessionTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblSessionNo;
@property (weak, nonatomic) IBOutlet UILabel *lblSessionNumber;



// presentation
@property (weak, nonatomic) IBOutlet UILabel *lblPresentationTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPresentationDate;
@property (weak, nonatomic) IBOutlet UILabel *lblPresentationTime;
@property (weak, nonatomic) IBOutlet UIImageView *imgPresentationPDF;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activator;
@property (weak, nonatomic) IBOutlet UIImageView *imgWebcast;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activatorForWebcast;
@property (weak, nonatomic) IBOutlet UIImageView *imgPosterPDF;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activatorForPosterPDF;
@property (weak, nonatomic) IBOutlet UILabel *lblSessionRoom;
@property (weak, nonatomic) IBOutlet UILabel *lblRoomText;
@property (weak, nonatomic) IBOutlet UILabel *lblLocation;

// bottom items
@property (strong, nonatomic) NSMutableArray *arrayOfUtilsActions;
@property (weak, nonatomic) IBOutlet UIImageView *imgTimeline;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthImgTimeline;
@property (weak, nonatomic) IBOutlet UIImageView *imgVote;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthImgVote;
@property (weak, nonatomic) IBOutlet UIImageView *imgNotes;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthImgNotes;
@property (weak, nonatomic) IBOutlet UIImageView *imgQA;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthImgJoinTheDialogue;

@property (weak, nonatomic) IBOutlet UIImageView *imgPin;

@property (assign, nonatomic) Talk *talk;

@property (nonatomic, retain) WYPopoverController *selectPopoverController;
// abastract button
@property (weak, nonatomic) IBOutlet UIButton *btnAbstract;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintAbstracButtonHeight;

// AVAILABILITY OF ACTIONS
// if user isnt login disable all actions: + show message for registration:
// view materials
// view webcast
// note
//
// check consent -> if hasn't available -> show message depaned on status
//                  else show material

typedef enum : NSUInteger {
    ConsentTypeNoConsent                    = 0,
    ConsentTypeNotAvailableMaterial         = 1, // negative consent by speaker
    ConsentTypeAvailableNotReadyMaterial    = 2,
    ConsentTypeHasMaterial                  = 3,
} ConsentType;

@end

@implementation PresentationDetailsViewController

- (UIImage *)timelineAddImage {
    if (!_timelineAddImage) {
        _timelineAddImage = [self.talk.tlkType isEqualToString:kKeyPoster]
        ? [UIImage imageNamed:@"poster_tag"]
        : [UIImage imageNamed:@"timeline_add_text"];
    } return _timelineAddImage;
}

- (UIImage *)timelineRemoveImage {
    if (!_timelineRemoveImage) {
        _timelineRemoveImage = [self.talk.tlkType isEqualToString:kKeyPoster]
        ? [UIImage imageNamed:@"poster_tagged"]
        : [UIImage imageNamed:@"timeline_remove_text"];
    } return _timelineRemoveImage;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.btnNext setTintColor:[UIColor colorWithHex:kGlobalColor andAlpha:1.0]];
    [self.btnPrevious setTintColor:[UIColor colorWithHex:kGlobalColor andAlpha:1.0]];
    
    [self setupTalkNavigation];
    
    // add shadow to view
    [self.viewDetails.layer setShadowColor:[UIColor lightGrayColor].CGColor];
    [self.viewDetails.layer setShadowOpacity:0.8];
    [self.viewDetails.layer setShadowRadius:3.0];
    [self.viewDetails.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [self.imgPin colorItWithColor:[UIColor colorWithHex:kGlobalColor andAlpha:1.0]];
    
    // if user logged in - getOrders
    if ([User current])
    {
        [[ESyncData sharedInstance] getOrdersWithCompletitionBlock:^{
            self.talk = [Talk getTalkWithId:self.talk.tlkTalkId];
        }];
    }
    
    [self setupUtilsActions];
    [[ESyncData sharedInstance] trackUserAction:kTrackUserActionBannerShown
                                      forBanner:self.talk.tlkTalkId
                                     objectType:@"Presentation"
                                     objectInfo:@"ios"];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupPage];
}

-(void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    _imgSpeaker.layer.cornerRadius = _imgSpeaker.frame.size.width / 2;
    _imgSpeaker.clipsToBounds = YES;
}



#pragma mark - Setups

-(void)setupTalkNavigation
{
    if(_currentTalkIndex == 0)
    {
        _btnPrevious.hidden = YES;
    }
    
    if (_arrayTalks.count == 1)
    {
        _btnNext.hidden = YES;
        _constraintNavTalksHeight.constant = 0;
    }
    
    if (_arrayTalks.count - 1 == _currentTalkIndex) {
        _btnNext.hidden = YES;
    }
    
    if(_arrayTalks.count > 0)
    {
        self.talk = _arrayTalks[_currentTalkIndex];
    }
}

-(void)setupUtilsActions
{
    self.arrayOfUtilsActions = [[NSMutableArray alloc] initWithObjects:self.imgTimeline, self.imgVote, self.imgNotes, self.imgQA, nil];
    if (!kJoinTheDialogueExist)
    {
        [self.arrayOfUtilsActions removeObject:self.imgQA];
    }
    
    if (!kNotesExist)
    {
        [self.arrayOfUtilsActions removeObject:self.imgNotes];
    }
    
    if (!kJoinTheDialogueExist)
    {
        self.imgQA.hidden = YES;
        self.widthImgJoinTheDialogue.constant = 0;
    }
    else
    {
        self.widthImgJoinTheDialogue.constant = self.view.bounds.size.width / self.arrayOfUtilsActions.count;
    }
    
    if (!kNotesExist)
    {
        self.imgNotes.hidden = YES;
        self.widthImgNotes.constant = 0;
    }
    else
    {
        self.widthImgNotes.constant = self.view.bounds.size.width / self.arrayOfUtilsActions.count;
    }
    
    self.widthImgTimeline.constant = self.view.bounds.size.width / self.arrayOfUtilsActions.count;
    self.widthImgVote.constant = self.view.bounds.size.width / self.arrayOfUtilsActions.count;
}

-(void)setupPage
{
    [self setupSpeakers];
    [self setupPresentationTitleAndTime];
    
    if ([self.talk.tlkType isEqualToString:kKeyPoster])
    {
        self.lblLocation.text = _talk.session.venue.venName;
        self.lblRoomText.text = @"Board no:";
        self.lblSessionRoom.text = _talk.tlkBoardNo;
        self.title = @"Poster Details";
    }
    else
    {
        self.lblLocation.text = @"";
        self.lblRoomText.text = @"Room:";
        self.lblSessionRoom.text = _talk.session.venue.venName;
        self.title = @"Presentation Details";
    }
    
    // setup abstract
    if (_talk.tlkDescription.length == 0)
    {
        self.constraintAbstracButtonHeight.constant = 0;
        self.btnAbstract.hidden = YES;
    }
    else
    {
        self.constraintAbstracButtonHeight.constant = 60;
        self.btnAbstract.hidden = NO;
    }
    
    [self setupSession];
    
    // STEPS FOR setupMaterialsStatus function
    // if consent_completed
    //          if available main
    //              if has webcast
    //                  material image tag = 3
    //              else
    //                  material image tag = 2
    //          else
    //              material image tag = 1
    //
    //          if available webcast
    //              if has webcast
    //                  webcast image tag = 3
    //              else
    //                  webcast image tag = 2
    //          else
    //              webcast image tag = 1
    //
    // else
    //      material and webcast tag = 0
    [self setupMaterialsStatus];
    
    // Timeline, QA and Vote
    [self setupTimelineQaAndVote];
    
    // Notes
    if ([User current] && kNotesExist == YES)
    {
        [self syncNotes];
    }
    
    // setup height for content view; it depends on other elements so it has to be execute on the end
    [self setupContentViewHeight];
}

-(void)setupSpeakers
{
    [self.imgSpeaker layoutIfNeeded];
    
    UIImage* img = [UIImage imageNamed:@"speaker-placeholder"];
    NSURL* speakerImgURL = nil;
    
    if(_talk.tlkSpeakerImageUrl)
    {
        NSString* request = [_talk.tlkSpeakerImageUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        speakerImgURL = [NSURL URLWithString:request];
    }
    
    [self.imgSpeaker setImageWithURL:speakerImgURL placeholderImage:img];
    
    if (!speakerImgURL)
    {
        self.constraintSpeakerImageWidth.constant = 0;
    }
    else
    {
        self.constraintSpeakerImageWidth.constant = 90;
    }
    
    self.lblSpeakersFullNames.text = _talk.tlkSpeakerName;
}

-(void)setupPresentationTitleAndTime
{
    self.lblPresentationTitle.text = _talk.tlkTitle;
    
    self.lblPresentationDate.text = (_talk.tlkStartTime.length > 0) ? [NSString stringWithFormat:@"%@,", [Utils formatDateString:[Utils getDate:_talk.tlkStartTime]]] : nil;
    
    if (_talk.tlkStartTime.length == 0 && _talk.tlkEndTime.length == 0)
    {
        self.lblPresentationTime.text = @"";
    }
    else
    {
        self.lblPresentationTime.text = [NSString stringWithFormat:@"%@ - %@", [Utils formatTimeStringFromDate:_talk.tlkStartTime], [Utils formatTimeStringFromDate:_talk.tlkEndTime]];
    }
    
    if ([_talk.tlkType isEqualToString:kKeyPoster])
    {
        if (_talk.session.sesStartTime.length != 0 && _talk.session.sesEndTime.length != 0)
        {
            self.lblPresentationTime.text = [NSString stringWithFormat:@"%@ - %@", [Utils formatTimeStringFromDate:_talk.session.sesStartTime], [Utils formatTimeStringFromDate:_talk.session.sesEndTime]];
        }
    }
}

-(void)setupSession
{
    NSString *trimmedSessionNo = [_talk.session.sesCode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    self.lblSessionNumber.text = trimmedSessionNo;
    
    _lblSessionNo.text = (trimmedSessionNo.length > 0) ? @"Session No:" : nil;
    
    self.lblSessionTitle.text = _talk.session.sesName;
    
    self.viewSessionCatColor.backgroundColor = [SessionCategory getColorForSessionCategoryId:_talk.session.sesCategoryId];
}

-(void)setupMaterialsStatus
{
    _imgWebcast.hidden = _imgPresentationPDF.hidden = ([_talk.tlkType isEqualToString:kKeyPoster]);
    self.imgPosterPDF.hidden = !([_talk.tlkType isEqualToString:kKeyPoster]);
    
    if (_talk.tlkConsentCompleted.integerValue == 1)
    {
        // check AVAILABLE MAIN MATERIAL -> change materials image and getPresentation button status
        if (self.talk.tlkAvailableMainMaterial.integerValue == 1 || self.talk.tlkAvailableSupportingMaterial.integerValue == 1)
        {
            // check HAS MAIN MATERIAL -> change materials image status
            if (self.talk.tlkHasMainMaterial.integerValue == 1 || self.talk.tlkHasSupportingMaterial.integerValue == 1)
            {
                // set status of material and poster image
                [_imgPresentationPDF setImage:[UIImage imageNamed:@"material_pdf_available"]];
                [self.imgPosterPDF setImage:[self.talk.tlkHasAudioMaterial boolValue]
                                           ?[UIImage imageNamed:@"material-poster-with-audio"]
                                           :[UIImage imageNamed:@"poster_pdf_available"]];
                _imgPresentationPDF.tag = self.imgPosterPDF.tag = ConsentTypeHasMaterial;
            }
            else // tlkHasMainMaterial == 0
            {
                // set status of material and poster image
                [_imgPresentationPDF setImage:[UIImage imageNamed:@"material_pdf"]];
                [self.imgPosterPDF setImage:[UIImage imageNamed:@"poster_pdf"]];
                
                _imgPresentationPDF.tag = self.imgPosterPDF.tag = ConsentTypeAvailableNotReadyMaterial;
            }
        }
        else // tlkAvailableMainMaterial == 0
        {
            // set status of material and poster image
            [_imgPresentationPDF setImage:[UIImage imageNamed:@"material_pdf"]];
            [self.imgPosterPDF setImage:[UIImage imageNamed:@"poster_pdf"]];
            
            _imgPresentationPDF.tag = self.imgPosterPDF.tag = ConsentTypeNotAvailableMaterial;
        }
        ////////////////////////////////////////////////////////////////////
        
        // check AVAILABLE WEBCAST MATERIAL -> change materials image
        if (self.talk.tlkAvailableWebcastMaterial.integerValue == 1)
        {
            // check HAS WEBCAST MATERIAL -> change materials image status
            if (self.talk.tlkHasWebcastMaterial.integerValue == 1)
            {
                [_imgWebcast setImage:[UIImage imageNamed:@"webcast_available"]];
                _imgWebcast.tag = ConsentTypeHasMaterial;
            }
            else // tlkHasWebcastMaterial == 0
            {
                [_imgWebcast setImage:[UIImage imageNamed:@"webcast"]];
                _imgWebcast.tag = ConsentTypeAvailableNotReadyMaterial;
            }
        }
        else // tlkAvailableWebcastMaterial == 0
        {
            [_imgWebcast setImage:[UIImage imageNamed:@"webcast"]];
            _imgWebcast.tag = ConsentTypeNotAvailableMaterial;
        }
        ////////////////////////////////////////////////////////////////////
    }
    else // tlkConsentCompleted == 0
    {
        // disable material and webcast and poster images
        [_imgWebcast setImage:[UIImage imageNamed:@"webcast"]];
        [_imgPresentationPDF setImage:[UIImage imageNamed:@"material_pdf"]];
        [self.imgPosterPDF setImage:[UIImage imageNamed:@"poster_pdf"]];

        _imgWebcast.tag = _imgPresentationPDF.tag = self.imgPosterPDF.tag = ConsentTypeNoConsent;
    }
}

-(void)setupTimelineQaAndVote
{
    // timeline
    if (_talk.zetBookmarked.boolValue == YES)
    {
        [_imgTimeline setImage:self.timelineRemoveImage];
    }
    else
    {
        [_imgTimeline setImage:self.timelineAddImage];
    }
    
    _imgVote.userInteractionEnabled = _talk.tlkVotingEnabled.boolValue;
    _imgVote.alpha = _talk.tlkVotingEnabled.boolValue ? 1.0 : 0.3;
    
    // check if user is speaker on this presenation -> disable voting for him
    User *currentUser = [User current];
    if (currentUser)
    {
        NSSet *speakers = [self.talk.speakrs filteredSetUsingPredicate:[NSPredicate predicateWithFormat:@"spkUserId == %@", @(currentUser.id)]];
        if (speakers.count > 0)
        {
            _imgVote.hidden = YES;
        }
    }
    
    if ([_talk.tlkType isEqualToString:kKeyPoster])
    {
        _imgVote.hidden = _imgQA.hidden = _imgNotes.hidden = YES;
    }
}

-(void)syncNotes
{
    [[ESyncData sharedInstance] uploadNotes];
    [[ESyncData sharedInstance] getNotesForPresentation:self.talk];
}

-(void)setupContentViewHeight
{
    // to get correct height of viewDetails with self.viewDetails.frame.size.height
    [self.viewDetails setNeedsLayout];
    [self.viewDetails layoutIfNeeded];
    
    //    NSLog(@"viewDetails heigth %f", self.viewDetails.frame.size.height);
    //    NSLog(@"imgPresentationPDF heigth %f", self.imgPresentationPDF.frame.size.height);
    
    self.constraintContentViewHeight.constant = self.viewDetails.frame.size.height + self.imgPresentationPDF.frame.size.height + 60;
}

#pragma mark - Actions

- (IBAction)actionAbstract:(UIButton *)sender
{
    if (![User current]) {
        [CUtils showLoginQuestion];
    } else {
        [[ESyncData sharedInstance] trackUserAction:kTrackUserActionBannerClick
                                          forBanner:self.talk.tlkTalkId
                                         objectType:@"Abstract"
                                         objectInfo:@"ios"];
        [self showAbstract];
    }
}

-(void)showAbstract
{
    [self.view setUserInteractionEnabled:NO];
    
    NSString *strID = NSStringFromClass([AbstractViewController class]);
    
    AbstractViewController *vc = [[UIStoryboard storyboardWithName:strID bundle:nil] instantiateViewControllerWithIdentifier:strID];
    
    vc.delegate = self;
    
    vc.abstractText = _talk.tlkDescription;
    
    vc.preferredContentSize = CGSizeMake(self.view.frame.size.width - 40, vc.view.frame.size.height - 80);
    
    self.selectPopoverController = [[WYPopoverController alloc] initWithContentViewController:vc];
    
    [self.selectPopoverController presentPopoverFromRect:self.view.bounds inView:self.view permittedArrowDirections:WYPopoverArrowDirectionNone animated:YES];
}

-(void)showInfoAbstractPopup
{
    AbstractInfoViewController *vc = [[UIStoryboard storyboardWithName:@"AbstractViewController" bundle:nil] instantiateViewControllerWithIdentifier:@"AbstractInfoViewController"];
    
    vc.delegate = self;
    
    vc.preferredContentSize = CGSizeMake(self.view.frame.size.width - 20, vc.view.frame.size.height - 40);
    
    self.selectPopoverController = [[WYPopoverController alloc] initWithContentViewController:vc];
    
    [self.selectPopoverController presentPopoverFromRect:self.view.bounds inView:self.view permittedArrowDirections:WYPopoverArrowDirectionNone animated:YES];
}

- (IBAction)actionNext:(UIButton *)sender
{
    NSUInteger newIndex = _currentTalkIndex + 1;
    
    if (_arrayTalks.count > 1) {
        _btnPrevious.hidden = NO;
    }
    
    if (_arrayTalks[newIndex])
    {
        self.currentTalkIndex = newIndex;
        self.talk = _arrayTalks[newIndex];
        
        [self setupPage];
        
        if (_arrayTalks.count - 1 == newIndex)
        {
            _btnNext.hidden = YES;
        }
    }
}

- (IBAction)actionPrevious:(UIButton *)sender
{
    NSUInteger newIndex = _currentTalkIndex - 1;
    
    if (_arrayTalks.count > 1) {
        _btnNext.hidden = NO;
    }
    
    if (_arrayTalks[newIndex])
    {
        self.currentTalkIndex = newIndex;
        self.talk = _arrayTalks[newIndex];
        
        [self setupPage];
        
        if (newIndex == 0)
        {
            _btnPrevious.hidden = YES;
        }
    }
}

- (IBAction)actionNote:(UITapGestureRecognizer *)sender
{
    if (![User current])
    {
        [CUtils showLoginQuestion];
        return;
    }

    PresentationNotesViewController *vc = [[UIStoryboard storyboardWithName:@"Notes" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([PresentationNotesViewController class])];

    vc.talk = self.talk;

    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)actionViewWebcast:(UITapGestureRecognizer *)sender
{
    UIView* view = sender.view;
    CGPoint loc = [sender locationInView:view];
    UIView* subview = [view hitTest:loc withEvent:nil];
    
    if(subview.tag != ConsentTypeHasMaterial)
    {
        [self showMessageByConsentType:subview.tag];
        return;
    }
    
    User *currentUser = [User current];
    // if user is not logined in
    if (!currentUser)
    {
        [CUtils showLoginQuestion];
        return;
    }
    
    if(!currentUser.hasBadge) // it is important to requiredLogin is after requiredBadge
    {
        [self showRequiredBadgeMessage];
        return;
    }
    
    [self activatorStart];
    
    [[ESyncData sharedInstance] getMaterialsForPresentation:self.talk.tlkTalkId WithCompletitionBlock:^{
        // refresh talk property, because there is maybe update materials
        self.talk = [Talk getTalkWithId:self.talk.tlkTalkId];
        
        [self webcastIt];
        
        [self activatorStop];
    }];
}

-(void)webcastIt
{
    User *currentUser = [User current];
    
    // -> order -> download -> show
    if ([Order doesExistOrderWithTalkId:self.talk.tlkTalkId])
    {
        [self activatorStop];
        [self showWebcast];
    }
    else
    {
        if(currentUser.isMember)
        {
            [[ESyncData sharedInstance] orderderTalk:self.talk success:^{
                [self activatorStop];
                [self showWebcast];
            } failure:^{
                [self activatorStop];
            }];
        }
        else
        {
            [[ESyncData sharedInstance] getPresentationsCost:self.talk.tlkTalkId
                                                 withSuccess:^(int cost)
             {
                 // get epoints for gain message
                 [[ESyncData sharedInstance] getEpointsWithSuccess:^(int epoints) {
                     
                     [self activatorStop];
                     [self showGainAccessMessageForEpoints:epoints forTalk:self.talk withCost:cost];
                     
                 } failure:^{
                     [self activatorStop];
                 }];
             } failure:^{
                 [self activatorStop];
             }];
        }
    }
}

-(void)showGainAccessMessageForEpoints:(int)epoints forTalk:(Talk *)talk withCost:(int)cost
{
    if(cost == 0)
    {
        [[ESyncData sharedInstance] orderderTalk:talk success:^{
            [self showWebcast];
        } failure:^{
        }];
    }
    else
    {
        NSString *epointsString = [NSString stringWithFormat:@"%d", epoints];
        NSString *costString = [NSString stringWithFormat:@"%d", cost];
        
        UIAlertController *alert;
        if(epoints == 0)
        {
            alert = [UIAlertController
                     alertControllerWithTitle:@""
                     message: @"You currently have no further access points left for this related event. Please contact, support@e-materials.com to request access to this presentation."
                     preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* btnContactUs = [UIAlertAction actionWithTitle:@"Contact Us"
                                                                   style:UIAlertActionStyleDefault
                                                                 handler:^(UIAlertAction * action) {
                                                                     
                                                                     NSString *recipients = @"mailto:support@e-materials.com";
                                                                     
                                                                     NSString *email = recipients;
                                                                     
                                                                     email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                                                                     
                                                                     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
                                                                 }];
            [alert addAction:btnContactUs];
        }
        else
        {
            alert = [UIAlertController
                     alertControllerWithTitle:@""
                     message: [NSString stringWithFormat:@"Gaining access to \n%@\n will cost you %@ access point.\nYou currently have %@ access %@ left for this related event.\nDo you want to access this presentation?", talk.tlkTitle, costString, epointsString, epoints > 1 ? @"points" : @"point"]
                     preferredStyle:UIAlertControllerStyleAlert];
            
            
            UIAlertAction* btnRegister = [UIAlertAction actionWithTitle:@"GAIN ACCESS"
                                                                  style:UIAlertActionStyleDefault
                                                                handler:^(UIAlertAction * action) {
                                                                    
                                                                    [[ESyncData sharedInstance] orderderTalk:talk success:^{
                                                                        [self showWebcast];
                                                                    } failure:^{
                                                                    }];
                                                                }];
            [alert addAction:btnRegister];
        }
        
        UIAlertAction* btnCancel = [UIAlertAction actionWithTitle:@"CANCEL"
                                                            style:UIAlertActionStyleDefault
                                                          handler:nil];
        
        [alert addAction:btnCancel];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}


-(void)showRequiredBadgeMessage
{
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:nil
                                message:@"To gain access to E-materials you need to insert the badge number."
                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* btnInsert = [UIAlertAction actionWithTitle:@"Insert"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action) {
                                                          UINavigationController *nav = [[UIStoryboard storyboardWithName:@"EnterBadgeViewController" bundle:nil] instantiateViewControllerWithIdentifier:@"navEnterBadgeViewController"];
                                                          [self.navigationController presentViewController:nav animated:YES completion:nil];
                                                      }];
    
    UIAlertAction* btnCancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
    
    [alert addAction:btnCancel];
    [alert addAction:btnInsert];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)showWebcast
{
    NSArray *arrayMat = [_talk.materials.allObjects filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"matType == 'webcast'"]];
    
    if (arrayMat.count == 0)
    {
        if (![[NSUserDefaults standardUserDefaults] valueForKey:kTypeOfInternetConnection])
        {
            [CUtils showOfflineMessage];
        }
        return;
    }
    
    Material *matWebcast = [arrayMat firstObject];
    
    WebcastViewController *webcastVC = [[WebcastViewController alloc] initWithNibName:@"WebcastViewController" bundle:nil];
    webcastVC.hasRefreshButton = YES;
    [webcastVC setMaterial:matWebcast];
    
    [self.navigationController pushViewController:webcastVC animated:YES];
}

-(void)activatorStart
{
    self.view.userInteractionEnabled = NO;
    [self.activatorForWebcast startAnimating];
}

-(void)activatorStop
{
    self.view.userInteractionEnabled = YES;
    [self.activatorForWebcast stopAnimating];
}

- (IBAction)actionViewPDF:(UITapGestureRecognizer *)sender
{
    UIView* view = sender.view;
    CGPoint loc = [sender locationInView:view];
    UIView* subview = [view hitTest:loc withEvent:nil];
    
    if(subview.tag != ConsentTypeHasMaterial)
    {
        [self showMessageByConsentType:subview.tag];
        return;
    }
    
    User *currentUser = [User current];
    // if user is not logined in
    if (!currentUser)
    {
        [CUtils showLoginQuestion];
        return;
    }
    
    if(!currentUser.hasBadge) // it is important to requiredLogin is after requiredBadge
    {
        [self showRequiredBadgeMessage];
        return;
    }
    
    [self addLoader];
    [[ESyncData sharedInstance] getMaterialsForPresentation:self.talk.tlkTalkId
                                      WithCompletitionBlock:^{
                                          [self removeLoaderIfExists];
                                          // refresh talk property, because there is maybe update materials
                                          self.talk = [Talk getTalkWithId:self.talk.tlkTalkId];
                                          
                                          if (([self.talk.tlkType isEqualToString:kKeyPoster]))
                                          {
                                              self.imgPosterPDF.userInteractionEnabled = YES;
                                              [self.activatorForPosterPDF stopAnimating];
                                          }
                                          else
                                          {
                                              self.imgPresentationPDF.userInteractionEnabled = YES;
                                              [self.activator stopAnimating];
                                          }
                                          
                                          [self showMaterialsForTalk:self.talk];
                                      }];
}

-(void)showMessageByConsentType:(ConsentType)type
{
    switch (type) {
        case ConsentTypeNoConsent:
            [Utils showMessage:@"Waiting for the speaker’s consent."];
            break;
        case ConsentTypeNotAvailableMaterial:
            [Utils showMessage:@"Material not available due to non consent of speaker."];
            break;
        case ConsentTypeAvailableNotReadyMaterial:
            [Utils showMessage:@"The media file(s) of this presentation will be available soon."];
            break;
        default:
            break;
    }
}

-(void)showMaterialsForTalk:(Talk *)talk
{
    NSArray *arrayOfMat = [self.talk.materials.allObjects filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"matType != 'webcast' AND matType != 'html' AND matType != 'audio'"]];
    
    if (arrayOfMat.count == 0)
    {
        if (![[NSUserDefaults standardUserDefaults] valueForKey:kTypeOfInternetConnection])
        {
            [CUtils showOfflineMessage];
        }
        
        return;
    }
    else
    {
        MaterialsListViewController *vc = [[UIStoryboard storyboardWithName:NSStringFromClass([MaterialsListViewController class]) bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([MaterialsListViewController class])];
        
        vc.arrayMaterials = arrayOfMat;
        for (Material *mat in self.talk.materials) {
            if ([mat.matType isEqualToString:@"audio"]) {
                vc.streamingURL = [NSURL URLWithString:mat.matFileUrl];
                break;
            }
        }
        
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (IBAction)actionGoToMap:(id)sender
{
    if (kEnableCongressMapPinning == NO)
    {
        return;
    }
    
    if (self.talk.session.venue && self.talk.session.venue.venCoordX.doubleValue != 0.0 && self.talk.session.venue.venCoordY.doubleValue != 0.0)
    {
        IACongressCenterMapViewController* vc = [[UIStoryboard storyboardWithName:@"CongressCenterMap" bundle:nil] instantiateViewControllerWithIdentifier:@"CongressCenterMap"];
        vc.roomLevel = [self.talk.session.venue.venFloor stringValue];
        vc.strCoordX = self.talk.session.venue.venCoordX;
        vc.strCoordY = self.talk.session.venue.venCoordY;
        
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (IBAction)actionTimeline:(UITapGestureRecognizer *)sender
{
    if (_talk.zetBookmarked.boolValue == YES)
    {
        [_imgTimeline setImage:self.timelineAddImage];
        
        self.talk.zetBookmarked = [NSNumber numberWithBool:NO];
        
        [Talk updateTimeline:self.talk isAddition:NO];
    }
    else
    {
        [_imgTimeline setImage:self.timelineRemoveImage];
        
        self.talk.zetBookmarked = [NSNumber numberWithBool:YES];
        
        [Talk updateTimeline:self.talk isAddition:YES];
    }
    
    NSError *error;
    
    [[LLDataAccessLayer sharedInstance].managedObjectContext save:&error];
    
    _arrayTalks[_currentTalkIndex] = _talk;
}

- (IBAction)actonQA:(UITapGestureRecognizer *)sender
{
    if (![[NSUserDefaults standardUserDefaults] valueForKey:kTypeOfInternetConnection])
    {
        [CUtils showOfflineMessage];
        return;
    }
    
    User *currentUser = [User current];
    // if user is not logined in
    if (!currentUser || ![[NSUserDefaults standardUserDefaults] valueForKey:kUserToken])
    {
        [CUtils showLoginQuestion];
        return;
    }
    
    // Create NSData object
    NSData *nsdata = [[[NSUserDefaults standardUserDefaults] valueForKey:kUserToken]
                      dataUsingEncoding:NSUTF8StringEncoding];
    
    // Get NSString from NSData object in Base64
    NSString *base64Encoded = [nsdata base64EncodedStringWithOptions:0];
    NSString *base = [[RealmRepository shared] streamingBaseURL];

    NSNumber *blockId = _talk.session.sesSessionId;
    NSString *strUrl = [NSString stringWithFormat:@"%@block/%@?token=%@#dialogue", base, blockId, base64Encoded];
    
    NSString *strID = NSStringFromClass([WebViewController class]);
    WebViewController *vc = [[UIStoryboard storyboardWithName:strID bundle:nil] instantiateViewControllerWithIdentifier:strID];
    vc.url = strUrl;
    vc.title = @"Join the Dialogue";
    vc.hasRefreshButton = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)actionVote:(UITapGestureRecognizer *)sender
{
    // if user is not logined in
    if (![User current])
    {
        [CUtils showLoginQuestion];
        return;
    }
    VotingSessionViewModel *viewModel = [[VotingSessionViewModel alloc] initWithTalk:self.talk];
    UIViewController *viewController = [VotingSessionViewController controllerWithViewModel:viewModel inNavigation:YES];
    [self presentViewController:viewController animated:YES completion:nil];
}

- (IBAction)actionShowSpeakersProfile:(id)sender
{
    NSArray *arrayOfSpeaker = [_talk.speakrs.allObjects sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"spkFirstName" ascending:YES]]];
    
    if (!arrayOfSpeaker.count)
    {
        return;
    }
    
    if (arrayOfSpeaker.count == 1)
    {
        IASpeakerInfoViewController *vc = [[UIStoryboard storyboardWithName:@"Speakers" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([IASpeakerInfoViewController class])];
        [vc setSpeaker:[arrayOfSpeaker firstObject]];
        
        [self.navigationController pushViewController:vc animated:YES];
        return;
    }
    
    NSMutableArray *speakrsForPicker = [[NSMutableArray alloc] initWithCapacity:10];
    
    for (Speaker *speaker in arrayOfSpeaker)
    {
        [speakrsForPicker addObject:[NSString stringWithFormat:@"%@ %@", speaker.spkFirstName, speaker.spkLastName]];
    }
    
    PSSinglePickerViewController* spVC = [[PSSinglePickerViewController alloc] initWithItems:speakrsForPicker DisplayMember:nil];
    
    spVC.delegate = self;
    spVC.popovertag = @"chooseSpeaker";
    spVC.preferredContentSize = CGSizeMake(self.view.frame.size.width, speakrsForPicker.count * 50);
    spVC.disableTableViewCellSelectionStyle = YES;
    spVC.title = @"Choose speaker:";
    
    self.selectPopoverController = [[WYPopoverController alloc] initWithContentViewController:spVC];
    
    [self.selectPopoverController presentPopoverFromRect:(self.view).bounds inView:(self.view) permittedArrowDirections:WYPopoverArrowDirectionNone animated:YES];
}

#pragma mark - PSSinglePickerViewDelegate

-(void)SinglePickerItemSelected:(id)item Sender:(id)sender
{
    PSSinglePickerViewController *spVC = (PSSinglePickerViewController *)sender;
    
    if ([spVC.popovertag isEqualToString:@"chooseSpeaker"])
    {
        [self.selectPopoverController dismissPopoverAnimated:YES completion:^{
            NSString *strSpeakerFullName = item;
            
            for (Speaker *speaker in _talk.speakrs.allObjects)
            {
                if ([[NSString stringWithFormat:@"%@ %@", speaker.spkFirstName, speaker.spkLastName] isEqualToString:strSpeakerFullName])
                {
                    IASpeakerInfoViewController *vc = [[UIStoryboard storyboardWithName:@"Speakers" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([IASpeakerInfoViewController class])];
                    [vc setSpeaker:speaker];
                    
                    [self.navigationController pushViewController:vc animated:YES];
                    break;
                }
            }
        }];
    }
}

#pragma mark - AbstractDelegate

-(void)abstractCancel
{
    [self.view setUserInteractionEnabled:YES];
    
    [self.selectPopoverController dismissPopoverAnimated:YES];
}

#pragma mark - AbstractInfoDelegate

-(void)abstractInfoCancel
{
    [self.selectPopoverController dismissPopoverAnimated:YES];
}

@end

