//
//  PresentationDetailsViewController.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 12/13/16.
//  Copyright © 2016 Navus. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Talk;

@protocol PresentationDetailsViewControllerDelegate

-(void)didAddPresentationToLibrary;

@end

@interface PresentationDetailsViewController : UIViewController

@property (strong, nonatomic) id<PresentationDetailsViewControllerDelegate>delegate;

@property (assign, nonatomic) NSInteger currentTalkIndex;
@property (strong, nonatomic) NSMutableArray *arrayTalks;

@end
