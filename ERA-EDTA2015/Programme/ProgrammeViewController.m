//
//  ProgrammeViewController.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 12/5/16.
//  Copyright © 2016 Navus. All rights reserved.
//

#import "ProgrammeViewController.h"

#import "AppDelegate.h"

@interface ProgrammeViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *imgScientific;
@property (weak, nonatomic) IBOutlet UIImageView *imgProgramme;
@property (weak, nonatomic) IBOutlet UIImageView *imgPoster;
@property (weak, nonatomic) IBOutlet UIImageView *imgFilter;

@property (strong, nonatomic) NSArray *arrayElements;

@property (weak, nonatomic) IBOutlet UIImageView *imgTopBanner;
@property (weak, nonatomic) IBOutlet UIImageView *imgBottomBanner;

@end

@implementation ProgrammeViewController

#pragma mark - View

- (void)viewDidLoad
{
    [super viewDidLoad];
    

    
    self.arrayElements = @[ @{ @"id" : @"ScientificProgramme", @"element" : _imgScientific},
                            @{ @"id" : @"ProgrammeAtAGlance", @"element" : _imgProgramme},
                            @{ @"id" : @"PosterSessionsViewController", @"element" : _imgPoster},
                            @{ @"id" : @"AdvancedFilter", @"element" : _imgFilter} ];
        
    [[BannersServices shared] addWithImageView:self.imgTopBanner
                                            at:BannerPositionTopBanner
                                   sender: self];
    [[BannersServices shared] addWithImageView:self.imgTopBanner
                                            at:BannerPositionBottomBanner
                                   sender: self];
}


-(void)setBorderToImages
{
    for (NSDictionary *dicElement in _arrayElements)
    {
        UIView *view = [dicElement objectForKey:@"element"];
        view.layer.borderColor = [UIColor colorWithHex:kGlobalColor andAlpha:1.0].CGColor;
        view.layer.borderWidth = 1;
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = NO;
    
    self.view.userInteractionEnabled = YES;
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.landscapeON = NO;
    
    [self supportedInterfaceOrientations];
    
    [self shouldAutorotate];
    
    UIApplication* application = [UIApplication sharedApplication];
    if (application.statusBarOrientation != UIInterfaceOrientationPortrait)
    {
        UIViewController *c = [[UIViewController alloc]init];
        [c.view setBackgroundColor:[UIColor clearColor]];
        [self.navigationController presentViewController:c animated:NO completion:^{
            [self.navigationController dismissViewControllerAnimated:YES completion:^{
            }];
        }];
    }
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

- (BOOL)shouldAutorotate
{
    return YES;
}



#pragma mark - IBActions

- (IBAction)actionTap:(UITapGestureRecognizer *)sender
{
    UIView* view = sender.view;
    CGPoint loc = [sender locationInView:view];
    UIView* subview = [view hitTest:loc withEvent:nil];
    
    self.view.userInteractionEnabled = NO;

    NSString *strID = _arrayElements[subview.tag][@"id"];
    
    if ([strID containsString:kKeyPoster] && kEnablePosters == NO)
    {
        [CUtils showNotAvailableYetMessage];
        self.view.userInteractionEnabled = YES;
        return;
    }
    
    UIViewController *vc = [[UIStoryboard storyboardWithName:strID bundle:nil] instantiateViewControllerWithIdentifier:strID];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

@end
