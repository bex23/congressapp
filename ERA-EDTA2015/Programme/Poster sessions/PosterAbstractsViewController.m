//
//  PosterAbstractsViewController.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 3/21/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "PosterAbstractsViewController.h"

#import "PosterSessionsViewController.h"

@interface PosterAbstractsViewController () <QLPreviewControllerDataSource, QLPreviewControllerDelegate>

@property (strong, nonatomic) NSString *strFilePath;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end

@implementation PosterAbstractsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    

}




#pragma mark - IBActions

- (IBAction)acionClickHere:(id)sender
{
    NSString *fileURL = @"http://media.wix.com/ugd/1f2cb2_ebb16602705347e9bcc6de6d11ee732f.pdf";
    NSString *fileExtension = [[fileURL pathExtension] isEqualToString:@""] ? @"pdf": [fileURL pathExtension]; // material.matFileType;
    NSString *fileName = [[fileURL lastPathComponent] stringByDeletingPathExtension];
    NSString *fileFullName = [NSString stringWithFormat:@"%@.%@", fileName, fileExtension];
    NSString *fileURLString = fileURL;
    
    NSString *libraryPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *filePath = [NSString stringWithFormat:@"%@/%@", libraryPath, @"PosterBoardSchedule"]; //fileId
    
    self.strFilePath = [NSString stringWithFormat:@"%@/%@", filePath, fileFullName];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:[NSString stringWithFormat:@"%@", filePath]]) // If file doesnt exist, create it
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:filePath withIntermediateDirectories:YES attributes:nil error:nil];
    }
    else // If other type of connection, delete file and dowload again
    {
        [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
        [[NSFileManager defaultManager] createDirectoryAtPath:filePath withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    [self.activityIndicator startAnimating];
    
    NSLog(@"Request for file: %@", fileURLString);
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:fileURLString]];
    
    // Requesting for file
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request] ;
    operation.outputStream = [NSOutputStream outputStreamToFileAtPath:[NSString stringWithFormat:@"%@/%@", filePath, fileFullName] append:NO];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"Received file.!!!");
         
         [self.activityIndicator stopAnimating];
         
         [self quickPreview];
     }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"LOCAL URL: %@, error: %@", operation.request.URL,  error);
         
         [self.activityIndicator stopAnimating];
         
         [CUtils showErrorInDownloadingFileMessage];
     }
     ];
    
    [[EWebServiceClient sharedInstance] enqueueHTTPRequestOperation:operation];
}

- (void)quickPreview
{
    //QuickLook
    IAQLPreviewViewController *previewController = [[IAQLPreviewViewController alloc] init];
    previewController.dataSource = self;
    previewController.delegate = self;
    previewController.hidesBottomBarWhenPushed = YES;
    
    // start previewing the document at the current section index
    previewController.currentPreviewItemIndex = 0;
    
    [self.navigationController pushViewController:previewController animated:YES];
}

- (IBAction)actionViewPosters:(id)sender
{
    NSString *strID = NSStringFromClass([PosterSessionsViewController class]);
    UIViewController *vc = [[UIStoryboard storyboardWithName:strID bundle:nil] instantiateViewControllerWithIdentifier:strID];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - QLPreviewControllerDataSource

// Returns the number of items that the preview controller should preview
- (NSInteger)numberOfPreviewItemsInPreviewController:(QLPreviewController *)previewController
{
    NSInteger numToPreview = 1;
    return numToPreview;
}

- (void)previewControllerDidDismiss:(QLPreviewController *)controller
{
    // if the preview dismissed (done button touched), use this method to post-process previews
}

// returns the item that the preview controller should preview
- (id)previewController:(QLPreviewController *)previewController previewItemAtIndex:(NSInteger)idx
{
    NSURL *fileURL = nil;
    fileURL = [NSURL fileURLWithPath:self.strFilePath];
    return fileURL;
}

@end
