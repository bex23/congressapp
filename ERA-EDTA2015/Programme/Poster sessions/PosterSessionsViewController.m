//
//  PosterSessionsViewController.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 2/17/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "PosterSessionsViewController.h"
#import "PosterTableViewCell.h"
#import "IAMyTimelineViewController.h"
#import "SessionDetailsViewController.h"

@interface PosterSessionsViewController ()  <UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate, UISearchResultsUpdating>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSArray *days;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControlDays;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintSegmentedContorlDaysHeight;

@property (strong, nonatomic) UISearchController *searchController;
@property (nonatomic, retain) NSMutableArray *filtered;

@property (strong, nonatomic) NSString *searchString;

@end

@implementation PosterSessionsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    

    
    // Setting SearchController
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    [self.searchController.searchBar sizeToFit];
    self.definesPresentationContext = YES;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.tableView.tableHeaderView = self.searchController.searchBar;
    
    self.managedObjectContext = [LLDataAccessLayer sharedInstance].managedObjectContext;
    
    // fetch to get all seession, without poster and sorted data by sesStartTime and sesName
    [self doFetch];
    
    [self loadData];
    
    // register poster cell
    UINib *nib = [UINib nibWithNibName:NSStringFromClass([PosterTableViewCell class]) bundle:nil];
    [[self tableView] registerNib:nib forCellReuseIdentifier:NSStringFromClass([PosterTableViewCell class])];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 300.0;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"taggedNavigation"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(toTagged)];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.view.userInteractionEnabled = YES;
    
    self.segmentedControlDays.tintAdjustmentMode = UIViewTintAdjustmentModeNormal;
}

#pragma mark - Setups

// setup days for segment control and doFetch after setup
-(void)loadData
{
    // +(NSArray *)getTalks;
    NSMutableArray *array = [NSMutableArray array];
    
    NSArray *sessions = [Session getSessions];
    sessions = [sessions filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"sesType == 'Poster'"]];
    sessions = [sessions filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"talks.@count > 0"]];
    sessions = [sessions sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"sesStartTime" ascending:YES]]];
    
    for (Session *session in sessions)
    {
        if (![array containsObject:[Utils getDate:session.sesStartTime]])
        {
            [array addObject:[Utils getDate:session.sesStartTime]];
        }
    }
    
    self.days = [NSArray arrayWithArray:array];
    
    [_segmentedControlDays removeAllSegments];
    _constraintSegmentedContorlDaysHeight.constant = 0;
    
    if (self.days.count > 0)
    {
        _constraintSegmentedContorlDaysHeight.constant = 45;
        
        int i;
        for(i = 0; i < self.days.count; i++)
        {
            NSString *strDate = [Utils formatDateString:[self.days objectAtIndex:i] fromFormat:kDateDefaultFormat toFormat:kSegmentedControlDateFormat];
            [_segmentedControlDays insertSegmentWithTitle:strDate atIndex:i animated:false];
        }
        
        [_segmentedControlDays setSelectedSegmentIndex:0];
        
        [self doFetch];
    }
    
    [self.segmentedControlDays setTintColor:[UIColor whiteColor]];
}



#pragma mark - FRC

-(NSFetchedResultsController *)fetchedResultsController
{
    if (!_fetchedResultsController)
    {
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"Session" inManagedObjectContext:self.managedObjectContext];
        
        NSMutableArray *predicates = [[NSMutableArray alloc] initWithCapacity:1];
        
        [predicates addObject:[NSPredicate predicateWithFormat:@"sesType == 'Poster'"]];
        
        if(self.days && _segmentedControlDays.selectedSegmentIndex != -1)
        {
            if (self.days.count > 0)
            {
                //RCF restore soon
                NSString *date = [self.days objectAtIndex:_segmentedControlDays.selectedSegmentIndex];
                [predicates addObject:[NSPredicate predicateWithFormat:@"sesStartTime CONTAINS [cd] %@", date]];
            }
        }
        
        if (self.searchString.length)
        {
            NSPredicate* predicateNo = [NSPredicate predicateWithFormat:@"SUBQUERY(talks, $tlk, $tlk.tlkBoardNo CONTAINS[cd] %@).@count>0", self.searchString];
            NSPredicate* predicateTitle = [NSPredicate predicateWithFormat:@"SUBQUERY(talks, $tlk, $tlk.tlkTitle CONTAINS[cd] %@).@count>0", self.searchString];
            NSPredicate *predicateBlokTopic = [NSPredicate predicateWithFormat:@"sesName CONTAINS [cd] %@", self.searchString];
            NSPredicate* chair = [NSPredicate predicateWithFormat:@"sesChairPerson CONTAINS[cd] %@"];
            NSPredicate* name = [NSPredicate predicateWithFormat:@"SUBQUERY(talks, $tlk, $tlk.tlkSpeakerName CONTAINS[cd] %@).@count>0", self.searchString];
            
            NSPredicate *searchPredicate = [NSCompoundPredicate orPredicateWithSubpredicates:@[predicateNo, predicateTitle, predicateBlokTopic, chair, name]];
            [predicates addObject:searchPredicate];
        }
        
        fetchRequest.predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
        [fetchRequest setEntity:entity];
        
        NSSortDescriptor *sortDescriptorTime = [[NSSortDescriptor alloc] initWithKey:@"sesStartTime" ascending:YES];
        NSSortDescriptor *sortDescriptorSessNo = [[NSSortDescriptor alloc] initWithKey:@"sesCode" ascending:YES selector:@selector(localizedStandardCompare:)];
        NSSortDescriptor *sortDescriptorName = [[NSSortDescriptor alloc] initWithKey:@"sesName" ascending:YES selector:@selector(localizedStandardCompare:)];
        NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptorTime, sortDescriptorSessNo, sortDescriptorName, nil];
        
        [fetchRequest setSortDescriptors:sortDescriptors];
        
        NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
        aFetchedResultsController.delegate = self;
        self.fetchedResultsController = aFetchedResultsController;
    }
    return _fetchedResultsController;
}

-(void)doFetch
{
    self.fetchedResultsController = nil;
    
    NSError *error;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"error %@ %@", error, error.userInfo);
        abort();
    }
    
    [self.tableView reloadData];
}

#pragma mark - TableView Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger count = [[_fetchedResultsController sections] count];
    
    if (count == 0)
    {
        count = 1;
    }
    return count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section
{
    int rows = 0;
    
    if ([[_fetchedResultsController sections] count] > 0)
    {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[_fetchedResultsController sections] objectAtIndex:section];
        rows = (int)[sectionInfo numberOfObjects];
    }
    
    return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    PosterTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PosterTableViewCell class]) forIndexPath:indexPath];
    
    Session *session = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    cell.lblPosterTitle.text = session.sesName;
    cell.lblRoom.text = session.venue.venName;
    
    [cell setBoardNo:nil];
    
    [cell setTime:session];
    
    [cell updateConstraintsIfNeeded];
    
    return cell;
}

#pragma mark - TableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    Session *session = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    NSString *strID = NSStringFromClass([SessionDetailsViewController class]);
    SessionDetailsViewController *vc = [[UIStoryboard storyboardWithName:strID bundle:nil] instantiateViewControllerWithIdentifier:strID];
    
    [vc setSession:session];
    
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - IBActions

- (IBAction)actionDayChanged:(UISegmentedControl *)sender
{
    [self doFetch];
}

#pragma mark - UISearchResultUpdating delegate

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    self.searchString = searchController.searchBar.text;
    
    [self doFetch];
}

#pragma mark - Set left margin of table cells to ziro for iOS 8

-(void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - tagged

- (void)toTagged {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Timeline"
                                                         bundle:nil];
    IAMyTimelineViewController *timelineVc = [storyboard instantiateInitialViewController];
    timelineVc.isPoster = YES;
    [self.navigationController pushViewController:timelineVc
                                         animated:YES];
}


@end
