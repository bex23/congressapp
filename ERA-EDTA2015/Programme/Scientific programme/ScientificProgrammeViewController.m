//
//  ScientificProgrammeViewController.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 12/6/16.
//  Copyright © 2016 Navus. All rights reserved.
//

#import "ScientificProgrammeViewController.h"

#import "SessionTableViewCell.h"
#import "SessionDetailsViewController.h"

#import "PSSinglePickerViewController.h"

#import "ScientificProgrammeSearchViewController.h"
#import "IASearchResultsViewController.h"

typedef enum {
    FilterAll,
    FilterByRoom,
    FilterByCategory,
    FilterAdvanced
} FilterBy;

@interface ScientificProgrammeViewController () <UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate, PSSinglePickerViewDelegate, ScientificProgrammeSearchDelegate>

// Filter Value
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLblChoosenValueByFilterHeight;
@property (weak, nonatomic) IBOutlet UILabel *lblChoosenValueByFilter;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSArray *days;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControlDays;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintSegmentedContorlDaysHeight;

@property (nonatomic, retain) WYPopoverController *selectPopoverController;

@property (strong, nonatomic) NSArray *arrayFilterValues;
@property (assign, nonatomic) FilterBy filterBy;

@property (strong, nonatomic) NSArray *arrayAllSesssions;

@property (strong, nonatomic) NSDictionary *dicTempChoosenSessionCategory;

@property (weak, nonatomic) IBOutlet UIImageView *imgSearch;
@property (weak, nonatomic) IBOutlet UIImageView *imgFilter;

@end

@implementation ScientificProgrammeViewController

#pragma mark - View

- (void)viewDidLoad
{
    [super viewDidLoad];
    

    
    _constraintLblChoosenValueByFilterHeight.constant = 0;
    
    self.managedObjectContext = [LLDataAccessLayer sharedInstance].managedObjectContext;
    
    // fetch to get all seession, without poster and sorted data by sesStartTime and sesName
    [self doFetch];
    
    // use for filter - By Room, By Category
    // use for setupDays
    self.arrayAllSesssions = [[self fetchedResultsController] fetchedObjects];
    
    // init filter data
    self.arrayFilterValues = @[@"All", @"By Room", @"By Category", @"Advanced Filter"];
    self.filterBy = FilterAll;
    
    [self loadData];
    
    // register session cell
    UINib *nib = [UINib nibWithNibName:NSStringFromClass([SessionTableViewCell class]) bundle:nil];
    [[self tableView] registerNib:nib forCellReuseIdentifier:NSStringFromClass([SessionTableViewCell class])];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 300.0;
    
    self.imgSearch.layer.borderWidth = 1;
    self.imgSearch.layer.borderColor = [UIColor colorWithHex:kGlobalColor andAlpha:1.0].CGColor;
    
    self.imgFilter.layer.borderWidth = 1;
    self.imgFilter.layer.borderColor = [UIColor colorWithHex:kGlobalColor andAlpha:1.0].CGColor;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.segmentedControlDays.tintAdjustmentMode = UIViewTintAdjustmentModeNormal;
}

#pragma mark - Setups

// setup days for segment control and doFetch after setup
-(void)loadData
{
    NSMutableArray *array = [NSMutableArray array];
    
    for (Session *session in [self arrayOfFilteredSessions])
    {
        if (![array containsObject:[Utils getDate:session.sesStartTime]])
        {
            [array addObject:[Utils getDate:session.sesStartTime]];
        }
    }
    
    self.days = [NSArray arrayWithArray:array];
    
    [_segmentedControlDays removeAllSegments];
    _constraintSegmentedContorlDaysHeight.constant = 0;
    
    if (self.days.count > 0)
    {
        _constraintSegmentedContorlDaysHeight.constant = 45;
        
        int i;
        for(i = 0; i < self.days.count; i++)
        {
            NSString *strDate = [Utils formatDateString:[self.days objectAtIndex:i] fromFormat:kDateDefaultFormat toFormat:kSegmentedControlDateFormat];
            [_segmentedControlDays insertSegmentWithTitle:strDate atIndex:i animated:false];
        }
        
        [_segmentedControlDays setSelectedSegmentIndex:0];
        
        [self doFetch];
    }
    
    [self.segmentedControlDays setTintColor:[UIColor whiteColor]];
}

-(NSArray *)arrayOfFilteredSessions
{
    NSArray *arrayOfFilteredSessions = _arrayAllSesssions;
    
    switch (_filterBy)
    {
        case FilterByRoom:
            arrayOfFilteredSessions = [arrayOfFilteredSessions filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"venue.venName == %@", _lblChoosenValueByFilter.text]];
            break;
        case FilterByCategory:
            arrayOfFilteredSessions = [arrayOfFilteredSessions filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"sesCategoryId == %@", _dicTempChoosenSessionCategory[@"sesSessionCategoryId"]]];
            break;
        default: // All
            break;
    }
    
    return arrayOfFilteredSessions;
}



#pragma mark - FRC

-(NSFetchedResultsController *)fetchedResultsController
{
    if (!_fetchedResultsController)
    {
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"Session" inManagedObjectContext:self.managedObjectContext];
        
        NSMutableArray *predicates = [[NSMutableArray alloc] initWithCapacity:1];
        
        switch (_filterBy)
        {
            case FilterByRoom:
                [predicates addObject:[NSPredicate predicateWithFormat:@"venue.venName = %@", _lblChoosenValueByFilter.text]];
                break;
            case FilterByCategory:
                [predicates addObject:[NSPredicate predicateWithFormat:@"sesCategoryId = %@", _dicTempChoosenSessionCategory[@"sesSessionCategoryId"]]];
                break;
            default: // All
                break;
        }
        
        [predicates addObject:[NSPredicate predicateWithFormat:@"sesType != 'Poster'"]]; // dont show posters
        
        if(self.days && _segmentedControlDays.selectedSegmentIndex != -1)
        {
            if (self.days.count > 0)
            {
                //RCF restore soon
                NSString *date = [self.days objectAtIndex:_segmentedControlDays.selectedSegmentIndex];
                [predicates addObject:[NSPredicate predicateWithFormat:@"sesStartTime CONTAINS [cd] %@", date]];
            }
        }
        
        fetchRequest.predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
        [fetchRequest setEntity:entity];
        
        NSSortDescriptor *sortDescriptorTime = [[NSSortDescriptor alloc] initWithKey:@"sesStartTime" ascending:YES];
        NSSortDescriptor *sortDescriptorName = [[NSSortDescriptor alloc] initWithKey:@"sesName" ascending:YES selector:@selector(localizedStandardCompare:)];
        NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptorTime, sortDescriptorName, nil];
        
        [fetchRequest setSortDescriptors:sortDescriptors];
        
        NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
        aFetchedResultsController.delegate = self;
        self.fetchedResultsController = aFetchedResultsController;
    }
    return _fetchedResultsController;
}

-(void)doFetch
{
    self.fetchedResultsController = nil;
    
    NSError *error;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"error %@ %@", error, error.userInfo);
        abort();
    }
    

    [self.tableView reloadData];
}

#pragma mark - TableView Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger count = [[_fetchedResultsController sections] count];
    
    if (count == 0)
    {
        count = 1;
    }
    return count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section
{
    int rows = 0;
    
    if ([[_fetchedResultsController sections] count] > 0)
    {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[_fetchedResultsController sections] objectAtIndex:section];
        rows = (int)[sectionInfo numberOfObjects];
    }
    
    return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SessionTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([SessionTableViewCell class]) forIndexPath:indexPath];
    
    Session *session = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    cell.lblSessionTitle.text = session.sesName;
    [cell setSessionNo:session.sesCode];
    cell.lblRoomName.text = session.venue.venName;
    
    [cell setTime:session];
    
    cell.viewCategoryColor.backgroundColor = [SessionCategory getColorForSessionCategoryId:session.sesCategoryId];
    
    [cell updateConstraintsIfNeeded];
    
    return cell;
}

#pragma mark - TableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    Session *ses = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    NSString *strID = NSStringFromClass([SessionDetailsViewController class]);
    SessionDetailsViewController* vc = [[UIStoryboard storyboardWithName:strID bundle:nil] instantiateViewControllerWithIdentifier:strID];
    [vc setSession:ses];
    
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Segment Control Delegate

- (IBAction)segmentControlValueChanged:(UISegmentedControl *)sender
{
    //Do something
    [self doFetch];
}

#pragma mark - IBActions

- (IBAction)actionShowFilter:(UITapGestureRecognizer *)sender
{
    UIView* view = sender.view;
    CGPoint loc = [sender locationInView:view];
    UIView* subview = [view hitTest:loc withEvent:nil];
    
    PSSinglePickerViewController* spVC = [[PSSinglePickerViewController alloc] initWithItems:_arrayFilterValues DisplayMember:nil];
    
    spVC.delegate = self;
    spVC.preferredContentSize = CGSizeMake(subview.frame.size.width, _arrayFilterValues.count * 50);
    spVC.popovertag = @"chooseFilterValuePopover";
    spVC.disableTableViewScrollable = YES;
    spVC.backgroudColor = [UIColor colorWithHex:kGlobalColor andAlpha:1.0];
    spVC.textColor = [UIColor whiteColor];
    spVC.tableViewTintColor = [UIColor colorWithHex:kOtherColor andAlpha:1.0];
    
    if (_lblChoosenValueByFilter.text.length)
    {
        spVC.selectedRow = _filterBy;
    }
    
    self.selectPopoverController = [[WYPopoverController alloc] initWithContentViewController:spVC];
    self.selectPopoverController.theme.fillTopColor = [UIColor colorWithHex:kGlobalColor andAlpha:1.0];
    
    [self.selectPopoverController presentPopoverFromRect:(subview).bounds inView:(subview) permittedArrowDirections:WYPopoverArrowDirectionAny animated:YES];
}

- (IBAction)actionShowSearch:(UITapGestureRecognizer *)sender
{
    ScientificProgrammeSearchViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([ScientificProgrammeSearchViewController class])];
    
    vc.delegate = self;
    vc.preferredContentSize = CGSizeMake(self.view.frame.size.width - 40, 200);
    
    self.selectPopoverController = [[WYPopoverController alloc] initWithContentViewController:vc];
    
    [self.selectPopoverController presentPopoverFromRect:(self.view).bounds inView:(self.view) permittedArrowDirections:WYPopoverArrowDirectionNone animated:YES];
}

#pragma mark - ScientificProgrammeSearchDelegate

-(void)scientificProgrammeSearchCancel:(id)sender
{
    [self.selectPopoverController dismissPopoverAnimated:YES];
}

-(void)scientificProgrammeSearch:(id)item Sender:(id)sender
{
    [self.selectPopoverController dismissPopoverAnimated:YES completion:^{
        NSString *strKeywords = (NSString *)item;
        
        // Get reference to the destination view controller
        IASearchResultsViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([IASearchResultsViewController class])];
        NSArray* keyWordsArray = [strKeywords componentsSeparatedByString:@","];
        NSMutableArray* keyWords = [[NSMutableArray alloc] initWithCapacity:3];
        
        for(int i =0; i < keyWordsArray.count && i < 3; i++)
        {
            if ([keyWordsArray objectAtIndex:i] && ((NSString*)[keyWordsArray objectAtIndex:i]).length >0)
            {
                [keyWords addObject:[[keyWordsArray objectAtIndex:i] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
            }
        }
        
        if(keyWords.count < 3)
        {
            for(int i = (int)keyWords.count; i < 3; i++)
            {
                [keyWords addObject:@""];
            }
        }
        
        // Pass any objects to the view controller here, like...
        [vc setKeyWords:keyWords];
        
        [self.navigationController pushViewController:vc animated:YES];
    }];
}

#pragma mark - PSSinglePickerViewDelegate

-(void)SinglePickerItemSelected:(id)item Sender:(id)sender
{
    PSSinglePickerViewController *spVC = (PSSinglePickerViewController *)sender;
    
    if ([spVC.popovertag isEqualToString:@"chooseFilterValuePopover"])
    {
        [self.selectPopoverController dismissPopoverAnimated:YES completion:^{
            
            self.filterBy = (int)[_arrayFilterValues indexOfObject:(NSString *)item];
            switch (_filterBy) {
                case FilterAdvanced: {
                    UIViewController *vc = [[UIStoryboard storyboardWithName:@"AdvancedFilter" bundle:nil] instantiateInitialViewController];
                    [self.navigationController pushViewController:vc animated:YES];
                    break;
                }
                case FilterByRoom:
                    [self filterByRoom];
                    break;
                case FilterByCategory:
                    [self filterBySessionCategories];
                    break;
                default: // All
                    [self clearFilter];
                    break;
            }
            
        }];
    }
    else if ([spVC.popovertag isEqualToString:@"changeRoomPicker"])
    {
        [self.selectPopoverController dismissPopoverAnimated:YES completion:^{
            
            NSDictionary *dic = (NSDictionary *)item;
            [self showFilterValue:dic[@"roomName"]];
            
            [self.navigationController popViewControllerAnimated:YES];
            
        }];
    }
    else if ([spVC.popovertag isEqualToString:@"changeSessionCatagoryPicker"])
    {
        [self.selectPopoverController dismissPopoverAnimated:YES completion:^{
            
            NSDictionary *dic = (NSDictionary *)item;
            _dicTempChoosenSessionCategory = dic;
            [self showFilterValue:dic[@"secName"]];
            
            [self.navigationController popViewControllerAnimated:YES];
            
        }];
    }
}

#pragma mark Filter

-(void)clearFilter
{
    _lblChoosenValueByFilter.text = @"";
    _constraintLblChoosenValueByFilterHeight.constant = 0;
    
    _filterBy = FilterAll;
    
    _dicTempChoosenSessionCategory = nil;
    
    [self loadData];
}

-(void)showFilterValue:(NSString *)value
{
    _lblChoosenValueByFilter.text = value;
    _constraintLblChoosenValueByFilterHeight.constant = 45;
    
    [self loadData];
}

- (void)filterByRoom
{
    NSArray* roomsArray = [self searchForFilterName:@"Rooms"];
    
    int selectedIndex = 1000; // because of deselection
    if (_filterBy == FilterByRoom && _lblChoosenValueByFilter.text.length)
    {
        for(NSDictionary *dic in roomsArray)
        {
            if ([[dic objectForKey:@"roomName"] isEqualToString:_lblChoosenValueByFilter.text]) {
                selectedIndex = (int)[roomsArray indexOfObject:dic];
            }
        }
    }
    
    PSSinglePickerViewController* spVC = [[PSSinglePickerViewController alloc] initWithItems:roomsArray DisplayMember:@"roomName"];
    
    spVC.delegate = self;
    spVC.popovertag = @"changeRoomPicker";
    spVC.textColor = [UIColor colorWithHex:kGlobalColor andAlpha:1.0];
    spVC.tableViewTintColor = [UIColor colorWithHex:kOtherColor andAlpha:1.0];
    
    spVC.selectedRow = selectedIndex;
    
    spVC.title = @"Select Session Room";
    
    [self.navigationController pushViewController:spVC animated:YES];
}

- (void)filterBySessionCategories
{
    NSArray* sessionsArray = [self searchForFilterName:@"SessionCategories"];
    
    int selectedIndex = 1000; // because of deselection
    if (_filterBy == FilterByCategory && _lblChoosenValueByFilter.text.length)
    {
        for(NSDictionary *dic in sessionsArray)
        {
            if ([[dic objectForKey:@"secName"] isEqualToString:_lblChoosenValueByFilter.text]) {
                selectedIndex = (int)[sessionsArray indexOfObject:dic];
            }
        }
    }
    
    PSSinglePickerViewController* spVC = [[PSSinglePickerViewController alloc] initWithItems:sessionsArray DisplayMember:@"secName"];
    spVC.tableViewTintColor = [UIColor colorWithHex:kOtherColor andAlpha:1.0];
    spVC.colorKey = @"sesSessionCategoryId";
    
    spVC.delegate = self;
    spVC.popovertag = @"changeSessionCatagoryPicker";
    
    spVC.selectedRow = selectedIndex;
    
    spVC.title = @"Select Session Category";
    
    [self.navigationController pushViewController:spVC animated:YES];
}

-(NSArray *)searchForFilterName:(NSString *)filterName
{
    NSArray *arrayOfElements = nil;
    if([filterName isEqualToString:@"Rooms"])
    {
        //get grouped room names
        NSMutableSet* roomsSet = [[NSMutableSet alloc] initWithCapacity:3];
        
        for(Session* ses in _arrayAllSesssions)
        {
            if ([ses.venue.venType isEqualToString:@"Room"])
                [roomsSet addObject:ses.venue.venName];
        }
        
        NSMutableArray* sesRooms = [[NSMutableArray alloc] initWithCapacity:3];
        for(NSString* roomName in roomsSet)
        {
            NSDictionary* dict = [NSDictionary dictionaryWithObject:roomName forKey:@"roomName"];
            
            [sesRooms addObject:dict];
        }
        NSArray* sortedRoomsArray = [sesRooms sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"roomName" ascending:YES]]];
        arrayOfElements = [sortedRoomsArray copy];
    }
    else if([filterName isEqualToString:@"SessionCategories"])
    {
        //get grouped room names
        NSMutableSet* secSet = [[NSMutableSet alloc] initWithCapacity:3];
        
        for(Session* ses in _arrayAllSesssions)
        {            
            if(ses.sesCategoryId.intValue != 0) {
                SessionCategory *c = [SessionCategory getSessionCategoryWithId:ses.sesCategoryId];
                if (c != nil) [secSet addObject:c];
            }
        }
        
        NSMutableArray* secNames = [[NSMutableArray alloc] initWithCapacity:3];
        for(SessionCategory* sec in secSet)
        {
            NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:sec.secSessionCategoryId, @"sesSessionCategoryId", sec.secName, @"secName",nil];
            
            [secNames addObject:dict];
        }
        NSArray* sortedSessionCategoriesArray = [secNames sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"secName" ascending:YES]]];
        arrayOfElements = [sortedSessionCategoriesArray copy];
    }
    
    return arrayOfElements;
}

#pragma mark - Set left margin of table cells to ziro for iOS 8

-(void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

@end
