//
//  IASearchResultsViewController.h
//  ERA-EDTA2015
//
//  Created by admin on 1/14/15.
//  Copyright (c) 2015 Navus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IASearchResultsViewController : UIViewController <NSFetchedResultsControllerDelegate, UITableViewDataSource, UITableViewDelegate>

@property (retain, nonatomic) IBOutlet UISegmentedControl *segControl;
@property (strong, nonatomic) IBOutlet UILabel *lblSearchKeyWords;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray* keyWords;
@property (strong, nonatomic) NSFetchedResultsController * fetchedResultsController;

- (IBAction)segControlValueChangedAction:(id)sender;

@end
