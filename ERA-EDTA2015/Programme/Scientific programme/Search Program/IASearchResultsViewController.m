//
//  IASearchResultsViewController.m
//  ERA-EDTA2015
//
//  Created by admin on 1/14/15.
//  Copyright (c) 2015 Navus. All rights reserved.
//

#import "IASearchResultsViewController.h"

#import "SessionDetailsViewController.h"
#import "PresentationDetailsViewController.h"
#import "SessionTableViewCell.h"
#import "PresentationTableViewCell.h"

@interface IASearchResultsViewController () <PresentationTableViewCellDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblCount;

@end

@implementation IASearchResultsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 300.0;
    
    NSMutableString* txt = [NSMutableString new];
    for(NSString* keyWord in _keyWords)
    {
        if(keyWord.length >0)
        {
            [txt appendString:[NSString stringWithFormat:@"%@, ",keyWord]];
        }
    }

    
    self.lblSearchKeyWords.text = txt;
    
    [self doFetch];
    
    [self registerCells];
}

-(void)registerCells
{
    // register session cell
    UINib *nibS = [UINib nibWithNibName:NSStringFromClass([SessionTableViewCell class]) bundle:nil];
    [[self tableView] registerNib:nibS forCellReuseIdentifier:NSStringFromClass([SessionTableViewCell class])];
    
    // register presentation cell
    UINib *nibP = [UINib nibWithNibName:NSStringFromClass([PresentationTableViewCell class]) bundle:nil];
    [[self tableView] registerNib:nibP forCellReuseIdentifier:NSStringFromClass([PresentationTableViewCell class])];
}

#pragma mark tableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    NSInteger count = [[_fetchedResultsController sections] count];
    NSLog(@"Sections NO: %ld", (long)count);
    return count;
}

- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section{
    int rows = 0;
    
    if ([[_fetchedResultsController sections] count] > 0) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[_fetchedResultsController sections] objectAtIndex:section];
        rows = (int)[sectionInfo numberOfObjects];
    }
    
    NSLog(@"How many rows %i", rows);
    return rows;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[_fetchedResultsController sections] objectAtIndex:section];
    
    switch (self.segControl.selectedSegmentIndex)
    {
        case 0:
            
            return [Utils formatDateString:[Utils getDate:((Session *)[[sectionInfo objects]lastObject]).sesStartTime]];
            break;
        case 1:
            return [Utils formatDateString:[Utils getDate:((Talk *)[[sectionInfo objects]lastObject]).tlkStartTime]];
            break;
            
        default:
            break;
    }
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SessionTableViewCell *cell = nil;
    
    Session *session;
    
    Talk* talk;
    
    switch (self.segControl.selectedSegmentIndex)
    {
        case 0:
            
            session = [self.fetchedResultsController objectAtIndexPath:indexPath];
            
            cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([SessionTableViewCell class]) forIndexPath:indexPath];
            
            cell.lblSessionTitle.text = session.sesName;
            [cell setSessionNo:session.sesCode];
            cell.lblRoomName.text = session.venue.venName;
            
            [cell setTime:session];
            
            cell.viewCategoryColor.backgroundColor = [SessionCategory getColorForSessionCategoryId:session.sesCategoryId];
            
            break;
        case 1:
        {
            talk = [self.fetchedResultsController objectAtIndexPath:indexPath];
            
            cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PresentationTableViewCell class]) forIndexPath:indexPath];
            
            ((PresentationTableViewCell*)cell).delegate = self;
            ((PresentationTableViewCell*)cell).tag = indexPath.row;
            
            ((PresentationTableViewCell*)cell).lblTalkTitle.text = talk.tlkTitle;
            ((PresentationTableViewCell*)cell).lblSpeakerFullName.text = talk.tlkSpeakerName;
            
            if (talk.tlkAvailable.integerValue == 1)
            {
                ((PresentationTableViewCell*)cell).imgMaterialAvailabilityIcon.image = [UIImage imageNamed:kEmaterialsAvailable];
                [((PresentationTableViewCell*)cell).imgMaterialAvailabilityIcon colorItWithColor:[UIColor colorWithHex:kGlobalColor andAlpha:1.0]];
            }
            else
            {
                ((PresentationTableViewCell*)cell).imgMaterialAvailabilityIcon.image = [UIImage imageNamed:kEmaterialsUnavailable];
                [((PresentationTableViewCell*)cell).imgMaterialAvailabilityIcon colorItWithColor:[UIColor lightGrayColor]];
            }
            
            ((PresentationTableViewCell*)cell).imgQaIcon.image = [UIImage imageNamed:[talk.tlkVotingEnabled boolValue] ? kVotingAvailable : kVotingUnavailable];
            
            if ([talk.tlkType isEqualToString:kKeyPoster])
            {
                ((PresentationTableViewCell*)cell).imgMaterialAvailabilityIcon.hidden = YES;
                [ ((PresentationTableViewCell*)cell) setBoardNo:talk.tlkBoardNo];
            }
            else
            {
                ((PresentationTableViewCell*)cell).imgMaterialAvailabilityIcon.hidden = NO;
                [((PresentationTableViewCell*)cell) setBoardNo:nil];
            }
            
            if (talk.zetBookmarked.boolValue == YES)
            {
                ((PresentationTableViewCell*)cell).btnAddToTimeline.selected = YES;
            }
            else
            {
                ((PresentationTableViewCell*)cell).btnAddToTimeline.selected = NO;
            }
            
            ((PresentationTableViewCell*)cell).btnAddToTimeline.tag = indexPath.row;
            
            [cell setTime:talk];
            
            break;
        }
        default:
            break;
    }
    
    // because on iOS 8 doesn't work
    // self.tableView.rowHeight = UITableViewAutomaticDimension;
    // self.tableView.estimatedRowHeight = 200.0;
    [cell updateConstraintsIfNeeded];
    
    return cell;
}

// because on iOS 8 doesn't work
// self.tableView.rowHeight = UITableViewAutomaticDimension;
// self.tableView.estimatedRowHeight = 200.0;
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UIViewController* vc = nil;
    
    switch (self.segControl.selectedSegmentIndex)
    {
        case 0:
        {
            Session *session = [self.fetchedResultsController objectAtIndexPath:indexPath];
            NSString *strID = NSStringFromClass([SessionDetailsViewController class]);
            vc = [[UIStoryboard storyboardWithName:strID bundle:nil] instantiateViewControllerWithIdentifier:strID];
            [((SessionDetailsViewController*)vc) setSession:session];
            break;
        }
        case 1:
        {
            NSString *strID = NSStringFromClass([PresentationDetailsViewController class]);
            Talk *talk = [self.fetchedResultsController objectAtIndexPath:indexPath];
            vc = [[UIStoryboard storyboardWithName:strID bundle:nil] instantiateViewControllerWithIdentifier:strID];
            
            NSArray *arrayTalks = [self.fetchedResultsController fetchedObjects];
            ((PresentationDetailsViewController*)vc).arrayTalks = [NSMutableArray arrayWithArray:arrayTalks];
            ((PresentationDetailsViewController*)vc).currentTalkIndex = [arrayTalks indexOfObject:talk];
            break;
        }
        default:
            break;
    }
    if (vc)
    {
        [self.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark FechResultsController

-(void)doFetch{
    self.fetchedResultsController = nil;
    NSError *error;
    if (![self.fetchedResultsController performFetch:&error])
    {
        NSLog(@"error %@ %@", error, error.userInfo);
        abort();
    }
    
    self.lblCount.text = [NSString stringWithFormat:@"%u", (int)self.fetchedResultsController.fetchedObjects.count];
    [self.tableView reloadData];
}

-(NSFetchedResultsController *)fetchedResultsController
{
    if (!_fetchedResultsController) {
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription * entity = nil;
        NSPredicate* oralPredicate = nil;
        NSMutableArray *predicates = [[NSMutableArray alloc] initWithCapacity:3];
        NSArray *sortDescriptors = nil;
        
        switch (self.segControl.selectedSegmentIndex)
        {
            case 0:
            {
                entity = [NSEntityDescription entityForName:@"Session" inManagedObjectContext:[LLDataAccessLayer sharedInstance].managedObjectContext];
                for(NSString* keyWord in _keyWords)
                {
                    if(keyWord.length >0)
                    {
                        
                        NSPredicate* predicate = [NSPredicate predicateWithFormat:@"sesName CONTAINS[cd] %@ OR sesChairPerson CONTAINS[cd] %@ OR SUBQUERY(talks, $tlk, $tlk.tlkSpeakerName CONTAINS[cd] %@).@count>0 OR SUBQUERY(talks, $tlk, $tlk.tlkTitle CONTAINS[cd] %@).@count>0", keyWord, keyWord, keyWord, keyWord];
                        [predicates addObject:predicate];
                    }
                }
                
                oralPredicate = [NSPredicate predicateWithFormat:@"sesType != %@", kKeyPoster];
                
                NSSortDescriptor *sortDescriptorTime = [[NSSortDescriptor alloc] initWithKey:@"sesStartTime" ascending:YES];
                NSSortDescriptor *sortDescriptorName = [[NSSortDescriptor alloc] initWithKey:@"sesName" ascending:YES selector:@selector(localizedStandardCompare:)];
                sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptorTime, sortDescriptorName, nil];
                
                break;
            }
            case 1:
            {
                entity = [NSEntityDescription entityForName:@"Talk" inManagedObjectContext:[LLDataAccessLayer sharedInstance].managedObjectContext];
                for(NSString* keyWord in _keyWords)
                {
                    if(keyWord.length >0)
                    {
                        NSPredicate* predicate = [NSPredicate predicateWithFormat:@"tlkTitle CONTAINS[cd] %@ OR tlkSpeakerName CONTAINS[cd] %@", keyWord, keyWord];
                        [predicates addObject:predicate];
                    }
                }

                oralPredicate = [NSPredicate predicateWithFormat:@"tlkType != %@", kKeyPoster];
                
                NSSortDescriptor *sortDescriptorTime = [[NSSortDescriptor alloc] initWithKey:@"tlkStartTime" ascending:YES];
                NSSortDescriptor *sortDescriptorName = [[NSSortDescriptor alloc] initWithKey:@"tlkTitle" ascending:YES selector:@selector(localizedStandardCompare:)];
                sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptorTime, sortDescriptorName, nil];
                
                break;
            }
            default:
                break;
        }
        
        [fetchRequest setEntity:entity];
        NSPredicate* keyWordsPredicate = [NSCompoundPredicate orPredicateWithSubpredicates:predicates];
        NSArray* wholePredicate = [NSArray arrayWithObjects:keyWordsPredicate, oralPredicate, nil];
        
        fetchRequest.predicate = [NSCompoundPredicate andPredicateWithSubpredicates:wholePredicate];
        
        [fetchRequest setSortDescriptors:sortDescriptors];
        
//        [NSFetchedResultsController deleteCacheWithName:@"SearchCache"];
        NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[LLDataAccessLayer sharedInstance].managedObjectContext sectionNameKeyPath:@"zetDay" cacheName:nil];
        aFetchedResultsController.delegate = self;
        self.fetchedResultsController = aFetchedResultsController;
    }
    return _fetchedResultsController;
}

- (IBAction)segControlValueChangedAction:(id)sender
{
    [self doFetch];
}

#pragma mark

-(void)presentationTableViewCell:(PresentationTableViewCell *)tableCell didTimelinePressed:(UIButton *)sender
{
    Talk *talk = [self.fetchedResultsController.fetchedObjects objectAtIndex:sender.tag];
    
    if (talk.zetBookmarked.boolValue == YES)
    {
        [Talk updateTimeline:talk isAddition:NO];
    }
    else
    {
        [Talk updateTimeline:talk isAddition:YES];
    }
    
    NSError *error;
    
    [[LLDataAccessLayer sharedInstance].managedObjectContext save:&error];
}



#pragma mark - Set left margin of table cells to ziro for iOS 8

-(void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

@end
