//
//  ScientificProgrammeSearchViewController.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 12/8/16.
//  Copyright © 2016 Navus. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ScientificProgrammeSearchDelegate <NSObject>

@optional
- (void)scientificProgrammeSearch:(id)item Sender:(id)sender;
@optional
- (void)scientificProgrammeSearchCancel:(id)sender;

@end

@interface ScientificProgrammeSearchViewController : UIViewController

@property (retain, nonatomic) IBOutlet UITextField *txtKeyWords;

@property (nonatomic, assign) id <ScientificProgrammeSearchDelegate> delegate;

@end
