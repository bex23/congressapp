//
//  ScientificProgrammeSearchViewController.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 12/8/16.
//  Copyright © 2016 Navus. All rights reserved.
//

#import "ScientificProgrammeSearchViewController.h"

@interface ScientificProgrammeSearchViewController ()

@end

@implementation ScientificProgrammeSearchViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

#pragma mark - IBActions

- (IBAction)searchAction:(UIButton *)sender
{
    [self.delegate scientificProgrammeSearch:_txtKeyWords.text Sender:self];
}

- (IBAction)actionCancel:(UIButton *)sender
{
    [self.delegate scientificProgrammeSearchCancel:self];
}

@end
