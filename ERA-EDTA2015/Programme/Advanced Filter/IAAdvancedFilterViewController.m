//
//  IAAdvancedFilterViewController.m
//  ERA-EDTA2015
//
//  Created by admin on 7/17/14.
//  Copyright (c) 2014 Navus. All rights reserved.
//

#import "IAAdvancedFilterViewController.h"
#import "IAAdvancedFilterCell.h"
#import "IAAdvancedFilterDaysCell.h"
#import "PSMultiSelectViewController.h"
#import "IAAdvancedFilterResultsViewController.h"


static NSString *CellIdentifier = @"advancedFilterCell";

@interface IAAdvancedFilterViewController () <PSMultiSelectViewControllerDelegate>

@property (retain, nonatomic) NSArray* daySwitches;
@property (retain, nonatomic) NSMutableDictionary *filterData;
@property (retain, nonatomic) NSMutableDictionary *timeFlags;
@property (retain, nonatomic) NSMutableDictionary *sesTypeFlags;
@property (retain, nonatomic) NSMutableDictionary *sesSessionCategoriesFlags;
@property (retain, nonatomic) NSMutableDictionary *sesSessionNoFlags;
@property (retain, nonatomic) NSMutableDictionary *sesRoomFlags;
@property (retain, nonatomic) NSMutableDictionary *dayFlags;
@property (strong, nonatomic) NSMutableDictionary *ematCellData;

@property (strong, nonatomic) NSMutableDictionary *offscreenCells;

@end

@implementation IAAdvancedFilterViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initFilter];
    [self loadSessions];
    

    
    [self.tableView registerClass:[IAAdvancedFilterCell class] forCellReuseIdentifier:CellIdentifier];
    
    // Setting the estimated row height prevents the table view from calling tableView:heightForRowAtIndexPath: for every row in the table on first load;
    // it will only be called as cells are about to scroll onscreen. This is a major performance optimization.
    self.tableView.estimatedRowHeight = UITableViewAutomaticDimension;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    [[BannersServices shared] addWithImageView:self.imgUpperBanner
                                            at:BannerPositionTopBanner
                                   sender: self];
}

- (void)loadSessions
{
    NSManagedObjectContext* moc = [LLDataAccessLayer sharedInstance].managedObjectContext;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init] ;
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Session" inManagedObjectContext:moc];
    [fetchRequest setEntity:entity];
    
    NSSortDescriptor *sortDescriptorTime = [[NSSortDescriptor alloc] initWithKey:@"sesStartTime" ascending:YES];
    NSSortDescriptor *sortDescriptorName = [[NSSortDescriptor alloc] initWithKey:@"sesName" ascending:YES selector:@selector(localizedStandardCompare:)];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptorTime, sortDescriptorName, nil];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    sessions = [moc executeFetchRequest:fetchRequest error:nil];
}

-(void)initFilter
{
    self.timeFlags = [[NSMutableDictionary alloc] initWithCapacity:3];
    self.dayFlags = [[NSMutableDictionary alloc] initWithCapacity:3];
    self.sesTypeFlags = [[NSMutableDictionary alloc] initWithCapacity:3];
    self.sesRoomFlags = [[NSMutableDictionary alloc] initWithCapacity:3];
    self.sesSessionCategoriesFlags = [NSMutableDictionary new];
//    self.sesSessionNoFlags = [NSMutableDictionary new];

    self.filterData = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                       [NSMutableDictionary dictionaryWithObjectsAndKeys:@"0",@"position", self.sesTypeFlags, @"flags",@"", @"display_string",nil], @"Session types",
//                       [NSMutableDictionary dictionaryWithObjectsAndKeys:@"1",@"position", @"", @"flags",@"", @"display_string",nil], @"Days",
                       [NSMutableDictionary dictionaryWithObjectsAndKeys:@"1",@"position", self.dayFlags, @"flags",@"", @"display_string",nil], @"Days",
                       [NSMutableDictionary dictionaryWithObjectsAndKeys:@"2",@"position", self.timeFlags, @"flags",@"", @"display_string",nil], @"Times",
                       [NSMutableDictionary dictionaryWithObjectsAndKeys:@"3",@"position", self.sesSessionCategoriesFlags, @"flags",@"", @"display_string",nil], @"SessionCategories",
//                       [NSMutableDictionary dictionaryWithObjectsAndKeys:@"4",@"position", self.sesSessionNoFlags, @"flags",@"", @"display_string",nil], @"SessionNo",
                       [NSMutableDictionary dictionaryWithObjectsAndKeys:@"4",@"position", self.sesRoomFlags, @"flags",@"", @"display_string",nil], @"Rooms",
                       [NSMutableDictionary dictionaryWithObjectsAndKeys:@"5",@"position", self.ematCellData, @"flags",@"", @"display_string",nil], @"Ematerials availability",
                       nil];
}

-(void)clearFilterCascadingFromPosition:(int)position
{
    for (NSString *key in self.filterData)
    {
        if([[[self.filterData objectForKey:key] objectForKey:@"position"] intValue] > position)
        {
            [[self.filterData objectForKey:key] removeObjectForKey:@"predicate"];
            
//            if([key isEqualToString:@"Days"])
//            {
//                for(NSDictionary *dict in self.daySwitches)
//                {
//                    ((UISwitch*)[dict objectForKey:@"switch"]).on = YES;
//                }
//            }
//            else
            if ([key isEqualToString:@"Ematerials availability"])
            {
                ((UISwitch*)[self.ematCellData objectForKey:@"switch"]).on = NO;
            }
            else
            {
                //clear view for filter selections beneath current position
                [[self.filterData objectForKey:key] setObject:@"" forKey:@"display_string"];
                
                //clear selection flags for the filter
                for (NSString * flagKey in [[[self.filterData objectForKey:key] objectForKey:@"flags"] allKeys])
                {
                    [[[self.filterData objectForKey:key] objectForKey:@"flags"] setValue:[NSNumber numberWithBool:FALSE] forKey:flagKey];
                }
            }
        }
    }
    
    [self.tableView reloadData];
}

- (NSArray*)searchForFilterName:(NSString*)filterName
{
    // but clear predicate for filter name if it is already applied
    if([[self.filterData objectForKey:filterName] objectForKey:@"predicate"])
        [[self.filterData objectForKey:filterName] removeObjectForKey:@"predicate"];
    
    NSArray* sessionsArray = nil;
    NSMutableArray* predicates = [[NSMutableArray alloc] initWithCapacity:8];
    
    //go trough filter an add existing predicates with smaler position number (only before selected filter)
    for (NSString* key in self.filterData)
    {
        if([[[self.filterData objectForKey:filterName] objectForKey:@"position"]intValue] > [[[self.filterData objectForKey:key] objectForKey:@"position"]intValue])
        {
            NSLog(@"adding predicate of filter: %@", key);
            if ([[self.filterData objectForKey:key] objectForKey:@"predicate"])
                [predicates addObject:[[self.filterData objectForKey:key] objectForKey:@"predicate"]];
        }
    }
    
    NSManagedObjectContext* moc = [LLDataAccessLayer sharedInstance].managedObjectContext;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init] ;
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Session" inManagedObjectContext:moc];
    [fetchRequest setEntity:entity];
    
    NSSortDescriptor *sortDescriptorTime = [[NSSortDescriptor alloc] initWithKey:@"sesStartTime" ascending:YES selector:@selector(localizedStandardCompare:)];
    NSSortDescriptor *sortDescriptorName = [[NSSortDescriptor alloc] initWithKey:@"sesName" ascending:YES selector:@selector(localizedStandardCompare:)];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptorTime, sortDescriptorName, nil];
    
    NSPredicate* compoundPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
    
    fetchRequest.predicate = compoundPredicate;

    [fetchRequest setSortDescriptors:sortDescriptors];
    
    sessionsArray = [moc executeFetchRequest:fetchRequest error:nil];
    
    //custom part for Times
    if([filterName isEqualToString:@"Times"])
    {
        //get grouped times
        NSMutableSet* timesSet = [[NSMutableSet alloc] initWithCapacity:3];
        
        for(Session* ses in sessionsArray)
        {
            if (ses.sesStartTime.length > 0)
            {
                [timesSet addObject:[Utils formatDateString:ses.sesStartTime fromFormat:kDateFormatInDatabase toFormat:kTimeFormat]];
            }
        }
        
        NSArray* timesArray = [timesSet allObjects];
        NSArray* sortedTimesArray = [timesArray sortedArrayUsingSelector:@selector(localizedStandardCompare:)];
        
        NSMutableArray* sesTimes = [[NSMutableArray alloc] initWithCapacity:3];
        for(NSString* time in sortedTimesArray)
        {
            NSDictionary* dict = [NSDictionary dictionaryWithObject:time forKey:@"time"];
            
            [sesTimes addObject:dict];
        }
        
        sessionsArray = [sesTimes copy];
    }
    //custom part for Days
    if([filterName isEqualToString:@"Days"])
    {
        //get grouped days
        NSMutableSet* daysSet = [[NSMutableSet alloc] initWithCapacity:3];
        
        for(Session* ses in sessionsArray)
        {
            if (ses.sesStartTime.length > 0)
            {
                [daysSet addObject:[Utils formatDateString:ses.sesStartTime fromFormat:kDateFormatInDatabase toFormat:kDateFormat]];
            }
        }
        
        NSArray* daysArray = [daysSet allObjects];
        NSArray* sortedTimesArray = [daysArray sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
        
        NSMutableArray* sesDays = [[NSMutableArray alloc] initWithCapacity:3];
        for(NSString* time in sortedTimesArray)
        {
            NSDictionary* dict = [NSDictionary dictionaryWithObject:time forKey:@"day"];
            
            [sesDays addObject:dict];
        }
        
        sessionsArray = [sesDays copy];
    }
    //custom part for session types
    if([filterName isEqualToString:@"Session types"])
    {
        //get grouped talk types
        NSMutableSet* talkTypesSet = [[NSMutableSet alloc] initWithCapacity:3];
        
        for(Session* ses in sessionsArray)
        {
            if ([ses.sesType isEqualToString:@"Oral"])
            {
                [talkTypesSet addObject:@"Scientific session"];
            }
            else if ([ses.sesType isEqualToString:kKeyPoster])
            {
                [talkTypesSet addObject:kKeyPoster];
            }
            else
            {
                [talkTypesSet addObject:@"Industrial Symposium"];
            }
        }
        
        NSMutableArray* talkTypes = [[NSMutableArray alloc] initWithCapacity:3];
        for(NSString* type in talkTypesSet)
        {
            NSDictionary* dict = [NSDictionary dictionaryWithObject:type forKey:@"name"];
            
            [talkTypes addObject:dict];
        }
        
        sessionsArray = [talkTypes copy];
    }
    
    //custom part for rooms
    if([filterName isEqualToString:@"Rooms"])
    {
        //get grouped room names
        NSMutableSet* roomsSet = [[NSMutableSet alloc] initWithCapacity:3];
        
        for(Session* ses in sessionsArray)
        {
            if ([ses.venue.venType isEqualToString:@"Room"])
                [roomsSet addObject:ses.venue.venName];
        }
        
        NSMutableArray* sesRooms = [[NSMutableArray alloc] initWithCapacity:3];
        for(NSString* roomName in roomsSet)
        {
            NSDictionary* dict = [NSDictionary dictionaryWithObject:roomName forKey:@"roomName"];
            
            [sesRooms addObject:dict];
        }
        NSArray* sortedRoomsArray = [sesRooms sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"roomName" ascending:YES]]];
        sessionsArray = [sortedRoomsArray copy];
    }
    
    //custom part for Session Categories
    if([filterName isEqualToString:@"SessionCategories"])
    {
        //get grouped room names
        NSMutableSet* secSet = [[NSMutableSet alloc] initWithCapacity:3];
        
        for(Session* ses in sessionsArray)
        {
            if(ses.sesCategoryId.intValue != 0) {
                SessionCategory *c = [SessionCategory getSessionCategoryWithId:ses.sesCategoryId];
                if (c != nil) [secSet addObject:c];
            }
        }
        
        NSMutableArray* secNames = [[NSMutableArray alloc] initWithCapacity:3];
        for(SessionCategory* sec in secSet)
        {
            NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:sec.secSessionCategoryId, @"sesSessionCategoryId", sec.secName, @"secName",nil];
            
            [secNames addObject:dict];
        }
        NSArray* sortedSessionCategoriesArray = [secNames sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"secName" ascending:YES]]];
        sessionsArray = [sortedSessionCategoriesArray copy];
    }
    
    return sessionsArray;
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark tableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    IAAdvancedFilterCell *cell = nil;

    switch (indexPath.row)
    {
        case 0:
            cell = [self.tableView dequeueReusableCellWithIdentifier:@"advancedFilterCell" forIndexPath:indexPath];
            cell.lblFilterName.text = @"Session type";
            cell.lblSelectedValues.text = [[self.filterData objectForKey:@"Session types"] objectForKey:@"display_string"];
            cell.cellData = [self.filterData objectForKey:@"Session types"];
            break;
        case 1:
            cell = [self.tableView dequeueReusableCellWithIdentifier:@"advancedFilterCell" forIndexPath:indexPath];
            cell.lblFilterName.text = @"Date";
            cell.lblSelectedValues.text = [[self.filterData objectForKey:@"Days"] objectForKey:@"display_string"];
            cell.cellData = [self.filterData objectForKey:@"Days"];
            break;
        case 2:
            cell = [self.tableView dequeueReusableCellWithIdentifier:@"advancedFilterCell" forIndexPath:indexPath];
            cell.lblFilterName.text = @"Time";
            cell.lblSelectedValues.text = [[self.filterData objectForKey:@"Times"] objectForKey:@"display_string"];
            cell.cellData = [self.filterData objectForKey:@"Times"];
            break;
        case 3:
            cell = [self.tableView dequeueReusableCellWithIdentifier:@"advancedFilterCell" forIndexPath:indexPath];
            cell.lblFilterName.text = @"Session category";
            cell.lblSelectedValues.text = [[self.filterData objectForKey:@"SessionCategories"] objectForKey:@"display_string"];
            cell.cellData = [self.filterData objectForKey:@"SessionCategories"];
            break;
//        case 4:
//            cell = [self.tableView dequeueReusableCellWithIdentifier:@"advancedFilterCell" forIndexPath:indexPath];
//            cell.lblFilterName.text = @"Session number";
//            cell.lblSelectedValues.text = [[self.filterData objectForKey:@"SessionNo"] objectForKey:@"display_string"];
//            cell.cellData = [self.filterData objectForKey:@"SessionNo"];
//            break;
        case 4:
            cell = [self.tableView dequeueReusableCellWithIdentifier:@"advancedFilterCell" forIndexPath:indexPath];
            cell.lblFilterName.text = @"Room";
            cell.lblSelectedValues.text = [[self.filterData objectForKey:@"Rooms"] objectForKey:@"display_string"];
            cell.cellData = [self.filterData objectForKey:@"Rooms"];
            break;
        default:
            break;
    }
    
    // Make sure the constraints have been added to this cell, since it may have just been created from scratch
    [cell setNeedsUpdateConstraints];
    [cell updateConstraintsIfNeeded];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch (indexPath.row)
    {
        case 0:
            [self filterBySessionType];
            break;
        case 1:
            [self filterByDay];
            break;
        case 2:
            [self filterByTime];
            break;
        case 3:
            [self filterBySessionCategories];
            break;
//        case 4:
//            [self filterBySessionNo];
//            break;
        case 4:
            [self filterByRoom];
            break;
        case 5:
            break;
        default:
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    CGFloat height = 100.0;
    if (indexPath.row != 5)
    {
        CGFloat height = 63;
        BOOL resize = NO;
        for (NSString* key in self.filterData)
        {
            if ([[[self.filterData objectForKey:key] objectForKey:@"position"] intValue] == indexPath.row )
            {
                if( ![[[self.filterData objectForKey:key] objectForKey:@"display_string"] isEqualToString:@""])
                {
                    resize = YES;
                    break;
                }
            }
        }
        
        if(resize)
        {
            IAAdvancedFilterCell *cell = [self.offscreenCells objectForKey:@"advancedFilterCell"];
            
            if (!cell) {
                cell = [[IAAdvancedFilterCell alloc] init];
                [self.offscreenCells setObject:cell forKey:@"advancedFilterCell"];
            }
            
            // Configure the cell for this indexPath
            
            //    NSDictionary *dataSourceItem = [self.model.dataSource objectAtIndex:indexPath.row];
            //    switch (indexPath.row)
            //    {
            //        case 0:
            //            cell.lblFilterName.text = @"Session type";
            //            cell.lblSelectedValues.text = [[self.filterData objectForKey:@"Session types"] objectForKey:@"display_string"];
            //            break;
            //        case 1:
            //            cell.lblFilterName.text = @"Time";
            //            cell.lblSelectedValues.text = [[self.filterData objectForKey:@"Times"] objectForKey:@"display_string"];
            //            break;
            //        case 2:
            //            cell.lblFilterName.text = @"Room";
            //            cell.lblSelectedValues.text = [[self.filterData objectForKey:@"Rooms"] objectForKey:@"display_string"];
            //            break;
            //
            //        default:
            //            break;
            //    }
            
            switch (indexPath.row)
            {
                case 0:
                    cell.lblFilterName.text = @"Session type";
                    cell.lblSelectedValues.text = [[self.filterData objectForKey:@"Session types"] objectForKey:@"display_string"];
                    break;
                case 1:
                    cell.lblFilterName.text = @"Day";
                    cell.lblSelectedValues.text = [[self.filterData objectForKey:@"Days"] objectForKey:@"display_string"];
                    break;
                case 2:
                    cell.lblFilterName.text = @"Time";
                    cell.lblSelectedValues.text = [[self.filterData objectForKey:@"Times"] objectForKey:@"display_string"];
                    break;
                case 3:
                    cell.lblFilterName.text = @"Session category";
                    cell.lblSelectedValues.text = [[self.filterData objectForKey:@"SessionCategories"] objectForKey:@"display_string"];
                    break;
//                case 4:
//                    cell.lblFilterName.text = @"Session number";
//                    cell.lblSelectedValues.text = [[self.filterData objectForKey:@"SessionNo"] objectForKey:@"display_string"];
//                    break;
                case 4:
                    cell.lblFilterName.text = @"Room";
                    cell.lblSelectedValues.text = [[self.filterData objectForKey:@"Rooms"] objectForKey:@"display_string"];
                    break;
                case 5:
                    //                    cell.lblFilterName.text = @"E-materials available";
                    //                    cell.lblSelectedValues.text = [[self.filterData objectForKey:@"E-materials"] objectForKey:@"display_string"];
                    break;
                default:
                    break;
            }
            
            // Make sure the constraints have been added to this cell, since it may have just been created from scratch
            [cell setNeedsUpdateConstraints];
            [cell updateConstraintsIfNeeded];
            
            // The cell's width must be set to the same size it will end up at once it is in the table view.
            // This is important so that we'll get the correct height for different table view widths, since our cell's
            // height depends on its width due to the multi-line UILabel word wrapping. Don't need to do this above in
            // -[tableView:cellForRowAtIndexPath:] because it happens automatically when the cell is used in the table view.
            cell.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(tableView.bounds), CGRectGetHeight(cell.bounds));
            // NOTE: if you are displaying a section index (e.g. alphabet along the right side of the table view), or
            // if you are using a grouped table view style where cells have insets to the edges of the table view,
            // you'll need to adjust the cell.bounds.size.width to be smaller than the full width of the table view we just
            // set it to above. See http://stackoverflow.com/questions/3647242 for discussion on the section index width.
            
            // Do the layout pass on the cell, which will calculate the frames for all the views based on the constraints
            // (Note that the preferredMaxLayoutWidth is set on multi-line UILabels inside the -[layoutSubviews] method
            // in the UITableViewCell subclass
            [cell setNeedsLayout];
            [cell layoutIfNeeded];
            
            // Get the actual height required for the cell
            height = [cell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
            
            // Add an extra point to the height to account for the cell separator, which is added between the bottom
            // of the cell's contentView and the bottom of the table view cell.
            height += 1;
        }
        return height;
    }
    else
    {
        return 63;
    }
}

#pragma mark FILTERS

- (void)filterByDay
{
//    //    This method iterates through days switches array, and check which switch is on.
//    //    Every time it creates day predicate from scratch
//    
//    NSMutableArray* predicates = [[NSMutableArray alloc] initWithCapacity:3];
//    
//    //     NSArray* daysSwitchesArray = [NSArray arrayWithObjects:self.switchDay1, self.switchDay2, self.switchDay3,self.switchDay4, nil];
//    for (NSDictionary * daySwitchDict in self.daySwitches)
//    {
//        //check which switch is on
//        if(((UISwitch*)[daySwitchDict objectForKey:@"switch"]).on)
//        {
//            NSPredicate* predicate = [NSPredicate predicateWithFormat:@"sesStartTime CONTAINS[cd] %@", [daySwitchDict objectForKey:@"date"]];//AND (sesStartTime < '2013-05-19 00:00:00)'
//            
//            [predicates addObject:predicate];
//        }
//    }
//    
//    if (predicates.count)
//    {
//        NSPredicate* orPredicate = [NSCompoundPredicate orPredicateWithSubpredicates:predicates];
//        
//        [[self.filterData objectForKey:@"Days"] setObject:orPredicate forKey:@"predicate"];
//    }
//    else
//    {
//        [[self.filterData objectForKey:@"Days"] setObject:[NSPredicate predicateWithFormat:@"sesStartTime CONTAINS[cd] '1900-01-01 00:00:00' "] forKey:@"predicate"];
//    }
//    
//    //we changed status of switch so we need to clear other filters
//    [self clearFilterCascadingFromPosition:[[[self.filterData objectForKey:@"Days"] objectForKey:@"position"]intValue]];
    
    
    //populate sessions
    NSArray* timesArray = [self searchForFilterName:@"Days"];
    
    if (!self.dayFlags.count)
    {
        for(NSDictionary* time in timesArray)
        {
            [self.dayFlags setValue:[NSNumber numberWithBool:FALSE] forKey:[time objectForKey:@"day"]];
        }
        
    }
    PSMultiSelectViewController* mpVC = [[PSMultiSelectViewController alloc]
                                         initWithItemArray:timesArray
                                         Flags:self.dayFlags
                                         Title:@"Select Session Day"
                                         ValueMember:@"day"
                                         DisplayMember:@"day"
                                         FilterName:@"Days"];
    mpVC.delegate = self;
    mpVC.tag = @"dayPicker";
    mpVC.title = @"Date selection";
    
    [self.navigationController pushViewController:mpVC animated:YES];
    
}
- (void)filterByTime
{
    //populate sessions
    NSArray* timesArray = [self searchForFilterName:@"Times"];
    
    if (!self.timeFlags.count)
    {
        for(NSDictionary* time in timesArray)
        {
            [self.timeFlags setValue:[NSNumber numberWithBool:FALSE] forKey:[time objectForKey:@"time"]];
        }
        
    }
    PSMultiSelectViewController* mpVC = [[PSMultiSelectViewController alloc]
                                         initWithItemArray:timesArray
                                         Flags:self.timeFlags
                                         Title:@"Select Session Time"
                                         ValueMember:@"time"
                                         DisplayMember:@"time"
                                         FilterName:@"Times"];
    mpVC.delegate = self;
    mpVC.tag = @"timePicker";
    mpVC.title = @"Time selection";
    
    [self.navigationController pushViewController:mpVC animated:YES];
}

- (void)filterBySessionType
{
    NSArray* sessionTypesArray = [self searchForFilterName:@"Session types"];
    
    if (!self.sesTypeFlags.count)
    {
        for(NSDictionary *sessionType in sessionTypesArray)
        {
            [self.sesTypeFlags setValue:[NSNumber numberWithBool:FALSE] forKey:[sessionType objectForKey:@"name"]];
        }
    }
    PSMultiSelectViewController* mpVC = [[PSMultiSelectViewController alloc] initWithItemArray:sessionTypesArray
                                                                                         Flags:self.sesTypeFlags
                                                                                         Title:@"Session types"
                                                                                   ValueMember:@"name"
                                                                                 DisplayMember:@"name"
                                                                                    FilterName:@"Session types"];
    mpVC.delegate = self;
    mpVC.tag = @"sessionTypePicker";
    mpVC.title = @"Session type selection";
    
    [self.navigationController pushViewController:mpVC animated:YES];
}

- (void)filterBySessionCategories
{
    NSArray* sessionsArray = [self searchForFilterName:@"SessionCategories"];
    
//    [self.sesSessionCategoriesFlags removeAllObjects];
    if (!self.sesSessionCategoriesFlags.count)
    {
        for(NSDictionary* ses in sessionsArray)
        {
            [self.sesSessionCategoriesFlags setValue:[NSNumber numberWithBool:FALSE] forKey:[[ses objectForKey:@"sesSessionCategoryId"] stringValue]];
        }
    }
    
    PSMultiSelectViewController* mpVC = [[PSMultiSelectViewController alloc]
                                         initWithItemArray:sessionsArray
                                         Flags:self.sesSessionCategoriesFlags
                                         Title:@"Select Session Category"
                                         ValueMember:@"sesSessionCategoryId"
                                         DisplayMember:@"secName"
                                         FilterName:@"SessionCategories"];
    mpVC.delegate = self;
    mpVC.tag = @"sesCategoryPicker";
    mpVC.title = @"Session category selection";
    
    [self.navigationController pushViewController:mpVC animated:YES];
}
//
//- (void)filterBySessionNo
//{
//    NSArray *sessionsArray = [self searchForFilterName:@"SessionNo"];
//    NSMutableArray *sessionsMutableArray = [NSMutableArray arrayWithArray:sessionsArray];
//
//    if (!self.sesSessionNoFlags.count)
//    {
//        for(Session* ses in sessionsArray)
//        {
//            NSString *trimmedSessionNo = [ses.sesCode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
//
//            if (ses.sesCode && trimmedSessionNo.length > 0)
//            {
//                [self.sesSessionNoFlags setValue:[NSNumber numberWithBool:FALSE] forKey:ses.sesCode];
//            }
//            else
//            {
//                [sessionsMutableArray removeObject:ses];
//            }
//        }
//    }
//    else
//    {
//        for(Session* ses in sessionsArray)
//        {
//            NSString *trimmedSessionNo = [ses.sesCode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
//
//            if (ses.sesCode == nil || trimmedSessionNo.length == 0)
//            {
//                [sessionsMutableArray removeObject:ses];
//            }
//        }
//    }
//
//    PSMultiSelectViewController* mpVC = [[PSMultiSelectViewController alloc]
//                                         initWithItemArray:sessionsMutableArray
//                                         Flags:self.sesSessionNoFlags
//                                         Title:@"Select Session Number"
//                                         ValueMember:@"sesCode"
//                                         DisplayMember:@"sesCode"
//                                         FilterName:@"SessionNo"];
//    mpVC.delegate = self;
//    mpVC.tag = @"sesSessionNoPicker";
//    mpVC.title = @"Session number selection";
//
//    [self.navigationController pushViewController:mpVC animated:YES];
//}

- (void)filterByRoom
{
    NSArray* roomsArray = [self searchForFilterName:@"Rooms"];
    
    if (!self.sesRoomFlags.count)
    {
        for(NSDictionary* room in roomsArray)
        {
            [self.sesRoomFlags setValue:[NSNumber numberWithBool:FALSE] forKey:[room objectForKey:@"roomName"]];
        }
    }
    
    PSMultiSelectViewController* mpVC = [[PSMultiSelectViewController alloc]
                                         initWithItemArray:roomsArray
                                         Flags:self.sesRoomFlags
                                         Title:@"Select Session Room"
                                         ValueMember:@"roomName"
                                         DisplayMember:@"roomName"
                                         FilterName:@"Rooms"];
    mpVC.delegate = self;
    mpVC.tag = @"sesRoomPicker";
    mpVC.title = @"Room selection";
    
    [self.navigationController pushViewController:mpVC animated:YES];
}

- (void)filterByEMatAvailability:(UISwitch *)sender
{
    NSPredicate* predicate = nil;
    
    if (((UISwitch*)[self.ematCellData objectForKey:@"switch"]).on)
    {
        predicate = [NSPredicate predicateWithFormat:@"SUBQUERY(talks, $tlk, $tlk.tlkAvailable = 1 ).@count>0"];
    }
    
    if(predicate)
    {
        [[self.filterData objectForKey:@"Ematerials availability"] setObject:predicate  forKey:@"predicate"];
    }
    else
    {
        [[self.filterData objectForKey:@"Ematerials availability"] removeObjectForKey:@"predicate"];
    }
}

- (IBAction)btnResetAction:(id)sender
{
//    self.daySwitches = nil;
    for (NSString *key in self.filterData)
    {
        [[self.filterData objectForKey:key] removeObjectForKey:@"predicate"];
        
//        if([key isEqualToString:@"Days"])
//        {
//            for(NSDictionary *dict in self.daySwitches)
//            {
//                ((UISwitch*)[dict objectForKey:@"switch"]).hidden = NO;
//                ((UISwitch*)[dict objectForKey:@"switch"]).on = YES;
//            }
//        }
//        else
        if ([key isEqualToString:@"Ematerials availability"])
        {
            ((UISwitch*)[self.ematCellData objectForKey:@"switch"]).on = NO;
        }
        else
        {
            //clear view for filter selections beneath current position
            [[self.filterData objectForKey:key] setObject:@"" forKey:@"display_string"];
            
            //clear selection flags for the filter
            for (NSString * flagKey in [[[self.filterData objectForKey:key] objectForKey:@"flags"] allKeys])
            {
                [[[self.filterData objectForKey:key] objectForKey:@"flags"] setValue:[NSNumber numberWithBool:FALSE] forKey:flagKey];
            }
        }
    }
    
    [self.tableView reloadData];
}

- (IBAction)btnSearchAction:(id)sender
{}

////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark MultiPickerDelegate methods
////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)MPselectionDone:(id)sender
{
    NSMutableArray* predicates = [[NSMutableArray alloc] initWithCapacity:3];
    NSMutableString* display_string = [[NSMutableString alloc] init];
    
    if([sender isKindOfClass:[PSMultiSelectViewController class]])
    {
        PSMultiSelectViewController* tpmpvc = (PSMultiSelectViewController*)sender;
        
        if([tpmpvc.tag isEqualToString:@"timePicker"])
        {
            NSArray* allKeys = [self.timeFlags allKeys];
            NSArray* sortedAllKeys = [allKeys sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
            
            for (NSString* key in sortedAllKeys)
            {
                if([[self.timeFlags objectForKey:key] boolValue] == TRUE)
                {
                    NSString *tempDate = [NSString stringWithFormat:@"2017-10-10 %@", key];
                    NSString *tempDateFormat = [NSString stringWithFormat:@"%@ %@", kDateDefaultFormat, kTimeFormat];
                    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"sesStartTime CONTAINS[cd] %@", [Utils formatDateString:tempDate fromFormat:tempDateFormat toFormat:kTimeDefaultFormat]];
                    [display_string appendFormat:@"%@\t", key];
                    
                    [predicates addObject:predicate];
                }
            }
            
            [[self.filterData objectForKey:@"Times"] setObject:display_string  forKey:@"display_string"];
            //            IAAdvancedFilterCell* cell = (IAAdvancedFilterCell*) [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
            //            cell.lblSelectedValues.text = display_string;
            //            self.txtViewSelectedTimes.text = display_string;
            
            if (predicates.count)
            {
                NSPredicate* orPredicate = [NSCompoundPredicate orPredicateWithSubpredicates:predicates];
                
                [[self.filterData objectForKey:@"Times"] setObject:orPredicate  forKey:@"predicate"];
            }
            else
            {
                [[self.filterData objectForKey:@"Times"] removeObjectForKey:@"predicate"];
            }
            
        }
        else if([tpmpvc.tag isEqualToString:@"dayPicker"])
        {
            NSArray* allKeys = [self.dayFlags allKeys];
            NSArray* sortedAllKeys = [allKeys sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
            
            for (NSString* key in sortedAllKeys)
            {
                if([[self.dayFlags objectForKey:key] boolValue] == TRUE)
                {
                    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"sesStartTime CONTAINS[cd] %@", [Utils formatDateString:key fromFormat:kDateFormat toFormat:kDateDefaultFormat]];
                    [display_string appendFormat:@"%@\t", key];
                    
                    [predicates addObject:predicate];
                }
            }
            
            [[self.filterData objectForKey:@"Days"] setObject:display_string  forKey:@"display_string"];
            //            IAAdvancedFilterCell* cell = (IAAdvancedFilterCell*) [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
            //            cell.lblSelectedValues.text = display_string;
            //            self.txtViewSelectedTimes.text = display_string;
            
            if (predicates.count)
            {
                NSPredicate* orPredicate = [NSCompoundPredicate orPredicateWithSubpredicates:predicates];
                
                [[self.filterData objectForKey:@"Days"] setObject:orPredicate  forKey:@"predicate"];
            }
            else
            {
                [[self.filterData objectForKey:@"Days"] removeObjectForKey:@"predicate"];
            }
            
        }
        else if([tpmpvc.tag isEqualToString:@"sessionTypePicker"])
        {
            NSPredicate* predicate = nil;
            
            if([[self.sesTypeFlags objectForKey:@"Scientific session"] boolValue] == TRUE)
            {
                predicate = [NSPredicate predicateWithFormat:@"sesType == 'Oral'"];
                [predicates addObject:predicate ];
                [display_string appendFormat:@"Scientific session\n"];
            }
            if([[self.sesTypeFlags objectForKey:kKeyPoster]boolValue] == TRUE)
            {
                predicate = [NSPredicate predicateWithFormat:@"sesType == 'Poster'"];
                [predicates addObject:predicate];
                [display_string appendFormat:@"Poster\n"];
            }
            if([[self.sesTypeFlags objectForKey:@"Industrial Symposium"] boolValue] == TRUE)
            {
                predicate = [NSPredicate predicateWithFormat:@"sesType == %@", kKeyIndustrialSymposium];
                [predicates addObject:predicate];
                [display_string appendFormat:@"Industrial Symposium"];
            }
            
            if(predicate)
            {
                [predicates addObject:predicate];
            }
            [[self.filterData objectForKey:@"Session types"] setObject:display_string  forKey:@"display_string"];
            //            self.txtViewSelectedSesTypes.text = display_string;
            
            if (predicates.count)
            {
                NSPredicate* orPredicate = [NSCompoundPredicate orPredicateWithSubpredicates:predicates];
                
                [[self.filterData objectForKey:@"Session types"] setObject:orPredicate forKey:@"predicate"];
            }
            else
            {
                [[self.filterData objectForKey:@"Session types"] removeObjectForKey:@"predicate"];
            }
        }
        else if([tpmpvc.tag isEqualToString:@"sesRoomPicker"])
        {
            for (NSString* key in self.sesRoomFlags)
            {
                if ([[self.sesRoomFlags objectForKey:key] boolValue] == TRUE)
                {
                    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"venue.venName = %@ ", key];
                    [display_string appendFormat:@"%@\t", key];
                    
                    [predicates addObject:predicate];
                }
            }
            [[self.filterData objectForKey:@"Rooms"] setObject:display_string  forKey:@"display_string"];
            //            self.txtViewSelectedSesRooms.text = display_string;
            
            if (predicates.count)
            {
                NSPredicate* orPredicate = [NSCompoundPredicate orPredicateWithSubpredicates:predicates];
                
                [[self.filterData objectForKey:@"Rooms"] setObject:orPredicate forKey:@"predicate"];
            }
            else
            {
                [[self.filterData objectForKey:@"Rooms"] removeObjectForKey:@"predicate"];
            }
        }
//        else if([tpmpvc.tag isEqualToString:@"sesSessionNoPicker"])
//        {
//            for (NSString* key in self.sesSessionNoFlags)
//            {
//                if ([[self.sesSessionNoFlags objectForKey:key] boolValue] == TRUE)
//                {
//                    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"sesCode = %@ ", key];
//                    [display_string appendFormat:@"%@\t", key];
//
//                    [predicates addObject:predicate];
//                }
//            }
//            [[self.filterData objectForKey:@"SessionNo"] setObject:display_string  forKey:@"display_string"];
//            //            self.txtViewSelectedSesRooms.text = display_string;
//
//            if (predicates.count)
//            {
//                NSPredicate* orPredicate = [NSCompoundPredicate orPredicateWithSubpredicates:predicates];
//
//                [[self.filterData objectForKey:@"SessionNo"] setObject:orPredicate forKey:@"predicate"];
//            }
//            else
//            {
//                [[self.filterData objectForKey:@"SessionNo"] removeObjectForKey:@"predicate"];
//            }
//        }
        else if([tpmpvc.tag isEqualToString:@"sesCategoryPicker"])
        {
            int i = 0;
            
            for (NSDictionary* item in tpmpvc.itemsToDisplay)
            {
                NSString* key = [NSString stringWithFormat:@"%@", [item objectForKey:@"sesSessionCategoryId"]];
                
                if ([[self.sesSessionCategoriesFlags objectForKey:key] boolValue] == TRUE)
                {
                    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"sesCategoryId = %@ ", key];
                    
                    if (i++==0)
                    {
                        [display_string appendFormat:@"%@", [item objectForKey:@"secName"]];
                    }
                    else
                    {
                        [display_string appendFormat:@"\n\n%@", [item objectForKey:@"secName"]];
                    }
                    
                    [predicates addObject:predicate];
                }
            }
            [[self.filterData objectForKey:@"SessionCategories"] setObject:display_string  forKey:@"display_string"];
            //            self.txtViewSelectedSesRooms.text = display_string;
            
            if (predicates.count)
            {
                NSPredicate* orPredicate = [NSCompoundPredicate orPredicateWithSubpredicates:predicates];
                
                [[self.filterData objectForKey:@"SessionCategories"] setObject:orPredicate forKey:@"predicate"];
            }
            else
            {
                [[self.filterData objectForKey:@"SessionCategories"] removeObjectForKey:@"predicate"];
            }
        }
        
//        //if position is less than Days filter update switches
//        if ([[[self.filterData objectForKey:tpmpvc.filterName] objectForKey:@"position" ] intValue] < [[[self.filterData objectForKey:@"Days"] objectForKey:@"position" ] intValue])
//        {
//            [self updateDaySwitches];
//        }
        tpmpvc = nil;
    }
    
    [self.tableView reloadData];
}

- (void)MPitemSelected:(id)item sender:(id)sender
{
    PSMultiSelectViewController* tpmpvc = (PSMultiSelectViewController*)sender;
    
    [self clearFilterCascadingFromPosition:[[[self.filterData objectForKey:tpmpvc.filterName] objectForKey:@"position"] intValue]];
}

#pragma mark Update Day switches
//-(void)updateDaySwitches
//{
//    NSArray* sessionsArray = [self searchForFilterName:@"Days"];
//    
//    NSMutableSet* dayDates = [[NSMutableSet alloc] initWithCapacity:4];
//    
//    for(Session* ses in sessionsArray)
//    {
//        [dayDates addObject:[Utils getDateForAdvancedFilter:ses.sesStartTime]];
//    }
//    
//    NSArray* dayDatesArray = [dayDates allObjects];
//    
//    for(NSDictionary* daySwitch in self.daySwitches)
//    {
//        if(daySwitch)
//        {
//            ((UISwitch*)[daySwitch objectForKey:@"switch"]).hidden = YES;
//            ((UISwitch*)[daySwitch objectForKey:@"switch"]).on = NO;
//            ((UISwitch*)[daySwitch objectForKey:@"switch"]).userInteractionEnabled = NO;
//            
//            for (int i=0 ; i < dayDatesArray.count; i++)
//            {
//                if([[daySwitch objectForKey:@"date"] isEqualToString:[dayDatesArray objectAtIndex:i]])
//                {
//                    
//                    ((UISwitch*)[daySwitch objectForKey:@"switch"]).hidden = NO;
//                    ((UISwitch*)[daySwitch objectForKey:@"switch"]).on = YES;
//                    ((UISwitch*)[daySwitch objectForKey:@"switch"]).userInteractionEnabled = YES;
//                }
//            }
//        }
//    }
//}

#pragma mark IAAdvancedFilterDaysCellDelegate
-(void)swDayValueChanged:(UISwitch *)swDay inCell:(UITableViewCell *)cell
{
//    [self filterByDay:swDay];
}

#pragma mark - IAAdvancedFilterEmatCellDelegate
-(void)swEmatValueChanged:(UISwitch *)swDay inCell:(UITableViewCell *)cell
{
    [self filterByEMatAvailability:swDay];
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"advancedSearchResultsSegue"])
    {
        // Get reference to the destination view controller
        IAAdvancedFilterResultsViewController *vc = [segue destinationViewController];
        
        NSMutableArray* predicates = [[NSMutableArray alloc] initWithCapacity:8];
        
        for (id key in self.filterData)
        {
            if([[self.filterData objectForKey:key] objectForKey:@"predicate"])
            {
                [predicates addObject:[[self.filterData objectForKey:key] objectForKey:@"predicate"]];
            }
        }
        
        NSManagedObjectContext* moc = [LLDataAccessLayer sharedInstance].managedObjectContext;
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init] ;
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"Session" inManagedObjectContext:moc];
        [fetchRequest setEntity:entity];
        
//        NSSortDescriptor *sortDescriptorDay = [[NSSortDescriptor alloc] initWithKey:@"zetDay" ascending:YES];
        NSSortDescriptor *sortDescriptorTime = [[NSSortDescriptor alloc] initWithKey:@"sesStartTime" ascending:YES];
//        NSSortDescriptor *sortDescriptorRoom = [[NSSortDescriptor alloc] initWithKey:@"venue.venName" ascending:YES];
        NSSortDescriptor *sortDescriptorName = [[NSSortDescriptor alloc] initWithKey:@"sesName" ascending:YES selector:@selector(localizedStandardCompare:)];
        NSArray *sortDescriptors = [[NSArray alloc] initWithObjects: sortDescriptorTime, sortDescriptorName,  nil];
        
        NSPredicate* compoundPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
        
        fetchRequest.predicate = compoundPredicate;
        
        [fetchRequest setSortDescriptors:sortDescriptors];
        
        NSArray* sessionsArray = [moc executeFetchRequest:fetchRequest error:nil] ;
        
        // Pass any objects to the view controller here, like...
        [vc setSessions:sessionsArray];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

@end
