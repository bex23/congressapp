//
//  IAAdvancedFilterCell.m
//  ERA-EDTA2015
//
//  Created by admin on 7/17/14.
//  Copyright (c) 2014 Navus. All rights reserved.
//

#import "IAAdvancedFilterCell.h"

#define kLabelHorizontalInsets      15.0f
#define kLabelVerticalInsets        10.0f
#define kSwitchHorisontalInsets     

@interface IAAdvancedFilterCell()

@property (nonatomic, assign) BOOL didSetupConstraints;

@end

@implementation IAAdvancedFilterCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self)
    {
        self.lblFilterName = [UILabel newAutoLayoutView];
        [self.lblFilterName setLineBreakMode:NSLineBreakByTruncatingTail];
        [self.lblFilterName setNumberOfLines:1];
        [self.lblFilterName setTextAlignment:NSTextAlignmentLeft];
        [self.lblFilterName setTextColor:[UIColor colorWithHex:kGlobalColor andAlpha:1.0]];
//        self.lblFilterName.backgroundColor = [UIColor colorWithRed:0 green:0 blue:1 alpha:0.1];
        
        self.lblSelectedValues = [UILabel newAutoLayoutView];
        [self.lblSelectedValues setLineBreakMode:NSLineBreakByTruncatingTail];
        [self.lblSelectedValues setNumberOfLines:0];
        [self.lblSelectedValues setTextAlignment:NSTextAlignmentLeft];
        [self.lblSelectedValues setFont:[UIFont systemFontOfSize:15]];
        [self.lblSelectedValues setTextColor:[UIColor grayColor]];
//        self.lblSelectedValues.backgroundColor = [UIColor colorWithRed:1 green:0 blue:0 alpha:0.1];
        
//        self.contentView.backgroundColor = [UIColor colorWithRed:0 green:1 blue:0 alpha:0.1];
        
        [self.contentView addSubview:self.lblFilterName];
        [self.contentView addSubview:self.lblSelectedValues];

        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    return self;
}

- (void)updateConstraints
{
    [super updateConstraints];
    
    if (self.didSetupConstraints) {
        return;
    }
    
    // Note: if the constraints you add below require a larger cell size than the current size (which is likely to be the default size {320, 44}), you'll get an exception.
    // As a fix, you can temporarily increase the size of the cell's contentView so that this does not occur using code similar to the line below.
    //      See here for further discussion: https://github.com/Alex311/TableCellWithAutoLayout/commit/bde387b27e33605eeac3465475d2f2ff9775f163#commitcomment-4633188
    // self.contentView.bounds = CGRectMake(0.0f, 0.0f, 99999.0f, 99999.0f);
    
    [UIView autoSetPriority:UILayoutPriorityRequired forConstraints:^{
        [self.lblFilterName autoSetContentCompressionResistancePriorityForAxis:ALAxisVertical];
    }];
    [self.lblFilterName autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:kLabelVerticalInsets];
    [self.lblFilterName autoPinEdgeToSuperviewEdge:ALEdgeLeading withInset:kLabelHorizontalInsets];
    [self.lblFilterName autoPinEdgeToSuperviewEdge:ALEdgeTrailing withInset:kLabelHorizontalInsets];
    
    // This is the constraint that connects the title and body labels. It is a "greater than or equal" inequality so that if the row height is
    // slightly larger than what is actually required to fit the cell's subviews, the extra space will go here. (This is the case on iOS 7
    // where the cell separator is only 0.5 points tall, but in the tableView:heightForRowAtIndexPath: method of the view controller, we add
    // a full 1.0 point in extra height to account for it, which results in 0.5 points extra space in the cell.)
    // See https://github.com/smileyborg/TableViewCellWithAutoLayout/issues/3 for more info.
    [self.lblSelectedValues autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:self.lblFilterName withOffset:kLabelVerticalInsets relation:NSLayoutRelationGreaterThanOrEqual];
    
    [UIView autoSetPriority:UILayoutPriorityRequired forConstraints:^{
        [self.lblSelectedValues autoSetContentCompressionResistancePriorityForAxis:ALAxisVertical];
    }];
    [self.lblSelectedValues autoPinEdgeToSuperviewEdge:ALEdgeLeading withInset:kLabelHorizontalInsets];
    [self.lblSelectedValues autoPinEdgeToSuperviewEdge:ALEdgeTrailing withInset:kLabelHorizontalInsets];
    [self.lblSelectedValues autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:kLabelVerticalInsets];

    self.didSetupConstraints = YES;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    // Make sure the contentView does a layout pass here so that its subviews have their frames set, which we
    // need to use to set the preferredMaxLayoutWidth below.
    [self.contentView setNeedsLayout];
    [self.contentView layoutIfNeeded];
    
    // Set the preferredMaxLayoutWidth of the mutli-line lblSelectedValues based on the evaluated width of the label's frame,
    // as this will allow the text to wrap correctly, and as a result allow the label to take on the correct height.
    self.lblSelectedValues.preferredMaxLayoutWidth = CGRectGetWidth(self.lblSelectedValues.frame);
}
@end
