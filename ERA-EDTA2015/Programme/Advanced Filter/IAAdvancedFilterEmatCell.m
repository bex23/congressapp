//
//  IAAdvancedFilterEmatCell.m
//  ERA-EDTA2015
//
//  Created by admin on 1/15/15.
//  Copyright (c) 2015 Navus. All rights reserved.
//

#import "IAAdvancedFilterEmatCell.h"

@implementation IAAdvancedFilterEmatCell

- (NSDictionary*)initialiseEMatSwitch
{
    NSDictionary* eMatAvailabilityData = [NSDictionary dictionaryWithObjectsAndKeys:self.swEmat, @"switch", nil];
    
    return eMatAvailabilityData;
}

- (IBAction)swEmatValueChanged:(UISwitch *)sender
{
    if([self.delegate respondsToSelector:@selector(swEmatValueChanged:inCell:)])
    {
        [self.delegate swEmatValueChanged:sender inCell:self];
    }
}

@end
