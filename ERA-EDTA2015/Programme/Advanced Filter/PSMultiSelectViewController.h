//
//  TPCategoryDropDown.h
//  TourPad
//
//  Created by Mac on 11/9/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

/*
    Overview

 This class simplifies the task of displaying an array of items in 
 a UITableView in a UIPopoverController and maintain a dictionary
 with information on which items are selected. This easiest to do
 if the class being displayed has an identifier property that is 
 used as a key in the associated dictionary. 
 
    Prerequisites

 To use it, you need a NSArray with the items you want to display and
 a popover to display the view controller in.

 The array is expected tod consist of complex objects. You must therefore 
 provide a value for the DisplayMember parameter in the init method so 
 that the class knows which one of the properties to display in the table.
 Also, the ValueMember parameter tells the class which property to use
 to access the corresponding flag in the associated dictionary.
 
 The dictionary's key-value pairs are therefore of type NSString* and
 NSNumber*.

    Example

 // in the case illustrated below, assume that myArray is a simple array of NSString objects
 - (void)someMethod
 {
    // create an instance of the class, init with the items to be displayed
    TPPopoverMultiPickerViewController* mpVC = [[TPPopoverMultiPickerViewController alloc] 
        initWithItemArray:myArray Flags:myArrayFlags Title:title ValueMember:@"idProperty" 
        DisplayMember:@"name"];
    mpVC.delegate = self;
    self.popController = [[UIPopoverController alloc] initWithContentViewController:mpVC];
    self.popController.delegate = self;
    [popController presentPopoverFromBarButtonItem:button permittedArrowDirections:UIPopoverArrowDirectionUp 
                                        animated:YES];
 }

 // your class can implement the PopoverMultiPickerDelegate protocol. Note that both
 // of its methods are optional, because in most cases you will be interested in the
 // state of the flags and will not need to display any images next to the items in
 // the table view.

    Notes

 The popover does not close when a selection is made in order to 
 allow you to select multiple items. It only closes after a touch
 event is registered outside of the popover. 
 
*/


#import <UIKit/UIKit.h>

@protocol PSMultiSelectViewControllerDelegate <NSObject>
        
@optional
- (UIImage*)MPimageForItem:(id)item sender:(id)sender;
- (void)MPitemSelected:(id)item sender:(id)sender;
- (void)MPselectionDone:(id)sender;

@end

@interface PSMultiSelectViewController : UITableViewController {
}

- (id)initWithItemArray:(NSArray*)items Flags:(NSDictionary*)flags Title:(NSString*)title ValueMember:(NSString*)valmem DisplayMember:(NSString*)dispmem FilterName: (NSString*)filterName;
- (id)initWithItemArray:(NSArray*)items Flags:(NSDictionary*)flags Title:(NSString*)title ValueMember:(NSString*)valmem DisplayMember:(NSString*)dispmem Style: (UITableViewStyle)style;
-(void)selectionDone:(id)sender;
@property (nonatomic, assign) id<PSMultiSelectViewControllerDelegate> delegate;
@property (nonatomic, strong) NSArray* itemsToDisplay;
@property (nonatomic, strong) NSDictionary* itemFlags;
@property (nonatomic, strong) NSString* tableTitle;
@property (nonatomic, strong) NSString* displayMember;
@property (nonatomic, strong) NSString* valueMember;
@property (nonatomic, strong) NSString* tag;
@property (nonatomic, strong) NSString* filterName;
@property (nonatomic, strong) NSString* vcTitle;

@end
