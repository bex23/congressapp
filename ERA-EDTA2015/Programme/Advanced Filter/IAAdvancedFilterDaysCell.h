//
//  IAAdvancedFilterDaysCell.h
//  ERA-EDTA2015
//
//  Created by admin on 1/13/15.
//  Copyright (c) 2015 Navus. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol IAAdvancedFilterDaysCellDelegate <NSObject>

- (void) swDayValueChanged:(UISwitch*)swDay inCell: (UITableViewCell*)cell;

@end

@interface IAAdvancedFilterDaysCell : UITableViewCell

@property (strong, nonatomic) NSArray* sessions;
@property (strong, nonatomic) NSArray* daySwitches;
@property (strong, nonatomic) id <IAAdvancedFilterDaysCellDelegate> delegate;
@property (strong, nonatomic) IBOutlet UILabel *lblDay1;
@property (strong, nonatomic) IBOutlet UILabel *lblDay2;
@property (strong, nonatomic) IBOutlet UILabel *lblDay3;
@property (strong, nonatomic) IBOutlet UILabel *lblDay4;

@property (strong, nonatomic) IBOutlet UISwitch *swDay1;
@property (strong, nonatomic) IBOutlet UISwitch *swDay2;
@property (strong, nonatomic) IBOutlet UISwitch *swDay3;
@property (strong, nonatomic) IBOutlet UISwitch *swDay4;

- (IBAction)swDaysValueChangedAction:(UISwitch *)sender;
- (NSArray*)initialiseDaySwitches;
@end
