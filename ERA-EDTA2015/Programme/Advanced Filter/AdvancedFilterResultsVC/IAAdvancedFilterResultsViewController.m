//
//  IAAdvancedFilterResultsViewController.m
//  ERA-EDTA2015
//
//  Created by admin on 7/24/14.
//  Copyright (c) 2014 Navus. All rights reserved.
//

#import "IAAdvancedFilterResultsViewController.h"

#import "SessionDetailsViewController.h"
#import "SessionTableViewCell.h"

@interface IAAdvancedFilterResultsViewController ()

@property (weak, nonatomic) IBOutlet UILabel *lblCount;
@property (weak, nonatomic) IBOutlet UIImageView *imgTopBanner;


@end

@implementation IAAdvancedFilterResultsViewController

#pragma mark - View

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 300.0;
    

    
    // register presentation cell
    UINib *nib = [UINib nibWithNibName:NSStringFromClass([SessionTableViewCell class]) bundle:nil];
    [[self tableView] registerNib:nib forCellReuseIdentifier:NSStringFromClass([SessionTableViewCell class])];
    
    // set count of results
    self.lblCount.text = [NSString stringWithFormat:@"%lu", (unsigned long)self.sessions.count];
    
    [[BannersServices shared] addWithImageView:self.imgTopBanner
                                            at:BannerPositionTopBanner
                                   sender: self];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.view.userInteractionEnabled = YES;
}

#pragma mark - UITableView Data Source

- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section
{
	return self.sessions.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SessionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([SessionTableViewCell class]) forIndexPath:indexPath];
    
    Session *currentSession = [self.sessions objectAtIndex:indexPath.row];
    
   	cell.lblSessionTitle.text = currentSession.sesName;
    [cell setSessionNo:currentSession.sesCode];
    cell.lblRoomName.text = currentSession.venue.venName;
    
    [cell setTime:currentSession];
    
    cell.viewCategoryColor.backgroundColor = [SessionCategory getColorForSessionCategoryId:currentSession.sesCategoryId];
    
    // because on iOS 8 doesn't work
    // self.tableView.rowHeight = UITableViewAutomaticDimension;
    // self.tableView.estimatedRowHeight = 200.0;
    [cell updateConstraintsIfNeeded];
    
    return cell;
}

// because on iOS 8 doesn't work
// self.tableView.rowHeight = UITableViewAutomaticDimension;
// self.tableView.estimatedRowHeight = 200.0;
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

#pragma mark - UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSString *strID = NSStringFromClass([SessionDetailsViewController class]);
    SessionDetailsViewController* vc = [[UIStoryboard storyboardWithName:strID bundle:nil] instantiateViewControllerWithIdentifier:strID];

    Session *session = [self.sessions objectAtIndex:indexPath.row];
    [vc setSession:session];
    
    self.view.userInteractionEnabled = NO;
    
    [self.navigationController pushViewController:vc animated:YES];
}



#pragma mark - Set left margin of table cells to ziro for iOS 8

-(void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

@end
