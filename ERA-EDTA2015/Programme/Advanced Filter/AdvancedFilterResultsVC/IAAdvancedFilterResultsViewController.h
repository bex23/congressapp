//
//  IAAdvancedFilterResultsViewController.h
//  ERA-EDTA2015
//
//  Created by admin on 7/24/14.
//  Copyright (c) 2014 Navus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IAAdvancedFilterResultsViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, retain)NSArray* sessions;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end
