//
//  IAAdvancedFilterEmatCell.h
//  ERA-EDTA2015
//
//  Created by admin on 1/15/15.
//  Copyright (c) 2015 Navus. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol IAAdvancedFilterEmatCellDelegate <NSObject>

- (void) swEmatValueChanged:(UISwitch*)swDay inCell: (UITableViewCell*)cell;

@end

@interface IAAdvancedFilterEmatCell : UITableViewCell

@property (strong, nonatomic) id <IAAdvancedFilterEmatCellDelegate> delegate;
@property (strong, nonatomic) IBOutlet UISwitch *swEmat;

- (NSDictionary*)initialiseEMatSwitch;
- (IBAction)swEmatValueChanged:(UISwitch *)sender;
@end
