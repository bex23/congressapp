//
//  IAAdvancedFilterCell.h
//  ERA-EDTA2015
//
//  Created by admin on 7/17/14.
//  Copyright (c) 2014 Navus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IAAdvancedFilterCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblFilterName;
@property (strong, nonatomic) IBOutlet UILabel *lblSelectedValues;
@property (retain, nonatomic) NSDictionary* cellData;
@end
