//
//  TPCategoryDropDown.m
//  TourPad
//
//  Created by Mac on 11/9/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PSMultiSelectViewController.h"


@implementation PSMultiSelectViewController

@synthesize delegate, itemsToDisplay, itemFlags, tableTitle, valueMember, displayMember, tag;


- (id)initWithItemArray:(NSArray*)items Flags:(NSDictionary*)flags Title:(NSString*)title ValueMember:(NSString*)valmem DisplayMember:(NSString*)dispmem FilterName: (NSString*)filterName
{
    self = [super initWithStyle:UITableViewStylePlain];
    if (self) {
        // Custom initialization
        self.itemsToDisplay = items;
        self.itemFlags = flags;
        self.tableTitle = title;
        self.valueMember = valmem;
        self.displayMember = dispmem;
        self.filterName = filterName;
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
    }

    return self;
}

- (id)initWithItemArray:(NSArray*)items Flags:(NSDictionary*)flags Title:(NSString*)title ValueMember:(NSString*)valmem DisplayMember:(NSString*)dispmem Style: (UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        self.itemsToDisplay = items;
        self.itemFlags = flags;
        self.tableTitle = title;
        self.valueMember = valmem;
        self.displayMember = dispmem;
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
    }
    
    return self;
}
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(selectionDone:)];
    
    if(self.filterName)
    {
        self.title = self.filterName;
    }

    [self.tableView setTintColor:[UIColor colorWithHex:kOtherColor andAlpha:1.0]];
    
    // this is there to ensure that the popover does not take up the entire screen (the default behavior)
//    self.contentSizeForViewInPopover = CGSizeMake(340.0, 600.0);
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void)addBackButton
{
    UIButton *backButton = [[UIButton alloc] init];
    backButton.titleLabel.text = @"Done";
    [backButton addTarget:self action:@selector(selectionDone:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backBarButton = [[UIBarButtonItem alloc]initWithCustomView:backButton] ;
    self.navigationItem.leftBarButtonItem = backBarButton;
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    //	return YES;
    return  (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown);
}

#pragma mark - Table view data source
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger height;
    
    id item = [itemsToDisplay objectAtIndex:indexPath.row];
    
    NSMutableString *title = [item valueForKey:displayMember];
    
    CGSize constraint = CGSizeMake(300 - (10 * 2), 20000.0f);
    
    CGSize size = [self text:title sizeWhitFont:[UIFont systemFontOfSize:16] constrainedToSize:constraint];
    height = ceil(size.height) + 10 * 2;
    
//    NSMutableString *authorsFull = [NSMutableString string];
//	NSArray *authorsInPaper = [authors objectAtIndex:indexPath.row];
//	
//	if ([authorsInPaper count]>1) {
//		int i=0;
//		for (i=0; i<([authorsInPaper count]-1); i++) {
//			[authorsFull appendFormat:@"%@, ", [authorsInPaper objectAtIndex:i]];
//		}
//		[authorsFull appendFormat:@"and %@", [authorsInPaper lastObject]];
//	}else {
//        authorsFull = [authorsInPaper lastObject];
//	}
//    
//    size = [self text:authorsFull sizeWhitFont:[UIFont systemFontOfSize:14] constrainedToSize:constraint];
//    height = height + ceil(size.height) + 3* CELL_CONTENT_MARGIN + 40;
    
	return height;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return tableTitle;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [itemsToDisplay count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) 
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    // Configure the cell...
    id item = [itemsToDisplay objectAtIndex:indexPath.row];
    
    NSString *title = [item valueForKey:displayMember];
    
    //Size the label to fit all text
    CGSize size = [self text:title sizeWhitFont:cell.textLabel.font constrainedToSize:CGSizeMake(cell.textLabel.frame.size.width - (10 * 2), 2000)];
    
    [cell.textLabel setText:title];
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [cell.textLabel setFrame:CGRectMake(cell.textLabel.frame.origin.x, cell.textLabel.frame.origin.y, cell.textLabel.frame.size.width, ceil(size.height))];
    
    if([delegate respondsToSelector:@selector(MPimageForItem:sender:)])
    {
        cell.imageView.image = [delegate MPimageForItem:item sender:self];
    }
    NSString* item_id;
    // display checkmark in cell if subcategory is selected
    if([[item valueForKey:valueMember] respondsToSelector:@selector(stringValue)])
        item_id = [[item valueForKey:valueMember] stringValue];
    else
        item_id = [item valueForKey:valueMember];
    
    cell.tintColor = [UIColor colorWithHex:kOtherColor andAlpha:1.0];
    
    if([[itemFlags valueForKey:item_id] boolValue] == TRUE)
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;    
    }
    else 
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    return cell;
}

- (CGSize)text:(NSString*)text sizeWhitFont:(UIFont*) font constrainedToSize:(CGSize)constrainedSize
{
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          font, NSFontAttributeName,
                                          nil];
    
    CGRect frame = [text boundingRectWithSize:constrainedSize
                                      options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                   attributes:attributesDictionary
                                      context:nil];
    return frame.size;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(delegate)
    {
        [delegate MPitemSelected:[itemsToDisplay objectAtIndex:indexPath.row] sender:self];
    }
    
    id item = [itemsToDisplay objectAtIndex:indexPath.row];
    
    NSString* item_id;
    
    if([[item valueForKey:valueMember] respondsToSelector:@selector(stringValue)])
        item_id = [[item valueForKey:valueMember] stringValue];
    else
        item_id = [item valueForKey:valueMember];
    
    BOOL current_val = [[itemFlags valueForKey:item_id] boolValue];
    
    [itemFlags setValue:[NSNumber numberWithBool:!current_val] forKey:item_id];
    
    [tableView reloadData];
}

-(void)selectionDone:(id)sender
{
    [self.delegate MPselectionDone:self];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
