//
//  IAAdvancedFilterDaysCell.m
//  ERA-EDTA2015
//
//  Created by admin on 1/13/15.
//  Copyright (c) 2015 Navus. All rights reserved.
//

#import "IAAdvancedFilterDaysCell.h"

@implementation IAAdvancedFilterDaysCell

- (NSArray*)initialiseDaySwitches
{
    NSMutableArray* daySettings = [[NSMutableArray alloc] initWithCapacity:4];
    //add switches to array so we can iterate
    NSArray* daysSwitchesArray = [NSArray arrayWithObjects:self.swDay1, self.swDay2, self.swDay3,self.swDay4, nil];
    NSArray* daysLabelsArray = [NSArray arrayWithObjects:self.lblDay1, self.lblDay2, self.lblDay3, self.lblDay4, nil];
    
    NSMutableSet* dayDates = [[NSMutableSet alloc] initWithCapacity:4];
    
    for (Session* ses in self.sessions)
    {
        [dayDates addObject:[Utils getDate:ses.sesStartTime]];
    }
    
    NSArray* dayDatesArray = [dayDates allObjects];
    NSArray* sortedDayDatesArray = [dayDatesArray sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    for (int i=0 ; i < daysSwitchesArray.count; i++)
    {
        if(sortedDayDatesArray.count > i)
        {
            [daySettings addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:[daysSwitchesArray objectAtIndex:i], @"switch",[sortedDayDatesArray objectAtIndex:i] , @"date", nil]];
            ((UILabel*)[daysLabelsArray objectAtIndex:i]).text = [Utils formatDayString:[sortedDayDatesArray objectAtIndex:i]];
//            ((UISwitch*)[daysSwitchesArray objectAtIndex:i]).userInteractionEnabled = YES;
//            ((UISwitch*)[daysSwitchesArray objectAtIndex:i]).hidden = NO;
        }
        else
        {
            //disable switches
            ((UISwitch*)[daysSwitchesArray objectAtIndex:i]).userInteractionEnabled = NO;
            ((UISwitch*)[daysSwitchesArray objectAtIndex:i]).hidden = YES;
        }
    }
    self.daySwitches = daySettings;
    
    return self.daySwitches;
}

- (IBAction)swDaysValueChangedAction:(UISwitch *)sender
{
    if([self.delegate respondsToSelector:@selector(swDayValueChanged:inCell:)])
    {
        [self.delegate swDayValueChanged:sender inCell:self];
    }
}

@end
