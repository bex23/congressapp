//
//  IAAdvancedFilterViewController.h
//  ERA-EDTA2015
//
//  Created by admin on 7/17/14.
//  Copyright (c) 2014 Navus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IAAdvancedFilterDaysCell.h"
#import "IAAdvancedFilterEmatCell.h"

@interface IAAdvancedFilterViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, IAAdvancedFilterDaysCellDelegate, IAAdvancedFilterEmatCellDelegate>
{
    NSArray *sessions;
}
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIImageView *imgUpperBanner;

- (IBAction)btnResetAction:(id)sender;
- (IBAction)btnSearchAction:(id)sender;

@end
