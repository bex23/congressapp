//
//  HighlightedSlideComment+CoreDataClass.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 4/25/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "HighlightedSlideComment+CoreDataClass.h"
#import "HighlightedSlide+CoreDataClass.h"

@implementation HighlightedSlideComment

+(NSArray *)getHighlightedSlideCommentsForSlide:(NSNumber *)slideId
{
    NSArray *reslutArray = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"HighlightedSlideComment"
                                                                                 predicate:[NSPredicate predicateWithFormat:@"hscSlideId == %@", slideId]
                                                                           sortDescriptors:nil
                                                                                     inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    if(reslutArray.count > 0)
    {
        return reslutArray;
    }
    return nil;
}

@end
