//
//  ExhibitorRole+CoreDataProperties.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 6/16/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "ExhibitorRole+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface ExhibitorRole (CoreDataProperties)

+ (NSFetchRequest<ExhibitorRole *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *erExhibitorId;
@property (nullable, nonatomic, copy) NSNumber *erExhibitorRoleId;
@property (nullable, nonatomic, copy) NSNumber *erOrderPosition;
@property (nullable, nonatomic, copy) NSString *erRoleName;
@property (nullable, nonatomic, copy) NSNumber *erSessionId;
@property (nullable, nonatomic, retain) Exhibitor *exhibitor;
@property (nullable, nonatomic, retain) Session *session;

@end

NS_ASSUME_NONNULL_END
