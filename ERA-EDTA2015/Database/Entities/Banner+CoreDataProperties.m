//
//  Banner+CoreDataProperties.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 6/23/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "Banner+CoreDataProperties.h"

@implementation Banner (CoreDataProperties)

+ (NSFetchRequest<Banner *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Banner"];
}

@dynamic banBannerId;
@dynamic banResourceUrl;
@dynamic banTags;
@dynamic banExternalUrl;

@end
