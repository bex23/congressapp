//
//  Exhibitor+CoreDataClass.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 2/27/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Booth, Session;

NS_ASSUME_NONNULL_BEGIN

@interface Exhibitor : NSManagedObject

+(NSArray *)getExhibitors;
+(Exhibitor *)getExhibitorWithId:(NSNumber *)exhibitorId;
+(Exhibitor *)getExhibitorWithName:(NSString *)exhibitorName;

@end

NS_ASSUME_NONNULL_END

#import "Exhibitor+CoreDataProperties.h"
