//
//  TalkQA+CoreDataClass.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 3/15/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Talk;

NS_ASSUME_NONNULL_BEGIN

@interface TalkQA : NSManagedObject

+(TalkQA*)addQuestion:(NSDictionary *)params;
+(void)deleteQA:(TalkQA *)qa ForTalk:(Talk *)talk;
+(void)linkQAs;
+(NSArray*)getUnsyncedQAs;

@end

NS_ASSUME_NONNULL_END

#import "TalkQA+CoreDataProperties.h"
