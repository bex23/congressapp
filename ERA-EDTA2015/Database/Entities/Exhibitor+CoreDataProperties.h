//
//  Exhibitor+CoreDataProperties.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 8/29/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "Exhibitor+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Exhibitor (CoreDataProperties)

+ (NSFetchRequest<Exhibitor *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *exhAddress1;
@property (nullable, nonatomic, copy) NSString *exhAddress2;
@property (nullable, nonatomic, copy) NSNumber *exhBoothNo;
@property (nullable, nonatomic, copy) NSString *exhBoothNumber;
@property (nullable, nonatomic, copy) NSString *exhDescription;
@property (nullable, nonatomic, copy) NSString *exhEmail;
@property (nullable, nonatomic, copy) NSNumber *exhExhibitorId;
@property (nullable, nonatomic, copy) NSString *exhImage;
@property (nullable, nonatomic, copy) NSString *exhImageThumb;
@property (nullable, nonatomic, copy) NSString *exhName;
@property (nullable, nonatomic, copy) NSString *exhPhone;
@property (nullable, nonatomic, copy) NSString *exhWebsite;
@property (nullable, nonatomic, copy) NSString *exhImportedId;
@property (nullable, nonatomic, retain) NSSet<Booth *> *booths;
@property (nullable, nonatomic, retain) NSSet<ExhibitorRole *> *roles;

@end

@interface Exhibitor (CoreDataGeneratedAccessors)

- (void)addBoothsObject:(Booth *)value;
- (void)removeBoothsObject:(Booth *)value;
- (void)addBooths:(NSSet<Booth *> *)values;
- (void)removeBooths:(NSSet<Booth *> *)values;

- (void)addRolesObject:(ExhibitorRole *)value;
- (void)removeRolesObject:(ExhibitorRole *)value;
- (void)addRoles:(NSSet<ExhibitorRole *> *)values;
- (void)removeRoles:(NSSet<ExhibitorRole *> *)values;

@end

NS_ASSUME_NONNULL_END
