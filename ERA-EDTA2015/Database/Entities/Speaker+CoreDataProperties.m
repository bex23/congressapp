//
//  Speaker+CoreDataProperties.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 11/3/17.
//  Copyright © 2017 Navus. All rights reserved.
//
//

#import "Speaker+CoreDataProperties.h"

@implementation Speaker (CoreDataProperties)

+ (NSFetchRequest<Speaker *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Speaker"];
}

@dynamic spkBiography;
@dynamic spkFirstName;
@dynamic spkLastName;
@dynamic spkPicture;
@dynamic spkSpeakerId;
@dynamic spkTalkId;
@dynamic spkUserId;
@dynamic talks;

@end
