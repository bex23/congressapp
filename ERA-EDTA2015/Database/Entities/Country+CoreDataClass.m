//
//  Country+CoreDataClass.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 5/18/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "Country+CoreDataClass.h"

@implementation Country

+(NSString*)getNameForId:(int)country_id
{
    
    
    NSArray* arrayResults = [Country fetchManagedObjectsWithName:@"Country"
                                                       predicate:[NSPredicate predicateWithFormat:@"ctrCountryId == %d", country_id]
                                                 sortDescriptors:nil
                                                           inMOC:[Country getManagedObjectContext]];
    if (arrayResults.count)
    {
        return [arrayResults.lastObject valueForKey:@"ctrName"];
    }
    
    return nil;
}
+(Country*)getCountryWithId:(int)country_id
{
    NSArray* arrayResults = [Country fetchManagedObjectsWithName:@"Country"
                                                       predicate:[NSPredicate predicateWithFormat:@"ctrCountryId == %d", country_id]
                                                 sortDescriptors:nil
                                                           inMOC:[Country getManagedObjectContext]];
    if (arrayResults.count)
    {
        return arrayResults.lastObject;
    }
    
    return nil;
}

+(NSString*)getContryCodeForId:(int)country_id
{
    NSArray* arrayResults = [Country fetchManagedObjectsWithName:@"Country"
                                                       predicate:[NSPredicate predicateWithFormat:@"ctrCountryId == %d", country_id]
                                                 sortDescriptors:nil
                                                           inMOC:[Country getManagedObjectContext]];
    if (arrayResults.count)
    {
        return [arrayResults.lastObject valueForKey:@"ctrCode"];
    }
    
    return nil;
}

+(NSArray*)getCountries
{
    NSArray *sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"ctrName" ascending:NO]];
    return  [Country fetchManagedObjectsWithName:@"Country"
                                       predicate:nil
                                 sortDescriptors:sortDescriptors
                                           inMOC:[Country getManagedObjectContext]];
}

+(void)insertCountries:(NSArray*)countriesArray
{
    int savedctr = 0;
    
    if(countriesArray.count > 0)
    {
        for (NSDictionary* item in countriesArray)
        {
            Country* newCountry = (Country*)[NSEntityDescription insertNewObjectForEntityForName:@"Country" inManagedObjectContext:[Country getManagedObjectContext]];
            
            for(NSString* key in [item allKeys])
            {
                if ([newCountry respondsToSelector:NSSelectorFromString(key)])
                {
                    if([item objectForKey:key] != (id)[NSNull null])
                    {
                        [newCountry setValue:[item objectForKey:key] forKey:key];
                    }
                }
            }
            
            // save in smaller batches, which drastically speeds up insertion
            savedctr++;
            if(savedctr % 200 == 0)
            {
                [Country saveContext];
            }
        }
        
        //save what is left
        [Country saveContext];
    }
}

+(BOOL)hasData
{
    NSFetchRequest * allDataRequest = [[NSFetchRequest alloc] init];
    [allDataRequest setEntity:[NSEntityDescription entityForName:@"Country" inManagedObjectContext:[Country getManagedObjectContext]]];
    [allDataRequest setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSUInteger count = [[Country getManagedObjectContext] countForFetchRequest:allDataRequest error:&error];
    
    if(!error && count != 0)
    {
        return YES;
    }
    return NO;
}

+ (NSArray *) fetchManagedObjectsWithName:(NSString *)entityName predicate: (NSPredicate *)predicate sortDescriptors:(NSArray *)sortDescriptors inMOC:(NSManagedObjectContext *)moc
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:entityName inManagedObjectContext:moc]];
    
    // Add a sort descriptor. Mandatory.
    [fetchRequest setSortDescriptors:sortDescriptors];
    fetchRequest.predicate = predicate;
    
    NSError *error;
    NSArray *fetchResults = [moc executeFetchRequest:fetchRequest error:&error];
    
    if (fetchResults == nil) {
        // Handle the error.
        NSLog(@"executeFetchRequest failed with error: %@", [error localizedDescription]);
    }
    
    return fetchResults;
    
}

+(NSManagedObjectContext*) getManagedObjectContext
{
    return[LLDataAccessLayer sharedInstance].managedObjectContext;
}

+ (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = [Country getManagedObjectContext];
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}


@end
