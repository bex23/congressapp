//
//  Banner+CoreDataProperties.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 6/23/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "Banner+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Banner (CoreDataProperties)

+ (NSFetchRequest<Banner *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *banBannerId;
@property (nullable, nonatomic, copy) NSString *banResourceUrl;
@property (nullable, nonatomic, copy) NSString *banTags;
@property (nullable, nonatomic, copy) NSString *banExternalUrl;

@end

NS_ASSUME_NONNULL_END
