//
//  Venue+CoreDataProperties.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 5/19/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "Venue+CoreDataProperties.h"

@implementation Venue (CoreDataProperties)

+ (NSFetchRequest<Venue *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Venue"];
}

@dynamic venCapacity;
@dynamic venColor;
@dynamic venCoordX;
@dynamic venCoordY;
@dynamic venFloor;
@dynamic venIsRoom;
@dynamic venName;
@dynamic venStatus;
@dynamic venType;
@dynamic venVenueId;
@dynamic venImportedId;
@dynamic sessions;

@end
