//
//  Note+CoreDataClass.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 5/18/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Talk;

NS_ASSUME_NONNULL_BEGIN

@interface Note : NSManagedObject

+(Note *)insertNote:(NSDictionary *)dicNostes inContext:(NSManagedObjectContext *)moc;
+(Note *)deleteNote:(Note *)note;
+(void)linkNotes;
+(NSArray *)getUnsyncedNotes;
+(NSArray *)getNotesForTalk:(Talk *)talk;

@end

NS_ASSUME_NONNULL_END

#import "Note+CoreDataProperties.h"
