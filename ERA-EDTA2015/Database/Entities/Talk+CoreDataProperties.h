//
//  Talk+CoreDataProperties.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 11/3/17.
//  Copyright © 2017 Navus. All rights reserved.
//
//

#import "Talk+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Talk (CoreDataProperties)

+ (NSFetchRequest<Talk *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *timelineId;
@property (nullable, nonatomic, copy) NSNumber *timelineSynced;
@property (nullable, nonatomic, copy) NSNumber *tlkAllowQA;
@property (nullable, nonatomic, copy) NSNumber *tlkAvailable;
@property (nullable, nonatomic, copy) NSNumber *tlkAvailableMainMaterial;
@property (nullable, nonatomic, copy) NSNumber *tlkAvailableSupportingMaterial;
@property (nullable, nonatomic, copy) NSNumber *tlkAvailableWebcastMaterial;
@property (nullable, nonatomic, copy) NSString *tlkBoardNo;
@property (nullable, nonatomic, copy) NSNumber *tlkConsentCompleted;
@property (nullable, nonatomic, copy) NSString *tlkDescription;
@property (nullable, nonatomic, copy) NSString *tlkEndTime;
@property (nullable, nonatomic, copy) NSString *tlkExternalUrl;
@property (nullable, nonatomic, copy) NSNumber *tlkHasAudioMaterial;
@property (nullable, nonatomic, copy) NSNumber *tlkHasMainMaterial;
@property (nullable, nonatomic, copy) NSNumber *tlkHasSupportingMaterial;
@property (nullable, nonatomic, copy) NSNumber *tlkHasWebcastMaterial;
@property (nullable, nonatomic, copy) NSNumber *tlkImportedId;
@property (nullable, nonatomic, copy) NSNumber *tlkSessionId;
@property (nullable, nonatomic, copy) NSString *tlkSpeakerImageUrl;
@property (nullable, nonatomic, copy) NSString *tlkSpeakerName;
@property (nullable, nonatomic, copy) NSString *tlkStartTime;
@property (nullable, nonatomic, copy) NSString *tlkStatus;
@property (nullable, nonatomic, copy) NSNumber *tlkTalkId;
@property (nullable, nonatomic, copy) NSString *tlkTitle;
@property (nullable, nonatomic, copy) NSString *tlkType;
@property (nullable, nonatomic, copy) NSNumber *tlkVoted;
@property (nullable, nonatomic, copy) NSNumber *tlkVoteSynced;
@property (nullable, nonatomic, copy) NSNumber *tlkVotingEnabled;
@property (nullable, nonatomic, copy) NSNumber *zetBookmarked;
@property (nullable, nonatomic, copy) NSString *zetDay;
@property (nullable, nonatomic, copy) NSNumber *tlkVoteAvailable;
@property (nullable, nonatomic, retain) NSSet<Material *> *materials;
@property (nullable, nonatomic, retain) NSSet<Note *> *notes;
@property (nullable, nonatomic, retain) NSSet<Order *> *orders;
@property (nullable, nonatomic, retain) NSSet<TalkQA *> *questions;
@property (nullable, nonatomic, retain) Session *session;
@property (nullable, nonatomic, retain) NSSet<Speaker *> *speakrs;

@end

@interface Talk (CoreDataGeneratedAccessors)

- (void)addMaterialsObject:(Material *)value;
- (void)removeMaterialsObject:(Material *)value;
- (void)addMaterials:(NSSet<Material *> *)values;
- (void)removeMaterials:(NSSet<Material *> *)values;

- (void)addNotesObject:(Note *)value;
- (void)removeNotesObject:(Note *)value;
- (void)addNotes:(NSSet<Note *> *)values;
- (void)removeNotes:(NSSet<Note *> *)values;

- (void)addOrdersObject:(Order *)value;
- (void)removeOrdersObject:(Order *)value;
- (void)addOrders:(NSSet<Order *> *)values;
- (void)removeOrders:(NSSet<Order *> *)values;

- (void)addQuestionsObject:(TalkQA *)value;
- (void)removeQuestionsObject:(TalkQA *)value;
- (void)addQuestions:(NSSet<TalkQA *> *)values;
- (void)removeQuestions:(NSSet<TalkQA *> *)values;

- (void)addSpeakrsObject:(Speaker *)value;
- (void)removeSpeakrsObject:(Speaker *)value;
- (void)addSpeakrs:(NSSet<Speaker *> *)values;
- (void)removeSpeakrs:(NSSet<Speaker *> *)values;

@end

NS_ASSUME_NONNULL_END
