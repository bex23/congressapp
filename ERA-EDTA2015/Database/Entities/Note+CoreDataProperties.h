//
//  Note+CoreDataProperties.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 5/18/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "Note+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Note (CoreDataProperties)

+ (NSFetchRequest<Note *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *ntDate;
@property (nullable, nonatomic, copy) NSNumber *ntDeleted;
@property (nullable, nonatomic, copy) NSString *ntNote;
@property (nullable, nonatomic, copy) NSNumber *ntNoteId;
@property (nullable, nonatomic, copy) NSNumber *ntTalkId;
@property (nullable, nonatomic, copy) NSNumber *synced;
@property (nullable, nonatomic, retain) Talk *talk;

@end

NS_ASSUME_NONNULL_END
