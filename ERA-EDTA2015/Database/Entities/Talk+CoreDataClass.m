//
//  Talk+CoreDataClass.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 12/28/16.
//  Copyright © 2016 Navus. All rights reserved.
//

#import "Talk+CoreDataClass.h"
#import "Material+CoreDataClass.h"
#import "Note+CoreDataClass.h"
#import "Order+CoreDataClass.h"
#import "Session+CoreDataClass.h"
#import "Speaker+CoreDataClass.h"
#import "TalkQA+CoreDataClass.h"
@implementation Talk

// Insert code here to add functionality to your managed object subclass
+(NSArray *)getTalks
{
    NSArray* tlkArray = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Talk"
                                                                              predicate:nil
                                                                        sortDescriptors:nil
                                                                                  inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    return tlkArray;
}

+(NSArray<Talk *> *)getTalksWithVoting
{
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"tlkStartTime" ascending:YES];
    NSArray* tlkArray = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Talk"
                                                                              predicate:[NSPredicate predicateWithFormat:@"tlkVotingEnabled = 1"]
                                                                        sortDescriptors:@[sortDescriptor]
                                                                                  inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    return tlkArray;
}

+(void)deleteSpeakersForTalks
{
    NSArray* tlkArray = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Talk"
                                                                              predicate:nil
                                                                        sortDescriptors:nil
                                                                                  inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    for (Talk *talk in tlkArray)
    {
        NSSet *setSpeakers = talk.speakrs;
        [talk removeSpeakrs:setSpeakers];
    }
    
    NSError *error;
    if (![[LLDataAccessLayer sharedInstance].managedObjectContext save:&error])
    {
        NSLog(@"Error saving main context. Linking. %@", error.userInfo);
    }
}

+(Talk*)getTalkWithId:(NSNumber *)talkId
{
    NSArray* tlkArray = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Talk"
                                                                              predicate:[NSPredicate predicateWithFormat:@"tlkTalkId == %@", talkId]
                                                                        sortDescriptors:nil
                                                                                  inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    if(tlkArray.count > 0)
    {
        return [tlkArray firstObject];
    }
    return nil;
}

+(Talk*)getTalkWithId:(NSString*)talkId usingMOC:(NSManagedObjectContext*)moc
{
    NSArray* tlkArray = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Talk"
                                                                              predicate:[NSPredicate predicateWithFormat:@"tlkTalkId == %@ ", talkId]
                                                                        sortDescriptors:nil
                                                                                  inMOC:moc];
    if(tlkArray.count > 0)
    {
        return [tlkArray firstObject];
    }
    return nil;
}

+(NSArray*)getUnsyncedTimelineTalks
{
    NSArray* tlkArray = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Talk"
                                                                              predicate:[NSPredicate predicateWithFormat:@"timelineSynced == 0"]
                                                                        sortDescriptors:nil
                                                                                  inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    return tlkArray;
}

+(NSArray*)getUnsyncedVotedTalks
{
    NSArray* tlkArray = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Talk"
                                                                              predicate:[NSPredicate predicateWithFormat:@"tlkVoteSynced == 0"]
                                                                        sortDescriptors:nil
                                                                                  inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    return tlkArray;
}

+(NSArray*)getTimelinedTalks
{
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"tlkStartTime" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    
    NSArray* tlkArray = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Talk"
                                                                              predicate:[NSPredicate predicateWithFormat:@"zetBookmarked = 1"]
                                                                        sortDescriptors:sortDescriptors
                                                                                  inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    if(tlkArray.count > 0)
    {
        return tlkArray;
    }
    
    return nil;
}

+(NSArray*)getVotedTalks
{
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"tlkStartTime" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    
    NSArray* tlkArray = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Talk"
                                                                              predicate:[NSPredicate predicateWithFormat:@"tlkVoted = 1"]
                                                                        sortDescriptors:sortDescriptors
                                                                                  inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    if(tlkArray.count > 0)
    {
        return tlkArray;
    }
    
    return nil;
}

+(void)updateSavedTimelineBeforeUpdate:(NSArray *)talksArray
{
    NSMutableArray* receivedItemsIDsInTimeline = [[NSMutableArray alloc] initWithCapacity:10];
    NSMutableArray* receivedItemsIDsNotInTimeline = [[NSMutableArray alloc] initWithCapacity:10];

    // first extract the IDs of the mapItems we just received and sort them
    for(id item in talksArray)
    {
        if(item != (id)[NSNull null])
        {
            if ([(NSNumber *)[item objectForKey:@"zetBookmarked"] isEqual:@1])
            {
                [receivedItemsIDsInTimeline addObject:(NSString *)[item objectForKey:@"presentation_id"]];
            }
            else
            {
                [receivedItemsIDsNotInTimeline addObject:(NSString *)[item objectForKey:@"presentation_id"]];
            }
            
            //            [receivedItems setObject:(NSString *)[item objectForKey:@"presentation_id"] forKey:(NSString*)[item objectForKey:@"presentation_id"]];
        }
    }
    [receivedItemsIDsInTimeline sortUsingSelector:@selector(compare:)];
    [receivedItemsIDsNotInTimeline sortUsingSelector:@selector(compare:)];
    // then get the IDs of all existing mapItems and sort them
    //            NSSortDescriptor* descriptor = [[NSSortDescriptor alloc] initWithKey:predicateKeyAttribute ascending:YES];
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"tlkTalkId IN %@", receivedItemsIDsInTimeline];//,predicateKeyAttribute, receivedItemsIDs
    
    NSArray* resultArray = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Talk"
                                                                                 predicate:predicate
                                                                           sortDescriptors:nil
                                                                                     inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    for(Talk *tlk in resultArray)
    {
        tlk.zetBookmarked = @1;
        tlk.timelineSynced = @NO;
    }
    
    [[LLDataAccessLayer sharedInstance] saveContextWithMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    
    // remove presentations from timeline; they deleted from web
    NSPredicate* predicate1 = [NSPredicate predicateWithFormat:@"tlkTalkId IN %@", receivedItemsIDsNotInTimeline];//,predicateKeyAttribute, receivedItemsIDs
    
    NSArray* talksNotInTimeline = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Talk"
                                                                                        predicate:predicate1
                                                                                  sortDescriptors:nil
                                                                                            inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    
    
    for(Talk *tlk in talksNotInTimeline)
    {
        tlk.zetBookmarked = @0;
        tlk.timelineSynced = @NO;
    }
    
    [[LLDataAccessLayer sharedInstance] saveContextWithMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
}

+(void)updateSavedVoteBeforeUpdate:(NSArray *)talksArray
{
    NSMutableArray* receivedItemsIDsVoted = [[NSMutableArray alloc] initWithCapacity:10];
    NSMutableArray* receivedItemsIDsUnvoted = [[NSMutableArray alloc] initWithCapacity:10];
    
    // first extract the IDs of the mapItems we just received and sort them
    for(id item in talksArray)
    {
        if(item != (id)[NSNull null])
        {
            if ([(NSNumber *)[item objectForKey:@"tlkVoted"] isEqual:@1])
            {
                [receivedItemsIDsVoted addObject:(NSString *)[item objectForKey:@"presentation_id"]];
            }
            else
            {
                [receivedItemsIDsUnvoted addObject:(NSString *)[item objectForKey:@"presentation_id"]];
            }
        }
    }
    [receivedItemsIDsVoted sortUsingSelector:@selector(compare:)];
    [receivedItemsIDsUnvoted sortUsingSelector:@selector(compare:)];
    // then get the IDs of all existing mapItems and sort them
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"tlkTalkId IN %@", receivedItemsIDsVoted];
    
    NSArray* resultArray = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Talk"
                                                                                 predicate:predicate
                                                                           sortDescriptors:nil
                                                                                     inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    for(Talk *tlk in resultArray)
    {
        tlk.tlkVoted = @1;
        tlk.tlkVoteSynced = @NO;
    }
    
    [[LLDataAccessLayer sharedInstance] saveContextWithMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    
    // remove presentations from timeline; they deleted from web
    NSPredicate* predicate1 = [NSPredicate predicateWithFormat:@"tlkTalkId IN %@", receivedItemsIDsUnvoted];//,predicateKeyAttribute, receivedItemsIDs
    
    NSArray* talksUnvoted = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Talk"
                                                                                        predicate:predicate1
                                                                                  sortDescriptors:nil
                                                                                            inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    
    
    for(Talk *tlk in talksUnvoted)
    {
        tlk.tlkVoted = @0;
        tlk.tlkVoteSynced = @NO;
    }
    
    [[LLDataAccessLayer sharedInstance] saveContextWithMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
}

+(void)updateTimeline:(NSArray *)talksArray
{
    NSMutableArray* receivedItemsIDs = [[NSMutableArray alloc] initWithCapacity:10];
    
    // first extract the IDs of the mapItems we just received and sort them
    for(id item in talksArray)
    {
        if(item != (id)[NSNull null])
        {
            [receivedItemsIDs addObject:(NSString *)[item objectForKey:@"presentation_id"]];
        }
    }
    [receivedItemsIDs sortUsingSelector:@selector(compare:)];
    // then get the IDs of all existing mapItems and sort them
    
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"tlkTalkId IN %@", receivedItemsIDs];
    
    NSArray* resultArray = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Talk"
                                                                                 predicate:predicate
                                                                           sortDescriptors:nil
                                                                                     inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    for(Talk *tlk in resultArray)
    {
        tlk.zetBookmarked = @1;
        tlk.timelineSynced = @YES;
    }
    
    [[LLDataAccessLayer sharedInstance] saveContextWithMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    
    // remove presentations from timeline; they deleted from web
    NSPredicate* predicate1 = [NSPredicate predicateWithFormat:@"zetBookmarked = 1 AND timelineSynced = 1 AND NOT (tlkTalkId IN %@)", receivedItemsIDs];
    
    NSArray* talksNotInTimeline = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Talk"
                                                                                 predicate:predicate1
                                                                           sortDescriptors:nil
                                                                                     inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    
    
    for(Talk *tlk in talksNotInTimeline)
    {
        tlk.zetBookmarked = nil;
        tlk.timelineSynced = nil;
    }
    
    [[LLDataAccessLayer sharedInstance] saveContextWithMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"TimelineSynced" object:nil];
}

+(void)updateVote:(NSArray *)talksArray
{
    NSMutableArray* receivedItemsIDs = [[NSMutableArray alloc] initWithCapacity:10];
    
    // first extract the IDs of the mapItems we just received and sort them
    for(id item in talksArray)
    {
        if(item != (id)[NSNull null])
        {
            [receivedItemsIDs addObject:(NSString *)item];
        }
    }
    [receivedItemsIDs sortUsingSelector:@selector(compare:)];
    
    // then get the IDs of all existing mapItems and sort them
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"tlkTalkId IN %@", receivedItemsIDs];
    
    NSArray* resultArray = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Talk"
                                                                                 predicate:predicate
                                                                           sortDescriptors:nil
                                                                                     inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    for(Talk *tlk in resultArray)
    {
        tlk.tlkVoted = @1;
        tlk.tlkVoteSynced = @YES;
    }
    
    [[LLDataAccessLayer sharedInstance] saveContextWithMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    
    // remove presentations from timeline; they deleted from web
    NSPredicate* predicate1 = [NSPredicate predicateWithFormat:@"tlkVoted = 1 AND tlkVoteSynced = 1 AND NOT (tlkTalkId IN %@)", receivedItemsIDs];
    
    NSArray* talksUnvoted = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Talk"
                                                                                        predicate:predicate1
                                                                                  sortDescriptors:nil
                                                                                            inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    
    
    for(Talk *tlk in talksUnvoted)
    {
        tlk.tlkVoted = nil;
        tlk.tlkVoteSynced = nil;
    }
    
    [[LLDataAccessLayer sharedInstance] saveContextWithMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
}

+(void)resetMyTimeline
{
    NSArray* tlkArray = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Talk"
                                                                              predicate:[NSPredicate predicateWithFormat:@"zetBookmarked == 1"]
                                                                        sortDescriptors:nil
                                                                                  inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    if(tlkArray.count > 0)
    {
        for(Talk* tlk in tlkArray)
        {
            tlk.timelineSynced = @NO;
        }
    }
    
    [[LLDataAccessLayer sharedInstance] saveContextWithMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
}

+(void)resetVotes
{
    NSArray* tlkArray = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Talk"
                                                                              predicate:[NSPredicate predicateWithFormat:@"tlkVoted == 1"]
                                                                        sortDescriptors:nil
                                                                                  inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    if(tlkArray.count > 0)
    {
        for(Talk* tlk in tlkArray)
        {
            tlk.tlkVoteSynced = @NO;
        }
    }
    
    [[LLDataAccessLayer sharedInstance] saveContextWithMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
}

+(void)updateTimeline:(Talk*)talk isAddition:(BOOL)isAddition
{
    talk.zetBookmarked = [NSNumber numberWithBool:isAddition];
    talk.timelineSynced = @NO;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (![User current] && !isAddition)
        {
            talk.zetBookmarked = nil;
            talk.timelineSynced = nil;
        }
        
        [[LLDataAccessLayer sharedInstance] saveContextWithMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
        
        if ([talk.timelineSynced isEqual: @NO])
        {
            if (isAddition)
            {
                [[ESyncData sharedInstance] addTalksToTimeline:@[talk.tlkTalkId]];
            }
            else
            {
                [[ESyncData sharedInstance] removeTalksFromTimeline:@[talk.tlkTalkId]];
            }
        }
    });
}

+(void)updateVote:(Talk*)talk isAddition:(BOOL)isAddition
{
    talk.tlkVoted = [NSNumber numberWithBool:isAddition];
    talk.tlkVoteSynced = @NO;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (![User current] && !isAddition)
        {
            talk.tlkVoted = nil;
            talk.tlkVoteSynced = nil;
        }
        
        [[LLDataAccessLayer sharedInstance] saveContextWithMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
        
        if ([talk.tlkVoteSynced isEqual: @NO])
        {
            if (isAddition)
            {
                [[ESyncData sharedInstance] voteTalks:@[talk.tlkTalkId]];
            }
            else
            {
                [[ESyncData sharedInstance] unvotedTalks:@[talk.tlkTalkId]];
            }
        }
    });
}

+(void)addMaterialsWithoutDelete:(NSArray *)arrayMaterials forPresentation:(NSNumber *)presentationId
{
    //    {
    //        available = 1;
    //        description = "<null>";
    //        disclaimer = 0;
    //        downloadable = 1;
    //        "file_name" = "presentazione_edta-21_05_16.pdf";
    //        "file_type" = pdf;
    //        id = 690;
    //        "presentation_id" = 8339;
    //        "thumb_path" = "<null>";
    //        title = "Marsupialized catheter with incremental dialysis dose, key to increased peritoneal dialysis prevalence in elderly";
    //        type = main;
    //        version = 0;
    //        watermark = 0;
    //    }
    Talk *talk = [Talk getTalkWithId:presentationId];
    
    for (NSDictionary *dictMaterial in arrayMaterials)
    {
        Material *material = [Material getMetarialWithId:dictMaterial[@"id"]];
        
        if (!material)
        {
            material = [NSEntityDescription insertNewObjectForEntityForName:@"Material" inManagedObjectContext:[LLDataAccessLayer sharedInstance].managedObjectContext];
            
            material.matTalkId = presentationId;
            material.matMaterialId = dictMaterial[@"id"];
            
            material.matName = (![dictMaterial[@"title"] isKindOfClass:[NSNull class]]) ? dictMaterial[@"title"] : @"";
            material.matType = (![dictMaterial[@"type"] isKindOfClass:[NSNull class]]) ? dictMaterial[@"type"] : @"";
            material.matAvailableDownload = dictMaterial[@"downloadable"];
            material.matFileUrl = (![dictMaterial[@"external_url"] isKindOfClass:[NSNull class]]) ? dictMaterial[@"external_url"] : @"";
            
            material.talk = talk;
        }
        else // material exist
        {
            material.matName = (![dictMaterial[@"title"] isKindOfClass:[NSNull class]]) ? dictMaterial[@"title"] : @"";
            material.matType = (![dictMaterial[@"type"] isKindOfClass:[NSNull class]]) ? dictMaterial[@"type"] : @"";
            material.matAvailableDownload = dictMaterial[@"downloadable"];
            material.matFileUrl = (![dictMaterial[@"external_url"] isKindOfClass:[NSNull class]]) ? dictMaterial[@"external_url"] : @"";
        }
        
        // save parent to disk asynchronously
        [[LLDataAccessLayer sharedInstance].managedObjectContext performBlock:^{
            NSError *error;
            if (![[LLDataAccessLayer sharedInstance].managedObjectContext save:&error])
            {
                NSLog(@"Error saving main context. Linking. %@", error.userInfo);
            }
        }];
    }
}

+(void)addMaterials:(NSArray *)arrayMaterials forPresentation:(NSNumber *)presentationId
{
//    {
//        available = 1;
//        description = "<null>";
//        disclaimer = 0;
//        downloadable = 1;
//        "file_name" = "presentazione_edta-21_05_16.pdf";
//        "file_type" = pdf;
//        id = 690;
//        "presentation_id" = 8339;
//        "thumb_path" = "<null>";
//        title = "Marsupialized catheter with incremental dialysis dose, key to increased peritoneal dialysis prevalence in elderly";
//        type = main;
//        version = 0;
//        watermark = 0;
//    }
    Talk *talk = [Talk getTalkWithId:presentationId];
    
    [[LLDataAccessLayer sharedInstance] deleteAllDataForEntity:@"Material" withPredicate:[NSPredicate predicateWithFormat:@"matTalkId == %@", presentationId]];
    
    for (NSDictionary *dictMaterial in arrayMaterials)
    {
        Material *material = [Material getMetarialWithId:dictMaterial[@"id"]];
        
        if (!material)
        {
            material = [NSEntityDescription insertNewObjectForEntityForName:@"Material" inManagedObjectContext:[LLDataAccessLayer sharedInstance].managedObjectContext];
            
            material.matTalkId = presentationId;
            material.matMaterialId = dictMaterial[@"id"];
            
            material.matName = (![dictMaterial[@"title"] isKindOfClass:[NSNull class]]) ? dictMaterial[@"title"] : @"";
            material.matType = (![dictMaterial[@"type"] isKindOfClass:[NSNull class]]) ? dictMaterial[@"type"] : @"";
            material.matAvailableDownload = dictMaterial[@"downloadable"];
            
            material.talk = talk;
        }
        else // material exist
        {
            material.matName = (![dictMaterial[@"title"] isKindOfClass:[NSNull class]]) ? dictMaterial[@"title"] : @"";
            material.matType = (![dictMaterial[@"type"] isKindOfClass:[NSNull class]]) ? dictMaterial[@"type"] : @"";
            material.matAvailableDownload = dictMaterial[@"downloadable"];
        }
        
        if (dictMaterial[@"external_url"] == nil  || [dictMaterial[@"external_url"] isKindOfClass:[NSNull class]]) {
            if ([dictMaterial[@"type"] isEqualToString:@"webcast"]) {
                material.matFileUrl = [NSString stringWithFormat:@"%@%@/webcast?token=%@",
                                       [[RealmRepository shared] streamingBaseURL],
                                       material.matMaterialId,
                                       [[User current] token]];
            } else {
                material.matFileUrl = [NSString stringWithFormat:@"%@materials/%@/view?token=%@",
                                       kBaseAPIURL,
                                       material.matMaterialId,
                                       [[User current] token]];
            }
        } else {
            material.matFileUrl = dictMaterial[@"external_url"];
        }
        
        // save parent to disk asynchronously
        [[LLDataAccessLayer sharedInstance].managedObjectContext performBlock:^{
            NSError *error;
            if (![[LLDataAccessLayer sharedInstance].managedObjectContext save:&error])
            {
                NSLog(@"Error saving main context. Linking. %@", error.userInfo);
            }
        }];
    }
}

+(NSArray *)getOrderedTalks
{
    NSArray* resultsArray = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Talk"
                                                                                  predicate:[NSPredicate predicateWithFormat:@"orders.@count != 0"]
                                                                            sortDescriptors:nil
                                                                                      inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    
    if(resultsArray.count > 0)
    {
        return resultsArray;
    }
    return nil;
}

+(void)linkTalksAndSpeakrs:(NSArray *)arraySpeakrsPresentations
{
    NSManagedObjectContext *temporaryContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    temporaryContext.parentContext = [LLDataAccessLayer sharedInstance].managedObjectContext;
    
    [temporaryContext performBlock:^{
        
        for (NSDictionary *dicTalksSpeakrs in arraySpeakrsPresentations)
        {
            Talk *talk = [Talk getTalkWithId:[dicTalksSpeakrs objectForKey:@"presentation_id"] usingMOC:temporaryContext];
            Speaker *speaker = [Speaker getSpeakerWithId:[dicTalksSpeakrs objectForKey:@"party_id"] usingMOC:temporaryContext];
            
            if (talk && speaker)
            {
                if (![speaker.talks containsObject:talk])
                {
                    [speaker addTalksObject:talk];
                }
                
                NSError *error;
                if (![temporaryContext save:&error])
                {
                    // handle error
                    NSLog(@"Error saving temp context. Linking. %@", error.userInfo);
                }
                
                // save parent to disk asynchronously
                [[LLDataAccessLayer sharedInstance].managedObjectContext performBlock:^{
                    NSError *error;
                    if (![[LLDataAccessLayer sharedInstance].managedObjectContext save:&error])
                    {
                        NSLog(@"Error saving main context. Linking. %@", error.userInfo);
                    }
                }];
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            ConferenceCoreDataLink *link = [[RealmRepository shared] link];
            link.presentationSpeaker = NO;
            [[RealmRepository shared] persistLink:link];
        });

        
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"Linked"] object:self];
    }];
}

+(NSArray*)getAvailableTalks
{
    NSArray* tlkArray = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Talk"
                                                                              predicate:[NSPredicate predicateWithFormat:@"tlkAvailableMainMaterial == 1 || tlkAvailableWebcastMaterial == 1 || tlkAvailableSupportingMaterial == 1"]
                                                                        sortDescriptors:nil
                                                                                  inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    return tlkArray;
}

-(NSString *)timeframe {
    NSDateFormatter *df = [NSDateFormatter new];
    df.dateFormat = kDateFormatInDatabase;
    NSDate *start = [df dateFromString:self.tlkStartTime];
    NSDate *end = [df dateFromString:self.tlkEndTime];
    df.dateFormat = kTimeDefaultFormat;
    NSString *startString = [df stringFromDate:start];
    NSString *endString = [df stringFromDate:end];
//    df.dateFormat = kDateFormat;
//    NSString *startDateString = [df stringFromDate:start];
//    return [NSString stringWithFormat:@"%@\n%@-%@", startDateString, startString, endString];
    return [NSString stringWithFormat:@"%@-%@", startString, endString];
}

-(NSString *)speakerName {
    if (self.tlkSpeakerName.length) {
        return self.tlkSpeakerName;
    } else {
        return @"N/A";
    }
}

@end
