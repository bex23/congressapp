//
//  SessionCategory+CoreDataClass.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 5/18/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Session;

NS_ASSUME_NONNULL_BEGIN

@interface SessionCategory : NSManagedObject

+(UIColor*)getColorForSessionCategoryId:(NSNumber*) secCategoryId;
+(SessionCategory*)getSessionCategoryWithId:(NSNumber*) secCategoryId;

@end

NS_ASSUME_NONNULL_END

#import "SessionCategory+CoreDataProperties.h"
