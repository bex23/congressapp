//
//  ExhibitorRole+CoreDataClass.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 2/28/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "ExhibitorRole+CoreDataClass.h"
@implementation ExhibitorRole

+(ExhibitorRole *)getExhibitorRoleForExhibitorId:(NSString *)exhibitorId inMOC:(NSManagedObjectContext *)moc
{
    NSArray* exhRolesArray = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"ExhibitorRole"
                                                                              predicate:[NSPredicate predicateWithFormat:@"erExhibitorId == %@", exhibitorId]
                                                                        sortDescriptors:nil
                                                                                  inMOC:moc];
    if(exhRolesArray.count > 0)
    {
        return [exhRolesArray firstObject];
    }
    return nil;
}

+(NSArray *)getExhibitorsWithRole:(NSString *)role
{
    NSArray* exhRolesArray = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"ExhibitorRole"
                                                                                   predicate:[NSPredicate predicateWithFormat:@"erRoleName == %@", role]
                                                                             sortDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"exhibitor.exhName" ascending:YES]]
                                                                                       inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    if(exhRolesArray.count > 0)
    {
        return exhRolesArray;
    }
    
    return nil;
}

@end
