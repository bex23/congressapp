//
//  HighlightedSlideComment+CoreDataProperties.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 5/18/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "HighlightedSlideComment+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface HighlightedSlideComment (CoreDataProperties)

+ (NSFetchRequest<HighlightedSlideComment *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *hscComment;
@property (nullable, nonatomic, copy) NSNumber *hscCommentId;
@property (nullable, nonatomic, copy) NSNumber *hscSlideId;
@property (nullable, nonatomic, retain) HighlightedSlide *slide;

@end

NS_ASSUME_NONNULL_END
