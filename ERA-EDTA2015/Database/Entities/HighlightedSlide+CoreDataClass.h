//
//  HighlightedSlide+CoreDataClass.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 4/24/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface HighlightedSlide : NSManagedObject

+(HighlightedSlide *)getHighlitedSlide:(NSNumber *)page forMaterial:(NSNumber *)materialId;
+(HighlightedSlide *)getHighlitedSlide:(NSNumber *)slideId;

@end

NS_ASSUME_NONNULL_END

#import "HighlightedSlide+CoreDataProperties.h"
