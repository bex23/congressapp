//
//  Talk+CoreDataClass.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 12/28/16.
//  Copyright © 2016 Navus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Material, Note, Order, Session, Speaker, TalkQA;

NS_ASSUME_NONNULL_BEGIN

@interface Talk : NSManagedObject

// Insert code here to declare functionality of your managed object subclass
+(Talk*)getTalkWithId:(NSNumber *)talkId;
+(NSArray *)getTalks;
+(NSArray<Talk *> *)getTalksWithVoting;
+(Talk*)getTalkWithId:(NSString*)talkId usingMOC:(NSManagedObjectContext*)moc;
+(void)updateTimeline:(NSArray*)talksArray;
+(NSArray*)getUnsyncedTimelineTalks;
+(void)resetMyTimeline;
+(void)updateTimeline:(Talk*)talk isAddition:(BOOL)isAddition;
+(void)addMaterials:(NSArray *)arrayMaterials forPresentation:(NSNumber *)presentationId;
+(void)addMaterialsWithoutDelete:(NSArray *)arrayMaterials forPresentation:(NSNumber *)presentationId;
+(void)deleteSpeakersForTalks;
+(void)linkTalksAndSpeakrs:(NSArray *)arraySpeakrsPresentations;
+(NSArray*)getTimelinedTalks;
+(NSArray*)getAvailableTalks;
+(void)updateSavedTimelineBeforeUpdate:(NSArray *)talksArray;
// Vote
+(NSArray*)getVotedTalks;
+(void)updateSavedVoteBeforeUpdate:(NSArray *)talksArray;
+(void)resetVotes;
+(void)updateVote:(NSArray *)talksArray;
+(NSArray*)getUnsyncedVotedTalks;
+(void)updateVote:(Talk*)talk isAddition:(BOOL)isAddition;
-(NSString *)timeframe;
-(NSString *)speakerName;
@end

NS_ASSUME_NONNULL_END

#import "Talk+CoreDataProperties.h"
