//
//  SurveyAnswers+CoreDataProperties.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 5/18/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "SurveyAnswers+CoreDataProperties.h"

@implementation SurveyAnswers (CoreDataProperties)

+ (NSFetchRequest<SurveyAnswers *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"SurveyAnswers"];
}

@dynamic suaContent;
@dynamic suaQuestionId;
@dynamic suaSurveyId;
@dynamic synced;

@end
