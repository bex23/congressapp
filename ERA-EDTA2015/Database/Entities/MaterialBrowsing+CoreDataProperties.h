//
//  MaterialBrowsing+CoreDataProperties.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 9/26/17.
//  Copyright © 2017 Navus. All rights reserved.
//
//

#import "MaterialBrowsing+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface MaterialBrowsing (CoreDataProperties)

+ (NSFetchRequest<MaterialBrowsing *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *objectId;
@property (nullable, nonatomic, copy) NSNumber *duration;
@property (nullable, nonatomic, copy) NSString *relation;
@property (nullable, nonatomic, copy) NSString *objectType;
@property (nullable, nonatomic, copy) NSString *sessionId;
@property (nullable, nonatomic, copy) NSString *objectInfo;
@property (nullable, nonatomic, copy) NSNumber *synced;
@property (nullable, nonatomic, copy) NSNumber *lastRecordTime;

@end

NS_ASSUME_NONNULL_END
