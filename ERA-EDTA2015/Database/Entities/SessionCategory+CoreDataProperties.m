//
//  SessionCategory+CoreDataProperties.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 5/18/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "SessionCategory+CoreDataProperties.h"

@implementation SessionCategory (CoreDataProperties)

+ (NSFetchRequest<SessionCategory *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"SessionCategory"];
}

@dynamic secColor;
@dynamic secName;
@dynamic secSessionCategoryId;
@dynamic sessions;

@end
