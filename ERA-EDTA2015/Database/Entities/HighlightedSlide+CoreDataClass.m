//
//  HighlightedSlide+CoreDataClass.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 4/24/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "HighlightedSlide+CoreDataClass.h"

@implementation HighlightedSlide

+(HighlightedSlide *)getHighlitedSlide:(NSNumber *)page forMaterial:(NSNumber *)materialId
{
    NSArray *reslutArray = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"HighlightedSlide"
                                                                                   predicate:[NSPredicate predicateWithFormat:@"hsPage == %@ AND hsMaterialId == %@", page, materialId]
                                                                             sortDescriptors:nil
                                                                                       inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    if(reslutArray.count > 0)
    {
        return [reslutArray firstObject];
    }
    return nil;
}

+(HighlightedSlide *)getHighlitedSlide:(NSNumber *)slideId
{
    NSArray *reslutArray = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"HighlightedSlide"
                                                                                 predicate:[NSPredicate predicateWithFormat:@"hsSlideId == %@", slideId]
                                                                           sortDescriptors:nil
                                                                                     inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    if(reslutArray.count > 0)
    {
        return [reslutArray firstObject];
    }
    return nil;
}

@end
