//
//  Session+CoreDataProperties.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 10/19/17.
//  Copyright © 2017 Navus. All rights reserved.
//
//

#import "Session+CoreDataClass.h"
#import "ExhibitorRole+CoreDataClass.h"

NS_ASSUME_NONNULL_BEGIN

@interface Session (CoreDataProperties)

+ (NSFetchRequest<Session *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *sesAttended;
@property (nullable, nonatomic, copy) NSNumber *sesCategoryId;
@property (nullable, nonatomic, copy) NSString *sesChairPerson;
@property (nullable, nonatomic, copy) NSString *sesCode;
@property (nullable, nonatomic, copy) NSString *sesDay;
@property (nullable, nonatomic, copy) NSString *sesEndTime;
@property (nullable, nonatomic, copy) NSNumber *sesFeedbacked;
@property (nullable, nonatomic, copy) NSString *sesKeywords;
@property (nullable, nonatomic, copy) NSString *sesLink;
@property (nullable, nonatomic, copy) NSString *sesName;
@property (nullable, nonatomic, copy) NSNumber *sesSessionId;
@property (nullable, nonatomic, copy) NSString *sesStartTime;
@property (nullable, nonatomic, copy) NSString *sesStatus;
@property (nullable, nonatomic, copy) NSNumber *sesSurvey;
@property (nullable, nonatomic, copy) NSString *sesTopicId;
@property (nullable, nonatomic, copy) NSString *sesType;
@property (nullable, nonatomic, copy) NSNumber *sesVenueId;
@property (nullable, nonatomic, copy) NSString *zetDay;
@property (nullable, nonatomic, copy) NSString *sesSubtitle;
@property (nullable, nonatomic, retain) SessionCategory *sessionCategory;
@property (nullable, nonatomic, retain) NSSet<ExhibitorRole *> *sponsors;
@property (nullable, nonatomic, retain) NSSet<Talk *> *talks;
@property (nullable, nonatomic, retain) Venue *venue;

@end

@interface Session (CoreDataGeneratedAccessors)

- (void)addSponsorsObject:(ExhibitorRole *)value;
- (void)removeSponsorsObject:(ExhibitorRole *)value;
- (void)addSponsors:(NSSet<ExhibitorRole *> *)values;
- (void)removeSponsors:(NSSet<ExhibitorRole *> *)values;

- (void)addTalksObject:(Talk *)value;
- (void)removeTalksObject:(Talk *)value;
- (void)addTalks:(NSSet<Talk *> *)values;
- (void)removeTalks:(NSSet<Talk *> *)values;

@end

NS_ASSUME_NONNULL_END
