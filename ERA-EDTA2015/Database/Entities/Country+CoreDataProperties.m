//
//  Country+CoreDataProperties.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 5/18/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "Country+CoreDataProperties.h"

@implementation Country (CoreDataProperties)

+ (NSFetchRequest<Country *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Country"];
}

@dynamic ctrCode;
@dynamic ctrCode3;
@dynamic ctrCountryId;
@dynamic ctrName;

@end
