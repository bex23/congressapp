//
//  Survey+CoreDataProperties.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 5/18/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "Survey+CoreDataProperties.h"

@implementation Survey (CoreDataProperties)

+ (NSFetchRequest<Survey *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Survey"];
}

@dynamic survCreatedAt;
@dynamic survDescription;
@dynamic survName;
@dynamic survSurveyId;
@dynamic survType;
@dynamic survUserId;
@dynamic questions;

@end
