//
//  MaterialBrowsing+CoreDataClass.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 9/26/17.
//  Copyright © 2017 Navus. All rights reserved.
//
//

#import "MaterialBrowsing+CoreDataClass.h"

@implementation MaterialBrowsing

+(void)insertMaterialBrowsingSession:(NSString *)sessionId forMaterial:(Material *)material
{
    MaterialBrowsing *matBrow = [self getMaterialBrowsingForSession:sessionId];

    if (!matBrow)
    {
        matBrow = [NSEntityDescription insertNewObjectForEntityForName:@"MaterialBrowsing" inManagedObjectContext:[LLDataAccessLayer sharedInstance].managedObjectContext];
        matBrow.sessionId = sessionId;
        matBrow.duration = @0;
        matBrow.objectId = material.matMaterialId;
        matBrow.relation = @"BROWSING";
        matBrow.objectType = @"material";
        matBrow.objectInfo = @"ios";
        matBrow.lastRecordTime = [NSNumber numberWithDouble:[[NSDate date] timeIntervalSince1970]];
        matBrow.synced = @NO;
    }

    NSError *error;
    if (![[LLDataAccessLayer sharedInstance].managedObjectContext save:&error])
    {
        NSLog(@"Error saving main context. Linking. %@", error.userInfo);
    }
    
    NSLog(@"insert browsing material %@ for session %@", matBrow, sessionId);
}

+(void)recordMaterialBrowsingForSession:(NSString *)sessionId
{
    MaterialBrowsing *matBrow = [self getMaterialBrowsingForSession:sessionId];
    
    double currentSeconds = [[NSDate date] timeIntervalSince1970];
    if (matBrow)
    {
        double additionalDuration = currentSeconds - [matBrow.lastRecordTime doubleValue];
        int newDuration = [matBrow.duration intValue] + additionalDuration;
        matBrow.duration = [NSNumber numberWithInt:newDuration];
        
        matBrow.lastRecordTime = [NSNumber numberWithDouble:currentSeconds];
        
        matBrow.synced = @NO;
    }

    NSError *error;
    if (![[LLDataAccessLayer sharedInstance].managedObjectContext save:&error])
    {
        NSLog(@"Error saving main context. Linking. %@", error.userInfo);
    }
    
    NSLog(@"record browsing material %@ for session %@", matBrow, sessionId);
}

+(void)updateLastRecordTimeForMaterialBrowsingForSession:(NSString *)sessionId
{
    MaterialBrowsing *matBrow = [self getMaterialBrowsingForSession:sessionId];
    
    if (matBrow)
    {
        matBrow.lastRecordTime = [NSNumber numberWithDouble:[[NSDate date] timeIntervalSince1970]];
    }
    
    NSError *error;
    if (![[LLDataAccessLayer sharedInstance].managedObjectContext save:&error])
    {
        NSLog(@"Error saving main context. Linking. %@", error.userInfo);
    }
}

+(MaterialBrowsing *)getMaterialBrowsingForSession:(NSString *)sessionId
{
    NSArray* arrayResults = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"MaterialBrowsing"
                                                       predicate:[NSPredicate predicateWithFormat:@"sessionId == %@", sessionId]
                                                 sortDescriptors:nil
                                                           inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    if (arrayResults.count)
    {
        return [arrayResults firstObject];
    }
    
    return nil;
}

+(NSArray *)getUnsyncedMaterialBrowsings
{
    NSArray *arrayResults = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"MaterialBrowsing"
                                                                                  predicate:[NSPredicate predicateWithFormat:@"synced == 0"]
                                                                            sortDescriptors:nil
                                                                                      inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    if (arrayResults.count)
    {
        return arrayResults;
    }
    
    return nil;
}

@end
