//
//  Country+CoreDataProperties.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 5/18/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "Country+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Country (CoreDataProperties)

+ (NSFetchRequest<Country *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *ctrCode;
@property (nullable, nonatomic, copy) NSString *ctrCode3;
@property (nullable, nonatomic, copy) NSNumber *ctrCountryId;
@property (nullable, nonatomic, copy) NSString *ctrName;

@end

NS_ASSUME_NONNULL_END
