//
//  Venue+CoreDataProperties.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 5/19/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "Venue+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Venue (CoreDataProperties)

+ (NSFetchRequest<Venue *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *venCapacity;
@property (nullable, nonatomic, copy) NSString *venColor;
@property (nullable, nonatomic, copy) NSString *venCoordX;
@property (nullable, nonatomic, copy) NSString *venCoordY;
@property (nullable, nonatomic, copy) NSNumber *venFloor;
@property (nullable, nonatomic, copy) NSString *venIsRoom;
@property (nullable, nonatomic, copy) NSString *venName;
@property (nullable, nonatomic, copy) NSString *venStatus;
@property (nullable, nonatomic, copy) NSString *venType;
@property (nullable, nonatomic, copy) NSNumber *venVenueId;
@property (nullable, nonatomic, copy) NSNumber *venImportedId;
@property (nullable, nonatomic, retain) NSSet<Session *> *sessions;

@end

@interface Venue (CoreDataGeneratedAccessors)

- (void)addSessionsObject:(Session *)value;
- (void)removeSessionsObject:(Session *)value;
- (void)addSessions:(NSSet<Session *> *)values;
- (void)removeSessions:(NSSet<Session *> *)values;

@end

NS_ASSUME_NONNULL_END
