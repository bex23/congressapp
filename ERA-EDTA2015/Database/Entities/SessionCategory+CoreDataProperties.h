//
//  SessionCategory+CoreDataProperties.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 5/18/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "SessionCategory+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface SessionCategory (CoreDataProperties)

+ (NSFetchRequest<SessionCategory *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *secColor;
@property (nullable, nonatomic, copy) NSString *secName;
@property (nullable, nonatomic, copy) NSNumber *secSessionCategoryId;
@property (nullable, nonatomic, retain) NSSet<Session *> *sessions;

@end

@interface SessionCategory (CoreDataGeneratedAccessors)

- (void)addSessionsObject:(Session *)value;
- (void)removeSessionsObject:(Session *)value;
- (void)addSessions:(NSSet<Session *> *)values;
- (void)removeSessions:(NSSet<Session *> *)values;

@end

NS_ASSUME_NONNULL_END
