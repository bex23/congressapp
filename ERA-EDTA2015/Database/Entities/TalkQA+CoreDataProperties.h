//
//  TalkQA+CoreDataProperties.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 5/18/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "TalkQA+CoreDataClass.h"

NS_ASSUME_NONNULL_BEGIN

@interface TalkQA (CoreDataProperties)

+ (NSFetchRequest<TalkQA *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *synced;
@property (nullable, nonatomic, copy) NSString *tqaContent;
@property (nullable, nonatomic, copy) NSString *tqaCreatedAt;
@property (nullable, nonatomic, copy) NSNumber *tqaId;
@property (nullable, nonatomic, copy) NSNumber *tqaParentId;
@property (nullable, nonatomic, copy) NSNumber *tqaSpeakerId;
@property (nullable, nonatomic, copy) NSNumber *tqaTalkId;
@property (nullable, nonatomic, copy) NSNumber *tqaUserId;
@property (nullable, nonatomic, retain) Talk *talk;

@end

NS_ASSUME_NONNULL_END
