//
//  SessionCategory+CoreDataClass.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 5/18/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "SessionCategory+CoreDataClass.h"
#import "Session+CoreDataClass.h"

@implementation SessionCategory


+(UIColor*)getColorForSessionCategoryId:(NSNumber*) secCategoryId
{
    NSArray *arrayResults = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"SessionCategory"
                                                                                  predicate:[NSPredicate predicateWithFormat:@"secSessionCategoryId == %@",secCategoryId]
                                                                            sortDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"secSessionCategoryId" ascending:YES]]
                                                                                      inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    SessionCategory* sec= arrayResults.lastObject;
    
    if(sec)
    {
        if (sec.secColor == nil)
        {
            return [UIColor clearColor];
        }
        
        NSMutableString* st = [NSMutableString new];
        
        NSRange range = [sec.secColor rangeOfString:@"#"];
        
        if(range.length == 0)
        {
            [st appendString:@"#"];
            [st appendString:sec.secColor];
        }
        else
        {
            [st appendString:sec.secColor];
        }
        unsigned int red, green, blue;
        
        sscanf([st UTF8String], "#%02X%02X%02X", &red, &green, &blue);
        
        UIColor *color = [UIColor colorWithRed:red/255.0 green:green/255.0 blue:blue/255.0 alpha:1];
        
        return color;
    }
    
    return nil;
}

+(SessionCategory*)getSessionCategoryWithId:(NSNumber*) secCategoryId
{
    if (secCategoryId)
    {
        NSArray *arrayResults = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"SessionCategory"
                                                                                      predicate:[NSPredicate predicateWithFormat:@"secSessionCategoryId == %@",secCategoryId]
                                                                                sortDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"secSessionCategoryId" ascending:YES]]
                                                                                          inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
        SessionCategory* sec= arrayResults.lastObject;
        
        if(sec)
        {
            return sec;
        }
    }
    return nil;
}

@end
