//
//  SurveyQuestion+CoreDataProperties.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 5/18/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "SurveyQuestion+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface SurveyQuestion (CoreDataProperties)

+ (NSFetchRequest<SurveyQuestion *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *suqCampaignId;
@property (nullable, nonatomic, copy) NSString *suqDescription;
@property (nullable, nonatomic, copy) NSString *suqGroup;
@property (nullable, nonatomic, copy) NSNumber *suqMax;
@property (nullable, nonatomic, copy) NSNumber *suqMin;
@property (nullable, nonatomic, copy) NSNumber *suqRequired;
@property (nullable, nonatomic, copy) NSNumber *suqSurveyId;
@property (nullable, nonatomic, copy) NSString *suqSurveyQuestionId;
@property (nullable, nonatomic, copy) NSString *suqTitle;
@property (nullable, nonatomic, copy) NSString *suqType;
@property (nullable, nonatomic, retain) Survey *survey;

@end

NS_ASSUME_NONNULL_END
