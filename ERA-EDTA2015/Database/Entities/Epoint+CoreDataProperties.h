//
//  Epoint+CoreDataProperties.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 5/18/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "Epoint+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Epoint (CoreDataProperties)

+ (NSFetchRequest<Epoint *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *eptDone;
@property (nullable, nonatomic, copy) NSString *eptPoints;
@property (nullable, nonatomic, copy) NSString *eptSlug;
@property (nullable, nonatomic, copy) NSString *eptTitle;

@end

NS_ASSUME_NONNULL_END
