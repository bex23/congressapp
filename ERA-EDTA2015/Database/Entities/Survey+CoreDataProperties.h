//
//  Survey+CoreDataProperties.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 5/18/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "Survey+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Survey (CoreDataProperties)

+ (NSFetchRequest<Survey *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *survCreatedAt;
@property (nullable, nonatomic, copy) NSString *survDescription;
@property (nullable, nonatomic, copy) NSString *survName;
@property (nullable, nonatomic, copy) NSString *survSurveyId;
@property (nullable, nonatomic, copy) NSString *survType;
@property (nullable, nonatomic, copy) NSNumber *survUserId;
@property (nullable, nonatomic, retain) NSSet<SurveyQuestion *> *questions;

@end

@interface Survey (CoreDataGeneratedAccessors)

- (void)addQuestionsObject:(SurveyQuestion *)value;
- (void)removeQuestionsObject:(SurveyQuestion *)value;
- (void)addQuestions:(NSSet<SurveyQuestion *> *)values;
- (void)removeQuestions:(NSSet<SurveyQuestion *> *)values;

@end

NS_ASSUME_NONNULL_END
