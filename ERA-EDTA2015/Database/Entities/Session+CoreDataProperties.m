//
//  Session+CoreDataProperties.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 10/19/17.
//  Copyright © 2017 Navus. All rights reserved.
//
//

#import "Session+CoreDataProperties.h"

@implementation Session (CoreDataProperties)

+ (NSFetchRequest<Session *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Session"];
}

@dynamic sesAttended;
@dynamic sesCategoryId;
@dynamic sesChairPerson;
@dynamic sesCode;
@dynamic sesDay;
@dynamic sesEndTime;
@dynamic sesFeedbacked;
@dynamic sesKeywords;
@dynamic sesLink;
@dynamic sesName;
@dynamic sesSessionId;
@dynamic sesStartTime;
@dynamic sesStatus;
@dynamic sesSurvey;
@dynamic sesTopicId;
@dynamic sesType;
@dynamic sesVenueId;
@dynamic zetDay;
@dynamic sesSubtitle;
@dynamic sessionCategory;
@dynamic sponsors;
@dynamic talks;
@dynamic venue;

@end
