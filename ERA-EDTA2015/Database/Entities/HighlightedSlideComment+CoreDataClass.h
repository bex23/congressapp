//
//  HighlightedSlideComment+CoreDataClass.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 4/25/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class HighlightedSlide;

NS_ASSUME_NONNULL_BEGIN

@interface HighlightedSlideComment : NSManagedObject

+(NSArray *)getHighlightedSlideCommentsForSlide:(NSNumber *)slideId;

@end

NS_ASSUME_NONNULL_END

#import "HighlightedSlideComment+CoreDataProperties.h"
