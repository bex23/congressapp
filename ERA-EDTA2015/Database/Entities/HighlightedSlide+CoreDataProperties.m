//
//  HighlightedSlide+CoreDataProperties.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 5/18/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "HighlightedSlide+CoreDataProperties.h"

@implementation HighlightedSlide (CoreDataProperties)

+ (NSFetchRequest<HighlightedSlide *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"HighlightedSlide"];
}

@dynamic hsHighlighted;
@dynamic hsMaterialId;
@dynamic hsPage;
@dynamic hsSlideId;
@dynamic synced;
@dynamic comments;

@end
