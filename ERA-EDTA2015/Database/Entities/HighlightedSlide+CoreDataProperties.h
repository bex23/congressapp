//
//  HighlightedSlide+CoreDataProperties.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 5/18/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "HighlightedSlide+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface HighlightedSlide (CoreDataProperties)

+ (NSFetchRequest<HighlightedSlide *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *hsHighlighted;
@property (nullable, nonatomic, copy) NSNumber *hsMaterialId;
@property (nullable, nonatomic, copy) NSNumber *hsPage;
@property (nullable, nonatomic, copy) NSNumber *hsSlideId;
@property (nullable, nonatomic, copy) NSNumber *synced;
@property (nullable, nonatomic, retain) NSSet<HighlightedSlideComment *> *comments;

@end

@interface HighlightedSlide (CoreDataGeneratedAccessors)

- (void)addCommentsObject:(HighlightedSlideComment *)value;
- (void)removeCommentsObject:(HighlightedSlideComment *)value;
- (void)addComments:(NSSet<HighlightedSlideComment *> *)values;
- (void)removeComments:(NSSet<HighlightedSlideComment *> *)values;

@end

NS_ASSUME_NONNULL_END
