//
//  Exhibitor+CoreDataProperties.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 8/29/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "Exhibitor+CoreDataProperties.h"

@implementation Exhibitor (CoreDataProperties)

+ (NSFetchRequest<Exhibitor *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Exhibitor"];
}

@dynamic exhAddress1;
@dynamic exhAddress2;
@dynamic exhBoothNo;
@dynamic exhBoothNumber;
@dynamic exhDescription;
@dynamic exhEmail;
@dynamic exhExhibitorId;
@dynamic exhImage;
@dynamic exhImageThumb;
@dynamic exhName;
@dynamic exhPhone;
@dynamic exhWebsite;
@dynamic exhImportedId;
@dynamic booths;
@dynamic roles;

@end
