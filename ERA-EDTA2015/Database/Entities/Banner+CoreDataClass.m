//
//  Banner+CoreDataClass.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 6/23/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "Banner+CoreDataClass.h"
#import <stdlib.h>

@implementation Banner

+(Banner *)getBannerRandomByType:(NSString *)bannerType
{
    NSArray *arrayResults = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Banner"
                                                                                  predicate:[NSPredicate predicateWithFormat:@"banTags CONTAINS[cd] %@", bannerType]
                                                                            sortDescriptors:nil
                                                                                      inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    if(arrayResults.count > 0)
    {
        return [arrayResults objectAtIndex:arc4random() % arrayResults.count];
    }
    
    return nil;
}

+(Banner *)getBannerById:(NSNumber *)bannerId
{
    NSArray *arrayResults = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Banner"
                                                                                  predicate:[NSPredicate predicateWithFormat:@"banBannerId == %@", bannerId]
                                                                            sortDescriptors:nil
                                                                                      inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    if(arrayResults.count > 0)
    {
        return [arrayResults firstObject];
    }
    
    return nil;
}

@end
