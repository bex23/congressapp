//
//  Survey+CoreDataClass.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 3/18/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "Survey+CoreDataClass.h"
#import "SurveyQuestion+CoreDataClass.h"
@implementation Survey

+(Survey *)getSurveyWithId:(NSNumber *)surveyId
{
    NSArray *arrayResult = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Survey"
                                                                              predicate:[NSPredicate predicateWithFormat:@"survSurveyId == %@", surveyId]
                                                                        sortDescriptors:nil
                                                                                  inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    if(arrayResult.count > 0)
    {
        return [arrayResult firstObject];
    }
    return nil;
}

@end
