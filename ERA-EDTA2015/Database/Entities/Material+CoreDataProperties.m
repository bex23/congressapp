//
//  Material+CoreDataProperties.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 5/18/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "Material+CoreDataProperties.h"

@implementation Material (CoreDataProperties)

+ (NSFetchRequest<Material *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Material"];
}

@dynamic matAvailableDownload;
@dynamic matFileSize;
@dynamic matFileType;
@dynamic matFileUrl;
@dynamic matMaterialId;
@dynamic matName;
@dynamic matStatus;
@dynamic matTalkId;
@dynamic matType;
@dynamic zetDownloaded;
@dynamic talk;

@end
