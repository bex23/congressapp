//
//  Speaker+CoreDataClass.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 3/15/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "Speaker+CoreDataClass.h"
#import "Talk+CoreDataClass.h"
@implementation Speaker

// Insert code here to add functionality to your managed object subclass
+(Speaker *)getSpeakerWithId:(NSString*)speakerId
{
    NSArray* arraySpeakrs = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Speaker"
                                                                                  predicate:[NSPredicate predicateWithFormat:@"spkSpeakerId == %@ ", speakerId]
                                                                            sortDescriptors:nil
                                                                                      inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    if(arraySpeakrs.count > 0)
    {
        return [arraySpeakrs firstObject];
    }
    
    return nil;
}

+(Speaker *)getSpeakerWithId:(NSString*)speakerId usingMOC:(NSManagedObjectContext*)moc
{
    NSArray* arraySpeakrs = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Speaker"
                                                                                  predicate:[NSPredicate predicateWithFormat:@"spkSpeakerId == %@ ", speakerId]
                                                                            sortDescriptors:nil
                                                                                      inMOC:moc];
    if(arraySpeakrs.count > 0)
    {
        return [arraySpeakrs firstObject];
    }
    return nil;
}

@end
