//
//  Booth+CoreDataProperties.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 5/18/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "Booth+CoreDataProperties.h"

@implementation Booth (CoreDataProperties)

+ (NSFetchRequest<Booth *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Booth"];
}

@dynamic bthBoothId;
@dynamic bthCoordX;
@dynamic bthCoordXRetina;
@dynamic bthCoordY;
@dynamic bthCoordYRetina;
@dynamic bthExhibitorId;
@dynamic bthName;
@dynamic exhibitor;

@end
