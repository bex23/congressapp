//
//  TalkQA+CoreDataProperties.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 5/18/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "TalkQA+CoreDataProperties.h"

@implementation TalkQA (CoreDataProperties)

+ (NSFetchRequest<TalkQA *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"TalkQA"];
}

@dynamic synced;
@dynamic tqaContent;
@dynamic tqaCreatedAt;
@dynamic tqaId;
@dynamic tqaParentId;
@dynamic tqaSpeakerId;
@dynamic tqaTalkId;
@dynamic tqaUserId;
@dynamic talk;

@end
