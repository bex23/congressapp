//
//  Order+CoreDataClass.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 5/18/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Talk;

NS_ASSUME_NONNULL_BEGIN

@interface Order : NSManagedObject

+(void)insertOrders:(NSArray*)paramsArray;
+(void)insertOrders:(NSArray*)paramsArray withCompletitionBlock:(void(^)(void))finished;

//+(void)rejectOrder:(Order*)order;
//+(void)updateOrder:(Order*)ord withData:(NSDictionary*)jsonData;
+(void)saveOrders:(NSArray*)jsonData;
+(void)linkOrders;
+(Order*)doesExistOrderWithTalkId:(NSNumber*)talkId andPartyId:(NSString*)partyId;
+(Order*)doesExistOrderWithTalkId:(NSNumber*)talkId usingMOC:(NSManagedObjectContext*)moc;
+(Order*)doesExistOrderWithTalkId:(NSNumber*)talkId;
+(void)deleteAllOrders;

+(Order*)addOrder:(NSString *)orderId forPresentation:(NSNumber *)presentationId;


@end

NS_ASSUME_NONNULL_END

#import "Order+CoreDataProperties.h"
