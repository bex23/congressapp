//
//  MaterialBrowsing+CoreDataClass.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 9/26/17.
//  Copyright © 2017 Navus. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface MaterialBrowsing : NSManagedObject

+(void)insertMaterialBrowsingSession:(NSString *)sessionId forMaterial:(Material *)material;
+(void)recordMaterialBrowsingForSession:(NSString *)sessionId;
+(NSArray *)getUnsyncedMaterialBrowsings;
+(void)updateLastRecordTimeForMaterialBrowsingForSession:(NSString *)sessionId;

@end

NS_ASSUME_NONNULL_END

#import "MaterialBrowsing+CoreDataProperties.h"
