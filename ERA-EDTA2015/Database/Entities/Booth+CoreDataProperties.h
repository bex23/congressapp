//
//  Booth+CoreDataProperties.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 5/18/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "Booth+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Booth (CoreDataProperties)

+ (NSFetchRequest<Booth *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *bthBoothId;
@property (nullable, nonatomic, copy) NSString *bthCoordX;
@property (nullable, nonatomic, copy) NSString *bthCoordXRetina;
@property (nullable, nonatomic, copy) NSString *bthCoordY;
@property (nullable, nonatomic, copy) NSString *bthCoordYRetina;
@property (nullable, nonatomic, copy) NSString *bthExhibitorId;
@property (nullable, nonatomic, copy) NSString *bthName;
@property (nullable, nonatomic, retain) Exhibitor *exhibitor;

@end

NS_ASSUME_NONNULL_END
