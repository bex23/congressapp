//
//  Order+CoreDataProperties.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 5/18/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "Order+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Order (CoreDataProperties)

+ (NSFetchRequest<Order *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *ordOrderDate;
@property (nullable, nonatomic, copy) NSString *ordOrderedFrom;
@property (nullable, nonatomic, copy) NSNumber *ordOrderId;
@property (nullable, nonatomic, copy) NSNumber *ordPartyId;
@property (nullable, nonatomic, copy) NSString *ordRfid;
@property (nullable, nonatomic, copy) NSNumber *ordStatus;
@property (nullable, nonatomic, copy) NSNumber *ordTalkId;
@property (nullable, nonatomic, copy) NSNumber *synced;
@property (nullable, nonatomic, retain) Talk *talk;

@end

NS_ASSUME_NONNULL_END
