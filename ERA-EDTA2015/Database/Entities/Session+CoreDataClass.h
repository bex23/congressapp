//
//  Session+CoreDataClass.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 2/14/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class SessionCategory, Sponsors, Talk, Venue;

NS_ASSUME_NONNULL_BEGIN

@interface Session : NSManagedObject

+(NSArray*)getProgramDays;
+(NSArray*)getSessions;
+(NSArray*)getSessionsByType:(NSString *)sessionType;
+(NSArray*)getSessionsByType:(NSString *)sessionType AndDate:(NSString *)date;

+(Session*)getSessionWithId:(NSString*)sessionId;

+(NSDictionary*)getActiveSessionForVenueName:(NSString*)venueName;
+(NSDictionary*)getActiveSessionForVenueId:(NSString*)venueId;

+(void)updateAttendedSessions:(NSArray*)sessionsAttendedArray;
+(void)resetAttendedSessions;
+(Session*)getCurrentlyAttendingSession;
+(Session *)getSessionByName:(NSString *)sessionName;

+(NSArray *)getAttendedSessions;

@end

NS_ASSUME_NONNULL_END

#import "Session+CoreDataProperties.h"
