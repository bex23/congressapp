//
//  SurveyQuestion+CoreDataClass.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 3/18/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Survey;

NS_ASSUME_NONNULL_BEGIN

@interface SurveyQuestion : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "SurveyQuestion+CoreDataProperties.h"
