//
//  Material+CoreDataClass.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 2/7/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "Material+CoreDataClass.h"
#import "Talk+CoreDataProperties.h"
@implementation Material

+(void)setAsDownloading:(Material*)mat
{
    mat.zetDownloaded = [NSNumber numberWithInt:2];
    [[Material getManagedObjectContext] save:nil];
}

+(void)materialDownloaded:(Material*)mat
{
    mat.zetDownloaded = [NSNumber numberWithBool:YES];
    [[Material getManagedObjectContext] save:nil];
}

+(void)resetDownloadingFlag:(Material*)mat
{
    mat.zetDownloaded = [NSNumber numberWithBool:NO];
    [[Material getManagedObjectContext] save:nil];
}

+ (NSArray *) fetchManagedObjectsWithName:(NSString *)entityName predicate: (NSPredicate *)predicate sortDescriptors:(NSArray *)sortDescriptors inMOC:(NSManagedObjectContext *)moc
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:entityName inManagedObjectContext:moc]];
    
    // Add a sort descriptor. Mandatory.
    [fetchRequest setSortDescriptors:sortDescriptors];
    fetchRequest.predicate = predicate;
    
    NSError *error;
    NSArray *fetchResults = [moc executeFetchRequest:fetchRequest error:&error];
    
    if (fetchResults == nil) {
        // Handle the error.
        NSLog(@"executeFetchRequest failed with error: %@", [error localizedDescription]);
    }
    
    return fetchResults;
}

+(NSManagedObjectContext*) getManagedObjectContext
{
    return [LLDataAccessLayer sharedInstance].managedObjectContext;
}

+ (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = [Material getManagedObjectContext];
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

+(Material *)getMetarialWithId:(NSString *)materialId
{
    NSArray *materialArray = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Material"
                                                                                   predicate:[NSPredicate predicateWithFormat:@"matMaterialId == %@", materialId]
                                                                             sortDescriptors:nil
                                                                                       inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    if(materialArray.count > 0)
    {
        return [materialArray firstObject];
    }
    return nil;
}

@end
