//
//  Booth+CoreDataClass.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 2/27/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Exhibitor;

NS_ASSUME_NONNULL_BEGIN

@interface Booth : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Booth+CoreDataProperties.h"
