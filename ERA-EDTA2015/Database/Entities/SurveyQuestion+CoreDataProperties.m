//
//  SurveyQuestion+CoreDataProperties.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 5/18/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "SurveyQuestion+CoreDataProperties.h"

@implementation SurveyQuestion (CoreDataProperties)

+ (NSFetchRequest<SurveyQuestion *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"SurveyQuestion"];
}

@dynamic suqCampaignId;
@dynamic suqDescription;
@dynamic suqGroup;
@dynamic suqMax;
@dynamic suqMin;
@dynamic suqRequired;
@dynamic suqSurveyId;
@dynamic suqSurveyQuestionId;
@dynamic suqTitle;
@dynamic suqType;
@dynamic survey;

@end
