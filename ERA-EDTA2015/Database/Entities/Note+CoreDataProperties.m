//
//  Note+CoreDataProperties.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 5/18/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "Note+CoreDataProperties.h"

@implementation Note (CoreDataProperties)

+ (NSFetchRequest<Note *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Note"];
}

@dynamic ntDate;
@dynamic ntDeleted;
@dynamic ntNote;
@dynamic ntNoteId;
@dynamic ntTalkId;
@dynamic synced;
@dynamic talk;

@end
