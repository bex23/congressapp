//
//  Banner+CoreDataClass.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 6/23/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Banner : NSManagedObject

+(Banner *)getBannerRandomByType:(NSString *)bannerType;
+(Banner *)getBannerById:(NSNumber *)bannerId;

@end

NS_ASSUME_NONNULL_END

#import "Banner+CoreDataProperties.h"
