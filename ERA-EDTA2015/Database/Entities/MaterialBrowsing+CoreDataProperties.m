//
//  MaterialBrowsing+CoreDataProperties.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 9/26/17.
//  Copyright © 2017 Navus. All rights reserved.
//
//

#import "MaterialBrowsing+CoreDataProperties.h"

@implementation MaterialBrowsing (CoreDataProperties)

+ (NSFetchRequest<MaterialBrowsing *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"MaterialBrowsing"];
}

@dynamic objectId;
@dynamic duration;
@dynamic relation;
@dynamic objectType;
@dynamic sessionId;
@dynamic objectInfo;
@dynamic synced;
@dynamic lastRecordTime;

@end
