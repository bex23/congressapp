//
//  TalkQA+CoreDataClass.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 3/15/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "TalkQA+CoreDataClass.h"
#import "Talk+CoreDataClass.h"
@implementation TalkQA

// Insert code here to add functionality to your managed object subclass

+(TalkQA*)addQuestion:(NSDictionary *)params
{
    //add question to core data
    TalkQA* question = [NSEntityDescription insertNewObjectForEntityForName:@"TalkQA" inManagedObjectContext:[LLDataAccessLayer sharedInstance].managedObjectContext];
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = kDateFormatInDatabase;
    
    question.tqaTalkId      = [params objectForKey:@"talkId"];
    question.tqaContent     = [params objectForKey:@"content"];
    question.tqaCreatedAt   = [dateFormatter stringFromDate:[NSDate date]];
    question.tqaUserId      = [NSNumber numberWithInt:[[params objectForKey:@"userId"] intValue]];
    question.synced         = @NO;
    
    //link order to talk
    Talk* tlk = [Talk getTalkWithId:[params objectForKey:@"talkId"]];
    
    question.talk = tlk;
    
    NSError* error;
    [[LLDataAccessLayer sharedInstance].managedObjectContext save:&error];
    
    if(!error)
    {
        return question;
    }
    
    return nil;
}

+(void)deleteQA:(TalkQA *)qa ForTalk:(Talk *)talk
{
    [talk removeQuestionsObject:qa];
}

+(NSArray*)getUnlinkedQAs
{
    NSArray *arrayResults = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"TalkQA"
                                                                                  predicate:[NSPredicate predicateWithFormat:@"talk == nil"]
                                                                            sortDescriptors:nil
                                                                                      inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    if(arrayResults.count > 0)
    {
        return arrayResults;
    }
    
    return nil;
}

+(NSArray*)getAllQAs
{
    NSArray *arrayResults = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"TalkQA"
                                                                                  predicate:nil
                                                                            sortDescriptors:nil
                                                                                      inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    if(arrayResults.count > 0)
    {
        return arrayResults;
    }
    
    return nil;
}

+(void)linkQAs
{
    NSArray* QAs = [TalkQA getUnlinkedQAs];
    
    NSMutableArray* talkIDs = [NSMutableArray new];
    
    for(TalkQA *QA in QAs)
    {
        [talkIDs addObject:QA.tqaTalkId];
    }
    
    NSArray* tlkArray = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Talk"
                                                                              predicate:[NSPredicate predicateWithFormat:@"tlkTalkId IN %@", talkIDs]
                                                                        sortDescriptors:nil
                                                                                  inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    
    for (TalkQA *qa in QAs)
    {
        Talk *talk = (Talk *)[[tlkArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"tlkTalkId = %@", qa.tqaTalkId]] firstObject];
        
        if (talk)
        {
            qa.talk = talk;
            qa.synced = @1;
        }
    }
    
    NSError *error;
    if (![[LLDataAccessLayer sharedInstance].managedObjectContext save:&error])
    {
        NSLog(@"Error saving main context. QAs linking. %@", error.userInfo);
    }
}

+(NSArray*)getUnsyncedQAs
{
    NSArray *arrayResults = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"TalkQA"
                                                                                  predicate:[NSPredicate predicateWithFormat:@"synced == 0"]
                                                                            sortDescriptors:nil
                                                                                      inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    return arrayResults;
}


@end
