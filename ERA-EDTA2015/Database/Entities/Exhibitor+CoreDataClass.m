//
//  Exhibitor+CoreDataClass.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 2/27/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "Exhibitor+CoreDataClass.h"
#import "Booth+CoreDataClass.h"
#import "Session+CoreDataClass.h"

@implementation Exhibitor

// Insert code here to add functionality to your managed object subclass
+(NSArray *)getExhibitors
{
    NSArray *sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"exhName" ascending:YES selector:@selector(localizedStandardCompare:)]];
    
    NSArray* tlkArray = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Exhibitor"
                                                                              predicate:nil
                                                                        sortDescriptors:sortDescriptors
                                                                                  inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    return tlkArray;
}

+(Exhibitor *)getExhibitorWithId:(NSNumber *)exhibitorId
{
    NSArray* tlkArray = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Exhibitor"
                                                                              predicate:[NSPredicate predicateWithFormat:@"exhExhibitorId == %@", exhibitorId]
                                                                        sortDescriptors:nil
                                                                                  inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    if(tlkArray.count > 0)
    {
        return [tlkArray firstObject];
    }
    return nil;
}

+(Exhibitor *)getExhibitorWithName:(NSString *)exhibitorName
{
    NSArray* tlkArray = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Exhibitor"
                                                                              predicate:[NSPredicate predicateWithFormat:@"exhName CONTAINS [cd] %@", exhibitorName]
                                                                        sortDescriptors:nil
                                                                                  inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    if(tlkArray.count > 0)
    {
        return [tlkArray firstObject];
    }
    return nil;
}

@end
