//
//  Venue+CoreDataClass.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 3/15/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "Venue+CoreDataClass.h"
#import "Session+CoreDataClass.h"
@implementation Venue

+(UIColor*)getColorForVenueName:(NSString*) venueName
{
    if (venueName.length > 0)
    {
        NSArray *arrayResults = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Venue"
                                                                                      predicate:[NSPredicate predicateWithFormat:@"venName == %@",venueName]
                                                                                sortDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"venName" ascending:YES]]
                                                                                          inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
        Venue* ven= arrayResults.lastObject;
        
        int red, green, blue;
        sscanf([ven.venColor UTF8String], "#%2X%2X%2X", &red, &green, &blue);
        
        UIColor *color = [UIColor colorWithRed:red/255.0 green:green/255.0 blue:blue/255.0 alpha:1];
        
        return color;
    }
    return nil;
}

+(UIColor*)getColorForVenueId:(NSNumber*) venueId
{
    if (venueId)
    {
        NSArray *arrayResults = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Venue"
                                                                                      predicate:[NSPredicate predicateWithFormat:@"venVenueId == %@",venueId]
                                                                                sortDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"venVenueId" ascending:YES]]
                                                                                          inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
        Venue* ven= arrayResults.lastObject;
        
        if(ven)
        {
            NSMutableString* st = [NSMutableString new];
            [st appendString:@"#"];
            [st appendString:ven.venColor];
            int red, green, blue;
            
            sscanf([st UTF8String], "#%2X%2X%2X", &red, &green, &blue);
            
            UIColor *color = [UIColor colorWithRed:red/255.0 green:green/255.0 blue:blue/255.0 alpha:1];
            
            return color;
        }
    }
    return nil;
}


@end
