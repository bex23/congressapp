//
//  Country+CoreDataClass.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 5/18/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Country : NSManagedObject

+(NSArray*)getCountries;
+(Country*)getCountryWithId:(int)country_id;
+(NSString*)getNameForId:(int)country_id;
+(NSString*)getContryCodeForId:(int)country_id;
+(void)insertCountries:(NSArray*)countriesArray;
+(BOOL)hasData;

@end

NS_ASSUME_NONNULL_END

#import "Country+CoreDataProperties.h"
