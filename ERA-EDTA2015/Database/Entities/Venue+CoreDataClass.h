//
//  Venue+CoreDataClass.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 3/15/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Session;

NS_ASSUME_NONNULL_BEGIN

@interface Venue : NSManagedObject

+(UIColor*)getColorForVenueName:(NSString*) venueName;
+(UIColor*)getColorForVenueId:(NSNumber*) venueId;

@end

NS_ASSUME_NONNULL_END

#import "Venue+CoreDataProperties.h"
