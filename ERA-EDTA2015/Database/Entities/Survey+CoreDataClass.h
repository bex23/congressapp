//
//  Survey+CoreDataClass.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 3/18/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class SurveyQuestion;

NS_ASSUME_NONNULL_BEGIN

@interface Survey : NSManagedObject

+(Survey *)getSurveyWithId:(NSNumber *)surveyId;

@end

NS_ASSUME_NONNULL_END

#import "Survey+CoreDataProperties.h"
