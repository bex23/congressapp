//
//  Speaker+CoreDataClass.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 3/15/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Talk;

NS_ASSUME_NONNULL_BEGIN

@interface Speaker : NSManagedObject

// Insert code here to declare functionality of your managed object subclass
+(Speaker *)getSpeakerWithId:(NSString*)speakerId;
+(Speaker *)getSpeakerWithId:(NSString*)speakerId usingMOC:(NSManagedObjectContext*)moc;


@end

NS_ASSUME_NONNULL_END

#import "Speaker+CoreDataProperties.h"
