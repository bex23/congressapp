//
//  HighlightedSlideComment+CoreDataProperties.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 5/18/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "HighlightedSlideComment+CoreDataProperties.h"

@implementation HighlightedSlideComment (CoreDataProperties)

+ (NSFetchRequest<HighlightedSlideComment *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"HighlightedSlideComment"];
}

@dynamic hscComment;
@dynamic hscCommentId;
@dynamic hscSlideId;
@dynamic slide;

@end
