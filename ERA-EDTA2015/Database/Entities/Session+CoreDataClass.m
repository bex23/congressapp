//
//  Session+CoreDataClass.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 2/14/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "Session+CoreDataClass.h"
#import "SessionCategory+CoreDataClass.h"
#import "Talk+CoreDataClass.h"
#import "Venue+CoreDataClass.h"
@implementation Session



+(NSArray*)getProgramDays
{
    NSArray * sessions = [Session getSessions];
    
    NSMutableArray *array = [NSMutableArray new];
    
    for (Session *session in sessions)
    {
        if (array.count == 0)
        {
            [array addObject:[Utils getDate:session.sesStartTime]];
            continue;
        }
        if (![array containsObject:[Utils getDate:session.sesStartTime]])
        {
            [array addObject:[Utils getDate:session.sesStartTime]];
        }
    }
    
    if(array.count > 0)
    {
        return [NSArray arrayWithArray:array];
    }
    return nil;
}

+(NSArray*)getSessions
{
    NSArray* resultsArray = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Session"
                                                                                  predicate:nil
                                                                            sortDescriptors:nil
                                                                                      inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    
    if(resultsArray.count > 0)
    {
        return resultsArray;
    }
    return nil;
}

+(NSArray*)getSessionsByType:(NSString *)sessionType
{
    NSSortDescriptor *sortDescriptorTime = [[NSSortDescriptor alloc] initWithKey:@"sesStartTime" ascending:YES];

    NSArray* resultsArray = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Session"
                                                                                  predicate:[NSPredicate predicateWithFormat:@"sesType == %@", sessionType]
                                                                            sortDescriptors:@[sortDescriptorTime]
                                                                                      inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    
    if(resultsArray.count > 0)
    {
        return resultsArray;
    }
    return nil;
}

+(NSArray*)getSessionsByType:(NSString *)sessionType AndDate:(NSString *)date
{
    NSSortDescriptor *sortDescriptorTime = [[NSSortDescriptor alloc] initWithKey:@"sesStartTime" ascending:YES];
    
    NSArray* resultsArray = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Session"
                                                                                  predicate:[NSPredicate predicateWithFormat:@"sesType == %@ AND sesStartTime CONTAINS [cd] %@", sessionType, date]
                                                                            sortDescriptors:@[sortDescriptorTime]
                                                                                      inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    
    if(resultsArray.count > 0)
    {
        return resultsArray;
    }
    return nil;
}

+(Session *)getSessionByName:(NSString *)sessionName
{
    NSArray* resultsArray = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Session"
                                                                                  predicate:[NSPredicate predicateWithFormat:@"sesName CONTAINS [cd] %@", sessionName]
                                                                            sortDescriptors:nil
                                                                                      inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    
    if(resultsArray.count > 0)
    {
        return [resultsArray firstObject];
    }
    return nil;
}

+(NSArray *)getOrderedTalksInSession:(Session *)session
{
    NSArray *arrayResults = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Talk"
                                                                                  predicate:[NSPredicate predicateWithFormat:@"session == %@ AND orders.@count > 0", session]
                                                                            sortDescriptors:nil
                                                                                      inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    
    return arrayResults;
}

+(Session*)getSessionWithId:(NSString*)sessionId
{
    
    NSArray* resultsArray = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Session"
                                                                                  predicate:[NSPredicate predicateWithFormat:@"sesSessionId == %@", sessionId]
                                                                            sortDescriptors:nil
                                                                                      inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    if(resultsArray.count > 0)
    {
        return [resultsArray firstObject];
    }
    return nil;
}

+(Session*)getCurrentlyAttendingSession
{
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    //    NSTimeZone* tzone = [NSTimeZone timeZoneWithName:@"UTC"];
    //    [dateFormatter setTimeZone:tzone];
    dateFormatter.dateFormat = kDateFormatInDatabase;
    NSDate* dayToday = [NSDate date];
    NSString* stringDay = [dateFormatter stringFromDate:dayToday];
    NSString* stringDate = [Utils getDate:stringDay];
    
    NSFetchRequest *otherFetch = [NSFetchRequest fetchRequestWithEntityName:@"Session"];
    otherFetch.sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"sesStartTime" ascending:YES]];
    
    otherFetch.predicate = [NSPredicate predicateWithFormat:@"sesAttended = '1' && sesType == 'Oral' && (sesStartTime CONTAINS %@ && (sesStartTime < %@ && sesEndTime > %@))", stringDate, stringDay, stringDay];
    
    NSArray *array = [[LLDataAccessLayer sharedInstance].managedObjectContext executeFetchRequest:otherFetch error:NULL];
    
    if(array.count)
    {
        NSLog(@"Count of currently attending sessions: %lu", (unsigned long)array.count);
        return [array firstObject];
    }
    
    return nil;
}

+(NSDictionary*)getActiveSessionForVenueName:(NSString*)venueName
{
    NSArray* resultsArray = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Venue"
                                                                                  predicate:[NSPredicate predicateWithFormat:@"venName == %@ ", venueName]
                                                                            sortDescriptors:nil
                                                                                      inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    if(resultsArray.count > 0)
    {
        return  [Session findActiveSessionForVenue:[resultsArray firstObject]];
    }
    return nil;
}

+(NSDictionary*)getActiveSessionForVenueId:(NSString*)venueId
{
    NSArray* resultsArray = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Venue"
                                                                                  predicate:[NSPredicate predicateWithFormat:@"venVenueId == %@ ", venueId]
                                                                            sortDescriptors:nil
                                                                                      inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    if(resultsArray.count > 0)
    {
        return  [Session findActiveSessionForVenue:[resultsArray firstObject]];
    }
    return nil;
}

+(NSDictionary*)findActiveSessionForVenue:(Venue*)venue
{
    //    NSLog(@"Find active session");
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    //    NSTimeZone* tzone = [NSTimeZone timeZoneWithName:@"UTC"];
    //    [dateFormatter setTimeZone:tzone];
    dateFormatter.dateFormat = kDateFormatInDatabase;
    NSDate* dayToday = [NSDate date];
    
    NSFetchRequest *otherFetch = [NSFetchRequest fetchRequestWithEntityName:@"Session"];
    otherFetch.sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"sesStartTime" ascending:YES]];
    
    otherFetch.predicate = [NSPredicate predicateWithFormat:@"venue = %@ && sesStartTime CONTAINS [c] %@", venue, [Utils getDate: [dateFormatter stringFromDate:dayToday]]];
    
    NSArray *array = [[LLDataAccessLayer sharedInstance].managedObjectContext executeFetchRequest:otherFetch error:NULL];
    
    NSNumber *hasEmats;
    
    for(Session* ses in array)
    {
        if([Session isActiveSession: ses])
        {
            NSLog(@"Find active session: FOUND SESSION");
            
            if ([Session checkIfHasEmatTalks:ses])
            {
                hasEmats = @1;// session found
            }
            else
            {
                hasEmats = @0;// session found but no talks available as emats
            }
            
            NSDictionary* foundSessionDict = [NSDictionary dictionaryWithObjectsAndKeys:ses, @"session", hasEmats, @"hasEmats", nil];
            
            return foundSessionDict;
        }
    }
    
    NSLog(@"Find active session: NOT FOUND");
    
    return nil;
}

+(BOOL)isActiveSession:(Session*)ses
{
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    //    NSTimeZone* tzone = [NSTimeZone timeZoneWithName:@"UTC"];
    //    [dateFormatter setTimeZone:tzone];
    dateFormatter.dateFormat = kDateFormatInDatabase;
    
    NSDate* currentSesEndDateTime = [dateFormatter dateFromString:ses.sesEndTime];
    NSDate* currentSesStartDateTime = [dateFormatter dateFromString:ses.sesStartTime];
    
    NSLog(@"currentsesEndDate %@", currentSesEndDateTime);
    
    NSDate* dayToday = [NSDate date];
    
    if ( [currentSesStartDateTime compare:dayToday]== NSOrderedAscending && [currentSesEndDateTime compare:dayToday]==NSOrderedDescending)
    {
        return YES;
    }
    
    return NO;
}

+ (BOOL)checkIfHasEmatTalks:(Session*)ses
{
    NSFetchRequest * allDataRequest = [[NSFetchRequest alloc] init];
    [allDataRequest setEntity:[NSEntityDescription entityForName:@"Talk" inManagedObjectContext:[LLDataAccessLayer sharedInstance].managedObjectContext]];
    allDataRequest.predicate = [NSPredicate predicateWithFormat:@"tlkAvailable = 1 && session = %@", ses];
    
    [allDataRequest setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSUInteger count = [[LLDataAccessLayer sharedInstance].managedObjectContext countForFetchRequest:allDataRequest error:&error];
    
    if(!error && count != 0)
    {
        return YES;
    }
    return NO;
}

+(BOOL)checkIfHasStartedTalks:(Session*)ses
{
    for(Talk* tlk in ses.talks)
    {
        if([Session checkTalkStarted:tlk])
            return YES;
    }
    
    return NO;
}

+(BOOL)checkTalkStarted:(Talk *)talk
{
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone* tzone = [NSTimeZone localTimeZone]; //[NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:tzone];
    dateFormatter.dateFormat = kDateFormatInDatabase;
    NSDate* dayToday = [NSDate date];
    NSDate* fixedDate = [dateFormatter dateFromString:talk.tlkStartTime]; //2013-05-22 09:00:00
    
    if ([fixedDate compare:dayToday]==NSOrderedAscending)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

+(void)resetAttendedSessions
{
    NSArray* resultArray = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Session"
                                                                                 predicate:[NSPredicate predicateWithFormat:@"sesAttended = '1'"]
                                                                           sortDescriptors:nil
                                                                                     inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    if(resultArray.count > 0)
    {
        for(Session* ses in resultArray)
        {
            ses.sesAttended = @"0";
        }
    }
    
    [[LLDataAccessLayer sharedInstance] saveContextWithMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
}

+(NSArray *)getAttendedSessions
{
    NSArray* resultArray = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Session"
                                                                                 predicate:[NSPredicate predicateWithFormat:@"sesAttended = '1'"]
                                                                           sortDescriptors:nil
                                                                                     inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    if(resultArray.count > 0)
    {
        return resultArray;
    }
    
    return nil;
}

+(void)updateAttendedSessions:(NSArray*)sessionsAttendedArray
{
    [self resetAttendedSessions];
    
    NSMutableArray* receivedItemsIDs = [[NSMutableArray alloc] initWithCapacity:10];
    
    // first extract the IDs of the mapItems we just received and sort them
    for(id item in sessionsAttendedArray)
    {
        if(item != (id)[NSNull null])
        {
            [receivedItemsIDs addObject:(NSNumber*)[item objectForKey:@"id"]];
        }
    }
    [receivedItemsIDs sortUsingSelector:@selector(compare:)];
    // then get the IDs of all existing mapItems and sort them
    //            NSSortDescriptor* descriptor = [[NSSortDescriptor alloc] initWithKey:predicateKeyAttribute ascending:YES];
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"sesSessionId IN %@", receivedItemsIDs];//,predicateKeyAttribute, receivedItemsIDs
    
    NSArray* resultArray = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Session"
                                                                                 predicate:predicate
                                                                           sortDescriptors:nil
                                                                                     inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    if(resultArray.count > 0)
    {
        for(Session* ses in resultArray)
        {
            ses.sesAttended = @"1";
        }
    }
    
    [[LLDataAccessLayer sharedInstance] saveContextWithMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
}

+(int)countAttendedSessions
{
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"sesAttended = '1'"];//,predicateKeyAttribute, receivedItemsIDs
    
    NSArray* resultArray = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Session"
                                                                                 predicate:predicate
                                                                           sortDescriptors:nil
                                                                                     inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    return (int)resultArray.count;
}

@end
