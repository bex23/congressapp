//
//  Note+CoreDataClass.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 5/18/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "Note+CoreDataClass.h"
#import "Talk+CoreDataClass.h"

@implementation Note

+(Note *)insertNote:(NSDictionary *)dicNostes inContext:(NSManagedObjectContext *)moc
{
    Talk *talk = [dicNostes objectForKey:@"talk"];
    
    Note *newNote = [NSEntityDescription insertNewObjectForEntityForName:@"Note" inManagedObjectContext:moc];
    
    newNote.ntTalkId = talk.tlkTalkId;
    newNote.talk = talk;
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = kDateFormatInDatabase;
    
    int countOfNotesForTalk = (int)[talk.notes count];
    newNote.ntNoteId = [NSNumber numberWithInt:countOfNotesForTalk++];
    newNote.ntNote = [dicNostes objectForKey:@"note"];
    newNote.ntDate = [dateFormatter stringFromDate:[NSDate date]];
    newNote.synced = @NO;
    
    NSError *error;
    if (![moc save:&error])
    {
        NSLog(@"Error saving main context. Linking. %@", error.userInfo);
    }
    
    return newNote;
}

+(Note *)deleteNote:(Note *)note
{
    note.ntDeleted = @YES;
    note.synced = @NO;
    
    [[LLDataAccessLayer sharedInstance].managedObjectContext save:nil];
    
    return note;
}

+(NSArray*)getUnlinkedNotes
{
    NSArray *arrayResults = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Note"
                                                                                  predicate:[NSPredicate predicateWithFormat:@"talk == nil"]
                                                                            sortDescriptors:nil
                                                                                      inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    if(arrayResults.count > 0)
    {
        return arrayResults;
    }
    
    return nil;
}

+(void)linkNotes
{
    NSArray* arrayNotes = [Note getUnlinkedNotes];
    
    NSMutableArray* talkIDs = [NSMutableArray new];
    
    for(Note *note in arrayNotes)
    {
        [talkIDs addObject:note.ntTalkId];
    }
    
    NSArray* tlkArray = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Talk"
                                                                              predicate:[NSPredicate predicateWithFormat:@"tlkTalkId IN %@", talkIDs]
                                                                        sortDescriptors:nil
                                                                                  inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    
    for (Note *note in arrayNotes)
    {
        Talk *talk = (Talk *)[[tlkArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"tlkTalkId = %@", note.ntTalkId]] firstObject];
        
        if (talk)
        {
            note.talk = talk;
            note.synced = @YES;
        }
    }
    
    NSError *error;
    if (![[LLDataAccessLayer sharedInstance].managedObjectContext save:&error])
    {
        NSLog(@"Error saving main context. QAs linking. %@", error.userInfo);
    }
}

+(NSArray *)getUnsyncedNotes
{
    NSArray *arrayResults = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Note"
                                                                                  predicate:[NSPredicate predicateWithFormat:@"synced == 0 && talk != nil"]
                                                                            sortDescriptors:nil
                                                                                      inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    return arrayResults;
}

+(NSArray *)getNotesForTalk:(Talk *)talk
{
    NSArray *arrayResults = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Note"
                                                                                  predicate:[NSPredicate predicateWithFormat:@"talk == %@", talk]
                                                                            sortDescriptors:nil
                                                                                      inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    return arrayResults;
}



@end
