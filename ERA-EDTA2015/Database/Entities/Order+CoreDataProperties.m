//
//  Order+CoreDataProperties.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 5/18/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "Order+CoreDataProperties.h"

@implementation Order (CoreDataProperties)

+ (NSFetchRequest<Order *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Order"];
}

@dynamic ordOrderDate;
@dynamic ordOrderedFrom;
@dynamic ordOrderId;
@dynamic ordPartyId;
@dynamic ordRfid;
@dynamic ordStatus;
@dynamic ordTalkId;
@dynamic synced;
@dynamic talk;

@end
