//
//  Material+CoreDataClass.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 2/7/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Talk;

NS_ASSUME_NONNULL_BEGIN

@interface Material : NSManagedObject

+(void)materialDownloaded:(Material*)mat;
+(void)setAsDownloading:(Material*)mat;
+(void)resetDownloadingFlag:(Material*)mat;
+(Material *)getMetarialWithId:(NSString *)materialId;

@end

NS_ASSUME_NONNULL_END

#import "Material+CoreDataProperties.h"
