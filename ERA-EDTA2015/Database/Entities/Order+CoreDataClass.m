//
//  Order+CoreDataClass.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 5/18/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "Order+CoreDataClass.h"
#import "Talk+CoreDataClass.h"

@implementation Order

- (void) awakeFromInsert
{
    [super awakeFromInsert];
    [self setSynced:@YES];
}

+(NSArray*)getOrders
{
    return nil;
}

+(void)insertOrders:(NSArray*)paramsArray
{
    NSManagedObjectContext *temporaryContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    temporaryContext.parentContext = [LLDataAccessLayer sharedInstance].managedObjectContext;
    
    [temporaryContext performBlock:^{
        
        int savedctr = 0;
        
        for (NSDictionary* orderParams in paramsArray)
        {
            //            confirmed = 0;
            //            id = 2;
            //            presentationId = 6;
            //            timeOfOrder = "2015-04-09 21:05:18";
            
            //check if order doesn't exists
            NSNumber *talkId = [NSNumber numberWithInteger:[[orderParams objectForKey:@"id"] integerValue]];
            Order* order = [self doesExistOrderWithTalkId:talkId usingMOC:temporaryContext];
            
            if(!order)
            {
                if (![orderParams objectForKey:@"deleted_at"])
                {
                    //add order to core data
                    order = [NSEntityDescription insertNewObjectForEntityForName:@"Order" inManagedObjectContext:temporaryContext];
                    [self saveParams:orderParams toEntity:order usingMOC: temporaryContext];
                }
            }
            else
            {
                if (![orderParams objectForKey:@"deleted_at"])
                {
                    [self saveParams:orderParams toEntity:order usingMOC: temporaryContext];
                }
                else
                {
                    [temporaryContext deleteObject:order];
                }
            }
            
            savedctr++;
            if(savedctr % 200 == 0)
            {
                NSError *error;
                if (![temporaryContext save:&error])
                {
                    NSLog(@"There was an error saving the context: %@", [error localizedDescription]);
                }
            }
        }
        
        NSError *error;
        if (![temporaryContext save:&error])
        {
            NSLog(@"There was an error saving the context: %@", [error localizedDescription]);
        }
        
        NSLog(@"Finished inserting %d Order", savedctr);
        
        // save parent to disk asynchronously
        [[LLDataAccessLayer sharedInstance].managedObjectContext performBlock:^{
            NSError *error;
            if (![[LLDataAccessLayer sharedInstance].managedObjectContext save:&error])
            {
                NSLog(@"Error saving main context. Linking. %@", error.userInfo);
            }
        }];
    }];
}

+(void)insertOrders:(NSArray*)paramsArray withCompletitionBlock:(void(^)(void))finished
{
    NSManagedObjectContext *temporaryContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    temporaryContext.parentContext = [LLDataAccessLayer sharedInstance].managedObjectContext;
    
    [temporaryContext performBlock:^{
        
        int savedctr = 0;
        
        for (NSNumber *talkId in paramsArray)
        {
            NSDictionary *orderParams = @{@"id" : talkId};
            
            //check if order doesn't exists
            NSNumber *talkId = [NSNumber numberWithInteger:[[orderParams objectForKey:@"id"] integerValue]];
            Order* order = [self doesExistOrderWithTalkId:talkId usingMOC:temporaryContext];
            
            if(!order)
            {
                if (![orderParams objectForKey:@"deleted_at"])
                {
                    //add order to core data
                    order = [NSEntityDescription insertNewObjectForEntityForName:@"Order" inManagedObjectContext:temporaryContext];
                    [self saveParams:orderParams toEntity:order usingMOC: temporaryContext];
                }
            }
            else
            {
                if (![orderParams objectForKey:@"deleted_at"])
                {
                    [self saveParams:orderParams toEntity:order usingMOC: temporaryContext];
                }
                else
                {
                    [temporaryContext deleteObject:order];
                }
            }
            
            savedctr++;
            if(savedctr % 200 == 0)
            {
                NSError *error;
                if (![temporaryContext save:&error])
                {
                    NSLog(@"There was an error saving the context: %@", [error localizedDescription]);
                }
            }
        }
        
        NSError *error;
        if (![temporaryContext save:&error])
        {
            NSLog(@"There was an error saving the context: %@", [error localizedDescription]);
        }
        
        NSLog(@"Finished inserting %d Order", savedctr);
        
        // save parent to disk asynchronously
        [[LLDataAccessLayer sharedInstance].managedObjectContext performBlock:^{
            NSError *error;
            if (![[LLDataAccessLayer sharedInstance].managedObjectContext save:&error])
            {
                NSLog(@"Error saving main context. Linking. %@", error.userInfo);
            }
            
            finished();
        }];
    }];
}

// OLD

//+(void)insertOrders:(NSArray*)paramsArray withCompletitionBlock:(void(^)())finished
//{
//    NSManagedObjectContext *temporaryContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
//    temporaryContext.parentContext = [LLDataAccessLayer sharedInstance].managedObjectContext;
//
//    [temporaryContext performBlock:^{
//
//        int savedctr = 0;
//
//        for (NSDictionary* orderParams in paramsArray)
//        {
//            //            confirmed = 0;
//            //            id = 2;
//            //            presentationId = 6;
//            //            timeOfOrder = "2015-04-09 21:05:18";
//
//            //check if order doesn't exists
//            NSNumber *talkId = [NSNumber numberWithInteger:[[orderParams objectForKey:@"id"] integerValue]];
//            Order* order = [self doesExistOrderWithTalkId:talkId usingMOC:temporaryContext];
//
//            if(!order)
//            {
//                if (![orderParams objectForKey:@"deleted_at"])
//                {
//                    //add order to core data
//                    order = [NSEntityDescription insertNewObjectForEntityForName:@"Order" inManagedObjectContext:temporaryContext];
//                    [self saveParams:orderParams toEntity:order usingMOC: temporaryContext];
//                }
//            }
//            else
//            {
//                if (![orderParams objectForKey:@"deleted_at"])
//                {
//                    [self saveParams:orderParams toEntity:order usingMOC: temporaryContext];
//                }
//                else
//                {
//                    [temporaryContext deleteObject:order];
//                }
//            }
//
//            savedctr++;
//            if(savedctr % 200 == 0)
//            {
//                NSError *error;
//                if (![temporaryContext save:&error])
//                {
//                    NSLog(@"There was an error saving the context: %@", [error localizedDescription]);
//                }
//            }
//        }
//
//        NSError *error;
//        if (![temporaryContext save:&error])
//        {
//            NSLog(@"There was an error saving the context: %@", [error localizedDescription]);
//        }
//
//        NSLog(@"Finished inserting %d Order", savedctr);
//
//        // save parent to disk asynchronously
//        [[LLDataAccessLayer sharedInstance].managedObjectContext performBlock:^{
//            NSError *error;
//            if (![[LLDataAccessLayer sharedInstance].managedObjectContext save:&error])
//            {
//                NSLog(@"Error saving main context. Linking. %@", error.userInfo);
//            }
//
//            finished();
//        }];
//    }];
//}

+(void)saveParams:(NSDictionary*)orderParams toEntity:(Order*)order usingMOC: (NSManagedObjectContext*)moc
{
    //    order.ordPartyId     = [orderParams objectForKey:@"ordPartyId"] != (id)[NSNull null]?[NSNumber numberWithInt:[[orderParams objectForKey:@"ordPartyId"] intValue]]:nil;
    order.ordTalkId      = [orderParams objectForKey:@"id"] != (id)[NSNull null]?[NSNumber numberWithInt:[[orderParams objectForKey:@"id"] intValue]]:nil;
    //    order.ordOrderId     = [orderParams objectForKey:@"id"] != (id)[NSNull null]?[NSNumber numberWithInt:[[orderParams objectForKey:@"id"] intValue]]:nil;
    //    order.ordStatus      = [orderParams objectForKey:@"confirmed"] != (id)[NSNull null]?[NSNumber numberWithInt:[[orderParams objectForKey:@"confirmed"] intValue]]:nil;
    //    order.ordOrderDate   = [orderParams objectForKey:@"timeOfOrder"] != (id)[NSNull null]?[orderParams objectForKey:@"timeOfOrder"]:nil;
    //    order.ordOrderedFrom = [orderParams objectForKey:@"ordOrderedFrom"] != (id)[NSNull null]?[orderParams objectForKey:@"ordOrderedFrom"]:nil;
    order.synced         = @YES;
    
    if(!order.talk)
    {
        //link order to talk
        Talk* tlk = [Talk getTalkWithId:[orderParams objectForKey:@"id"] usingMOC:moc];
        
        order.talk = tlk;
    }
}

+(Order*)addOrder:(NSString *)orderId forPresentation:(NSNumber *)presentationId
{
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    
    dateFormatter.dateFormat = kDateFormatInDatabase;
    NSDate* dayToday = [NSDate date];
    NSString* ordOrderDate = [dateFormatter stringFromDate:dayToday];
    
    //add order to core data
    Order *order = [NSEntityDescription insertNewObjectForEntityForName:@"Order" inManagedObjectContext:[LLDataAccessLayer sharedInstance].managedObjectContext];
    
    order.ordOrderId     = [NSNumber numberWithInt:[orderId intValue]];
    order.ordTalkId      = [NSNumber numberWithInt:[presentationId intValue]];
    //    order.ordStatus      = @1; // in library
    order.ordOrderDate   = ordOrderDate;
    order.synced         = @YES;
    
    if(!order.talk)
    {
        //link order to talk
        Talk* tlk = [Talk getTalkWithId:presentationId];
        order.talk = tlk;
    }
    
    NSError* error;
    
    [[LLDataAccessLayer sharedInstance].managedObjectContext save:&error];
    
    if(!error)
    {
        return order;
    }
    
    return nil;
}

+(void)linkOrders
{
    NSArray *arrayResults = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Order"
                                                                                  predicate:nil
                                                                            sortDescriptors:nil
                                                                                      inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    if (arrayResults.count > 0)
    {
        for(Order* ord in arrayResults)
        {
            if(!ord.talk)
            {
                //link order to talk
                Talk* tlk = [Talk getTalkWithId:ord.ordTalkId];
                ord.talk = tlk;
            }
        }
        
        [[LLDataAccessLayer sharedInstance].managedObjectContext save:nil];
    }
}

+(void)saveOrders:(NSArray*)jsonData
{
    int savedctr = 0;
    
    if(jsonData.count > 0)
    {
        for (NSDictionary* item in jsonData)
        {
            Order* newEntity = (Order*)[NSEntityDescription insertNewObjectForEntityForName:@"Order" inManagedObjectContext:[LLDataAccessLayer sharedInstance].managedObjectContext];
            
            for(NSString* key in [item allKeys])
            {
                if ([newEntity respondsToSelector:NSSelectorFromString(key)])
                {
                    if([item objectForKey:key] != (id)[NSNull null])
                    {
                        NSDictionary *attributes = [[newEntity entity] attributesByName];
                        NSAttributeDescription *keyAttribute = [attributes objectForKey:key];
                        
                        if([keyAttribute.attributeValueClassName isEqualToString:@"NSNumber"])
                        {
                            [newEntity setValue:[NSNumber numberWithInt:[[item objectForKey:key] intValue]]  forKey:key];
                        }
                        else
                        {
                            [newEntity setValue:[item objectForKey:key] forKey:key];
                        }
                    }
                }
            }
            
            if(!newEntity.talk)
            {
                //link order to talk
                Talk* tlk = [Talk getTalkWithId:[item objectForKey:@"ordTalkId"]];
                
                newEntity.talk = tlk;
            }
            // save in smaller batches, which drastically speeds up insertion
            savedctr++;
            if(savedctr % 200 == 0)
            {
                [[LLDataAccessLayer sharedInstance].managedObjectContext save:nil];
            }
        }
        
        //save what is left
        [[LLDataAccessLayer sharedInstance].managedObjectContext save:nil];
    }
}

//+(void)rejectOrder:(Order*)order
//{
//    order.ordStatus      = @3;
//    order.synced         = @NO;
//
////    [[LLDataAccessLayer sharedInstance].managedObjectContext deleteObject:order];
//
//    NSError* error;
//
//    [[LLDataAccessLayer sharedInstance].managedObjectContext save:&error];
//}

+(Order*)doesExistOrderWithTalkId:(NSNumber*)talkId andPartyId:(NSString*)partyId usingMOC:(NSManagedObjectContext*)moc
{
    if(talkId)
    {
        NSArray *arrayResults = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Order"
                                                                                      predicate:[NSPredicate predicateWithFormat:@"ordTalkId == %@ && ordPartyId == %@", talkId, partyId]
                                                                                sortDescriptors:nil
                                                                                          inMOC:moc];
        if (arrayResults.count > 0)
        {
            //if order is rejected it can be added again
            return [arrayResults lastObject];
        }
    }
    return nil;
}

+(Order*)doesExistOrderWithTalkId:(NSNumber*)talkId andPartyId:(NSString*)partyId
{
    if(talkId)
    {
        NSArray *arrayResults = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Order"
                                                                                      predicate:[NSPredicate predicateWithFormat:@"ordTalkId == %@ && ordPartyId == %@", talkId, partyId]
                                                                                sortDescriptors:nil
                                                                                          inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
        if (arrayResults.count > 0)
        {
            //if order is rejected it can be added again
            return [arrayResults lastObject];
        }
    }
    return nil;
}

+(Order*)doesExistOrderWithTalkId:(NSNumber *)talkId usingMOC:(NSManagedObjectContext*)moc
{
    if(talkId)
    {
        NSArray *arrayResults = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Order"
                                                                                      predicate:[NSPredicate predicateWithFormat:@"ordTalkId == %@", talkId]
                                                                                sortDescriptors:nil
                                                                                          inMOC:moc];
        if (arrayResults.count > 0)
        {
            //if order is rejected it can be added again
            return [arrayResults lastObject];
        }
    }
    return nil;
}

+(Order*)doesExistOrderWithTalkId:(NSNumber*)talkId
{
    if(talkId)
    {
        NSArray *arrayResults = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"Order"
                                                                                      predicate:[NSPredicate predicateWithFormat:@"ordTalkId == %@", talkId]
                                                                                sortDescriptors:nil
                                                                                          inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
        if (arrayResults.count > 0)
        {
            //if order is rejected it can be added again
            return [arrayResults lastObject];
        }
    }
    return nil;
}

//+(void)updateOrder:(Order*)order withData:(NSDictionary*)jsonData
//{
//    order.ordOrderId      = [jsonData objectForKey:@"ordOrderId"] != (id)[NSNull null]?[NSNumber numberWithInt:[[jsonData objectForKey:@"ordOrderId"] intValue]]:nil;
////    order.ordOrderDate   = [jsonData objectForKey:@"ordModifiedOn"] != (id)[NSNull null]?[jsonData objectForKey:@"ordModifiedOn"]:nil;
//    order.synced         = [NSNumber numberWithBool:YES];
//
//    NSError* error;
//
//    [[LLDataAccessLayer sharedInstance].managedObjectContext save:&error];
//}

+(void)deleteAllOrders
{
    NSFetchRequest * allDataRequest = [[NSFetchRequest alloc] init];
    [allDataRequest setEntity:[NSEntityDescription entityForName:@"Order" inManagedObjectContext:[LLDataAccessLayer sharedInstance].managedObjectContext]];
    [allDataRequest setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSUInteger count = [[LLDataAccessLayer sharedInstance].managedObjectContext countForFetchRequest:allDataRequest error:&error];
    
    if(!error && count != 0)
    {
        NSArray * resultsArray = [[LLDataAccessLayer sharedInstance].managedObjectContext executeFetchRequest:allDataRequest error:&error];
        
        //error handling goes here
        for (NSManagedObject * mob in resultsArray) {
            [[LLDataAccessLayer sharedInstance].managedObjectContext deleteObject:mob];
        }
        NSError *saveError = nil;
        [[LLDataAccessLayer sharedInstance].managedObjectContext save:&saveError];
    }
}


@end
