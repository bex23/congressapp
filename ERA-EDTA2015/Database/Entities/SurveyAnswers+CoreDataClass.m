//
//  SurveyAnswers+CoreDataClass.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 3/20/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "SurveyAnswers+CoreDataClass.h"

@implementation SurveyAnswers


+(void)insertSurveyAnswers:(NSDictionary *)dictAnswers forSurveyId:(NSNumber *)surveyId withCompletitionBlock:(void(^)(void))finished
{
    NSManagedObjectContext *temporaryContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    temporaryContext.parentContext = [LLDataAccessLayer sharedInstance].managedObjectContext;
    
    [temporaryContext performBlock:^{
        
        int savedctr = 0;
        
        for (NSNumber *key in dictAnswers.allKeys)
        {
            SurveyAnswers *sa = [self doesSurveyAnswerWithQuestionId:key surveyId:surveyId usingMOC:temporaryContext];
            
            if(!sa)
            {
                //add SurveyAnswer to core data
                sa = [NSEntityDescription insertNewObjectForEntityForName:@"SurveyAnswers" inManagedObjectContext:temporaryContext];
                sa.suaSurveyId = surveyId;
                sa.suaQuestionId = key.stringValue;
                sa.suaContent = ([[dictAnswers objectForKey:key] isKindOfClass:[NSString class]]) ? [dictAnswers objectForKey:key] : [[dictAnswers objectForKey:key] stringValue];
                sa.synced = @NO;
            }
            
            savedctr++;
            if(savedctr % 10 == 0)
            {
                NSError *error;
                if (![temporaryContext save:&error])
                {
                    NSLog(@"There was an error saving the context: %@", [error localizedDescription]);
                }
            }
        }
        
        NSError *error;
        if (![temporaryContext save:&error])
        {
            NSLog(@"There was an error saving the context: %@", [error localizedDescription]);
        }
        
        // save parent to disk asynchronously
        [[LLDataAccessLayer sharedInstance].managedObjectContext performBlock:^{
            NSError *error;
            if (![[LLDataAccessLayer sharedInstance].managedObjectContext save:&error])
            {
                NSLog(@"Error saving main context. Linking. %@", error.userInfo);
            }
            
            finished();
        }];
    }];
}


+(void)setSyncedSurveyAnswers:(NSArray *)arraySA
{
    NSManagedObjectContext *temporaryContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    temporaryContext.parentContext = [LLDataAccessLayer sharedInstance].managedObjectContext;
    
    [temporaryContext performBlock:^{
        
        int savedctr = 0;
        
        for (NSDictionary *syncedAnswer in arraySA)
        {
            SurveyAnswers *sa = [self doesSurveyAnswerWithQuestionId:[syncedAnswer objectForKey:@"question_id"] usingMOC:temporaryContext];
            
            if(sa)
            {
                sa.synced = @YES;
            }
            
            savedctr++;
            if(savedctr % 10 == 0)
            {
                NSError *error;
                if (![temporaryContext save:&error])
                {
                    NSLog(@"There was an error saving the context: %@", [error localizedDescription]);
                }
            }
        }
        
        NSError *error;
        if (![temporaryContext save:&error])
        {
            NSLog(@"There was an error saving the context: %@", [error localizedDescription]);
        }
        
        // save parent to disk asynchronously
        [[LLDataAccessLayer sharedInstance].managedObjectContext performBlock:^{
            NSError *error;
            if (![[LLDataAccessLayer sharedInstance].managedObjectContext save:&error])
            {
                NSLog(@"Error saving main context. Linking. %@", error.userInfo);
            }
        }];
    }];
}

+(SurveyAnswers *)doesSurveyAnswerWithQuestionId:(NSNumber *)questionId surveyId:(NSNumber *)surveryId usingMOC:(NSManagedObjectContext*)moc
{
    NSArray *arrayResults = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"SurveyAnswers"
                                                                                  predicate:[NSPredicate predicateWithFormat:@"suaSurveyId == %@ AND suaQuestionId == %@", surveryId, questionId]
                                                                            sortDescriptors:nil
                                                                                      inMOC:moc];
    if (arrayResults.count > 0)
    {
        return [arrayResults lastObject];
    }
    
    return nil;
}

+(SurveyAnswers *)doesSurveyAnswerWithQuestionId:(NSNumber *)questionId usingMOC:(NSManagedObjectContext*)moc
{
    NSArray *arrayResults = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"SurveyAnswers"
                                                                                  predicate:[NSPredicate predicateWithFormat:@"suaQuestionId == %@", questionId]
                                                                            sortDescriptors:nil
                                                                                      inMOC:moc];
    if (arrayResults.count > 0)
    {
        return [arrayResults lastObject];
    }
    
    return nil;
}

+(NSArray *)getSurveyAnswersForSurveyId:(NSNumber *)surveryId
{
    NSArray *arrayResults = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"SurveyAnswers"
                                                                                  predicate:[NSPredicate predicateWithFormat:@"suaSurveyId == %@", surveryId]
                                                                            sortDescriptors:nil
                                                                                      inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    if (arrayResults.count > 0)
    {
        return arrayResults;
    }
    
    return nil;
}

+(NSArray *)getUnsyncedSurveysAnswers
{
    NSArray *arrayResults = [[LLDataAccessLayer sharedInstance] fetchManagedObjectsWithName:@"SurveyAnswers"
                                                                                  predicate:[NSPredicate predicateWithFormat:@"synced == 0"]
                                                                            sortDescriptors:nil
                                                                                      inMOC:[LLDataAccessLayer sharedInstance].managedObjectContext];
    if (arrayResults.count > 0)
    {
        return arrayResults;
    }
    
    return nil;
}

@end
