//
//  ExhibitorRole+CoreDataClass.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 2/28/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Exhibitor, Session;

NS_ASSUME_NONNULL_BEGIN

@interface ExhibitorRole : NSManagedObject

+(ExhibitorRole *)getExhibitorRoleForExhibitorId:(NSString *)exhibitorId inMOC:(NSManagedObjectContext *)moc;
+(NSArray *)getExhibitorsWithRole:(NSString *)role;

@end

NS_ASSUME_NONNULL_END

#import "ExhibitorRole+CoreDataProperties.h"
