//
//  Material+CoreDataProperties.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 5/18/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "Material+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Material (CoreDataProperties)

+ (NSFetchRequest<Material *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *matAvailableDownload;
@property (nullable, nonatomic, copy) NSString *matFileSize;
@property (nullable, nonatomic, copy) NSString *matFileType;
@property (nullable, nonatomic, copy) NSString *matFileUrl;
@property (nullable, nonatomic, copy) NSNumber *matMaterialId;
@property (nullable, nonatomic, copy) NSString *matName;
@property (nullable, nonatomic, copy) NSString *matStatus;
@property (nullable, nonatomic, copy) NSNumber *matTalkId;
@property (nullable, nonatomic, copy) NSString *matType;
@property (nullable, nonatomic, copy) NSNumber *zetDownloaded;
@property (nullable, nonatomic, retain) Talk *talk;

@end

NS_ASSUME_NONNULL_END
