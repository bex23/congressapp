//
//  Talk+CoreDataProperties.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 11/3/17.
//  Copyright © 2017 Navus. All rights reserved.
//
//

#import "Talk+CoreDataProperties.h"

@implementation Talk (CoreDataProperties)

+ (NSFetchRequest<Talk *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Talk"];
}

@dynamic timelineId;
@dynamic timelineSynced;
@dynamic tlkAllowQA;
@dynamic tlkAvailable;
@dynamic tlkAvailableMainMaterial;
@dynamic tlkAvailableSupportingMaterial;
@dynamic tlkAvailableWebcastMaterial;
@dynamic tlkBoardNo;
@dynamic tlkConsentCompleted;
@dynamic tlkDescription;
@dynamic tlkEndTime;
@dynamic tlkExternalUrl;
@dynamic tlkHasAudioMaterial;
@dynamic tlkHasMainMaterial;
@dynamic tlkHasSupportingMaterial;
@dynamic tlkHasWebcastMaterial;
@dynamic tlkImportedId;
@dynamic tlkSessionId;
@dynamic tlkSpeakerImageUrl;
@dynamic tlkSpeakerName;
@dynamic tlkStartTime;
@dynamic tlkStatus;
@dynamic tlkTalkId;
@dynamic tlkTitle;
@dynamic tlkType;
@dynamic tlkVoted;
@dynamic tlkVotingEnabled;
@dynamic tlkVoteSynced;
@dynamic zetBookmarked;
@dynamic zetDay;
@dynamic tlkVoteAvailable;
@dynamic materials;
@dynamic notes;
@dynamic orders;
@dynamic questions;
@dynamic session;
@dynamic speakrs;

@end
