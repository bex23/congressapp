//
//  ExhibitorRole+CoreDataProperties.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 6/16/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "ExhibitorRole+CoreDataProperties.h"

@implementation ExhibitorRole (CoreDataProperties)

+ (NSFetchRequest<ExhibitorRole *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"ExhibitorRole"];
}

@dynamic erExhibitorId;
@dynamic erExhibitorRoleId;
@dynamic erOrderPosition;
@dynamic erRoleName;
@dynamic erSessionId;
@dynamic exhibitor;
@dynamic session;

@end
