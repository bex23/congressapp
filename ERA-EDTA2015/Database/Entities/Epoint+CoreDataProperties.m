//
//  Epoint+CoreDataProperties.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 5/18/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "Epoint+CoreDataProperties.h"

@implementation Epoint (CoreDataProperties)

+ (NSFetchRequest<Epoint *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Epoint"];
}

@dynamic eptDone;
@dynamic eptPoints;
@dynamic eptSlug;
@dynamic eptTitle;

@end
