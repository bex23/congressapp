//
//  SurveyAnswers+CoreDataProperties.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 5/18/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "SurveyAnswers+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface SurveyAnswers (CoreDataProperties)

+ (NSFetchRequest<SurveyAnswers *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *suaContent;
@property (nullable, nonatomic, copy) NSString *suaQuestionId;
@property (nullable, nonatomic, copy) NSNumber *suaSurveyId;
@property (nullable, nonatomic, copy) NSNumber *synced;

@end

NS_ASSUME_NONNULL_END
