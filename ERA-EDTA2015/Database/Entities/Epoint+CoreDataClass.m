//
//  Epoint+CoreDataClass.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 5/18/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "Epoint+CoreDataClass.h"

@implementation Epoint

+(void)saveEpoints:(NSDictionary *)dicEpointsStructure
{
    //    "registered-eMaterials": {
    //        "slug": "registered-eMaterials",
    //        "title": "Registered on the delegate portal",
    //        "points": "5",
    //        "done": 0
    //    },
    //    "registered-eMaterials-early": {
    //        "slug": "registered-eMaterials-early",
    //        "title": "Registering on the delegate portal before the congress",
    //        "points": "2",
    //        "done": 0
    //    }
    
    NSMutableArray *arrayNewEpointsStructure = [[NSMutableArray alloc] init];
    
    for (NSString *strEpointSlug in dicEpointsStructure)
    {
        [arrayNewEpointsStructure addObject:[dicEpointsStructure objectForKey:strEpointSlug]];
    }
    
    NSDictionary* attributeMappings = [NSDictionary dictionaryWithObjectsAndKeys:
                                       @"slug", @"eptSlug",
                                       @"title", @"eptTitle",
                                       @"points", @"eptPoints",
                                       @"done", @"eptDone",
                                       nil];
    
    [[LLDataAccessLayer sharedInstance] insertEntitiesArray:arrayNewEpointsStructure withName:@"Epoint" receivedPredicatePrimaryKey:@"slug" predicateKeyAttribute:@"eptSlug" sortDescriptorKey:@"eptSlug"  activeAttributeName:nil attributeMappings:attributeMappings withCompletitionBlock:^(bool finished)
     {
     }];
}

+(void)updateEpoints:(NSArray *)arrayUsersEpointsStructure WithCompletitionBlock:(void (^)(void))finished
{
    //    {
    //        "created_at" = "2016-05-10 10:42:48";
    //        description = "Registered on the delegate portal";
    //        "e_points" = 5;
    //        id = 29;
    //        "party_id" = 19407;
    //        slug = "registered-eMaterials";
    //    },
    //    {
    //        "created_at" = "2016-05-10 10:42:48";
    //        description = "Registering on the delegate portal before the congress";
    //        "e_points" = 2;
    //        id = 30;
    //        "party_id" = 19407;
    //        slug = "registered-eMaterials-early";
    //    }
    NSMutableArray *arrayNewUsersEpointsStructure = [[NSMutableArray alloc] init];
    
    for (NSMutableDictionary *dicEpointStructure in arrayUsersEpointsStructure)
    {
        [dicEpointStructure setObject:@1 forKey:@"done"];
        [arrayNewUsersEpointsStructure addObject:dicEpointStructure];
    }
    
    NSDictionary* attributeMappings = [NSDictionary dictionaryWithObjectsAndKeys:
                                       @"slug", @"eptSlug",
                                       @"description", @"eptTitle",
                                       @"e_points", @"eptPoints",
                                       @"done", @"eptDone",
                                       nil];
    
    [[LLDataAccessLayer sharedInstance] insertEntitiesArray:arrayNewUsersEpointsStructure withName:@"Epoint" receivedPredicatePrimaryKey:@"slug" predicateKeyAttribute:@"eptSlug" sortDescriptorKey:@"eptSlug"  activeAttributeName:nil attributeMappings:attributeMappings withCompletitionBlock:^(bool insertFinished)
     {
         finished();
     }];
}


@end
