//
//  Epoint+CoreDataClass.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 5/18/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Epoint : NSManagedObject

+(void)saveEpoints:(NSDictionary*)epointsData;
+(void)updateEpoints:(NSArray *)epointsData WithCompletitionBlock:(void (^)(void))finished;

@end

NS_ASSUME_NONNULL_END

#import "Epoint+CoreDataProperties.h"
