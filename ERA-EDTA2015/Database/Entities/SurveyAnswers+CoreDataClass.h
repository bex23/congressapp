//
//  SurveyAnswers+CoreDataClass.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 3/20/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface SurveyAnswers : NSManagedObject

+(void)insertSurveyAnswers:(NSDictionary *)dictAnswers forSurveyId:(NSNumber *)surveyId withCompletitionBlock:(void(^)(void))finished;
+(NSArray *)getSurveyAnswersForSurveyId:(NSNumber *)surveryId;
+(NSArray *)getUnsyncedSurveysAnswers;
+(void)setSyncedSurveyAnswers:(NSArray *)arraySA;

@end

NS_ASSUME_NONNULL_END

#import "SurveyAnswers+CoreDataProperties.h"
