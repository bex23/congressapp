//
//  Speaker+CoreDataProperties.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 11/3/17.
//  Copyright © 2017 Navus. All rights reserved.
//
//

#import "Speaker+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Speaker (CoreDataProperties)

+ (NSFetchRequest<Speaker *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *spkBiography;
@property (nullable, nonatomic, copy) NSString *spkFirstName;
@property (nullable, nonatomic, copy) NSString *spkLastName;
@property (nullable, nonatomic, copy) NSString *spkPicture;
@property (nullable, nonatomic, copy) NSString *spkSpeakerId;
@property (nullable, nonatomic, copy) NSNumber *spkTalkId;
@property (nullable, nonatomic, copy) NSNumber *spkUserId;
@property (nullable, nonatomic, retain) NSSet<Talk *> *talks;

@end

@interface Speaker (CoreDataGeneratedAccessors)

- (void)addTalksObject:(Talk *)value;
- (void)removeTalksObject:(Talk *)value;
- (void)addTalks:(NSSet<Talk *> *)values;
- (void)removeTalks:(NSSet<Talk *> *)values;

@end

NS_ASSUME_NONNULL_END
