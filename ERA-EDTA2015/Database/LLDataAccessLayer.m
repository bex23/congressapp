
//  DADataAccessLayer.m
//  DentalApp
//
//  Created by admin on 11/5/12.
//  Copyright (c) 2012 OXY. All rights reserved.
//

#import "LLDataAccessLayer.h"

@interface LLDataAccessLayer ()

- (NSURL *)applicationDocumentsDirectory;
@property (nonatomic, strong) NSManagedObjectContext* bgrndManagedObjectContext;

@end

@implementation LLDataAccessLayer

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize bgrndManagedObjectContext = _bgrndManagedObjectContext;

+ (LLDataAccessLayer *)sharedInstance
{
    __strong static LLDataAccessLayer *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[LLDataAccessLayer alloc] init];
        sharedInstance.persistentStoreCoordinator = [sharedInstance persistentStoreCoordinator];
        sharedInstance.managedObjectContext = [sharedInstance managedObjectContext];
    });
    return sharedInstance;
}
// returns a separate MOC for background use
// NOTE: since this will be called for the first time in one of the get* methods, which
// are executed on a background thread via dispatch_async in PSViewController, it will
// automatically be created on the proper thread.
- (void)setBgrndManagedObjectContext
{
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _bgrndManagedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType: NSPrivateQueueConcurrencyType];
        [_bgrndManagedObjectContext setPersistentStoreCoordinator:coordinator];
    }
}

- (void)saveContextWithMOC:(NSManagedObjectContext*)moc
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = moc;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            [self saveContextWithMOC:moc];
        }
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil)
    {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"congressDatabase" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    NSString *path = [[RealmRepository shared] storePath];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:path];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:@{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES} error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

- (void)rollbackDefaultMOC
{
    [self.managedObjectContext rollback];
}

- (void)deleteManagedObjectFromDefaultMOC:(NSManagedObject *)managedObject
{
    [self.managedObjectContext deleteObject:managedObject];
}

- (NSArray *) fetchManagedObjectsWithName:(NSString *)entityName predicate: (NSPredicate *)predicate sortDescriptors:(NSArray *)sortDescriptors inMOC:(NSManagedObjectContext *)moc
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:entityName inManagedObjectContext:moc]];
    
    // Add a sort descriptor. Mandatory.
    [fetchRequest setSortDescriptors:sortDescriptors];
    fetchRequest.predicate = predicate;
    
    NSError *error;
    NSArray *fetchResults = [moc executeFetchRequest:fetchRequest error:&error];
    
    if (fetchResults == nil) {
        // Handle the error.spe
        NSLog(@"executeFetchRequest failed with error: %@", [error localizedDescription]);
    }
    
    return fetchResults;
}

- (NSManagedObject *)fetchManagedObjectWithName:(NSString *)entityName predicate:(NSPredicate *)predicate sortDescriptors:(NSArray *)sortDescriptors inMOC:(NSManagedObjectContext *)moc
{
   	NSArray *fetchResults = [self fetchManagedObjectsWithName:entityName predicate:predicate sortDescriptors:sortDescriptors inMOC:moc];
    
    NSManagedObject *managedObject = nil;
    
    if (fetchResults && [fetchResults count] > 0) {
        // Found record
        managedObject = [fetchResults objectAtIndex:0];
    }
    
    return managedObject;
}

- (void)insertEntitiesArray:(NSArray*)entetiesArray withName:(NSString*)entityName receivedPredicatePrimaryKey:(NSString*)receivedPredicatePrimaryKey predicateKeyAttribute:(NSString*)predicateKeyAttribute sortDescriptorKey:(NSString*)sortKey activeAttributeName:(NSString*)activeAttributeName attributeMappings:(NSDictionary*) attributeMappings withCompletitionBlock:(void(^)(bool))finished
{
    NSManagedObjectContext *temporaryContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    temporaryContext.parentContext = self.managedObjectContext;
    // 1
    [temporaryContext performBlock:^{
        
        if(entetiesArray.count > 0)
        {
            // add all the mapItemArray
            NSMutableArray* receivedEntetiesArray = [entetiesArray mutableCopy];
            
            NSError *error = nil;
            
            // The following code aims to speed up the find or create operation. Doing a fetch for
            // every mapitem in the loop to see if it already exists is extremely resource-intensive.
            // This is suggested as an alternative in the Apple documentation.
            NSMutableArray* receivedItemsIDs = [[NSMutableArray alloc] initWithCapacity:10];
            
            // first extract the IDs of the mapItems we just received and sort them
            for(id item in entetiesArray)
            {
                if(item != (id)[NSNull null])
                {
                    [receivedItemsIDs addObject:(NSString*)[item objectForKey:receivedPredicatePrimaryKey]];
                }
                else
                {
                    [receivedEntetiesArray removeObject:item];
                }
            }
            [receivedItemsIDs sortUsingSelector:@selector(compare:)];
            // then get the IDs of all existing mapItems and sort them
            //            NSSortDescriptor* descriptor = [[NSSortDescriptor alloc] initWithKey:predicateKeyAttribute ascending:YES];
            NSPredicate* predicate = [NSPredicate predicateWithFormat:@"%K IN %@",predicateKeyAttribute, receivedItemsIDs];//,predicateKeyAttribute, receivedItemsIDs
            
            NSArray* existingItemsInCoreData = [self fetchManagedObjectsWithName:entityName
                                                                       predicate:predicate
                                                                 sortDescriptors:nil
                                                                           inMOC:temporaryContext];
            
            NSArray *sortedArray = [existingItemsInCoreData sortedArrayUsingComparator: ^(id obj1, id obj2) {
                double n1 = [[obj1 valueForKey:predicateKeyAttribute] intValue];
                double n2 = [[obj2 valueForKey:predicateKeyAttribute] intValue];
                if (n1 > n2) {
                    return (NSComparisonResult)NSOrderedDescending;
                }
                
                if (n1 < n2) {
                    return (NSComparisonResult)NSOrderedAscending;
                }
                
                return (NSComparisonResult)NSOrderedSame;
            }];
            
            existingItemsInCoreData = sortedArray;
            
            NSSortDescriptor* itemDesctiptor = [[NSSortDescriptor alloc] initWithKey:receivedPredicatePrimaryKey ascending:YES];
            receivedEntetiesArray = [[receivedEntetiesArray sortedArrayUsingDescriptors:[NSArray arrayWithObjects:itemDesctiptor, nil]] mutableCopy];
            // now go through the arrays and add the items that don't already exist
            
            int emictr = 0;
            int mictr = 0;
            int savedctr = 0;
            int newctr = 0;
            int updatectr = 0;
            // add every MapItem that doesn't already exist (TEMPORARY until change date is added!)
            while(mictr < [receivedEntetiesArray count])
            {
                NSNumber* emiid = [existingItemsInCoreData count] > emictr ? [[existingItemsInCoreData objectAtIndex:emictr] valueForKey:predicateKeyAttribute] : nil;
                NSNumber* miid = [[receivedEntetiesArray objectAtIndex:mictr] objectForKey:receivedPredicatePrimaryKey];
                id item = [receivedEntetiesArray objectAtIndex:mictr];
                // item already exits, update it
                id entity = nil;
                
                if(emiid && [emiid intValue] == [miid intValue])
                {
                    entity = [existingItemsInCoreData objectAtIndex:emictr];
                    
                    BOOL deleteItem = NO;
                    
                    if(activeAttributeName)
                    {
                        if ([item valueForKey:activeAttributeName])
                        {
                            deleteItem  = YES;
                        }
                    }
                    
                    if(!deleteItem)
                    {
                        [self addZDayFromItem:item toEntity:entity withEntityName:entityName];
                        [self saveItem:item toEntity:entity withMapping:attributeMappings];
                    }
                    else
                    {
                        NSLog(@"BRISEM %@ sa id-jem: %@", entityName, [entity valueForKey:predicateKeyAttribute]);
                        //delete the entity
                        
                        [temporaryContext deleteObject:entity];
                    }
                    emictr++;
                    mictr++;
                    updatectr++;
                }
                else
                {
                    BOOL skipItem = NO;
                    
                    if(activeAttributeName)
                    {
                        if ([item valueForKey:activeAttributeName])
                        {
                            skipItem  = YES;
                        }
                    }
                    
                    if(!skipItem)
                    {
                        entity = [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext:temporaryContext];
                        
                        [self addZDayFromItem:item toEntity:entity withEntityName:entityName];
                        [self saveItem:item toEntity:entity withMapping:attributeMappings];
                    }
                    else
                    {
                        NSLog(@"Preskacem upis");
                    }
                    
                    mictr++;
                    newctr++;
                }
                
                // save in smaller batches, which drastically speeds up insertion
                savedctr++;
                if(savedctr % 200 == 0)
                {
                    NSError *error;
                    if (![temporaryContext save:&error])
                    {
                        NSLog(@"There was an error saving the context: %@", [error localizedDescription]);
                    }
                }
            }
            // save anything that's left over
            
            if (![temporaryContext save:&error])
            {
                NSLog(@"There was an error saving the context: %@", [error localizedDescription]);
            }
            
            NSLog(@"Finished inserting %d %@",savedctr, entityName);
        }
        
        // save parent to disk asynchronously
        [self.managedObjectContext performBlock:^{
            NSError *error;
            if (![self.managedObjectContext save:&error])
            {
                NSLog(@"Error saving main context. Linking. %@", error.userInfo);
            }
            
            [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@Saved", entityName] object:self];
            
            finished(YES);
        }];
    }];
}

- (void)insertEntitiesArray:(NSArray*)entetiesArray withName:(NSString*)entityName receivedPredicatePrimaryKey:(NSString*)receivedPredicatePrimaryKey predicateKeyAttribute:(NSString*)predicateKeyAttribute sortDescriptorKey:(NSString*)sortKey activeAttributeName:(NSString*)activeAttributeName withCompletitionBlock:(void(^)(bool))finished
{
    NSManagedObjectContext *temporaryContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    temporaryContext.parentContext = self.managedObjectContext;
    // 2
    [temporaryContext performBlock:^{
        
        if(entetiesArray.count > 0)
        {
            // add all the mapItemArray
            NSMutableArray* receivedEntetiesArray = [entetiesArray mutableCopy];
            
            NSError *error = nil;
            
            // The following code aims to speed up the find or create operation. Doing a fetch for
            // every mapitem in the loop to see if it already exists is extremely resource-intensive.
            // This is suggested as an alternative in the Apple documentation.
            NSMutableArray* receivedItemsIDs = [[NSMutableArray alloc] initWithCapacity:10];
            
            // first extract the IDs of the mapItems we just received and sort them
            for(id item in entetiesArray)
            {
                if(item != (id)[NSNull null])
                {
                    [receivedItemsIDs addObject:(NSString*)[item objectForKey:receivedPredicatePrimaryKey]];
                }
                else
                {
                    [receivedEntetiesArray removeObject:item];
                }
            }
            [receivedItemsIDs sortUsingSelector:@selector(compare:)];
            // then get the IDs of all existing mapItems and sort them
            NSSortDescriptor* descriptor = [[NSSortDescriptor alloc] initWithKey:predicateKeyAttribute ascending:YES];
            NSPredicate* predicate = [NSPredicate predicateWithFormat:@"%K IN %@",predicateKeyAttribute, receivedItemsIDs];//,predicateKeyAttribute, receivedItemsIDs
            
            NSArray* existingItemsInCoreData = [self fetchManagedObjectsWithName:entityName
                                                                       predicate:predicate
                                                                 sortDescriptors:[NSArray arrayWithObjects:descriptor, nil]
                                                                           inMOC:temporaryContext];
            
            //            existingItemsInCoreData = [existingItemsInCoreData sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptor, nil]];
            
            NSSortDescriptor* itemDesctiptor = [[NSSortDescriptor alloc] initWithKey:receivedPredicatePrimaryKey ascending:YES];
            receivedEntetiesArray = [[receivedEntetiesArray sortedArrayUsingDescriptors:[NSArray arrayWithObjects:itemDesctiptor, nil]] mutableCopy];
            // now go through the arrays and add the items that don't already exist
            
            int emictr = 0;
            int mictr = 0;
            int savedctr = 0;
            int newctr = 0;
            int updatectr = 0;
            // add every MapItem that doesn't already exist (TEMPORARY until change date is added!)
            while(mictr < [receivedEntetiesArray count])
            {
                NSNumber* emiid = [existingItemsInCoreData count] > emictr ? [[existingItemsInCoreData objectAtIndex:emictr] valueForKey:predicateKeyAttribute] : nil;
                NSNumber* miid = [[receivedEntetiesArray objectAtIndex:mictr] objectForKey:receivedPredicatePrimaryKey];
                id item = [receivedEntetiesArray objectAtIndex:mictr];
                // item already exits, update it
                id entity = nil;
                
                if(emiid && [emiid intValue] ==[miid intValue])
                {
                    
                    entity = [existingItemsInCoreData objectAtIndex:emictr];
                    
                    BOOL deleteItem = NO;
                    
                    if(activeAttributeName)
                    {
                        if ([item valueForKey:activeAttributeName])
                        {
                            deleteItem  = YES;
                        }
                    }
                    
                    if(!deleteItem)
                    {
                        [self addZDayFromItem:item toEntity:entity withEntityName:entityName];
                        [self saveItem:item toEntity:entity];
                    }
                    else
                    {
                        NSLog(@"BRISEM %@ sa id-jem: %@", entityName, [item valueForKey:predicateKeyAttribute]);
                        //delete the entity
                        [temporaryContext deleteObject:entity];
                    }
                    emictr++;
                    mictr++;
                    updatectr++;
                }
                else
                {
                    BOOL skipItem = NO;
                    
                    if(activeAttributeName)
                    {
                        if ([item valueForKey:activeAttributeName])
                        {
                            skipItem  = YES;
                        }
                    }
                    
                    if(!skipItem)
                    {
                        entity = [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext:temporaryContext];
                        
                        [self addZDayFromItem:item toEntity:entity withEntityName:entityName];
                        [self saveItem:item toEntity:entity];
                    }
                    else
                    {
                        NSLog(@"Preskacem upis za entitet: %@", entityName);
                    }
                    
                    mictr++;
                    newctr++;
                }
                
                // save in smaller batches, which drastically speeds up insertion
                savedctr++;
                if(savedctr % 200 == 0)
                {
                    NSError *error;
                    if (![temporaryContext save:&error])
                    {
                        NSLog(@"There was an error saving the context: %@", [error localizedDescription]);
                    }
                }
            }
            // save anything that's left over
            
            if (![temporaryContext save:&error])
            {
                NSLog(@"There was an error saving the context: %@", [error localizedDescription]);
            }
            
            NSLog(@"Finished inserting %d %@",savedctr, entityName);
        }
        
        // save parent to disk asynchronously
        [self.managedObjectContext performBlock:^{
            NSError *error;
            if (![self.managedObjectContext save:&error])
            {
                NSLog(@"Error saving main context. Linking. %@", error.userInfo);
            }
            
            [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@Saved", entityName] object:self];
            
            finished(YES);
        }];
    }];
    
}
-(void)addZDayFromItem:(id)item toEntity:(id)entity withEntityName:(NSString*) entityName
{
    if([entityName isEqualToString:@"Session"])
    {
        if ( [item objectForKey:@"starts_at"] != (id)[NSNull null])
        {
            [entity setValue:[Utils getDate:[item objectForKey:@"starts_at"]] forKeyPath:@"zetDay"];
        }
    }
    
    if([entityName isEqualToString:@"Talk"])
    {
        if ([item objectForKey:@"starts_at"] != (id)[NSNull null])
        {
            [entity setValue:[Utils getDate:[item objectForKey:@"starts_at"]] forKeyPath:@"zetDay"];
        }
    }
}

-(void)saveItem:(NSDictionary*)item toEntity:(id)entity withMapping:(NSDictionary*)attributeMapping
{
    if(attributeMapping)
    {
        NSDictionary *attributes = [[entity entity] attributesByName];
        
        for(NSString* managedObjectAttributeName in attributeMapping.allKeys)
        {
            if ([entity respondsToSelector:NSSelectorFromString(managedObjectAttributeName)])
            {
//                if([item objectForKey:[attributeMapping valueForKey:managedObjectAttributeName]] != (id)[NSNull null])
                    if([item objectForKey:[attributeMapping valueForKey:managedObjectAttributeName]])
                {
                    id value = [item objectForKey:[attributeMapping valueForKey:managedObjectAttributeName]] != (id)[NSNull null]?[item objectForKey:[attributeMapping valueForKey:managedObjectAttributeName]]:nil;
                    
                    //check if we need nsnumber
                    NSAttributeDescription *keyAttribute = [attributes objectForKey:managedObjectAttributeName];
                    
                    NSAttributeType attributeType = keyAttribute.attributeType;
                    
                    if ((attributeType == NSStringAttributeType) && ([value isKindOfClass:[NSNumber class]]))
                    {
                        value = [value stringValue];
                    }
                    else if (((attributeType == NSInteger16AttributeType) || (attributeType == NSInteger32AttributeType) || (attributeType == NSInteger64AttributeType) || (attributeType == NSBooleanAttributeType)) && ([value isKindOfClass:[NSString class]]))
                    {
                        value = [NSNumber numberWithInteger:[value  integerValue]];
                    }
                    else if ((attributeType == NSFloatAttributeType) && ([value isKindOfClass:[NSString class]]))
                    {
                        value = [NSNumber numberWithDouble:[value doubleValue]];
                    }
                    
                    [entity setValue:value forKey:managedObjectAttributeName];
                    
                    //                    if([keyAttribute.attributeValueClassName isEqualToString:@"NSNumber"])
                    //                    {
                    //                        [entity setValue:[NSNumber numberWithInt:[[item objectForKey:[attributeMapping valueForKey:key]] intValue]]  forKey:key];
                    //                    }
                    //                    else
                    //                    {
                    //                        [entity setValue:[item objectForKey:[attributeMapping valueForKey:key]] forKey:key];
                    //                    }
                }
            }
            //
            //            id value = [item objectForKey:] != (id)[NSNull null]?[item objectForKey:[attributeMapping valueForKey:key]]:nil;
            //
            //            if([value isKindOfClass:[NSNumber class]])
            //            {
            //                value = [value stringValue];
            //            }
            //
            //            [entity setValue:value forKey:key];
        }
    }
    else
    {
        [self saveItem:item toEntity:entity];
    }
}

-(void)saveItem:(NSDictionary*)item toEntity:(id)entity
{
    if([entity class] == [Speaker class])
    {
        //             parCreatedOn = "2014-07-14 19:45:13";
        //             parDescription = "<null>";
        //             parFirstName = Bjoerg;
        //             parIsHCP = 0;
        //             parLastName = "Eva Skogoey";
        //             parModifiedOn = "2014-07-14 19:45:13";
        //             parPartyId = 1178;
        //             parPartyType = 1;
        //             parPicture = "<null>";
        //             parStatus = 1;
        
        Speaker* speaker = (Speaker*)entity;
        
        speaker.spkFirstName = [item objectForKey:@"spkName"] != (id)[NSNull null]?[[item objectForKey:@"spkName"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]:nil;
        speaker.spkLastName = [item objectForKey:@"spkLastName"] != (id)[NSNull null]?[[item objectForKey:@"spkLastName"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]:nil;
        speaker.spkPicture = [item objectForKey:@"spkPicture"] != (id)[NSNull null]?[item objectForKey:@"spkPicture"]:nil;
    }
    else
    {
        for(NSString* key in [item allKeys])
        {
            if ([entity respondsToSelector:NSSelectorFromString(key)])
            {
                if([item objectForKey:key] != (id)[NSNull null])
                {
                    //check if we need nsnumber
                    NSDictionary *attributes = [[entity entity] attributesByName];
                    NSAttributeDescription *keyAttribute = [attributes objectForKey:key];
                    
                    if([keyAttribute.attributeValueClassName isEqualToString:@"NSNumber"])
                    {
                        [entity setValue:[NSNumber numberWithInt:[[item objectForKey:key] intValue]]  forKey:key];
                    }
                    else
                    {
                        [entity setValue:[item objectForKey:key] forKey:key];
                    }
                }
            }
        }
    }
}

-(void)deleteAllDataForEntity:(NSString*)entityName
{
    NSFetchRequest * allDataRequest = [[NSFetchRequest alloc] init];
    [allDataRequest setEntity:[NSEntityDescription entityForName:entityName inManagedObjectContext:[LLDataAccessLayer sharedInstance].managedObjectContext]];
    [allDataRequest setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSUInteger count = [[LLDataAccessLayer sharedInstance].managedObjectContext countForFetchRequest:allDataRequest error:&error];
    
    if(!error && count != 0)
    {
        NSArray * resultsArray = [[LLDataAccessLayer sharedInstance].managedObjectContext executeFetchRequest:allDataRequest error:&error];
        
        //error handling goes here
        for (NSManagedObject * mob in resultsArray) {
            [[LLDataAccessLayer sharedInstance].managedObjectContext deleteObject:mob];
        }
        NSError *saveError = nil;
        [[LLDataAccessLayer sharedInstance].managedObjectContext save:&saveError];
    }
}

-(void)deleteAllDataForEntity:(NSString*)entityName withPredicate:(NSPredicate *)predicate
{
    NSFetchRequest * allDataRequest = [[NSFetchRequest alloc] init];
    [allDataRequest setEntity:[NSEntityDescription entityForName:entityName inManagedObjectContext:[LLDataAccessLayer sharedInstance].managedObjectContext]];
//    [allDataRequest setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    [allDataRequest setPredicate:predicate];
    
    NSError * error = nil;
    NSUInteger count = [[LLDataAccessLayer sharedInstance].managedObjectContext countForFetchRequest:allDataRequest error:&error];
    
    if(!error && count != 0)
    {
        NSArray * resultsArray = [[LLDataAccessLayer sharedInstance].managedObjectContext executeFetchRequest:allDataRequest error:&error];
        
        //error handling goes here
        for (NSManagedObject * mob in resultsArray) {
            [[LLDataAccessLayer sharedInstance].managedObjectContext deleteObject:mob];
        }
        NSError *saveError = nil;
        [[LLDataAccessLayer sharedInstance].managedObjectContext save:&saveError];
    }
}

#pragma mark - Link one-to-many relation

-(void)linkEntityNameWithToManyRelation:(NSString*)entityNameWithToManyRelation withPKattributeName:(NSString*)pkAttributeName toEntityNameWithToOneRelation:(NSString*)entityNameWithToOneRelation withPKattributeName:(NSString*)toOnePKattributeName linkIdAttributeName:(NSString*)linkIdAttributeName andToManyRelationName:(NSString*)toManyRelationName useBgrdMOC:(BOOL)useBgrdMOC
{
    NSManagedObjectContext *temporaryContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    temporaryContext.parentContext = self.managedObjectContext;
    
    [temporaryContext performBlock:^{
        
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:entityNameWithToManyRelation];
        //request.predicate = [NSPredicate predicateWithFormat:@"venVenueId == %@", [dict valueForKey:@"venVenueId"]];
        
        request.sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:pkAttributeName ascending:YES]];
        
        NSArray *arrayResults = [temporaryContext executeFetchRequest:request error:nil];
        
        int savedctr = 0;
        
        NSLog(@"Linking %@ to %@", entityNameWithToManyRelation, entityNameWithToOneRelation);
        
        for(id itemWithToManyRelation in arrayResults)
        {
            if(![self entityWasDeleted:itemWithToManyRelation inMOC:temporaryContext])
            {
                NSFetchRequest *requestB = [NSFetchRequest fetchRequestWithEntityName:entityNameWithToOneRelation];
                
                requestB.predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"%@ == %@", linkIdAttributeName, [itemWithToManyRelation valueForKey:pkAttributeName]]];
                
                requestB.sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:toOnePKattributeName ascending:YES]];
                
                NSArray *arrayResultsB = [temporaryContext executeFetchRequest:requestB error:nil];
                
                for(id itemWithToOneRelation in arrayResultsB)
                {
                    if (![self entityWasDeleted:itemWithToManyRelation inMOC:temporaryContext] || ![self entityWasDeleted:itemWithToOneRelation inMOC:temporaryContext])
                    {
                        [itemWithToOneRelation setValue:itemWithToManyRelation forKey:toManyRelationName];//NSLog(@"Conexión hecha T-S");
                    }
                    savedctr++;
                }
                
                //            if(savedctr % 50 == 0)
                //            {
                //                NSError *error;
                //                if (![temporaryContext save:&error])
                //                {
                //                    NSLog(@"There was an error saving the context: %@", [error localizedDescription]);
                //                }
                //
                //            }
            }
        }
        
        NSLog(@"Saving %d %@ to %@ ended", savedctr, entityNameWithToOneRelation, entityNameWithToManyRelation);
        NSError *error;
        if (![temporaryContext save:&error])
        {
            // handle error
            NSLog(@"Error saving temp context. Linking. %@", error.userInfo);
        }
        
        // save parent to disk asynchronously
        [self.managedObjectContext performBlock:^{
            NSError *error;
            if (![self.managedObjectContext save:&error])
            {
                NSLog(@"Error saving main context. Linking. %@", error.userInfo);
            }
            //save sync settings
            NSString *syncKey = [NSString stringWithFormat:@"Linked%@%@", entityNameWithToOneRelation, entityNameWithToManyRelation];
            ConferenceCoreDataLink *link = [[RealmRepository shared] link];
            if ([syncKey isEqualToString:@"LinkedSessionVenue"]) {
                link.blockLocation = NO;
            } else if ([syncKey isEqualToString:@"LinkedMaterialTalk"]) {
                link.presentationMaterial = NO;
            } else if ([syncKey isEqualToString:@"LinkedTalkNote"]) {
                link.presentationNote = NO;
            } else if ([syncKey isEqualToString:@"LinkedTalkOrder"]) {
                link.presentationOrder = NO;
            } else if ([syncKey isEqualToString:@"LinkedTalkSession"]) {
                link.presentationBlock = NO;
            } else if ([syncKey isEqualToString:@"LinkedTalkSpeaker"]) {
                link.presentationSpeaker = NO;
            } else if ([syncKey isEqualToString:@"LinkedExhibitorRoleSession"]) {
                link.blockExhibitorRole = NO;
            } else if ([syncKey isEqualToString:@"LinkedBoothExhibitor"]) {
                link.boothExhibitor = NO;
            } else if ([syncKey isEqualToString:@"LinkedExhibitorRoleExhibitor"]) {
                link.exhibitorExhibitorRole = NO;
            }
            [[RealmRepository shared] persistLink:link];

            if ([entityNameWithToOneRelation isEqualToString:@"Note"])
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"NoteSynced" object:nil];
            }
            
            [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"Linked"] object:self];
        }];
    }];
}

#pragma mark - Link one-to-many relation  withCompletitionBlock

-(void)linkEntityNameWithToManyRelation:(NSString*)entityNameWithToManyRelation withPKattributeName:(NSString*)pkAttributeName toEntityNameWithToOneRelation:(NSString*)entityNameWithToOneRelation withPKattributeName:(NSString*)toOnePKattributeName linkIdAttributeName:(NSString*)linkIdAttributeName andToManyRelationName:(NSString*)toManyRelationName useBgrdMOC:(BOOL)useBgrdMOC withCompletitionBlock:(void(^)(void))finished
{
    NSManagedObjectContext *temporaryContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    temporaryContext.parentContext = self.managedObjectContext;
    
    [temporaryContext performBlock:^{
        
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:entityNameWithToManyRelation];
        //request.predicate = [NSPredicate predicateWithFormat:@"venVenueId == %@", [dict valueForKey:@"venVenueId"]];
        
        request.sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:pkAttributeName ascending:YES]];
        
        NSArray *arrayResults = [temporaryContext executeFetchRequest:request error:nil];
        
        int savedctr = 0;
        
        NSLog(@"Linking %@ to %@", entityNameWithToManyRelation, entityNameWithToOneRelation);
        
        for(id itemWithToManyRelation in arrayResults)
        {
            if(![self entityWasDeleted:itemWithToManyRelation inMOC:temporaryContext])
            {
                NSFetchRequest *requestB = [NSFetchRequest fetchRequestWithEntityName:entityNameWithToOneRelation];
                
                requestB.predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"%@ == %@", linkIdAttributeName, [itemWithToManyRelation valueForKey:pkAttributeName]]];
                
                requestB.sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:toOnePKattributeName ascending:YES]];
                
                NSArray *arrayResultsB = [temporaryContext executeFetchRequest:requestB error:nil];
                
                for(id itemWithToOneRelation in arrayResultsB)
                {
                    if (![self entityWasDeleted:itemWithToManyRelation inMOC:temporaryContext] || ![self entityWasDeleted:itemWithToOneRelation inMOC:temporaryContext])
                    {
                        [itemWithToOneRelation setValue:itemWithToManyRelation forKey:toManyRelationName];//NSLog(@"Conexión hecha T-S");
                    }
                    savedctr++;
                }
            }
        }
        
        NSLog(@"Saving %d %@ to %@ ended", savedctr, entityNameWithToOneRelation, entityNameWithToManyRelation);
        NSError *error;
        if (![temporaryContext save:&error])
        {
            // handle error
            NSLog(@"Error saving temp context. Linking. %@", error.userInfo);
        }
        
        // save parent to disk asynchronously
        [self.managedObjectContext performBlock:^{
            NSError *error;
            if (![self.managedObjectContext save:&error])
            {
                NSLog(@"Error saving main context. Linking. %@", error.userInfo);
            }
            
            finished();
        }];
    }];
}

- (BOOL) entityWasDeleted:(id)someEntity inMOC:(NSManagedObjectContext*)moc
{
    NSError *err;
    return ((someEntity == nil) || ([moc existingObjectWithID:[someEntity objectID] error:&err] == nil));
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

//-(NSArray*)getJsonLeadsForDatabaseVC:(NSArray*) dataParameterNames
//{
//    NSArray* leadsObjectsArray = [NovartisLead getLeads];
//    if(leadsObjectsArray.count == 0)
//        return nil;
//
//    NSMutableArray* leadsDataJson = [[NSMutableArray alloc] initWithCapacity:30];
//
//
//    NSEntityDescription * myEntity = [[leadsObjectsArray objectAtIndex:0] entity];
//    NSDictionary* managedObjectAtributes = [myEntity attributesByName];
//
//    for (NSManagedObject* leadObject in leadsObjectsArray)
//    {
//        NSMutableDictionary* leadDataJson = [[NSMutableDictionary alloc] initWithCapacity:30];
//        //convert data to json
//        for(NSString* atribute in [managedObjectAtributes allKeys])
//        {
//            [leadDataJson setValue:[leadObject valueForKey:atribute]?[leadObject valueForKey:atribute]:@"" forKey:atribute];
//
//            //convert data to json
//            //            for(NSDictionary* databaseVCAtribute in dataParameterNames)
//            //            {
//            //                if([atribute isEqualToString:[databaseVCAtribute objectForKey:@"coreDataName"]])
//            //                {
//            //                    [leadDataJson setObject:[leadObject valueForKey:[databaseVCAtribute objectForKey:@"coreDataName"]] forKey:[databaseVCAtribute objectForKey:@"vcPresentName"]];
//            //                }
//            //            }
//        }
//
//        [leadsDataJson addObject:leadDataJson];
//    }
//
//    return leadsDataJson;
//}

- (void)_mocDidSaveNotification:(NSNotification *)notification
{
    NSManagedObjectContext *savedContext = [notification object];
    
    // ignore change notifications for the main MOC
    if (_managedObjectContext == savedContext)
    {
        return;
    }
    
    if (_managedObjectContext.persistentStoreCoordinator != savedContext.persistentStoreCoordinator)
    {
        // that's another database
        return;
    }
    
    dispatch_sync(dispatch_get_main_queue(), ^{
        [_managedObjectContext mergeChangesFromContextDidSaveNotification:notification];
    });
}

#pragma mark - Reset Program Data

- (void)resetProgramData
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"Venue" inManagedObjectContext:self.managedObjectContext]];
    
    [fetchRequest setReturnsDistinctResults:YES];
    [fetchRequest setPropertiesToFetch:[NSArray arrayWithObjects:@"venVenueId", @"venName", nil]];
    
    // Add a sort descriptor. Mandatory.
    [fetchRequest setSortDescriptors:[NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"venName" ascending:YES], nil]];
    
    NSError *error;
    NSArray *venuesArray = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (venuesArray == nil)
    {
        // Handle the error.spe
        NSLog(@"executeFetchRequest failed with error: %@", [error localizedDescription]);
    }
    
    for (Venue* ven in venuesArray)
    {
        //Set session times for venue and days
        [self resetTimesForVenue:ven];
    }
    
    [[LLDataAccessLayer sharedInstance].managedObjectContext save:nil];
}

- (void)resetTimesForVenue:(Venue*)ven
{
    //   NSLog(@"VENUE---------------------------------------------: %@", ven.venName);
    //format datuma u bazi
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    
    [format setDateFormat:kDateFormatInDatabase];
    
    //trenutni datum i vreme
    NSDate *now = [NSDate date];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    //vracamo vreme za jedan dan i sat
    NSDateComponents *compsAddHour = [[NSDateComponents alloc] init];
    //    [compsAddHour setDay:-1];
    //    [compsAddHour setHour:-1];
    
    //dodajemo komponente na trenutni datum
    NSDate *newDate = [calendar dateByAddingComponents:compsAddHour toDate:now options:0];
    
    //resetujemo minute na nulu
    NSDateComponents *compsSetMinute = [calendar components:NSUIntegerMax fromDate:newDate];
    
    //    [compsSetMinute setMinute:0];
    
    //pocetno datum i vreme koje cemo koristiti
    NSDate *startDateTime = [calendar dateFromComponents:compsSetMinute];
    
    NSDate *startDayTime = nil;
    
    NSArray* days = [Session getProgramDays];
    
    for (int i = 1; i < days.count+1; i++)
    {
        //        if(i>1)
        //        {
        //            NSDateComponents *compsAddHour = [[NSDateComponents alloc] init];
        //            [compsAddHour setDay:i-1];
        //
        //            startDayTime = [calendar dateByAddingComponents:compsAddHour toDate:startDateTime options:0];
        //        }
        //        else
        //        {
        startDayTime = startDateTime;
        //        }
        
        NSPredicate* predicate = [NSPredicate predicateWithFormat:@"sesVenueId = %@ && sesStartTime CONTAINS[cd] %@", ven.venVenueId, days[i-1]];
        NSArray* sessionsArray  = [self fetchManagedObjectsWithName:@"Session"
                                                          predicate:predicate
                                                    sortDescriptors:[NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"sesSessionId" ascending:YES], nil] inMOC:self.managedObjectContext];
        
        int sessionIndex = 0;
        int talkIndex = 0;
        
        NSDate *sessionStartTime = nil;
        
        //       NSLog(@"Day: %d, Venue: %@",i, ven.venName);
        
        for (Session* ses in sessionsArray)
        {
            //pocetno datum i vreme koje cemo koristiti
            if(sessionIndex == 0)
            {
                sessionStartTime = startDayTime;
            }
            else
            {
                if(talkIndex == 0)
                {
                    sessionStartTime = [sessionStartTime dateByAddingTimeInterval:(1*60)+2*60] ;
                }
                else
                {
                    sessionStartTime = [sessionStartTime dateByAddingTimeInterval:talkIndex *(1*60)+2*60] ;
                }
            }
            
            ses.sesStartTime = [format stringFromDate:sessionStartTime];
            
            //            NSLog(@"DAY %d, Session: %@,  starts: %@",i,ses.sesName, ses.sesStartTime);
            //            NSLog(@"DAY %d, Session: %@",i,ses.sesName);
            
            talkIndex = 0;
            
            for (Talk* tlk in ses.talks)
            {
                NSDate *talkEndTime;
                
                if (talkIndex == 0)
                {
                    tlk.tlkStartTime = [format stringFromDate:sessionStartTime];
                    
                    talkEndTime = [sessionStartTime dateByAddingTimeInterval: (1*60)];
                    tlk.tlkEndTime = [format stringFromDate:talkEndTime];
                }
                else
                {
                    NSDate *talkStartTime = [sessionStartTime dateByAddingTimeInterval:talkIndex * (1*60)];
                    tlk.tlkStartTime = [format stringFromDate:talkStartTime];
                    
                    talkEndTime = [talkStartTime dateByAddingTimeInterval:(1*60)];
                    tlk.tlkEndTime = [format stringFromDate:talkEndTime];
                }
                //                NSLog(@"Talk %@ starts %@ ENDS: %@", tlk.tlkTitle, tlk.tlkStartTime, tlk.tlkEndTime);
                
                talkIndex ++;
            }
            
            NSDate *sessionEndTime = [sessionStartTime dateByAddingTimeInterval:talkIndex * (1*60)];
            ses.sesEndTime = [format stringFromDate:sessionEndTime];
            
            sessionIndex++;
            //            NSLog(@"Session DAY %d ENDS: %@",i, ses.sesEndTime);
            //             NSLog(@"DAY %d, Session: %@,  starts: %@ ENDS: %@",i,ses.sesName, ses.sesStartTime,ses.sesEndTime);
        }
    }
}

-(NSMutableDictionary*)getJsonForObject:(NSManagedObject*)leadObject
{
    NSMutableDictionary* leadDataJson = [[NSMutableDictionary alloc] initWithCapacity:30];
    
    NSEntityDescription * myEntity = [leadObject entity];
    NSDictionary* managedObjectAtributes = [myEntity attributesByName];
    //convert data to json
    for(NSString* atribute in [managedObjectAtributes allKeys])
    {
        // NSString* value = [[leadData objectForKey:key] isKindOfClass:[NSString class]] ?[leadData objectForKey:key]:[leadData objectForKey:key];
        [leadDataJson setValue:[leadObject valueForKey:atribute] forKey:atribute];
    }
    
    return leadDataJson;
}

-(void)resetUserData
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"available"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"spent"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kUserToken];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"downloaded-app"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"user_id"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kKeyActiveMaterialBrowsingSession];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    //Reset My timeline
    [Talk resetMyTimeline];
    [Talk resetVotes];

    [Session resetAttendedSessions];
    
    // Remove materials in materials directory
//    NSString *libraryPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
//    [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@/materials" , libraryPath] error:nil];
    
    [self deleteAllDataForEntity:@"Order"];
    [self deleteAllDataForEntity:@"Material"];
    [self deleteAllDataForEntity:@"TalkQA"];
    [self deleteAllDataForEntity:@"Note"];
    [self deleteAllDataForEntity:@"Epoint"];
    [self deleteAllDataForEntity:@"HighlightedSlide"];
    [self deleteAllDataForEntity:@"MaterialBrowsing"];    
}

@end
