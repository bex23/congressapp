//
//  DADataAccessLayer.h
//  DentalApp
//
//  Created by admin on 11/5/12.
//  Copyright (c) 2012 OXY. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Venue+CoreDataClass.h"

@interface LLDataAccessLayer : NSObject

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

+ (LLDataAccessLayer *)sharedInstance;
- (void)setBgrndManagedObjectContext;

- (void)saveContextWithMOC:(NSManagedObjectContext*)moc;
- (void)rollbackDefaultMOC;
- (void)deleteManagedObjectFromDefaultMOC:(NSManagedObject *)managedObject;

- (NSArray *) fetchManagedObjectsWithName:(NSString *)entityName predicate: (NSPredicate *)predicate sortDescriptors:(NSArray *)sortDescriptors inMOC:(NSManagedObjectContext *)moc;

- (NSManagedObject *)fetchManagedObjectWithName:(NSString *)entityName predicate:(NSPredicate *)predicate sortDescriptors:(NSArray *)sortDescriptors inMOC:(NSManagedObjectContext *)moc;

- (void)insertEntitiesArray:(NSArray*)entetiesArray withName:(NSString*)entityName receivedPredicatePrimaryKey:(NSString*)receivedPredicatePrimaryKey predicateKeyAttribute:(NSString*)predicateKeyAttribute sortDescriptorKey:(NSString*)sortKey activeAttributeName:(NSString*)activeAttributeName withCompletitionBlock:(void(^)(bool))finished;
- (void)insertEntitiesArray:(NSArray*)entetiesArray withName:(NSString*)entityName receivedPredicatePrimaryKey:(NSString*)receivedPredicatePrimaryKey predicateKeyAttribute:(NSString*)predicateKeyAttribute sortDescriptorKey:(NSString*)sortKey activeAttributeName:(NSString*)activeAttributeName attributeMappings: (NSDictionary*)attributeMappings withCompletitionBlock:(void(^)(bool))finished;

-(void)linkEntityNameWithToManyRelation:(NSString*)entityNameWithToManyRelation withPKattributeName:(NSString*)pkAttributeName toEntityNameWithToOneRelation:(NSString*)entityNameWithToOneRelation withPKattributeName:(NSString*)toOnePKattributeName linkIdAttributeName:(NSString*)linkIdAttributeName andToManyRelationName:(NSString*)toManyRelationName useBgrdMOC:(BOOL)useBgrdMOC;
//-(NSArray*)getJsonLeadsForDatabaseVC:(NSArray*) dataParameterNames;
-(NSMutableDictionary*)getJsonForObject:(NSManagedObject*)leadObject;

- (void)resetUserData;
- (void)resetProgramData;
- (void)resetTimesForVenue:(Venue*)ven;
-(void)deleteAllDataForEntity:(NSString*)entityName;

-(void)deleteAllDataForEntity:(NSString*)entityName withPredicate:(NSPredicate *)predicate;


-(void)linkEntityNameWithToManyRelation:(NSString*)entityNameWithToManyRelation withPKattributeName:(NSString*)pkAttributeName toEntityNameWithToOneRelation:(NSString*)entityNameWithToOneRelation withPKattributeName:(NSString*)toOnePKattributeName linkIdAttributeName:(NSString*)linkIdAttributeName andToManyRelationName:(NSString*)toManyRelationName useBgrdMOC:(BOOL)useBgrdMOC withCompletitionBlock:(void(^)(void))finished;

@end
