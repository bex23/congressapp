//
//  IAExibitorDetailsViewController.h
//  ERA-EDTA2015
//
//  Created by Predrag Despotović on 7/19/14.
//  Copyright (c) 2014 Navus. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Exhibitor;

@interface IAExibitorDetailsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *imgExibitorLogo;
@property (weak, nonatomic) IBOutlet UITextView *txtExibitorDetails;
@property (strong, nonatomic) IBOutlet UILabel *lblExibitorName;
@property (strong, nonatomic) Exhibitor* exibitor;
@property (strong, nonatomic) IBOutlet UIButton *goToNavigationMap;
@property (weak, nonatomic) IBOutlet UILabel *labelSiteLink;

// TEMP CODE ////////////////
@property (strong, nonatomic) NSString *strExhibName;
@property (strong, nonatomic) NSString *strImageName;
@property (strong, nonatomic) NSString *strSiteLink;
////////////////

@end
