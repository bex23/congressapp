//
//  IAExibitorDetailsViewController.m
//  ERA-EDTA2015
//
//  Created by Predrag Despotović on 7/19/14.
//  Copyright (c) 2014 Navus. All rights reserved.
//

#import "IAExibitorDetailsViewController.h"

#import "ExhibitorMapViewController.h"

@interface IAExibitorDetailsViewController ()

@end

@implementation IAExibitorDetailsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"sponsor"])
    {
        UIButton *but = [UIButton buttonWithType:UIButtonTypeCustom];
        [but setBackgroundImage:[UIImage imageNamed:@"home"] forState:UIControlStateNormal];
        [but setBackgroundImage:[UIImage imageNamed:@"home_pressed"] forState:UIControlStateHighlighted];
        [but addTarget:self action:@selector(goHome) forControlEvents:UIControlEventTouchUpInside];
        but.bounds = CGRectMake(0, 0, 20, 20);
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:but];
    }
    
    UIImage* img = [UIImage imageNamed:@"exhibitors_placeholder"];
    
    if(self.exibitor.exhLogo.length > 0)
    {
        NSURL* url = [NSURL URLWithString:self.exibitor.exhLogo];
        [self.imgExibitorLogo setImageWithURL:url placeholderImage:img];
    }
    else
    {
        self.imgExibitorLogo.image = img;
    }
    
    self.lblExibitorName.text = self.exibitor.exhName;
    self.labelSiteLink.text = self.exibitor.exhWebsite;
    
    
    // TEMP CODE ////////////////////////////
    
//    self.imgExibitorLogo.image = [UIImage imageNamed:_strImageName];
//    self.lblExibitorName.text = _strExhibName;
//    self.labelSiteLink.text = _strSiteLink;
    
    ////////////////////////////
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // This line must set here because TextView doesn't show text from top if you set it in viewDidLoad

    self.txtExibitorDetails.text = self.exibitor.exhDescription;
    
//    self.txtExibitorDetails.text = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse tincidunt eros leo. Donec rhoncus posuere gravida. Sed orci augue, volutpat sed massa in, blandit egestas tortor. Donec vestibulum, augue vel pellentesque tempus, mauris massa interdum nulla, nec sagittis eros lectus sed est. Nullam scelerisque arcu non dolor fringilla, in fermentum augue suscipit. Ut vel purus a enim venenatis iaculis. Aenean dictum justo vulputate purus laoreet tempus. Fusce est purus, varius in sollicitudin at, sodales ac urna. Donec enim orci, congue nec vehicula id, aliquet at diam. Sed mi quam, tempus sed tristique sed, tempus sit amet eros. Vestibulum odio tellus, dignissim ac ex vitae, fringilla rutrum nulla.\nInteger viverra elit ipsum, a sollicitudin felis sollicitudin et. Curabitur sit amet felis nisl. Praesent rutrum, nisl luctus rhoncus condimentum, ligula metus scelerisque augue, quis volutpat libero eros vitae eros. Nulla congue placerat massa sit amet posuere. In vitae vestibulum lorem. Pellentesque sed lobortis quam, et posuere erat. Integer convallis mi ut erat pharetra, tempus convallis libero vestibulum. Aenean et arcu eros. Quisque vehicula, libero id ornare rhoncus, sem erat eleifend augue, nec lacinia purus dolor nec massa. Aliquam ut velit pellentesque, malesuada dolor quis, varius dolor. Cras sollicitudin sem ac orci hendrerit, et ullamcorper nulla imperdiet.\nAliquam facilisis, arcu in bibendum aliquet, leo massa vulputate orci, sit amet posuere nisl turpis eu purus. Curabitur in volutpat sapien. Ut et neque luctus, mollis mauris consequat, viverra lacus. Sed sollicitudin vehicula lorem, ullamcorper vulputate eros. Ut nec massa efficitur, tincidunt lectus sed, eleifend leo. Proin convallis vitae ligula vitae aliquet. Pellentesque vitae nulla imperdiet, faucibus lorem semper, commodo nisi. Mauris a accumsan orci. Morbi orci sapien, dignissim ac congue vitae, finibus in magna. Morbi auctor egestas turpis vitae porttitor. Mauris fringilla turpis nulla, nec auctor orci scelerisque posuere. In sagittis sed leo vel placerat. Donec eget iaculis augue. Nulla tempor lorem in nibh bibendum, in lacinia turpis vehicula.";
}

#pragma mark - Navigation

-(void)goHome
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - IBActions

- (IBAction)actionGoToExhibitionHall:(id)sender
{
    if(kEnableExhibitorsMapPinning == NO)
    {
        [Utilities showNotAvailableYetMessage];
        return;
    }
    
    NSString *strID = NSStringFromClass([ExhibitorMapViewController class]);
    ExhibitorMapViewController *vc = [[UIStoryboard storyboardWithName:strID bundle:nil] instantiateViewControllerWithIdentifier:strID];
    [vc setExibitor:self.exibitor];
    
    [self.navigationController pushViewController:vc animated:YES];
}

@end
