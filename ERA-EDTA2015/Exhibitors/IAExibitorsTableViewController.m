//
//  IAExibitorsTableViewController.m
//  ERA-EDTA2015
//
//  Created by admin on 7/3/14.
//  Copyright (c) 2014 Navus. All rights reserved.
//

#import "IAExibitorsTableViewController.h"

#import "IAExibitorTableViewCell.h"
#import "IAExibitorDetailsViewController.h"
#import "UIImageView+AFNetworking.h"

@interface IAExibitorsTableViewController ()

@property (strong,nonatomic)NSFetchedResultsController* fetchedResultsController;
@property (strong, nonatomic)NSManagedObjectContext* managedObjectContext;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSArray *arrayDummyExhibitors;
@property (strong, nonatomic) NSArray *arrayDummyExDetails;

@end

@implementation IAExibitorsTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.managedObjectContext = [LLDataAccessLayer sharedInstance].managedObjectContext;
    
    NSError *error;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"error %@ %@", error, error.userInfo);
        abort();
    }
    
    [self.tableView reloadData];
    
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"sponsor"])
    {
        UIButton *but = [UIButton buttonWithType:UIButtonTypeCustom];
        [but setBackgroundImage:[UIImage imageNamed:@"home"] forState:UIControlStateNormal];
        [but setBackgroundImage:[UIImage imageNamed:@"home_pressed"] forState:UIControlStateHighlighted];
        [but addTarget:self action:@selector(goHome) forControlEvents:UIControlEventTouchUpInside];
        but.bounds = CGRectMake(0, 0, 20, 20);
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:but];
    }
    
    self.arrayDummyExhibitors = @[@"amgen", @"basilea", @"gilead", @"janssen", @"jazz pharmaceuticals", @"msd"];
    self.arrayDummyExDetails = @[@"http://www.amgen.com/", @"http://basilea.com/", @"http://www.gilead.com/", @"http://www.janssen.com/", @"https://www.jazzpharma.com/", @"http://www.msd.com/"];
}

#pragma mark - Navigation

-(void)goHome
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark FRC

-(NSFetchedResultsController *)fetchedResultsController
{
    if (!_fetchedResultsController)
    {
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"Exhibitor" inManagedObjectContext:self.managedObjectContext];
        //NSPredicate *predicate = [NSPredicate predicateWithFormat:@"cod_event ==  %@", [self.eventInfoDictionary valueForKey:@"id_evento"]];
        //fetchRequest.predicate = predicate;
        [fetchRequest setEntity:entity];
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"exhName" ascending:YES];
        NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
        [fetchRequest setSortDescriptors:sortDescriptors];
//        [NSFetchedResultsController deleteCacheWithName:@"Exhibitors Cache"];
        NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
        self.fetchedResultsController = aFetchedResultsController;
    }
    return _fetchedResultsController;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//     return _arrayDummyExhibitors.count;
    
    int rows = 0;
    
    if ([[_fetchedResultsController sections] count] > 0) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[_fetchedResultsController sections] objectAtIndex:section];
        rows = (int)[sectionInfo numberOfObjects];
    }
    return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    IAExibitorTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"exibitorCell" forIndexPath:indexPath];

    Exhibitor* exhibitor = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    UIImage* img = [UIImage imageNamed:@"exhibitors_placeholder"];
    
    if(exhibitor.exhLogo.length > 0)
    {
        NSURL* url = [NSURL URLWithString:exhibitor.exhLogo];
        [cell.imgExibitorLogo setImageWithURL:url  placeholderImage:img];
    }
    else
    {
        cell.imgExibitorLogo.image = img;
    }
    
    cell.lblExibitorName.text = exhibitor.exhName;
    
//    NSString *strExh = _arrayDummyExhibitors[indexPath.row];
//    
//    UIImage* img = [UIImage imageNamed:strExh];
//    cell.imgExibitorLogo.image = img;
//    cell.lblExibitorName.text = strExh.uppercaseString;

    return cell;
}

-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    if ([tableView numberOfRowsInSection:0] > 0) {
        return @[@"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H", @"I", @"J", @"K", @"L", @"M", @"N", @"O", @"P", @"Q", @"R", @"S", @"T", @"U", @"V", @"W", @"X", @"Y", @"Z"];
    }
    
    return nil;
}

-(NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    if (title == UITableViewIndexSearch)
    {
        [tableView scrollRectToVisible:CGRectMake(0, 0, 320, 44) animated:YES];
        //            [tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:-1 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
        return -1;
    }
    else
    {
        for (int x = 0; x<_fetchedResultsController.fetchedObjects.count; x++) {
            
            Exhibitor *exhibitor = [_fetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:x inSection:0]];
            
            if ([exhibitor.exhName hasPrefix:title])
            {
                [tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:x inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
                return x;
            }
        }
    }
    return 2;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Navigation
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showExibitorDetails"])
    {
        NSIndexPath* selectedIndexPath = [self.tableView indexPathForSelectedRow];
        Exhibitor* exibitor = [self.fetchedResultsController objectAtIndexPath:selectedIndexPath];
        
        IAExibitorDetailsViewController* vc = [segue destinationViewController];
        [vc setExibitor:exibitor];
        
        [_tableView deselectRowAtIndexPath:selectedIndexPath animated:YES];
    }
    
//    if ([segue.identifier isEqualToString:@"showExibitorDetails"])
//    {
//        NSIndexPath* selectedIndexPath = [self.tableView indexPathForSelectedRow];
//
//        IAExibitorDetailsViewController* vc = [segue destinationViewController];
//        
//        vc.strExhibName = [[_arrayDummyExhibitors objectAtIndex:selectedIndexPath.row] uppercaseString];
//        vc.strImageName = [_arrayDummyExhibitors objectAtIndex:selectedIndexPath.row];
//        vc.strSiteLink = [_arrayDummyExDetails objectAtIndex:selectedIndexPath.row];
//        
//        [_tableView deselectRowAtIndexPath:selectedIndexPath animated:YES];
//    }
}

#pragma mark - Set left margin of table cells to ziro for iOS 8

-(void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

@end
