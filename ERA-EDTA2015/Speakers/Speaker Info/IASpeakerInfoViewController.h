//
//  IASpeakerInfoViewController.h
//  ERA-EDTA2015
//
//  Created by admin on 7/3/14.
//  Copyright (c) 2014 Navus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IASpeakerInfoViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate, UIScrollViewDelegate>

@property (nonatomic, retain) Speaker *speaker;
@property (nonatomic, retain) IBOutlet UILabel *lblSpakerFullName;
@property (retain, nonatomic) IBOutlet UIImageView *speakerImgView;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end
