//
//  IASpeakerInfoViewController.m
//  ERA-EDTA2015
//
//  Created by admin on 7/3/14.
//  Copyright (c) 2014 Navus. All rights reserved.
//

#import "IASpeakerInfoViewController.h"

#import "UIImageView+AFNetworking.h"
#import "PresentationTableViewCell.h"
#import "PresentationDetailsViewController.h"

@interface IASpeakerInfoViewController () <PresentationTableViewCellDelegate>

@property (strong, nonatomic) NSArray* arrayTalks;

@property (weak, nonatomic) IBOutlet UILabel *lblPresentationsTitle;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintImageTopSpaceToSuperview;
@property (weak, nonatomic) IBOutlet UILabel *lblPanGescure;
@property (weak, nonatomic) IBOutlet UIImageView *imgBackground;
@property (weak, nonatomic) IBOutlet UIButton *btnSpeakerBiography;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintSpeakerBioHeight;

@end

@implementation IASpeakerInfoViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 300.0;
  
    UIImage* img = [UIImage imageNamed:@"speaker-placeholder"];

    if(self.speaker.spkPicture)
    {
        NSString* request = [self.speaker.spkPicture stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:request];
        [self.speakerImgView setImageWithURL:url  placeholderImage:img];
    }
    
    _speakerImgView.layer.cornerRadius = _speakerImgView.frame.size.width / 2;
    _speakerImgView.layer.borderWidth = 2;
    _speakerImgView.layer.borderColor = [UIColor whiteColor].CGColor;
    _speakerImgView.clipsToBounds = YES;
    
//    _lblPresentationsTitle.layer.borderColor = [UIColor colorWithHex:kGlobalColor andAlpha:1.0].CGColor;
//    _lblPresentationsTitle.layer.borderWidth = 1;
    
    //Set speaker name
    self.lblSpakerFullName.text = [NSString stringWithFormat:@"%@ %@", self.speaker.spkLastName ? self.speaker.spkLastName: @"", self.speaker.spkFirstName ? self.speaker.spkFirstName: @""];
    

    self.navigationItem.rightBarButtonItem.tintColor = [UIColor blackColor];
    
    // register presentation cell
    UINib *nib = [UINib nibWithNibName:NSStringFromClass([PresentationTableViewCell class]) bundle:nil];
    [[self tableView] registerNib:nib forCellReuseIdentifier:NSStringFromClass([PresentationTableViewCell class])];
    
    if (_speaker.spkBiography.length > 0)
    {
        _constraintSpeakerBioHeight.constant = 50;
        _btnSpeakerBiography.hidden = NO;
    }
    else
    {
        _constraintSpeakerBioHeight.constant = 0;
        _btnSpeakerBiography.hidden = YES;
    }
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.arrayTalks = [self.speaker.talks allObjects];
    self.arrayTalks = [self.arrayTalks sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"tlkStartTime" ascending:YES],
                                                                     [[NSSortDescriptor alloc] initWithKey:@"tlkTitle" ascending:YES]]];
    [[self tableView] reloadData];
}

#pragma mark - TableView Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section
{
    return [self.arrayTalks count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{    
    PresentationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PresentationTableViewCell class]) forIndexPath:indexPath];
    
    Talk *talk = [self.arrayTalks objectAtIndex:indexPath.row];
    cell.lblTalkTitle.text = [NSString stringWithFormat:@"%@",talk.tlkTitle];
    
    if (talk.tlkAvailable.integerValue == 1)
    {
        cell.imgMaterialAvailabilityIcon.image = [UIImage imageNamed:kEmaterialsAvailable];
        [cell.imgMaterialAvailabilityIcon colorItWithColor:[UIColor colorWithHex:kGlobalColor andAlpha:1.0]];
    }
    else
    {
        cell.imgMaterialAvailabilityIcon.image = [UIImage imageNamed:kEmaterialsUnavailable];
        [cell.imgMaterialAvailabilityIcon colorItWithColor:[UIColor lightGrayColor]];
    }
    
    cell.imgQaIcon.image = [UIImage imageNamed:[talk.tlkVotingEnabled boolValue] ? kVotingAvailable : kVotingUnavailable];

    cell.lblSpeakerFullName.text = talk.tlkSpeakerName;
    
    cell.tag = indexPath.row;
    
    if ([talk.tlkType isEqualToString:kKeyPoster])
    {
        cell.imgMaterialAvailabilityIcon.hidden = YES;
        [cell setBoardNo:talk.tlkBoardNo];
    }
    else
    {
        cell.imgMaterialAvailabilityIcon.hidden = NO;
        [cell setBoardNo:nil];
    }
    
    if (talk.zetBookmarked.boolValue == YES)
    {
        [cell.btnAddToTimeline setImage:[UIImage imageNamed:kTimelineRemove] forState:UIControlStateNormal];
    }
    else
    {
        [cell.btnAddToTimeline setImage:[UIImage imageNamed:kTimelineAdd] forState:UIControlStateNormal];
    }
    
    [cell setTime:talk];
    
    cell.delegate = self;
    cell.tag = indexPath.row;
    
    // because on iOS 8 doesn't work
    // self.tableView.rowHeight = UITableViewAutomaticDimension;
    // self.tableView.estimatedRowHeight = 200.0;
    [cell updateConstraintsIfNeeded];
    
    return cell;
}

-(void)presentationTableViewCell:(PresentationTableViewCell *)tableCell didTimelinePressed:(UIButton *)sender
{
    Talk* talk = [self.arrayTalks objectAtIndex:sender.tag];
    
    if (talk.zetBookmarked.boolValue == YES)
    {
        [Talk updateTimeline:talk isAddition:NO];
    }
    else
    {
        [Talk updateTimeline:talk isAddition:YES];
    }
    
    NSError *error;
    
    [[LLDataAccessLayer sharedInstance].managedObjectContext save:&error];
}

// because on iOS 8 doesn't work
// self.tableView.rowHeight = UITableViewAutomaticDimension;
// self.tableView.estimatedRowHeight = 200.0;
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

#pragma mark - TableView Delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Get reference to the destination view controller
    NSString *strID = NSStringFromClass([PresentationDetailsViewController class]);
    PresentationDetailsViewController *vc = [[UIStoryboard storyboardWithName:strID bundle:nil] instantiateViewControllerWithIdentifier:strID];
    
    vc.currentTalkIndex = (int)indexPath.row;
    vc.arrayTalks = [NSMutableArray arrayWithArray:self.arrayTalks];
    
    [self.navigationController pushViewController:vc animated:YES];
}



#pragma - IBActions

- (IBAction)panGestureRec:(UIPanGestureRecognizer *)sender
{
    CGPoint velocity = [sender velocityInView:sender.view];
    
    if(velocity.y > 0)
    {
        NSLog(@"gesture moving Up");
        
        [self.view layoutIfNeeded];
        
        [UIView animateWithDuration:.4 animations:^{
            _constraintImageTopSpaceToSuperview.constant = 0;
            
            [self.view layoutIfNeeded];
        }];
    }
    else
    {
        NSLog(@"gesture moving Bottom");
        
        [self.view layoutIfNeeded];
        
        [UIView animateWithDuration:.4 animations:^{
            _constraintImageTopSpaceToSuperview.constant = -_imgBackground.frame.size.height;;
            
            [self.view layoutIfNeeded];
        }];
    }
}

#pragma mark - Set left margin of table cells to ziro for iOS 8

-(void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - IBActions

- (IBAction)actionShowSpeakersBiography:(id)sender
{
    NSString *strID = NSStringFromClass([HTMLPreviewerViewController class]);
    HTMLPreviewerViewController *vc = [[UIStoryboard storyboardWithName:strID bundle:nil] instantiateViewControllerWithIdentifier:strID];

    vc.htmlContent = _speaker.spkBiography;
    vc.title = @"Speaker's Biograpyhy";
    
    [self.navigationController pushViewController:vc animated:YES];
}


@end
