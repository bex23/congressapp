//
//  IASpeakerTableViewCell.h
//  ERA-EDTA2015
//
//  Created by admin on 7/3/14.
//  Copyright (c) 2014 Navus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IASpeakerTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *speakerFullName;

@property (weak, nonatomic) IBOutlet UILabel *lblPresentationsText;
@property (strong, nonatomic) IBOutlet UILabel *txtPresentationsCount;

@property (strong, nonatomic) IBOutlet UIImageView *imgEmatAvailability;

@property (weak, nonatomic) IBOutlet UIImageView *imgSpeaker;

-(void)setNumberOfPresentations:(NSString *)number;

@end
