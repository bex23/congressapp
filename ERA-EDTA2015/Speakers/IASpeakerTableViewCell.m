//
//  IASpeakerTableViewCell.m
//  ERA-EDTA2015
//
//  Created by admin on 7/3/14.
//  Copyright (c) 2014 Navus. All rights reserved.
//

#import "IASpeakerTableViewCell.h"

@implementation IASpeakerTableViewCell

-(void)setImgSpeaker:(UIImageView *)newImg
{
    UIImageView *newImageSpeaker = newImg;
    
    newImageSpeaker.layer.cornerRadius = newImageSpeaker.frame.size.width / 2;
    newImageSpeaker.clipsToBounds = YES;
    
    _imgSpeaker = newImageSpeaker;
}

-(void)setNumberOfPresentations:(NSString *)number
{
    self.txtPresentationsCount.text = number;
    self.lblPresentationsText.text = (number.integerValue == 1) ? @"Presentation" : @"Presentations";
}

@end
