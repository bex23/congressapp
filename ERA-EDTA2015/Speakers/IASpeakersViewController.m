//
//  IASpeakersTableViewController.m
//  ERA-EDTA2015
//
//  Created by admin on 7/3/14.
//  Copyright (c) 2014 Navus. All rights reserved.
//

#import "IASpeakersViewController.h"

#import "IASpeakerTableViewCell.h"
#import "IASpeakerInfoViewController.h"


@interface IASpeakersViewController ()

@property (strong,nonatomic) NSFetchedResultsController* fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext* managedObjectContext;
@property (strong, nonatomic) Speaker* selectedSpeaker;
@property (strong, nonatomic) UISearchController *searchController;
@property (weak, nonatomic) IBOutlet UIImageView *imgTopBanner;

@end

@implementation IASpeakersViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 300.0;
    
    // Setting SearchController
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    [self.searchController.searchBar sizeToFit];
    self.definesPresentationContext = YES;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.tableView.tableHeaderView = self.searchController.searchBar;
    
    self.managedObjectContext = [[LLDataAccessLayer sharedInstance] managedObjectContext];
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    NSError *error;
    if (![self.fetchedResultsController performFetch:&error])
    {
        NSLog(@"error %@ %@", error, error.userInfo);
        abort();
    }
    
    [self.tableView reloadData];
    
    self.selectedSpeaker = nil;
    self.filtered = [NSMutableArray new];
    


    [[BannersServices shared] addWithImageView:self.imgTopBanner
                                            at:BannerPositionTopBanner
                                   sender: self];
}



#pragma mark FRC

-(NSFetchedResultsController *)fetchedResultsController
{
    if (!_fetchedResultsController) {
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"Speaker" inManagedObjectContext:self.managedObjectContext];
        
        [fetchRequest setEntity:entity];
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"spkLastName" ascending:YES selector:@selector(caseInsensitiveCompare:)];
        NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
        
        [fetchRequest setSortDescriptors:sortDescriptors];
        
        NSMutableArray *predicates = [[NSMutableArray alloc] initWithCapacity:1];
        [predicates addObject:[NSPredicate predicateWithFormat:@"talks.@count > 0"]];
        fetchRequest.predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
        
        NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
        
        aFetchedResultsController.delegate = self;
        
        self.fetchedResultsController = aFetchedResultsController;
    }
    return _fetchedResultsController;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type)
    {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeMove:
            
            break;
        case NSFetchedResultsChangeUpdate:
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeUpdate:
            //[self configureCell:[self.tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    NSInteger count = [[_fetchedResultsController sections] count];
    
    if (count == 0 || self.searchController.active)
    {
        count = 1;
    }
    return count;
}

- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section{
    int rows = 0;
    
    if (self.searchController.active)
    {
        rows = (int)[self.filtered count];
    }
    else
    {
        if ([[_fetchedResultsController sections] count] > 0) {
            id <NSFetchedResultsSectionInfo> sectionInfo = [[_fetchedResultsController sections] objectAtIndex:section];
            rows = (int)[sectionInfo numberOfObjects];
        }
    }
    
    return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    IASpeakerTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"speakerCell" forIndexPath:indexPath];
    
    Speaker *speaker;
    
    if (self.searchController.active)
    {
        speaker = [self.filtered objectAtIndex:indexPath.row];
    }
    else
    {
        speaker = [self.fetchedResultsController objectAtIndexPath:indexPath];
    }
    
    cell.speakerFullName.text = [NSString stringWithFormat:@"%@ %@", speaker.spkLastName ? speaker.spkLastName: @"", speaker.spkFirstName ? speaker.spkFirstName: @""];
    
    //count presentations for speaker id
    NSFetchRequest * allDataRequest = [[NSFetchRequest alloc] init];
    [allDataRequest setEntity:[NSEntityDescription entityForName:@"Talk" inManagedObjectContext:[LLDataAccessLayer sharedInstance].managedObjectContext]];
    
    NSArray* talks = [speaker.talks allObjects];
    
    [cell setNumberOfPresentations:[NSString stringWithFormat:@"%lu", (unsigned long)talks.count]];
    
    NSArray *arrayAvailableEmaterials = [talks filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"tlkAvailable == 1"]];
    
    if(arrayAvailableEmaterials.count > 0)
    {
        cell.imgEmatAvailability.image = [UIImage imageNamed:kEmaterialsAvailable];
        [cell.imgEmatAvailability colorItWithColor:[UIColor colorWithHex:kGlobalColor andAlpha:1.0]];
    }
    else
    {
        cell.imgEmatAvailability.image = [UIImage imageNamed:kEmaterialsUnavailable];
        [cell.imgEmatAvailability colorItWithColor:[UIColor lightGrayColor]];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    
    UIImage* img = [UIImage imageNamed:@"speaker-placeholder"];
    
    if(speaker.spkPicture)
    {
        NSString* request = [speaker.spkPicture stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:request];
        [cell.imgSpeaker setImageWithURL:url  placeholderImage:img];
    }
    else
    {
        [cell.imgSpeaker setImage:img];
    }
    
    // because on iOS 8 doesn't work
    // self.tableView.rowHeight = UITableViewAutomaticDimension;
    // self.tableView.estimatedRowHeight = 200.0;
    [cell updateConstraintsIfNeeded];
    
    return cell;
}

-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    if (self.searchController.active)
    {
        return nil;
    }
    else
    {
        NSArray* abc = [NSArray arrayWithObjects:UITableViewIndexSearch,@"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H", @"I", @"J", @"K", @"L", @"M", @"N", @"O", @"P", @"Q", @"R", @"S", @"T", @"U", @"V", @"W", @"X", @"Y", @"Z", nil];
        
        return abc;
    }
}

-(NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    if (self.searchController.active)
    {
        return 0;
    }
    else
    {
        if (title == UITableViewIndexSearch)
        {
            [tableView scrollRectToVisible:CGRectMake(0, 0, 320, 55) animated:YES];
            //            [tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:-1 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
            return -1;
        }
        else
        {
            for (int x = 0; x<_fetchedResultsController.fetchedObjects.count; x++) {
                
                Speaker *spk = [_fetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:x inSection:0]];
                
                if ([spk.spkLastName hasPrefix:title])
                {
                    [tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:x inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
                    return x;
                }
            }
        }
    }
    return 2;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    Speaker *speaker;
    if (self.searchController.active)
    {
        if (self.filtered.count == 0) {
            speaker = [self.filtered objectAtIndex:indexPath.row];
        }
    }
    else
    {
        speaker = [self.fetchedResultsController objectAtIndexPath:indexPath];
    }
    
    self.selectedSpeaker = speaker;
    
    [self performSegueWithIdentifier:@"speakerInformationSegue" sender:self];
}

// because on iOS 8 doesn't work
// self.tableView.rowHeight = UITableViewAutomaticDimension;
// self.tableView.estimatedRowHeight = 200.0;
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

#pragma mark - UISearchResultUpdating delegate

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    NSString *searchString = searchController.searchBar.text;
    [self filterContentForSearchText:searchString
                               scope:[[searchController.searchBar scopeButtonTitles]
                                      objectAtIndex:[searchController.searchBar selectedScopeButtonIndex]]];
    
    [self.tableView reloadData];
}

#pragma mark Content Filtering

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    [self.filtered removeAllObjects]; // First clear the filtered array.
    
    if (searchText.length)
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"spkLastName CONTAINS[cd] %@ OR spkFirstName CONTAINS[cd] %@", searchText, searchText];
        [self.filtered addObjectsFromArray:[self.fetchedResultsController.fetchedObjects filteredArrayUsingPredicate:predicate]];
    }
    
    //	for (id bandOrConcert in self.fetchedResultsController.fetchedObjects)
    //    {
    //        NSString *comparator = nil;
    //        if ([bandOrConcert isMemberOfClass:[Speaker class]])
    //        {
    //            comparator = [(Speaker *)bandOrConcert spkLastName];
    //        }
    //        NSComparisonResult result = [comparator compare:searchText options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)
    //                                                  range:[comparator rangeOfString:searchText options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)]];
    //
    //        if (result == NSOrderedSame)
    //        {
    //            [self.filtered addObject:bandOrConcert];
    //        }
    //	}
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if([identifier isEqualToString: @"speakerInformationSegue"])
    {
        return NO;
    }
    
    return YES;
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([[segue identifier] isEqualToString:@"speakerInformationSegue"])
    {
        IASpeakerInfoViewController* vc = [segue destinationViewController];
        [vc setSpeaker:self.selectedSpeaker];
    }
}

#pragma mark - Set left margin of table cells to ziro for iOS 8

-(void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

@end
