//
//  IASpeakersTableViewController.h
//  ERA-EDTA2015
//
//  Created by admin on 7/3/14.
//  Copyright (c) 2014 Navus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IASpeakersViewController : UIViewController <NSFetchedResultsControllerDelegate, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating>

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, retain) NSMutableArray *filtered;

@end
