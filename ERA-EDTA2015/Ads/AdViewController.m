//
//  AdViewController.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 3/1/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "AdViewController.h"

@interface AdViewController ()

@end

@implementation AdViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)actionClose:(id)sender
{
    [self.delegate AdCancel];
}

- (IBAction)actionOpenSite:(id)sender
{
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@"Info"
                                message:[[RealmRepository shared] leavingAppMessage]
                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* btnClose = [UIAlertAction
                               actionWithTitle:@"Close"
                               style:UIAlertActionStyleDefault
                               handler:nil];
    
    UIAlertAction* btnContinue = [UIAlertAction
                                  actionWithTitle:@"Continue"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action)
                                  {
                                      [self.delegate AdDidClickOpenAdSite];
                                  }];
    [alert addAction:btnClose];
    [alert addAction:btnContinue];
    [self presentViewController:alert animated:YES completion:nil];
}

@end
