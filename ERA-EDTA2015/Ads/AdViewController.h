//
//  AdViewController.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 3/1/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AdDelegate <NSObject>

-(void)AdCancel;
-(void)AdDidClickOpenAdSite;

@end

@interface AdViewController : UIViewController

@property (nonatomic, assign) id <AdDelegate> delegate;

@end
