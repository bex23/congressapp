//
//  IAAppDelegate.m
//  ERA-EDTA2015
//
//  Created by admin on 6/24/14.
//  Copyright (c) 2014 Navus. All rights reserved.
//

#import "AppDelegate.h"
#import "CommentViewController.h"
#import "HomeViewController.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <Realm/Realm.h>

#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
@import UserNotifications;
#endif

@import Firebase;
@import FirebaseInstanceID;
@import FirebaseMessaging;

// Implement UNUserNotificationCenterDelegate to receive display notification via APNS for devices
// running iOS 10 and above. Implement FIRMessagingDelegate to receive data message via FCM for
// devices running iOS 10 and above.
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
@interface AppDelegate () <UNUserNotificationCenterDelegate, FIRMessagingDelegate, UIAlertViewDelegate>
#else
@interface IAAppDelegate () <UIAlertViewDelegate, FIRMessagingDelegate>
#endif

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    RLMRealmConfiguration *config = [RLMRealmConfiguration defaultConfiguration];
    config.schemaVersion = 5;
    config.migrationBlock = ^(RLMMigration *migration, uint64_t oldSchemaVersion) {
        // We haven’t migrated anything yet, so oldSchemaVersion == 0
        if (oldSchemaVersion < 5) {
            NSLog(@"Migrated to v5");
        }
    };
    [RLMRealmConfiguration setDefaultConfiguration:config];
    [RLMRealm defaultRealm];
    //force t&c for this version
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"force"]) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kTermsAccepted];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"force"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }

    [Fabric with:@[[Crashlytics class], [Answers class]]];
    
    [self setupApiCommunication];
        
    [self setupNotificationsForApplication:application];
    
    //set rootview controller
    if ([[NSUserDefaults standardUserDefaults] stringForKey:@"selectedConferenceId"]) {
        [[Application shared] configureMainInterfaceIn:self.window];
    } else {
        ConferenceSelectionViewController *selectionVc = [[UIStoryboard storyboardWithName:@"New" bundle:nil] instantiateViewControllerWithIdentifier:@"ConferenceSelectionViewController"];
        selectionVc.viewModel = [ConferenceSelectionViewModel new];
        self.window.rootViewController = selectionVc;
    }
        
    [self.window makeKeyAndVisible];
    
    return YES;
}

#pragma mark - Setups

-(void)setupApiCommunication
{
    [EWebServiceClient setBaseUrl:kBaseAPIURL];
    [EWebServiceClient sharedInstance];
    if ([[NSUserDefaults standardUserDefaults] valueForKey:kUserToken])
    {
        [[ESyncData sharedInstance] setAuthorizationHeaderWithToken:[[NSUserDefaults standardUserDefaults] valueForKey:kUserToken]];
    }
    // set Api key
    [[ESyncData sharedInstance] setApiKeyInHeader:kApiKey];
    [[ESyncData sharedInstance] setHeaderWithValue: [Utils getDevicUUID] forKey:@"device_id"];
}

-(void)setupNotificationsForApplication:(UIApplication *)application
{
    // Register for remote notifications. This shows a permission dialog on first run, to
    // show the dialog at a more appropriate time move this registration accordingly.
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_7_1) {
        // iOS 7.1 or earlier. Disable the deprecation warnings.
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        UIRemoteNotificationType allNotificationTypes =
        (UIRemoteNotificationTypeSound |
         UIRemoteNotificationTypeAlert |
         UIRemoteNotificationTypeBadge);
        [application registerForRemoteNotificationTypes:allNotificationTypes];
#pragma clang diagnostic pop
    } else {
        // iOS 8 or later
        // [START register_for_notifications]
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
            UIUserNotificationType allNotificationTypes =
            (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
            UIUserNotificationSettings *settings =
            [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
            [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        } else {
            // iOS 10 or later
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
            // For iOS 10 display notification (sent via APNS)
            [UNUserNotificationCenter currentNotificationCenter].delegate = self;
            UNAuthorizationOptions authOptions =
            UNAuthorizationOptionAlert
            | UNAuthorizationOptionSound
            | UNAuthorizationOptionBadge;
            [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error) {
            }];
            
            // For iOS 10 data message (sent via FCM)
            [FIRMessaging messaging].delegate = self;
#endif
        }
        
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        // [END register_for_notifications]
    }
    
    // [START configure_firebase]
    [FIRApp configure];
    // [END configure_firebase]
    // [START add_token_refresh_observer]
    // Add observer for InstanceID token refresh callback.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenRefreshNotification:)
                                                 name:kFIRInstanceIDTokenRefreshNotification object:nil];
    // [END add_token_refresh_observer]
}

// [START connect_on_active]
- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [self connectToFcm];
    
    // CHECK IF OS VERSION AND APP VERSION CHANGED //////////// -> SEND IT TO SERVER
    NSString *osVersion = [[UIDevice currentDevice] systemVersion];
    NSString *appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    
    if (![[[NSUserDefaults standardUserDefaults] valueForKey:kKeyOSVersion] isEqualToString:osVersion] || ![[[NSUserDefaults standardUserDefaults] valueForKey:kKeyAppVersion] isEqualToString:appVersion])
    {
        [[NSUserDefaults standardUserDefaults] setValue:osVersion forKey:kKeyOSVersion];
        [[NSUserDefaults standardUserDefaults] setValue:appVersion forKey:kKeyAppVersion];

        [[ESyncData sharedInstance] registerDevice];
    }
    ////////////////////////////////////////////////////////////
    
    // if user was seeing materials -> make session active again
    NSMutableDictionary *dictSessin = [NSMutableDictionary dictionaryWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:kKeyActiveMaterialBrowsingSession]];
    if (dictSessin && [[dictSessin objectForKey:@"active"] isEqualToString:@"NO"])
    {
        [dictSessin setObject:@"YES" forKey:@"active"];
        [[NSUserDefaults standardUserDefaults] setObject:dictSessin forKey:kKeyActiveMaterialBrowsingSession];
        [MaterialBrowsing updateLastRecordTimeForMaterialBrowsingForSession:[dictSessin objectForKey:@"sessionId"]];
    }
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    // check are there unreceived notifications
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
    if ([[UIApplication sharedApplication] currentUserNotificationSettings].types)
    {
        [[UNUserNotificationCenter currentNotificationCenter] getDeliveredNotificationsWithCompletionHandler:^(NSArray<UNNotification *> * _Nonnull notifications) {
            
            if (notifications.count > 0)
            {
                NSArray *ntf = notifications;
//                [self handleDeleveredNotifications:ntf];
                [[UNUserNotificationCenter currentNotificationCenter] removeAllDeliveredNotifications];
            }
        }];
    }
#endif
    
    if([[Utils topViewController] isKindOfClass:[HomeViewController class]])
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:kUIUserNotificationTypeChanged object:self userInfo:nil];

    }
    
}
// [END connect_on_active]

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

// Maja: landscapeON - use for ProgrammeAtAGlance
- (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    if (self.landscapeON == YES)
    {
        return UIInterfaceOrientationMaskAll;
    }
    else
    {
        return UIInterfaceOrientationMaskPortrait;
    }
}

// [START receive_message]
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification
    
    // Print message ID.
    if (userInfo[kGCMMessageIDKey]) {
        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }
    
    [self manageNotificationForUserInfo:userInfo];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification
    
    // Print message ID.
    if (userInfo[kGCMMessageIDKey]) {
        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }
    
    [self manageNotificationForUserInfo:userInfo];
    
    completionHandler(UIBackgroundFetchResultNewData);
}
// [END receive_message]

// [START ios_10_message_handling]
// Receive displayed notifications for iOS 10 devices.
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
// Handle incoming notification messages while app is in the foreground.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
    // Print message ID.
    NSDictionary *userInfo = notification.request.content.userInfo;
//    if (userInfo[kGCMMessageIDKey]) {
//        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
//    }
    
    [self manageNotificationForUserInfo:userInfo];
    
    // Change this to your preferred presentation option
    completionHandler(UNNotificationPresentationOptionNone);
}

// Handle notification messages after display notification is tapped by the user.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void (^)(void))completionHandler {
    NSDictionary *userInfo = response.notification.request.content.userInfo;
    
    [self userTapOnNotification:userInfo];
    
    completionHandler();
}

-(void)userTapOnNotification:(NSDictionary *)userInfo
{
    [ConferenceNotification insert:userInfo];
    if(![[Utils topViewController] isKindOfClass:[NotificationCenterController class]])
    {
        [[[Application shared] navigator] toNotifications];
    }
}
#endif
// [END ios_10_message_handling]

// [START ios_10_data_message_handling]
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
// Receive data message on iOS 10 devices while app is in the foreground.
- (void)applicationReceivedRemoteMessage:(FIRMessagingRemoteMessage *)remoteMessage {
    // Print full message
    NSLog(@"%@", remoteMessage.appData);
    
    [self manageNotificationForUserInfo:remoteMessage.appData];
}
#endif
// [END ios_10_data_message_handling]

// [START refresh_token]
- (void)tokenRefreshNotification:(NSNotification *)notification {
    // Note that this callback will be fired everytime a new token is generated, including the first
    // time. So if you need to retrieve the token as soon as it is available this is where that
    // should be done.
    NSString *refreshedToken = [[FIRInstanceID instanceID] token];
    NSLog(@"InstanceID token: %@", refreshedToken);
    
    // Connect to FCM since connection may have failed when attempted before having a token.
    [self connectToFcm];
    
    if (refreshedToken)
    {
        NSString *previousFirebaseToken = [[NSUserDefaults standardUserDefaults] valueForKey:@"firebaseToken"];
        if (!previousFirebaseToken || ![previousFirebaseToken isEqualToString:refreshedToken])
        {
            [[NSUserDefaults standardUserDefaults] setValue:refreshedToken forKey:@"firebaseToken"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[ESyncData sharedInstance] registerDevice];
        }
    }
}
// [END refresh_token]

// [START connect_to_fcm]
- (void)connectToFcm
{
    // Won't connect since there is no token
    if (![[FIRInstanceID instanceID] token]) {
        return;
    }
    
    // Disconnect previous FCM connection if it exists.
    [[FIRMessaging messaging] setShouldEstablishDirectChannel:NO];
    [[FIRMessaging messaging] setShouldEstablishDirectChannel:YES];
}
// [END connect_to_fcm]

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"Unable to register for remote notifications: %@", error);
}

// This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
// If swizzling is disabled then this function must be implemented so that the APNs token can be paired to
// the InstanceID token.
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSLog(@"APNs token retrieved: %@", deviceToken);
    
    // With swizzling disabled you must set the APNs token here.
    [[FIRMessaging messaging] setAPNSToken:deviceToken];
    
    NSString * deviceTokenString = [[[[deviceToken description]
                                      stringByReplacingOccurrencesOfString: @"<" withString: @""]
                                     stringByReplacingOccurrencesOfString: @">" withString: @""]
                                    stringByReplacingOccurrencesOfString: @" " withString: @""];
    
    NSLog(@"The generated device token string is : %@",deviceTokenString);
    
    NSString *previousApnsToken = [[NSUserDefaults standardUserDefaults] valueForKey:kKeyAPNSToken];
    if (![previousApnsToken isEqualToString:deviceTokenString])
    {
        [[NSUserDefaults standardUserDefaults] setValue:deviceTokenString forKey:kKeyAPNSToken];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[ESyncData sharedInstance] registerDevice];
    }
}

// [START disconnect_from_fcm]
- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [[FIRMessaging messaging] setShouldEstablishDirectChannel:NO];
    NSLog(@"Disconnected from FCM");
    
    // if user was seeing materials -> app did enter background -> make session unactive
    NSMutableDictionary *dictSessin = [NSMutableDictionary dictionaryWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:kKeyActiveMaterialBrowsingSession]];
    if (dictSessin && [[dictSessin objectForKey:@"active"] isEqualToString:@"YES"])
    {
        [dictSessin setObject:@"NO" forKey:@"active"];
        [[NSUserDefaults standardUserDefaults] setObject:dictSessin forKey:kKeyActiveMaterialBrowsingSession];
        [MaterialBrowsing recordMaterialBrowsingForSession:[dictSessin objectForKey:@"sessionId"]];
    }
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}
// [END disconnect_from_fcm]

-(void)manageNotificationForUserInfo:(NSDictionary *)userInfo
{
    // Print full message.
    NSLog(@"%@", userInfo);
    
    // if action for update - dont show any message, just update program
    if ([userInfo objectForKey:@"action"] && [[userInfo objectForKey:@"action"] isEqualToString:@"FORCE_UPDATE"])
    {
        [[RealmRepository shared] setLastSyncWithDate:nil];
        if ([[[RealmRepository shared] getSelectedConferenceId] length]) {
            [[ESyncData sharedInstance] downloadZipFile];
        }
        return;
    }
    
    // if notifications disabled - dont show any message
    if (![[UIApplication sharedApplication] currentUserNotificationSettings].types)
    {
        return;
    }
    
    if ([userInfo objectForKey:@"message"])
    {
        [ConferenceNotification insert:userInfo];
        if(![[Utils topViewController] isKindOfClass:[NotificationCenterController class]]) {
            UITextView *textView = [[UITextView alloc] init];
            
            [textView setHTMLFromString:[userInfo objectForKey:@"message"]];
            textView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            textView.editable = NO;
            
            UIViewController *controller = [[UIViewController alloc] init];
            
            textView.frame = controller.view.frame;
            [controller.view addSubview:textView];
            
            UIAlertController *alert = [UIAlertController
                                        alertControllerWithTitle:nil
                                        message:nil
                                        preferredStyle:UIAlertControllerStyleAlert];
            
            [alert setValue:controller forKey:@"contentViewController"];

            UIAlertAction* btnOK = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * _Nonnull action) {
                                        [ConferenceNotification read:userInfo[@"message_id"]];
                                    }];

            [alert addAction:btnOK];

            NSLayoutConstraint *c = [NSLayoutConstraint constraintWithItem:alert.view attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:textView.contentSize.height + 100];
            [alert.view addConstraint:c];

            [self.window.rootViewController presentViewController:alert animated:YES completion:nil];
        }
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:kGCMMessageIDKey
                                                             object:nil
                                                           userInfo:userInfo];
}


-(void)handleDeleveredNotifications:(NSArray<UNNotification *> *)notifications
{
    for (UNNotification *notification in notifications)
    {
        if ([notification.request.identifier containsString:@"_start"] || [notification.request.identifier containsString:@"_end"]) {
            return;
        }
        NSDictionary *userInfo = notification.request.content.userInfo;
        
        // Print full message.
        NSLog(@"%@", userInfo);
        
        if (userInfo)
        {
            // if action for update - dont show any message, just update program
            if ([userInfo objectForKey:@"action"] && [[userInfo objectForKey:@"action"] isEqualToString:@"FORCE_UPDATE"])
            {
                [[RealmRepository shared] setLastSyncWithDate:nil];
                if ([[[RealmRepository shared] getSelectedConferenceId] length]) {
                    [[ESyncData sharedInstance] downloadZipFile];
                }
                continue;
            }
            
//            [PushNotification insertNotification:userInfo];
        }
    }
}

@end
