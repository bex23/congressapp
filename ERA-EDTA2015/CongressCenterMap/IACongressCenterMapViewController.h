//
//  IACongressCenterMapViewController.h
//  ERA-EDTA2015
//
//  Created by Predrag Despotović on 7/20/14.
//  Copyright (c) 2014 Navus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IACongressCenterMapViewController : UIViewController

@property (strong, nonatomic) NSString *roomLevel;
@property (strong, nonatomic) IBOutlet UIView *mapView;
@property (strong, nonatomic) NSString *strCoordX;
@property (strong, nonatomic) NSString *strCoordY;

@end
