//
//  IAWeatherViewController.m
//  ERA-EDTA2015
//
//  Created by admin on 7/28/14.
//  Copyright (c) 2014 Navus. All rights reserved.
//

#import "IAWeatherViewController.h"

#import "UIImageView+AFNetworking.h"

@interface IAWeatherViewController ()

@property (strong, nonatomic) NSArray *days;

@property (weak, nonatomic) IBOutlet UIImageView *imgToday;
@property (weak, nonatomic) IBOutlet UILabel *lblTempToday;
@property (weak, nonatomic) IBOutlet UILabel *lblInfoToday;
@property (weak, nonatomic) IBOutlet UIImageView *imgTomorrow;
@property (weak, nonatomic) IBOutlet UILabel *lblTempTomorrow;
@property (weak, nonatomic) IBOutlet UILabel *lblInfoTomorrow;
@property (weak, nonatomic) IBOutlet UIImageView *imgDayAfter;
@property (weak, nonatomic) IBOutlet UILabel *lblTempDayAfter;
@property (weak, nonatomic) IBOutlet UILabel *lblInfoDayAfter;

@property (weak, nonatomic) IBOutlet UILabel *lblLocation;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityInidcatorView;

@end

@implementation IAWeatherViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIButton *but = [UIButton buttonWithType:UIButtonTypeCustom];
    [but setBackgroundImage:[UIImage imageNamed:@"home"] forState:UIControlStateNormal];
    [but setBackgroundImage:[UIImage imageNamed:@"home_pressed"] forState:UIControlStateHighlighted];
    [but addTarget:self action:@selector(goHome) forControlEvents:UIControlEventTouchUpInside];
    but.bounds = CGRectMake(0, 0, 20, 20);
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:but];
    
    self.lblLocation.text = kWeatherLocation;
    
    [self reloadWeather];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)reloadWeather
{
    [self.activityInidcatorView startAnimating];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://api.worldweatheronline.com/free/v1/weather.ashx?q=%@&format=json&num_of_days=3&key=88un5n737mnxb2fscf52p7zb", kWeatherLocation]]];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response_, NSData *data_, NSError *error_)
    {
        if (error_)
        {
            [[[UIAlertView alloc]initWithTitle:@"Error" message:@"The weather information could not be retrieved. Try later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
        else
        {
//            NSLog(@"The data is %@", [[NSString alloc]initWithData:data_ encoding:NSUTF8StringEncoding]);
            
            if ([[[NSString alloc]initWithData:data_ encoding:NSUTF8StringEncoding]  hasPrefix:@"<h1>"])
            {
                [[[UIAlertView alloc]initWithTitle:@"Error" message:@"The weather information could not be retrieved. Try later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            }
            if (self.days)
            {
                self.days = nil;
            }
            
            self.days = [[[NSJSONSerialization JSONObjectWithData:data_ options:NSJSONReadingAllowFragments error:nil]valueForKey:@"data"]valueForKey:@"weather"];
            
            if (self.days.count > 2)
            {
                self.lblInfoToday.text = [[[[self.days objectAtIndex:0]valueForKey:@"weatherDesc"]lastObject]valueForKey:@"value"];
                self.lblInfoTomorrow.text = [[[[self.days objectAtIndex:1]valueForKey:@"weatherDesc"]lastObject]valueForKey:@"value"];
                self.lblInfoDayAfter.text =[[[[self.days objectAtIndex:2]valueForKey:@"weatherDesc"]lastObject]valueForKey:@"value"];
                
                self.lblTempToday.text = [NSString stringWithFormat:@"%@º",[[self.days objectAtIndex:0]valueForKey:@"tempMaxC"]];
                self.lblTempTomorrow.text = [NSString stringWithFormat:@"%@º",[[self.days objectAtIndex:1]valueForKey:@"tempMaxC"]];
                self.lblTempDayAfter.text = [NSString stringWithFormat:@"%@º",[[self.days objectAtIndex:2]valueForKey:@"tempMaxC"]];
                
                NSLog(@"Image log %@", [NSString stringWithFormat:@"%@Day.png", [[self.days objectAtIndex:0]valueForKey:@"weatherCode"]]);
                
                self.imgToday.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@Day.png", [[self.days objectAtIndex:0]valueForKey:@"weatherCode"]]];
                self.imgTomorrow.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@Day.png", [[self.days objectAtIndex:1]valueForKey:@"weatherCode"]]];
                self.imgDayAfter.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@Day.png", [[self.days objectAtIndex:2]valueForKey:@"weatherCode"]]];
            }
        }
        
        [self.activityInidcatorView stopAnimating];
    }];
}

#pragma mark - Navigation

-(void)goHome
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
