//
//  CUtils.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 5/20/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "CUtils.h"

@implementation CUtils

+ (UIViewController*)topViewController {
    return [self topViewControllerWithRootViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

+ (UIViewController*)topViewControllerWithRootViewController:(UIViewController*)rootViewController
{
    if ([rootViewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController* tabBarController = (UITabBarController*)rootViewController;
        return [self topViewControllerWithRootViewController:tabBarController.selectedViewController];
    } else if ([rootViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController* navigationController = (UINavigationController*)rootViewController;
        return [self topViewControllerWithRootViewController:navigationController.visibleViewController];
    } else if (rootViewController.presentedViewController) {
        UIViewController* presentedViewController = rootViewController.presentedViewController;
        return [self topViewControllerWithRootViewController:presentedViewController];
    } else {
        return rootViewController;
    }
}

+(void)showNotAvailableYetMessage
{
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@"Coming Soon"
                                message:@""
                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* btnCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:btnCancel];
    
    [[self topViewController] presentViewController:alert animated:YES completion:nil];
}

+(void)showOfflineMessage
{
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@"No network connection"
                                message:@"Please check your internet connection.\nYou need to be online to use this feature."
                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* btnCancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:btnCancel];
    
    [[self topViewController] presentViewController:alert animated:YES completion:nil];
}

+(void)showLoginQuestion
{
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@""
                                message:@"You can use this content of the app only if you are logged in. Do you want to login?"
                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* btnRegister = [UIAlertAction actionWithTitle:@"LOGIN"
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action) {
                                                            UINavigationController *nav = [[UIStoryboard storyboardWithName:@"LoginRegistration" bundle:nil] instantiateViewControllerWithIdentifier:@"navLogin"];
                                                            [[self topViewController].navigationController presentViewController:nav animated:YES completion:nil];
                                                        }];
    
    UIAlertAction* btnCancel = [UIAlertAction actionWithTitle:@"LATER" style:UIAlertActionStyleDefault handler:nil];
    
    [alert addAction:btnCancel];
    [alert addAction:btnRegister];
    
    [[self topViewController] presentViewController:alert animated:YES completion:nil];
}

+(void)showRequiredBadgeMessage
{
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:nil
                                message:@"To gain access to E-materials you need to insert the badge number."
                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* btnInsert = [UIAlertAction actionWithTitle:@"Insert"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action) {
                                                          UINavigationController *nav = [[UIStoryboard storyboardWithName:@"EnterBadgeViewController" bundle:nil] instantiateViewControllerWithIdentifier:@"navEnterBadgeViewController"];
                                                          [[self topViewController] presentViewController:nav
                                                                                                 animated:YES
                                                                                               completion:nil];
                                                      }];
    
    UIAlertAction* btnCancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
    
    [alert addAction:btnCancel];
    [alert addAction:btnInsert];
    
    [[self topViewController] presentViewController:alert animated:YES completion:nil];
}


+(UIAlertController *)showMessageAddToTimeline:(BOOL)added
{
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@""
                                message:[NSString stringWithFormat:@"Presentation was %@ to your timeline.", (added == YES) ? @"added" : @"removed"]
                                preferredStyle:UIAlertControllerStyleAlert];
    
    [[self topViewController] presentViewController:alert animated:YES completion:nil];
    
    return alert;
}

+(void)showErrorInDownloadingFileMessage
{
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@"Error"
                                message:@"There is error in downloading the file."
                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* btnClose = [UIAlertAction
                               actionWithTitle:@"Close"
                               style:UIAlertActionStyleDefault
                               handler:nil];
    
    [alert addAction:btnClose];
    [[self topViewController] presentViewController:alert animated:YES completion:nil];
}

+(NSString *)getFilePathForMaterial:(Material *)material
{
    NSString *libraryPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    // /Users/iMaja/Library/Developer/CoreSimulator/Devices/7191433C-6DA5-4DA8-8408-485B1BD3EB7F/data/Containers/Data/Application/3C58C4F2-494F-4BB0-9B58-D14071233E8B/Library/materials/presentation49/material_name.pdf
    NSString *filePath = [NSString stringWithFormat:@"%@/materials/presentation%@/%@.zip", libraryPath, material.matTalkId, material.matMaterialId]; //fileId
    
    return filePath;
}

+(NSString *)getDirPathForMaterial:(Material *)material
{
    NSString *libraryPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    // /Users/iMaja/Library/Developer/CoreSimulator/Devices/7191433C-6DA5-4DA8-8408-485B1BD3EB7F/data/Containers/Data/Application/3C58C4F2-494F-4BB0-9B58-D14071233E8B/Library/materials/presentation49/material_name.pdf
    NSString *filePath = [NSString stringWithFormat:@"%@/materials/presentation%@/material%@/", libraryPath, material.matTalkId, material.matMaterialId]; //fileId
    
    return filePath;
}

+(int)countOfFilesOnPath:(NSString *)path
{
    NSArray *filelist= [[NSFileManager defaultManager] contentsOfDirectoryAtPath:path error:nil];
    
    int filesCount = (int)[filelist count];
    NSLog(@"filesCount:%d", filesCount);
    
    return filesCount;
}

+(void)showLeavingTheAppMessageWithUrl:(NSString *)siteUrl
{
    NSLog(@"%@",siteUrl);
    
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@"Info"
                                message:[[RealmRepository shared] leavingAppMessage]
                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* btnClose = [UIAlertAction
                               actionWithTitle:@"Close"
                               style:UIAlertActionStyleDefault
                               handler:nil];
    
    UIAlertAction* btnContinue = [UIAlertAction
                                  actionWithTitle:@"Continue"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action)
                                  {
                                      NSString *trimmedURL = [siteUrl stringByTrimmingCharactersInSet:
                                                              [NSCharacterSet whitespaceCharacterSet]];
                                      NSString *url;
                                      if (![siteUrl containsString:@"http"])
                                      {
                                          url = [@"http://" stringByAppendingString:trimmedURL];
                                      }
                                      else
                                      {
                                          url = trimmedURL;
                                      }
                                      
                                      NSLog(@"%@", url);
                                      
                                      [self openScheme:url];
                                      
                                      
                                  }];
    [alert addAction:btnClose];
    [alert addAction:btnContinue];
    [[self topViewController] presentViewController:alert animated:YES completion:nil];
}

+ (void)openScheme:(NSString *)scheme {
    UIApplication *application = [UIApplication sharedApplication];
    NSURL *URL = [NSURL URLWithString:scheme];
    
    if ([application respondsToSelector:@selector(openURL:options:completionHandler:)]) {
        [application openURL:URL options:@{}
           completionHandler:^(BOOL success) {
               NSLog(@"Open %@: %d",scheme,success);
           }];
    } else {
        [application openURL:URL options:@{} completionHandler:nil];
        NSLog(@"Open %@: %d",scheme, [application canOpenURL:URL]);
    }
}


+(BOOL)checkIfAllPresentationsFree
{
    BOOL allPresentationsFree = NO;
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone* tzone = [NSTimeZone localTimeZone]; // [NSTimeZone localTimeZone]
    [dateFormatter setTimeZone:tzone];
    dateFormatter.dateFormat = kDateFormatInDatabase;
    NSDate* dateAfterCongress = [dateFormatter dateFromString:@"2017-06-06 16:00:00"];
    
    NSDate *dateNow = [NSDate date];
    
    // if congress finished and user has more then 6 attended sessions on all congress
    if ([dateAfterCongress compare:dateNow] == NSOrderedAscending)
    {
        NSArray *attendedSessions = [Session getAttendedSessions];
        
        if (attendedSessions.count >= 6)
        {
            allPresentationsFree = YES;
        }
    }
    
    return allPresentationsFree;
}

+(BOOL)checkIfSessionToday:(Session *)session
{
    BOOL isSessionToday = NO;
    
    NSDate *dateNow = [NSDate date];
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone* tzone = [NSTimeZone localTimeZone]; // [NSTimeZone localTimeZone]
    [dateFormatter setTimeZone:tzone];
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    NSString *day = [dateFormatter stringFromDate:dateNow]; // 2017-03-02
    
    if ([session.sesStartTime containsString:day])
    {
        isSessionToday = YES;
    }
    
    return isSessionToday;
}

+(BOOL)checkIfCongressEnd
{
    BOOL checkIfCongressEnd = NO;
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone* tzone = [NSTimeZone localTimeZone]; // [NSTimeZone localTimeZone]
    [dateFormatter setTimeZone:tzone];
    dateFormatter.dateFormat = kDateFormatInDatabase;
    NSDate* dateAfterCongress = [dateFormatter dateFromString:@"2017-06-06 16:00:00"];
    
    NSDate *dateNow = [NSDate date];
    
    // if congress finished and user has more then 6 attended sessions on all congress
    if ([dateAfterCongress compare:dateNow] == NSOrderedAscending)
    {
        checkIfCongressEnd = YES;
    }
    
    return checkIfCongressEnd;
}

@end
