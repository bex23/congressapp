//
//  CUtils.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 5/20/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CUtils : NSObject

+(void)showOfflineMessage;

+(UIAlertController *)showMessageAddToTimeline:(BOOL)added;
+(void)showNotAvailableYetMessage;
+(void)showLoginQuestion;
+(void)showRequiredBadgeMessage;
+(void)showLeavingTheAppMessageWithUrl:(NSString *)siteUrl;

+(NSString *)getFilePathForMaterial:(Material *)material;
+(NSString *)getDirPathForMaterial:(Material *)material;
+(int)countOfFilesOnPath:(NSString *)path;
+(void)showErrorInDownloadingFileMessage;

+(BOOL)checkIfAllPresentationsFree;
+(BOOL)checkIfSessionToday:(Session *)session;
+(BOOL)checkIfCongressEnd;

@end
