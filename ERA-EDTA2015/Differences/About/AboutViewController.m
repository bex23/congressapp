//
//  AboutViewController.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 5/30/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "AboutViewController.h"

@interface AboutViewController () <UITextViewDelegate, MFMailComposeViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UITextView *textView;

@end

@implementation AboutViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.textView.text = [NSString stringWithFormat:@"This App has been developed and designed by Navus, a Swiss based digital partner of %@. If you want to benefit from our experience for your own events, contact Navus via email support@e-materials.com or meet them on site at the e-campus in the exhibition hall.", [Application shared].navigator.conference.name];
    
    if (@available(iOS 11.0, *)) {
        self.navigationController.navigationBar.prefersLargeTitles = NO;
    }

    
    self.textView.delegate = self;
    self.textView.dataDetectorTypes = UIDataDetectorTypeLink;
    
    self.title = @"About";
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    [self.textView setContentOffset:CGPointZero animated:NO];
}



#pragma mark - UITextViewDelegate

-(BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange
{
    textView.userInteractionEnabled = NO;
    
    if ([[URL absoluteString] containsString:@"@"]) // it is email address
    {
        NSString *email = [[URL absoluteString] stringByReplacingOccurrencesOfString:@"mailto:" withString:@""];
        NSArray *toRecipents = [NSArray arrayWithObject:email];
        
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        mc.mailComposeDelegate = self;
        
        [mc setToRecipients:toRecipents];
        
        // Determine the file name and extension
        if(mc)
        {
            // Present mail view controller on screen
            [self presentViewController:mc animated:YES completion:NULL];
        }
    }
    
    return NO;
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

@end
