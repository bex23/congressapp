//
//  Constants.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 23.1.16..
//  Copyright © 2016. Navus. All rights reserved.
//

#ifndef Constants_h
#define Constants_h
static NSString *const kApiKey = @"KP5kFNnF2Te1ggyRydxnfJKoVwtfTjlh";
static NSString *const kGlobalColor = @"6ba6ce";
static NSString *const kOtherColor = @"f49d31";

static NSString *const kSponsorName = @"ERA-EDTA";
static BOOL const kNotesExist = YES;
static BOOL const kJoinTheDialogueExist = YES;
static NSString *const kCongressLevels = @"ground_floor,first_floor";

static BOOL const kEnableCongressMapPinning = YES;
static BOOL const kEnableExhibitorsMap = YES;
static BOOL const kEnablePosters = YES;
static BOOL const kEnableSponsorsAndExhibitors = YES;

// RecieveFutureInfo
//static NSString *const kIDsOfCountriesDisabledForRecieveFutureInfo = @"15,76,83,110,218,237";
//static NSString *const kCountriesDisabledForRecieveFutureInfo = @"UK, Ger, It, Fr, AU and CH";

static BOOL const kEnableRemoteNotifications = YES;

static NSString *const kKeyIndustrialSymposium = @"Industrial Symposium";
static NSString *const kKeyPoster = @"Poster";

static NSString *const kTrackUserActionBannerShown = @"SHOWN";
static NSString *const kTrackUserActionBannerClick = @"CLICK";

static NSString *const kTimeFormat = @"HH:mm"; // @"h:mm a";
//On iOS, the user can override the default AM/PM versus 24-hour time setting (via Settings > General > Date & Time > 24-Hour Time), which causes NSDateFormatter to rewrite the format string you set, which can cause your time parsing to fail.
static BOOL const kCheckTimeHourFormatInSettings = YES;
static NSString *const kTimeDefaultFormat = @"HH:mm";
static NSString *const kDateFormat = @"dd.MM.yyyy"; // @"MM/dd/yy";
static NSString *const kDateFormatInDatabase = @"yyyy-MM-dd HH:mm:ss";
static NSString *const kDateDefaultFormat = @"yyyy-MM-dd";
static NSString *const kSegmentedControlDateFormat = @"dd.MM.yyyy"; // @"MM/dd";

#endif /* Constants_h */
