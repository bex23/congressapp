//
//  OpeningHoursViewController.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 2/23/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "OpeningHoursViewController.h"

@interface OpeningHoursViewController ()

@property (weak, nonatomic) IBOutlet UITextView *textView;

@end

@implementation OpeningHoursViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    

}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    [self.textView setContentOffset:CGPointZero animated:NO];
}



@end
