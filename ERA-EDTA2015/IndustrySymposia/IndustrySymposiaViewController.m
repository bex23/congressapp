//
//  IndustrySymposiaViewController.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 2/8/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "IndustrySymposiaViewController.h"

#import "SessionTableViewCell.h"
#import "SessionDetailsViewController.h"
#import "SectionView.h"

@interface IndustrySymposiaViewController () <UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSArray *days;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControlDays;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintSegmentedContorlDaysHeight;

@property (strong, nonatomic) NSDictionary *dictExhibitorsRolesColors;

@property (strong, nonatomic) NSArray *arrayIndustrySymposiaSessions;

@end

static int const kSectionHeight = 55;

@implementation IndustrySymposiaViewController

#pragma mark - View

- (void)viewDidLoad
{
    [super viewDidLoad];
    

    
    self.managedObjectContext = [LLDataAccessLayer sharedInstance].managedObjectContext;
    
    // register session cell
    UINib *nib = [UINib nibWithNibName:NSStringFromClass([SessionTableViewCell class]) bundle:nil];
    [[self tableView] registerNib:nib forCellReuseIdentifier:NSStringFromClass([SessionTableViewCell class])];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 300.0;
    
    self.dictExhibitorsRolesColors = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"exhibitorsRolesSettings" ofType:@"plist"]];
    
    [self setupDays];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.view.userInteractionEnabled = YES;
    
    self.segmentedControlDays.tintAdjustmentMode = UIViewTintAdjustmentModeNormal;
    
    [self doFetch];
}

#pragma mark - Setups

-(void)setupDays
{
    NSMutableArray *array = [NSMutableArray array];
    
    for (Session *session in [Session getSessionsByType:kKeyIndustrialSymposium])
    {
        if (![array containsObject:[Utils getDate:session.sesStartTime]])
        {
            [array addObject:[Utils getDate:session.sesStartTime]];
        }
    }
    
    self.days = [NSArray arrayWithArray:array];
    
    [_segmentedControlDays removeAllSegments];
    _constraintSegmentedContorlDaysHeight.constant = 0;
    
    if (self.days.count > 0)
    {
        _constraintSegmentedContorlDaysHeight.constant = 45;
        
        int i;
        for(i = 0; i < self.days.count; i++)
        {
            NSString *strDate = [Utils formatDateString:[self.days objectAtIndex:i] fromFormat:kDateDefaultFormat toFormat:kSegmentedControlDateFormat];
            [_segmentedControlDays insertSegmentWithTitle:strDate atIndex:i animated:false];
        }
        
        [_segmentedControlDays setSelectedSegmentIndex:0];
    }
    
    [self.segmentedControlDays setTintColor:[UIColor whiteColor]];
}



#pragma mark - FRC

-(NSFetchedResultsController *)fetchedResultsController
{
    if (!_fetchedResultsController)
    {
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"ExhibitorRole" inManagedObjectContext:self.managedObjectContext];
        
        NSMutableArray *predicates = [[NSMutableArray alloc] initWithCapacity:1];
        [predicates addObject:[NSPredicate predicateWithFormat:@"session.sesType == %@", kKeyIndustrialSymposium]];
        
        if(self.days.count > 0 && _segmentedControlDays.selectedSegmentIndex != -1)
        {
            NSString *date = [self.days objectAtIndex:_segmentedControlDays.selectedSegmentIndex];
            [predicates addObject:[NSPredicate predicateWithFormat:@"session.sesStartTime CONTAINS [cd] %@", date]];
        }
        
        fetchRequest.predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
        [fetchRequest setEntity:entity];
        [fetchRequest setResultType:NSDictionaryResultType];
        [fetchRequest setPropertiesToFetch:@[@"erExhibitorId", @"erRoleName", @"erSessionId"]];
        [fetchRequest setReturnsDistinctResults:YES];
        
        NSSortDescriptor *sortDescriptorTime = [[NSSortDescriptor alloc] initWithKey:@"session.sesStartTime"
                                                                           ascending:YES];
        NSSortDescriptor *sortDescriptorName = [[NSSortDescriptor alloc] initWithKey:@"session.sesName"
                                                                           ascending:YES
                                                                            selector:@selector(localizedStandardCompare:)];
        NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptorTime, sortDescriptorName, nil];
        
        [fetchRequest setSortDescriptors:sortDescriptors];
        
        NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:@"erRoleName" cacheName:nil];
        
        self.fetchedResultsController = aFetchedResultsController;
    }
    
    return _fetchedResultsController;
}

-(void)doFetch
{
    self.fetchedResultsController = nil;
    
    self.arrayIndustrySymposiaSessions = @[];
    
    if(self.days.count > 0 && _segmentedControlDays.selectedSegmentIndex != -1)
    {
        NSString *date = [self.days objectAtIndex:_segmentedControlDays.selectedSegmentIndex];
        _arrayIndustrySymposiaSessions = [[Session getSessionsByType:kKeyIndustrialSymposium AndDate:date] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"sponsors.@count == 0"]];
        
        NSSortDescriptor *sortDescriptorTime = [[NSSortDescriptor alloc] initWithKey:@"sesStartTime"
                                                                           ascending:YES];
        NSSortDescriptor *sortDescriptorName = [[NSSortDescriptor alloc] initWithKey:@"sesName"
                                                                           ascending:YES
                                                                            selector:@selector(localizedStandardCompare:)];
        NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptorTime, sortDescriptorName, nil];
        
        
        _arrayIndustrySymposiaSessions = [_arrayIndustrySymposiaSessions sortedArrayUsingDescriptors:sortDescriptors];
    }
    
    NSError *error;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"error %@ %@", error, error.userInfo);
        abort();
    }
    
    [self.tableView reloadData];
}

#pragma mark - TableView Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger count = [[_fetchedResultsController sections] count];
    
    if (self.arrayIndustrySymposiaSessions.count > 0) // -> there are Industrial Symposium sessions those dont have sponsors have sponsors - add one more section
    {
        count = count + 1;
    }
    
    return count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int rows = 0;
    
    if (section == tableView.numberOfSections - 1 && _arrayIndustrySymposiaSessions.count > 0) // -> last section with  Industrial Symposium sessions those dont have sponsors
    {
        rows = (int)_arrayIndustrySymposiaSessions.count;
    }
    else
        if ([[_fetchedResultsController sections] count] > 0)
    {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[_fetchedResultsController sections] objectAtIndex:section];
        rows = (int)[sectionInfo numberOfObjects];
    }
    
    return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SessionTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([SessionTableViewCell class]) forIndexPath:indexPath];
    
    Session *session;
    
    if (indexPath.section == tableView.numberOfSections - 1 && _arrayIndustrySymposiaSessions.count > 0) // -> last section with  Industrial Symposium sessions those dont have sponsors
    {
        session = [_arrayIndustrySymposiaSessions objectAtIndex:indexPath.row];
    }
    else
    {
        NSDictionary *exhR = [self.fetchedResultsController objectAtIndexPath:indexPath];
        session = [Session getSessionWithId:exhR[@"erSessionId"]];
    }
    
    if (session.sesSubtitle.length > 0)
    {
        cell.lblSessionTitle.text = [NSString stringWithFormat:@"%@\n\n%@", session.sesName, session.sesSubtitle];
    }
    else
    {
        cell.lblSessionTitle.text = session.sesName;
    }
    
    [cell setSessionNo:session.sesCode];
    cell.lblRoomName.text = session.venue.venName;
    
    [cell setTime:session];
    
    cell.viewCategoryColor.backgroundColor = [SessionCategory getColorForSessionCategoryId:session.sesCategoryId];
    
    [cell updateConstraintsIfNeeded];
    
    return cell;
}

#pragma mark - TableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    Session *session;
    
    if (indexPath.section == tableView.numberOfSections - 1 && _arrayIndustrySymposiaSessions.count > 0) // -> last section with  Industrial Symposium sessions those dont have sponsors
    {
        session = [_arrayIndustrySymposiaSessions objectAtIndex:indexPath.row];
    }
    else
    {
        NSDictionary *exhR = [self.fetchedResultsController objectAtIndexPath:indexPath];
        session = [Session getSessionWithId:exhR[@"erSessionId"]];
    }
    
    NSString *strID = NSStringFromClass([SessionDetailsViewController class]);
    SessionDetailsViewController* vc = [[UIStoryboard storyboardWithName:strID bundle:nil] instantiateViewControllerWithIdentifier:strID];
    [vc setSession:session];
    
    self.view.userInteractionEnabled = NO;
    
    [self.navigationController pushViewController:vc animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == tableView.numberOfSections - 1 && _arrayIndustrySymposiaSessions.count > 0 && section == 0) // -> last section with  Industrial Symposium sessions those dont have sponsors AND there is no other sesctions (section == 0)
    {
        return 0;
    }
    
    return [UIScreen mainScreen].bounds.size.width * 0.16625;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == tableView.numberOfSections - 1 && _arrayIndustrySymposiaSessions.count > 0) {
        return [SectionHeaderView industySymposiaHeader:@"OTHER INDUSTRY SYMPOSIA"];

    } else {
        SectionHeaderView *shv = [SectionHeaderView industySymposiaHeader:@"FEATURED ON ENP"];
        UIImageView *iv = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, kSectionHeight)];
        iv.image = [UIImage imageNamed:@"industrySymposiaHeader"];
        [shv addSubview:iv];
        [shv sendSubviewToBack:iv];
        return shv;
    }
}

#pragma mark - IBActions

- (IBAction)actionDayChanged:(UISegmentedControl *)sender
{
    [self doFetch];
}

#pragma mark - Set left margin of table cells to ziro for iOS 8

-(void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

@end
