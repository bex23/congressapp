//
//  SectionView.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 5/11/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SectionView : UIView

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgLogo;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintWidthOfImage;

@end
