//
//  PresentationNotesViewController.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 2/21/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "PresentationNotesViewController.h"

#import "NoteTableViewCell.h"
#import "PresentationDetailsViewController.h"

@interface PresentationNotesViewController () <UITableViewDelegate, UITableViewDataSource, UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *viewHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblSpeakerName;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UILabel *lblPresentationName;

@property (weak, nonatomic) IBOutlet UITableView *tblNotes;

@property (nonatomic, strong) NSMutableArray *arrayNotes;

@property (weak, nonatomic) IBOutlet UIView *viewNoteTextView;
@property (weak, nonatomic) IBOutlet UITextView *txtViewNote;

@property (strong, nonatomic) NSString *strPlaceholder;

@property (weak, nonatomic) IBOutlet UIButton *btnAddNotes;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
@property (weak, nonatomic) IBOutlet UIButton *btnCancelFullScreen;

@end

@implementation PresentationNotesViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self loadData];
    

    
    self.strPlaceholder = @"Enter your notes for this presentation";
    
    // add shadow to view
    [self.viewHeader.layer setShadowColor:[UIColor lightGrayColor].CGColor];
    [self.viewHeader.layer setShadowOpacity:0.8];
    [self.viewHeader.layer setShadowRadius:3.0];
    [self.viewHeader.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    // register note cell
    UINib *nib = [UINib nibWithNibName:NSStringFromClass([NoteTableViewCell class]) bundle:nil];
    [self.tblNotes registerNib:nib forCellReuseIdentifier:NSStringFromClass([NoteTableViewCell class])];
    
    self.tblNotes.rowHeight = UITableViewAutomaticDimension;
    self.tblNotes.estimatedRowHeight = 300.0;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // enable view interaction after - it disabled on actionGoToPresentationDetails
    self.view.userInteractionEnabled = YES;
}

-(void)loadData
{
    NSString *strSpeakersName = nil;
    for (Speaker *speaker in self.talk.speakrs)
    {
        if (strSpeakersName)
        {
            strSpeakersName = [NSString stringWithFormat:@"%@/n %@ %@", strSpeakersName, speaker.spkLastName ? speaker.spkLastName: @"", speaker.spkFirstName ? speaker.spkFirstName: @""];
        }
        else
        {
            strSpeakersName = [NSString stringWithFormat:@"%@ %@", speaker.spkLastName ? speaker.spkLastName: @"", speaker.spkFirstName ? speaker.spkFirstName: @""];
        }
    }
    self.lblSpeakerName.text = strSpeakersName;
    
    self.lblDate.text = [Utils formatDateString:[Utils getDate:self.talk.tlkStartTime]];
    
    NSString* startTime = [Utils formatTimeStringFromDate:self.talk.tlkStartTime];
    NSString* endTime = [Utils formatTimeStringFromDate:self.talk.tlkEndTime];
    self.lblTime.text = [NSString stringWithFormat:@"%@ - %@", startTime, endTime];
    
    self.lblPresentationName.text = self.talk.tlkTitle;
    
    [self loadTableData];
}

-(void)loadTableData
{
    self.arrayNotes = [NSMutableArray arrayWithArray:[[Note getNotesForTalk:self.talk] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"ntDeleted != 1"]]];
    self.arrayNotes = [NSMutableArray arrayWithArray:[self.arrayNotes sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"ntDate" ascending:YES]]]];
    
    [self.tblNotes reloadData];
}

-(void)initTxtView
{
    self.txtViewNote.layer.borderColor = [UIColor colorWithHex:@"AAAAAA" andAlpha:1.0].CGColor;
    self.txtViewNote.layer.borderWidth = 1;
    
    self.txtViewNote.text = self.strPlaceholder;
    self.txtViewNote.textColor = [UIColor lightGrayColor];
    
    self.txtViewNote.delegate = self;
    
    [self.txtViewNote setEditable:true];
    [_txtViewNote setSelectable:true];
}

#pragma mark - UITextView delegate

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:self.strPlaceholder])
    {
        textView.text = @"";
    }
    textView.textColor = [UIColor blackColor];
    return YES;
}

-(void)textViewDidChange:(UITextView *)textView
{
    if(textView.text.length == 0)
    {
        textView.textColor = [UIColor lightGrayColor];
        textView.text = self.strPlaceholder;
        [textView resignFirstResponder];
    }
}

#pragma mark - TableView Data source

-(NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @"Remove note";
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self deleteNoteWithId:(int)indexPath.row];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section
{
    return [self.arrayNotes count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NoteTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([NoteTableViewCell class]) forIndexPath:indexPath];
    
    Note *note = [self.arrayNotes objectAtIndex:indexPath.row];
    
    cell.lblNote.text = note.ntNote;
    [cell setDateAndTime:note];
    
    [cell.btnTrash setTag:indexPath.row];
    [cell.btnTrash addTarget:self action:@selector(deleteButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell updateConstraintsIfNeeded];
    
    return cell;
}

- (IBAction)deleteButtonTapped:(UIButton*)sender
{
    [self deleteNoteWithId:(int)sender.tag];
}

-(void)deleteNoteWithId:(int)noteId
{
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:nil
                                message:@"You are about to delete this note, do you want to proceed?"
                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* btnNO = [UIAlertAction
                            actionWithTitle:@"NO"
                            style:UIAlertActionStyleDefault
                            handler:nil];
    
    UIAlertAction* btnYES = [UIAlertAction
                             actionWithTitle:@"YES"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 Note *note = [self.arrayNotes objectAtIndex:noteId];
                                 
                                 Note *deletedNote = [Note deleteNote:note];
                                 
                                 [[ESyncData sharedInstance] deleteNote:deletedNote];
                                 
                                 [self loadTableData];
                             }];
    [alert addAction:btnNO];
    [alert addAction:btnYES];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - TableView Delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    Note *note = [self.arrayNotes objectAtIndex:indexPath.row];
//    [self showNote:note];
//    
//    [tableView deselectRowAtIndexPath:indexPath animated:true];
}

#pragma mark - Navitation



#pragma mark - IBActions

- (IBAction)addNotes:(id)sender
{
    [self showInsertNoteView];
}

- (IBAction)save:(id)sender
{
    if (![self.txtViewNote.text isEqualToString:self.strPlaceholder])
    {
        NSDictionary *dicNote = @{ @"talk" : self.talk,
                                   @"note" : self.txtViewNote.text};
        
        Note *note = [Note insertNote:dicNote inContext:self.talk.managedObjectContext];
        [[ESyncData sharedInstance] uploadNote:note];
        
        [self loadTableData];
        [self showAllNotesView];
    }
    else
    {
        [self.txtViewNote shake];
    }
}

- (IBAction)cancel:(id)sender
{
    [self showAllNotesView];
}

- (IBAction)actionGoToPresentationDetails:(id)sender
{
    NSString *strID = NSStringFromClass([PresentationDetailsViewController class]);
    PresentationDetailsViewController *vc = [[UIStoryboard storyboardWithName:strID bundle:nil] instantiateViewControllerWithIdentifier:strID];
    
    vc.currentTalkIndex = 0;
    vc.arrayTalks = [NSMutableArray arrayWithArray:@[self.talk]];
    
    self.view.userInteractionEnabled = NO;
    
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - other functions

-(void)showInsertNoteView
{
    self.viewNoteTextView.hidden = self.btnSave.hidden = self.btnCancel.hidden = NO;
    self.btnAddNotes.hidden = YES;
    
    self.btnCancelFullScreen.hidden = YES;
    
    [self initTxtView];
    
    self.title = @"Add note";
}

//-(void)showNote:(Note *)note
//{
//    self.viewNoteTextView.hidden = self.btnCancel.hidden = NO;
//    self.btnAddNotes.hidden = self.btnSave.hidden = self.btnCancel.hidden = YES;
//    
//    [self initTxtView];
//    
//    self.txtViewNote.text = note.ntNote;
//    self.txtViewNote.textColor = [UIColor blackColor];
//    
//    [_txtViewNote setSelectable:false];
//    [self.txtViewNote setEditable:false];
//    
//    self.btnCancelFullScreen.hidden = NO;
//    
//    self.title = @"Note";
//}

-(void)showAllNotesView
{
    [self.txtViewNote resignFirstResponder];
    self.viewNoteTextView.hidden = self.btnSave.hidden = self.btnCancel.hidden = self.btnCancelFullScreen.hidden = YES;
    self.btnAddNotes.hidden = NO;
    self.title = @"Notes";
}

#pragma mark - Set left margin of table cells to ziro for iOS 8

-(void)viewDidLayoutSubviews
{
    if ([self.tblNotes respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tblNotes setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tblNotes respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tblNotes setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

@end
