//
//  NoteTableViewCell.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 2/21/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "NoteTableViewCell.h"

@implementation NoteTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void)setDateAndTime:(id)managedObject
{
    if ([managedObject class] == [Note class])
    {
        self.lblTime.text = [Utils formatTimeStringFromDate: [managedObject valueForKey:@"ntDate"]];
        self.lblDate.text = [Utils formatDateString:[Utils getDate: [managedObject valueForKey:@"ntDate"]]];
    }
}

@end
