//
//  PresentationWithNotesTableViewCell.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 2/21/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PresentationWithNotesTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblTalkTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblSpeakersNames;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;

-(void)setTime:(id)managedObject;

@end
