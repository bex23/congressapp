//
//  NotesTableViewController.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 2/21/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "NotesTableViewController.h"

#import "PresentationWithNotesTableViewCell.h"
#import "PresentationNotesViewController.h"

@interface NotesTableViewController ()

@property (strong, nonatomic) NSFetchedResultsController* fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext* managedObjectContext;

@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;

@end

@implementation NotesTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"My Notes";
    

    
    self.managedObjectContext = [LLDataAccessLayer sharedInstance].managedObjectContext;
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 300.0;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(noteSynced:) name:@"NoteSynced" object:nil];
    
    [[ESyncData sharedInstance] uploadNotes];
    [[ESyncData sharedInstance] getNotes];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    UIBarButtonItem * barButton = [[UIBarButtonItem alloc] initWithCustomView:self.activityIndicator];
    [self navigationItem].rightBarButtonItem = barButton;
    [self.activityIndicator startAnimating];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self doFetch];
}



#pragma mark FRC

-(NSFetchedResultsController *)fetchedResultsController
{
    if (!_fetchedResultsController)
    {
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"Talk" inManagedObjectContext:self.managedObjectContext];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"notes.@count != 0"];
        fetchRequest.predicate = predicate;
        
        [fetchRequest setEntity:entity];
        
        NSSortDescriptor *sortDescriptorTime = [[NSSortDescriptor alloc] initWithKey:@"tlkStartTime" ascending:YES];
        NSSortDescriptor *sortDescriptorName = [[NSSortDescriptor alloc] initWithKey:@"tlkTitle" ascending:YES];
        NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptorTime, sortDescriptorName, nil];
        
        [fetchRequest setSortDescriptors:sortDescriptors];
        
        NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
        self.fetchedResultsController = aFetchedResultsController;
    }
    return _fetchedResultsController;
}

-(void)doFetch
{
    self.fetchedResultsController = nil;
    
    NSError *error;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"error %@ %@", error, error.userInfo);
        abort();
    }
    
    [self.tableView reloadData];
}

-(void)noteSynced:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:notification.name object:nil];

    [self.activityIndicator stopAnimating];
    
    [self doFetch];
    

}

#pragma mark - TableView Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger count = [[_fetchedResultsController sections] count];
    
    if (count == 0)
    {
        count = 1;
    }
    return count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section
{
    int rows = 0;
    
    if ([[_fetchedResultsController sections] count] > 0)
    {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[_fetchedResultsController sections] objectAtIndex:section];
        rows = (int)[sectionInfo numberOfObjects];
    }
    
    return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    PresentationWithNotesTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PresentationWithNotesTableViewCell class]) forIndexPath:indexPath];
    
    Talk *talk = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    cell.lblTalkTitle.text = talk.tlkTitle;
    cell.lblSpeakersNames.text = talk.tlkSpeakerName;
    
    [cell setTime:talk];
    
    [cell updateConstraintsIfNeeded];
    
    return cell;
}

#pragma mark - TableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSString *strID = NSStringFromClass([PresentationNotesViewController class]);
    PresentationNotesViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:strID];
    
    Talk *talk = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    vc.talk = talk;
    
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Set left margin of table cells to ziro for iOS 8

-(void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

@end
