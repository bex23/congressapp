//
//  PresentationWithNotesTableViewCell.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 2/21/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "PresentationWithNotesTableViewCell.h"

@implementation PresentationWithNotesTableViewCell

-(void)setTime:(id)managedObject
{
    NSString* startTime = nil;
    NSString* endTime = nil;
    
    if ([managedObject class] == [Talk class])
    {
        startTime = [Utils formatTimeStringFromDate: [managedObject valueForKey:@"tlkStartTime"]];
        
        endTime= [Utils formatTimeStringFromDate: [managedObject valueForKey:@"tlkEndTime"]];
    }
    else
    {
        startTime = [Utils formatTimeStringFromDate: [managedObject valueForKey:@"sesStartTime"]];
        
        endTime= [Utils formatTimeStringFromDate: [managedObject valueForKey:@"sesEndTime"]];
    }
    self.lblTime.text = [NSString stringWithFormat:@"%@ - %@", startTime, endTime];
}

@end
