//
//  TermsAndConditionsViewController.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 12/5/16.
//  Copyright © 2016 Navus. All rights reserved.
//

#import <UIKit/UIKit.h>
@class TermsAndConditionsViewController;
@protocol TermsAndConditionsViewControllerDelegate <NSObject>
@required
-(void)termsAndConditionsVC_didCancelTC:(TermsAndConditionsViewController*)sender;
-(void)termsAndConditionsVC_didAcceptTC:(TermsAndConditionsViewController*)sender;
@end

@interface TermsAndConditionsViewController : UIViewController

@property (nonatomic, strong)NSString* targetPdfURL;
@property (nonatomic, strong)NSString* pdfNameNoExtension;
@property (nonatomic, assign) BOOL isAcceptanceOfTCMandatory; //no cancel button will be shown if mandatory.
@property (nonatomic, strong) id <TermsAndConditionsViewControllerDelegate>delegate;
-(void)loadBundledPdfWithName:(NSString*)nameNoExtension;
-(void)loadRemotePdfWithURL:(NSString*)targetPdfURL;

@end
