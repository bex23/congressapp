//
//  TermsAndConditionsViewController.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 12/5/16.
//  Copyright © 2016 Navus. All rights reserved.
//

#import "TermsAndConditionsViewController.h"

@interface TermsAndConditionsViewController () <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (weak, nonatomic) IBOutlet UIToolbar *toolbar;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *barBtnCancel;

@end

@implementation TermsAndConditionsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.webView.delegate = self;
    
    if (self.pdfNameNoExtension)
    {
        [self loadBundledPdfWithName:self.pdfNameNoExtension];
    }
    if (self.targetPdfURL)
    {
        [self loadRemotePdfWithURL:self.targetPdfURL];
    }
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    // Assuming self.webView is our UIWebView
    // We go though all sub views of the UIWebView and set their backgroundColor to white
    UIView *v = self.webView;
    while (v) {
        v.backgroundColor = [UIColor whiteColor];
        v = [v.subviews firstObject];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - load bundled or remote pdf to webView
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(void)loadBundledPdfWithName:(NSString*)nameNoExtension
{
    [self.activityIndicator startAnimating];
    NSString *path = [[NSBundle mainBundle] pathForResource:nameNoExtension ofType:@"pdf"];
    NSURL *targetURL = [NSURL fileURLWithPath:path];
    NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
    
    [self.webView loadRequest:request];
}
-(void)loadRemotePdfWithURL:(NSString*)targetPdfURL
{
    [self.activityIndicator startAnimating];
    NSURL *targetURL = [NSURL URLWithString:targetPdfURL];
    NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
    [self.webView loadRequest:request];
}

#///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - setters
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(void)setIsAcceptanceOfTCMandatory:(BOOL)isAcceptanceOfTCMandatory
{
    self.barBtnCancel.enabled = !isAcceptanceOfTCMandatory;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - actions
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (IBAction)cancelTC:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(termsAndConditionsVC_didCancelTC:)])
    {
        if ([self.pdfNameNoExtension isEqualToString:@"AppTC"])
        {
            exit(0);
        }
        else
        {
            [self.delegate termsAndConditionsVC_didCancelTC:self];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }
}

- (IBAction)acceptTC:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(termsAndConditionsVC_didAcceptTC:)])
    {
        [self.delegate termsAndConditionsVC_didAcceptTC:self];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIWebViewDelegate
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [self.activityIndicator stopAnimating];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [self.activityIndicator stopAnimating];
    UIAlertController* ac = [UIAlertController alertControllerWithTitle:@"Error" message:@"Failed to load T&C.\nPlease check your internet connection and try again." preferredStyle:UIAlertControllerStyleAlert];
    [ac addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:ac animated:YES completion:nil];
}


@end
