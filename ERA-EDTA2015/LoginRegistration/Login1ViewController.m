//
//  Login1ViewController.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 1/18/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "Login1ViewController.h"
#import <Realm/Realm.h>

@interface Login1ViewController () <MFMailComposeViewControllerDelegate, ReceiveFurtherInfoViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UITextField *txtUsername;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UIImageView *imgTopBanner;
@property (weak, nonatomic) IBOutlet UITextView *gdprTextView;

@end

@implementation Login1ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Later" style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Continue" style:UIBarButtonItemStylePlain target:self action:@selector(continueLogin)];
    
    [self.gdprTextView setAttributedText:[[RealmRepository shared] gdpr]];
    
    [self setShowHideButtons];
    
    [[BannersServices shared] addWithImageView:self.imgTopBanner
                                            at:BannerPositionTopBanner
                                   sender: self];
}

#pragma mark - show/hide button for password

-(void)setShowHideButtons
{
    CGSize hideShowSize = [@"SHOWX" sizeWithAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.0f]}];
    UIButton *hideShow = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, hideShowSize.width, self.txtPassword.frame.size.height)];
    [hideShow.titleLabel setFont:[UIFont systemFontOfSize:14.0f]];
    [hideShow setTitle:@"SHOW" forState:UIControlStateNormal];
    [hideShow setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    
    self.txtPassword.rightView = hideShow;
    self.txtPassword.rightViewMode = UITextFieldViewModeAlways;
    
    [hideShow addTarget:self action:@selector(hideShow:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)hideShow:(UIButton *)sender
{
    UITextField *txtParent = (UITextField *)sender.superview;
    UIButton *hideShow = (UIButton *)txtParent.rightView;
    if (!txtParent.secureTextEntry)
    {
        txtParent.secureTextEntry = YES;
        [hideShow setTitle:@"SHOW" forState:UIControlStateNormal];
    }
    else
    {
        txtParent.secureTextEntry = NO;
        [hideShow setTitle:@"HIDE" forState:UIControlStateNormal];
    }
    [txtParent becomeFirstResponder];
}

#pragma mark - Continue Login

-(void)continueLogin
{
    BOOL isOK = YES;
    
    __block NSString* infoMessage = @"";
    
    [self.view endEditing:YES];
    if ([self.txtUsername.text isEqualToString:@""])
    {
        isOK = NO;
        [self.txtUsername shake];
        infoMessage = [infoMessage stringByAppendingString:@"Username\n"];
    }
    if ([self.txtPassword.text isEqualToString:@""])
    {
        isOK = NO;
        [self.txtPassword shake];
        infoMessage = [infoMessage stringByAppendingString:@"Password\n"];
    }
    if (!isOK)
    {
        UIAlertController* ac = [UIAlertController alertControllerWithTitle:@"Please provide all mandatory fields:" message:infoMessage preferredStyle:UIAlertControllerStyleAlert];
        [ac addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil]];
        [self presentViewController:ac animated:YES completion:nil];
    }
    else
    {
        self.navigationItem.rightBarButtonItem.enabled = NO;
        
        [self addLoader];
        [[ESyncData sharedInstance] loginWithUsername:self.txtUsername.text AndPassword:self.txtPassword.text success:^{
            [self removeLoaderIfExists];
            [[ESyncData sharedInstance] syncUserData:YES];
            self.navigationItem.rightBarButtonItem.enabled = YES;
            
            if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"sponsored"] intValue])
            {
                ReceiveFurtherInfoViewController *vc = [[ReceiveFurtherInfoViewController alloc] init];
                vc.delegate = self;
                UINavigationController *navBar = [[UINavigationController alloc] initWithRootViewController:vc];
                [self presentViewController:navBar animated:YES completion:nil];
            }
            else
            {
                [self back];
            }
            
        } failure:^{
            [self removeLoaderIfExists];
            self.navigationItem.rightBarButtonItem.enabled = YES;
        }];
    }
}

-(void)back
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - IBActions

- (IBAction)btnNeedHelpAction:(id)sender
{
    //    NSString *emailTitle = @"My order from ERA-EDTA 2014";
    NSArray *toRecipents = [NSArray arrayWithObject:@"support@e-materials.com"];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    //    [mc setSubject:emailTitle];
    //    [mc setMessageBody:messageBody isHTML:NO];
    
    [mc setToRecipients:toRecipents];
    
    // Determine the file name and extension
    if(mc)
    {
        // Present mail view controller on screen
        [self presentViewController:mc animated:YES completion:NULL];
    }
}

#pragma mark MFMailComposerDelegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - ReceiveFurtherInfoViewControllerDelegate

-(void)didCancelReceiveFurtherInfoViewController:(ReceiveFurtherInfoViewController *)vc
{
    [vc dismissViewControllerAnimated:YES completion:nil];
    [self back];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

@end
