//
//  PSCountryViewController.h
//  ERA-EDTA2013
//
//  Created by admin on 1/29/14.
//  Copyright (c) 2014 Raul Catena. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PSCountryViewControllerDelegate <NSObject>
@required
-(void)didSelectRow:(UITableViewCell*)cell withItem:(id)item;

@end

@interface PSCountryViewController : UIViewController

@property (strong, nonatomic)id <PSCountryViewControllerDelegate> delegate;
@property (retain, nonatomic) IBOutlet UITableView *tblView;
@property (nonatomic, strong) NSMutableArray *searchResults;
@property (nonatomic, strong) NSArray *countriesArray;
@property (nonatomic, strong) NSMutableArray *sectionsArray;
@property (nonatomic, strong) UILocalizedIndexedCollation *collation;

@end
