//
//  ERARegistrationStep2VC.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 23.12.15..
//  Copyright © 2015. Navus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSCountryViewController.h"

@interface ERARegistrationStep2VC : UIViewController <UITextFieldDelegate>

@property (nonatomic, strong) NSString *code;
@property (nonatomic, retain) Country *selectedCountry;
@property (nonatomic, strong) NSMutableDictionary *dicUserData;

@end
