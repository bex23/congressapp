//
//  ERARegistrationStep2VC.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 23.12.15..
//  Copyright © 2015. Navus. All rights reserved.
//

#import "ERARegistrationStep2VC.h"

@interface ERARegistrationStep2VC () <ReceiveFurtherInfoViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
//@property (strong, nonatomic) IBOutlet UITextField *txtFirstName;
//@property (strong, nonatomic) IBOutlet UITextField *txtLastName;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtRepeatPassword;
@property (weak, nonatomic) IBOutlet UISwitch *swtHP;

@property (weak, nonatomic) IBOutlet UIImageView *imgTopBanner;
@property (weak, nonatomic) IBOutlet UIImageView *imgBottomBanner;

@end

@implementation ERARegistrationStep2VC

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Register" style:UIBarButtonItemStylePlain target:self action:@selector(continueRegistration)];

    [self setShowHideButtons];
    
    self.txtEmail.text = ([self.dicUserData objectForKey:@"email"] != (id)[NSNull null]) ? [self.dicUserData objectForKey:@"email"] : @"";
    [self.txtEmail addTarget:self action:@selector(checkEmail:) forControlEvents:UIControlEventEditingChanged];
    
    if ([self.dicUserData objectForKey:@"first_name"] == (id)[NSNull null])
    {
        [self.dicUserData setValue:@"First Name" forKey:@"first_name"];
    }
    if ([self.dicUserData objectForKey:@"last_name"] == (id)[NSNull null])
    {
        [self.dicUserData setValue:@"Last Name" forKey:@"last_name"];
    }

    [[BannersServices shared] addWithImageView:self.imgTopBanner
                                            at:BannerPositionTopBanner
                                        sender:self];
}

#pragma mark - show/hide button for password

-(void)setShowHideButtons
{
    CGSize hideShowSize = [@"SHOWX" sizeWithAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.0f]}];
    UIButton *hideShow = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, hideShowSize.width, self.txtPassword.frame.size.height)];
    [hideShow.titleLabel setFont:[UIFont systemFontOfSize:14.0f]];
    [hideShow setTitle:@"SHOW" forState:UIControlStateNormal];
    [hideShow setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    
    UIButton *hideShow1 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, hideShowSize.width, self.txtRepeatPassword.frame.size.height)];
    [hideShow1.titleLabel setFont:[UIFont systemFontOfSize:14.0f]];
    [hideShow1 setTitle:@"SHOW" forState:UIControlStateNormal];
    [hideShow1 setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    
    self.txtPassword.rightView = hideShow;
    self.txtPassword.rightViewMode = UITextFieldViewModeAlways;
    
    self.txtRepeatPassword.rightView = hideShow1;
    self.txtRepeatPassword.rightViewMode = UITextFieldViewModeAlways;
    
    [hideShow addTarget:self action:@selector(hideShow:) forControlEvents:UIControlEventTouchUpInside];
    [hideShow1 addTarget:self action:@selector(hideShow:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)hideShow:(UIButton *)sender
{
    UITextField *txtParent = (UITextField *)sender.superview;
    UIButton *hideShow = (UIButton *)txtParent.rightView;
    if (!txtParent.secureTextEntry)
    {
        txtParent.secureTextEntry = YES;
        [hideShow setTitle:@"SHOW" forState:UIControlStateNormal];
    }
    else
    {
        txtParent.secureTextEntry = NO;
        [hideShow setTitle:@"HIDE" forState:UIControlStateNormal];
    }
    [txtParent becomeFirstResponder];
}

#pragma mark - Continue Registration process

-(void)continueRegistration
{
    BOOL isOK = YES;
    __block NSString* infoMessage = @"";
    
    if ([self.txtEmail.text isEqualToString:@""])
    {
        isOK = NO;
        [self.txtEmail shake];
        infoMessage = [infoMessage stringByAppendingString:@"Email\n"];
    }
    else if(![Utils checkEmailIsValid:self.txtEmail.text])
    {
        isOK = NO;
        [self.txtEmail shake];
        infoMessage = [infoMessage stringByAppendingString:@"Valid Email\n"];
    }
    if ([self.txtPassword.text isEqualToString:@""])
    {
        isOK = NO;
        [self.txtPassword shake];
        infoMessage = [infoMessage stringByAppendingString:@"Password\n"];
    }
    if (![self.txtRepeatPassword.text isEqualToString:self.txtPassword.text])
    {
        isOK = NO;
        [self.txtRepeatPassword shake];
        infoMessage = [infoMessage stringByAppendingString:@"Passwords doesn't match.\n"];
    }
    
    if (!isOK)
    {
        UIAlertController* ac = [UIAlertController alertControllerWithTitle:@"Please provide all mandatory fields:"
                                                                    message:infoMessage
                                                             preferredStyle:UIAlertControllerStyleAlert];
        [ac addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil]];
        [self presentViewController:ac animated:YES completion:nil];
    }
    else
    {
        NSString *slug = [[RealmRepository shared] confSlug];
        UIAlertController* ac = [UIAlertController alertControllerWithTitle:@"Registration"
                                                                    message:[NSString stringWithFormat:@"Please confirm that you are %@ %@ from %@ and your email address is: \n\n %@ \n\nPlease note that your email address is your username. Please use the same credentials for your online e-materials account at e-materials.com/%@", [self.dicUserData valueForKey:@"first_name"], [self.dicUserData valueForKey:@"last_name"], self.selectedCountry.ctrName, self.txtEmail.text, slug]
                                                             preferredStyle:UIAlertControllerStyleAlert];
        [ac addAction:[UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self registerUser];
        }]];
        [ac addAction:[UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleCancel handler:nil]];
        [self presentViewController:ac animated:YES completion:nil];
    }
}

-(void)registerUser
{
    self.navigationItem.rightBarButtonItem.enabled = NO;
    
    NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   @1, @"terms_and_conditions",
                                   self.code , @"code",
                                   self.txtEmail.text, @"email",
                                   self.txtPassword.text, @"password",
                                   [self.dicUserData valueForKey:@"first_name"], @"first_name",
                                   [self.dicUserData valueForKey:@"last_name"], @"last_name",
                                   self.selectedCountry.ctrCountryId, @"country_id",
                                   @"user", @"role",
                                   nil];
    
    NSLog(@"Registration params: %@", params);
    
    [self addLoader];
    [[ESyncData sharedInstance] registerUser:params success:^(NSDictionary *dicResponse){
        [self removeLoaderIfExists];
        NSString *slug = [[RealmRepository shared] confSlug];
        UIAlertController *alert = [UIAlertController
                                    alertControllerWithTitle:nil
                                    message:[NSString stringWithFormat:@"Thank you for your registration. You can also use your username (email address) and password to access your E-materials account at a PC via e-materials.com/%@", slug]
                                    preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* btnClose = [UIAlertAction
                                   actionWithTitle:@"Close"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       [self.dicUserData setValue:self.txtEmail.text forKey:@"email"];
                                       
                                       [[ESyncData sharedInstance] limitedLogin:self.dicUserData success:^{
                                           [self removeLoaderIfExists];
                                           [[ESyncData sharedInstance] syncUserData:YES];
                                           self.navigationItem.rightBarButtonItem.enabled = YES;
                                           
                                           if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"sponsored"] intValue])
                                           {
                                               ReceiveFurtherInfoViewController *vc = [[ReceiveFurtherInfoViewController alloc] init];
                                               vc.delegate = self;
                                               UINavigationController *navBar = [[UINavigationController alloc] initWithRootViewController:vc];
                                               [self presentViewController:navBar animated:YES completion:nil];
                                           }
                                           else
                                           {
                                               [self.navigationController dismissViewControllerAnimated:YES completion:nil];
                                           }
                                           
                                       } failure:^{
                                           [self removeLoaderIfExists];
                                           self.navigationItem.rightBarButtonItem.enabled = YES;
                                           [self.navigationController dismissViewControllerAnimated:YES completion:nil];
                                       }];
                                   }];
        [alert addAction:btnClose];
        [self presentViewController:alert animated:YES completion:nil];
    } failure:^{
        [self removeLoaderIfExists];
        self.navigationItem.rightBarButtonItem.enabled = YES;
    }];
}

-(void)checkEmail:(UITextField *)txtField
{
    NSString *txt = txtField.text;
    if (txt.length > 0 && !([txt rangeOfString:@"@"].location == NSNotFound) && !([txt rangeOfString:@"."].location == NSNotFound))
    {
        [[ESyncData sharedInstance] checkEmail:txtField.text success:^() {
            UIAlertController* ac = [UIAlertController alertControllerWithTitle:@"Email"
                                                                        message:@"It seams that you are already registered on E-Materials. Do you want to login?"
                                                                 preferredStyle:UIAlertControllerStyleAlert];
            [ac addAction:[UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self.dicUserData setValue:self.txtEmail.text forKey:@"email"];
                
                [[ESyncData sharedInstance] limitedLogin:self.dicUserData success:^{
                    [[ESyncData sharedInstance] connectAccount:[self.dicUserData objectForKey:@"id"] success:^{
                        
                        if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"sponsored"] intValue])
                        {
                            ReceiveFurtherInfoViewController *vc = [[ReceiveFurtherInfoViewController alloc] init];
                            vc.delegate = self;
                            UINavigationController *navBar = [[UINavigationController alloc] initWithRootViewController:vc];
                            [self presentViewController:navBar animated:YES completion:nil];
                        }
                        else
                        {
                            [self.navigationController dismissViewControllerAnimated:YES completion:nil];
                        }
                        
                    } failure:^{
                        // we have to resetLoginProperties those set in limitedLogin function, because connectAccount failure
                        [[ESyncData sharedInstance] resetLoginProperties];
                    }];
                } failure:^{
                    
                }];
            }]];
            [ac addAction:[UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleCancel handler:nil]];
            [self presentViewController:ac animated:YES completion:nil];
        } failure:^{
            
        }];
    }
}

#pragma mark - ReceiveFurtherInfoViewControllerDelegate

-(void)didCancelReceiveFurtherInfoViewController:(ReceiveFurtherInfoViewController *)vc
{
    [vc dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

@end
