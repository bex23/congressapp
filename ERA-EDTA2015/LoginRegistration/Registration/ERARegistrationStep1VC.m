//
//  ERARegistrationStep1VC.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 23.12.15..
//  Copyright © 2015. Navus. All rights reserved.
//

#import "ERARegistrationStep1VC.h"
#import "IQKeyboardManager.h"
#import "ERARegistrationStep2VC.h"
#import <AVFoundation/AVFoundation.h>

@interface ERARegistrationStep1VC () <ReceiveFurtherInfoViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UITextField *txtBarcode;
@property (weak, nonatomic) IBOutlet UITextField *txtCountry;
@property (weak, nonatomic) IBOutlet UIImageView *imgPhotoCamera;

@property (retain, nonatomic) Country *selectedCountry;
@property (strong, nonatomic) NSDictionary *dicResponseUserData;

@property (weak, nonatomic) IBOutlet UITextView *gdprTextView;
@property (weak, nonatomic) IBOutlet UIButton *btnLoginWithUsername;
@property (weak, nonatomic) IBOutlet UIImageView *imgTopBanner;
@property (weak, nonatomic) IBOutlet UIView *separator;
@property (weak, nonatomic) IBOutlet UILabel *orLabel;

@end

@implementation ERARegistrationStep1VC

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //Left bar button
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Later" style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Continue" style:UIBarButtonItemStylePlain target:self action:@selector(continueRegistration)];
    [self.gdprTextView setAttributedText:[[RealmRepository shared] gdpr]];
    [[BannersServices shared] addWithImageView:self.imgTopBanner
                                            at:BannerPositionTopBanner
                                   sender: self];
  
    UIColor *global = [UIColor colorWithHex:kGlobalColor
                                   andAlpha:1.0];
    self.orLabel.textColor = global;
    self.separator.backgroundColor = global;
    [self.btnLoginWithUsername.layer setCornerRadius:10.0];
    [self.btnLoginWithUsername.layer setMasksToBounds:YES];
    [self.btnLoginWithUsername.layer setBorderWidth:1.0];
    [self.btnLoginWithUsername.layer setBorderColor:global.CGColor];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[IQKeyboardManager sharedManager] setPreviousNextDisplayMode:IQPreviousNextDisplayModeAlwaysHide];
    [self.imgPhotoCamera setImage:[UIImage imageNamed:@"photo_camera"]];
    [self.imgPhotoCamera colorItWithColor:[UIColor colorWithHex:kGlobalColor andAlpha:1.0]];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[IQKeyboardManager sharedManager] setPreviousNextDisplayMode:IQPreviousNextDisplayModeDefault];
}

#pragma mark - Continue Registration process

-(void)continueRegistration
{
    BOOL isOK = YES;
    
    __block NSString* infoMessage = @"";
    
    [self.txtBarcode resignFirstResponder];
    if (!isOK)
    {
        UIAlertController* ac = [UIAlertController alertControllerWithTitle:@"Please provide all mandatory fields:" message:infoMessage preferredStyle:UIAlertControllerStyleAlert];
        [ac addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil]];
        [self presentViewController:ac animated:YES completion:nil];
    }
    else
    {
        self.navigationItem.rightBarButtonItem.enabled = NO;
        [self addLoader];
        [[ESyncData sharedInstance] checkCode:self.txtBarcode.text ForCountry:self.selectedCountry
                                      success:^(NSDictionary *responseObject) {
                                          __block NSDictionary *dicParty = [responseObject objectForKey:@"party"];
                                          
                                          if (![[dicParty objectForKey:@"user_id"] isEqual:(id)[NSNull null]]) // user registered for this congress
                                          {
                                              NSString *strUsersEmail = [[responseObject objectForKey:@"user"] objectForKey:@"email"];
                                              
                                              UIAlertController *alert = [UIAlertController
                                                                          alertControllerWithTitle:@"Registration"
                                                                          message:[NSString stringWithFormat:@"Hello %@,\n\nYou already have an e-materials account with the following user name:\n%@\n\nIf this is not your email address please try again or contact our help desk.\n\nYou are now logged in.", [[dicParty objectForKey:@"name"] isEqual:[NSNull null]] ? @"" : [dicParty objectForKey:@"name"], strUsersEmail]
                                                                          preferredStyle:UIAlertControllerStyleAlert];
                                              
                                              UIAlertAction* btnEnter = [UIAlertAction
                                                                         actionWithTitle:@"ENTER APP"
                                                                         style:UIAlertActionStyleDefault
                                                                         handler:^(UIAlertAction * action)
                                                                         {
                                                                             [[ESyncData sharedInstance] limitedLogin:@{@"email" : strUsersEmail} success:^{
                                                                                 [self removeLoaderIfExists];
                                                                                 [[ESyncData sharedInstance] syncUserData:YES];
                                                                                 self.navigationItem.rightBarButtonItem.enabled = YES;

                                                                                 if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"sponsored"] intValue])
                                                                                 {
                                                                                     ReceiveFurtherInfoViewController *vc = [[ReceiveFurtherInfoViewController alloc] init];
                                                                                     vc.delegate = self;
                                                                                     UINavigationController *navBar = [[UINavigationController alloc] initWithRootViewController:vc];
                                                                                     [self presentViewController:navBar animated:YES completion:nil];
                                                                                 }
                                                                                 else
                                                                                 {
                                                                                     [self backToRootVC];
                                                                                 }

                                                                             } failure:^{
                                                                                 [self removeLoaderIfExists];
                                                                                 self.navigationItem.rightBarButtonItem.enabled = YES;
                                                                             }];
                                                                         }];
                                              
                                              UIAlertAction* btnCancel = [UIAlertAction
                                                                          actionWithTitle:@"CANCEL"
                                                                          style:UIAlertActionStyleDefault
                                                                          handler:^(UIAlertAction * action)
                                                                          {
                                                                              self.navigationItem.rightBarButtonItem.enabled = YES;
                                                                          }];
                                              
                                              [alert addAction:btnCancel];
                                              [alert addAction:btnEnter];
                                              
                                              [self presentViewController:alert animated:YES completion:nil];
                                          }
                                          else
                                          {
                                              // check if user registered on another congresses
                                              [[ESyncData sharedInstance] checkEmail:[dicParty objectForKey:@"email"] success:^{
                                                  [self removeLoaderIfExists];
                                                  UIAlertController *alert = [UIAlertController
                                                                              alertControllerWithTitle:@"Registration"
                                                                              message:[NSString stringWithFormat:@"Hello %@,\n\nYou already have an e-materials account with the following user name:\n%@\n\nIf this is not your email address please try again or contact our help desk.\n\nYou are now logged in.", [[dicParty objectForKey:@"name"] isEqual:[NSNull null]] ? @"" : [dicParty objectForKey:@"name"], [dicParty objectForKey:@"email"]]
                                                                              preferredStyle:UIAlertControllerStyleAlert];
                                                  
                                                  UIAlertAction *btnEnter = [UIAlertAction
                                                                             actionWithTitle:@"ENTER APP"
                                                                             style:UIAlertActionStyleDefault
                                                                             handler:^(UIAlertAction * action)
                                                                             {
                                                                                 [[ESyncData sharedInstance] limitedLogin:dicParty success:^{
                                                                                     [[ESyncData sharedInstance] connectAccount:[dicParty objectForKey:@"id"] success:^{
                                                                                         
                                                                                         self.navigationItem.rightBarButtonItem.enabled = YES;

                                                                                         if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"sponsored"] intValue])
                                                                                         {
                                                                                             ReceiveFurtherInfoViewController *vc = [[ReceiveFurtherInfoViewController alloc] init];
                                                                                             vc.delegate = self;
                                                                                             UINavigationController *navBar = [[UINavigationController alloc] initWithRootViewController:vc];
                                                                                             [self presentViewController:navBar animated:YES completion:nil];
                                                                                         }
                                                                                         else
                                                                                         {
                                                                                             [self backToRootVC];
                                                                                         }
                                                                                         
                                                                                     } failure:^{
                                                                                         // we have to resetLoginProperties those set in limitedLogin, because connectAccount failure
                                                                                         [[ESyncData sharedInstance] resetLoginProperties];
                                                                                         self.navigationItem.rightBarButtonItem.enabled = YES;
                                                                                     }];
                                                                                 } failure:^{
                                                                                     self.navigationItem.rightBarButtonItem.enabled = YES;
                                                                                 }];
                                                                             }];
                                                  
                                                  UIAlertAction* btnCancel = [UIAlertAction
                                                                              actionWithTitle:@"CANCEL"
                                                                              style:UIAlertActionStyleDefault
                                                                              handler:^(UIAlertAction * action)
                                                                              {
                                                                                  self.navigationItem.rightBarButtonItem.enabled = YES;
                                                                              }];
                                                  
                                                  [alert addAction:btnCancel];
                                                  [alert addAction:btnEnter];
                                                  
                                                  [self presentViewController:alert animated:YES completion:nil];
                                                  
                                              } failure:^{
                                                  [self removeLoaderIfExists];
                                                  self.dicResponseUserData = dicParty;
                                                  self.navigationItem.rightBarButtonItem.enabled = YES;
                                                  [self performSegueWithIdentifier:@"segueRegStep2" sender:nil];
                                                  self.navigationItem.rightBarButtonItem.enabled = YES;
                                              }];
                                          }
                                      } failure:^() {
                                          [self removeLoaderIfExists];
                                          self.navigationItem.rightBarButtonItem.enabled = YES;
                                      }];
    }
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqual:@"segueRegStep2"])
    {
        ERARegistrationStep2VC *vc = [segue destinationViewController];
        vc.code = self.txtBarcode.text;
        vc.dicUserData = [NSMutableDictionary dictionaryWithDictionary: self.dicResponseUserData];
        vc.selectedCountry = self.selectedCountry;
    }
}

-(void)back
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITextFieldDelegate

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == self.txtCountry)
    {
        [self.view endEditing:YES]; // resign a text field responder
        
        PSCountryViewController *countryVC = [[PSCountryViewController alloc]init] ;
        countryVC.delegate = self;
        [self.navigationController pushViewController:countryVC animated:YES];
    }
    return !(textField == self.txtCountry);
}

#pragma mark - IBActions

- (IBAction)actionScanBarcode:(UITapGestureRecognizer *)sender
{
    [self.imgPhotoCamera colorItWithColor:[UIColor colorWithHex:kOtherColor andAlpha:1.0]];
    
    NSString *mediaType = AVMediaTypeVideo;
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:mediaType];
    
    if(authStatus == AVAuthorizationStatusAuthorized)
    {
        [self.imgPhotoCamera colorItWithColor:[UIColor colorWithHex:kGlobalColor andAlpha:1.0]];
        [[BarcodeScannerProxy shared] showBarcodeWithCallback:^(NSString *code) {
            self.txtBarcode.text = code;
        }];
    }
    else if(authStatus == AVAuthorizationStatusDenied)
    {
        // denied
        [self.imgPhotoCamera colorItWithColor:[UIColor colorWithHex:kGlobalColor andAlpha:1.0]];
        
        [self showMessageAllowCameraAccess];
    }
    else if(authStatus == AVAuthorizationStatusRestricted)
    {
        // restricted, normally won't happen
        [self.imgPhotoCamera colorItWithColor:[UIColor colorWithHex:kGlobalColor andAlpha:1.0]];
        [self showMessageAllowCameraAccess];
    }
    else if(authStatus == AVAuthorizationStatusNotDetermined)
    {
        [self.imgPhotoCamera colorItWithColor:[UIColor colorWithHex:kGlobalColor andAlpha:1.0]];
        
        // not determined?!
        [AVCaptureDevice requestAccessForMediaType:mediaType completionHandler:^(BOOL granted) {
            
            if(granted){
                // Permission has been granted. Use dispatch_async for any UI updating
                // code because this block may be executed in a thread.
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[BarcodeScannerProxy shared] showBarcodeWithCallback:^(NSString *code) {
                        self.txtBarcode.text = code;
                    }];
                });
                
                NSLog(@"Granted access to %@", mediaType);
            } else {
                NSLog(@"Not granted access to %@", mediaType);
            }
        }];
    }
    else
    {
        // impossible, unknown authorization status
    }
}

-(void)showMessageAllowCameraAccess
{
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@"IMPORTANT"
                                message:@"Please allow camera access for barcode scanning"
                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *btnEnter = [UIAlertAction
                               actionWithTitle:@"Settings"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                               }];
    
    UIAlertAction* btnCancel = [UIAlertAction
                                actionWithTitle:@"Cancel"
                                style:UIAlertActionStyleDefault
                                handler:nil];
    
    [alert addAction:btnCancel];
    [alert addAction:btnEnter];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)btnNeedHelpAction:(id)sender
{
    //    NSString *emailTitle = @"My order from ERA-EDTA 2014";
    NSArray *toRecipents = [NSArray arrayWithObject:@"support@e-materials.com"];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    //    [mc setSubject:emailTitle];
    //    [mc setMessageBody:messageBody isHTML:NO];
    
    [mc setToRecipients:toRecipents];
    
    // Determine the file name and extension
    if(mc)
    {
        // Present mail view controller on screen
        [self presentViewController:mc animated:YES completion:NULL];
    }
}

#pragma mark - PSCountryVCDelegate method

-(void)didSelectRow:(UITableViewCell*)cell withItem:(id)item
{
    Country* country = (Country*)item;
    self.txtCountry.text = country.ctrName;
    self.selectedCountry = country;
}

#pragma mark MFMailComposerDelegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - other function

-(void)backToRootVC
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - ReceiveFurtherInfoViewControllerDelegate

-(void)didCancelReceiveFurtherInfoViewController:(ReceiveFurtherInfoViewController *)vc
{
    [vc dismissViewControllerAnimated:YES completion:nil];
    [self backToRootVC];
}

@end
