//
//  PSCountryViewController.m
//  ERA-EDTA2013
//
//  Created by admin on 1/29/14.
//  Copyright (c) 2014 Raul Catena. All rights reserved.
//

#import "PSCountryViewController.h"

@interface PSCountryViewController () <UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation PSCountryViewController
@synthesize collation, sectionsArray, countriesArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Countries";
//    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
//    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
//    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    self.extendedLayoutIncludesOpaqueBars=NO;

//    [self addBackButton];
    
    self.searchResults = [[NSMutableArray alloc] init];

    self.countriesArray = [Country getCountries];
    
    self.searchBar.delegate = self;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
//    [self.searchController.view removeFromSuperview];
}

-(void)addBackButton
{
    UIImage *back = [UIImage imageNamed:@"blackArrowBackBarButton.png"];
    CGRect frameimg = CGRectMake(0, 0, back.size.width/2.3, back.size.height/2.3);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    [backButton setBackgroundImage:back forState:UIControlStateNormal];
    
    [backButton addTarget:self action:@selector(stepBack) forControlEvents:UIControlEventTouchUpInside];
    
    [backButton setShowsTouchWhenHighlighted:YES];
    UIBarButtonItem *backBarButton = [[UIBarButtonItem alloc]initWithCustomView:backButton] ;
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero]  ;
    label.backgroundColor = [UIColor clearColor];
//    label.font = [UIFont fontWithName:GENERAL_FONT_LIGHT size:22.0];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor blackColor];
    self.navigationItem.titleView = label;
    label.text = self.title;
    [label sizeToFit];
    
    self.navigationItem.leftBarButtonItem = backBarButton;
}

-(void)stepBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if (self.searchBar.isFirstResponder)
	{
        return 1;
    }
    else
    {
        // The number of sections is the same as the number of titles in the collation.
        return self.sectionsArray.count;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	
	// The number of time zones in the section is the count of the array associated with the section in the sections array.
    if (self.searchBar.isFirstResponder)
	{
        return [self.searchResults count];
    }
    else
    {
        NSArray *countrysInSection = [sectionsArray objectAtIndex:section];
        
        return [countrysInSection count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    Country* country = nil;
    
    if (self.searchBar.isFirstResponder)
	{
        country= [self.searchResults objectAtIndex:indexPath.row];
    }
    else
    {
        NSArray *countrysInSection = [sectionsArray objectAtIndex:indexPath.section];

        country = [countrysInSection objectAtIndex:indexPath.row];
    }
    
    cell.textLabel.text = country.ctrName;
	
    return cell;
}

/*
 Section-related methods: Retrieve the section titles and section index titles from the collation.
 */

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    //    [[sectionsArray objectAtIndex:section] count] ? [[[UILocalizedIndexedCollation currentCollation] sectionTitles] objectAtIndex:section] : nil;
    if (self.searchBar.isFirstResponder)
    {
        return nil;
    }
    else
    {
        return [[sectionsArray objectAtIndex:section] count] ? [[collation sectionTitles] objectAtIndex:section] : nil;
    }
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    
    if (self.searchBar.isFirstResponder)
    {
        return nil;
    }
    else
    {
        //UITableViewIndexSearch dodaje malu lupu na pocetak
        return [[NSArray arrayWithObject:UITableViewIndexSearch] arrayByAddingObjectsFromArray:
                [collation sectionIndexTitles]];
    }
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    if (self.searchBar.isFirstResponder)
    {
        return 0;
    }
    else
    {
        if (title == UITableViewIndexSearch)
        {
            CGRect searchBarFrame = self.searchBar.frame;
            [tableView scrollRectToVisible:searchBarFrame animated:NO];
            return -1;
        }
        else
        {
            return [collation sectionForSectionIndexTitleAtIndex:index-1];
        }
    }
}

#pragma mark - TableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//	[tableView deselectRowAtIndexPath:indexPath animated:YES];
    if ([self.delegate respondsToSelector:@selector(didSelectRow:withItem:)])
    {
        if (self.searchBar.isFirstResponder)
        {
            [self.delegate didSelectRow:[tableView cellForRowAtIndexPath:indexPath] withItem:[self.searchResults objectAtIndex:indexPath.row]];
        }
        else
        {
            NSArray *countrysInSection = [sectionsArray objectAtIndex:indexPath.section];
            
            Country* country = [countrysInSection objectAtIndex:indexPath.row];
            [self.delegate didSelectRow:[tableView cellForRowAtIndexPath:indexPath] withItem:country];
        }
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark Set the data array and configure the section data

- (void)setCountriesArray:(NSMutableArray *)newDataArray {
	if (newDataArray != countriesArray) {
        
		countriesArray = newDataArray;
	}
	if (countriesArray == nil) {
		self.sectionsArray = nil;
	}
	else {
		[self configureSections];
	}
}

- (void)configureSections {
	
	// Get the current collation and keep a reference to it.
	self.collation = [UILocalizedIndexedCollation currentCollation];
	
	NSInteger index, sectionTitlesCount = [[collation sectionTitles] count];
	
	NSMutableArray *newSectionsArray = [[NSMutableArray alloc] initWithCapacity:sectionTitlesCount];
	
	// Set up the sections array: elements are mutable arrays that will contain the time zones for that section.
	for (index = 0; index < sectionTitlesCount; index++) {
		NSMutableArray *array = [[NSMutableArray alloc] init];
		[newSectionsArray addObject:array];
	}
	
	// Segregate the time zones into the appropriate arrays.
	for (Country *country in countriesArray) {
		
		// Ask the collation which section number the time zone belongs in, based on its locale name.
		NSInteger sectionNumber = [collation sectionForObject:country collationStringSelector:@selector(ctrName)];
		
		// Get the array for the section.
		NSMutableArray *sectionTimeZones = [newSectionsArray objectAtIndex:sectionNumber];
		
		//  Add the time zone to the section.
		[sectionTimeZones addObject:country];
	}
	
	// Now that all the data's in place, each section array needs to be sorted.
	for (index = 0; index < sectionTitlesCount; index++) {
		
		NSMutableArray *countriesArrayForSection = [newSectionsArray objectAtIndex:index];
		
		// If the table view or its contents were editable, you would make a mutable copy here.
		NSArray *sortedCountrysArrayForSection = [collation sortedArrayFromArray:countriesArrayForSection collationStringSelector:@selector(ctrName)];
		
		// Replace the existing array with the sorted array.
		[newSectionsArray replaceObjectAtIndex:index withObject:sortedCountrysArrayForSection];
	}
	
	self.sectionsArray = newSectionsArray;
}

#pragma mark Content Filtering

- (void)filterContentForSearchText:(NSString*)searchText
{
    //Update the filtered array based on the search text and scope.
	[self.searchResults removeAllObjects]; // First clear the filtered array.
	
	// Search the main list for products whose type matches the scope (if selected) and whose name matches searchText; add items that match to the filtered array.
    for (NSArray *section in self.sectionsArray)
    {
        for (Country *country in section)
        {
            NSRange range = [country.ctrName rangeOfString:searchText options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)];
            if (range.location != NSNotFound)
            {
                [self.searchResults addObject:country];
            }
            
            //                NSComparisonResult result = [country.title compare:searchText options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch) range:NSMakeRange(0, [searchText length])];
            //                if (result == NSOrderedSame)
            //                {
            //                    [self.searchResults addObject:country];
            //                }
        }
    }
}

#pragma mark - UISearchBarDelegate

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    NSString *searchString = searchText;
    [self filterContentForSearchText:searchString];
    [self.tblView reloadData];
}

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [self.tblView reloadData];
    searchBar.showsCancelButton = YES;
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    searchBar.showsCancelButton = NO;
    searchBar.text = @"";
    [searchBar resignFirstResponder];
    [self.tblView reloadData];
}

@end
