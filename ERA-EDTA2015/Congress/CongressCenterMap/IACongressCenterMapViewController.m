//
//  IACongressCenterMapViewController.m
//  ERA-EDTA2015
//
//  Created by Predrag Despotović on 7/20/14.
//  Copyright (c) 2014 Navus. All rights reserved.
//

#import "IACongressCenterMapViewController.h"

#import "Mapbox.h"

@interface IACongressCenterMapViewController () <RMMapViewDelegate>

@property (weak, nonatomic) RMMapView* mapboxView;
@property (strong, nonatomic) NSArray *arrayLevels;

@property (weak, nonatomic) IBOutlet UILabel *lblCurrentLevel;

@end

@implementation IACongressCenterMapViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIButton *but = [UIButton buttonWithType:UIButtonTypeCustom];
    [but setBackgroundImage:[UIImage imageNamed:@"filter.png"] forState:UIControlStateNormal];
    [but setBackgroundImage:[UIImage imageNamed:@"filter_pressed.png"] forState:UIControlStateHighlighted];
    [but addTarget:self action:@selector(changeFloor:) forControlEvents:UIControlEventTouchUpInside];
    but.bounds = CGRectMake(0, 0, 30, 30);
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:but];
    
    self.arrayLevels = [kCongressLevels componentsSeparatedByString:@","];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    NSString *strCurrentMap =  @"";
    
    if (self.roomLevel && [self.arrayLevels objectAtIndex:[self.roomLevel intValue]])
    {
        strCurrentMap = [self.arrayLevels objectAtIndex:[self.roomLevel intValue]];
        self.lblCurrentLevel.text = [[strCurrentMap stringByReplacingOccurrencesOfString:@"_" withString:@" "] uppercaseString];
    }
    else
    {
        strCurrentMap = [self.arrayLevels firstObject];
        self.lblCurrentLevel.text = [[strCurrentMap stringByReplacingOccurrencesOfString:@"_" withString:@" "] uppercaseString];
    }
    
    RMMBTilesSource *offlineSource = [[RMMBTilesSource alloc] initWithTileSetResource:strCurrentMap ofType:@"mbtiles"];
    
    RMMapView *mapView = [[RMMapView alloc] initWithFrame:self.mapView.bounds andTilesource:offlineSource];
    mapView.delegate = self;
    mapView.zoom = 1;
    mapView.minZoom = 1;
    mapView.maxZoom = 4;
    mapView.hideAttribution = YES;
    
    mapView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
    mapView.adjustTilesForRetinaDisplay = YES; // these tiles aren't designed specifically for retina, so make them legible
    
    self.mapboxView = mapView;
    
    [self.mapView addSubview:mapView];
    
    [self pinCoordinates];
}

#pragma mark - Navigation

-(void)goHome
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)singleTapOnMap:(RMMapView *)mapView at:(CGPoint)point
{
//        [mapView removeAllAnnotations];
//    
//        CLLocationCoordinate2D coordinate = [mapView pixelToCoordinate:point];
//        NSString *countryName = [NSString stringWithFormat:@"%f, %f", coordinate.latitude, coordinate.longitude];
//    
//        NSLog(@"latitude %f longitude %f", coordinate.latitude, coordinate.longitude);
//
//    
//        RMAnnotation *annotation = [RMAnnotation annotationWithMapView:mapView coordinate:[mapView pixelToCoordinate:point] andTitle:countryName];
//    
//        [mapView addAnnotation:annotation];
//    
//        [mapView selectAnnotation:annotation animated:YES];
    
}

-(void)pinCoordinates
{
    [self.mapboxView removeAllAnnotations];
    
    if (self.strCoordX.length > 0 && self.strCoordY.length > 0)
    {
        CLLocationDegrees cordinateX = [self.strCoordX doubleValue];
        CLLocationDegrees cordinateY = [self.strCoordY doubleValue];
        
//        NSLog(@"cordinateX %f cordinateY %f", cordinateX, cordinateY);
        
        CLLocationCoordinate2D coordinate  = CLLocationCoordinate2DMake(cordinateX, cordinateY);
        
        RMAnnotation *annotation = [RMAnnotation annotationWithMapView:self.mapboxView coordinate:coordinate andTitle:nil];
        
        [self.mapboxView addAnnotation:annotation];
        
        [self.mapboxView selectAnnotation:annotation animated:YES];
        
        self.mapboxView.maxZoom = 1;
        [self.mapboxView zoomWithLatitudeLongitudeBoundsSouthWest:coordinate northEast:coordinate animated:YES];
        self.mapboxView.maxZoom = 4;
    }
}

- (RMMapLayer *)mapView:(RMMapView *)mapView layerForAnnotation:(RMAnnotation *)annotation
{
    RMMarker *marker = [[RMMarker alloc] initWithMapboxMarkerImage:@"embassy"];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 32)];
    
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    imageView.image = annotation.userInfo;
    
    marker.leftCalloutAccessoryView = imageView;
    
    marker.canShowCallout = YES;
    
    return marker;
}

-(void)changeFloor:(UIBarButtonItem *)sender
{
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@"Choose level:"
                                message:nil
                                preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (NSString *strNumberOfFloor in self.arrayLevels)
    {
        UIAlertAction* btnLevel = [UIAlertAction
                                   actionWithTitle:[[strNumberOfFloor stringByReplacingOccurrencesOfString:@"_" withString:@" "] uppercaseString]
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       // set current level
                                       self.lblCurrentLevel.text = action.title;
                                       
                                       [self.mapboxView removeAllAnnotations];
                                       
                                       NSString *strMap = [[action.title stringByReplacingOccurrencesOfString:@" " withString:@"_"] lowercaseString];
                                       
                                       RMMBTilesSource *offlineSource = [[RMMBTilesSource alloc] initWithTileSetResource:strMap ofType:@"mbtiles"];
                                       [self.mapboxView setTileSource:offlineSource];
                                       
                                       if (self.roomLevel && [strMap isEqualToString:[self.arrayLevels objectAtIndex:[self.roomLevel intValue]]])
                                       {
                                           [self pinCoordinates];
                                       }
                                   }];
        [alert addAction:btnLevel];
    }
    
    if (alert.popoverPresentationController)
    {
        alert.popoverPresentationController.sourceView = self.view;
        alert.popoverPresentationController.sourceRect = self.view.bounds;
        alert.popoverPresentationController.permittedArrowDirections = 0;
        alert.popoverPresentationController.backgroundColor = [UIColor whiteColor];
    }
    else
    {
        UIAlertAction* btnCancel = [UIAlertAction
                                    actionWithTitle:@"Cancel"
                                    style:UIAlertActionStyleDefault
                                    handler:nil];
        [alert addAction:btnCancel];
    }
    
    [self presentViewController:alert animated:YES completion:nil];
}

@end
