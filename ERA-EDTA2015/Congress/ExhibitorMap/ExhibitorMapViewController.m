//
//  ExhibitorMapViewController.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 12/5/16.
//  Copyright © 2016 Navus. All rights reserved.
//

#import "ExhibitorMapViewController.h"

@interface ExhibitorMapViewController ()

@end

@implementation ExhibitorMapViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIButton *but = [UIButton buttonWithType:UIButtonTypeCustom];
    [but setBackgroundImage:[UIImage imageNamed:@"home"] forState:UIControlStateNormal];
    [but setBackgroundImage:[UIImage imageNamed:@"home_pressed"] forState:UIControlStateHighlighted];
    [but addTarget:self action:@selector(goHome) forControlEvents:UIControlEventTouchUpInside];
    but.bounds = CGRectMake(0, 0, 20, 20);
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:but];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    RMMBTilesSource *offlineSource = [[RMMBTilesSource alloc] initWithTileSetResource:@"exhibitorsMap" ofType:@"mbtiles"];
    
    RMMapView *mapView = [[RMMapView alloc] initWithFrame:self.view.bounds andTilesource:offlineSource];
    mapView.delegate = self;
    mapView.zoom = 1;
    mapView.minZoom = 1;
    mapView.maxZoom = 4;
    mapView.hideAttribution = YES;
    
    mapView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
    //mapView.adjustTilesForRetinaDisplay = YES; // these tiles aren't designed specifically for retina, so make them legible
    
    self.mapView = mapView;
    
    if (self.exibitor)
    {
        NSArray *myArray = [self.exibitor.booths allObjects];
        [self pinExibitorBooths:myArray];
    }
    else if (self.talk)
    {
        [self pinTalk:self.talk];
    }
    
    [self.view addSubview:mapView];
}

#pragma mark - Navigation

-(void)goHome
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)pinExibitorBooths:(NSArray*)exibitorBoothsArray
{
    [self.mapView removeAllAnnotations];
    
    //self.mapView.maxZoom = 2;
    
    for(int i = 0; i< [exibitorBoothsArray count]; i++)
    {
        Booth *booth = exibitorBoothsArray[i];
        CLLocationDegrees cordinateX = [booth.bthCoordX doubleValue];
        CLLocationDegrees cordinateY = [booth.bthCoordY doubleValue];
        
        NSLog(@"cordinateX %f cordinateY %f", cordinateX, cordinateY);
        
        CLLocationCoordinate2D coordinate  = CLLocationCoordinate2DMake(cordinateX, cordinateY);
        
        RMAnnotation *annotation = [RMAnnotation annotationWithMapView:self.mapView coordinate:coordinate andTitle:
                                    [NSString stringWithFormat:@"%@", self.exibitor.exhName]];
        
        [self.mapView addAnnotation:annotation];
        
        [self.mapView selectAnnotation:annotation animated:YES];
    }
    
    if (exibitorBoothsArray.count != 0)
    {
        Booth *booth = exibitorBoothsArray[0];
        CLLocationDegrees cordinateX = [booth.bthCoordX doubleValue];
        CLLocationDegrees cordinateY = [booth.bthCoordY doubleValue];
        
        //        NSLog(@"cordinateX %f cordinateY %f", cordinateX, cordinateY);
        
        CLLocationCoordinate2D coordinate  = CLLocationCoordinate2DMake(cordinateX, cordinateY);
        
        [self.mapView zoomWithLatitudeLongitudeBoundsSouthWest:coordinate northEast:coordinate animated:YES];
    }
}

-(void)pinTalk:(Talk *)talk
{
    [self.mapView removeAllAnnotations];
    
    if ([talk.session.venue.venCoordX integerValue] > 0 && [talk.session.venue.venCoordY integerValue] > 0)
    {
        CLLocationDegrees cordinateX = [talk.session.venue.venCoordX doubleValue];
        CLLocationDegrees cordinateY = [talk.session.venue.venCoordY doubleValue];
        
        NSLog(@"cordinateX %f cordinateY %f", cordinateX, cordinateY);
        
        CLLocationCoordinate2D coordinate  = CLLocationCoordinate2DMake(cordinateX, cordinateY);
        
        RMAnnotation *annotation = [RMAnnotation annotationWithMapView:self.mapView coordinate:coordinate andTitle:nil];
        
        [self.mapView addAnnotation:annotation];
        
        [self.mapView selectAnnotation:annotation animated:YES];
        
        self.mapView.maxZoom = 1;
        [self.mapView zoomWithLatitudeLongitudeBoundsSouthWest:coordinate northEast:coordinate animated:YES];
        self.mapView.maxZoom = 4;
    }
}

- (void)singleTapOnMap:(RMMapView *)mapView at:(CGPoint)point
{
//        [mapView removeAllAnnotations];
//    
//        CLLocationCoordinate2D coordinate = [mapView pixelToCoordinate:point];
//        NSString *countryName = [NSString stringWithFormat:@"%f, %f", coordinate.latitude, coordinate.longitude];
//    
//        NSLog(@"latitude %f longitude %f", coordinate.latitude, coordinate.longitude);
//    
//        RMAnnotation *annotation = [RMAnnotation annotationWithMapView:mapView coordinate:[mapView pixelToCoordinate:point] andTitle:countryName];
//    
//        [mapView addAnnotation:annotation];
//    
//        [mapView selectAnnotation:annotation animated:YES];
}

- (RMMapLayer *)mapView:(RMMapView *)mapView layerForAnnotation:(RMAnnotation *)annotation
{
    RMMarker *marker = [[RMMarker alloc] initWithMapboxMarkerImage:@"embassy"];
    //    RMMarker *marker = [[RMMarker alloc] initWithUIImage:[UIImage imageNamed:@"room"]];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 32)];
    
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    imageView.image = annotation.userInfo;
    
    marker.leftCalloutAccessoryView = imageView;
    
    marker.canShowCallout = YES;
    
    return marker;
}

@end
