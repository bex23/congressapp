//
//  ExhibitorMapViewController.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 12/5/16.
//  Copyright © 2016 Navus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Mapbox.h"

@interface ExhibitorMapViewController : UIViewController <RMMapViewDelegate>

@property (nonatomic, strong) RMMapView* mapView;
@property (strong, nonatomic) Exhibitor* exibitor;
@property (strong, nonatomic) Talk *talk;


@end
