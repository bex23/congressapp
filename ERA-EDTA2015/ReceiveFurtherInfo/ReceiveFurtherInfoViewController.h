//
//  ReceiveFurtherInfoViewController.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 9/22/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ReceiveFurtherInfoViewController;

@protocol ReceiveFurtherInfoViewControllerDelegate <NSObject>

-(void)didCancelReceiveFurtherInfoViewController:(ReceiveFurtherInfoViewController *)vc;

@end

@interface ReceiveFurtherInfoViewController : UIViewController

@property (strong, nonatomic) id <ReceiveFurtherInfoViewControllerDelegate> delegate;

@end
