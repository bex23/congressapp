//
//  ReceiveFurtherInfoViewController.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 12/2/16.
//  Copyright © 2016 Navus. All rights reserved.
//

#import "ReceiveFurtherInfoViewController.h"

@interface ReceiveFurtherInfoViewController ()

@property (weak, nonatomic) IBOutlet UISwitch *switcherContact;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblInvitationBold;
@property (weak, nonatomic) IBOutlet UILabel *lblPermission;
@property (weak, nonatomic) IBOutlet UILabel *lblConsent;

@property (weak, nonatomic) IBOutlet UIButton *btnOnSwitch;

@property (weak, nonatomic) IBOutlet UIImageView *imgLetter;

@end

@implementation ReceiveFurtherInfoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Receive Further Information";
    
    UIBarButtonItem *navBar = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(actionCancel)];
    self.navigationItem.leftBarButtonItem = navBar;
    
    self.lblTitle.text = [NSString stringWithFormat:@"Welcome to %@’s e-Service", kSponsorName];
    
    self.lblInvitationBold.text = [NSString stringWithFormat:@"%@ herewith invites you to sign up to %@’s e-Service.", kSponsorName, kSponsorName];
    
    self.lblPermission.text = [NSString stringWithFormat:@"With your permission, %@ would like to contact you by e-mail for the purpose of providing you with additional information about %@’s pharmaceutical products. Your personal data (Name, email address) will be used by %@ and its representatives only for such purpose.", kSponsorName, kSponsorName, kSponsorName];
    
    self.lblConsent.text = [NSString stringWithFormat:@"I herewith consent to be contacted by %@ via e-mail with information about %@`s products.", kSponsorName, kSponsorName];
    
    [self.imgLetter colorItWithColor:[UIColor colorWithHex:kGlobalColor andAlpha:1.0]];
}

#pragma mark - IBActions

- (IBAction)switchContact:(UISwitch *)sender
{
    [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"opt-in"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[ESyncData sharedInstance] setUserAction:@"opt-in"];
    
    if ([self.delegate respondsToSelector:@selector(didCancelReceiveFurtherInfoViewController:)])
    {
        [self.delegate didCancelReceiveFurtherInfoViewController:self];
    }
}

- (void)actionCancel
{
    if ([self.delegate respondsToSelector:@selector(didCancelReceiveFurtherInfoViewController:)])
    {
        [self.delegate didCancelReceiveFurtherInfoViewController:self];
    }
}

@end

