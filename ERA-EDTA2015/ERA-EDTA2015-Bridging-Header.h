//
//  ERA-EDTA2015-Bridging-Header.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 9/14/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#ifndef ERA_EDTA2015_Bridging_Header_h
#define ERA_EDTA2015_Bridging_Header_h

#import <UIKit/UIKit.h>

#import "Constants.h"
#import "Utils.h"
#import "UIView+HTML.h"
#import "UIColor+HEX2RGB.h"
#import "Constants.h"
#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>
#import "Talk+CoreDataProperties.h"
#import "Talk+CoreDataClass.h"
#import "Session+CoreDataClass.h"
#import "Session+CoreDataProperties.h"
#import "Venue+CoreDataClass.h"
#import "Venue+CoreDataProperties.h"
#import "Exhibitor+CoreDataClass.h"
#import "Exhibitor+CoreDataProperties.h"
#import "ExhibitorRole+CoreDataClass.h"
#import "ExhibitorRole+CoreDataProperties.h"
#import "Venue+CoreDataClass.h"
#import "Venue+CoreDataProperties.h"
#import <BOTNetworkActivityIndicator/BOTNetworkActivityIndicator.h>
#import "ESyncData.h"
#import "LLDataAccessLayer.h"
#import "Country+CoreDataClass.h"
#import "Country+CoreDataProperties.h"
#import "HighlightedSlide+CoreDataClass.h"
#import "HighlightedSlide+CoreDataProperties.h"
#import "HighlightedSlideComment+CoreDataClass.h"
#import "HighlightedSlideComment+CoreDataProperties.h"
#import "CUtils.h"
#import "AppDelegate.h"
#import "AboutViewController.h"
#import "PresentationDetailsViewController.h"
#import "GlobalConstants.h"
#import "UIBarButtonItem+Badge.h"

#endif /* ERA_EDTA2015_Bridging_Header_h */
