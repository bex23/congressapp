//
//  Utils.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 9/21/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "Utils.h"

@implementation Utils

+(NSString *)getDate:(NSString *)datetime{
    NSArray *array = [datetime componentsSeparatedByString:@" "];
    if(array.count == 2)
        return [array objectAtIndex:0];
    else
        // No date available
        return @"";
    //        return @"No date available";
}

+(NSString *)getTime:(NSString *)datetime{
    NSArray *array = [datetime componentsSeparatedByString:@" "];
    if(array.count == 2)
        return [array objectAtIndex:1];
    else
        // No time available
        return @"";
    //        return @"No time available";
    
}

+(NSDate *)dateFromString:(NSString *)datetime{
    
    NSArray *comps = [datetime componentsSeparatedByString:@" "];
    NSArray *dateComps;
    NSArray *timeComps;
    if(comps.count == 2){
        dateComps = [[comps objectAtIndex:0]componentsSeparatedByString:@"-"];
        timeComps = [[comps objectAtIndex:1]componentsSeparatedByString:@":"];
    }else{
        return nil;
    }
    
    NSCalendar * gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    int flags = (NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond);
    NSDateComponents * componentsOther = [gregorian components:flags fromDate:[NSDate date]];
    componentsOther.year = [[dateComps objectAtIndex:0]intValue];
    componentsOther.month = [[dateComps objectAtIndex:1]intValue];
    componentsOther.day = [[dateComps objectAtIndex:2]intValue];
    componentsOther.hour = [[timeComps objectAtIndex:0]intValue];
    componentsOther.minute = [[timeComps objectAtIndex:1]intValue];
    if(timeComps.count > 2)
    {
        componentsOther.second = [[timeComps objectAtIndex:2]intValue];
    }
    NSDate *date = [gregorian dateFromComponents:componentsOther];
    
    return date;
}

+(BOOL)isStringDateToday:(NSString*) otherDayString
{
    NSDateFormatter *dateFormat1 = [[NSDateFormatter alloc] init];
    // this is imporant - we set our input date format to match our input string
    // if format doesn't match you'll get nil from your string, so be careful
    [dateFormat1 setDateFormat:@"yyyy-MM-dd"];
    
    NSDate *dateFromString = [dateFormat1 dateFromString:otherDayString];
    
    NSCalendar * gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents * components = [gregorian components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:[NSDate date]];
    NSDate * today = [gregorian dateFromComponents:components];
    
    components = [gregorian components: (NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:dateFromString];
    NSDate * otherDay = [gregorian dateFromComponents:components];
    
    if([today isEqualToDate:otherDay])
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

+(NSString*)formatDateString:(NSString *)dateString
{
    //    NSString *dateString = @"01-02-2010";
    NSDateFormatter *dateFormat1 = [[NSDateFormatter alloc] init];
    // this is imporant - we set our input date format to match our input string
    // if format doesn't match you'll get nil from your string, so be careful
    [dateFormat1 setDateFormat:@"yyyy-MM-dd"];
    
    NSDate *dateFromString = [dateFormat1 dateFromString:dateString];
    
    NSDateFormatter *dateFormat2 = [[NSDateFormatter alloc] init];
    [dateFormat2 setDateFormat:kDateFormat];
    NSString *strDate = [dateFormat2 stringFromDate:dateFromString];
    
    return strDate;
}

+(NSString*)formatDateString:(NSString *)dateString fromFormat:(NSString *)fromFormatString toFormat:(NSString *)toFormatString
{
    //    NSString *dateString = @"01-02-2010";
    NSDateFormatter *dateFormat1 = [[NSDateFormatter alloc] init];
    // this is imporant - we set our input date format to match our input string
    // if format doesn't match you'll get nil from your string, so be careful
    [dateFormat1 setDateFormat:fromFormatString];
    if (kCheckTimeHourFormatInSettings == NO)
    {
        dateFormat1.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    }
    
    NSDate *dateFromString = [dateFormat1 dateFromString:dateString];
    
    NSDateFormatter *dateFormat2 = [[NSDateFormatter alloc] init];
    [dateFormat2 setDateFormat:toFormatString];
    if (kCheckTimeHourFormatInSettings == NO)
    {
        dateFormat2.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    }

    NSString *strDate = [dateFormat2 stringFromDate:dateFromString];
    
    return strDate;
}

+(NSString*)formatTimeString:(NSString *)timeString
{
    NSString* formatedString = timeString;

    NSArray* timeComponents = [timeString componentsSeparatedByString:@":"];

    if(timeComponents.count > 1)
        formatedString = [NSString stringWithFormat:@"%@:%@", [timeComponents objectAtIndex:0], [timeComponents objectAtIndex:1]];

    return formatedString;
}

+(NSString*)formatTimeStringFromDate:(NSString *)dateString
{
    //    NSString *dateString = @"01-02-2010";
    NSDateFormatter *dateFormat1 = [[NSDateFormatter alloc] init];
    // this is imporant - we set our input date format to match our input string
    // if format doesn't match you'll get nil from your string, so be careful
    [dateFormat1 setDateFormat:kDateFormatInDatabase];
    
    NSDate *dateFromString = [dateFormat1 dateFromString:dateString];
    
    NSDateFormatter *dateFormat2 = [[NSDateFormatter alloc] init];
    
    if (kCheckTimeHourFormatInSettings == NO)
    {
        dateFormat2.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    }
    
    [dateFormat2 setDateFormat:kTimeFormat];
    NSString *strDate = [dateFormat2 stringFromDate:dateFromString];

    return strDate;
}

+(NSString*)formatDayString:(NSString*)dateString
{
    //    NSString *dateString = @"01-02-2010";
    NSDateFormatter *dateFormat1 = [[NSDateFormatter alloc] init];
    // this is imporant - we set our input date format to match our input string
    // if format doesn't match you'll get nil from your string, so be careful
    [dateFormat1 setDateFormat:@"yyyy-MM-dd"];
    // voila!
    NSDate *dateFromString = [dateFormat1 dateFromString:dateString];
    
    NSDateFormatter *dateFormat2 = [[NSDateFormatter alloc] init];
    [dateFormat2 setDateFormat:@"MMM dd"];
    NSString *strDate = [dateFormat2 stringFromDate:dateFromString];
    
    return [strDate uppercaseString];
}

+(BOOL)checkEmailIsValid:(NSString *)email
{
    BOOL stricterFilter = YES; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

+ (UIViewController*)topViewController {
    return [self topViewControllerWithRootViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

+ (UIViewController*)topViewControllerWithRootViewController:(UIViewController*)rootViewController
{
    if ([rootViewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController* tabBarController = (UITabBarController*)rootViewController;
        return [self topViewControllerWithRootViewController:tabBarController.selectedViewController];
    } else if ([rootViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController* navigationController = (UINavigationController*)rootViewController;
        return [self topViewControllerWithRootViewController:navigationController.visibleViewController];
    } else if (rootViewController.presentedViewController) {
        UIViewController* presentedViewController = rootViewController.presentedViewController;
        return [self topViewControllerWithRootViewController:presentedViewController];
    } else {
        return rootViewController;
    }
}

+(void)showMessage:(NSString *)message
{
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@""
                                message:message
                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* btnCancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:btnCancel];
    
    [[self topViewController] presentViewController:alert animated:YES completion:nil];
}

+(void)showMessage:(NSString *)message withTitle:(NSString *)title
{
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:title
                                message:message
                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* btnCancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:btnCancel];
    
    [[self topViewController] presentViewController:alert animated:YES completion:nil];
}

+(NSString *)getDevicUUID
{
    NSString *devicUUID = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    return devicUUID;
}

+ (CGSize)findHeightForText:(NSString *)text havingWidth:(CGFloat)widthValue andFont:(UIFont *)font {
    CGSize size = CGSizeZero;
    if (text) {
        //iOS 7
        CGRect frame = [text boundingRectWithSize:CGSizeMake(widthValue, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName:font } context:nil];
        size = CGSizeMake(frame.size.width, frame.size.height + 1);
    }
    return size;
}

@end
