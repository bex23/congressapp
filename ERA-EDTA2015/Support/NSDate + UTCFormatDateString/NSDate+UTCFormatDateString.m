//
//  NSDate+UTCFormatDateString.m
//  ERA-EDTA2015
//
//  Created by Predrag Despotovic on 5/6/15.
//  Copyright (c) 2015 Navus. All rights reserved.
//

#import "NSDate+UTCFormatDateString.h"

@implementation NSDate (UTCFormatDateString)
+(NSString*)localDateStringFromUTCDateString:(NSString*)serverTime withDateFormat:(NSString*)dateFormat
{
    NSTimeZone *inputTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    NSDateFormatter *inputDateFormatter = [[NSDateFormatter alloc] init];
    [inputDateFormatter setTimeZone:inputTimeZone];
    NSString *dateFormatUTC = @"YYYY-MM-dd'T'HH:mm:ssZZZ";
    [inputDateFormatter setDateFormat:dateFormatUTC];
    NSDate *date = [inputDateFormatter dateFromString:serverTime];
    
    NSTimeZone *outputTimeZone = [NSTimeZone localTimeZone];
    NSDateFormatter *outputDateFormatter = [[NSDateFormatter alloc] init];
    [outputDateFormatter setTimeZone:outputTimeZone];
    [outputDateFormatter setDateFormat:dateFormat];
    NSString *outputString = [outputDateFormatter stringFromDate:date];
    
    return outputString;
}
//
//+(NSString *)UTCFormatDateStringFromDate:(NSDate *)localDate
//{
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
//    [dateFormatter setTimeZone:timeZone];
//    [dateFormatter setDateFormat:@"YYYY-MM-dd'T'HH:mm:ssZZZ"];
//    NSString *dateString = [dateFormatter stringFromDate:localDate];
//    return dateString;
//}


@end
