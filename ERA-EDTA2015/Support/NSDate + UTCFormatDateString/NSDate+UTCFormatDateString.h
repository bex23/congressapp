//
//  NSDate+UTCFormatDateString.h
//  ERA-EDTA2015
//
//  Created by Predrag Despotovic on 5/6/15.
//  Copyright (c) 2015 Navus. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (UTCFormatDateString)
+(NSString*)localDateStringFromUTCDateString:(NSString*)serverTime withDateFormat:(NSString*)dateFormat;
//+(NSString *)UTCFormatDateStringFromDate:(NSDate *)localDate;
@end
