//
//  UIImageView+Color.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 4/3/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (Color)

-(void)colorItWithColor:(UIColor *)color;

@end
