//
//  UIImageView+Color.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 4/3/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "UIImageView+Color.h"

@implementation UIImageView (Color)

-(void)colorItWithColor:(UIColor *)color;
{
    self.image = [self.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self setTintColor:color];
}

@end
