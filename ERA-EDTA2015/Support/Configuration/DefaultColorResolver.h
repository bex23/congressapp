//
//  DefaultColorResolver.h
//  Otpbank
//
//  Created by Igor Bespaljko on 5/15/14.
//  Copyright (c) 2014 Igor Bespaljko. All rights reserved.
//  Copyright (c) 2015 Boris Kekić. All rights reserved.
//  Copyright (c) 2015 Predrag Despotović. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ColorResolverDelegate.h"

@interface DefaultColorResolver : NSObject <ColorResolverDelegate>

@end
