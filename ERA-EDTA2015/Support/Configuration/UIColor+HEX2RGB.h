//
//  UIColor+HEX2RGB.h
//  iBossToken
//
//  Created by Igor Bespaljko on 1/16/14.
//
//

#import <Foundation/Foundation.h>

@interface UIColor (HEX2RGB)

+ (UIColor *) colorWithHex:(NSString *)_hexColorCode andAlpha:(CGFloat)_alpha;

@end
