//
//  UIView+StoryboardAttributes.h
//  Otpbank
//
//  Created by Valentin Rep on 27/05/14.
//  Copyright (c) 2014 Igor Bespaljko. All rights reserved.
//  Copyright (c) 2015 Boris Kekić. All rights reserved.
//  Copyright (c) 2015 Predrag Despotović. All rights reserved.
//

#import <UIKit/UIKit.h>


/**
 *  Config User Defined Runtime Attributes in your Storyboard for any UIView subclass
 *
 *	e.g.
 *	KEY PATH				TYPE			VALUE
 *	configFont				String			FONT_FLOATING_LABEL
 *	configText				String			calculator_loan_lbl_interest_rate
 *	configTextColor			String			COLOR_TEXT_3
 */
@interface UIView (StoryboardAttributes)

/**
 *  setBackgroundColor:
 */
@property (nonatomic, weak) NSString *configBackgroundColor;

/**
 *  setText:
 */
@property (nonatomic, weak) NSString *configText;

/**
 *  setPlaceholder:
 */
@property (nonatomic, weak) NSString *configPlaceholder;

/**
 *  setTextColor:
 */
@property (nonatomic, weak) NSString *configTextColor;

/**
 *  setHighlightedTextColor:
 */
@property (nonatomic, weak) NSString *configHighlightedTextColor;

/**
 *  setFont:
 */
@property (nonatomic, weak) NSString *configFont;

/**
 *  setMinimumTrackTintColor:
 */
@property (nonatomic, weak) NSString *configMinimumTrackTintColor;

/**
 *  setMaximumTrackTintColor:
 */
@property (nonatomic, weak) NSString *configMaximumTrackTintColor;

//pedja:
//set tint color (e.g. for segmented control):
@property (nonatomic, weak) NSString* configTintColor;
@property (nonatomic, weak) NSString* configBorderColor;
@property (nonatomic, weak) NSNumber* configCornerRadius;
@property (nonatomic, weak) NSNumber* configBorderWidth;

@end
