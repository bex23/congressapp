//
//  UIConfiguration.h
//  Otpbank
//
//  Created by Igor Bespaljko on 5/15/14.
//  Copyright (c) 2014 Igor Bespaljko. All rights reserved.
//  Copyright (c) 2015 Boris Kekić. All rights reserved.
//  Copyright (c) 2015 Predrag Despotović. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FontResolverDelegate.h"
#import "ColorResolverDelegate.h"

//define color constants
static NSString *GLOBAL_COLOR = @"GLOBAL_COLOR";
static NSString *OTHER_COLOR = @"OTHER_COLOR";
static NSString *SILVER_COLOR = @"SILVER_COLOR";

//static NSString *COLOR_BACKGROUND = @"COLOR_BACKGROUND";
//static NSString *COLOR_SEPARATOR_DARK = @"COLOR_SEPARATOR_DARK";
//
////define font constants
//static NSString *FONT_EXTRA_SMALL_REGULAR = @"FONT_EXTRA_SMALL_REGULAR";
//static NSString *FONT_PLACEHOLDER_SMALL_REGULAR = @"FONT_PLACEHOLDER_SMALL_REGULAR";
//static NSString *FONT_EXCHANGE_SMALL_REGULAR_CONDESED = @"FONT_EXCHANGE_SMALL_REGULAR_CONDESED";
//
//static NSString *FONT_SMALL_LIGHT = @"FONT_SMALL_LIGHT";
//static NSString *FONT_EXTRA_SMALL_LIGHT = @"FONT_EXTRA_SMALL_LIGHT";
//static NSString *FONT_SMALL_REGULAR = @"FONT_SMALL_REGULAR";
//static NSString *FONT_SMALL_REGULAR_CONDESED = @"FONT_SMALL_REGULAR_CONDESED";
//static NSString *FONT_SMALL_BOLD = @"FONT_SMALL_BOLD";
//
//static NSString *FONT_NORMAL_LIGHT = @"FONT_NORMAL_LIGHT";
//static NSString *FONT_NORMAL_REGULAR = @"FONT_NORMAL_REGULAR";
//static NSString *FONT_NORMAL_SEMIBOLD = @"FONT_NORMAL_SEMIBOLD";
//static NSString *FONT_NORMAL_SEMIBOLD_CONDESED = @"FONT_NORMAL_SEMIBOLD_CONDESED";
//static NSString *FONT_NAV_NORMAL_LIGHT = @"FONT_NAV_NORMAL_LIGHT";
//static NSString *FONT_NAV_NORMAL_SEMIBOLD = @"FONT_NAV_NORMAL_SEMIBOLD";
//
//static NSString *FONT_LARGE_BOLD = @"FONT_LARGE_BOLD";
//static NSString *FONT_LARGE_LIGHT = @"FONT_LARGE_LIGHT";
//static NSString *FONT_EXTRA_LARGE_LIGHT = @"FONT_EXTRA_LARGE_LIGHT";
//
//static NSString *FONT_LABEL = @"FONT_LABEL";
//static NSString *FONT_FLOATING_LABEL = @"FONT_FLOATING_LABEL";
//
//static NSString *FONT_EXTRA_SMALL_REGULAR_CONDESED = @"FONT_EXTRA_SMALL_REGULAR_CONDESED";

@interface UIConfiguration : NSObject

@property (nonatomic, strong) id<FontResolverDelegate> fontResolver;
@property (nonatomic, strong) id<ColorResolverDelegate> colorResolver;

+ (UIConfiguration *)sharedInstance;

- (UIFont *) getFont:(NSString *) fontName;
- (UIColor *) getColor:(NSString *) colorName;
+ (UIStatusBarStyle) getStatusBarStyle;
- (void) setupNavigationBar:(UINavigationBar *) navigationBar translucent:(BOOL) translucent;
@end
