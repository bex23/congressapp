//
//  DefaultColorResolver.m
//  Otpbank
//
//  Created by Igor Bespaljko on 5/15/14.
//  Copyright (c) 2014 Igor Bespaljko. All rights reserved.
//  Copyright (c) 2015 Boris Kekić. All rights reserved.
//  Copyright (c) 2015 Predrag Despotović. All rights reserved.
//

#import "DefaultColorResolver.h"
#import "UIConfiguration.h"
#import "UIColor+HEX2RGB.h"

@interface DefaultColorResolver ()

@property (nonatomic, strong) NSDictionary *colorMap;

@end

@implementation DefaultColorResolver

- (UIColor *) getColorNamed:(NSString *)colorName{
    
    UIColor *color = (UIColor *)[self.colorMap objectForKey:colorName];
    return color ? color : [UIColor blackColor];
}

- (NSDictionary *) colorMap{
    
    if(!_colorMap){
        
        [self initColorMap];
    }
    
    return _colorMap;
}

- (void) initColorMap{
    
    NSMutableDictionary *map = [[NSMutableDictionary alloc] init];

    [map setValue:[UIColor colorWithHex:kGlobalColor andAlpha:1.0] forKey:GLOBAL_COLOR];
    [map setValue:[UIColor colorWithHex:kOtherColor andAlpha:1.0] forKey:OTHER_COLOR];
    [map setValue:[UIColor colorWithHex:kDefaultSilverColor andAlpha:1.0] forKey:SILVER_COLOR];
    
    _colorMap = map;
}

@end
