//
//  UIView+StoryboardAttributes.m
//  Otpbank
//
//  Created by Valentin Rep on 27/05/14.
//  Copyright (c) 2014 Igor Bespaljko. All rights reserved.
//  Copyright (c) 2015 Boris Kekić. All rights reserved.
//  Copyright (c) 2015 Predrag Despotović. All rights reserved.
//

#import "UIView+StoryboardAttributes.h"

#import "UIConfiguration.h"


@implementation UIView (StoryboardAttributes)

@dynamic configBackgroundColor;
@dynamic configText;
@dynamic configPlaceholder;
@dynamic configTextColor;
@dynamic configHighlightedTextColor;
@dynamic configFont;
@dynamic configMinimumTrackTintColor;
@dynamic configMaximumTrackTintColor;
@dynamic configTintColor;
@dynamic configBorderColor;
@dynamic configCornerRadius;
@dynamic configBorderWidth;


- (void)setConfigBackgroundColor:(NSString *)configBackgroundColor
{
	if ( [self respondsToSelector:@selector(setBackgroundColor:)] )
	{
		UIColor *color = [[UIConfiguration sharedInstance] getColor:configBackgroundColor];
		[self setBackgroundColor:color];
	}
}

- (void)setConfigText:(NSString *)configText
{
//	if ( [self respondsToSelector:@selector(setText:)] )
//	{
//		[(id)self setText:[Localized string:configText]];
//	}
}

- (void)setConfigPlaceholder:(NSString *)configPlaceholder
{
//	if ( [self respondsToSelector:@selector(setPlaceholder:)] )
//	{
//		[(id)self setPlaceholder:[Localized string:configPlaceholder]];
//	}
}

- (void)setConfigTextColor:(NSString *)configTextColor
{
	if ( [self respondsToSelector:@selector(setTextColor:)] )
	{
		UIColor *color = [[UIConfiguration sharedInstance] getColor:configTextColor];
		[(id)self setTextColor:color];
	}
}

- (void)setConfigHighlightedTextColor:(NSString *)configHighlightedTextColor
{
	if ( [self respondsToSelector:@selector(setHighlightedTextColor:)] )
	{
		UIColor *color = [[UIConfiguration sharedInstance] getColor:configHighlightedTextColor];
		[(id)self setHighlightedTextColor:color];
	}
}

- (void)setConfigFont:(NSString *)configFont
{
	if ( [self respondsToSelector:@selector(setFont:)] )
	{
		UIFont *font = [[UIConfiguration sharedInstance] getFont:configFont];
		[(id)self setFont:font];
	}
}

- (void)setConfigMinimumTrackTintColor:(NSString *)configMinimumTrackTintColor
{
	if ( [self respondsToSelector:@selector(setMinimumTrackTintColor:)])
	{
		UIColor *color = [[UIConfiguration sharedInstance] getColor:configMinimumTrackTintColor];
		[(id)self setMinimumTrackTintColor:color];
	}
	
	if ( [self respondsToSelector:@selector(setProgressTintColor:)])
	{
		UIColor *color = [[UIConfiguration sharedInstance] getColor:configMinimumTrackTintColor];
		[(id)self setProgressTintColor:color];
	}
}

- (void)setConfigMaximumTrackTintColor:(NSString *)configMaximumTrackTintColor
{
	if ( [self respondsToSelector:@selector(setMaximumTrackTintColor:)])
	{
		UIColor *color = [[UIConfiguration sharedInstance] getColor:configMaximumTrackTintColor];
		[(id)self setMaximumTrackTintColor:color];
	}
	
	if ( [self respondsToSelector:@selector(setTrackTintColor:)])
	{
		UIColor *color = [[UIConfiguration sharedInstance] getColor:configMaximumTrackTintColor];
		[(id)self setTrackTintColor:color];
	}
}

-(void) setConfigTintColor:(NSString *)configTintColor
{
    if ( [self respondsToSelector:@selector(setTintColor:)])
    {
        UIColor *color = [[UIConfiguration sharedInstance] getColor:configTintColor];
        [(id)self setTintColor:color];
    }
}

-(void) setConfigBorderColor:(NSString *)configBorderColor
{
    if ( [self.layer respondsToSelector:@selector(setBorderColor:)])
    {
        UIColor* color = [[UIConfiguration sharedInstance] getColor:configBorderColor];
        [(id)self.layer setBorderColor:color.CGColor];
    }
}

-(void) setConfigCornerRadius:(NSNumber *)configCornerRadius
{
    if ( [self.layer respondsToSelector:@selector(setCornerRadius:)])
    {
        [(id)self.layer setCornerRadius:[configCornerRadius floatValue]];
    }
}

//-(void) setConfigBorderWidth:(NSNumber *)configBorderWidth
//{
//    if ( [self.layer respondsToSelector:@selector(setBorderWidth:)])
//    {
//        [(id)self.layer setBorderWidth:[configBorderWidth floatValue]];
//    }
//}
@end
