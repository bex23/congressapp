//
//  DefaultFontResolver.m
//  Otpbank
//
//  Created by Igor Bespaljko on 5/15/14.
//  Copyright (c) 2014 Igor Bespaljko. All rights reserved.
//  Copyright (c) 2015 Boris Kekić. All rights reserved.
//  Copyright (c) 2015 Predrag Despotović. All rights reserved.
//

#import "DefaultFontResolver.h"
#import "UIConfiguration.h"

@interface DefaultFontResolver ()

@property (nonatomic, strong) NSDictionary *fontMap;
@end

@implementation DefaultFontResolver

- (UIFont *) getFontNamed:(NSString *)fontName{
    
     return (UIFont *)[self.fontMap objectForKey:fontName];

}

- (NSDictionary *) fontMap{
    
    if(!_fontMap){
        
        [self initFontMap];
    }
    
    return _fontMap;
}

- (void) initFontMap{
    
    NSMutableDictionary *map = [[NSMutableDictionary alloc] init];
    
//    [map setValue:[UIFont fontWithName:@"OpenSans" size:12.0] forKey:FONT_EXTRA_SMALL_REGULAR];//??
//    [map setValue:[UIFont fontWithName:@"OpenSans" size:14.0] forKey:FONT_PLACEHOLDER_SMALL_REGULAR];
//    [map setValue:[UIFont fontWithName:@"HelveticaNeueLTCom-Cn" size:13.0] forKey:FONT_EXCHANGE_SMALL_REGULAR_CONDESED];
//
//    [map setValue:[UIFont fontWithName:@"OpenSans-Light" size:16.0] forKey:FONT_SMALL_LIGHT];
//    [map setValue:[UIFont fontWithName:@"OpenSans-Light" size:12.0] forKey:FONT_EXTRA_SMALL_LIGHT];
//    [map setValue:[UIFont fontWithName:@"OpenSans" size:16.0] forKey:FONT_SMALL_REGULAR];//??
//    [map setValue:[UIFont fontWithName:@"HelveticaNeueLTCom-Cn" size:16.0] forKey:FONT_SMALL_REGULAR_CONDESED];
//    [map setValue:[UIFont fontWithName:@"OpenSans-Bold" size:16.0] forKey:FONT_SMALL_BOLD];
//    
//    [map setValue:[UIFont fontWithName:@"OpenSans-Light" size:18.0] forKey:FONT_NORMAL_LIGHT];
//    [map setValue:[UIFont fontWithName:@"OpenSans" size:18.0] forKey:FONT_NORMAL_REGULAR];
//    [map setValue:[UIFont fontWithName:@"OpenSans-Semibold" size:18.0] forKey:FONT_NORMAL_SEMIBOLD];
//    [map setValue:[UIFont fontWithName:@"HelveticaNeueLTCom-Cn" size:18.0] forKey:FONT_NORMAL_SEMIBOLD_CONDESED];
//    [map setValue:[UIFont fontWithName:@"OpenSans-Light" size:18.0] forKey:FONT_NAV_NORMAL_LIGHT];
//    [map setValue:[UIFont fontWithName:@"OpenSans-Semibold" size:18.0] forKey:FONT_NAV_NORMAL_SEMIBOLD];
//    
//    [map setValue:[UIFont fontWithName:@"OpenSans-Bold" size:22.0] forKey:FONT_LARGE_BOLD];
//    [map setValue:[UIFont fontWithName:@"OpenSans-Light" size:22.0] forKey:FONT_LARGE_LIGHT];
//    
//    [map setValue:[UIFont fontWithName:@"HelveticaNeueLTCom-Th" size:30.0] forKey:FONT_EXTRA_LARGE_LIGHT];
//    
//    [map setValue:[UIFont fontWithName:@"OpenSans-Bold" size:14.0] forKey:FONT_LABEL];
//    
//    [map setValue:[UIFont fontWithName:@"OpenSans-Semibold" size:12.0] forKey:FONT_FLOATING_LABEL];
//    
//    [map setValue:[UIFont fontWithName:@"HelveticaNeueLTCom-Cn" size:10.0] forKey:FONT_EXTRA_SMALL_REGULAR_CONDESED];
    
    _fontMap = map;
}

@end
