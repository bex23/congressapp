//
//  UIConfiguration.m
//  Otpbank
//
//  Created by Igor Bespaljko on 5/15/14.
//  Copyright (c) 2014 Igor Bespaljko. All rights reserved.
//  Copyright (c) 2015 Boris Kekić. All rights reserved.
//  Copyright (c) 2015 Predrag Despotović. All rights reserved.
//

#import "UIConfiguration.h"
#import "DefaultColorResolver.h"
#import "DefaultFontResolver.h"

@interface UIConfiguration ()

@end

@implementation UIConfiguration


#pragma mark-
#pragma mark Public API
#pragma mark-
+ (UIConfiguration *)sharedInstance {
    static UIConfiguration *sharedInstance = nil;
    static dispatch_once_t onceToken; // onceToken = 0
    dispatch_once(&onceToken, ^{
        sharedInstance = [[UIConfiguration alloc] init];
    });
    
    return sharedInstance;
}


- (UIFont *) getFont:(NSString *) fontName{
    
    return [self.fontResolver getFontNamed:fontName];
}

- (UIColor *) getColor:(NSString *) colorName{
    
    return [self.colorResolver getColorNamed:colorName];
}

- (void) setupNavigationBar:(UINavigationBar *) navigationBar translucent:(BOOL) translucent{
    
//    navigationBar.translucent = translucent;
    
//    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
//        [navigationBar setBarTintColor:[[UIConfiguration sharedInstance] getColor:COLOR_SHAPE_1]];
//        [navigationBar setTintColor:[[UIConfiguration sharedInstance] getColor:COLOR_TEXT_4]];
//        navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[[UIConfiguration sharedInstance] getColor:COLOR_TEXT_4]};
//        
//        switch ([UIConfiguration getStatusBarStyle]) {
//            case UIStatusBarStyleLightContent:
//                [navigationBar setBarStyle:UIBarStyleBlack];
//                break;
//                
//            default:
//                break;
//        }
//    }else{
//        [navigationBar setTintColor:[[UIConfiguration sharedInstance] getColor:COLOR_SHAPE_1]];
//    }
}

+ (UIStatusBarStyle) getStatusBarStyle{
    
    NSString *style = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"UIStatusBarStyle"];
    if(style){
        
        if([style isEqualToString:@"UIStatusBarStyleLightContent"]){
            return UIStatusBarStyleLightContent;
        }else{
            return UIStatusBarStyleDefault;
        }
    }
    
    return UIStatusBarStyleDefault;
}

#pragma mark-
#pragma mark Override getters
#pragma mark-
- (id<FontResolverDelegate>) fontResolver{
    
    if(!_fontResolver){
        
        _fontResolver = (id<FontResolverDelegate>)[[DefaultFontResolver alloc] init];
    }
    
    return _fontResolver;
}

- (id<ColorResolverDelegate>) colorResolver{
    
    if(!_colorResolver){
        
        _colorResolver = (id<ColorResolverDelegate>)[[DefaultColorResolver alloc] init];
    }
    
    return _colorResolver;
}
@end
