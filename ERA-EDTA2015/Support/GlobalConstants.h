//
//  DefaultConstants.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 11.2.16..
//  Copyright © 2016. Navus. All rights reserved.
//

#ifndef GlobalConstants_h
#define GlobalConstants_h

// URLs
static NSString *const kBaseAPIURL = @"https://service.e-materials.com/api/";
static NSString *const kURLZip = @"https://service.e-materials.com/data/";
//static NSString *const kBaseAPIURL = @"http://staging.e-materials.com/api/";
//static NSString *const kURLZip = @"http://staging.e-materials.com/data/";

// List of presentations

static NSString *const kAvailable = @"kAvailable";
static NSString *const kMyLibrary = @"kMyLibrary";

// Indicators

static NSString *const kEmaterialsAvailable     = @"ematerials_available";
static NSString *const kEmaterialsUnavailable   = @"ematerials_unavailable";
static NSString *const kDiscussionAvailable     = @"discussion_available";
static NSString *const kDiscussionUnavailable   = @"discussion_unavailable";
static NSString *const kTimelineAdd             = @"timeline_add";
static NSString *const kTimelineRemove          = @"timeline_remove";
static NSString *const kVotingAvailable         = @"voting_available";
static NSString *const kVotingUnavailable       = @"voting_unavailable";

// colors

static NSString *const kDefaultSilverColor = @"D9D9D9"; // default color for table selection

// Message for user

static NSString *const kMessageInvalideResponse = @"There was a problem with your request, please try again.";

static NSString *const kTermsAccepted = @"kTermsAccepted";
static NSString *const kUserToken = @"kUserToken";
//static NSString *const kUsersActionVifor = @"kUsersActionVifor";

// Sync data consts

static NSString *const kDateForSyncOnHomePage = @"SyncAllDataOnHomePage";
static NSString *const kSyncFinished = @"SyncFinished";
static NSString *const kHandlePartOfSyncFinished = @"kHandlePartOfSyncFinished";
static NSString *const kPushNotificationReceived = @"kPushNotificationReceived";
static NSString *const kUIUserNotificationTypeChanged = @"kUIUserNotificationTypeChanged";

static NSString *const kESyncDataSessionsDownloaded            = @"SessionsDownloaded";
static NSString *const kESyncDataSessionCategoriesDownloaded   = @"SessionCategoriesDownloaded";
static NSString *const kESyncDataSpeakersDownloaded            = @"SpeakersDownloaded";
static NSString *const kESyncDataMaterialsDownloaded           = @"MaterialsDownloaded";
static NSString *const kESyncDataVenuesDownloaded              = @"VenuesDownloaded";
static NSString *const kESyncDataExhibitorsDownloaded          = @"ExhibitorsDownloaded";
static NSString *const kESyncDataBoothsDownloaded              = @"BoothsDownloaded";
static NSString *const kESyncDataCountriesDownloaded           = @"CountriesDownloaded";
static NSString *const kESyncDataTalksSpeakersSaved            = @"TalksSpeakersSaved";
static NSString *const kESyncDataCurrentPortalDownloaded       = @"CurrentPortalDownloaded";
static NSString *const kESyncDataEventsDownloaded              = @"EventsDownloaded";
static NSString *const kESyncDataTalksDownloaded               = @"TalksDownloaded";
static NSString *const kESyncDataExhibitorsRolesDownloaded     = @"ExhibitorsRolesDownloaded";
static NSString *const kESyncAddTalksToTimeline                = @"SyncAddTalksToTimeline";
static NSString *const kESyncRemoveTalksToTimeline             = @"SyncRemoveTalksToTimeline";
static NSString *const kESyncVoteTalks                         = @"kESyncVoteTalks";
static NSString *const kESyncRemoveVoteTalks                   = @"kESyncRemoveVoteTalks";
static NSString *const kESyncDataSurveysDownloaded             = @"SyncDataSurveysDownloaded";

// connection

static NSString *const kTypeOfInternetConnection = @"TypeOfInternetConnection";

// nsuserdefaults keys
static NSString *const kKeyOSVersion = @"kKeyOSVersion";
static NSString *const kKeyAppVersion = @"kKeyAppVersion";
static NSString *const kKeyAPNSToken = @"apnsToken";

static NSString *const kKeyActiveMaterialBrowsingSession = @"kKeyActiveMaterialBrowsingSession";

static NSString *const kGCMMessageIDKey = @"gcm.message_id";

#endif /* GlobalConstants_h */
