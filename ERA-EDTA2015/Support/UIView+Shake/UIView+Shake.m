//
//  UIView+Shake.m
//  assecobank
//
//  Created by Predrag Despotović on 3/1/13.
//  Copyright (c) 2013 asseco. All rights reserved.
//

#import "UIView+Shake.h"
#import <QuartzCore/QuartzCore.h>

@implementation UIView (Shake)

-(void)shake
{
    CAKeyframeAnimation * anim = [ CAKeyframeAnimation animationWithKeyPath:@"transform" ] ;
    anim.values = @[ [ NSValue valueWithCATransform3D:CATransform3DMakeTranslation(-5.0f, 0.0f, 0.0f) ], [ NSValue valueWithCATransform3D:CATransform3DMakeTranslation(5.0f, 0.0f, 0.0f) ] ] ;
    anim.autoreverses = YES ;
    anim.repeatCount = 2.0f;
    anim.duration = 0.07f ;
    
    [ self.layer addAnimation:anim forKey:nil ] ;
}
@end
