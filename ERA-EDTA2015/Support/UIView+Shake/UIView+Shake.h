//
//  UIView+Shake.h
//  assecobank
//
//  Created by Predrag Despotović on 3/1/13.
//  Copyright (c) 2013 asseco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Shake)
-(void)shake;
@end
