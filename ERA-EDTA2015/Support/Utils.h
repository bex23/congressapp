//
//  Utils.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 9/21/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utils : NSObject

+(NSString *)getDate:(NSString *)datetime;
+(NSString *)getTime:(NSString *)datetime;
+(NSDate *)dateFromString:(NSString *)datetime;
+(NSString*)formatDateString: (NSString*)dateString;
+(NSString*)formatDateString:(NSString *)dateString fromFormat:(NSString *)fromFormatString toFormat:(NSString *)toFormatString;
+(NSString*)formatTimeString:(NSString *)timeString;
+(NSString*)formatTimeStringFromDate:(NSString *)dateString;
+(NSString*)formatDayString:(NSString*)dateString;
+(BOOL)isStringDateToday:(NSString*) otherDayString;

+(BOOL)checkEmailIsValid:(NSString *)email;

+(UIViewController*)topViewController;

+(void)showMessage:(NSString *)message;
+(void)showMessage:(NSString *)message withTitle:(NSString *)title;

+(NSString *)getDevicUUID;

+ (CGSize)findHeightForText:(NSString *)text havingWidth:(CGFloat)widthValue andFont:(UIFont *)font;

@end
