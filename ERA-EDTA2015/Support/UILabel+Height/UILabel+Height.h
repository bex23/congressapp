//
//  UILabel+Height.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 1/9/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Height)

-(CGFloat)heightForWidth:(CGFloat)width;

@end
