//
//  UILabel+Height.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 1/9/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "UILabel+Height.h"

@implementation UILabel (Height)

- (CGFloat)heightForWidth:(CGFloat)width
{
    CGSize constraint = CGSizeMake(width, MAXFLOAT);
    CGSize size;
    
    CGSize boundingBox = [self.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:self.font}
                                                  context:nil].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    return size.height;
}

@end
