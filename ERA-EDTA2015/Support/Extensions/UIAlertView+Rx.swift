//
//  UIAlertView+Rx.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 2/7/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

extension UIViewController {
    
    func alert(title: String, text: String?) -> Observable<Void> {
        return Observable.create { [weak self] observer in
            let alertVC = UIAlertController(title: title, message: text, preferredStyle: .alert)
            alertVC.addAction(UIAlertAction(title: "OK", style: .default, handler: {_ in
                observer.onCompleted()
            }))
            self?.present(alertVC, animated: true, completion: nil)
            return Disposables.create {
                self?.dismiss(animated: true, completion: nil)
            }
        }
    }
    
}
