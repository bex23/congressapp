//
//  UISegmentedControl+Rx.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 1/26/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RxCocoa
import RxSwift

extension Reactive where Base: UISegmentedControl {
    
    /// Bindable sink for `startAnimating()`, `stopAnimating()` methods.
    public var items: Binder<[Date]> {
        return Binder(self.base) { segmentedControl, items in
            let df =  DateFormatter()
            df.dateFormat = "M/d"
            segmentedControl.removeAllSegments()
            items.enumerated().forEach { index, element in
                segmentedControl.insertSegment(withTitle: df.string(from: element), at: index, animated: true)
            }
            segmentedControl.selectedSegmentIndex = 0
        }
    }
    
    /// Reactive wrapper for `TouchUpInside` control event.
    public var valueChanged: ControlEvent<Void> {
        return controlEvent(.valueChanged)
    }
    
}
