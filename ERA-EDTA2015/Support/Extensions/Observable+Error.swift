//
//  Observable+Error.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 2/5/18.
//  Copyright © 2018 Navus. All rights reserved.
//

import RxSwift
import Moya

extension ObservableType {
    
    public func catchServerError(handler: @escaping (String) -> Observable<Self.E>) -> Observable<Self.E> {
        return catchError { error -> Observable<Self.E> in
            guard let e = error as? MoyaError else { throw error }
            switch e {
            case .statusCode(let response):
                guard
                    let serverResponseOptional = try? response.mapJSON(failsOnEmptyData: true) as? [String: Any],
                    let serverResponse = serverResponseOptional
                    else { throw MoyaError.jsonMapping(response) }
                guard let message = serverResponse["message"] as? String else { throw error }
                return handler(message)
            case.underlying(let error, _):
                return handler(error.localizedDescription)
            default:
                throw error
            }
        }
    }
    
}
