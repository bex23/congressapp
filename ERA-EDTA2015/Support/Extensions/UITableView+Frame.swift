//
//  UITableView+Frame.swift
//  ERA-EDTA2015
//
//  Created by Dejan Bekic on 1/26/18.
//  Copyright © 2018 Navus. All rights reserved.
//

extension UITableView {
    
    func setFrame(forIndex index: Int) {
        guard let superview = superview else {
            return
        }
        let width = UIScreen.main.bounds.width
        frame = CGRect(x: width * CGFloat(index),
                       y: 0,
                       width: width,
                       height: superview.frame.height)
    }
    
}
