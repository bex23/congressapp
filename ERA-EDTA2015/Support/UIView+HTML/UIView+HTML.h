//
//  UIView+HTML.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 2/20/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (HTML)

-(void)setHTMLFromString:(NSString *)string;

@end
