//
//  UIView+HTML.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 2/20/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "UIView+HTML.h"

@implementation UIView (HTML)

-(void)setHTMLFromString:(NSString *)string
{
    if ([self isKindOfClass:[UITextView class]] || [self isKindOfClass:[UILabel class]])
    {
        UIFont *font = [UIFont fontWithName:@"Helvetica" size:17];
        string = [string stringByAppendingString:[NSString stringWithFormat:@"<style>body{font-family: '%@'; font-size:%fpx;}</style>",
                                                  font.fontName,
                                                  font.pointSize]];
       
       NSAttributedString *attString = [[NSAttributedString alloc] initWithData:[string dataUsingEncoding:NSUnicodeStringEncoding]
                                         options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                   NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
                              documentAttributes:nil
                                           error:nil];
        
        [self setValue:attString forKey:@"attributedText"];
    }
}

@end
