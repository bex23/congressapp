//
//  SinglePickerTableViewCell.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 12/22/16.
//  Copyright © 2016 Navus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SinglePickerTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblColor;
@property (weak, nonatomic) IBOutlet UILabel *lblText;
@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;

@property (assign, nonatomic) UIColor *labelsColor;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLblColorWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintImgIconWidth;

@end
