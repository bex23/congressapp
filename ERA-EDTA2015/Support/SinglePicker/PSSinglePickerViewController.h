//
//  PSSinglePickerViewController.h
//  ERA-EDTA2013
//
//  Created by admin on 3/3/14.
//  Copyright (c) 2014 Raul Catena. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PSSinglePickerViewDelegate <NSObject>

- (void)SinglePickerItemSelected:(id)item Sender:(id)sender;

@optional
- (void)SinglePickerSelectionDone:(id)sender;

@end

@interface PSSinglePickerViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSArray* itemsToDisplay;
@property (nonatomic, assign)id<PSSinglePickerViewDelegate> delegate;
@property (nonatomic, strong) NSString* popovertag;
@property (nonatomic, strong) NSString* displayMember;

// table properties
@property (retain, nonatomic) IBOutlet UITableView *tableView;
@property (assign, nonatomic) BOOL disableTableViewScrollable;
@property (assign, nonatomic) BOOL disableTableViewHasSeparator;
@property (assign, nonatomic) BOOL disableTableViewCellSelectionStyle;
@property (strong, nonatomic) UIColor *tableViewTintColor;
//
@property (strong, nonatomic) NSArray *arrayDisabledCheckmarksForIndexs;

// colors in hex-a
@property (strong, nonatomic) UIColor *backgroudColor;
@property (strong, nonatomic) UIColor *textColor;
@property (strong, nonatomic) UIColor *iconColor;

// colorable cells properties
@property (nonatomic, strong) NSString *colorKey;

@property (nonatomic, assign) int selectedRow;

// image in cells
@property (nonatomic, strong) NSString *imageKey;

- (id)initWithItems:(NSArray*)items DisplayMember:dismem;
- (id)initWithItems:(NSArray*)items DisplayMember:(NSString*)dismem Style:(UITableViewStyle) style;
- (void)selectionDone:(id)sender;


@end
