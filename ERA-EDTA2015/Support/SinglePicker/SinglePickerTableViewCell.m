//
//  SinglePickerTableViewCell.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 12/22/16.
//  Copyright © 2016 Navus. All rights reserved.
//

#import "SinglePickerTableViewCell.h"

@implementation SinglePickerTableViewCell

-(void)setLabelsColor:(UIColor *)labelsColor
{
    if (labelsColor)
    {
        self.lblColor.backgroundColor = labelsColor;
    }
    else
    {
        self.lblColor.backgroundColor = [UIColor grayColor];
    }
    
    _constraintLblColorWidth.constant = 15;
    
    _labelsColor = labelsColor;
}
@end
