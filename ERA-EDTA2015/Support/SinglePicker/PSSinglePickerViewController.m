//
//  PSSinglePickerViewController.m
//  ERA-EDTA2013
//
//  Created by admin on 3/3/14.
//  Copyright (c) 2014 Raul Catena. All rights reserved.
//

#import "PSSinglePickerViewController.h"

#import "SinglePickerTableViewCell.h"

@interface PSSinglePickerViewController ()

@end

@implementation PSSinglePickerViewController

@synthesize itemsToDisplay, delegate, displayMember, popovertag, selectedRow;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super init];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithItems:(NSArray*)items DisplayMember:(NSString*)dismem;
{
    self = [super init];
    if (self) {
        self.itemsToDisplay = items;
        self.displayMember = dismem;
    }
    return self;
}

- (id)initWithItems:(NSArray*)items DisplayMember:(NSString*)dismem Style:(UITableViewStyle) style
{
    self = [super init];
    if (self) {
        self.itemsToDisplay = items;
        self.displayMember = dismem;
    }
    return self;
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // table view scrollable
    if (_disableTableViewScrollable == YES) {
        _tableView.scrollEnabled = !_disableTableViewScrollable;
    }
    
    // table view separator
    if (_disableTableViewHasSeparator == YES) {
        self.tableView.separatorColor = [UIColor clearColor];
    }
    
    if (self.tableViewTintColor)
    {
        [self.tableView setTintColor:self.tableViewTintColor];
    }
    else
    {
        [self.tableView setTintColor:[UIColor colorWithHex:kOtherColor andAlpha:1.0]];
    }
    
    // register session cell
    UINib *nib = [UINib nibWithNibName:NSStringFromClass([SinglePickerTableViewCell class]) bundle:nil];
    [[self tableView] registerNib:nib forCellReuseIdentifier:NSStringFromClass([SinglePickerTableViewCell class])];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 300.0;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    //	return YES;
    return  (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown);
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [itemsToDisplay count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SinglePickerTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([SinglePickerTableViewCell class])];
    
    if(_colorKey)
    {
        [cell setLabelsColor:[SessionCategory getColorForSessionCategoryId:[[itemsToDisplay objectAtIndex:indexPath.row] valueForKey:_colorKey]]];
    }
    
    // Configure the cell..
    NSString *strText = @"";
    if(displayMember)
    {
        strText = [[itemsToDisplay objectAtIndex:indexPath.row] valueForKey:displayMember];
    }
    else
    {
        strText = [itemsToDisplay objectAtIndex:indexPath.row];
    }
    
    cell.lblText.text = strText;
    
    if (indexPath.row == self.selectedRow
        && ![_arrayDisabledCheckmarksForIndexs containsObject:[NSNumber numberWithInteger:indexPath.row]])
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    // text color
    if (_textColor) {
        [cell.lblText setTextColor:_textColor];
    }
    
    // background color
    if (_backgroudColor) {
        [cell setBackgroundColor:_backgroudColor];
    }
    
    // selection style
    if (_disableTableViewCellSelectionStyle == YES) {
        cell.selectionStyle = UITableViewCellEditingStyleNone;
    }
    
    if(_imageKey)
    {
        cell.imgIcon.image = [UIImage imageNamed:[[itemsToDisplay objectAtIndex:indexPath.row] valueForKey:_imageKey]];
        
        if (_iconColor)
        {
            cell.imgIcon.image = [cell.imgIcon.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [cell.imgIcon setTintColor:_iconColor];
        }
    }
    else
    {
        cell.constraintImgIconWidth.constant = 0;
    }
    
    // because on iOS 8 doesn't work
    // self.tableView.rowHeight = UITableViewAutomaticDimension;
    // self.tableView.estimatedRowHeight = 200.0;
    [cell updateConstraintsIfNeeded];
    
    return cell;
}

// because on iOS 8 doesn't work
// self.tableView.rowHeight = UITableViewAutomaticDimension;
// self.tableView.estimatedRowHeight = 200.0;
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedRow = (int)indexPath.row;
    
    if(delegate)
    {
        [delegate SinglePickerItemSelected:[itemsToDisplay objectAtIndex:indexPath.row] Sender:self];
    }
    
    [self.tableView reloadData];
}

-(void)selectionDone:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    [self.delegate SinglePickerSelectionDone:self];
}

#pragma mark - Set left margin of table cells to ziro for iOS 8

-(void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

@end
