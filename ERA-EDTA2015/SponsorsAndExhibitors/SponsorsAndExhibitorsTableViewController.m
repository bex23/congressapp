//
//  SponsorsAndExhibitorsTableViewController.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 2/27/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "SponsorsAndExhibitorsTableViewController.h"

#import "ExhibitorTableViewCell.h"
#import "ExhibitorDetailsViewController.h"

@interface SponsorsAndExhibitorsTableViewController ()

//@property (strong, nonatomic) NSFetchedResultsController* fetchedResultsController;
//@property (strong, nonatomic) NSManagedObjectContext* managedObjectContext;

@property (strong, nonatomic) NSArray *arrayExhibitors;
@property (strong, nonatomic) NSDictionary *dictExhibitorsRolesColors;

@end

//static int const kSectionHeight = 45;

@implementation SponsorsAndExhibitorsTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 300.0;
    
    // register exhibitor cell
    UINib *nibExhibitor = [UINib nibWithNibName:NSStringFromClass([ExhibitorTableViewCell class]) bundle:nil];
    [[self tableView] registerNib:nibExhibitor forCellReuseIdentifier:NSStringFromClass([ExhibitorTableViewCell class])];
    
//    self.managedObjectContext = [LLDataAccessLayer sharedInstance].managedObjectContext;
    
    self.dictExhibitorsRolesColors = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"exhibitorsRolesSettings" ofType:@"plist"]];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self doFetch];
}

#pragma mark - FRC
/*
-(NSFetchedResultsController *)fetchedResultsController
{
    if (!_fetchedResultsController)
    {
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"ExhibitorRole" inManagedObjectContext:self.managedObjectContext];
        
        [fetchRequest setEntity:entity];
        [fetchRequest setResultType:NSDictionaryResultType];
        [fetchRequest setPropertiesToFetch:@[@"erExhibitorId", @"erRoleName"]];
        [fetchRequest setReturnsDistinctResults:YES];
        
        NSSortDescriptor *sortDescriptorRoleName = [[NSSortDescriptor alloc] initWithKey:@"erOrderPosition" ascending:YES];
        NSSortDescriptor *sortDescriptorExhId = [[NSSortDescriptor alloc] initWithKey:@"erExhibitorId" ascending:YES selector:@selector(localizedStandardCompare:)];
        NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptorRoleName, sortDescriptorExhId, nil];
        
        [fetchRequest setSortDescriptors:sortDescriptors];
        
        NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:@"erRoleName" cacheName:nil];
        
        self.fetchedResultsController = aFetchedResultsController;
    }
    
    return _fetchedResultsController;
}
*/
-(void)doFetch
{
//    self.fetchedResultsController = nil;
    
//    _arrayExhibitors = [[Exhibitor getExhibitors] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"roles.@count == 0"]];
    
    _arrayExhibitors = [Exhibitor getExhibitors];

    
//    NSError *error;
//    if (![self.fetchedResultsController performFetch:&error]) {
//        NSLog(@"error %@ %@", error, error.userInfo);
//        abort();
//    }
    
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
//    NSInteger count = [[_fetchedResultsController sections] count];
//
//    if (_arrayExhibitors.count > 0)
//    {
//        count = count + 1;
//    }
//
//    return count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int rows = 0;
    
//    if (section == tableView.numberOfSections - 1 && _arrayExhibitors.count > 0)
//    {
        rows = (int)_arrayExhibitors.count;
//    }
//    else if ([[_fetchedResultsController sections] count] > 0)
//    {
//        id <NSFetchedResultsSectionInfo> sectionInfo = [[_fetchedResultsController sections] objectAtIndex:section];
//        rows = (int)[sectionInfo numberOfObjects];
//    }
    
    return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ExhibitorTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ExhibitorTableViewCell class])];
    
    Exhibitor *exhibitor;
    
    // its last section -> exhibitors
//    if (indexPath.section == tableView.numberOfSections - 1 && _arrayExhibitors.count > 0)
//    {
        exhibitor = [_arrayExhibitors objectAtIndex:indexPath.row];
//    }
//    else
//    {
//        NSDictionary *exRole = [_fetchedResultsController objectAtIndexPath:indexPath];
//        exhibitor = [Exhibitor getExhibitorWithId:exRole[@"erExhibitorId"]];
//    }
    
    UIImage* img = [UIImage imageNamed:@"company_placeholder"];
    
    if(exhibitor.exhImage.length > 0)
    {
        NSString* request = [exhibitor.exhImage stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL* url = [NSURL URLWithString:request];
        [cell.imgExibitorLogo setImageWithURL:url  placeholderImage:img];
    }
    else
    {
        cell.imgExibitorLogo.image = img;
    }
    
    cell.lblExibitorName.text = exhibitor.exhName;
    
    [cell updateConstraintsIfNeeded];
    
    return cell;
}

#pragma mark - Table view delegate

//-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return kSectionHeight;
//}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
/*
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, kSectionHeight)];
    
    // set default header color
    [headerView setBackgroundColor:[UIColor whiteColor]];
    
    // add label for title
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(8, 8, tableView.bounds.size.width - 16, kSectionHeight - 16)];
    UILabel *lblColorLine = [[UILabel alloc] initWithFrame:CGRectMake(50, 35, tableView.bounds.size.width - 100, 3)];
    
    NSString *sectionTitle = @"";
    
//    if (section == tableView.numberOfSections - 1 && _arrayExhibitors.count > 0) // its last section -> exhibitors
//    {
        sectionTitle = @"EXHIBITORS";
        [lblColorLine setBackgroundColor:[UIColor colorWithHex:@"326FA5" andAlpha:1.0]];
//    }
//    else
//    {
//        sectionTitle = [[[_fetchedResultsController sections] objectAtIndex:section] name];
//        sectionTitle = [[sectionTitle stringByReplacingOccurrencesOfString:@"_" withString:@" "] uppercaseString];
//
//         change header color
//        NSString *colorHex = [_dictExhibitorsRolesColors objectForKey:[[[_fetchedResultsController sections] objectAtIndex:section] name]][@"color"];
//
//        if (colorHex)
//        {
//           [lblColorLine setBackgroundColor:[UIColor colorWithHex:colorHex andAlpha:1.0]];
//        }
//    }
    
    label.text = sectionTitle;
    
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:@"Helvetica-Bold" size:17];
    
    [headerView addSubview:label];
    [headerView addSubview:lblColorLine];
    
    return headerView;
}
*/
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    Exhibitor *exhibitor;
    
//    if (indexPath.section == tableView.numberOfSections - 1 && _arrayExhibitors.count > 0) // its last section -> exhibitors
//    {
        exhibitor = [_arrayExhibitors objectAtIndex:indexPath.row];
//    }
//    else
//    {
//        NSDictionary *exRole = [_fetchedResultsController objectAtIndexPath:indexPath];
//        exhibitor = [Exhibitor getExhibitorWithId:exRole[@"erExhibitorId"]];
//    }
    
    ExhibitorDetailsViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([ExhibitorDetailsViewController class])];
    
    [vc setExhibitor:exhibitor];
    
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Set left margin of table cells to ziro for iOS 8

-(void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

@end
