//
//  ExhibitorDetailsViewController.h
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 3/1/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExhibitorDetailsViewController : UIViewController

@property (strong, nonatomic) Exhibitor *exhibitor;

@end
