//
//  ExhibitorDetailsViewController.m
//  ERA-EDTA2015
//
//  Created by Maja Kuzman on 3/1/17.
//  Copyright © 2017 Navus. All rights reserved.
//

#import "ExhibitorDetailsViewController.h"

#import "ExhibitorMapViewController.h"

@interface ExhibitorDetailsViewController ()

@property (weak, nonatomic) IBOutlet UILabel *lblSponsorType;
@property (weak, nonatomic) IBOutlet UILabel *lblBoothNoText;
@property (weak, nonatomic) IBOutlet UILabel *lblBoothNo;
@property (weak, nonatomic) IBOutlet UIImageView *imgLogo;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintImgLogoHeight;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblTelText;
@property (weak, nonatomic) IBOutlet UILabel *lblTel;
@property (weak, nonatomic) IBOutlet UILabel *lblEmailText;
@property (weak, nonatomic) IBOutlet UILabel *lblEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblSiteUrl;

@property (weak, nonatomic) IBOutlet UIButton *btnVisitUsInTheExhibitionHall;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contraintHeightOfButtonVisitUsInExhbitionHall;
@property (weak, nonatomic) IBOutlet UIImageView *imgTopBanner;

@end

@implementation ExhibitorDetailsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    

    
    [self loadData];
    
    [[BannersServices shared] addWithImageView:self.imgTopBanner
                                            at:BannerPositionTopBanner
                                   sender: self];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}



#pragma mark - Load data

-(void)loadData
{
    if (_exhibitor.roles.count > 0)
    {
        self.title = @"Sponsor Details";
    }
    else
    {
        self.title = @"Exhibitor Details";
    }
    
    // SETUP SPONSOR TYPE
    NSString *sponsorTypes = @"";
    
    for (ExhibitorRole *exhRole in _exhibitor.roles)
    {
        NSString *roleName = [NSString stringWithFormat:@"%@,", exhRole.erRoleName];
        
        // don't show double role name, some exhibitors can be GOLDE SPONSOR more times.
        if (![sponsorTypes containsString:exhRole.erRoleName])
        {
            sponsorTypes = [sponsorTypes stringByAppendingString:roleName];
        }
    }
    
    // remove last one comma ,
    if (sponsorTypes.length > 0)
    {
        sponsorTypes = [sponsorTypes substringToIndex:[sponsorTypes length]-1];
    }

    sponsorTypes = [sponsorTypes stringByReplacingOccurrencesOfString:@"_" withString:@" "];
    _lblSponsorType.text = [sponsorTypes uppercaseString];
    
    self.contraintHeightOfButtonVisitUsInExhbitionHall.constant = 60;
    self.btnVisitUsInTheExhibitionHall.hidden = NO;
    
    // SETUP BOOTH NO
    // SETUP TEL
    if (_exhibitor.exhImportedId.length > 0)
    {
        self.lblBoothNoText.text = @"Booth no:";
        self.lblBoothNo.text = _exhibitor.exhImportedId;
    }
    else
    {
        self.lblBoothNoText.text = self.lblBoothNo.text = nil;
    }
    
    if (_exhibitor.exhImportedId == nil)
    {
        self.contraintHeightOfButtonVisitUsInExhbitionHall.constant = 0;
        self.btnVisitUsInTheExhibitionHall.hidden = YES;
    }
    
    // SETUP IMAGE
    if (_exhibitor.exhImage.length > 0)
    {
        UIImage* img = [UIImage imageNamed:@"company_placeholder"];
        
        NSString* request = [_exhibitor.exhImage stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL* url = [NSURL URLWithString:request];
        [self.imgLogo setImageWithURL:url placeholderImage:img];
        _constraintImgLogoHeight.constant = 70;
    }
    else
    {
        _constraintImgLogoHeight.constant = 0;
    }
    
    // SETUP TITLE
    _lblTitle.text = _exhibitor.exhName;
    
    _lblDescription.attributedText = nil;
    
    // SETUP DESCRIPTION
    NSString *htmlString = _exhibitor.exhDescription;
    
    if (htmlString.length > 0)
    {
        UIFont *regularFont = [UIFont systemFontOfSize:15.0];
        
        NSMutableAttributedString * attrStr = [[NSMutableAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
        
        // And before you set the bold range, set your attributed string (the whole range!) to the new attributed font name
        [attrStr setAttributes:@{ NSFontAttributeName: regularFont } range:NSMakeRange(0, attrStr.length - 1)];
        
        _lblDescription.attributedText = attrStr;
    }

    // SETUP ADDRESS
    if (self.exhibitor.exhAddress1.length > 0 || self.exhibitor.exhAddress2.length > 0)
    {
        self.lblAddress.text = [NSString stringWithFormat:@"%@ %@", self.exhibitor.exhAddress1, self.exhibitor.exhAddress2];
    }
    else
    {
        self.lblAddress.text = @"";
    }
    
    // SETUP TEL
    if (_exhibitor.exhPhone.length > 0)
    {
        self.lblTelText.text = @"tel:";
        self.lblTel.text = _exhibitor.exhPhone;
    }
    else
    {
        self.lblTelText.text = self.lblTel.text = @"";
    }
    
    // SETUP EMAIL
    if (_exhibitor.exhEmail.length > 0)
    {
        self.lblEmailText.text = @"email:";
        self.lblEmail.text = _exhibitor.exhEmail;
    }
    else
    {
        self.lblEmailText.text = self.lblEmail.text = @"";
    }
    
    
    // SETUP SITE URL
    _lblSiteUrl.text = _exhibitor.exhWebsite;
}

#pragma mark - IBActions

- (IBAction)actionOpenSite:(id)sender
{
    if (_exhibitor.exhWebsite.length > 0)
    {
        [CUtils showLeavingTheAppMessageWithUrl:_exhibitor.exhWebsite];
    }
}

- (IBAction)actionGoToExhibitionHall:(id)sender
{
    if(kEnableExhibitorsMap == NO)
    {
        [CUtils showNotAvailableYetMessage];
        return;
    }
    
    NSString *strID = NSStringFromClass([ExhibitorMapViewController class]);
    ExhibitorMapViewController *vc = [[UIStoryboard storyboardWithName:strID bundle:nil] instantiateViewControllerWithIdentifier:strID];
    
    [vc setExibitor:_exhibitor];
    
    [self.navigationController pushViewController:vc animated:YES];
}

@end
